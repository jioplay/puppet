class mobi_livetovod_adapter::params {
  $package_name = "mobi-ril-livetovod-adapter"
  $package_ensure = undef
  $livetovod_version = ""
  $icinga = "true"
  $icinga_instance = "icinga"
  $icinga_cmd_args = "-I $::ipaddress -p 8080 -u  /mobi-ril-livetovod-adapter/monitoring/health -w 5 -c 10"

  $solr_server_url = "http://solrmastervip:8080/mobi-solr/"
  $storage_recording_provider = "http://recordingmanagervip:8080/mobi-storage-manager/"
  $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100"
  $recording_manager_profile_id = "MTEwMDAwOTE1NyNiYWxrcnVzaG5hY2hhdmhhbiM5MDg4YzI2Yi0xYWI2LTRjNjktOTM5My02MmQ1MDBmY2RlMmQ"
  $livetovod_time_interval = "1296000000"
  $livetovod_cron_expression = "0 0/5 * * * ?"
  $livetovod_job_enabled = false
  $thumbnail_upload_path = "/var/Jukebox/uploads/thumbnails/livetovod/"
  $recording_manager_endpoint_url = "http://recordingmanagervip:8080/mobi-recording-manager"
  $thumbnail_upload_url = "http://damvip:8080/dam/services/thumbnails/getThumbnail"
  $thumbnail_allowed_formats = "jpg,png"
  $update_metadata_excluded_parameters = "thumbnail_formats,expires,start_of_availabilty,skus_ids,upload_date,vid,crid,episode_number,media_aspect_ratio,media_class,provider_id,media_id"
}
