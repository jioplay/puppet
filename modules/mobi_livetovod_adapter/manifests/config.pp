class mobi_livetovod_adapter::config {
   file {"/opt/mobi-ril-livetovod-adapter/conf/application-config-override.xml":
        ensure => present,
        owner => "rtv",
        group => "rtv",
        content => template("mobi_livetovod_adapter/application-config-override.xml.erb"),
        require => Class["mobi_livetovod_adapter::install"],
        notify => Class["tomcat::service"],
    }

}
