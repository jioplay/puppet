## == Class: mobi_livetovod_adapter
#
# Manage the mobi_livetovod_adapter component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
# $package_name - Mobi package name
# $package_ensure - Version of livetovod rpm
# $icinga - whether icinga is enabled or not
# $icinga_instance - icinga hostname to register with
# $icinga_cmd_args - icinga healthcheck configuration string
# $solr_server_url - vip for solrmaster
# $storage_recording_provider - storage manager to communicate with
# $amq_broker_url - amq server
# $recording_manager_profile_id - recording manager profile
# $livetovod_time_interval
# $livetovod_cron_expression - cron expression
# $livetovod_job_enabled - whether cron job is enabled
# $thumbnail_upload_path - upload path on isilon for thumbnails
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_livetovod_adapter (
  $package_name = $mobi_livetovod_adapter::params::package_name,
  $livetovod_version = $mobi_livetovod_adapter::params::livetovod_version,
  $package_ensure = $mobi_livetovod_adapter::params::package_name,
  $icinga = $mobi_livetovod_adapter::params::icinga,
  $icinga_instance = $mobi_livetovod_adapter::params::icinga_instance,
  $icinga_cmd_args = $mobi_livetovod_adapter::params::icinga_cmd_args,
  $solr_server_url = $mobi_livetovod_adapter::params::solr_server_url,
  $storage_recording_provider = $mobi_livetovod_adapter::params::storage_recording_provider,
  $amq_broker_url = $mobi_livetovod_adapter::params::amq_broker_url,
  $recording_manager_profile_id = $mobi_livetovod_adapter::params::recording_manager_profile_id,
  $livetovod_time_interval = $mobi_livetovod_adapter::params::livetovod_time_interval,
  $livetovod_cron_expression = $mobi_livetovod_adapter::params::livetovod_cron_expression,
  $livetovod_job_enabled = $mobi_livetovod_adapter::params::livetovod_job_enabled,
  $thumbnail_upload_path = $mobi_livetovod_adapter::params::thumbnail_upload_path,
  $recording_manager_endpoint_url = $mobi_livetovod_adapter::params::recording_manager_endpoint_url,
  $thumbnail_upload_url = $mobi_livetovod_adapter::params::thumbnail_upload_url,
  $thumbnail_allowed_formats = $mobi_livetovod_adapter::params::thumbnail_allowed_formats,
  $update_metadata_excluded_parameters = $mobi_livetovod_adapter::params::update_metadata_excluded_parameters,
) inherits mobi_livetovod_adapter::params {

    anchor { "mobi_livetovod_adapter::begin": } ->
    class { "mobi_livetovod_adapter::install": } ->
    class { "mobi_livetovod_adapter::config": } ->
    class { "mobi_livetovod_adapter::icinga": } ->
    anchor { "mobi_livetovod_adapter::end": }

    os::motd::register{ "${name}":}
}
