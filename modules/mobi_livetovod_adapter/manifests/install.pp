class mobi_livetovod_adapter::install {
  package { "${::mobi_livetovod_adapter::package_name}":
    ensure => "${::mobi_livetovod_adapter::livetovod_version}",
    notify   => Class["tomcat::service"],
  }
}
