class flume::node {

  # need param class
        $flumemasterservers = 'flume1.mobitv.corp'
        $flumecollectoreventhost = 'flume1.mobitv.corp'


  $pkgs = [  'flume-node' ,
    ]

  package { $pkgs:
    ensure  => latest ,
    require  => [ Class['yum::cdh3'] ,
      ]
  }

  service { 'flume-node':
    ensure  => running ,
    enable  => true ,
    hasrestart  => true ,
    require  => File['/etc/flume/conf/flume-site.xml'] ,
  }

  file {'/etc/flume/conf/flume-site.xml':
    ensure  => file ,
    require  => Package[$pkgs] ,
    content  => template('flume/flume-site.xml.erb') ,
  }

}

