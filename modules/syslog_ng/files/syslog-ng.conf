# syslog-ng configuration file.
#
# This should behave pretty much like the original syslog on RedHat. But
# it could be configured a lot smarter.
#
# See syslog-ng(8) and syslog-ng.conf(5) for more information.
#

options {
        sync (0);
        time_reopen (10);
        log_fifo_size (1000);
        long_hostnames (off);
        use_dns (no);
        use_fqdn (no);
        create_dirs (no);
        keep_hostname (yes);
        log_msg_size(16348); 
        perm(0644);
};

source s_sys {
        file ("/proc/kmsg" log_prefix("kernel: "));
        unix-stream ("/dev/log" max_connections(2048));
        internal();
        # udp(ip(0.0.0.0) port(514));
};

destination d_cons { file("/dev/console"); };
destination d_mesg { file("/var/log/messages"); };
destination d_auth { file("/var/log/secure"); };
#destination d_auth { file("/var/log/auth.log"); };
#destination d_mail { file("/var/log/maillog" sync(10)); };
destination d_mail { file("/var/log/mail.log" sync(10)); };
destination d_spol { file("/var/log/spooler"); };
destination d_boot { file("/var/log/boot.log"); };
# Changed from cron.log, kern.log to cron/kern
destination d_cron { file("/var/log/cron"); };
destination d_kern { file("/var/log/kern"); };
destination d_mlal { usertty("*"); };
destination d_daemon { file("/var/log/daemon.log"); };
destination d_debug { file("/var/log/debug.log"); };
# MobiTV:
# better loghost-x selection method (http://twiki.mobitv.corp/bin/view/Ops/SyslogInfrastructure)
#destination d_smf1remote { tcp("loghost-b.smf1.mobitv" port(514)); };
destination d_fmp4 { file("/var/log/fmp4.log"); };
destination d_fmp4_debug { file("/var/log/fmp4_debug.log"); };
destination d_puppet { file("/var/log/puppet/puppet.log"); };

# New loghost ports
destination d_cron_remote { tcp("logcollector.smf1.mobitv" port(6011)); };
destination d_auth_remote { tcp("logcollector.smf1.mobitv" port(6012)); };
destination d_mesg_remote { tcp("logcollector.smf1.mobitv" port(6013)); };
destination d_kern_remote { tcp("logcollector.smf1.mobitv" port(6014)); };
destination d_fmp4_remote { tcp("logcollector.smf1.mobitv" port(6015)); };
destination d_fmp4_debug_remote { tcp("logcollector.smf1.mobitv" port(6016)); };

filter f_kernel     { facility(kern); };
filter f_default    { level(info..emerg) and
                        not (facility(mail)
                        or facility(authpriv) 
                        or facility(cron) 
			or match("puppet-agent") ); };
filter f_auth       { facility(authpriv); };
filter f_mail       { facility(mail); };
filter f_emergency  { level(emerg); };
filter f_news       { facility(uucp) or
                        (facility(news) 
                        and level(crit..emerg)); };
filter f_boot   { facility(local7); };
filter f_local0 { facility(local0); };
filter f_local1 { facility(local1); };
filter f_local2 { facility(local2); };
filter f_local3 { facility(local3); };
filter f_local4 { facility(local4); };
filter f_local5 { facility(local5); };
filter f_local6 { facility(local6); };
filter f_local7 { facility(local7); };
filter f_cron   { facility(cron); };
filter f_user   { facility(user); };
filter f_debug { not facility(auth, authpriv, mail, local0); };
filter f_messages { level(info..emerg) 
        and not facility(auth, authpriv, mail, cron, local0); };
filter f_info { level(info); };
filter f_notice { level(notice); };
filter f_warn { level(warn); };
filter f_crit { level(crit); };
filter f_err { level(err); };
# MobiTV:
filter f_syslog { not match("STATS: dropped 0"); };
filter f_nagios { not match("opt/ReachTV/Farm/monitor/bin"); };
filter f_fmp4 { facility(local0) and level(info..emerg); };
filter f_puppet { match("puppet-agent"); };

#log { source(s_sys); filter(f_kernel); destination(d_cons); };
log { source(s_sys); filter(f_kernel); destination(d_kern); destination(d_kern_remote); };
log { source(s_sys); filter(f_default); destination(d_mesg); destination(d_mesg_remote); };
log { source(s_sys); filter(f_auth); destination(d_auth); };
log { source(s_sys); filter(f_mail); destination(d_mail); };
log { source(s_sys); filter(f_emergency); destination(d_mlal); };
log { source(s_sys); filter(f_news); destination(d_spol); };
log { source(s_sys); filter(f_boot); destination(d_boot); };
log { source(s_sys); filter(f_cron); destination(d_cron); destination(d_cron_remote); };
# MobiTV: TODO verify filters on remote logging still needed below
#log { source(s_sys); filter(f_local0); destination(jukebox); };
#log { source(s_sys); filter(f_syslog); filter(f_nagios); destination(d_smf1remote); };
log { source(s_sys); filter(f_fmp4); destination(d_fmp4); }; 
# For fmp4 Debug logging uncomment the following
# Local logging of fmp4_debug activity filtered out by default. To start remote logging, uncomment the following
#log { source(s_sys); filter(f_local0); destination(d_fmp4_debug_remote); };
#log { source(s_sys); filter(f_local0); destination(d_fmp4_debug); };
log { source(s_sys); filter(f_puppet); destination(d_puppet); };

# NetPVR
#log { source(s_sys); filter(f_netpvr); destination(netpvr); };
#uncomment below to turn on debug logging for NetPVR
destination netpvr_debug { file("/var/log/mobi-recordingwriter/recordingwriter_debug.log"); };
log { source(s_sys); filter(f_local0); destination(netpvr_debug); };

destination netpvr_debug { file("/var/log/mobi-jobdistributor/jobdistributor_debug.log"); };
log { source(s_sys); filter(f_local0); destination(netpvr_debug); };

