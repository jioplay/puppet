# == Class: syslog_ng
#
# This module manages syslog_ng
#
# === Parameters:
#
#  $package::          Name of the rpm package to install (e.g. "syslog_ng")
#  $package_ensure::   Package ensure state (e.g. present)
#  $service::          The name of the service
#  $service_ensure::   The service ensure state (e.g. running)
#  $service_enable::   The service startup enabled (true | false)
#  $manage_config::    Should this module manage the config file.
#  $config::           The config file resource to use.
#  $sys_config::       The sysconfig file to use.
#  $logrotate_config:: The logrotate configuration to use.
#
# === Requires:
#
# === Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class syslog_ng (

  $package = $::syslog_ng::params::package,
  $package_ensure = $::syslog_ng::params::package_ensure,

  $service = $::syslog_ng::params::service,
  $service_ensure = $::syslog_ng::params::service_ensure,
  $service_enable = $::syslog_ng::params::service_enable,

  $config = $::syslog_ng::params::config,
  $manage_config = $::syslog_ng::params::manage_config,
  $logrotate_config = $::syslog_ng::params::logrotate_config,

) inherits syslog_ng::params {

  if defined(Class["rsyslog"]) {
    fail("Cannot include rsyslog and syslog-ng; choose one or the other")
  }

  anchor {  "syslog_ng::begin": } ->
  class { "syslog_ng::install": } ->
  class { "syslog_ng::config": } ->
  anchor { "syslog_ng::end": }

  class { "syslog_ng::service": }

  # include module name in motd
  os::motd::register { $name : }
}

