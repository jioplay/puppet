class syslog_ng::service {

  # disable other logging services
  service { "syslog":
    enable    => false,
    ensure    => stopped,
    hasstatus => true,
  }

  service { "rsyslog":
    enable    => false,
    ensure    => stopped,
    hasstatus => true,
  }

  service { $::syslog_ng::service:
    ensure      => $::syslog_ng::service_ensure,
    enable      => $::syslog_ng::service_enable,
    hasstatus   => true,
    hasrestart  => true,
    subscribe   => [ Service["syslog"],
                     Service["rsyslog"],
                     Class["syslog_ng::install"],
                     Class["syslog_ng::config"], ]
  }
}
