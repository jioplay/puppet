class syslog_ng::install {

    #if the package has another name make sure and remove normal package
    $remove_pkg = $syslog_ng::package ? {
        /^syslog-ng.*/ => "mobi-syslog-ng",
        /^mobi-syslog-ng.*/ => "syslog-ng",
        default => fail("This (${syslog_ng::package}) is an incorrect package name"),
    }

    package { $syslog_ng::install::remove_pkg:
        ensure => absent,
    }

    package { $syslog_ng::package:
        ensure => $::syslog_ng::package_ensure,
        require => Package[$syslog_ng::install::remove_pkg],
    }
}
