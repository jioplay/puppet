# Class: syslog_ng::params
#
# This module manages syslog_ng paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class syslog_ng::params {

  ### install.pp
  $package = "syslog-ng"
  $package_ensure = "present"

  ### service.pp
  $service = "syslog-ng"
  $service_ensure = "running"
  $service_enable = "true"

  ### config.pp
  $manage_config = "true"
  $config = "syslog-ng.conf"
  $sys_config = "syslog-ng.sysconfig"
  $logrotate_config = "syslog.logrotate"

}
