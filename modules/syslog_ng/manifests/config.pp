class syslog_ng::config {

  File {
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => Class["syslog_ng::install"],
  }

  if $syslog_ng::manage_config {
    file { "/etc/syslog-ng/syslog-ng.conf":
      source  => "puppet://$puppetserver/modules/syslog_ng/${::syslog_ng::config}",
    }
  }

  file { "/etc/logrotate.d/syslog":
    source  => "puppet://$puppetserver/modules/syslog_ng/${::syslog_ng::logrotate_config}",
  }

  file { ["/etc/logrotate.d/syslog-ng", "/etc/logrotate.d/.syslog"] :
    ensure => absent,
  }

  file { "/etc/sysconfig/syslog-ng":
    source  => "puppet://$puppetserver/modules/syslog_ng/${::syslog_ng::sys_config}",
  }
}
