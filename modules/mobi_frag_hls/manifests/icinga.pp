class mobi_frag_hls::icinga {

  if $::mobi_frag_hls::icinga {

    if ! $::mobi_frag_hls::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_frag_hls module when icinga = true")
    }

    @@nagios_service { "check_http_${fqdn}":
      host_name             => "$::fqdn",
      check_command         => "check_http! ${::mobi_frag_hls::icinga_cmd_args}",
      service_description   => "check_http",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_frag_hls::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }

  }
}

