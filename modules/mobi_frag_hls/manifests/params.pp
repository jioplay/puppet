# Class: mobi_frag_hls::params
#
# This module manages mobi_frag_hls paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class mobi_frag_hls::params {
    ###icinga.pp
    $icinga = false
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-frag-hls/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    #install.pp
    $hls_playlists_live_package_name = "mobi-hls-playlists-live-managed-v1"
    $hls_playlists_live_package_version = undef
    $hls_live_config_package_name = "mobi-httpd-hls-live-managed-config"
    $hls_live_config_package_version = undef
    $mobi_hls_playlists_live_managed_version = "*"
    $mobi_httpd_hls_live_managed_config_version = "*"
    $output_base = "/mnt/ramdisk"

    #config.pp
    $config = "mobi-httpd-hls-live-managed.conf"
    $enable_rewrite_log = false
    $enable_hls_live_aes_key = false
    $drm_output_base = "/var/www/live_hls"

}
