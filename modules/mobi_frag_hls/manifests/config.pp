class mobi_frag_hls::config {

    file{"/etc/httpd/conf.d/${::mobi_frag_hls::config}":
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        content => template("mobi_frag_hls/${::mobi_frag_hls::config}.erb"),
        notify  => Class["apache::service"],
    }

    if ($::mobi_frag_hls::hls_playlists_live_package_name == "mobi-hls-playlists-live") {
       file{ "/var/www/live_hls":
          force  => true,
          ensure => link,
          target => "/dev/shm/media/AS",
       }
    }
}
