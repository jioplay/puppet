class mobi_frag_hls::install {
  if (! $::mobi_frag_hls::hls_playlists_live_package_version) {
     $hls_playlists_live_pkg_version = $::mobi_frag_hls::mobi_hls_playlists_live_managed_version
  } else {
     $hls_playlists_live_pkg_version = $::mobi_frag_hls::hls_playlists_live_package_version
  }

  if (! $::mobi_frag_hls::hls_live_config_package_version) {
     $hls_live_config_pkg_version = $::mobi_frag_hls::mobi_httpd_hls_live_managed_config_version
  } else {
     $hls_live_config_pkg_version = $::mobi_frag_hls::hls_live_config_package_version
  }

  package { "${::mobi_frag_hls::hls_playlists_live_package_name}-${hls_playlists_live_pkg_version}":
    ensure  => present,
    require => Class["apache::install"],
  }

  package { "${::mobi_frag_hls::hls_live_config_package_name}-${hls_live_config_pkg_version}":
    ensure => present,
    require => Class["apache::install"],
  }
}
