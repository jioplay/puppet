# Class: mobi_frag_hls
#
# This module manages mobi_frag_hls
#
# Parameters:
#
# Actions:
#
# Requires:
#
#  apache
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class mobi_frag_hls (
    ###icinga.pp
    $icinga = $mobi_frag_hls::params::icinga,
    $icinga_instance = $mobi_frag_hls::params::icinga_instance,
    $icinga_cmd_args = $mobi_frag_hls::params::icinga_cmd_args,
    ###end icinga.pp

    $hls_playlists_live_package_name = $::mobi_frag_hls::params::hls_playlists_live_package_name,
    $hls_playlists_live_package_version = $::mobi_frag_hls::params::hls_playlists_live_package_version,
    $hls_live_config_package_name =  $::mobi_frag_hls::params::hls_live_config_package_name,
    $hls_live_config_package_version =  $::mobi_frag_hls::params::hls_live_config_package_version,
    $mobi_hls_playlists_live_managed_version = $::mobi_frag_hls::params::mobi_hls_playlists_live_managed_version,
    $mobi_httpd_hls_live_managed_config_version =  $::mobi_frag_hls::params::mobi_httpd_hls_live_managed_config_version,
    $output_base =  $::mobi_frag_hls::params::output_base,
    $config = $::mobi_frag_hls::params::config,
    $enable_rewrite_log = $::mobi_frag_hls::params::enable_rewrite_log,
    $enable_hls_live_aes_key = $::mobi_frag_hls::params::enable_hls_live_aes_key,
    $drm_output_base = $::mobi_frag_hls::params::drm_output_base,

) inherits mobi_frag_hls::params {
    anchor {"mobi_frag_hls::begin":} ->
    class {"mobi_frag_hls::install":} ->
    class {"mobi_frag_hls::config":} ->
    anchor {"mobi_frag_hls::end":}

    # include module name in motd
    os::motd::register { $name: }
}
