define mobi_frag_hls::playlist (
  $playlist_base = undef,
) {
    if (! $playlist_base) {
        fail ("expecting \$playlist_base")
    }

    file{ "${playlist_base}":
        ensure  => directory,
        owner   => "root",
        group   => "root",
        mode    => "0755",
        recurse => true,
        source  => "puppet:///modules/mobi_frag_hls/${title}",
    }

}
