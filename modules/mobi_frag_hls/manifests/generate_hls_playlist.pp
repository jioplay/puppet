define mobi_frag_hls::generate_hls_playlist(
  $playlist_array = undef,
){

  #you need to specify the $playlist_array to use to generate the playlist  #from the template
  if (! $playlist_array){
    fail("expecting \$playlist_array")
  }

  $array_length = inline_template("<%= title.split('/').length -%>")
  $filename = inline_template("<%= title.split('/')[-1] -%>")
  $directories = inline_template("<%= f = title.split('/').length-1;l = [];for i in 4..f do l.push(title.split('/')[0,i].join('/')) end; l.join(':') -%>")
  $directories_split_array = split($directories,':')

  if ! defined(File[$directories_split_array[0]]){
    file{$directories_split_array:
      ensure => directory,
    }

    file { "${title}":
        owner  => 'apache',
        group  => 'apache',
        mode   => '0755',
        path   => $title,
        ensure => present,
        content  => template("mobi_frag_hls/generate_playlist.erb"),
        require => File["${directories_split_array[-1]}"],
      }
  }
  else{
    file { "${title}":
        owner  => 'apache',
        group  => 'apache',
        mode   => '0755',
        path   => $title,
        ensure => present,
        content  => template("mobi_frag_hls/generate_playlist.erb"),
        require => File["${directories_split_array[-1]}"],
      }
  }
}
