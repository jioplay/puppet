class mobi_session_manager::icinga {

  if $::mobi_session_manager::icinga {

    if ! $::mobi_session_manager::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_session_manager module when icinga = true")
    }

    @@nagios_service { "check_http_${fqdn}":
      host_name             => "$::fqdn",
      check_command         => "check_http! ${::mobi_session_manager::icinga_cmd_args}",
      service_description   => "check_http",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "$::mobi_session_manager::icinga_instance",
    }

  }
}

