class mobi_session_manager::params {
    ###icinga.pp
    $icinga = false
    $icinga_instance = undef
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-session-manager/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $session_manager_ver = "5.3.0-234731"
    $storage_manager_url  = undef
    $identity_manager_url = undef
    $rights_manager_url = undef
    $carrier = "technicolor"
    $product = "mgo"
    $ttl_default = undef
    $ttl_carrier_product = undef
}
