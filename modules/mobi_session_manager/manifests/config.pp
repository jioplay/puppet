class mobi_session_manager::config
{

#    $db_url = $mobi_session_manager::db_url

    file { "/opt/mobi-session-manager/config/applicationProperties.xml":
        ensure => file,
        content => template("mobi_session_manager/applicationProperties.xml.erb"),
        notify => Class["tomcat::service"],
        require => Class["mobi_session_manager::install"],
    }
}