class mobi_session_manager::install {

    package { "mobi-session-manager-${mobi_session_manager::session_manager_ver}" :
        ensure => present,
        notify => Class["tomcat::service"],
    }

}
