# == Class: mobi_session_manager
#
#  installs session management component
#
# === Parameters:
#
#    $session_manager_ver::
#    $storage_manager_url::
#    $identity_manager_url::
#    $rights_manager_url
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_session_manager" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_session_manager (
    ###icinga.pp
    $icinga = $mobi_session_manager::params::icinga,
    $icinga_instance = $mobi_session_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_session_manager::params::icinga_cmd_args,
    ###end icinga.pp
    $session_manager_ver = $mobi_session_manager::params::session_manager_ver ,
    $storage_manager_url = $mobi_session_manager::params::storage_manager_url ,
    $identity_manager_url = $mobi_session_manager::params::identity_manager_url ,
    $rights_manager_url = $mobi_session_manager::rights_manager_url ,
    $rights_manager_url = $mobi_session_manager::rights_manager_url ,
    $carrier = $mobi_session_manager::carrier ,
    $product = $mobi_session_manager::product ,
    $ttl_default = $mobi_session_manager::ttl_default ,
    $ttl_carrier_product = $mobi_session_manager::ttl_carrier_product ,
)
inherits mobi_session_manager::params {
    include mobi_session_manager::install, mobi_session_manager::config
    anchor { "mobi_session_manager::begin": } -> Class["mobi_session_manager::install"]
    Class["mobi_session_manager::config"] -> anchor { "mobi_session_manager::end" :}
    os::motd::register { $name : }
}
