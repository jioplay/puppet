class python::install inherits python {
  package{ $package:
     ensure => $manage_package,
  }
}
