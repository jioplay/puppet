# Class: python::dev
#
# This class installs python development libraries
#
# Parameters:
#
# Actions:
#   - Installs devel and python as a requisite
#
# Requires:
#
# calls and requires the main python class
#
# Sample Usage:
#
# To install python and devel packages:
#
# class { "python::devel": package_devel => "python26-devel" }
#
class python::devel (
  $package_devel = $python::params::python_devel,
  $manage_package_devel = $python::params::manage_package_devel,
) inherits python {

  package { $package_devel:
    ensure => $manage_package_devel,
  }
}
