# Class: python
#
# This class declares the install class
#
# Parameters:
#
# Actions:
#   - Install python
#
# Requires:
#
# Sample Usage:
#
class python (
  $package = $python::params::package,
  $manage_package = $python::params::manage_package,
) inherits python::params {
  include python::install


  # include module name in motd
  os::motd::register { "python": }
}
