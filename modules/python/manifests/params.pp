# Class: python::params
#
# This class handles the python module parameters
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class python::params {

  ### install.pp

  $package = $python_package ? {
    ""      => "python26",
    default => $python_package,
  }

  $manage_package = $python_manage_package ? {
    ""      => "present",
    default => $python_manage_package,
  }

  ### devel.pp

  $package_devel = $python_package_devel ? {
    ""      => "python26-devel",
    default => $python_package_devel,
  }

  $manage_package_devel = $python_manage_package_devel ? {
    ""      => "present",
    default => $python_manage_package_devel,
  }
}
