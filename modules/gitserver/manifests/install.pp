class gitserver::install{
  package{$gitserver::git_package:
    ensure => $gitserver::git_package_ensure,
  }
  #Need perl-Time-HiRes for gitolite setup
  package{ $gitserver::perl_time_hires_package:
    ensure => $gitserver::perl_time_hires_package_ensure,
  }

  package{$gitserver::git_daemon_package:
    ensure => $gitserver::git_daemon_package_ensure,
  } 
}
