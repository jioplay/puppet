class gitserver(
  $gitolite_ssh_key = $gitserver::params::gitolite_ssh_key,
  $gitserver_home_dir = $gitserver::params::gitserver_home_dir,
  $gitolite_base_dir = $gitserver::params::gitolite_base_dir,
  $gitserver_user = $gitserver::params::gitserver_user,
  $gitolite_user = $gitserver::params::gitolite_user,
  $git_package =  $gitserver::params::git_package,
  $git_package_ensure =  $gitserver::params::git_package_ensure,
  $git_daemon_package =  $gitserver::params::git_daemon_package,
  $git_daemon_package_ensure =  $gitserver::params::git_daemon_package_ensure,
  $git_daemon_service =  $gitserver::params::git_daemon_service,
  $git_daemon_service_ensure =  $gitserver::params::git_daemon_service_ensure,
  $git_daemon_service_enable =  $gitserver::params::git_daemon_service_enable,
  $perl_time_hires_package =  $gitserver::params::perl_time_hires_package,
  $perl_time_hires_package_ensure =  $gitserver::params::perl_time_hires_package_ensure,
) inherits gitserver::params{
    anchor {  "gitserver::begin": } ->
    class { "gitserver::install": } ->
    class { "gitserver::config": } ->
    class { "gitserver::service": } ->
    anchor { "gitserver::end": }

}
