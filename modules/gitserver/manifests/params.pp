class gitserver::params{
  $gitserver_user = 'gitolite'
  $gitolite_user = 'gitolite'
  $gitolite_ssh_key = undef
  $gitserver_home_dir = '/home/gitolite'
  $gitolite_base_dir = '/var/lib/'
  $git_package = 'git'
  $git_package_ensure = 'present'
  $git_daemon_package = 'git-daemon'
  $git_daemon_package_ensure = 'present'
  $git_daemon_service = 'git-daemon'
  $git_daemon_service_ensure = running
  $git_daemon_service_enable = true
  $perl_time_hires_package = 'perl-Time-HiRes'
  $perl_time_hires_package_ensure = '1.9721-129.el6'
}
