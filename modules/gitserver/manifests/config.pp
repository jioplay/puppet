class gitserver::config{
  include accounts
  $gitolite_repo_dir = "${gitserver::gitserver_home_dir}/repositories/"
  realize(Accounts::User[$gitserver::gitserver_user])
  realize(Accounts::Group[$gitserver::gitserver_user])
  $real_gitolite_ssh_key = $gitserver::gitolite_ssh_key ? {
    undef   => "${gitserver::gitolite_user}.pub",
    default => $gitserver::gitolite_ssh_key,
  }
  file{"${gitserver::gitserver_home_dir}/${gitserver::gitolite_user}.pub":
    ensure  => present,
    owner   => $gitserver::gitolite_user,
    group   => $gitserver::gitolite_user,
    source  => "puppet:///modules/${module_name}/${real_gitolite_ssh_key}",
    require => Accounts::User[$gitserver::gitserver_user]
  }
  ->
  file{"${gitserver::gitserver_home_dir}/bin":
    owner  => $gitserver::gitolite_user,
    group  => $gitserver::gitolite_user,
    ensure => directory,
  }
  ->
  file{"/tmp/gitolite":
    owner   => $gitserver::gitolite_user,
    group   => $gitserver::gitolite_user,
    recurse => true,
    source  => "puppet:///modules/${module_name}/gitolite",
    ensure  => present,
  }
  ->
  exec{'setup_gitolite':
    command => "/tmp/gitolite/install -to ${gitserver::gitserver_home_dir}/bin/ && ${gitserver::gitserver_home_dir}/bin/gitolite setup -pk ${gitserver::gitserver_home_dir}/${gitserver::gitolite_user}.pub",
    unless => ["/usr/bin/test -d ${gitserver::gitserver_home_dir}/bin/gitolite","/usr/bin/test -d ${gitolite_repo_dir}/gitolite-admin.git"],
    cwd => $gitserver::gitserver_home_dir,
    environment => "HOME=${gitserver::gitserver_home_dir}",
    user => $gitserver::gitserver_user,
  }
  ->
  file{ ["${gitserver::gitserver_home_dir}/.ssh","${gitserver::gitserver_home_dir}/.gitolite","${gitserver::gitserver_home_dir}/repositories"]:
    owner   => $gitserver::gitolite_user,
    group   => $gitserver::gitolite_user,
    recurse => true,
  }

  $gitolite_user  = $gitserver::gitolite_user
  $gitolite_group = $gitserver::gitolite_user
  file{ '/etc/init.d/git-daemon':
    ensure => present,
    mode   => '0755',
    content => template("${module_name}/git-daemon.erb"),
  }
}
