class gitserver::service {
  service { $gitserver::git_daemon_service:
    ensure      => $gitserver::git_daemon_service_ensure,
    enable      => $gitserver::git_daemon_service_enable,
    hasstatus   => true,
    hasrestart  => true,
  }
}
