set serveroutput on
set echo on
set feed on
set serverout on size 1000000

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;
cursor get_obj is
        select TABLE_NAME, CONSTRAINT_NAME 
        from dba_constraints
        where OWNER='RIGHTS'
        and STATUS='DISABLED' and CONSTRAINT_TYPE='R';
BEGIN
        cursor_id:=dbms_sql.open_cursor;
        dbms_output.put_line('enabling fk if any...');

FOR obj_rec in get_obj LOOP
         l_sql := 'ALTER TABLE rights.'||obj_rec.TABLE_NAME||' ENABLE CONSTRAINT '||obj_rec.CONSTRAINT_NAME;
         dbms_output.put_line(l_sql);
         dbms_sql.parse(cursor_id,l_sql,1);
         result := dbms_sql.execute(cursor_id);
END LOOP;

dbms_sql.close_cursor(cursor_id);
END;
/
