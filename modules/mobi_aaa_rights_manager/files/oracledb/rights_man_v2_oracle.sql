/*
Changes:
1. Name PK and assign them to rightsman_index tablespace
2. Create tablespaces rightsman_index_01, rightsman_index
3. Create user rights_user, grant SELECT, INSERT, UPDATE, DELETE on tables in rights schema
4. Create separate db rights_manager
5. Create user rights_user, grant SELECT, INSERT, UPDATE, DELETE on tables in rights_manager
6. Set scale 0 for NUMBER columns, e.g. (*,0)
7. Created referential constraints:
	tbl RIGHTS_CONSTRAINTS: CONSTRAINT "FK_RIGHTS_CONSTRAINTS_01" FOREIGN KEY ("RIGHTS_ID") REFERENCES "RIGHTS"."RIGHTS" ("RIGHTS_ID") ON DELETE CASCADE
	tbl RIGHTS: CONSTRAINT "FK_RIGHTS_01" FOREIGN KEY ("OFFERS_ID") REFERENCES "RIGHTS"."OFFERS" ("OFFERS_ID") ON DELETE CASCADE
8. Added CACHE 20 to sequences (instead of NOCACHE)	
*/

----cleanup objects
/*
DROP USER rights_user CASCADE;
DROP USER rights CASCADE;
DROP ROLE rights_role;
DROP TABLESPACE rightsman_data INCLUDING CONTENTS;
DROP TABLESPACE rightsman_index INCLUDING CONTENTS;
!rm /data1/oradata/prdsub/rightsman_data_01
!rm /data3/oradata/prdsub/rightsman_index_01
*/

----create tablespaces

CREATE TABLESPACE  rightsman_data
LOGGING DATAFILE '/data1/oradata/prdsub/rightsman_data_01' SIZE 100M AUTOEXTEND 
ON NEXT  100M MAXSIZE  5G EXTENT MANAGEMENT LOCAL SEGMENT 
SPACE MANAGEMENT  AUTO;

CREATE TABLESPACE  rightsman_index
LOGGING DATAFILE '/data3/oradata/prdsub/rightsman_index_01' SIZE 100M AUTOEXTEND 
ON NEXT  100M MAXSIZE  5G EXTENT MANAGEMENT LOCAL SEGMENT 
SPACE MANAGEMENT  AUTO;

----create users and roles

CREATE USER rights_manager PROFILE "DEFAULT" 
IDENTIFIED BY rights_manager 
DEFAULT TABLESPACE rightsman_data 
TEMPORARY TABLESPACE "TEMP" 
ACCOUNT UNLOCK;

grant connect to rights_manager;
grant resource to rights_manager;
alter user rights_manager quota unlimited on rightsman_data;
alter user rights_manager quota unlimited on rightsman_index;

create role rights_role;

CREATE USER rights_user  PROFILE "DEFAULT" 
IDENTIFIED BY rights_user  
DEFAULT TABLESPACE rightsman_data 
TEMPORARY TABLESPACE "TEMP" 
ACCOUNT UNLOCK;

grant connect to rights_user;
grant rights_role to rights_user;
alter user rights_user quota unlimited on rightsman_data;
alter user rights_user quota unlimited on rightsman_index;

---- create tables/sequences
/*
DROP TABLE RIGHTS_MANAGER.OFFERS CASCADE CONSTRAINTS;
DROP TABLE RIGHTS_MANAGER.OFFERS_HISTORY CASCADE CONSTRAINTS;
DROP TABLE RIGHTS_MANAGER.RIGHTS CASCADE CONSTRAINTS;
DROP TABLE RIGHTS_MANAGER.RIGHTS_CONSTRAINTS CASCADE CONSTRAINTS;
DROP TABLE RIGHTS_MANAGER.RIGHTS_CONSTRAINTS_HISTORY CASCADE CONSTRAINTS;
DROP TABLE RIGHTS_MANAGER.RIGHTS_HISTORY CASCADE CONSTRAINTS;
DROP sequence RIGHTS_MANAGER.OFFERS_ID_SEQ;
DROP sequence RIGHTS_MANAGER.RIGHTS_CONSTRAINTS_ID_SEQ;
DROP sequence RIGHTS_MANAGER.RIGHTS_ID_SEQ;
*/

CREATE TABLE RIGHTS_MANAGER.OFFERS
  (
    OFFERS_ID           NUMBER (*,0) NOT NULL,
    ACTIVATION_WINDOW   NUMBER (*,0),
    ALLOWED_DOWNLOAD    VARCHAR2(1),
    ALLOWED_STREAMING   VARCHAR2(1),
    AVAILABILITY_WINDOW NUMBER (*,0),
    CREATED_DATE		TIMESTAMP,
    END_DATE			TIMESTAMP,
    MAX_HEIGHT              NUMBER (*,0),
    IS_CATCH_UP_ENABLED VARCHAR2(1),
    IS_HD               VARCHAR2(1),
    IS_OPT_IN           VARCHAR2(1),
    IS_OPT_OUT          VARCHAR2(1),
    IS_SD               VARCHAR2(1),
    MAX_BIT_RATE        NUMBER (*,0),
    OFFER_ID            NUMBER (*,0) NOT NULL,
    OFFER_STATUS        VARCHAR2(10),
    OFFER_TYPE          VARCHAR2(50) NOT NULL,
    OFFER_VERSION       NUMBER (*,0) NOT NULL,
    RENEWAL_PERIOD      NUMBER (*,0),
    SKUS                VARCHAR2(1024),
    START_DATE			TIMESTAMP,
    TRIAL_PERIOD		NUMBER (*,0),
    UPDATE_DATE			TIMESTAMP,
    VIEWING_WINDOW		NUMBER (*,0),
    MAX_WIDTH          		NUMBER (*,0),
    CONSTRAINT "PK_OFFERS" PRIMARY KEY ("OFFERS_ID") 
	USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "RIGHTSMAN_INDEX"  ENABLE
  );

 
/*
Notes:

1. ALLOWED_DOWNLOAD,ALLOWED_STREAMING,IS_CATCH_UP_ENABLED,IS_HD,IS_SD are "BOOLEN" and NOT NULL. Should they have a default value?
2. NUMBER (*,0) column. If data is integer we should have scale 0, e.g. (*,0). Restriction to precission would also be good if there is certainity of NUMBER (*,0) not exceeding it.
3. Limitation to OFFER_STATUS,OFFER_TYPE are set in bytes. If there is only limited amount of types/statuses would it be better to specify lenghth in characters?
*/  

CREATE sequence RIGHTS_MANAGER.OFFERS_ID_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;

/*
1. Is there a particular reason for not chaching sequence values?
*/
  
CREATE  TABLE RIGHTS_MANAGER.OFFERS_HISTORY
  (
    OFFERS_ID           NUMBER (*,0) NOT NULL,
    ACTIVATION_WINDOW   NUMBER (*,0),
    ALLOWED_DOWNLOAD    VARCHAR2(1),
    ALLOWED_STREAMING   VARCHAR2(1),
    AVAILABILITY_WINDOW NUMBER (*,0),
    CREATED_DATE		TIMESTAMP,
    END_DATE			TIMESTAMP,
    MAX_HEIGHT              NUMBER (*,0),
    IS_CATCH_UP_ENABLED VARCHAR2(1),
    IS_HD               VARCHAR2(1),
    IS_OPT_IN           VARCHAR2(1),
    IS_OPT_OUT          VARCHAR2(1),
    IS_SD               VARCHAR2(1),
    MAX_BIT_RATE        NUMBER (*,0),
    OFFER_ID            NUMBER (*,0) NOT NULL,
    OFFER_STATUS        VARCHAR2(10),
    OFFER_TYPE          VARCHAR2(50) NOT NULL,
    OFFER_VERSION       NUMBER (*,0) NOT NULL,
    RENEWAL_PERIOD      NUMBER (*,0),
    SKUS                VARCHAR2(1024),
    START_DATE			TIMESTAMP,
    TRIAL_PERIOD		NUMBER (*,0),
    UPDATE_DATE			TIMESTAMP,
    VIEWING_WINDOW		NUMBER (*,0),
    MAX_WIDTH          		NUMBER (*,0),
    CONSTRAINT "PK_OFFERS_HISTORY" PRIMARY KEY ("OFFERS_ID") 
	USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "RIGHTSMAN_INDEX"
  );


/*
Notes:

1. RIGHTS.OFFERS_HISTORY has the same structure as RIGHTS.OFFERS. Purpose, archival procedure?

*/  
  
CREATE TABLE RIGHTS_MANAGER.RIGHTS
  (
    RIGHTS_ID  			NUMBER (*,0) NOT NULL,
    ACCOUNT_ID 			VARCHAR2(255) NOT NULL,
    CARRIER    			VARCHAR2(20) NOT NULL,
    CREATED_BY 			VARCHAR2(30) NOT NULL,
    CREATED_TIME 		TIMESTAMP NOT NULL,
    END_DATE 			TIMESTAMP,
    LAST_UPDATED_BY 	VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TIME 	TIMESTAMP NOT NULL,
    OFFERS_ID   		NUMBER (*,0) NOT NULL,
    PRODUCT     		VARCHAR2(40) NOT NULL,
    PURCHASE_ID 		NUMBER (*,0) NOT NULL,
    START_DATE 			TIMESTAMP,
    VERSION 			VARCHAR2(10) NOT NULL,
    CONSTRAINT "PK_RIGHTS" PRIMARY KEY (RIGHTS_ID)
	USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "RIGHTSMAN_INDEX"  ENABLE,
  CONSTRAINT "FK_RIGHTS_01" FOREIGN KEY ("OFFERS_ID") REFERENCES "RIGHTS_MANAGER"."OFFERS" ("OFFERS_ID") ENABLE
  );
  

/*
Notes:

1. Referential costraint between OFFERS and RIGHTS: FK (OFFERS_ID)?
2. Create index on OFFERS_ID if there are UPDATES on OFFERS and JOINS with OFFERS
*/  

CREATE sequence RIGHTS_MANAGER.RIGHTS_ID_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;

/*
1. Is there a particular reason for not chaching sequence values?
*/

CREATE  TABLE RIGHTS_MANAGER.RIGHTS_HISTORY
  (
    RIGHTS_ID  			NUMBER (*,0) NOT NULL,
    ACCOUNT_ID 			VARCHAR2(255) NOT NULL,
    CARRIER    			VARCHAR2(20) NOT NULL,
    CREATED_BY 			VARCHAR2(30) NOT NULL,
    CREATED_TIME 		TIMESTAMP NOT NULL,
    END_DATE 			TIMESTAMP,
    LAST_UPDATED_BY 	VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TIME 	TIMESTAMP NOT NULL,
    OFFERS_ID   		NUMBER (*,0) NOT NULL,
    PRODUCT     		VARCHAR2(40) NOT NULL,
    PURCHASE_ID 		NUMBER (*,0) NOT NULL,
    START_DATE 			TIMESTAMP,
    VERSION 			VARCHAR2(10) NOT NULL,
    CONSTRAINT "PK_RIGHTS_HISTORY" PRIMARY KEY (RIGHTS_ID)
	USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "RIGHTSMAN_INDEX"  ENABLE
  );

/*
Notes:

1. RIGHTS.RIGHTS_HISTORY has the same structure as RIGHTS.RIGHTS. Purpose, archival procedure?

*/  
 
CREATE  TABLE RIGHTS_MANAGER.RIGHTS_CONSTRAINTS
  (
    RIGHTS_CONSTRAINTS_ID 		NUMBER (*,0) NOT NULL,
    ASSET_ID              		VARCHAR2(50) NOT NULL,
    CREATED_BY            		VARCHAR2(30) NOT NULL,
    CREATED_TIME 				TIMESTAMP NOT NULL,
    DEVICE_ID       			VARCHAR2(50),
    LAST_UPDATED_BY 			VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TIME 			TIMESTAMP NOT NULL,
    RIGHTS_ID 					NUMBER (*,0) NOT NULL,
    VIEWING_WINDOW_END_TIME 	TIMESTAMP NOT NULL,
    VIEWING_WINDOW_START_TIME 	TIMESTAMP NOT NULL,
    VIEWING_WINDOW_UPDATED_BY 	VARCHAR2(255),
    CONSTRAINT "PK_RIGHTS_CONSTRAINTS" PRIMARY KEY (RIGHTS_CONSTRAINTS_ID)
	USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "RIGHTSMAN_INDEX"  ENABLE,
	CONSTRAINT "FK_RIGHTS_CONSTRAINTS_01" FOREIGN KEY ("RIGHTS_ID") REFERENCES "RIGHTS_MANAGER"."RIGHTS" ("RIGHTS_ID") ENABLE
  );
  
/*
Notes:

1. Referential costraint between RIGHTS and RIGHTS_CONSTRAINTS: FK (RIGHTS_ID)?
2. Create index on RIGHTS_ID if there are UPDATES on OFFERS and JOINS with RIGHTS

*/ 

CREATE sequence RIGHTS_MANAGER.RIGHTS_CONSTRAINTS_ID_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;

/*
1. Is there a particular reason for not chaching sequence values?
*/
 
CREATE  TABLE RIGHTS_MANAGER.RIGHTS_CONSTRAINTS_HISTORY
  (
    RIGHTS_CONSTRAINTS_ID 		NUMBER (*,0) NOT NULL,
    ASSET_ID              		VARCHAR2(50) NOT NULL,
    CREATED_BY            		VARCHAR2(30) NOT NULL,
    CREATED_TIME 				TIMESTAMP NOT NULL,
    DEVICE_ID       			VARCHAR2(50),
    LAST_UPDATED_BY 			VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TIME 			TIMESTAMP NOT NULL,
    RIGHTS_ID 					NUMBER (*,0) NOT NULL,
    VIEWING_WINDOW_END_TIME 	TIMESTAMP NOT NULL,
    VIEWING_WINDOW_START_TIME 	TIMESTAMP NOT NULL,
    VIEWING_WINDOW_UPDATED_BY 	VARCHAR2(255),
    CONSTRAINT "PK_RIGHTS_CONSTRAINTS_HISTORY" PRIMARY KEY (RIGHTS_CONSTRAINTS_ID)
	USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "RIGHTSMAN_INDEX"  ENABLE
  );



/*
Notes:

1. RIGHTS.RIGHTS_CONSTRAINTS_HISTORY has the same structure as RIGHTS.RIGHTS_CONSTRAINTS. Purpose, archival procedure?

*/

-----grant privileges

set serveroutput on
set echo on
set feed on
set serverout on size 1000000

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_tab is
        select table_name, owner
        from all_tables
        where owner = 'RIGHTS_MANAGER';
cursor get_vw is
        select view_name, owner
        from all_views
        where owner = 'RIGHTS_MANAGER';

cursor get_seq is
        select sequence_name, sequence_owner
        from all_sequences
        where sequence_owner = 'RIGHTS_MANAGER';


BEGIN

cursor_id:=dbms_sql.open_cursor;

FOR tab_rec in get_tab LOOP
   
        l_sql := 'grant select, insert, update, delete on ' || tab_rec.owner || '.' || tab_rec.table_name || ' to RIGHTS_ROLE';

        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

FOR vw_rec in get_vw LOOP
        l_sql := 'grant select on ' || vw_rec.owner || '.' || vw_rec.view_name || ' to RIGHTS_ROLE';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        begin
           result := dbms_sql.execute(cursor_id);
        end;

END LOOP;

FOR seq_rec in get_seq LOOP

        l_sql := 'grant select on ' || seq_rec.sequence_owner || '.' || seq_rec.sequence_name || ' to RIGHTS_ROLE';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

---create synonyms

DECLARE
l_sql varchar2(254);
cursor_id integer;
result integer;
 
cursor get_obj is
        select object_name, owner
        from all_objects
        where owner = UPPER('RIGHTS_MANAGER')
        and object_type in
        ('TABLE', 'VIEW', 'CLUSTER', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE');
 
BEGIN
 
cursor_id:=dbms_sql.open_cursor;
 
dbms_output.put_line('Starting create syn...');

FOR obj_rec in get_obj LOOP
         l_sql := 'create synonym ' || 'rights_user' || '.' || obj_rec.object_name || ' for ' || obj_rec.owner || '.' || obj_rec.object_name;
         dbms_output.put_line(l_sql);
         dbms_sql.parse(cursor_id,l_sql,1);
         result := dbms_sql.execute(cursor_id);
END LOOP;
 
dbms_sql.close_cursor(cursor_id);
END;
/

--Enable Primary Key and Reference Key
DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;
cursor get_pri_obj is
        select TABLE_NAME, CONSTRAINT_NAME
        from dba_constraints
        where OWNER='RIGHTS_MANAGER'
        and STATUS='DISABLED' and CONSTRAINT_TYPE='P';

cursor get_ref_obj is
        select TABLE_NAME, CONSTRAINT_NAME
        from dba_constraints
        where OWNER='RIGHTS_MANAGER'
        and STATUS='DISABLED' and CONSTRAINT_TYPE='R';

BEGIN
        cursor_id:=dbms_sql.open_cursor;
        dbms_output.put_line('enabling pk if any...');

FOR obj_rec in get_pri_obj LOOP
         l_sql := 'ALTER TABLE rights_manager.'||obj_rec.TABLE_NAME||' ENABLE CONSTRAINT '||obj_rec.CONSTRAINT_NAME;
         dbms_output.put_line(l_sql);
         dbms_sql.parse(cursor_id,l_sql,1);
         result := dbms_sql.execute(cursor_id);
END LOOP;

        dbms_output.put_line('enabling fk if any...');

FOR obj_rec in get_ref_obj LOOP
         l_sql := 'ALTER TABLE rights_manager.'||obj_rec.TABLE_NAME||' ENABLE CONSTRAINT '||obj_rec.CONSTRAINT_NAME;
         dbms_output.put_line(l_sql);
         dbms_sql.parse(cursor_id,l_sql,1);
         result := dbms_sql.execute(cursor_id);
END LOOP;

dbms_sql.close_cursor(cursor_id);
END;
/

