class mobi_aaa_rights_manager::oracledb::install {
    file { "/var/mobi_aaa_rights_manager":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi_aaa_rights_manager/oracledb":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_aaa_rights_manager/oracledb",
        require => File["/var/mobi_aaa_rights_manager"],
    }

    file { "/var/mobi_aaa_rights_manager/oracledb/rights_schema.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_aaa_rights_manager/oracledb/rights_schema.sh.erb'),
        require => File["/var/mobi_aaa_rights_manager"],
    }

    exec { "exec /var/mobi_aaa_rights_manager/oracledb/rights_schema.sh":
        command => "/bin/su - oracle -c /var/mobi_aaa_rights_manager/oracledb/rights_schema.sh",
        logoutput => "true",
        require => File["/var/mobi_aaa_rights_manager/oracledb/rights_schema.sh"],
    }
}
