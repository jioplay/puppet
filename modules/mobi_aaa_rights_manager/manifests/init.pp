# == class: mobi_aaa_rights_manager
#
#  install rights manager
#
# === Parameters:
#    $rights_manager_version::
#    $sku_rights::
#    $sku_rights_technicolor_mgo::
#    $sku_rights_dt_eons::
#    $available_sku_rights::
#    $available_sku_rights_technicolor_mgo::
#    $available_sku_rights_dt_eons::
#    $end_date_grace_period::
#    $adapter_rights_manager_health_http_connection_timeout::
#    $adapter_rights_manager_health_http_read_timeout::
#
# === Requires:
#
#    java
#    tomcat
#
# === Sample Usage
#
#
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_rights_manager(
    ###icinga.pp
    $icinga = $mobi_aaa_rights_manager::params::icinga,
    $icinga_instance = $mobi_aaa_rights_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_aaa_rights_manager::params::icinga_cmd_args,
    ###end icinga.pp
    ###depricated
    $end_date_grace_period = $mobi_aaa_rights_manager::params::end_date_grace_period,
    ###depricated end
    $rights_manager_version = $mobi_aaa_rights_manager::params::rights_manager_version,
    $package_name = $mobi_aaa_rights_manager::params::package_name,
    $sku_rights = $mobi_aaa_rights_manager::params::sku_rights,
    $sku_rights_technicolor_mgo = $mobi_aaa_rights_manager::params::sku_rights_technicolor_mgo,
    $sku_rights_sprint = $mobi_aaa_rights_manager::params::sku_rights_sprint,
    $sku_rights_dt_eons = $mobi_aaa_rights_manager::params::sku_rights_dt_eons,
    $sku_rights_infotel = $mobi_aaa_rights_manager::params::sku_rights_infotel,
    $available_sku_rights = $mobi_aaa_rights_manager::params::available_sku_rights,
    $available_sku_rights_technicolor_mgo = $mobi_aaa_rights_manager::params::available_sku_rights_technicolor_mgo,
    $available_sku_rights_sprint = $mobi_aaa_rights_manager::params::available_sku_rights_sprint,
    $available_sku_rights_dt_eons = $mobi_aaa_rights_manager::params::available_sku_rights_dt_eons,
    $available_sku_rights_infotel = $mobi_aaa_rights_manager::params::available_sku_rights_infotel,
    $drm_license_period = $mobi_aaa_rights_manager::params::drm_license_period,
    $adapter_rights_manager_health_http_connection_timeout= $mobi_aaa_rights_manager::params::adapter_rights_manager_health_http_connection_timeout,
    $adapter_rights_manager_health_http_read_timeout = $mobi_aaa_rights_manager::params::adapter_rights_manager_health_http_read_timeout,
    $database_datasource_validationquery = $mobi_aaa_rights_manager::params::database_datasource_validationquery,
    $database_url = $mobi_aaa_rights_manager::params::database_url,
    $database_username = $mobi_aaa_rights_manager::params::database_username,
    $database_password = $mobi_aaa_rights_manager::params::database_password,
    $database_driver = $mobi_aaa_rights_manager::params::database_driver,
    $hibernate_dialect = $mobi_aaa_rights_manager::params::hibernate_dialect,
    $hibernate_show_sql = $mobi_aaa_rights_manager::params::hibernate_show_sql,
    $httpconnection_pool_size = $mobi_aaa_rights_manager::params::httpconnection_pool_size,
    $jerseyclient_enable_clientlog = $mobi_aaa_rights_manager::params::jerseyclient_enable_clientlog,
    $jerseyclient_readtimeout_seconds = $mobi_aaa_rights_manager::params::jerseyclient_readtimeout_seconds,
    $jerseyclient_connectiontimeout_seconds = $mobi_aaa_rights_manager::params::jerseyclient_connectiontimeout_seconds,
    $oms_url = $mobi_aaa_rights_manager::params::oms_url,
    $am_url = $mobi_aaa_rights_manager::params::am_url,
    $pmm_url = $mobi_aaa_rights_manager::params::pmm_url,
    $policy_manager_url = $mobi_aaa_rights_manager::params::policy_manager_url,
    $policy_manager_search_byvid_url = $mobi_aaa_rights_manager::params::policy_manager_search_byvid_url,
    $policy_manager_get_byinventory_url = $mobi_aaa_rights_manager::params::policy_manager_get_byinventory_url,
    $policy_manager_get_bypolicyid_url = $mobi_aaa_rights_manager::params::policy_manager_get_bypolicyid_url,
    $recording_manager_url = $mobi_aaa_rights_manager::params::recording_manager_url,
    $solr_server_url = $mobi_aaa_rights_manager::params::solr_server_url,
    $rm_cleanup_job_enabled = $mobi_aaa_rights_manager::params::rm_cleanup_job_enabled,
    $rm_cleanup_job_cron_expression = $mobi_aaa_rights_manager::params::rm_cleanup_job_cron_expression,
    $drm_rights_mobitv_reference = $mobi_aaa_rights_manager::params::drm_rights_mobitv_reference,
    $user_rights_mobitv_reference = $mobi_aaa_rights_manager::params::user_rights_mobitv_reference,
    $sku_rights_mobitv_reference = $mobi_aaa_rights_manager::params::sku_rights_mobitv_reference,
    $available_sku_rights_mobitv_reference = $mobi_aaa_rights_manager::params::available_sku_rights_mobitv_reference,
    $core_service_rights_technicolor_mgo = $mobi_aaa_rights_manager::params::core_service_rights_technicolor_mgo,
    $core_service_rights_dt_eons = $mobi_aaa_rights_manager::params::core_service_rights_dt_eons,
    $core_service_rights_sprint = $mobi_aaa_rights_manager::params::core_service_rights_rights_sprint,
    $core_service_rights_infotel = $mobi_aaa_rights_manager::params::core_service_rights_infotel,
    $core_service_rights_mobitv_reference = $mobi_aaa_rights_manager::params::core_service_rights_mobitv_reference,
    $auth_manager_url = $mobi_aaa_rights_manager::params::auth_manager_url,
    $auth_support_core_service = $mobi_aaa_rights_manager::params::auth_support_core_service,
    $rights_manager_url = $mobi_aaa_rights_manager::params::rights_manager_url,
    $purchase_manager_endpoint_url = $mobi_aaa_rights_manager::params::purchase_manager_endpoint_url,
    $solr_cache_ttl = $mobi_aaa_rights_manager::params::solr_cache_ttl,
    $zookeeper_connect_string = $mobi_aaa_rights_manager::params::zookeeper_connect_string,
    $oms_health_check_enabled = $mobi_aaa_rights_manager::oms_health_check_enabled,
    $pm_health_check_enabled = $mobi_aaa_rights_manager::pm_health_check_enabled,
    $purchase_health_check_enabled = $mobi_aaa_rights_manager::purchase_health_check_enabled,
    $am_health_check_enabled = $mobi_aaa_rights_manager::am_health_check_enabled,
    $auth_health_check_enabled = $mobi_aaa_rights_manager::auth_health_check_enabled,
    $solr_health_check_enabled = $mobi_aaa_rights_manager::solr_health_check_enabled,
    $rights_manager_service_name = $mobi_aaa_rights_manager::params::rights_manager_service_name,
    $session_manager_endpoint_url = $mobi_aaa_rights_manager::params::session_manager_endpoint_url,
    $auth_use_legacy_verify_tokens_endpoint = $mobi_aaa_rights_manager::params::auth_use_legacy_verify_tokens_endpoint,
    $auth_enable_session_manager = $mobi_aaa_rights_manager::params::auth_enable_session_manager,
    $database_datasource_initialsize = $mobi_aaa_rights_manager::params::database_datasource_initialsize,
    $database_datasource_maxactive = $mobi_aaa_rights_manager::params::database_datasource_maxactive,
    $database_datasource_maxidle = $mobi_aaa_rights_manager::params::database_datasource_maxidle,
    $max_executor_threads = $mobi_aaa_rights_manager::params::max_executor_threads,
    $cdl_maxwait_milliseconds = $mobi_aaa_rights_manager::params::cdl_maxwait_milliseconds,
)
inherits mobi_aaa_rights_manager::params {
    include mobi_aaa_rights_manager::install, mobi_aaa_rights_manager::config
    anchor { "mobi_aaa_rights_manager::begin": } -> Class["mobi_aaa_rights_manager::install"]
    class { "mobi_aaa_rights_manager::icinga":} ->
    Class["mobi_aaa_rights_manager::config"] -> anchor { "mobi_aaa_rights_manager::end" :}
    os::motd::register { $name : }
}

