class mobi_aaa_rights_manager::config {

  $database_datasource_validationquery = $mobi_aaa_rights_manager::database_datasource_validationquery
  $database_url = $mobi_aaa_rights_manager::database_url
  $database_username = $mobi_aaa_rights_manager::database_username
  $database_password = $mobi_aaa_rights_manager::database_password
  $database_driver = $mobi_aaa_rights_manager::database_driver
  $hibernate_dialect = $mobi_aaa_rights_manager::hibernate_dialect
  $hibernate_show_sql = $mobi_aaa_rights_manager::hibernate_show_sql
  $jerseyclient_enable_clientlog = $mobi_aaa_rights_manager::jerseyclient_enable_clientlog
  $jerseyclient_readtimeout_seconds = $mobi_aaa_rights_manager::jerseyclient_readtimeout_seconds
  $jerseyclient_connectiontimeout_seconds = $mobi_aaa_rights_manager::jerseyclient_connectiontimeout_seconds
  $httpconnection_pool_size = $mobi_aaa_rights_manager::httpconnection_pool_size
  $oms_url = $mobi_aaa_rights_manager::oms_url
  $am_url = $mobi_aaa_rights_manager::am_url
  $pmm_url = $mobi_aaa_rights_manager::pmm_url
  $policy_manager_url = $mobi_aaa_rights_manager::policy_manager_url
  $policy_manager_search_byvid_url = $mobi_aaa_rights_manager::policy_manager_search_byvid_url
  $policy_manager_get_byinventory_url = $mobi_aaa_rights_manager::policy_manager_get_byinventory_url
  $policy_manager_get_bypolicyid_url = $mobi_aaa_rights_manager::policy_manager_get_bypolicyid_url
  $recording_manager_url = $mobi_aaa_rights_manager::recording_manager_url
  $solr_server_url = $mobi_aaa_rights_manager::solr_server_url
  $rm_cleanup_job_enabled = $mobi_aaa_rights_manager::rm_cleanup_job_enabled
  $rm_cleanup_job_cron_expression = $mobi_aaa_rights_manager::rm_cleanup_job_cron_expression
  $sku_rights = $mobi_aaa_rights_manager::sku_rights
  $sku_rights_technicolor_mgo = $mobi_aaa_rights_manager::sku_rights_technicolor_mgo
  $sku_rights_dt_eons = $mobi_aaa_rights_manager::sku_rights_dt_eons
  $sku_rights_sprint = $mobi_aaa_rights_manager::sku_rights_sprint
  $sku_rights_infotel = $mobi_aaa_rights_manager::sku_rights_infotel
  $available_sku_rights = $mobi_aaa_rights_manager::available_sku_rights
  $available_sku_rights_technicolor_mgo = $mobi_aaa_rights_manager::available_sku_rights_technicolor_mgo
  $available_sku_rights_dt_eons = $mobi_aaa_rights_manager::available_sku_rights_dt_eons
  $available_sku_rights_sprint = $mobi_aaa_rights_manager::available_sku_rights_sprint
  $available_sku_rights_infotel = $mobi_aaa_rights_manager::available_sku_rights_infotel
  $drm_rights = $mobi_aaa_rights_manager::drm_rights
  $drm_rights_platform = $mobi_aaa_rights_manager::drm_rights_platform
  $user_rights = $mobi_aaa_rights_manager::user_rights
  $user_rights_platform = $mobi_aaa_rights_manager::user_rights_platform
  $service_rights_platform = $mobi_aaa_rights_manager::service_rights_platform
  $drm_rights_mobitv_reference = $mobi_aaa_rights_manager::drm_rights_mobitv_reference
  $sku_rights_mobitv_reference = $mobi_aaa_rights_manager::sku_rights_mobitv_reference
  $available_sku_rights_mobitv_reference = $mobi_aaa_rights_manager::available_sku_rights_mobitv_reference
  $drm_license_period = $mobi_aaa_rights_manager::drm_license_period
  $core_service_rights_technicolor_mgo = $mobi_aaa_rights_manager::core_service_rights_technicolor_mgo
  $core_service_rights_dt_eons = $mobi_aaa_rights_manager::core_service_rights_dt_eons
  $core_service_rights_sprint = $mobi_aaa_rights_manager::core_service_rights_rights_sprint
  $core_service_rights_infotel = $mobi_aaa_rights_manager::core_service_rights_infotel
  $core_service_rights_mobitv_reference = $mobi_aaa_rights_manager::core_service_rights_mobitv_reference
  $auth_manager_url = $mobi_aaa_rights_manager::auth_manager_url
  $auth_support_core_service = $mobi_aaa_rights_manager::auth_support_core_service
  $rights_manager_url = $mobi_aaa_rights_manager::rights_manager_url
  $user_rights_mobitv_reference = $mobi_aaa_rights_manager::user_rights_mobitv_reference
  $purchase_manager_endpoint_url = $mobi_aaa_rights_manager::purchase_manager_endpoint_url
  $solr_cache_ttl = $mobi_aaa_rights_manager::solr_cache_ttl
  $zookeeper_connect_string = $mobi_aaa_rights_manager::zookeeper_connect_string
  $oms_health_check_enabled = $mobi_aaa_rights_manager::oms_health_check_enabled
  $pm_health_check_enabled = $mobi_aaa_rights_manager::pm_health_check_enabled
  $purchase_health_check_enabled = $mobi_aaa_rights_manager::purchase_health_check_enabled
  $am_health_check_enabled = $mobi_aaa_rights_manager::am_health_check_enabled
  $auth_health_check_enabled = $mobi_aaa_rights_manager::auth_health_check_enabled
  $solr_health_check_enabled = $mobi_aaa_rights_manager::solr_health_check_enabled
  $rights_manager_service_name = $mobi_aaa_rights_manager::rights_manager_service_name
  $session_manager_endpoint_url = $mobi_aaa_rights_manager::session_manager_endpoint_url
  $auth_use_legacy_verify_tokens_endpoint = $mobi_aaa_rights_manager::auth_use_legacy_verify_tokens_endpoint
  $auth_enable_session_manager = $mobi_aaa_rights_manager::auth_enable_session_manager

  file {
    "/opt/mobi-aaa/config/rights-manager/service-config-override.properties":
      ensure => file,
      content => template('mobi_aaa_rights_manager/service-config-override.properties.erb'),
      owner => "rtv",
      group => "rtv",
      mode => "755",
      notify => Class["tomcat::service"],
      require => Class["mobi_aaa_rights_manager::install"],
  }



  file {
    "/opt/mobi-aaa/config/rights-manager/application-config-override.properties":
      ensure => file,
      content => template('mobi_aaa_rights_manager/application-config-override.properties.erb'),
      owner => "rtv",
      group => "rtv",
      mode => "755",
      notify => Class["tomcat::service"],
      require => Class["mobi_aaa_rights_manager::install"],
  }

}
