class mobi_aaa_rights_manager::install {

  package {
    "${mobi_aaa_rights_manager::package_name}":
      ensure => "${mobi_aaa_rights_manager::rights_manager_version}",
      notify =>[ Class["tomcat::service"], ]
  }

}
