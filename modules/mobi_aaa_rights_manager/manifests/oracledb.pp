# == Class: mobi_aaa_rights_manager::oracledb
#
#  installs platform rights management database
#
# === Parameters:
#
#   $database_root_password::
#   $rights_username::
#   $rights_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_aaa_rights_manager::oracledb" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_rights_manager::oracledb (
  $oracle_system_username = $mobi_aaa_rights_manager::database::params::oracle_system_username,
  $oracle_system_password = $mobi_aaa_rights_manager::database::params::oracle_system_password,
)
  inherits mobi_aaa_rights_manager::oracledb::params {
    include mobi_aaa_rights_manager::oracledb::install
    anchor { "mobi_aaa_rights_manager::oracledb::begin":} -> Class["mobi_aaa_rights_manager::oracledb::install"]
    Class["mobi_aaa_rights_manager::oracledb::install"] -> anchor { "mobi_aaa_rights_manager::oracledb::end": }
    os::motd::register { $name : }
}
