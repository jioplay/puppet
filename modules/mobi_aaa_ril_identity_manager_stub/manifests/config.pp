class mobi_aaa_ril_identity_manager_stub::config {

    file { "/opt/mobi-aaa-ril-identity-manager-stub/webapp/WEB-INF/classes/application-config-default.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_aaa_ril_identity_manager_stub/application-config-default.properties.erb'),
        require => Class["mobi_aaa_ril_identity_manager_stub::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-aaa-ril-identity-manager-stub/config/authentication.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0755",
        content => template('mobi_aaa_ril_identity_manager_stub/authentication.properties.erb'),
        notify   => Class["tomcat::service"],
    }

}
