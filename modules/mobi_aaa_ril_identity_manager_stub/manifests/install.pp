class mobi_aaa_ril_identity_manager_stub::install {
    package { "mobi-aaa-ril-identity-manager-stub-${mobi_aaa_ril_identity_manager_stub::mobi_aaa_ril_identity_manager_stub_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
