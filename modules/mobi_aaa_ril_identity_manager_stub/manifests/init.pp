class mobi_aaa_ril_identity_manager_stub(
   $mobi_aaa_ril_identity_manager_stub_version = $mobi_aaa_ril_identity_manager_stub::params::mobi_aaa_ril_identity_manager_stub_version,
   $authentication_file_path = $mobi_aaa_ril_identity_manager_stub::params::authentication_file_path,
   $test_users = $mobi_aaa_ril_identity_manager_stub::params::test_users,
   $enable_authentication = $mobi_aaa_ril_identity_manager_stub::params::enable_authentication,
)
inherits mobi_aaa_ril_identity_manager_stub::params{
  anchor { "mobi_aaa_ril_identity_manager_stub::begin":} ->
    class{"mobi_aaa_ril_identity_manager_stub::install":} ->
    class{"mobi_aaa_ril_identity_manager_stub::config":} ->
  anchor { "mobi_aaa_ril_identity_manager_stub::end": }

  os::motd::register { $name : }
}
