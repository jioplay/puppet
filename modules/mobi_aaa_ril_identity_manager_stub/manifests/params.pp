class mobi_aaa_ril_identity_manager_stub::params {
    $mobi_aaa_ril_identity_manager_stub_version = "5.0.0-238976.noarch"
    $authentication_file_path = "/opt/mobi-aaa-ril-identity-manager-stub/config/authentication.properties"
    $enable_authentication = "false"
    $test_users = []
}
