class mobi_cassandra::service {

    service { "cassandra":
        enable => true,
        hasrestart => true,
        ensure => running,
        hasstatus => true,
        subscribe => [ Package["java-install"], Class["java::config"] ],
        notify => [ Class["tomcat::service"], ],
    }
}
