# == Class: mobi_cassandra
#
#  Installs cassandra, makes config changes in cassandra.yaml, starts cassandra and
#  runs a script to create Column Families
#
# === Parameters:
#
#    cassandra_version::   version of the cassandra
#    cluster_name::   name of the cluster
#    seeds:: the contact points for the nodes to learn about the ring
#    initial_token:: initial token value to keep the nodes balanced on the ring
#    auto_bootstrap:: true or false
#
# === Requires:
#
#
# === Sample Usage
#
#       class { 'mobi_cassandra':
#           cassandra_version => '1.0.8-191725',
#           cluster_name => 'Test Cluster',
#           seeds => '10.172.65.32,10.172.65.35',
#           initial_token => 0,
#           auto_bootstrap => true,
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cassandra(
    $cassandra_version,
    $cluster_name,
    $seeds,
    $initial_token,
    $auto_bootstrap) {

    if $cassandra_version == undef { fail("Please specify cassandra version")}
    if $cluster_name == undef {fail("Please specify the cluster name for cassandra configuration")}
    if $seeds == undef {fail("Please specify seeds for cassandra configuration")}
    if $initial_token == undef {fail("Please specify initial token value for cassandra configuration")}
    if $auto_bootstrap == undef {fail("Please specify true or false for auto_bootstrap")}

    anchor { begin:} ->
    class {'mobi_cassandra::install': } ->
    class {'mobi_cassandra::config': } ->
    anchor { end: }
    class {"mobi_cassandra::service": }
    os::motd::register { $name :}
}
