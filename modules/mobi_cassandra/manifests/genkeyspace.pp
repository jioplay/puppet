# == Class: mobi_cassandra::genkeyspace
#
#  Runs the script file using cassandra client
#
# === Parameters:
#
#    keySpaceFile::   script file to be run
#
# === Requires:
#
#    cassandra
#
# === Sample Usage
#
#  class { "mobi_cassandra::genkeyspace" :
#           keySpaceFile => 'smdata_keyspace.txt'
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cassandra::genkeyspace($keySpaceFile) {

    File {
        require => Class["mobi_cassandra::install"],
    }

    file {"/var/lib/cassandra/scripts" :
        ensure => directory,
        owner => cassandra,
        group => cassandra,
        mode => 755,
    }
    file {"/var/lib/cassandra/script-logs" :
        ensure => directory,
        owner => cassandra,
        group => cassandra,
        mode => 755,
    }
    file {"KeySpaceFile":
        path    => "/var/lib/cassandra/scripts/${keySpaceFile}",
        replace => true,
        owner   => cassandra,
        group   => cassandra,
        mode    => 755,
        require => File["/var/lib/cassandra/scripts"],
        before  => Exec["Generate Keyspace"],
        source  => "puppet:///modules/mobi_cassandra/${keySpaceFile}",
    }

    exec {"Generate Keyspace":
        path      => "/usr/bin:/usr/local/bin:/bin",
        command   => "sleep 15s; /bin/sh cassandra-cli -host localhost -u storage_manager -pw troy -f /var/lib/cassandra/scripts/${keySpaceFile}",
        onlyif    => "test ! -f /var/lib/cassandra/script-logs/${keySpaceFile}.log",
        logoutput => true,
        require   => Class["mobi_cassandra::service"],
    }

    file {"ResultKeySpaceFile":
        path => "/var/lib/cassandra/script-logs/${keySpaceFile}.log",
        replace => false,
        owner => cassandra,
        group => cassandra,
        mode => 755,
        content => "Completed running ${keySpaceFile}",
        require => Exec["Generate Keyspace"],
        #notify => Class["mobi_cassandra::service"],
    }

    os::motd::register { $name :}
}
