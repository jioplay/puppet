class mobi_cassandra::config {
    file { "/etc/cassandra/conf/cassandra.yaml":
        ensure => present,
        path => "/etc/cassandra/conf/cassandra.yaml",
        owner => "cassandra",
        group => "cassandra",
        content => template("mobi_cassandra/cassandra.yaml.erb"),
        notify => Class["mobi_cassandra::service"],
    }

}
