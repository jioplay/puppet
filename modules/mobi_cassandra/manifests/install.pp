class mobi_cassandra::install {
    package { "mobi-cassandra" :
      ensure  => "${::mobi_cassandra::cassandra_version}",
      notify => Class["mobi_cassandra::service"],
    }
}
