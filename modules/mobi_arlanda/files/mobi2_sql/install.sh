#!/bin/bash
# Seed the Mobi2 database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2


# (2) Create quartz objects for Ingestor
export ORACLE_SID=MOBI2
sqlplus -s / as sysdba <<EOF > 1_alter_mobi2.sql

set echo off
set feed off
set pages 0

REM Add the Main-Profile Encoding Column (see ARN-1409)
ALTER TABLE "MOBI2"."PROVIDERS" add (MAIN_PROFILE_PGDL_ENCODING varchar2(40) default 'FALSE');

commit;

quit
EOF


# (3) insert hostname for Jukebox
export ORACLE_SID=MOBI2
sqlplus -s mobi2/mobi2  <<EOF > 2_install_mobi2.sql

set echo off
set feed off
set pages 0

Insert into SERVICE_TYPE (TYPE) values ('buyengine');
Insert into SERVICE_TYPE (TYPE) values ('carriermodule');
Insert into SERVICE_TYPE (TYPE) values ('clipfarm');
Insert into SERVICE_TYPE (TYPE) values ('database');
Insert into SERVICE_TYPE (TYPE) values ('dbgateway');
Insert into SERVICE_TYPE (TYPE) values ('encoder');
Insert into SERVICE_TYPE (TYPE) values ('gcgserver');
Insert into SERVICE_TYPE (TYPE) values ('guideserver');
Insert into SERVICE_TYPE (TYPE) values ('interactionserver');
Insert into SERVICE_TYPE (TYPE) values ('jukebox_chunk');
Insert into SERVICE_TYPE (TYPE) values ('jukebox_ingest');
Insert into SERVICE_TYPE (TYPE) values ('jukebox_xcode');
Insert into SERVICE_TYPE (TYPE) values ('logger');
Insert into SERVICE_TYPE (TYPE) values ('mmi');
Insert into SERVICE_TYPE (TYPE) values ('mobiserver');
Insert into SERVICE_TYPE (TYPE) values ('purchasemanager');
Insert into SERVICE_TYPE (TYPE) values ('rtspserver');
Insert into SERVICE_TYPE (TYPE) values ('urlsigner');
Insert into SERVICE_TYPE (TYPE) values ('datawarehouse');
Insert into SERVICE_TYPE (TYPE) values ('radiometadataserver');
Insert into SERVICE_TYPE (TYPE) values ('memcached');
Insert into SERVICE_TYPE (TYPE) values ('ratingsserver');
Insert into SERVICE_TYPE (TYPE) values ('searchserver');
Insert into SERVICE_TYPE (TYPE) values ('mediasegmentserver');


insert into host (host_id, hostname) values (host_ctr.nextval, 'lab502g.oak1.mobitv');
insert into host (host_id, hostname) values (host_ctr.nextval, 'lab502i.oak1.mobitv');
insert into host (host_id, hostname) values (host_ctr.nextval, 'lab502k.oak1.mobitv');

insert into service (service_id, type, label, host_id) values (service_ctr.nextval, 'jukebox_xcode', 'jtra01-xcode', (select host_id from host where hostname = 'lab502g.oak1.mobitv'));
insert into service (service_id, type, label, host_id) values (service_ctr.nextval, 'jukebox_xcode', 'jtra02-xcode', (select host_id from host where hostname = 'lab502i.oak1.mobitv'));
insert into service (service_id, type, label, host_id) values (service_ctr.nextval, 'jukebox_xcode', 'jtra03-xcode', (select host_id from host where hostname = 'lab502k.oak1.mobitv'));


REM INSERTING into CONFIGURATION
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('version','3.9.0',to_timestamp('22-AUG-08 09.05.43.200000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'3.0.0');
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('JUKE_ADFOLDER','advertising/',to_timestamp('25-APR-06 07.38.30.900000000 AM','DD-MON-RR HH.MI.SS.FF AM'),'advertising/');
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('RTSP_ROOT','rtsp://dssvod.mobitv.com:554/c3Rvc/jbvod/',to_timestamp('10-MAR-10 12.10.56.100000000 AM','DD-MON-RR HH.MI.SS.FF AM'),'rtsp://qtvevdo.mobitv.com:554/vod/');
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('MCD_ROOT','http://dssvod.mobitv.com/jbvod/',to_timestamp('02-OCT-09 07.09.52.200000000 AM','DD-MON-RR HH.MI.SS.FF AM'),'http://qtvevdo.mobitv.com/vod/');
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('MCD_TITLE_LEN','32',to_timestamp('25-APR-06 07.38.30.900000000 AM','DD-MON-RR HH.MI.SS.FF AM'),'32');
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('MCD_DESCRIPTION_LEN','255',to_timestamp('06-APR-07 05.52.50.800000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'300');
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('XCODE_AUDIO_SUPER','-vn -acodec aac -ar 48000 -ac 1 -ab 48 -f mp4',to_timestamp('09-JUN-06 09.23.28.800000000 PM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('smtp_host','mailhost.res.mobitv',to_timestamp('14-MAR-07 10.55.00.300000000 PM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('WMS_INGEST_USER_PASS','pwce:pwcertv12',to_timestamp('27-JUL-07 01.20.51.300000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('MULTICAST_PB_STARTING_PORT','20000',to_timestamp('21-FEB-08 10.04.21.900000000 PM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('RTSP_ROOT_LIVE_LOW','rtsp://dssuslo.mobitv.com:554/c3Rvc/',to_timestamp('10-MAR-10 12.10.56.100000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('RTSP_PREFIX','/var/Jukebox/pbsdp/',to_timestamp('29-JUN-07 12.45.41.700000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('WMS_ROOT','mmst://wssushi.mobitv.com/JBVoD/encoding-000/',to_timestamp('11-FEB-10 02.03.55.900000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('RTSP_ROOT_LIVE_HIGH','rtsp://dssushi.mobitv.com:554/c3Rvc/',to_timestamp('10-MAR-10 12.10.56.100000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('YAHOO_AD_PROVIDER_ID','2671',to_timestamp('15-APR-08 08.46.31.100000000 PM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('DAM_WSDL_URL','http://juarnvip:8080/dam/ws?wsdl',to_timestamp('28-AUG-08 02.37.17.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('MP_WSDL_URL','http://juarnvip:8080/mp/ws?wsdl',to_timestamp('28-AUG-08 02.37.17.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('WFM_WSDL_URL','http://juarnvip:8080/wfm/ws?wsdl',to_timestamp('28-AUG-08 02.37.17.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('SCHEDULER_WSDL_URL','http://juschvip:8080/scheduler/ws?wsdl',to_timestamp('28-AUG-08 02.37.18.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('FOLDER_MANAGER_URL','http://juarnvip:8080/foldermanager/',null, EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('LICENSEMANAGER_WSDL_URL','http://judrmvip:8080/licensemanager/ws?wsdl',null, EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('SMTP_HOST','mailhost',to_timestamp('14-JUN-07 04.59.41.700000000 PM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('MULTICAST_PB_ADDRESS','232.0.1.2',to_timestamp('12-APR-10 05.02.41.500000000 PM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('WMS_INGEST_URL','ftp://engine-ftp.farm.res.mobitv/pctv/',to_timestamp('27-JUL-07 01.19.34.900000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('WINMO_INGEST_URL','ftp://engine-ftp.farm.res.mobitv/winmo/',to_timestamp('27-JUL-07 01.20.25.500000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('YAHOO_AD_PERCENTAGE','25',to_timestamp('15-APR-08 08.45.40.200000000 PM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('YAHOO_AD_URL','http://mobitv.smap.mobile.yahoo.com/p/smap',to_timestamp('02-MAY-08 05.36.54.200000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('HTTP_ROOT','http://m2g.mobitv.com/',to_timestamp('11-NOV-09 08.16.48.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('INGESTOR_WSDL_URL','http://juingvip:8080/ingestor/ws?wsdl',to_timestamp('02-JUN-09 04.35.51.500000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());
Insert into CONFIGURATION (NAME,VALUE,LASTMOD,DEFAULT_VALUE) values ('TRANSCODING_CLUSTERS','media,simulator',to_timestamp('11-AUG-11 04.01.16.700000000 AM','DD-MON-RR HH.MI.SS.FF AM'), EMPTY_CLOB());


REM INSERTING into JB_EMAIL_TYPE
Insert into JB_EMAIL_TYPE (TYPE) values ('xcode_complete');
Insert into JB_EMAIL_TYPE (TYPE) values ('xcode_fail');
Insert into JB_EMAIL_TYPE (TYPE) values ('indirect_upload_fail');


REM Add Provider Admin Account 'mobi'
Update providers set password = 'd2c75a26973a1888f241125717b166cf' where provider = 'mobi';

REM FUNCTIONAL_TESTING - creates ci_test, inserting 1 record into PROVIDERS, 1 record into CHANNEL_AD_PARAMS, 2 records into PLAYLIST, 1 record into PROVIDER_METADATA_CONFIG and 2 records into UPLOAD_CONFIG  
REM Create Provider account for use with Continuous Integration Testing
Insert into PROVIDERS (PROVIDER_ID,PROVIDER,PASSWORD,EMAIL,TYPE,DESIRED_ENCODING,TITLE,IS_HIGH_PRIORITY,WMS_ENCODING,TIME_ZONE,WINMO_ENCODING,DFLT_CONTENT_SHELF_LIFE,AUTHORIZATION_TYPE,AUTHORIZER_EMAIL,MANAGE_PRE_ROLL_POST_ROLL,ADVERTISING_SOURCE,PROGRESSIVE_DOWNLOAD_ENCODING,MANAGE_CHAPTERIZED_CONTENT,DRM_ENABLED,SEGMENTER_ENABLED,PGDL_RESTRICTED,FRAGMENTED_MP4_ENABLED,TRANSCODING_CLUSTER,SECURED_OVER_WIFI,REQUIRES_XML_METADATA) values (2,'ci_test','5f4dcc3b5aa765d61d8327deb882cf99','contentalerts@mobitv.com','content','3gpp', EMPTY_CLOB(),'0','FALSE','GMT','FALSE',0,0,null,null,null,'TRUE','TRUE','TRUE','FALSE','FALSE','FALSE','simulator','FALSE','FALSE');

INSERT INTO channel_ad_params (provider_id) VALUES (2);

REM INSERTING into PLAYLIST
Insert into PLAYLIST (PROVIDER_ID,ORDER_CLAUSE,WHERE_CLAUSE,TYPE) values (2,'slot, crid','status=0','ads');
Insert into PLAYLIST (PROVIDER_ID,ORDER_CLAUSE,WHERE_CLAUSE,TYPE) values (2,'slot, crid','status=0 and (expires is null or expires > current_timestamp) and (start_of_availability is null or start_of_availability < current_timestamp)','content');

REM INSERTING into PROVIDER_METADATA_CONFIG
Insert into PROVIDER_METADATA_CONFIG (PROVIDER_ID,TITLE,SHORT_TITLE,SYNOPSIS,KEYWORD,GENRE,EXPIRES,SLOT,START_OF_AVAILABILITY,FOLDER,INDIRECT_UPLOAD_URL,CHECKSUM,THUMBNAIL_UPLOAD_URL,INDUSTRY,TICKER,CRID,ENABLE_LOCAL_PLAYBACK,ENABLE_WIFI) values (2,'TITLE','SHORTTITLE','SYNOPSIS','KEYWORD','GENRE','ENDOFAVAILABILITY','SLOT','STARTOFAVAILABILITY','FOLDER','INDIRECTUPLOADURL','CHECKSUM',null,null,null,null,'ENABLELOCALPLAYBACK','ENABLEWIFI');

REM INSERTING into UPLOAD_CONFIG
Insert into UPLOAD_CONFIG (CONFIG_ID,PROVIDER_ID,UPLOAD_TYPE,USERNAME,PASSWORD,URL_REGEX) values (upload_config_ctr.nextval,2,'Direct', EMPTY_CLOB(), EMPTY_CLOB(), EMPTY_CLOB());
Insert into UPLOAD_CONFIG (CONFIG_ID,PROVIDER_ID,UPLOAD_TYPE,USERNAME,PASSWORD,URL_REGEX) values (upload_config_ctr.nextval,2,'MobiFTP', EMPTY_CLOB(), EMPTY_CLOB(), EMPTY_CLOB());


commit;

quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


