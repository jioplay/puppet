#!/bin/bash
# Install the Ingestor database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) Create users and tablespace for Ingestor
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 2_cr_ingestor_users.sql

set echo off
set feed off
set pages 0

def sid = 'prdjb';


CREATE TABLESPACE  ing_data
    LOGGING 
    DATAFILE '/data2/oradata/&sid/ing_data01.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

CREATE TABLESPACE  ing_index
    LOGGING 
    DATAFILE '/data2/oradata/&sid/ing_index1.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;
--------------------------------------------------------------------------------------------------------

-- ing ingema
-- the ing ingema will own all the "ing" objects

CREATE USER ing  PROFILE "DEFAULT" 
    IDENTIFIED BY ing 
    DEFAULT TABLESPACE ing_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to ing;
grant resource to ing;
alter user ing quota unlimited on ing_data;
alter user ing quota unlimited on ing_index;
--------------------------------------------------------------------------------------------------------

-- ing role
-- grant object privs to this role
create role ing_role;

--------------------------------------------------------------------------------------------------------
-- ing_user user
-- the ing_user will be used by the ing application to connect to the database
-- the ing_user will only own synonyms to the ing ingema objects.

CREATE USER ing_user  PROFILE "DEFAULT" 
    IDENTIFIED BY ing_user 
    DEFAULT TABLESPACE ing_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to ing_user;
grant ing_role to ing_user;
alter user ing_user quota unlimited on ing_data;
alter user ing_user quota unlimited on ing_index;

quit
EOF

# (3) Create schema objects for Ingestor
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 3_cr_ingestor_schema_objects.sql

set echo off
set feed off
set pages 0


DROP TABLE ing.ING_RSS_FEEDS CASCADE CONSTRAINTS;

--
-- General comments for all the tables
-- Most of the tables have a VERSION column which is used for optimistic locking, i.e. to check
-- if the row has been updated by someone else
-- Also, most of the tables have audit columns like created by, created time, last updated by
-- and last updated time.
--

--
-- ing.ING_RSS_FEEDS : This table stores RSS feed configuration for providers
--
CREATE TABLE ing.ING_RSS_FEEDS (
  ID number(19,0) not null,
  URL varchar2(2048 char),
  PROVIDER_ID number(19,0),
  CRON varchar2(255 char),
  DATE_STRING varchar2(255 char),
  NAME varchar2(255 char),
  FEED_TYPE varchar2(40 char)
);

ALTER TABLE ing.ING_RSS_FEEDS
ADD CONSTRAINT pk_ing_rss_feeds
PRIMARY KEY (id)
using index tablespace ing_index;

ALTER TABLE ing.ING_RSS_FEEDS ADD CONSTRAINT UNQ_PROVIDER_ID
UNIQUE(PROVIDER_ID);



--
-- SEQUENCES 
--

-- 
-- Dropping all the sequences
--
DROP SEQUENCE ing.ING_RSS_FEEDS_SEQ;

--
-- Create the sequences with the starting value of 1000 to accomodate any data migration
--

CREATE SEQUENCE ing.ING_RSS_FEEDS_SEQ
  START WITH 1000
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;

COMMIT;

quit
EOF




# (4) Create quartz objects for Ingestor
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 4_cr_ingestor_quartz_objects.sql

set echo off
set feed off
set pages 0


delete from  ing.ing_qrtz_job_listeners;
delete from  ing.ing_qrtz_trigger_listeners;
delete from  ing.ing_qrtz_fired_triggers;
delete from  ing.ing_qrtz_simple_triggers;
delete from  ing.ing_qrtz_cron_triggers;
delete from  ing.ing_qrtz_blob_triggers;
delete from  ing.ing_qrtz_triggers;
delete from  ing.ing_qrtz_job_details;
delete from  ing.ing_qrtz_calendars;
delete from  ing.ing_qrtz_paused_trigger_grps;
delete from  ing.ing_qrtz_locks;
delete from  ing.ing_qrtz_scheduler_state;

drop table  ing.ing_qrtz_calendars;
drop table  ing.ing_qrtz_fired_triggers;
drop table  ing.ing_qrtz_trigger_listeners;
drop table  ing.ing_qrtz_blob_triggers;
drop table  ing.ing_qrtz_cron_triggers;
drop table  ing.ing_qrtz_simple_triggers;
drop table  ing.ing_qrtz_triggers;
drop table  ing.ing_qrtz_job_listeners;
drop table  ing.ing_qrtz_job_details;
drop table  ing.ing_qrtz_paused_trigger_grps;
drop table  ing.ing_qrtz_locks;
drop table  ing.ing_qrtz_scheduler_state;


CREATE TABLE  ing.ing_qrtz_job_details
  (
    JOB_NAME  VARCHAR2(80) NOT NULL,
    JOB_GROUP VARCHAR2(80) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    JOB_CLASS_NAME   VARCHAR2(128) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    IS_STATEFUL VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP)
);
CREATE TABLE  ing.ing_qrtz_job_listeners
  (
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    JOB_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
    REFERENCES  ing.ing_qrtz_JOB_DETAILS(JOB_NAME,JOB_GROUP)
);
CREATE TABLE  ing.ing_qrtz_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(80) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
    REFERENCES  ing.ing_qrtz_JOB_DETAILS(JOB_NAME,JOB_GROUP) 
);
CREATE TABLE  ing.ing_qrtz_simple_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(7) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES  ing.ing_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE  ing.ing_qrtz_cron_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    CRON_EXPRESSION VARCHAR2(80) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES  ing.ing_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE  ing.ing_qrtz_blob_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
        REFERENCES  ing.ing_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE  ing.ing_qrtz_trigger_listeners
  (
    TRIGGER_NAME  VARCHAR2(80) NOT NULL, 
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    TRIGGER_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES  ing.ing_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE  ing.ing_qrtz_calendars
  (
    CALENDAR_NAME  VARCHAR2(80) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (CALENDAR_NAME)
);
CREATE TABLE  ing.ing_qrtz_paused_trigger_grps
  (
    TRIGGER_GROUP  VARCHAR2(80) NOT NULL, 
    PRIMARY KEY (TRIGGER_GROUP)
);
CREATE TABLE  ing.ing_qrtz_fired_triggers 
  (
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(80) NULL,
    JOB_GROUP VARCHAR2(80) NULL,
    IS_STATEFUL VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    PRIMARY KEY (ENTRY_ID)
);
CREATE TABLE  ing.ing_qrtz_scheduler_state 
  (
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    PRIMARY KEY (INSTANCE_NAME)
);
CREATE TABLE  ing.ing_qrtz_locks
  (
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    PRIMARY KEY (LOCK_NAME)
);
INSERT INTO  ing.ing_qrtz_locks values('TRIGGER_ACCESS');
INSERT INTO  ing.ing_qrtz_locks values('JOB_ACCESS');
INSERT INTO  ing.ing_qrtz_locks values('CALENDAR_ACCESS');
INSERT INTO  ing.ing_qrtz_locks values('STATE_ACCESS');
INSERT INTO  ing.ing_qrtz_locks values('MISFIRE_ACCESS');
create index ing.idx_qrtz_j_req_recovery on  ing.ing_qrtz_job_details(REQUESTS_RECOVERY);
create index ing.idx_qrtz_t_next_fire_time on  ing.ing_qrtz_triggers(NEXT_FIRE_TIME);
create index ing.idx_qrtz_t_state on  ing.ing_qrtz_triggers(TRIGGER_STATE);
create index ing.idx_qrtz_t_nft_st on  ing.ing_qrtz_triggers(NEXT_FIRE_TIME,TRIGGER_STATE);
create index ing.idx_qrtz_t_volatile on  ing.ing_qrtz_triggers(IS_VOLATILE);
create index ing.idx_qrtz_ft_trig_name on  ing.ing_qrtz_fired_triggers(TRIGGER_NAME);
create index ing.idx_qrtz_ft_trig_group on  ing.ing_qrtz_fired_triggers(TRIGGER_GROUP);
create index ing.idx_qrtz_ft_trig_nm_gp on  ing.ing_qrtz_fired_triggers(TRIGGER_NAME,TRIGGER_GROUP);
create index ing.idx_qrtz_ft_trig_volatile on  ing.ing_qrtz_fired_triggers(IS_VOLATILE);
create index ing.idx_qrtz_ft_trig_inst_name on  ing.ing_qrtz_fired_triggers(INSTANCE_NAME);
create index ing.idx_qrtz_ft_job_name on  ing.ing_qrtz_fired_triggers(JOB_NAME);
create index ing.idx_qrtz_ft_job_group on  ing.ing_qrtz_fired_triggers(JOB_GROUP);
create index ing.idx_qrtz_ft_job_stateful on  ing.ing_qrtz_fired_triggers(IS_STATEFUL);
create index ing.idx_qrtz_ft_job_req_recovery on  ing.ing_qrtz_fired_triggers(REQUESTS_RECOVERY);

quit
EOF




# (5) Grant privileges to Ingestor user
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 5_grant.sql

set echo off
set feed off
set pages 0

def param_grantee = 'ing_user';
def param_owner = 'ing';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_tab is
        select table_name, owner
        from all_tables
        where owner = UPPER('&param_owner');
cursor get_vw is
        select view_name, owner
        from all_views
        where owner = UPPER('&param_owner');

cursor get_seq is
        select sequence_name, sequence_owner
        from all_sequences
        where sequence_owner = UPPER('&param_owner');


BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

FOR tab_rec in get_tab LOOP

        l_sql := 'grant select, insert, update, delete on ' || tab_rec.owner || '.' || tab_rec.table_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

FOR vw_rec in get_vw LOOP

        l_sql := 'grant select on ' || vw_rec.owner || '.' || vw_rec.view_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        begin
           result := dbms_sql.execute(cursor_id);
        end;

END LOOP;

FOR seq_rec in get_seq LOOP

        l_sql := 'grant select on ' || seq_rec.sequence_owner || '.' || seq_rec.sequence_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/


quit
EOF



# (6) Drop Ingestor Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 6_drop_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'ing_user';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select synonym_name,owner
        from dba_synonyms
        where owner = UPPER('&param_user');

BEGIN

cursor_id:=dbms_sql.open_cursor;

dbms_output.put_line('Starting drop syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'drop synonym ' || obj_rec.owner || '.' || obj_rec.synonym_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (7) Create Ingestor Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 7_cr_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'ing_user';
def param_schema = 'ing';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select object_name, owner
        from all_objects
        where owner = UPPER('&param_schema')
        and object_type in
        ('TABLE', 'VIEW', 'CLUSTER', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE')
        ;

BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

dbms_output.put_line('Starting create syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'create synonym ' || '&param_user' || '.' || obj_rec.object_name || ' for ' || obj_rec.owner || '.' || obj_rec.object_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (8) Seed the Ingestor database with configuration data.
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 8_seed_ingestor.sql

set echo off
set feed off
set pages 0


quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


