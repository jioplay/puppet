#!/bin/bash
# Install the Scheduler database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) Create users and tablespace for Scheduler
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 2_cr_scheduler_users.sql

set echo off
set feed off
set pages 0

def sid = 'prdjb';

CREATE TABLESPACE  sch_data
    LOGGING 
    DATAFILE '/data2/oradata/&sid/sch_data01.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

CREATE TABLESPACE  sch_index
    LOGGING 
    DATAFILE '/data2/oradata/&sid/sch_index1.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;
--------------------------------------------------------------------------------------------------------

-- sch schema
-- the sch schema will own all the "sch" objects

CREATE USER sch  PROFILE "DEFAULT" 
    IDENTIFIED BY sch 
    DEFAULT TABLESPACE sch_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to sch;
grant resource to sch;
alter user sch quota unlimited on sch_data;
alter user sch quota unlimited on sch_index;
--------------------------------------------------------------------------------------------------------

-- sch role
-- grant object privs to this role
create role sch_role;

--------------------------------------------------------------------------------------------------------
-- sch_user user
-- the sch_user will be used by the sch application to connect to the database
-- the sch_user will only own synonyms to the sch schema objects.

CREATE USER sch_user  PROFILE "DEFAULT" 
    IDENTIFIED BY sch_user 
    DEFAULT TABLESPACE sch_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to sch_user;
grant sch_role to sch_user;
alter user sch_user quota unlimited on sch_data;
alter user sch_user quota unlimited on sch_index;

quit
EOF

# (3) Create schema objects for Scheduler
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 3_cr_scheduler_schema_objects.sql

set echo off
set feed off
set pages 0


DROP TABLE sch.SCH_CHANNEL CASCADE CONSTRAINTS;
DROP TABLE sch.SCH_LOCK CASCADE CONSTRAINTS;
DROP TABLE sch.SCH_NODE CASCADE CONSTRAINTS;
DROP TABLE sch.SCH_PLAYLIST CASCADE CONSTRAINTS;
DROP TABLE sch.SCH_PLAYLIST_CLIP CASCADE CONSTRAINTS;
DROP TABLE sch.SCH_CHANNEL_SCHEDULE CASCADE CONSTRAINTS;
DROP TABLE sch.SCH_PLAYLIST_RULE CASCADE CONSTRAINTS;
DROP TABLE sch.SCH_NODE_CHANNEL CASCADE CONSTRAINTS;

--
-- General comments for all the tables
-- Most of the tables have a VERSION column which is used for optimistic locking, i.e. to check
-- if the row has been updated by someone else
-- Also, most of the tables have audit columns like created by, created time, last updated by
-- and last updated time.
--

--
-- sch.SCH_CHANNEL : This table stores all the channels in the system
--
CREATE TABLE sch.SCH_CHANNEL ( 
  ID NUMBER(19) NOT NULL, 
  VERSION NUMBER(10), 
  NAME VARCHAR2(255 CHAR) NOT NULL,
  CHANNEL_AD_INTERVAL NUMBER(19),
  CREATED_BY VARCHAR2(255 CHAR), 
  CREATED_TIME TIMESTAMP(6), 
  LAST_UPDATED_BY VARCHAR2(255 CHAR), 
  LAST_UPDATED_TIME TIMESTAMP(6), 
  PROVIDER_ID NUMBER(19) NOT NULL,
  ENABLED NUMBER(1),
  CURRENT_PLAYLIST_ID NUMBER(19),
  AUDIO_ONLY NUMBER DEFAULT 0,
  BACKEND_CHANNEL VARCHAR2(120 BYTE) DEFAULT '0',
  ENCODING VARCHAR2(255 CHAR)
);

alter table sch.sch_channel
add constraint pk_sch_channel
primary key (id)
using index tablespace sch_index;

ALTER TABLE sch.SCH_CHANNEL ADD CONSTRAINT UNQ_CHANNEL_NAME
UNIQUE(NAME);


--
-- sch.SCH_PLAYLIST  : This table stores all the explicit playlists defined by the provider
--
CREATE TABLE sch.SCH_PLAYLIST ( 
    ID NUMBER(19) NOT NULL, 
    VERSION NUMBER(10), 
    DESCRIPTION VARCHAR2(2000 CHAR), 
    DISPLAY_NAME VARCHAR2(255 CHAR), 
    NAME VARCHAR2(255 CHAR) NOT NULL, 
    LOCKED_TYPE NUMBER(10), -- Determines if the playlist can be interrupted in between
    RECURRENCE_TYPE NUMBER(10), -- Determines if the playlist is looped or play once
    PLAYLIST_TYPE NUMBER(10),
    CREATED_BY VARCHAR2(255 CHAR), 
    CREATED_TIME TIMESTAMP(6), 
    LAST_UPDATED_BY VARCHAR2(255 CHAR), 
    LAST_UPDATED_TIME TIMESTAMP(6), 
    PROVIDER_ID NUMBER(19) NOT NULL,
    INVENTORY_ID NUMBER
);

alter table sch.sch_playlist
add constraint pk_sch_playlist
primary key (id)
using index tablespace sch_index;

ALTER TABLE sch.SCH_PLAYLIST ADD CONSTRAINT UNQ_PLAYLIST_NAME_PROV_ID
UNIQUE(NAME, PROVIDER_ID);


--
-- sch.SCH_PLAYLIST_CLIP : This table stores the clips for a given playlist
--
CREATE TABLE sch.SCH_PLAYLIST_CLIP ( 
    ID NUMBER(19) NOT NULL, 
    INVENTORY_ID NUMBER(19) NOT NULL, 
    PLAYLIST_ID NUMBER(19) NOT NULL 
);

alter table sch.sch_playlist_clip
add constraint pk_sch_playlist_clip
primary key (id)
using index tablespace sch_index;


ALTER TABLE sch.SCH_PLAYLIST_CLIP ADD ( 
    CONSTRAINT FK_SCH_PLAYLIST_CLIP_01 FOREIGN KEY (PLAYLIST_ID) REFERENCES sch.SCH_PLAYLIST (ID)
);

create index sch.idx_SCH_PLAYLIST_CLIP_01 on sch.SCH_PLAYLIST_CLIP(PLAYLIST_ID)
tablespace sch_index;

--
-- sch.SCH_PLAYLIST_RULE : This table stores all the rules associated with a playlist
--
CREATE TABLE sch.SCH_PLAYLIST_RULE ( 
    ID NUMBER(19) NOT NULL, 
    RULE_ARGS VARCHAR2(100 CHAR), 
    RULE_CLASS_NAME VARCHAR2(100 CHAR) NOT NULL, 
    PLAYLIST_ID NUMBER(19) NOT NULL 
);

alter table sch.sch_playlist_rule
add constraint pk_sch_playlist_rule
primary key (id)
using index tablespace sch_index;


ALTER TABLE sch.SCH_PLAYLIST_RULE ADD ( 
    CONSTRAINT FK_SCH_PLAYLIST_RULE_01 FOREIGN KEY (PLAYLIST_ID) REFERENCES sch.SCH_PLAYLIST (ID)
);

create index sch.idx_SCH_PLAYLIST_RULE_01 on sch.SCH_PLAYLIST_RULE(PLAYLIST_ID)
tablespace sch_index;

--
-- sch.SCH_CHANNEL_SCHEDULE : This table stores the mapping between the playlists and channels
-- along with the schedule as to when to play a given playlist for the given channel.
--
CREATE TABLE sch.SCH_CHANNEL_SCHEDULE ( 
    ID NUMBER(19) NOT NULL, 
    VERSION NUMBER(10), 
    CRON_EXPRESSION VARCHAR2(50 CHAR), 
    CHANNEL_ID NUMBER(19) NOT NULL, 
    PLAYLIST_ID NUMBER(19) NOT NULL, 
    CREATED_BY VARCHAR2(255 CHAR), 
    CREATED_TIME TIMESTAMP(6), 
    LAST_UPDATED_BY VARCHAR2(255 CHAR), 
    LAST_UPDATED_TIME TIMESTAMP(6)
);


alter table sch.sch_channel_schedule
add constraint pk_sch_channel_schedule
primary key (id)
using index tablespace sch_index;


ALTER TABLE sch.SCH_CHANNEL_SCHEDULE ADD ( 
    CONSTRAINT FK_SCH_CHANNEL_SCHEDULE_01 FOREIGN KEY (CHANNEL_ID) REFERENCES sch.SCH_CHANNEL (ID)
);


ALTER TABLE sch.SCH_CHANNEL_SCHEDULE ADD ( 
    CONSTRAINT FK_SCH_CHANNEL_SCHEDULE_02 FOREIGN KEY (PLAYLIST_ID) REFERENCES sch.SCH_PLAYLIST (ID)
);

create index sch.idx_SCH_CHANNEL_SCHEDULE_01 on sch.SCH_CHANNEL_SCHEDULE(CHANNEL_ID)
tablespace sch_index;

create index sch.idx_SCH_CHANNEL_SCHEDULE_02 on sch.SCH_CHANNEL_SCHEDULE(PLAYLIST_ID)
tablespace sch_index;


-- TODO - this needs to be done!!!!!!!!!!!!!!!!!!!!!!!!!!
--ALTER TABLE sch.SCH_CHANNEL_SCHEDULE ADD CONSTRAINT UNQ_CHANNEL_SCHEDULE_CRON
--UNIQUE(CHANNEL_ID, PLAYLIST_ID, CRON_EXPRESSION);


--
-- sch.SCH_LOCK : This table is used by the cluster component for locking.
--
CREATE TABLE sch.SCH_LOCK ( 
    ID NUMBER(19) NOT NULL, 
    LOCK_TYPE VARCHAR2(20 CHAR) NOT NULL
);

alter table sch.sch_lock
add constraint pk_sch_lock
primary key (id)
using index tablespace sch_index;

--
-- sch.SCH_NODE : This table maintains the runtime instances of scheduler caches available
--
CREATE TABLE sch.SCH_NODE ( 
    ID NUMBER(19) NOT NULL, 
    INSTANCE_NAME VARCHAR2(255 CHAR) NOT NULL, -- the name of the instance
    INSTANCE_URL VARCHAR2(255 CHAR) NOT NULL,  -- the URL of the instance to communicate with the instance
    LAST_CHECKIN_TIME NUMBER(19) NOT NULL -- the timestamp used to determine if the node is still alive
);


alter table sch.sch_node
add constraint pk_sch_node
primary key (id)
using index tablespace sch_index;

--
-- sch.SCH_NODE_CHANNEL  (Table) 
--
CREATE TABLE sch.SCH_NODE_CHANNEL ( 
    ID NUMBER(19) NOT NULL, 
    CHANNEL_NAME VARCHAR2(255 CHAR) NOT NULL, 
    NODE_ID NUMBER(19) NOT NULL 
);

alter table sch.sch_node_channel
add constraint pk_sch_node_channel
primary key (id)
using index tablespace sch_index;


ALTER TABLE sch.SCH_NODE_CHANNEL ADD ( 
    CONSTRAINT FK_SCH_NODE_CHANNEL_01 FOREIGN KEY (NODE_ID) REFERENCES sch.SCH_NODE (ID)
);

create index sch.idx_SCH_NODE_CHANNEL_01 on sch.SCH_NODE_CHANNEL(NODE_ID)
tablespace sch_index;


--
-- SEQUENCES 
--

-- 
-- Dropping all the sequences
--
DROP SEQUENCE sch.SCH_CHANNEL_SCHEDULE_SEQ;
DROP SEQUENCE sch.SCH_CHANNEL_SEQ;
DROP SEQUENCE sch.SCH_PLAYLIST_CLIP_SEQ;
DROP SEQUENCE sch.SCH_LOCK_SEQ;
DROP SEQUENCE sch.SCH_NODE_SEQ;
DROP SEQUENCE sch.SCH_PLAYLIST_RULE_SEQ;
DROP SEQUENCE sch.SCH_PLAYLIST_SEQ;
DROP SEQUENCE sch.SCH_NODE_CHANNEL_SEQ;

--
-- Create the sequences with the starting value of 1000 to accomodate any data migration
--

CREATE SEQUENCE sch.SCH_CHANNEL_SCHEDULE_SEQ
  START WITH 1000
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


CREATE SEQUENCE sch.SCH_CHANNEL_SEQ
  START WITH 1000
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
  

CREATE SEQUENCE sch.SCH_PLAYLIST_CLIP_SEQ
  START WITH 1000
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


CREATE SEQUENCE sch.SCH_PLAYLIST_RULE_SEQ
  START WITH 1000
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


CREATE SEQUENCE sch.SCH_PLAYLIST_SEQ
  START WITH 1000
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;

CREATE SEQUENCE sch.SCH_LOCK_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;  

CREATE SEQUENCE sch.SCH_NODE_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
  
CREATE SEQUENCE sch.SCH_NODE_CHANNEL_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
  
  
INSERT INTO sch.SCH_LOCK ( ID, LOCK_TYPE ) VALUES ( sch.SCH_LOCK_SEQ.nextVal, 'CHECK_INSTANCES_LOCK'); 
COMMIT;
  


quit
EOF




# (4) Create quartz objects for Scheduler
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 4_cr_scheduler_quartz_objects.sql

set echo off
set feed off
set pages 0

delete from  sch.sch_qrtz_job_listeners;
delete from  sch.sch_qrtz_trigger_listeners;
delete from  sch.sch_qrtz_fired_triggers;
delete from  sch.sch_qrtz_simple_triggers;
delete from  sch.sch_qrtz_cron_triggers;
delete from  sch.sch_qrtz_blob_triggers;
delete from  sch.sch_qrtz_triggers;
delete from  sch.sch_qrtz_job_details;
delete from  sch.sch_qrtz_calendars;
delete from  sch.sch_qrtz_paused_trigger_grps;
delete from  sch.sch_qrtz_locks;
delete from  sch.sch_qrtz_scheduler_state;

drop table  sch.sch_qrtz_calendars;
drop table  sch.sch_qrtz_fired_triggers;
drop table  sch.sch_qrtz_trigger_listeners;
drop table  sch.sch_qrtz_blob_triggers;
drop table  sch.sch_qrtz_cron_triggers;
drop table  sch.sch_qrtz_simple_triggers;
drop table  sch.sch_qrtz_triggers;
drop table  sch.sch_qrtz_job_listeners;
drop table  sch.sch_qrtz_job_details;
drop table  sch.sch_qrtz_paused_trigger_grps;
drop table  sch.sch_qrtz_locks;
drop table  sch.sch_qrtz_scheduler_state;


CREATE TABLE  sch.sch_qrtz_job_details
  (
    JOB_NAME  VARCHAR2(80) NOT NULL,
    JOB_GROUP VARCHAR2(80) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    JOB_CLASS_NAME   VARCHAR2(128) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    IS_STATEFUL VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP)
);
CREATE TABLE  sch.sch_qrtz_job_listeners
  (
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    JOB_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
    REFERENCES  sch.sch_qrtz_JOB_DETAILS(JOB_NAME,JOB_GROUP)
);
CREATE TABLE  sch.sch_qrtz_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(80) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
    REFERENCES  sch.sch_qrtz_JOB_DETAILS(JOB_NAME,JOB_GROUP) 
);
CREATE TABLE  sch.sch_qrtz_simple_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(7) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES  sch.sch_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE  sch.sch_qrtz_cron_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    CRON_EXPRESSION VARCHAR2(80) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES  sch.sch_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE  sch.sch_qrtz_blob_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
        REFERENCES  sch.sch_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE  sch.sch_qrtz_trigger_listeners
  (
    TRIGGER_NAME  VARCHAR2(80) NOT NULL, 
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    TRIGGER_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES  sch.sch_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE  sch.sch_qrtz_calendars
  (
    CALENDAR_NAME  VARCHAR2(80) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (CALENDAR_NAME)
);
CREATE TABLE  sch.sch_qrtz_paused_trigger_grps
  (
    TRIGGER_GROUP  VARCHAR2(80) NOT NULL, 
    PRIMARY KEY (TRIGGER_GROUP)
);
CREATE TABLE  sch.sch_qrtz_fired_triggers 
  (
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(80) NULL,
    JOB_GROUP VARCHAR2(80) NULL,
    IS_STATEFUL VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    PRIMARY KEY (ENTRY_ID)
);
CREATE TABLE  sch.sch_qrtz_scheduler_state 
  (
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    PRIMARY KEY (INSTANCE_NAME)
);
CREATE TABLE  sch.sch_qrtz_locks
  (
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    PRIMARY KEY (LOCK_NAME)
);
INSERT INTO  sch.sch_qrtz_locks values('TRIGGER_ACCESS');
INSERT INTO  sch.sch_qrtz_locks values('JOB_ACCESS');
INSERT INTO  sch.sch_qrtz_locks values('CALENDAR_ACCESS');
INSERT INTO  sch.sch_qrtz_locks values('STATE_ACCESS');
INSERT INTO  sch.sch_qrtz_locks values('MISFIRE_ACCESS');
create index sch.idx_qrtz_j_req_recovery on  sch.sch_qrtz_job_details(REQUESTS_RECOVERY);
create index sch.idx_qrtz_t_next_fire_time on  sch.sch_qrtz_triggers(NEXT_FIRE_TIME);
create index sch.idx_qrtz_t_state on  sch.sch_qrtz_triggers(TRIGGER_STATE);
create index sch.idx_qrtz_t_nft_st on  sch.sch_qrtz_triggers(NEXT_FIRE_TIME,TRIGGER_STATE);
create index sch.idx_qrtz_t_volatile on  sch.sch_qrtz_triggers(IS_VOLATILE);
create index sch.idx_qrtz_ft_trig_name on  sch.sch_qrtz_fired_triggers(TRIGGER_NAME);
create index sch.idx_qrtz_ft_trig_group on  sch.sch_qrtz_fired_triggers(TRIGGER_GROUP);
create index sch.idx_qrtz_ft_trig_nm_gp on  sch.sch_qrtz_fired_triggers(TRIGGER_NAME,TRIGGER_GROUP);
create index sch.idx_qrtz_ft_trig_volatile on  sch.sch_qrtz_fired_triggers(IS_VOLATILE);
create index sch.idx_qrtz_ft_trig_inst_name on  sch.sch_qrtz_fired_triggers(INSTANCE_NAME);
create index sch.idx_qrtz_ft_job_name on  sch.sch_qrtz_fired_triggers(JOB_NAME);
create index sch.idx_qrtz_ft_job_group on  sch.sch_qrtz_fired_triggers(JOB_GROUP);
create index sch.idx_qrtz_ft_job_stateful on  sch.sch_qrtz_fired_triggers(IS_STATEFUL);
create index sch.idx_qrtz_ft_job_req_recovery on  sch.sch_qrtz_fired_triggers(REQUESTS_RECOVERY);

quit
EOF




# (5) Grant privileges to Scheduler user
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 5_grant.sql

set echo off
set feed off
set pages 0

def param_user = 'sch_user';
def param_owner = 'sch';


DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_tab is
        select table_name, owner
        from all_tables
        where owner = UPPER('&param_owner');
cursor get_vw is
        select view_name, owner
        from all_views
        where owner = UPPER('&param_owner');

cursor get_seq is
        select sequence_name, sequence_owner
        from all_sequences
        where sequence_owner = UPPER('&param_owner');


BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

FOR tab_rec in get_tab LOOP

        l_sql := 'grant select, insert, update, delete on ' || tab_rec.owner || '.' || tab_rec.table_name || ' to &param_user';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

FOR vw_rec in get_vw LOOP

        l_sql := 'grant select on ' || vw_rec.owner || '.' || vw_rec.view_name || ' to &param_user';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        begin
           result := dbms_sql.execute(cursor_id);
        end;

END LOOP;

FOR seq_rec in get_seq LOOP

        l_sql := 'grant select on ' || seq_rec.sequence_owner || '.' || seq_rec.sequence_name || ' to &param_user';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/


quit
EOF



# (6) Drop Scheduler Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 6_drop_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'sch_user';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select synonym_name,owner
        from dba_synonyms
        where owner = UPPER('&param_user');

BEGIN

cursor_id:=dbms_sql.open_cursor;

dbms_output.put_line('Starting drop syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'drop synonym ' || obj_rec.owner || '.' || obj_rec.synonym_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (7) Create Scheduler Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 7_cr_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'sch_user';
def param_schema = 'sch';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select object_name, owner
        from all_objects
        where owner = UPPER('&param_schema')
        and object_type in
        ('TABLE', 'VIEW', 'CLUSTER', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE')
        ;

BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

dbms_output.put_line('Starting create syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'create synonym ' || '&param_user' || '.' || obj_rec.object_name || ' for ' || obj_rec.owner || '.' || obj_rec.object_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (8) Seed the Scheduler database with configuration data.
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 8_seed_scheduler.sql

set echo off
set feed off
set pages 0


quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


