#!/bin/bash
# Set up more of the activemq database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) create users and tablespace for activemq
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 2_create_activemq_users.sql

set echo off
set feed off
set pages 0
def sid = 'MOBI2';

CREATE TABLESPACE  activemq_data
    LOGGING 
    DATAFILE '/data2/oradata/&sid/activemq_data01.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

CREATE USER activemq  PROFILE "DEFAULT" 
    DEFAULT TABLESPACE activemq_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK
    IDENTIFIED BY "27CUfRes";
grant connect to activemq;
grant resource to activemq;
alter user activemq quota unlimited on activemq_data;
quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


