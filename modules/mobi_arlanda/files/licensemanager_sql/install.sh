#!/bin/bash
# Install the License Manager database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) Create users and tablespace for LM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 2_cr_lm_users.sql

set echo off
set feed off
set pages 0

def sid = 'prdjb';

CREATE TABLESPACE  lm_data
    LOGGING 
    DATAFILE '/data2/oradata/&sid/lm_data01.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

CREATE TABLESPACE  lm_index
    LOGGING 
    DATAFILE '/data2/oradata/&sid/lm_index1.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;
--------------------------------------------------------------------------------------------------------

-- lm schema
-- the lm schema will own all the "lm" objects

CREATE USER lm  PROFILE "DEFAULT" 
    IDENTIFIED BY lm 
    DEFAULT TABLESPACE lm_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to lm;
grant resource to lm;
alter user lm quota unlimited on lm_data;
alter user lm quota unlimited on lm_index;
--------------------------------------------------------------------------------------------------------

-- lm role
-- grant object privs to this role
create role lm_role;

--------------------------------------------------------------------------------------------------------
-- lm_user user
-- the lm_user will be used by the lm application to connect to the database
-- the lm_user will only own synonyms to the lm schema objects.

CREATE USER lm_user  PROFILE "DEFAULT" 
    IDENTIFIED BY lm_user 
    DEFAULT TABLESPACE lm_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to lm_user;
grant lm_role to lm_user;
alter user lm_user quota unlimited on lm_data;
alter user lm_user quota unlimited on lm_index;

quit
EOF

# (3) Create schema objects for LM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 3_cr_lm_schema_objects.sql

set echo off
set feed off
set pages 0


DROP TABLE lm.LM_LICENSE CASCADE CONSTRAINTS;
DROP TABLE lm.LM_ASSET_KEYS CASCADE CONSTRAINTS;
DROP TABLE lm.LM_USER_KEYS CASCADE CONSTRAINTS;
DROP TABLE lm.LM_PROVIDER_CONFIG CASCADE CONSTRAINTS;
DROP TABLE lm.LM_PACKAGE_CONFIG CASCADE CONSTRAINTS;
DROP TABLE lm.LM_PACKAGE_KEYS CASCADE CONSTRAINTS;
DROP TABLE lm.LM_PACKAGE_ASSETS CASCADE CONSTRAINTS;
DROP TABLE lm.LM_CONTENT_LICENSE_ID CASCADE CONSTRAINTS;
DROP TABLE lm.LM_PACKAGE_LICENSE_ID CASCADE CONSTRAINTS;

CREATE TABLE  lm.LM_ASSET_KEYS 
   (	"ASSET_ID" NUMBER(19,0) NOT NULL ENABLE, 
	"CONTENT_TYPE" VARCHAR2(20) DEFAULT 'vod' NOT NULL ENABLE, 
	"BROADCAST_ID" NUMBER(19, 0) DEFAULT 0 NOT NULL ENABLE, 
	"AES_KEY" BLOB NOT NULL ENABLE, 
	"LAST_MODIFIED" TIMESTAMP (6), 
	"EXPIRES" TIMESTAMP (6), 
	"PROVIDER_ID" NUMBER(19,0) 
   )
/
alter table lm.lm_asset_keys
add constraint pk_lm_asset_keys
primary key (asset_id, content_type, broadcast_id)
using index tablespace lm_index;

CREATE TABLE  lm.LM_USER_KEYS
   (	"USER_ID" VARCHAR2(255 CHAR) NOT NULL ENABLE, 
	"BLACKLISTED" NUMBER(1,0), 
	"DEVICE_ID" VARCHAR2(1024 CHAR) NOT NULL ENABLE, 
	"LAST_MODIFIED" TIMESTAMP (6), 
	"RSA_KEY" BLOB NOT NULL ENABLE, 
	"X_VALUE" VARCHAR2(1024 CHAR) NOT NULL ENABLE
   )
/
alter table lm.lm_user_keys
add constraint pk_lm_user_keys
primary key (user_id)
using index tablespace lm_index;

CREATE TABLE  lm.LM_LICENSE
   (	"LICENSE_ID" NUMBER(10,0) NOT NULL ENABLE,
        "CREATE_TIME" TIMESTAMP(6) DEFAULT SYSTIMESTAMP NOT NULL
   )
/
alter table lm.lm_license
add constraint pk_lm_license
primary key (license_id)
using index tablespace lm_index;

CREATE TABLE lm.LM_PACKAGE_CONFIG (
    "PACKAGE_ID" NUMBER(19,0) NOT NULL ENABLE,
    "KEY_ROTATION_FREQ" NUMBER(5,0) DEFAULT 1440 NOT NULL ENABLE,
    "START_TIME_OFFSET" NUMBER(5,0) DEFAULT 0 NOT NULL ENABLE,
    "TOTAL_NUM_KEYS" NUMBER(5,0) DEFAULT 16 NOT NULL ENABLE,
    "MAX_PACKAGE_KEY_ID" NUMBER(5,0) DEFAULT 2000 NOT NULL ENABLE,
    "LAST_MODIFIED" TIMESTAMP (6)
);
alter table lm.lm_package_config
add constraint pk_lm_package_config
primary key (package_id)
using index tablespace lm_index;

CREATE TABLE lm.LM_PACKAGE_KEYS (
        "ID" NUMBER(19,0) NOT NULL,
        "PACKAGE_ID" NUMBER(19,0) NOT NULL ENABLE,
        "PACKAGE_KEY_ID" NUMBER(5,0) NOT NULL ENABLE,
        "PACKAGE_KEY" BLOB NOT NULL ENABLE,
        "EFFECTIVE_START_TIME" TIMESTAMP(6) NOT NULL ENABLE,
        "EFFECTIVE_END_TIME" TIMESTAMP(6) NOT NULL ENABLE,
        "LAST_MODIFIED" TIMESTAMP (6)
   );
alter table lm.lm_package_keys
add constraint pk_lm_package_keys
primary key (id)
using index tablespace lm_index;

alter table lm.lm_package_keys
add constraint fk_lm_package_keys_01
foreign key (package_id) references lm.lm_package_config(package_id);
alter table lm.lm_package_keys
add constraint uk_lm_package_keys_01 unique(PACKAGE_ID, PACKAGE_KEY_ID);

create index lm.idx_lm_package_keys_01 on lm.lm_package_keys(package_id)
tablespace lm_index;

alter table lm.lm_package_keys
add constraint pk_lm_package_keys_02
unique (PACKAGE_ID, EFFECTIVE_START_TIME, EFFECTIVE_END_TIME)
using index tablespace lm_index;

CREATE TABLE lm.LM_PACKAGE_ASSETS (
        "ID" NUMBER(19,0) NOT NULL,
        "PACKAGE_ID" NUMBER(19,0) NOT NULL ENABLE,
        "ASSET_ID" NUMBER(19,0) NOT NULL ENABLE,
        "CONTENT_TYPE" VARCHAR2(20) DEFAULT 'vod' NOT NULL,
        "BROADCAST_ID" NUMBER(19, 0) DEFAULT 0 NOT NULL,
        "LICENSE_START" TIMESTAMP(6) DEFAULT SYSTIMESTAMP NOT NULL ENABLE,
        "LAST_MODIFIED" TIMESTAMP (6)
   );
alter table lm.lm_package_assets
add constraint pk_lm_package_assets
primary key (id)
using index tablespace lm_index;

alter table lm.lm_package_assets
add constraint fk_lm_package_assets_01
foreign key (package_id) references lm.lm_package_config(package_id);
alter table lm.lm_package_assets
add constraint uk_lm_package_assets_01 unique(PACKAGE_ID, ASSET_ID, CONTENT_TYPE, BROADCAST_ID);
alter table lm.lm_package_assets
add constraint fk_lm_package_assets_02
foreign key (asset_id, content_type, broadcast_id) references lm.lm_asset_keys(asset_id, content_type, broadcast_id)
on delete cascade;

CREATE TABLE  lm.LM_PROVIDER_CONFIG
   (	"PROVIDER_ID" NUMBER(19,0) NOT NULL ENABLE, 
	"BEGIN_EXP" VARCHAR2(256 CHAR) NOT NULL ENABLE, 
	"EXPIRY_EXP" VARCHAR2(256 CHAR) NOT NULL ENABLE, 
	"LAST_MODIFIED" TIMESTAMP (6)
   )
/
alter table lm.lm_provider_config
add constraint pk_lm_provider_config
primary key (provider_id)
using index tablespace lm_index;

DROP SEQUENCE lm.LM_LICENSE_SEQ;
DROP SEQUENCE lm.LM_PACKAGE_KEYS_SEQ;
DROP SEQUENCE lm.LM_PACKAGE_ASSETS_SEQ;

CREATE SEQUENCE lm.LM_LICENSE_SEQ  MINVALUE 1 MAXVALUE 16777215 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER CYCLE;
CREATE SEQUENCE lm.LM_PACKAGE_KEYS_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER CYCLE;
CREATE SEQUENCE lm.LM_PACKAGE_ASSETS_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER CYCLE;

CREATE TABLE lm.LM_CONTENT_LICENSE_ID (
       "ID" NUMBER(19,0) NOT NULL ENABLE,
       "CREATE_TIME" TIMESTAMP(6) DEFAULT SYSTIMESTAMP NOT NULL
  );
CREATE TABLE lm.LM_PACKAGE_LICENSE_ID (
       "ID" NUMBER(19,0) NOT NULL ENABLE,
       "CREATE_TIME" TIMESTAMP(6) DEFAULT SYSTIMESTAMP NOT NULL
   );

DROP SEQUENCE lm.LM_CONTENT_LICENSE_SEQ;
CREATE SEQUENCE lm.LM_CONTENT_LICENSE_SEQ MINVALUE 1 MAXVALUE 16777215 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;

DROP SEQUENCE lm.LM_PACKAGE_LICENSE_SEQ;
CREATE SEQUENCE lm.LM_PACKAGE_LICENSE_SEQ MINVALUE 1 MAXVALUE 16777215 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;

quit
EOF




# (4) Create quartz objects for LM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 4_cr_lm_quartz_objects.sql

set echo off
set feed off
set pages 0


delete from lm.lm_qrtz_job_listeners;
delete from lm.lm_qrtz_trigger_listeners;
delete from lm.lm_qrtz_fired_triggers;
delete from lm.lm_qrtz_simple_triggers;
delete from lm.lm_qrtz_cron_triggers;
delete from lm.lm_qrtz_blob_triggers;
delete from lm.lm_qrtz_triggers;
delete from lm.lm_qrtz_job_details;
delete from lm.lm_qrtz_calendars;
delete from lm.lm_qrtz_paused_trigger_grps;
delete from lm.lm_qrtz_locks;
delete from lm.lm_qrtz_scheduler_state;

drop table lm.lm_qrtz_calendars;
drop table lm.lm_qrtz_fired_triggers;
drop table lm.lm_qrtz_trigger_listeners;
drop table lm.lm_qrtz_blob_triggers;
drop table lm.lm_qrtz_cron_triggers;
drop table lm.lm_qrtz_simple_triggers;
drop table lm.lm_qrtz_triggers;
drop table lm.lm_qrtz_job_listeners;
drop table lm.lm_qrtz_job_details;
drop table lm.lm_qrtz_paused_trigger_grps;
drop table lm.lm_qrtz_locks;
drop table lm.lm_qrtz_scheduler_state;


CREATE TABLE lm.lm_qrtz_job_details
  (
    JOB_NAME  VARCHAR2(80) NOT NULL,
    JOB_GROUP VARCHAR2(80) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    JOB_CLASS_NAME   VARCHAR2(128) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    IS_STATEFUL VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP)
);
CREATE TABLE lm.lm_qrtz_job_listeners
  (
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    JOB_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
	REFERENCES lm.lm_qrtz_job_details(JOB_NAME,JOB_GROUP)
);
CREATE TABLE lm.lm_qrtz_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(80) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
	REFERENCES lm.lm_qrtz_job_details(JOB_NAME,JOB_GROUP) 
);
CREATE TABLE lm.lm_qrtz_simple_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(7) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES lm.lm_qrtz_triggers(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE lm.lm_qrtz_cron_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    CRON_EXPRESSION VARCHAR2(80) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES lm.lm_qrtz_triggers(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE lm.lm_qrtz_blob_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
        REFERENCES lm.lm_qrtz_triggers(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE lm.lm_qrtz_trigger_listeners
  (
    TRIGGER_NAME  VARCHAR2(80) NOT NULL, 
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    TRIGGER_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES lm.lm_qrtz_triggers(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE lm.lm_qrtz_calendars
  (
    CALENDAR_NAME  VARCHAR2(80) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (CALENDAR_NAME)
);
CREATE TABLE lm.lm_qrtz_paused_trigger_grps
  (
    TRIGGER_GROUP  VARCHAR2(80) NOT NULL, 
    PRIMARY KEY (TRIGGER_GROUP)
);
CREATE TABLE lm.lm_qrtz_fired_triggers 
  (
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(80) NULL,
    JOB_GROUP VARCHAR2(80) NULL,
    IS_STATEFUL VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    PRIMARY KEY (ENTRY_ID)
);
CREATE TABLE lm.lm_qrtz_scheduler_state 
  (
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    PRIMARY KEY (INSTANCE_NAME)
);
CREATE TABLE lm.lm_qrtz_locks
  (
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    PRIMARY KEY (LOCK_NAME)
);
INSERT INTO lm.lm_qrtz_locks values('TRIGGER_ACCESS');
INSERT INTO lm.lm_qrtz_locks values('JOB_ACCESS');
INSERT INTO lm.lm_qrtz_locks values('CALENDAR_ACCESS');
INSERT INTO lm.lm_qrtz_locks values('STATE_ACCESS');
INSERT INTO lm.lm_qrtz_locks values('MISFIRE_ACCESS');
create index lm.idx_lm_qrtz_j_req_recovery on lm.lm_qrtz_job_details(REQUESTS_RECOVERY);
create index lm.idx_lm_qrtz_t_next_fire_time on lm.lm_qrtz_triggers(NEXT_FIRE_TIME);
create index lm.idx_lm_qrtz_t_state on lm.lm_qrtz_triggers(TRIGGER_STATE);
create index lm.idx_lm_qrtz_t_nft_st on lm.lm_qrtz_triggers(NEXT_FIRE_TIME,TRIGGER_STATE);
create index lm.idx_lm_qrtz_t_volatile on lm.lm_qrtz_triggers(IS_VOLATILE);
create index lm.idx_lm_qrtz_ft_trig_name on lm.lm_qrtz_fired_triggers(TRIGGER_NAME);
create index lm.idx_lm_qrtz_ft_trig_group on lm.lm_qrtz_fired_triggers(TRIGGER_GROUP);
create index lm.idx_lm_qrtz_ft_trig_nm_gp on lm.lm_qrtz_fired_triggers(TRIGGER_NAME,TRIGGER_GROUP);
create index lm.idx_lm_qrtz_ft_trig_volatile on lm.lm_qrtz_fired_triggers(IS_VOLATILE);
create index lm.idx_lm_qrtz_ft_trig_inst_name on lm.lm_qrtz_fired_triggers(INSTANCE_NAME);
create index lm.idx_lm_qrtz_ft_job_name on lm.lm_qrtz_fired_triggers(JOB_NAME);
create index lm.idx_lm_qrtz_ft_job_group on lm.lm_qrtz_fired_triggers(JOB_GROUP);
create index lm.idx_lm_qrtz_ft_job_stateful on lm.lm_qrtz_fired_triggers(IS_STATEFUL);
create index lm.idx_lm_qrtz_ft_job_req_rec on lm.lm_qrtz_fired_triggers(REQUESTS_RECOVERY);



commit;

quit
EOF




# (5) Grant privileges to LM user
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 5_grant.sql

set echo off
set feed off
set pages 0

def user='lm_user';
def owner='lm';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_tab is
        select table_name, owner
        from all_tables
        where owner = UPPER('&owner');
cursor get_vw is
        select view_name, owner
        from all_views
        where owner = UPPER('&owner');

cursor get_seq is
        select sequence_name, sequence_owner
        from all_sequences
        where sequence_owner = UPPER('&owner');


BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

FOR tab_rec in get_tab LOOP

        l_sql := 'grant select, insert, update, delete on ' || tab_rec.owner || '.' || tab_rec.table_name || ' to &user';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

FOR vw_rec in get_vw LOOP

        l_sql := 'grant select on ' || vw_rec.owner || '.' || vw_rec.view_name || ' to &user';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        begin
           result := dbms_sql.execute(cursor_id);
        end;

END LOOP;

FOR seq_rec in get_seq LOOP

        l_sql := 'grant select on ' || seq_rec.sequence_owner || '.' || seq_rec.sequence_name || ' to &user';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/


quit
EOF



# (6) Drop LM Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 6_drop_syn.sql

set echo off
set feed off
set pages 0

def user='lm_user';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select synonym_name,owner
        from dba_synonyms
        where owner = UPPER('&user');

BEGIN

cursor_id:=dbms_sql.open_cursor;

dbms_output.put_line('Starting drop syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'drop synonym ' || obj_rec.owner || '.' || obj_rec.synonym_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/


quit
EOF



# (7) Create LM Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 7_cr_syn.sql

set echo off
set feed off
set pages 0

def user='lm_user';
def schema='lm';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select object_name, owner
        from all_objects
        where owner = UPPER('&schema')
        and object_type in
        ('TABLE', 'VIEW', 'CLUSTER', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE')
        ;

BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

dbms_output.put_line('Starting create syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'create synonym ' || '&user' || '.' || obj_rec.object_name || ' for ' || obj_rec.owner || '.' || obj_rec.object_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (8) Seed the LM database with configuration data.
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 8_seed_lm.sql

set echo off
set feed off
set pages 0


quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


