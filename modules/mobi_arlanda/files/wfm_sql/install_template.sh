#!/bin/bash
# Install the WFM database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) Create users and tablespace for WFM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 2_cr_wfm_users.sql

set echo off
set feed off
set pages 0

def sid = 'prdjb';

quit
EOF

# (3) Create schema objects for WFM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 3_cr_wfm_schema_objects.sql

set echo off
set feed off
set pages 0


quit
EOF




# (4) Create quartz objects for WFM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 4_cr_wfm_quartz_objects.sql

set echo off
set feed off
set pages 0


quit
EOF




# (5) Grant privileges to WFM user
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 5_grant.sql

set echo off
set feed off
set pages 0


quit
EOF



# (6) Drop WFM Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 6_drop_syn.sql

set echo off
set feed off
set pages 0


quit
EOF



# (7) Create WFM Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 7_cr_syn.sql

set echo off
set feed off
set pages 0

quit
EOF



# (8) Seed the WFM database with configuration data.
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 8_seed_wfm.sql

set echo off
set feed off
set pages 0


quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


