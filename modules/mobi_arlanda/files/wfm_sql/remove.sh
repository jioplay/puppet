#!/bin/bash
# Remove the WFM database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) delete users and tablespace for WFM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 1_drop_wfm.sql

set echo off
set feed off
set pages 0

drop user wfm cascade;
drop user wfm_user cascade;
drop tablespace wfm_data including contents;
drop tablespace wfm_index including contents;
drop role wfm_role;

quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


