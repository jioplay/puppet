#!/bin/bash
# Install the WFM database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) Create users and tablespace for WFM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 2_cr_wfm_users.sql

set echo off
set feed off
set pages 0

def sid = 'prdjb';

CREATE TABLESPACE  wfm_data
    LOGGING 
    DATAFILE '/data2/oradata/&sid/wfm_data01.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

CREATE TABLESPACE  wfm_index
    LOGGING 
    DATAFILE '/data2/oradata/&sid/wfm_index1.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;
--------------------------------------------------------------------------------------------------------

-- wfm schema
-- the wfm schema will own all the "wfm" objects

CREATE USER wfm  PROFILE "DEFAULT" 
    IDENTIFIED BY wfm 
    DEFAULT TABLESPACE wfm_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to wfm;
grant resource to wfm;
alter user wfm quota unlimited on wfm_data;
alter user wfm quota unlimited on wfm_index;
--------------------------------------------------------------------------------------------------------

-- wfm role
-- grant object privs to this role
create role wfm_role;

--------------------------------------------------------------------------------------------------------
-- wfm_user user
-- the wfm_user will be used by the wfm application to connect to the database
-- the wfm_user will only own synonyms to the wfm schema objects.

CREATE USER wfm_user  PROFILE "DEFAULT" 
    IDENTIFIED BY wfm_user 
    DEFAULT TABLESPACE wfm_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to wfm_user;
grant wfm_role to wfm_user;
alter user wfm_user quota unlimited on wfm_data;
alter user wfm_user quota unlimited on wfm_index;

quit
EOF

# (3) Create schema objects for WFM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 3_cr_wfm_schema_objects.sql

set echo off
set feed off
set pages 0


drop table wfm.WFM_ASSET_CONTEXT cascade constraints;
drop table wfm.WFM_EMAIL_NOTIFICATION cascade constraints;
drop table wfm.WFM_ENCODING cascade constraints;
drop table wfm.WFM_ENCODING_JOB cascade constraints;
drop table wfm.WFM_ENCODING_MAP cascade constraints;
drop table wfm.WFM_MESSAGE_MAP cascade constraints;
drop table wfm.WFM_PROVIDER_CONTEXT cascade constraints;
drop table wfm.WFM_PROVIDER_ENCODING cascade constraints;
drop table wfm.WFM_PROVIDER_NOTIFICATION cascade constraints;
drop table wfm.WFM_PROVIDER_WORKFLOW cascade constraints;
drop table wfm.WFM_TASK_DATA cascade constraints;
drop table wfm.WFM_TASK_INSTANCE cascade constraints;
drop table wfm.WFM_TASK_INSTANCE_DATA cascade constraints;
drop table wfm.WFM_TASK_INSTANCE_DEPENDENCY cascade constraints;
drop table wfm.WFM_WORKFLOW cascade constraints;
drop table wfm.WFM_WORKFLOW_INSTANCE cascade constraints;
drop table wfm.WFM_WORKFLOW_INSTANCE_TASK cascade constraints;


create table wfm.WFM_ASSET_CONTEXT 
(
  id number(19,0) not null, 
  ASSET_ID number(19,0), 
  PROVIDER_ID number(19,0), 
  MEDIA_URL varchar2(1023 char), 
  METADATA_URL varchar2(1023 char), 
  CARRIER_APPROVED number(1,0), 
  INVENTORY_ID number(19,0), 
  VERSION number(10,0),
  EXPIRES VARCHAR2(100),
  PRE_ROLL_URL VARCHAR2(1023 CHAR),
  POST_ROLL_URL VARCHAR2(1023 CHAR),
  MEDIA_ASPECT_RATIO VARCHAR2(40) DEFAULT '4x3'
)
/
alter table wfm.WFM_ASSET_CONTEXT 
  add constraint PK_ASSET_CONTEXT primary key (id)
  using index tablespace wfm_index;
create index wfm.IDX_ASSET_CONTEXT_01 on wfm.WFM_ASSET_CONTEXT(ASSET_ID)
  tablespace wfm_index;    
create index wfm.IDX_ASSET_CONTEXT_02 on wfm.WFM_ASSET_CONTEXT(PROVIDER_ID)
  tablespace wfm_index;    
create index wfm.IDX_ASSET_CONTEXT_03 on wfm.WFM_ASSET_CONTEXT(INVENTORY_ID)
  tablespace wfm_index;    


create table wfm.WFM_EMAIL_NOTIFICATION 
(
  id number(19,0) not null, 
  NOTIFICATION_TYPE varchar2(64 char) not null, 
  EMAIL_ADDRESS varchar2(1023 char) not null
)
/
alter table wfm.WFM_EMAIL_NOTIFICATION 
  add constraint PK_EMAIL_NOTIFICATION primary key (id)
  using index tablespace wfm_index;

  
create table wfm.WFM_ENCODING 
(
  id number(19,0) not null, 
  NAME varchar2(255 char) not null, 
  DESCRIPTION varchar2(1023 char),
  IS_ENCRYPTED  NUMBER  DEFAULT 0
)
/
alter table wfm.WFM_ENCODING 
  add constraint PK_ENCODING primary key (id)
  using index tablespace wfm_index;
alter table wfm.WFM_ENCODING 
  add constraint UNQ_WFM_ENCODING_NAME unique (NAME); 

create table wfm.WFM_ENCODING_JOB 
(
  id number(19,0) not null, 
  ENCODING_JOB_ID number(19,0) not null
)
/
alter table wfm.WFM_ENCODING_JOB
  add constraint PK_ENCODING_JOB primary key (id)
  using index tablespace wfm_index;


create table wfm.WFM_ENCODING_MAP 
(
  WFM_ENCODING_ID number(19,0) not null, 
  MP_ENCODING_ID number(19,0) not null
)
/  
alter table wfm.WFM_ENCODING_MAP 
  add constraint PK_ENCODING_MAP primary key (WFM_ENCODING_ID, MP_ENCODING_ID)
  using index tablespace wfm_index;
alter table wfm.WFM_ENCODING_MAP 
  add constraint FK_ENCODING_MAP_01 foreign key (MP_ENCODING_ID) 
  references wfm.WFM_ENCODING_JOB;
alter table wfm.WFM_ENCODING_MAP 
  add constraint FK_ENCODING_MAP_02 foreign key (WFM_ENCODING_ID) 
  references wfm.WFM_ENCODING;

  
create table wfm.WFM_PROVIDER_CONTEXT 
(
  id number(19,0) not null, 
  PRIORITY number(10,0) not null, 
  CONTENT_AUTH_MODE varchar2(255 char) not null,
  TRANSCODING_CLUSTER VARCHAR2(100) DEFAULT 'media' NOT NULL ENABLE,
  ADVERTISING_SOURCE VARCHAR2(100)
)
/  
alter table wfm.WFM_PROVIDER_CONTEXT
  add constraint PK_PROVIDER_CONTEXT primary key (id)
  using index tablespace wfm_index;
  
  
create table wfm.WFM_PROVIDER_ENCODING 
(
  PROVIDER_ID number(19,0) not null, 
  ENCODING_ID number(19,0) not null
)
/  
alter table wfm.WFM_PROVIDER_ENCODING 
  add constraint PK_PROVIDER_ENCODING primary key (PROVIDER_ID, ENCODING_ID)
  using index tablespace wfm_index;
alter table wfm.WFM_PROVIDER_ENCODING 
  add constraint FK_PROVIDER_ENCODING_01 foreign key (PROVIDER_ID) 
  references wfm.WFM_PROVIDER_CONTEXT;
alter table wfm.WFM_PROVIDER_ENCODING 
  add constraint FK_PROVIDER_ENCODING_02 foreign key (ENCODING_ID) 
  references wfm.WFM_ENCODING;


create table wfm.WFM_PROVIDER_NOTIFICATION 
(
  PROVIDER_ID number(19,0) not null, 
  NOTIFICATION_ID number(19,0) not null
)
/  
alter table wfm.WFM_PROVIDER_NOTIFICATION 
  add constraint PK_PROVIDER_NOTIFICATION primary key (PROVIDER_ID, NOTIFICATION_ID)
  using index tablespace wfm_index;
alter table wfm.WFM_PROVIDER_NOTIFICATION 
  add constraint FK_PROVIDER_NOTIFICATION_01 foreign key (PROVIDER_ID) 
  references wfm.WFM_PROVIDER_CONTEXT;
alter table wfm.WFM_PROVIDER_NOTIFICATION 
  add constraint FK_PROVIDER_NOTIFICATION_02 foreign key (NOTIFICATION_ID) 
  references wfm.WFM_EMAIL_NOTIFICATION;
  

create table wfm.WFM_WORKFLOW 
(
  id number(19,0) not null, 
  NAME varchar2(255 char) not null, 
  DESCRIPTION varchar2(1023 char), 
  WORKFLOW_TYPE varchar2(255 char) not null, 
  XML_TEMPLATE clob
)
/
alter table wfm.WFM_WORKFLOW 
  add constraint PK_WORKFLOW primary key (id)
  using index tablespace wfm_index;
alter table wfm.WFM_WORKFLOW 
  add constraint UNQ_WORKFLOW_NAME unique (NAME); 



create table wfm.WFM_PROVIDER_WORKFLOW 
(
  PROVIDER_ID number(19,0) not null, 
  WORKFLOW_ID number(19,0) not null
)
/  
alter table wfm.WFM_PROVIDER_WORKFLOW 
  add constraint PK_PROVIDER_WORKFLOW primary key (PROVIDER_ID, WORKFLOW_ID)
  using index tablespace wfm_index;
alter table wfm.WFM_PROVIDER_WORKFLOW 
  add constraint FK_PROVIDER_WORKFLOW_01 foreign key (WORKFLOW_ID) 
  references wfm.WFM_WORKFLOW;
alter table wfm.WFM_PROVIDER_WORKFLOW 
  add constraint FK_PROVIDER_WORKFLOW_02 foreign key (PROVIDER_ID) 
  references wfm.WFM_PROVIDER_CONTEXT;


create table wfm.WFM_TASK_DATA 
(
  id number(19,0) not null, 
  KEY varchar2(255 char) not null, 
  VALUE varchar2(255 char) not null
)
/  
alter table wfm.WFM_TASK_DATA
  add constraint PK_TASK_DATA primary key (id)
  using index tablespace wfm_index;
  
  
create table wfm.WFM_TASK_INSTANCE 
(
  id number(19,0) not null, 
  OPTLOCK number(10,0), 
  PROVIDER_ID number(19,0), 
  TASK_STATUS varchar2(64 char) not null, 
  TASK_TYPE varchar2(64 char) not null, 
  QUEUED_AT timestamp, 
  STARTED_AT timestamp, 
  FINISHED_AT timestamp
)
/  
alter table wfm.WFM_TASK_INSTANCE
  add constraint PK_TASK_INSTANCE primary key (id)
  using index tablespace wfm_index;
create index wfm.IDX_TASK_INSTANCE_01 on wfm.WFM_TASK_INSTANCE(PROVIDER_ID)
  tablespace wfm_index;    
  
create table wfm.WFM_TASK_INSTANCE_DATA 
(
  TASK_INSTANCE_ID number(19,0) not null, 
  TASK_DATA_ID number(19,0) not null
)
/  
alter table wfm.WFM_TASK_INSTANCE_DATA
  add constraint PK_TASK_INSTANCE_DATA primary key (TASK_INSTANCE_ID, TASK_DATA_ID)
  using index tablespace wfm_index;
alter table wfm.WFM_TASK_INSTANCE_DATA 
  add constraint UNQ_TASK_DATA_ID unique (TASK_DATA_ID);  
alter table wfm.WFM_TASK_INSTANCE_DATA 
  add constraint FK_TASK_INSTANCE_DATA_01 foreign key (TASK_DATA_ID) 
  references wfm.WFM_TASK_DATA;  
alter table wfm.WFM_TASK_INSTANCE_DATA 
  add constraint FK_TASK_INSTANCE_DATA_02 foreign key (TASK_INSTANCE_ID) 
  references wfm.WFM_TASK_INSTANCE;
  

create table wfm.WFM_TASK_INSTANCE_DEPENDENCY 
(
  TASK_INSTANCE_ID number(19,0) not null, 
  TASK_DEPENDS_ON_ID number(19,0) not null
)
/  
alter table wfm.WFM_TASK_INSTANCE_DEPENDENCY 
  add constraint PK_TASK_INSTANCE_DEPENDENCY primary key (TASK_INSTANCE_ID, TASK_DEPENDS_ON_ID)
  using index tablespace wfm_index;
alter table wfm.WFM_TASK_INSTANCE_DEPENDENCY 
  add constraint FK_TASK_INSTANCE_DEPENDENCY_01 foreign key (TASK_DEPENDS_ON_ID) 
  references wfm.WFM_TASK_INSTANCE;
alter table wfm.WFM_TASK_INSTANCE_DEPENDENCY 
  add constraint FK_TASK_INSTANCE_DEPENDENCY_02 foreign key (TASK_INSTANCE_ID) 
  references wfm.WFM_TASK_INSTANCE;


create table wfm.WFM_MESSAGE_MAP 
(
  id number(19,0) not null, 
  MESSAGE_ID varchar2(255 char) not null, 
  CORRELATION_ID varchar2(255 char) not null, 
  TASK_ID number(19,0) not null
)
/  
alter table wfm.WFM_MESSAGE_MAP 
  add constraint PK_MESSAGE_MAP primary key (id)
  using index tablespace wfm_index;
alter table wfm.WFM_MESSAGE_MAP 
  add constraint FK_MESSAGE_MAP_01 foreign key (TASK_ID) 
  references wfm.WFM_TASK_INSTANCE;
create index wfm.IDX_MESSAGE_MAP_01 on wfm.WFM_MESSAGE_MAP(TASK_ID)
  tablespace wfm_index;   

  
create table wfm.WFM_WORKFLOW_INSTANCE 
(
  id number(19,0) not null, 
  WORKFLOW_STATUS varchar2(64 char) not null, 
  STARTED_AT timestamp, FINISHED_AT timestamp, 
  PROVIDER_CONTEXT_ID number(19,0) not null, 
  ASSET_CONTEXT_ID number(19,0) not null, 
  WORKFLOW_ID number(19,0) not null,
  OPTLOCK NUMBER(10,0)
)
/
alter table wfm.WFM_WORKFLOW_INSTANCE 
  add constraint PK_WORKFLOW_INSTANCE primary key (id)
  using index tablespace wfm_index;
alter table wfm.WFM_WORKFLOW_INSTANCE 
  add constraint FK_WORKFLOW_INSTANCE_01 foreign key (WORKFLOW_ID) 
  references wfm.WFM_WORKFLOW;
alter table wfm.WFM_WORKFLOW_INSTANCE 
  add constraint FK_WORKFLOW_INSTANCE_02 foreign key (ASSET_CONTEXT_ID) 
  references wfm.WFM_ASSET_CONTEXT;
alter table wfm.WFM_WORKFLOW_INSTANCE 
  add constraint FK_WORKFLOW_INSTANCE_03 foreign key (PROVIDER_CONTEXT_ID) 
  references wfm.WFM_PROVIDER_CONTEXT;
create index wfm.IDX_WORKFLOW_INSTANCE_01 on wfm.WFM_WORKFLOW_INSTANCE(ASSET_CONTEXT_ID)
  tablespace wfm_index;  
create index wfm.IDX_WORKFLOW_INSTANCE_02 on wfm.WFM_WORKFLOW_INSTANCE(PROVIDER_CONTEXT_ID)
  tablespace wfm_index;  
create index wfm.IDX_WORKFLOW_INSTANCE_03 on wfm.WFM_WORKFLOW_INSTANCE(WORKFLOW_ID)
  tablespace wfm_index;
 
### The following statement makes the WORKFLOW_ISTANCE_ID and TASK_INSTANCE_ID fields primary keys
### In addition, the following constraints should be made:
### WORKFLOW_INSTANCE_ID should be a foreign key to WFM_WORKFLOW_INSTANCE.ID
### TASK_INSTANCE_ID should be a foreign key to WFM_TASK_INSTANCE.ID
### However, the production code is currently doing the following:
### WORKFLOW_INSTANCE_ID is referencing WFM_TASK_INSTANCE.ID
### TASK_INSTANCE_ID is referencing WFM_WORKFLOW_INSTANCE.ID
create table wfm.WFM_WORKFLOW_INSTANCE_TASK 
(
  WORKFLOW_INSTANCE_ID number(19,0) not null, 
  TASK_INSTANCE_ID number(19,0) not null
)
/
alter table wfm.WFM_WORKFLOW_INSTANCE_TASK 
  add constraint PK_WORKFLOW_INSTANCE_TASK primary key (WORKFLOW_INSTANCE_ID, TASK_INSTANCE_ID)
  using index tablespace wfm_index;
alter table wfm.WFM_WORKFLOW_INSTANCE_TASK 
  add constraint FK_WORKFLOW_INSTANCE_TASK_01 foreign key (TASK_INSTANCE_ID) 
  references wfm.WFM_WORKFLOW_INSTANCE;
alter table wfm.WFM_WORKFLOW_INSTANCE_TASK
  add constraint FK_WORKFLOW_INSTANCE_TASK_02 foreign key (WORKFLOW_INSTANCE_ID)
  references wfm.WFM_TASK_INSTANCE;
  
drop sequence wfm.WFM_ASSET_SEQ;
drop sequence wfm.WFM_EMAIL_SEQ;
drop sequence wfm.WFM_ENCODING_JOB_SEQ;
drop sequence wfm.WFM_ENCODING_SEQ;
drop sequence wfm.WFM_TASK_DATA_SEQ;
drop sequence wfm.WFM_TASK_INSTANCE_SEQ;
drop sequence wfm.WFM_TASK_MESSAGE_MAP_SEQ;
drop sequence wfm.WFM_WORKFLOW_INTSTANCE_SEQ;
drop sequence wfm.WFM_WORKFLOW_SEQ;

create sequence wfm.WFM_ASSET_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
create sequence wfm.WFM_EMAIL_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
create sequence wfm.WFM_ENCODING_JOB_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
create sequence wfm.WFM_ENCODING_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
create sequence wfm.WFM_TASK_DATA_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
create sequence wfm.WFM_TASK_INSTANCE_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
create sequence wfm.WFM_TASK_MESSAGE_MAP_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
create sequence wfm.WFM_WORKFLOW_INTSTANCE_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;
create sequence wfm.WFM_WORKFLOW_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;

commit;
quit
EOF




# (4) Create quartz objects for WFM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 4_cr_wfm_quartz_objects.sql

set echo off
set feed off
set pages 0


delete from wfm.wfm_qrtz_job_listeners;
delete from wfm.wfm_qrtz_trigger_listeners;
delete from wfm.wfm_qrtz_fired_triggers;
delete from wfm.wfm_qrtz_simple_triggers;
delete from wfm.wfm_qrtz_cron_triggers;
delete from wfm.wfm_qrtz_blob_triggers;
delete from wfm.wfm_qrtz_triggers;
delete from wfm.wfm_qrtz_job_details;
delete from wfm.wfm_qrtz_calendars;
delete from wfm.wfm_qrtz_paused_trigger_grps;
delete from wfm.wfm_qrtz_locks;
delete from wfm.wfm_qrtz_scheduler_state;

drop table wfm.wfm_qrtz_calendars;
drop table wfm.wfm_qrtz_fired_triggers;
drop table wfm.wfm_qrtz_trigger_listeners;
drop table wfm.wfm_qrtz_blob_triggers;
drop table wfm.wfm_qrtz_cron_triggers;
drop table wfm.wfm_qrtz_simple_triggers;
drop table wfm.wfm_qrtz_triggers;
drop table wfm.wfm_qrtz_job_listeners;
drop table wfm.wfm_qrtz_job_details;
drop table wfm.wfm_qrtz_paused_trigger_grps;
drop table wfm.wfm_qrtz_locks;
drop table wfm.wfm_qrtz_scheduler_state;


CREATE TABLE wfm.wfm_qrtz_job_details
  (
    JOB_NAME  VARCHAR2(80) NOT NULL,
    JOB_GROUP VARCHAR2(80) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    JOB_CLASS_NAME   VARCHAR2(128) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    IS_STATEFUL VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP)
);
CREATE TABLE wfm.wfm_qrtz_job_listeners
  (
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    JOB_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
	REFERENCES wfm.WFM_QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP)
);
CREATE TABLE wfm.wfm_qrtz_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(80) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
	REFERENCES wfm.WFM_QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP) 
);
CREATE TABLE wfm.wfm_qrtz_simple_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(7) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES wfm.WFM_QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE wfm.wfm_qrtz_cron_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    CRON_EXPRESSION VARCHAR2(80) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES wfm.WFM_QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE wfm.wfm_qrtz_blob_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
        REFERENCES wfm.WFM_QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE wfm.wfm_qrtz_trigger_listeners
  (
    TRIGGER_NAME  VARCHAR2(80) NOT NULL, 
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    TRIGGER_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES wfm.WFM_QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE wfm.wfm_qrtz_calendars
  (
    CALENDAR_NAME  VARCHAR2(80) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (CALENDAR_NAME)
);
CREATE TABLE wfm.wfm_qrtz_paused_trigger_grps
  (
    TRIGGER_GROUP  VARCHAR2(80) NOT NULL, 
    PRIMARY KEY (TRIGGER_GROUP)
);
CREATE TABLE wfm.wfm_qrtz_fired_triggers 
  (
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(80) NULL,
    JOB_GROUP VARCHAR2(80) NULL,
    IS_STATEFUL VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    PRIMARY KEY (ENTRY_ID)
);
CREATE TABLE wfm.wfm_qrtz_scheduler_state 
  (
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    PRIMARY KEY (INSTANCE_NAME)
);
CREATE TABLE wfm.wfm_qrtz_locks
  (
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    PRIMARY KEY (LOCK_NAME)
);
create index wfm.idx_wfmqtz_j_req_recovery on wfm.wfm_qrtz_job_details(REQUESTS_RECOVERY);
create index wfm.idx_wfmqtz_t_next_fire_time on wfm.wfm_qrtz_triggers(NEXT_FIRE_TIME);
create index wfm.idx_wfmqtz_t_state on wfm.wfm_qrtz_triggers(TRIGGER_STATE);
create index wfm.idx_wfmqtz_t_nft_st on wfm.wfm_qrtz_triggers(NEXT_FIRE_TIME,TRIGGER_STATE);
create index wfm.idx_wfmqtz_t_volatile on wfm.wfm_qrtz_triggers(IS_VOLATILE);
create index wfm.idx_wfmqtz_ft_trig_name on wfm.wfm_qrtz_fired_triggers(TRIGGER_NAME);
create index wfm.idx_wfmqtz_ft_trig_group on wfm.wfm_qrtz_fired_triggers(TRIGGER_GROUP);
create index wfm.idx_wfmqtz_ft_trig_nm_gp on wfm.wfm_qrtz_fired_triggers(TRIGGER_NAME,TRIGGER_GROUP);
create index wfm.idx_wfmqtz_ft_trig_volatile on wfm.wfm_qrtz_fired_triggers(IS_VOLATILE);
create index wfm.idx_wfmqtz_ft_trig_inst_name on wfm.wfm_qrtz_fired_triggers(INSTANCE_NAME);
create index wfm.idx_wfmqtz_ft_job_name on wfm.wfm_qrtz_fired_triggers(JOB_NAME);
create index wfm.idx_wfmqtz_ft_job_group on wfm.wfm_qrtz_fired_triggers(JOB_GROUP);
create index wfm.idx_wfmqtz_ft_job_stateful on wfm.wfm_qrtz_fired_triggers(IS_STATEFUL);
create index wfm.idx_wfmqtz_ft_job_req_recovery on wfm.wfm_qrtz_fired_triggers(REQUESTS_RECOVERY);

commit;
quit
EOF




# (5) Grant privileges to WFM user
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 5_grant.sql

set echo off
set feed off
set pages 0

def param_grantee = 'wfm_user';
def param_owner = 'wfm';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_tab is
        select table_name, owner
        from all_tables
        where owner = UPPER('&param_owner');
cursor get_vw is
        select view_name, owner
        from all_views
        where owner = UPPER('&param_owner');

cursor get_seq is
        select sequence_name, sequence_owner
        from all_sequences
        where sequence_owner = UPPER('&param_owner');


BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

FOR tab_rec in get_tab LOOP

        l_sql := 'grant select, insert, update, delete on ' || tab_rec.owner || '.' || tab_rec.table_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

FOR vw_rec in get_vw LOOP

        l_sql := 'grant select on ' || vw_rec.owner || '.' || vw_rec.view_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        begin
           result := dbms_sql.execute(cursor_id);
        end;

END LOOP;

FOR seq_rec in get_seq LOOP

        l_sql := 'grant select on ' || seq_rec.sequence_owner || '.' || seq_rec.sequence_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/


quit
EOF



# (6) Drop WFM Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 6_drop_syn.sql

set echo off
set feed off
set pages 0

def param_owner = 'wfm_user';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select synonym_name,owner
        from dba_synonyms
        where owner = UPPER('&param_owner');

BEGIN

cursor_id:=dbms_sql.open_cursor;

dbms_output.put_line('Starting drop syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'drop synonym ' || obj_rec.owner || '.' || obj_rec.synonym_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (7) Create WFM Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 7_cr_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'wfm_user';
def param_schema = 'wfm';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select object_name, owner
        from all_objects
        where owner = UPPER('&param_schema')
        and object_type in
        ('TABLE', 'VIEW', 'CLUSTER', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE')
        ;

BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

dbms_output.put_line('Starting create syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'create synonym ' || '&param_user' || '.' || obj_rec.object_name || ' for ' || obj_rec.owner || '.' || obj_rec.object_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (8) Seed the WFM database with configuration data.
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 8_seed_wfm.sql

set echo off
set feed off
set pages 0

REM INSERTING into WFM.WFM_ENCODING
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (81,'AppleSegmentedVod','Apple Segmented Vod encoding set',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (141,'FragmentedMp4','Fragmented MP4 encoding set',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (202,'Core 16x9','Core WQVGA encoding set',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (201,'Core','Core QVGA encoding set',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (221,'WVGA ProgressiveDownload','WVGA PGDL 4x3 encoding set',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (222,'WVGA ProgressiveDownload 16x9','WVGA PGDL 16x9 encoding set',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (121,'RTSP 16x9','RTSP 500 and 800 Kbps WQVGA',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (122,'ProgressiveDownload 16x9','Progressive Download WQVGA encoding set',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (181,'DRM 16x9','DRM Encoding for 16x9',1);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (26,'Audio','Audio-only encoding set.',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (41,'ProgressiveDownload','Progressive Download encoding set',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (61,'DRM','DRM Encoding',1);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (101,'RSTP High','RTSP 500 and 800 Kbps QVGA',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (102,'ProgressiveDownload VGA 800','ProgressiveDownload VGA 800',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (21,'Master','Superchunk master format.',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (22,'Chunk','Legacy chunk encoding set.',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (23,'RTSP','Standard RTSP encoding set.',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (24,'WinMo','Windows Media / Mobile encoding set.',0);
Insert into WFM.WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (25,'PCTV','Windows Media / PCTV encoding set.',0);


REM INSERTING into WFM.WFM_ENCODING_JOB
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (61,11);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (401,1);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (402,2);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (403,3);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (404,4);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (405,5);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (406,6);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (626,65);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (521,94);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (407,7);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (408,8);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (522,91);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (627,66);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (74,100);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (628,75);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (409,12);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (410,13);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (411,14);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (412,15);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (413,18);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (569,70);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (570,71);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (181,102);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (182,101);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (571,77);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (572,78);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (573,79);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (574,72);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (575,73);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (576,74);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (462,21);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (463,23);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (464,22);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (465,25);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (466,24);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (467,27);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (468,26);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (361,73);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (523,93);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (524,95);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (601,103);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (121,41);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (122,40);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (577,68);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (578,69);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (579,64);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (580,67);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (581,62);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (582,61);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (583,60);
Insert into WFM.WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (602,104);

REM INSERTING into WFM.WFM_ENCODING_MAP
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (21,61);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,401);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,402);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,403);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,404);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,405);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,406);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,407);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,408);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,409);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,410);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,411);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,412);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,413);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (24,181);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (24,182);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (25,74);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (26,121);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (26,122);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,569);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,570);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,571);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,572);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,573);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,574);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,575);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,576);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (61,521);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (61,522);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (102,361);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,462);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,463);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,464);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,465);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,466);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,467);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,468);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,577);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,578);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,579);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,580);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,581);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,582);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,583);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (181,523);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (181,524);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (201,601);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (202,602);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (221,626);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (221,627);
Insert into WFM.WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (221,628);


REM INSERTING into WFM.WFM_QRTZ_LOCKS
Insert into WFM.WFM_QRTZ_LOCKS (LOCK_NAME) values ('CALENDAR_ACCESS');
Insert into WFM.WFM_QRTZ_LOCKS (LOCK_NAME) values ('JOB_ACCESS');
Insert into WFM.WFM_QRTZ_LOCKS (LOCK_NAME) values ('MISFIRE_ACCESS');
Insert into WFM.WFM_QRTZ_LOCKS (LOCK_NAME) values ('STATE_ACCESS');
Insert into WFM.WFM_QRTZ_LOCKS (LOCK_NAME) values ('TRIGGER_ACCESS');


REM INSERTING into WFM.WFM_QRTZ_JOB_DETAILS
Insert into WFM.WFM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('workflowCleanupJob','DEFAULT',null,'com.mobitv.arlanda.wfm.tasks.CleanupTask','0','0','0','0');


REM INSERTING into WFM.WFM_WORKFLOW
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (61,'TranscodeWinMo','Winmo transcode workflow','TranscodeWinMo','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>WinMo</value>
                </dataitem>
            </taskdata>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (2,'DefaultTranscode','Default content transcoding workflow','Transcode','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
        <dependency>GetAssetInfo</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>GetAssetInfo</id>
      <class>GetAssetInfoTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (81,'TranscodeRTSP','RTSP transcode workflow','TranscodeRTSP','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>RTSP</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (121,'DefaultAudioIngest','Default audio-only content ingestion workflow','IngestAudioContent','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>XCodeAudioSetup</id>
            <class>XCodeAudioSetup</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeAudioSetup</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (161,'TranscodeProgressiveDownload','Progressive Download transcode workflow','TranscodeProgressiveDownload','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>ProgressiveDownload</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (1,'DefaultIngest','Default content ingestion workflow','IngestContent','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
        <dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
        <dependency>XCodePreflight</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (201,'TranscodePCTV','PCTV transcode workflow','TranscodePCTV','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>PCTV</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (141,'RegenerateProviderSMILDocuments','Default SMIL document regeneration workflow','RegenerateProviderSMILDocuments','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>RegenerateProviderSMILDocuments</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>RegenerateProviderSMILDocuments</id>
            <class>RegenerateProviderSMILDocumentsTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (181,'IngestWithAds','Default ingestion workflow with ads','IngestContent','<?xml version="1.0"
  encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
<dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
<dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
<dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
<dependency>XCodePreflight</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');
Insert into WFM.WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (182,'TranscodeWithAds','Default transcode workflow with ads','Transcode','<?xml version="1.0"
  encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
<dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
<dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
<dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
<dependency>XCodePreflight</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');

REM Create Provider account for use with Continuous Integration testing
REM INSERTING into WFM.WFM_PROVIDER_CONTEXT
Insert into WFM.WFM_PROVIDER_CONTEXT (ID,PRIORITY,CONTENT_AUTH_MODE,TRANSCODING_CLUSTER,ADVERTISING_SOURCE) values (2,500,'NotRequired','simulator',null);

REM INSERTING into WFM.WFM_PROVIDER_ENCODING
Insert into WFM.WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,21);
Insert into WFM.WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,23);
Insert into WFM.WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,41);
Insert into WFM.WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,61);

REM INSERTING into WFM.WFM_PROVIDER_WORKFLOW
Insert into WFM.WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (2,1);


REM Adding new column 'ENCRYPTION_REQUIRED' into WFM.WFM_PROVIDER_CONTEXT
ALTER TABLE WFM.WFM_PROVIDER_CONTEXT add ENCRYPTION_REQUIRED VARCHAR2(16) DEFAULT 'TRUE';


commit;
quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


