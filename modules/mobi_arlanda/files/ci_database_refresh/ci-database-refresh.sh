#!/bin/sh

# File: ci-database-refresh.sh
# Author: Scott Kidder <skidder@mobitv.com>
# Date: 16 Nov 2011
# Desc: Removes and installs all Arlanda databases for the testing on the 
#       Continuous Integration host.
#

# Refresh database for each Arlanda component
for i in dam wfm mp ingestor activemq licensemanager scheduler; do
  echo "Refreshing ${i}"
  /bin/su - oracle -c /var/arlanda/${i}_sql/remove.sh
  /bin/su - oracle -c /var/arlanda/${i}_sql/install.sh
done
