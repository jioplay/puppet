#!/bin/bash
# Remove the DAM database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) delete users and tablespace for DAM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 1_drop_dam.sql

set echo off
set feed off
set pages 0

drop user dam cascade;
drop user dam_user cascade;
drop tablespace dam_data including contents;
drop tablespace dam_index including contents;
drop role dam_role;

quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


