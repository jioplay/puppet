#!/bin/bash
# Install the DAM database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) Create users and tablespace for DAM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 2_cr_dam_users.sql

set echo off
set feed off
set pages 0

def sid = 'prdjb';

CREATE TABLESPACE  dam_data
    LOGGING 
    DATAFILE '/data2/oradata/&sid/dam_data01.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

CREATE TABLESPACE  dam_index
    LOGGING 
    DATAFILE '/data2/oradata/&sid/dam_index1.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;
--------------------------------------------------------------------------------------------------------

-- dam schema
-- the dam schema will own all the "dam" objects

CREATE USER dam  PROFILE "DEFAULT" 
    IDENTIFIED BY dam 
    DEFAULT TABLESPACE dam_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to dam;
grant resource to dam;
alter user dam quota unlimited on dam_data;
alter user dam quota unlimited on dam_index;
--------------------------------------------------------------------------------------------------------

-- dam role
-- grant object privs to this role
create role dam_role;

--------------------------------------------------------------------------------------------------------
-- dam_user user
-- the dam_user will be used by the dam application to connect to the database
-- the dam_user will only own synonyms to the dam schema objects.

CREATE USER dam_user  PROFILE "DEFAULT" 
    IDENTIFIED BY dam_user 
    DEFAULT TABLESPACE dam_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to dam_user;
grant dam_role to dam_user;
alter user dam_user quota unlimited on dam_data;
alter user dam_user quota unlimited on dam_index;
quit
EOF

# (3) Create schema objects for DAM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 3_cr_dam_schema_objects.sql

set echo off
set feed off
set pages 0

DROP TABLE dam.DAM_ASSET CASCADE CONSTRAINTS;
DROP TABLE dam.DAM_ADDITIONAL_METADATA CASCADE CONSTRAINTS;
DROP TABLE dam.DAM_ASSET_INSTANCE CASCADE CONSTRAINTS;
DROP TABLE dam.DAM_CONFIG CASCADE CONSTRAINTS;
DROP TABLE dam.DAM_TRACK CASCADE CONSTRAINTS;

CREATE TABLE  dam.DAM_ASSET 
   (    "ASSET_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "TYPE" VARCHAR2(255 CHAR) NOT NULL ENABLE, 
    "AVAILABLE" NUMBER(1,0), 
    "CORRELATION_ID" VARCHAR2(1024 CHAR), 
    "CRID" VARCHAR2(1024 CHAR) NOT NULL ENABLE, 
    "DOWNLOAD_ATTEMPTS" NUMBER(10,0), 
    "EXPIRED" NUMBER(1,0), 
    "EXPIRES" TIMESTAMP (6), 
    "GENRE" VARCHAR2(1024 CHAR), 
    "GROUP_ID" NUMBER(10,0), 
    "INVENTORY_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "KEYWORD" VARCHAR2(1024 CHAR), 
    "LAST_MODIFIED" TIMESTAMP (6), 
    "PROVIDER_ID" NUMBER(10,0) NOT NULL ENABLE, 
    "PUBLISHED" NUMBER(1,0), 
    "SHORT_TITLE" VARCHAR2(512 CHAR), 
    "START_OF_AVAILABILITY" TIMESTAMP (6), 
    "STATE" NUMBER(10,0), 
    "SYNOPSIS" VARCHAR2(1024 CHAR), 
    "TITLE" VARCHAR2(1024 CHAR), 
    "TRASHED" NUMBER(1,0), 
    "UPLOAD_DATE" TIMESTAMP (6), 
    "VERSION" NUMBER(10,0) NOT NULL ENABLE,
    "PRE_ROLL_INVENTORY_ID" NUMBER(19, 0),
    "POST_ROLL_INVENTORY_ID" NUMBER(19, 0),
    "MEDIA_RESTRICTIONS" VARCHAR2(40 BYTE) DEFAULT 'm2g'
   )
/
alter table dam.dam_asset
add constraint pk_dam_asset
primary key (asset_id)
using index tablespace dam_index;

CREATE TABLE  dam.DAM_ADDITIONAL_METADATA 
   (    "METADATA_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "METAKEY" VARCHAR2(255 CHAR) NOT NULL ENABLE, 
    "METAVALUE" VARCHAR2(1024 CHAR), 
    "ASSET_ID" NUMBER(19,0) NOT NULL ENABLE
   )
/
alter table dam.dam_additional_metadata
add constraint pk_dam_additiona_metadata
primary key (metadata_id)
using index tablespace dam_index;

alter table dam.dam_additional_metadata
add constraint fk_dam_additiona_metadata_01
foreign key (asset_id) references dam.dam_asset(asset_id);

create index dam.idx_dam_additional_metadata_01 on dam_additional_metadata(asset_id)
tablespace dam_index;

CREATE TABLE  dam.DAM_ASSET_INSTANCE 
   (    "INSTANCE_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "BITRATE" FLOAT(126), 
    "DURATION" FLOAT(126), 
    "ENCODING_ID" NUMBER(10,0) NOT NULL ENABLE, 
    "FORMAT" VARCHAR2(24 CHAR),
    "INSTANCE_LINKTYPE" VARCHAR2(255 CHAR), 
    "IS_MBR" NUMBER(1,0), 
    "IS_ORIGINAL" NUMBER(1,0), 
    "PATH" VARCHAR2(1024 CHAR), 
    "SIZE_" FLOAT(126), 
    "ASSET_ID" NUMBER(19,0) NOT NULL ENABLE,
    "TYPE" VARCHAR2(20),
    "CHECKSUM" VARCHAR2(100)
   )
/
alter table dam.DAM_ASSET_INSTANCE
add constraint pk_DAM_ASSET_INSTANCE
primary key (INSTANCE_ID)
using index tablespace dam_index;

alter table dam.DAM_ASSET_INSTANCE
add constraint fk_DAM_ASSET_INSTANCE_01
foreign key (asset_id) references dam.dam_asset(asset_id);

create index dam.idx_DAM_ASSET_INSTANCE_01 on dam.DAM_ASSET_INSTANCE(asset_id)
tablespace dam_index;

CREATE TABLE  dam.DAM_CONFIG 
   (    "CONFIG_KEY" VARCHAR2(255 CHAR) NOT NULL ENABLE, 
    "CONFIG_VALUE" VARCHAR2(1024 CHAR), 
    "DEFAULT_CONFIG_VALUE" VARCHAR2(1024 CHAR), 
    "DESCRIPTION" VARCHAR2(2048 CHAR), 
    "LAST_MODIFIED" TIMESTAMP (6)
   )
/
alter table dam.DAM_CONFIG
add constraint pk_DAM_CONFIG
primary key (CONFIG_KEY)
using index tablespace dam_index;

CREATE TABLE  dam.DAM_TRACK 
   (    "TRACK_ID" NUMBER(19,0) NOT NULL ENABLE, 
    "BITRATE" FLOAT(126), 
    "BITSPERSAMPLE" NUMBER(10,0), 
    "CHANNELS" NUMBER(10,0), 
    "CODEC" VARCHAR2(255 CHAR), 
    "DURATION" FLOAT(126), 
    "FMTP" VARCHAR2(255 CHAR), 
    "FRAMERATE" FLOAT(126), 
    "HEIGHT" NUMBER(10,0), 
    "PAYLOAD" VARCHAR2(255 CHAR), 
    "SAMPLERATE" FLOAT(126), 
    "SIZE_" NUMBER(19,0), 
    "START_" FLOAT(126), 
    "TYPE" VARCHAR2(255 CHAR), 
    "WIDTH" NUMBER(10,0), 
    "INSTANCE_ID" NUMBER(19,0) NOT NULL ENABLE
   )
/

alter table dam.DAM_TRACK
add constraint pk_DAM_TRACK
primary key (TRACK_ID)
using index tablespace dam_index;

alter table dam.DAM_TRACK
add constraint fk_DAM_TRACK_01
foreign key (INSTANCE_ID) references dam.dam_asset_instance(INSTANCE_ID);

create index dam.idx_DAM_TRACK_01 on DAM_TRACK(INSTANCE_ID)
tablespace dam_index;


DROP SEQUENCE dam.DAM_ASSET_TRACK_SEQ;
DROP SEQUENCE dam.DAM_ASSET_SEQ;
DROP SEQUENCE dam.DAM_ASSET_METADATA_SEQ;
DROP SEQUENCE dam.DAM_ASSET_INSTANCE_SEQ;

 CREATE SEQUENCE   dam."DAM_ASSET_TRACK_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
/
 CREATE SEQUENCE   dam."DAM_ASSET_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
/
 CREATE SEQUENCE   dam."DAM_ASSET_METADATA_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
/
 CREATE SEQUENCE   dam."DAM_ASSET_INSTANCE_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
/


quit
EOF




# (4) Create quartz objects for DAM
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 4_cr_dam_quartz_objects.sql

set echo off
set feed off
set pages 0

delete from dam.dam_qrtz_job_listeners;
delete from dam.dam_qrtz_trigger_listeners;
delete from dam.dam_qrtz_fired_triggers;
delete from dam.dam_qrtz_simple_triggers;
delete from dam.dam_qrtz_cron_triggers;
delete from dam.dam_qrtz_blob_triggers;
delete from dam.dam_qrtz_triggers;
delete from dam.dam_qrtz_job_details;
delete from dam.dam_qrtz_calendars;
delete from dam.dam_qrtz_paused_trigger_grps;
delete from dam.dam_qrtz_locks;
delete from dam.dam_qrtz_scheduler_state;

drop table dam.dam_qrtz_calendars;
drop table dam.dam_qrtz_fired_triggers;
drop table dam.dam_qrtz_trigger_listeners;
drop table dam.dam_qrtz_blob_triggers;
drop table dam.dam_qrtz_cron_triggers;
drop table dam.dam_qrtz_simple_triggers;
drop table dam.dam_qrtz_triggers;
drop table dam.dam_qrtz_job_listeners;
drop table dam.dam_qrtz_job_details;
drop table dam.dam_qrtz_paused_trigger_grps;
drop table dam.dam_qrtz_locks;
drop table dam.dam_qrtz_scheduler_state;


CREATE TABLE dam.dam_qrtz_job_details
  (
    JOB_NAME  VARCHAR2(80) NOT NULL,
    JOB_GROUP VARCHAR2(80) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    JOB_CLASS_NAME   VARCHAR2(128) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    IS_STATEFUL VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP)
);
CREATE TABLE dam.dam_qrtz_job_listeners
  (
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    JOB_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
	REFERENCES dam.dam_qrtz_JOB_DETAILS(JOB_NAME,JOB_GROUP)
);
CREATE TABLE dam.dam_qrtz_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    JOB_NAME  VARCHAR2(80) NOT NULL, 
    JOB_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(80) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (JOB_NAME,JOB_GROUP) 
	REFERENCES dam.dam_qrtz_JOB_DETAILS(JOB_NAME,JOB_GROUP) 
);
CREATE TABLE dam.dam_qrtz_simple_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(7) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES dam.dam_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE dam.dam_qrtz_cron_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    CRON_EXPRESSION VARCHAR2(80) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES dam.dam_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE dam.dam_qrtz_blob_triggers
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
        REFERENCES dam.dam_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE dam.dam_qrtz_trigger_listeners
  (
    TRIGGER_NAME  VARCHAR2(80) NOT NULL, 
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    TRIGGER_LISTENER VARCHAR2(80) NOT NULL,
    PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER),
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES dam.dam_qrtz_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE dam.dam_qrtz_calendars
  (
    CALENDAR_NAME  VARCHAR2(80) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (CALENDAR_NAME)
);
CREATE TABLE dam.dam_qrtz_paused_trigger_grps
  (
    TRIGGER_GROUP  VARCHAR2(80) NOT NULL, 
    PRIMARY KEY (TRIGGER_GROUP)
);
CREATE TABLE dam.dam_qrtz_fired_triggers 
  (
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(80) NULL,
    JOB_GROUP VARCHAR2(80) NULL,
    IS_STATEFUL VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    PRIMARY KEY (ENTRY_ID)
);
CREATE TABLE dam.dam_qrtz_scheduler_state 
  (
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    PRIMARY KEY (INSTANCE_NAME)
);
CREATE TABLE dam.dam_qrtz_locks
  (
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    PRIMARY KEY (LOCK_NAME)
);
create index idx_qrtz_j_req_recovery on dam.dam_qrtz_job_details(REQUESTS_RECOVERY);
create index dam.idx_qrtz_t_next_fire_time on dam.dam_qrtz_triggers(NEXT_FIRE_TIME);
create index dam.idx_qrtz_t_state on dam.dam_qrtz_triggers(TRIGGER_STATE);
create index dam.idx_qrtz_t_nft_st on dam.dam_qrtz_triggers(NEXT_FIRE_TIME,TRIGGER_STATE);
create index dam.idx_qrtz_t_volatile on dam.dam_qrtz_triggers(IS_VOLATILE);
create index dam.idx_qrtz_ft_trig_name on dam.dam_qrtz_fired_triggers(TRIGGER_NAME);
create index dam.idx_qrtz_ft_trig_group on dam.dam_qrtz_fired_triggers(TRIGGER_GROUP);
create index dam.idx_qrtz_ft_trig_nm_gp on dam.dam_qrtz_fired_triggers(TRIGGER_NAME,TRIGGER_GROUP);
create index dam.idx_qrtz_ft_trig_volatile on dam.dam_qrtz_fired_triggers(IS_VOLATILE);
create index dam.idx_qrtz_ft_trig_inst_name on dam.dam_qrtz_fired_triggers(INSTANCE_NAME);
create index dam.idx_qrtz_ft_job_name on dam.dam_qrtz_fired_triggers(JOB_NAME);
create index dam.idx_qrtz_ft_job_group on dam.dam_qrtz_fired_triggers(JOB_GROUP);
create index dam.idx_qrtz_ft_job_stateful on dam.dam_qrtz_fired_triggers(IS_STATEFUL);
create index dam.idx_qrtz_ft_job_req_recovery on dam.dam_qrtz_fired_triggers(REQUESTS_RECOVERY);


quit
EOF




# (5) Grant privileges to DAM user
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 5_grant.sql

set echo off
set feed off
set pages 0

def param_grantee = 'dam_user';
def param_owner = 'dam';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_tab is
        select table_name, owner
        from all_tables
        where owner = UPPER('&param_owner');
cursor get_vw is
        select view_name, owner
        from all_views
        where owner = UPPER('&param_owner');

cursor get_seq is
        select sequence_name, sequence_owner
        from all_sequences
        where sequence_owner = UPPER('&param_owner');


BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

FOR tab_rec in get_tab LOOP

        l_sql := 'grant select, insert, update, delete on ' || tab_rec.owner || '.' || tab_rec.table_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

FOR vw_rec in get_vw LOOP

        l_sql := 'grant select on ' || vw_rec.owner || '.' || vw_rec.view_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        begin
           result := dbms_sql.execute(cursor_id);
        end;

END LOOP;

FOR seq_rec in get_seq LOOP

        l_sql := 'grant select on ' || seq_rec.sequence_owner || '.' || seq_rec.sequence_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (6) Drop DAM Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 6_drop_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'dam_user';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select synonym_name,owner
        from dba_synonyms
        where owner = UPPER('&param_user');

BEGIN

cursor_id:=dbms_sql.open_cursor;

dbms_output.put_line('Starting drop syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'drop synonym ' || obj_rec.owner || '.' || obj_rec.synonym_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (7) Create DAM Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 7_cr_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'dam_user';
def param_schema = 'dam';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select object_name, owner
        from all_objects
        where owner = UPPER('&param_schema')
        and object_type in
        ('TABLE', 'VIEW', 'CLUSTER', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE')
--      and object_name not in
--      (select synonym_name
--      from all_synonyms
--      where owner = upper('&param_user'))
        ;

BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

dbms_output.put_line('Starting create syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'create synonym ' || '&param_user' || '.' || obj_rec.object_name || ' for ' || obj_rec.owner || '.' || obj_rec.object_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (8) Seed the DAM database with configuration data.
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 8_seed_dam.sql

set echo off
set feed off
set pages 0

REM INSERTING into DAM.DAM_CONFIG
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.instance.smil.naming','/var/Jukebox/vod/smil/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','/var/Jukebox/vod/smil/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','Path and naming convention to where SMIL asset instances should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${VERSION}      - The version
    \${SUFFIX}       - The suffix of the file',to_timestamp('22-APR-09 04.08.37.236000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('business.search.method','solr','solr','If ''solr'', then the SOLR search server will be used for business search queries.  If ''dam'', then the DAM
    will perform all business search queries agains the relational database.',to_timestamp('22-OCT-09 04.53.14.113000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('search.server.url','http://jusearch:8080/mobi-search/arlanda','http://jusearch:8080/mobi-search/arlanda','The URL to the SOLR search server.  Must be specified if using the ''solr'' business.search.method.',to_timestamp('22-OCT-09 04.53.14.177000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('apply.winmo.pctv.namingpatch','true','true','If true DAM creates symlink named inventoryId.\${extension} to the latest version of an instance having
    encodingId 100, 101 and 102. true or false.',to_timestamp('09-AUG-08 12.01.56.888000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('service.stalled.ftp','1','1','This value desides when an ftp transport is concidered to be stalled. I.e the state is transporting and
	lastModified > this value. The number is in hours.',to_timestamp('09-AUG-08 12.01.56.909000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('remove.deadassets.job','0 0 8 * * ?','0 0 8 * * ?','This job permenantly removes any Assets that are concidered dead.',to_timestamp('19-NOV-10 02.55.55.129000000 PM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('monitor.event.directory','/opt/ReachTV/Farm/monitor','/opt/ReachTV/Farm/monitor','The directory to where the DAM should write its monitor event files. Since only one node writes the event files
    this directory should be a shared directory (SAN).',to_timestamp('09-AUG-08 12.01.56.913000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('assetexpired.job','0 0/10 * * * ?','0 * * * * ?','When executed, the DAM will find any Assets that has expired since last time this job ran,
    concecvently the more infrekvent this job runs the more inexact the extarnal systems will
    be informed of the expiration of the Asset.',to_timestamp('30-AUG-08 03.44.22.242000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('service.job','0 0/5 * * * ?','0 0/5 * * * ?','Interval when the DAM should run service jobs.',to_timestamp('09-AUG-08 12.01.56.916000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.instance.link.type','move','move','How DAM is going to move instance files into the asset repository, this can be supplied as a parameter
    when adding an Asset, if not DAM fallbacks to this configured value. Valid values are: copy, move, hardlink
    or softlink. This also applies to original instances.',to_timestamp('09-AUG-08 12.01.56.917000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('max.download.attempts','3','3','This value desides how many times DAM tries to download an Asset before it is concidered as an error.',to_timestamp('09-AUG-08 12.01.56.919000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('fetchassets.job','0 * * * * ?','0 * * * * ?','Upon indirect upload, the DAM tries to download the Asset from a remote location.
    If for some reason the remote server is unavailable. The Asset is put in a waiting
    state. When this jub runs the DAM tries to any Assets that not has been downloaded.',to_timestamp('09-AUG-08 12.01.56.921000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('max.concurrent.downloads','100','200','This value desides how many concurrent downloads from remote site on DAM node can have.',to_timestamp('01-DEC-10 08.45.58.377000000 PM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.topic.ttl','43200000','43200000','The time in milliseconds on how long a message sent to ASSET.TOPIC should be stored
    until it expires by the jmsbroker.',to_timestamp('09-AUG-08 12.01.56.925000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.original.naming','/var/Jukebox/uploads/originals/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','/var/Jukebox/uploads/originals/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','Path and naming convention to where asset original files should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${GROUP_ID}     - The group id
    \${VERSION}      - The version
    \${SUFFIX}       - The suffix of the file',to_timestamp('09-AUG-08 12.01.56.926000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('original.metadata.naming','/var/Jukebox/uploads/metadata/\${PROVIDER_ID}/\${INVENTORY_ID}-\${VERSION}.xml','/var/Jukebox/uploads/metadata/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.xml','Path and naming convention to where original meta data files (if provided) should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${GROUP_ID}     - The group id
    \${VERSION}      - The version',to_timestamp('09-AUG-08 12.01.56.928000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('dead.asset','3','7','This value desides when an Asset is concidered as dead. I.e. it should be expired, 
    and lastModified > this value. The number is in days.',to_timestamp('06-SEP-08 12.59.40.126000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('publishassets.job','0 * * * * ?','0 * * * * ?','When this job is triggered the DAM finds all Assets that are ready to be published
    (since last time this job was triggered) an publish them.',to_timestamp('09-AUG-08 12.01.56.932000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.instance.naming','/var/Jukebox/vod/encoding-\${ENCODING_ID}/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','/var/Jukebox/vod/encoding-\${ENCODING_ID}/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','Path and naming convention to where asset instances should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${GROUP_ID}     - The group id
    \${VERSION}      - The version
    \${ENCODING_ID}  - The encoding id
    \${SUFFIX}       - The suffix of the file',to_timestamp('09-AUG-08 12.01.56.934000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('version.removal','1','1','When an Asset is replaced, the DAM waits until the duration of the Asset + this value (in minutes) has
    gone before removing the old version permanently from disk.',to_timestamp('09-AUG-08 12.01.56.938000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('monitor.event.job','0 0/30 * * * ?','0 0/5 * * * ?','Interval in minutes between when the DAM should write its monitor event files.',to_timestamp('30-AUG-08 03.45.10.158000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('jmx.hibernate.stats','true','false','Boolean true / false if Hibernate JMX stat bean should be used, only applicable if Hibernate is used as JPA provider.
    Changing this conf parameter will need a restart before changes take effect.',to_timestamp('09-AUG-08 12.01.56.941000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('max.request.threads','10','10','The maximum simultainusly threads DAM can use for handling JMS messages',to_timestamp('31-AUG-08 09.02.29.094000000 PM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into DAM.DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.original.thumbnail.naming','/var/Jukebox/uploads/thumbnails/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','/var/Jukebox/uploads/thumbnails/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','Path and naming convention to where asset thumbnail original files should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${GROUP_ID}     - The group id
    \${VERSION}      - The version
    \${SUFFIX}       - The suffix of the file',to_timestamp('22-OCT-08 04.32.18.734000000 AM','DD-MON-RR HH.MI.SS.FF AM'));









REM INSERTING into DAM.DAM_QRTZ_JOB_DETAILS
Insert into DAM.DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('AssetExpiredJob','dam',null,'com.mobitv.arlanda.dam.scheduler.AssetExpiredJob','0','0','0','1');
Insert into DAM.DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('FetchAssetsJob','dam',null,'com.mobitv.arlanda.dam.scheduler.FetchAssetsJob','0','0','0','1');
Insert into DAM.DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('PublishAssetsJob','dam',null,'com.mobitv.arlanda.dam.scheduler.PublishAssetJob','0','0','0','1');
Insert into DAM.DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('RemoveDeadAssetsJob','dam',null,'com.mobitv.arlanda.dam.scheduler.RemoveDeadAssetsJob','0','0','0','1');
Insert into DAM.DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('ServiceJob','dam',null,'com.mobitv.arlanda.dam.scheduler.ASMServiceJob','0','0','0','1');


REM INSERTING into DAM.DAM_QRTZ_LOCKS
Insert into DAM.DAM_QRTZ_LOCKS (LOCK_NAME) values ('CALENDAR_ACCESS');
Insert into DAM.DAM_QRTZ_LOCKS (LOCK_NAME) values ('JOB_ACCESS');
Insert into DAM.DAM_QRTZ_LOCKS (LOCK_NAME) values ('MISFIRE_ACCESS');
Insert into DAM.DAM_QRTZ_LOCKS (LOCK_NAME) values ('STATE_ACCESS');
Insert into DAM.DAM_QRTZ_LOCKS (LOCK_NAME) values ('TRIGGER_ACCESS');




REM INSERTING into DAM.DAM_QRTZ_TRIGGERS
Insert into DAM.DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('RemoveDeadAssetsJobTrigger','dam ','RemoveDeadAssetsJob','dam','0',null,1303542000000,1303455600000,5,'WAITING','CRON',1290151231000,0,null,0);
Insert into DAM.DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('AssetExpiredJobTrigger','dam','AssetExpiredJob','dam','0',null,1303495740000,1303495680000,5,'WAITING','CRON',1218236517000,0,null,0);
Insert into DAM.DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('FetchAssetsJobTrigger','dam','FetchAssetsJob','dam','0',null,1303495740000,1303495680000,5,'WAITING','CRON',1218236517000,0,null,0);
Insert into DAM.DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('PublishAssetsJobTrigger','dam','PublishAssetsJob','dam','0',null,1303495740000,1303495680000,5,'WAITING','CRON',1218236517000,0,null,0);
Insert into DAM.DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('ServiceJobTrigger','dam','ServiceJob','dam','0',null,1303495800000,1303495500000,5,'WAITING','CRON',1218236517000,0,null,0);


REM INSERTING into DAM.DAM_QRTZ_CRON_TRIGGERS
Insert into DAM.DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('RemoveDeadAssetsJobTrigger','dam ','0 0 7 * * ?','UTC');
Insert into DAM.DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('AssetExpiredJobTrigger','dam','0 * * * * ?','Europe/London');
Insert into DAM.DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('FetchAssetsJobTrigger','dam','0 * * * * ?','Europe/London');
Insert into DAM.DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('PublishAssetsJobTrigger','dam','0 * * * * ?','Europe/London');
Insert into DAM.DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('ServiceJobTrigger','dam','0 0/5 * * * ?','Europe/London');


REM Adding new column 'ENCRYPTION_REQUIRED' into DAM.DAM_ASSET
ALTER TABLE DAM.DAM_ASSET add ENCRYPTION_REQUIRED VARCHAR2(16) DEFAULT 'DEFAULT';

quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


