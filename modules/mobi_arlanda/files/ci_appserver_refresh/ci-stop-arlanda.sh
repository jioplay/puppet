#!/bin/sh

# File: ci-stop-arlanda.sh
# Author: Scott Kidder <skidder@mobitv.com>
# Date: 16 Nov 2011
# Desc: Stops Arlanda services on the Continuous Integration host.
#

# Stop Services
puppet apply -e "include arlanda::profiles::mobi-arlanda-brick-stop"
