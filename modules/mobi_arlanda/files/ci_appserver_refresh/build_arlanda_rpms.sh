#!/bin/sh

# File: build-arlanda-rpms.sh
# Author: Scott Kidder <skidder@mobitv.com>
# Date: 16 Nov 2011
# Desc: Triggers regeneration of all Arlanda RPMs using the build factory.
#

# Use 5.5.0 branch of ActiveMQ
echo "Building: mobi-activemq"
curl -s "http://build-08.mobitv.corp/cgi-bin/server.py?COMPONENT=mobi-activemq&BRANCH=5.5.0" > /dev/null

# Build each of the Arlanda components from main-line
echo "Building: arlanda-commons"
curl -s "http://build-08.mobitv.corp/cgi-bin/module.py?MODULE=arlanda-commons&BRANCH=main" > /dev/null

echo "Building: Jukebox"
curl -s "http://build.corp/cgi-bin/server.py?COMPONENT=mobi-arlanda-jukebox&BRANCH=main" > /dev/null

for i in\
 mobi-arlanda-dam\
 mobi-arlanda-mp\
 mobi-arlanda-wfm\
 mobi-arlanda-playlistcomposer\
 mobi-arlanda-scheduler\
 mobi-arlanda-search\
 mobi-arlanda-searchagent\
 mobi-arlanda-tools-eesimulator\
 mobi-arlanda-ftplogparser\
 mobi-arlanda-licensemanager\
 mobi-arlanda-ingestor\
 mobi-arlanda-fm\
 mobi-arlanda-drmp\
 ; do

  echo "Building: ${i}"
  curl -s "http://build-08.mobitv.corp//cgi-bin/server.py?COMPONENT=${i}&BRANCH=main" > /dev/null
done
