#!/bin/sh

# File: ci-upgrade-arlanda.sh
# Author: Scott Kidder <skidder@mobitv.com>
# Date: 16 Nov 2011
# Desc: Stops, upgrades, and then starts Arlanda services on the 
#       Continuous Integration host.
#

# Stop Services
puppet apply -e "include arlanda::profiles::mobi-arlanda-brick-stop"

# Delete logs
rm -rf /var/log/arlanda/*

# Upgrade and Start Services
puppet apply -e "include arlanda::profiles::mobi-arlanda-brick"

