#!/bin/bash
# Install the MP database environment.
# This script must be run as user oracle.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Scott Kidder, 11/2/2011

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Based on "procedure_exp_imp_MOBI2.txt" by Dan Vieira. Modified his
# instructions to pull import/export scripts and dumpfiles from central 
# web server, rather than from the active DB on vmx12a.db.dmz

# (1) set the shell environment
shopt -s expand_aliases
. ~/.bash_profile
MOBI2

# (2) Create users and tablespace for MP
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 2_cr_mp_users.sql

set echo off
set feed off
set pages 0

def sid = 'prdjb';

CREATE TABLESPACE  mp_data
    LOGGING 
    DATAFILE '/data2/oradata/&sid/mp_data01.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

CREATE TABLESPACE  mp_index
    LOGGING 
    DATAFILE '/data2/oradata/&sid/mp_index1.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;
--------------------------------------------------------------------------------------------------------

-- mp schema
-- the mp schema will own all the "mp" objects

CREATE USER mp  PROFILE "DEFAULT" 
    IDENTIFIED BY mp  
    DEFAULT TABLESPACE mp_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to mp;
grant resource to mp;
alter user mp quota unlimited on mp_data;
alter user mp quota unlimited on mp_index;
--------------------------------------------------------------------------------------------------------

-- mp role
-- grant object privs to this role
create role mp_role;

--------------------------------------------------------------------------------------------------------
-- mp_user user
-- the mp_user will be used by the mp application to connect to the database
-- the mp_user will only own synonyms to the mp schema objects.

CREATE USER mp_user  PROFILE "DEFAULT" 
    IDENTIFIED BY mp_user 
    DEFAULT TABLESPACE mp_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to mp_user;
grant mp_role to mp_user;
alter user mp_user quota unlimited on mp_data;
alter user mp_user quota unlimited on mp_index;

quit
EOF

# (3) Create schema objects for MP
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 3_cr_mp_schema_objects.sql

set echo off
set feed off
set pages 0


DROP TABLE mp.MP_CONFIG CASCADE CONSTRAINTS;
DROP TABLE mp.MP_ENCODER_MAPPING CASCADE CONSTRAINTS;

CREATE TABLE  mp.MP_CONFIG
   (    "CONFIG_KEY" VARCHAR2(255 CHAR) NOT NULL ENABLE, 
    "CONFIG_VALUE" LONG, 
    "DEFAULT_CONFIG_VALUE" VARCHAR2(1024 CHAR), 
    "DESCRIPTION" VARCHAR2(2048 CHAR), 
    "LAST_MODIFIED" TIMESTAMP (6), 
     PRIMARY KEY ("CONFIG_KEY") ENABLE
   )
/
CREATE TABLE  mp.MP_ENCODER_MAPPING
   (    "ENCODING_ID" NUMBER(10,0) NOT NULL ENABLE, 
    "CLASS_NAME" VARCHAR2(1024 CHAR), 
    "DESCRIPTION" VARCHAR2(1024 CHAR), 
    "FILE_FORMAT" VARCHAR2(24 CHAR), 
    "FILE_NAME" VARCHAR2(512 CHAR), 
     PRIMARY KEY ("ENCODING_ID") ENABLE
   )
/

quit
EOF




# (5) Grant privileges to MP user
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 5_grant.sql

set echo off
set feed off
set pages 0

def param_grantee = 'mp_user';
def param_owner = 'mp';


DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_tab is
        select table_name, owner
        from all_tables
        where owner = UPPER('&param_owner');
cursor get_vw is
        select view_name, owner
        from all_views
        where owner = UPPER('&param_owner');

cursor get_seq is
        select sequence_name, sequence_owner
        from all_sequences
        where sequence_owner = UPPER('&param_owner');


BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

FOR tab_rec in get_tab LOOP

        l_sql := 'grant select, insert, update, delete on ' || tab_rec.owner || '.' || tab_rec.table_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

FOR vw_rec in get_vw LOOP

        l_sql := 'grant select on ' || vw_rec.owner || '.' || vw_rec.view_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        begin
           result := dbms_sql.execute(cursor_id);
        end;

END LOOP;

FOR seq_rec in get_seq LOOP

        l_sql := 'grant select on ' || seq_rec.sequence_owner || '.' || seq_rec.sequence_name || ' to &param_grantee';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (6) Drop MP Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 6_drop_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'mp_user';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select synonym_name,owner
        from dba_synonyms
        where owner = UPPER('&param_user');

BEGIN

cursor_id:=dbms_sql.open_cursor;

dbms_output.put_line('Starting drop syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'drop synonym ' || obj_rec.owner || '.' || obj_rec.synonym_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (7) Create MP Synonyms
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 7_cr_syn.sql

set echo off
set feed off
set pages 0

def param_user = 'mp_user';
def param_schema = 'mp';

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_obj is
        select object_name, owner
        from all_objects
        where owner = UPPER('&param_schema')
        and object_type in
        ('TABLE', 'VIEW', 'CLUSTER', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE')
--      and object_name not in
--      (select synonym_name
--      from all_synonyms
--      where owner = upper('&param_user'))
        ;

BEGIN

cursor_id:=dbms_sql.open_cursor;

/* Tables first */

dbms_output.put_line('Starting create syn...');

FOR obj_rec in get_obj LOOP

        l_sql := 'create synonym ' || '&param_user' || '.' || obj_rec.object_name || ' for ' || obj_rec.owner || '.' || obj_rec.object_name;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

quit
EOF



# (8) Seed the MP database with configuration data.
export ORACLE_SID=prdjb
sqlplus -s / as sysdba <<EOF > 8_seed_mp.sql

set echo off
set feed off
set pages 0

REM INSERTING into MP.MP_CONFIG
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.link.mode','symlink','symlink','This specifies how files should be copied for encoding into the Episode encoding folder.
     Two values are supported "copy" or "symlink". "copy" is slower, but usefull when lack of
     shared disk.',to_timestamp('09-AUG-08 12.05.23.441000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('setting.repository','/var/Jukebox/mp/setting-repository','/var/Jukebox/mp/setting-repository','The location where MP stores encoding setting files. Make sure MP has read/write
    privs in this dir.',to_timestamp('09-AUG-08 12.05.23.460000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('max.request.threads','10','1000','The maximum simultainusly threads MP can use for encoding requests / responses (JMS and WS).',to_timestamp('24-MAR-09 12.25.29.168000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.outfolder.root','/var/Jukebox/ee/','/var/Jukebox/ee/media/Output/','The root outfolder of Episode Engine. Make sure MP has read / write privs in this dir.',to_timestamp('09-AUG-08 12.05.23.463000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.default.outpath','mp','/var/Jukebox/ee/media/Output/mp/','If no outpath is provided in EncodingRequest, EpisodeEngineEncoder encodes files into this directory.
    must end with a / character. IMPORTANT, this dir needs to be accessably by Episode Engine.',to_timestamp('09-AUG-08 12.05.23.464000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.save.meta.data','false','false','If the meta data about every encoding files from Episode should be stored.',to_timestamp('09-AUG-08 12.05.23.465000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.cleanup','true','true','If the input and outpufolder shold be deleted after encoding batch been completed
    should always be true, but for debug it may sometimes be usefull',to_timestamp('07-MAR-11 07.02.47.114000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.watchfolder.root','/var/Jukebox/ee/','/var/Jukebox/ee/media/Input/','The root watchfolder of Episode Engine. Make sure MP has read / write privs in this dir.',to_timestamp('09-AUG-08 12.05.23.467000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('ffmpeg.location','/usr/local/bin/ffmpeg','/usr/local/bin/ffmpeg','The location of where metadata files should store encoding.',to_timestamp('09-AUG-08 12.05.23.469000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('metadata.repository','/var/Jukebox/mp/metadata-repository','/var/Jukebox/mp/metadata-repository','The location of where metadata files should store encoding.',to_timestamp('09-AUG-08 12.05.23.470000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.watchfolder.input.foldername','Input','Input','The name of the input directory relative to each cluster.',to_timestamp('17-AUG-10 05.05.55.474000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.clusters','media,simulator','media','A comma-separated list of the names of EE clusters.',to_timestamp('11-AUG-11 04.01.25.210000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.episode.watchfolder.output.foldername','Output','Output','The name of the output directory relative to each cluster.',to_timestamp('17-AUG-10 05.05.55.527000000 AM','DD-MON-RR HH.MI.SS.FF AM'));
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('clip.combination.method','ee','mp','The method of applying pre-roll/post-roll clips (default is EE, anything else uses MP)',to_timestamp('03-JUN-09 10.19.14.884000000 PM','DD-MON-RR HH.MI.SS.FF AM'));


Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.widevine.input.root','/var/Jukebox/widevine/Input','/var/Jukebox/widevine/Input','The input base directory for input to the Widevine Packager ',CURRENT_TIMESTAMP);
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.widevine.output.root','/var/Jukebox/widevine/Output','/var/Jukebox/widevine/Output','The output base directory for output from the Widevine Packager ',CURRENT_TIMESTAMP);
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.widevine.status.to.url','http://juarnvip:8080/mp/widevine_response','http://juarnvip:8080/mp/widevine_response','The URL that the Widevine Packager should use to send notifications about packaging operations',CURRENT_TIMESTAMP);
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.widevine.packager.url','http://wvpkgrvip/widevine/voddealer/cgi-bin/packagenotify.cgi','http://wvpkgrvip/widevine/voddealer/cgi-bin/packagenotify.cgi','The URL of the Widevine Packager to submit packaging requests',CURRENT_TIMESTAMP);
<<<<<<< HEAD
Insert into MP.MP_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('encoder.widevine.copy.control','77B','77B','The copy-control flags to pass to Widevine for DRM-protected content',CURRENT_TIMESTAMP);
=======
>>>>>>> 8e7739f... JIRA:ARN-2081: Update Arlanda Components to allow use with MySQL Database


REM INSERTING into MP.MP_ENCODER_MAPPING
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (1,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-1.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (2,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-2.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (3,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-3.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (4,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-4.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (5,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-5.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (6,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-6.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (7,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-7.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (8,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-8.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (11,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-11.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (12,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-12.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (13,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-13.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (14,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-14.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (15,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-15.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (18,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-18.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (21,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-21.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (22,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-22.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (23,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-23.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (24,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-24.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (25,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-25.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (26,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-26.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (27,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-27.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (28,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-28.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (40,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-40.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (41,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-41.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (60,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-60.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (61,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-61.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (62,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-62.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (64,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-64.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (65,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-65.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (66,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-66.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (67,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-67.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (68,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-68.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (69,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-69.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (70,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-70-2.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (71,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-71.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (72,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-72.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (73,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-73.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (74,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-74.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (75,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-75.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (77,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-77.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (78,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-78.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (79,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-79.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (91,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-91.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (93,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-93.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (94,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-94.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (95,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','encoding-95.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (100,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'wmv','encoding-100.mbrsetting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (101,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'wmv','encoding-101.mbrsetting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (102,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'wmv','encoding-102.mbrsetting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (103,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','fourthreemaster.setting');
Insert into MP.MP_ENCODER_MAPPING (ENCODING_ID,CLASS_NAME,DESCRIPTION,FILE_FORMAT,FILE_NAME) values (104,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',null,'3gp','sixteenninemaster.setting');


quit
EOF


# What are these for?
set ver on
set head on
set feed on
set pages 24


