class mobi_arlanda::jutra::install {
    include(mobi_arlanda::appbase)
    include (mobi_arlanda::jukebox::install)
    include (mobi_arlanda::jutra::start)

    Class["mobi_arlanda::appbase"] ->
    Class["mobi_arlanda::jukebox::install"] ->
    Class["mobi_arlanda::jutra::start"]

  }
