class mobi_arlanda::jutra::stop {

    service { "contentd":
      ensure => stopped,
    }

    service { "ingestd":
      ensure => stopped,
    }

    service { "playlistd":
      ensure => stopped,
    }

}
