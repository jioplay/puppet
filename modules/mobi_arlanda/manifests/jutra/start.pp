class mobi_arlanda::jutra::start {

    service { "contentd":
    ensure => running,
      enable => true,
    }

    service { "ingestd":
    ensure => running,
      enable => true,
    }

    service { "playlistd":
    ensure => running,
      enable => true,
    }

    notify { "JUTRAStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

    Notify['JUTRAStarted'] ->
    Service["contentd"] ->
    Service["ingestd"] ->
    Service["playlistd"]

  }
