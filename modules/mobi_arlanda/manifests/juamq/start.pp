class mobi_arlanda::juamq::start {

    service { "activemq":
      ensure => running,
      enable => true,
    }

  notify { "JUAMQStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

  Notify['JUAMQStarted'] ->
  Service['activemq']
}
