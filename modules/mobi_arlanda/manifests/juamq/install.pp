class mobi_arlanda::juamq::install {
    include(mobi_arlanda::appbase)
    include(mobi_arlanda::juamq::start)

    package { "ActiveMQ":
    ensure => $mobi_arlanda::rpmversions::mobi_activemq_version,
      name => "mobi-activemq",
      provider => yum,
      notify   => Service["activemq"],
    }

    file { "/opt/activemq/conf/activemq.xml":
      replace => true,
      owner  => "rtv",
      group  => "rtv",
      mode => "444",
      source => "puppet:///modules/mobi_arlanda/juamq_files/activemq.xml",
      notify   => Service["activemq"],
    }

    Class['mobi-arlanda::appbase'] ->
    Package["ActiveMQ"] ->
    File["/opt/activemq/conf/activemq.xml"] ->
    Class['mobi-arlanda::juamq::start']
  }
