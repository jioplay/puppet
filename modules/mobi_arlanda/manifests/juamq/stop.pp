class mobi_arlanda::juamq::stop {

    service { "activemq":
      ensure => stopped,
    }

}
