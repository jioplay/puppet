class mobi_arlanda::tomcat::stop {

    service { "tomcat":
      ensure => stopped,
      enable => false,
    }

}
