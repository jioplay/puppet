class mobi_arlanda::tomcat::install {

    package {"Tomcat Heapsize Increased":
    ensure => $mobi_arlanda::rpmversions::mobi_tomcat_heapsize_increased_version,
      name => "mobi-tomcat-heapsize-increased",
      provider => yum,
    }

    exec { "Patch Tomcat Heap-Size Config":
      command => "/bin/sed -i -e 's|Xmx1024m|Xmx1536m|' /opt/tomcat/bin/catalina.sh",
      onlyif => "/usr/bin/test `/bin/grep 'Xmx1024m' /opt/tomcat/bin/catalina.sh | wc -l` -eq 1",
    }

    exec { "Patch Tomcat Permgen-Size Config":
      command => "/bin/sed -i -e 's|MaxPermSize=128m|MaxPermSize=256m|' /opt/tomcat/bin/catalina.sh",
      onlyif => "/usr/bin/test `/bin/grep 'MaxPermSize=128m' /opt/tomcat/bin/catalina.sh | wc -l` -eq 1",
    }

    Package["Tomcat Heapsize Increased"] ->
    Exec["Patch Tomcat Heap-Size Config"] ->
    Exec["Patch Tomcat Permgen-Size Config"]
  }
