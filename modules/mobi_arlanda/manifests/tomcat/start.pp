class mobi_arlanda::tomcat::start {

    service { 'tomcat':
      enable => true,
      ensure => running,
    hasrestart => true,
      hasstatus  => true,
    }

}
