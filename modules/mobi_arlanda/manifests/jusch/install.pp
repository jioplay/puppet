class mobi_arlanda::jusch::install {
    include(mobi_arlanda::appbase)
    include(mobi_arlanda::tomcat::install)
    include(mobi_arlanda::jusch::start)

    package {"Scheduler":
    ensure => $scheduler_version,
      name => "mobi-arlanda-scheduler",
      provider => yum,
    }

    file { "/opt/tomcat/conf/Catalina/localhost/scheduler.xml":
      replace => true,
      source => "puppet:///modules/mobi_arlanda/jusch_files/scheduler-eng.xml",
    }

    Class['mobi-arlanda::appbase'] ->
    Class['mobi-arlanda::tomcat::install'] ->
    Package["Scheduler"] ->
    File["/opt/tomcat/conf/Catalina/localhost/scheduler.xml"] ->
    Class['mobi-arlanda::jusch::start']
  }
