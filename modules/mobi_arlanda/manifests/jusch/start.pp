class mobi_arlanda::jusch::start {
  include(mobi_arlanda::tomcat::start)

  notify { "JUSCHStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

  Notify['JUSCHStarted'] ->
  Class['mobi-arlanda::tomcat::start']
}
