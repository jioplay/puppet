# == Class: mobi_arlanda::database
#
#  installs database component
#
# === Parameters:
#
#   $delete_database_backups::
#
# === Requires:
#
#     oracle
#
# === Sample Usage
#
#  class { "mobi_arlanda::database" :
#           delete_database_backups => "true",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda::database (
    $delete_database_backups = $mobi_arlanda::database::params::delete_database_backups,
)
inherits mobi_arlanda::database::params {
    include mobi_arlanda::database::install
    anchor { "mobi_arlanda::database::begin":} -> Class["mobi_arlanda::database::install"] ->
    anchor { "mobi_arlanda::database::end": }
    os::motd::register { $name : }
}

