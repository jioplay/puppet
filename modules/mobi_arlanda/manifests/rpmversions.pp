class mobi_arlanda::rpmversions{
    #Listed in order should be installed

    #TOMCAT
    $jdk_version = "1.6.0_06-fcs"
    $mobi_jce_version = "6.0.0-96669"
    $tomcat_native_version = "1.1.12-1.el5"
    $mobi_tomcat_heapsize_increased_version = "6.0.16.1-170413"

    #JUAMQ
    $mobi_activemq_version = latest

    #JUING

    #JUARN
    $mobi_arlanda_dam_version = latest #
    $mobi_arlanda_wfm_version = latest #
    $mobi_arlanda_mp_version = latest #
    $mobi_arlanda_fragfileprocessor_version = latest #

    #JUDRM
    $mobi_arlanda_licensemanager_version = latest #

    #Jukebox
    $mobi_libcurl_compat_version = "1.0-0"
    $apr_version = "0.9.4-24.5"
    $apr_util_version = "0.9.4-21"
    $libxml2_version = "2.6.16-10"
    $zlib_version = "1.2.1.2-1.2"
    $curl_version = "7.12.1-11.el4"
    $sox_version = "12.17.4-4.3"
    $t1lib_version = "5.0.0-1"
    $oracle_instant_client_version = "10.1.0.4-6"
    $php_version = "5.2.0-56224"
    $httpd_version = "2.0.52-32.ent"
    $mobi_arlanda_jukebox_version = latest
  }
