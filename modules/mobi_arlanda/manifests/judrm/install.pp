class mobi_arlanda::judrm::install {
  include(mobi_arlanda::appbase)
    include(mobi_arlanda::tomcat::install)
    include(mobi_arlanda::judrm::start)

    package {"DRMP":
      ensure => $mobi_arlanda::rpmversions::drmp_version,
      name => "mobi-arlanda-drmp",
      provider => yum,
      notify   => Service["tomcat"],
    }

    package {"License Manager":
      ensure => $mobi_arlanda::rpmversions::mobi_arlanda_licensemanager_version,
      name => "mobi-arlanda-licensemanager",
      provider => yum,
      notify   => Service["tomcat"],
    }

    file { "/opt/tomcat/conf/Catalina/localhost/licensemanager.xml":
      replace => true,
    owner  => "rtv",
      group  => "rtv",
      mode => "444",
      source => "puppet:///modules/mobi_arlanda/judrm_files/licensemanager-eng.xml",
    }

  Class['mobi-arlanda::appbase'] ->
  Class['mobi-arlanda::tomcat::install'] ->
  Package["License Manager"] ->
  File["/opt/tomcat/conf/Catalina/localhost/licensemanager.xml"] ->
  Class['mobi-arlanda::judrm::start']
  }
