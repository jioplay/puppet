class mobi_arlanda::judrm::start {
    include(mobi_arlanda::tomcat::start)

  notify { "JUDRMStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

  Notify['JUDRMStarted'] ->
  Class['mobi-arlanda::tomcat::start']
}
