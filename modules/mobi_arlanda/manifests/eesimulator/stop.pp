class mobi_arlanda::eesimulator::stop {

    service { "eesimulator":
      ensure => stopped,
    }

}
