class mobi_arlanda::eesimulator::start {

    service { "eesimulator":
      ensure => running,
      enable => true,
    }

  notify { "EESStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

  Notify['EESStarted'] ->
  Service["eesimulator"]

}
