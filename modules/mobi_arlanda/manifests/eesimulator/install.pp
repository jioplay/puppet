class mobi_arlanda::eesimulator::install {
    include(mobi_arlanda::appbase)
    include(mobi_arlanda::eesimulator::start)

    package { "EE Simulator":
    ensure => $mobi_arlanda::rpmversions::mobi_arlanda_tools_eesimulator_version,
      name => "mobi-arlanda-tools-eesimulator",
      provider => yum,
    }

    Class['mobi-arlanda::appbase'] ->
    Package["EE Simulator"] ->
    Class[mobi-arlanda::eesimulator::start]
  }
