class mobi_arlanda::rtv {

    require(group::arlanda)
    require(group::rtv)
    require("user::rtv")

    exec { 'Install RTV User RPM':
      command => "/bin/rpm -Uvh http://build.corp/gold_masters/mobi-archive/mobi-user-rtv-1.2.1-2680.i686.rpm",
      onlyif => "/usr/bin/test `/bin/rpm -qa mobi-user-rtv | wc -l` -eq 0",
    }
  }
