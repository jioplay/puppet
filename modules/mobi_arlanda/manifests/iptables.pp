class mobi_arlanda::iptables {
  file { "/etc/sysconfig/iptables":
      replace => true,
      owner  => "rtv",
      group  => "rtv",
      mode => "444",
      source => "puppet:///modules/mobi_arlanda/iptables_files/iptables",
    }

  service { "iptables":
      ensure => running,
      enable => true,
    }

  File['/etc/sysconfig/iptables'] ->
  Service['iptables']
}