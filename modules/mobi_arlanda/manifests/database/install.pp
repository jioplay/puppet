class mobi_arlanda::database::install {

  file { "/var/arlanda":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
  }


## DAM
  file { "/var/arlanda/dam_sql":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/dam_sql",
  }

  exec { "Remove DAM Database":
    command => "/bin/su - oracle -c /var/arlanda/dam_sql/remove.sh",
    subscribe => File["/var/arlanda/dam_sql"],
    refreshonly => true,
  }

  exec { "Install DAM Database":
    command => "/bin/su - oracle -c /var/arlanda/dam_sql/install.sh",
    subscribe => Exec["Remove DAM Database"],
    refreshonly => true,
  }


## License Manager
  file { "/var/arlanda/licensemanager_sql":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/licensemanager_sql",
  }

  exec { "Remove LICENSEMANAGER Database":
    command => "/bin/su - oracle -c /var/arlanda/licensemanager_sql/remove.sh",
    subscribe => File["/var/arlanda/licensemanager_sql"],
    refreshonly => true,
  }

  exec { "Install LICENSEMANAGER Database":
    command => "/bin/su - oracle -c /var/arlanda/licensemanager_sql/install.sh",
    subscribe => Exec["Remove LICENSEMANAGER Database"],
    refreshonly => true,
  }


### Workflow Manager
  file { "/var/arlanda/wfm_sql":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/wfm_sql",
  }

  exec { "Remove WFM Database":
    command => "/bin/su - oracle -c /var/arlanda/wfm_sql/remove.sh",
    subscribe => File["/var/arlanda/wfm_sql"],
    refreshonly => true,
  }

  exec { "Install WFM Database":
    command => "/bin/su - oracle -c /var/arlanda/wfm_sql/install.sh",
    subscribe => Exec["Remove WFM Database"],
    refreshonly => true,
  }


### Media Processor
  file { "/var/arlanda/mp_sql":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/mp_sql",
  }

  exec { "Remove MP Database":
    command => "/bin/su - oracle -c /var/arlanda/mp_sql/remove.sh",
    subscribe => File["/var/arlanda/mp_sql"],
    refreshonly => true,
  }

  exec { "Install MP Database":
    command => "/bin/su - oracle -c /var/arlanda/mp_sql/install.sh",
    subscribe => Exec["Remove MP Database"],
    refreshonly => true,
  }


### ActiveMQ
  file { "/var/arlanda/activemq_sql":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/activemq_sql",
  }

  exec { "Remove ActiveMQ Database":
    command => "/bin/su - oracle -c /var/arlanda/activemq_sql/remove.sh",
    subscribe => File["/var/arlanda/activemq_sql"],
    refreshonly => true,
  }

  exec { "Install ActiveMQ Database":
    command => "/bin/su - oracle -c /var/arlanda/activemq_sql/install.sh",
    subscribe => Exec["Remove ActiveMQ Database"],
    refreshonly => true,
  }

### Scheduler
  file { "/var/arlanda/scheduler_sql":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/scheduler_sql",
  }

  exec { "Remove Scheduler Database":
    command => "/bin/su - oracle -c /var/arlanda/scheduler_sql/remove.sh",
    subscribe => File["/var/arlanda/scheduler_sql"],
    refreshonly => true,
  }

  exec { "Install Scheduler Database":
    command => "/bin/su - oracle -c /var/arlanda/scheduler_sql/install.sh",
    subscribe => Exec["Remove Scheduler Database"],
    refreshonly => true,
  }


### Ingestor
  file { "/var/arlanda/ingestor_sql":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/ingestor_sql",
  }

  exec { "Remove Ingestor Database":
    command => "/bin/su - oracle -c /var/arlanda/ingestor_sql/remove.sh",
    subscribe => File["/var/arlanda/ingestor_sql"],
    refreshonly => true,
  }

  exec { "Install Ingestor Database":
    command => "/bin/su - oracle -c /var/arlanda/ingestor_sql/install.sh",
    subscribe => Exec["Remove Ingestor Database"],
    refreshonly => true,
  }


### Mobi2
  file { "/var/arlanda/mobi2_sql":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/mobi2_sql",
  }

  exec { "Install Mobi2 Database":
    command => "/bin/su - oracle -c /var/arlanda/mobi2_sql/install.sh",
    subscribe => File["/var/arlanda/mobi2_sql"],
    refreshonly => true,
  }



### CI Refresh Scripts
  file { "/var/arlanda/ci_database_refresh":
    ensure => "directory",
    owner  => "oracle",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/ci_database_refresh",
  }


### Oracle Archive Deletion CRON job
  file { "/etc/cron.daily/delete_oracle_backups.sh":
    ensure => "directory",
    owner  => "root",
    group  => "root",
    mode   => 775,
    recurse => "true",
    source => "puppet:///modules/mobi_arlanda/delete_oracle_backups.sh",
  }



}
