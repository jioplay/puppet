class mobi_arlanda::filesystem {
    file { "/var/Jukebox":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox/vod":
    ensure => "directory",
      owner  => "root",
      group  => "arlanda",
      mode   => "4775",
    }

    file { "/var/Jukebox/uploads":
    ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }

    file { "/var/Jukebox/uploads/metadata":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "4775",
    }

    file { "/var/Jukebox/uploads/originals":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "4775",
    }

    file { "/var/Jukebox/uploads/search_transform":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "4775",
    }

    file { "/var/Jukebox/uploads/smil":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "4775",
    }

    file { "/var/Jukebox/uploads/thumbnails":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "4775",
    }

    file { "/var/Jukebox/uploads/xslt":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "4775",
    }

    file { "/var/Jukebox/local":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox/logs":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox/logs/contentd":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox/monitor":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "0775",
    }

    file { "/opt/ReachTV":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/opt/ReachTV/Farm":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }

    file { "/opt/ReachTV/Farm/monitor":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "0775",
    }

    file { "/var/Jukebox/ftpupload":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "0755",
    }

    file { "/var/Jukebox/extfeeds":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "0755",
    }

    file { "/var/Jukebox/tmp":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox/cache":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox/socks":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox/SCL":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }

    file { "/var/Jukebox/ee":
    ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }

    file { "/var/Jukebox/ee/simulator":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }


    file { "/var/Jukebox/ee/simulator/Input":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }


    file { "/var/Jukebox/ee/simulator/Output":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }


    file { "/var/Jukebox/ee/media":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }


    file { "/var/Jukebox/ee/media/Input":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }


    file { "/var/Jukebox/ee/media/Output":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }


    file { "/var/Jukebox/ee/media/Output/mp":
    ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }


    file { "/var/log/vsftpd.log":
    ensure => "present",
      owner  => "root",
      group  => "root",
      mode   => "0666",
    }


    file { "/var/log/ReachTV":
    ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }


    file { "/var/log/ReachTV/Farm":
    ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }


    file { "/opt/arlanda":
    ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }


    file { "/opt/arlanda/dam":
    ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }


    file { "/opt/arlanda/dam/monitor":
    ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }


    file { "/opt/arlanda/dam/scripts":
    ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }


    file { "/var/Jukebox/mp":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "4775",
    }


    file { "/var/Jukebox/mp/metadata-repository":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "4775",
    }

    file { "/var/Jukebox/mp/setting-repository":
    ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "4775",
      recurse => true,
      source => "puppet:///modules/mobi_arlanda/setting-repository",
    }

    # Automatically copy puppet scripts onto the machine
    file { "/etc/puppet/modules":
    ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0777",
      #  recurse => true,
      #  source => "puppet:///modules",
    }

    file { "/var/Jukebox/ftpupload/ci_test":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0775",
    }

    file { "/var/www":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0775",
    }

    file { "/var/www/html":
    ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0775",
    }

    file { "/var/www/html/redbar.mp4":
    ensure => "file",
      owner  => "apache",
      group  => "apache",
      mode   => "0664",
      source => "puppet:///modules/mobi_arlanda/ci_appserver_refresh/redbar.mp4",
    }

  }
