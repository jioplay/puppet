class mobi_arlanda::playlistcomposer::stop {

    service { "playlistcomposer":
      ensure => stopped,
    }

}
