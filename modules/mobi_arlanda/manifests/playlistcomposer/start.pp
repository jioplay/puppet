class mobi_arlanda::playlistcomposer::start {

    service { "playlistcomposer":
      ensure => running,
      enable => true,
    }

  notify { "PLCStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

  Notify['PLCStarted'] ->
  Service['playlistcomposer']
}
