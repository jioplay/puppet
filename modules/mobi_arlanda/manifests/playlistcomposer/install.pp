class mobi_arlanda::playlistcomposer::install {
    include(mobi_arlanda::appbase)
    include(mobi_arlanda::playlistcomposer::start)

    package {"Playlist Composer":
    ensure => $mobi_arlanda::rpmversions::mobi_arlanda_playlistcomposer_version,
      name => "mobi-arlanda-playlistcomposer",
      provider => yum,
    }

    file { "/opt/arlanda/conf/playlistcomposer/channel.properties":
      replace => true,
      source => "puppet:///modules/mobi_arlanda/playlistcomposer_files/channel-eng.properties",
    }

    Class['mobi-arlanda::appbase'] ->
    Package["Playlist Composer"] ->
    File["/opt/arlanda/conf/playlistcomposer/channel.properties"] ->
    Class[mobi-arlanda::playlistcomposer::start]
  }
