class mobi_arlanda::juing::start {
    include(mobi_arlanda::tomcat::start)

    service { "searchagent":
    ensure => running,
      enable => true,
    }

    service { "ftplogparser":
    ensure => running,
      enable => true,
    }

    service { "vsftpd":
    ensure => running,
      enable => true,
    }

    service { "httpd":
    ensure => running,
      enable => true,
    }

    notify { "JUINGStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

    Notify['JUINGStarted'] ->
    Service["httpd"] ->
    Service["vsftpd"] ->
    Service["searchagent"] ->
    Service["ftplogparser"] ->
    Class['mobi-arlanda::tomcat::start']
  }
