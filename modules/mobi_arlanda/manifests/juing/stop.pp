class mobi_arlanda::juing::stop {

    include(mobi_arlanda::tomcat::stop)

    service { "searchagent":
      ensure => stopped,
    }

    service { "ftplogparser":
      ensure => stopped,
    }

    service { "vsftpd":
      ensure => stopped,
    }

    service { "httpd":
      ensure => stopped,
    }

}
