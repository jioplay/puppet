class mobi_arlanda::juing::install {
    include(mobi_arlanda::appbase)
    include(mobi_arlanda::jukebox::install)
    include(mobi_arlanda::tomcat::install)
    include(mobi_arlanda::juing::start)
    include(mobi_arlanda::jukebox_syslog)

    package {"Ingestor":
    ensure => $mobi_arlanda::rpmversions::ingestor_version,
      name => "mobi-arlanda-ingestor",
      provider => yum,
    }

    package {"Solr Search":
    ensure => $mobi_arlanda::rpmversions::solr_search_version,
      name => "mobi-arlanda-search",
      provider => yum,
    }

    package {"Search Agent":
    ensure => $mobi_arlanda::rpmversions::searchagent_version,
      name => "mobi-arlanda-searchagent",
      provider => yum,
    }

    package {"FTP Log Parser":
    ensure => $mobi_arlanda::rpmversions::ftplogparser_version,
      name => "mobi-arlanda-ftplogparser",
      provider => yum,
    }

    ### FUNCTIONAL_TESTING - enable the Java FTPLogParser to handle indirect upload for the provider ci_test so that the Java Ingestor will perform the indirect upload.
    ### If this was left as NFL_test, then the PHP FTPHandler would handle the indirect upload for the provider ci_test so that the PHP Ingestd would perform the indirect upload.
    if $domain == "oak1.mobitv" {

        file { "/opt/arlanda/conf":
        ensure => "directory",
          owner  => "rtv",
          group  => "rtv",
          mode   => 664,
        }

        file { "/opt/arlanda/conf/ftplogparser":
        ensure => "directory",
          owner  => "rtv",
          group  => "rtv",
          mode   => 664,
        }

        file { "/opt/arlanda/conf/ftplogparser/ftplogparser.properties":
          replace => true,
        ensure => "file",
          owner  => "rtv",
          group  => "rtv",
          mode   => 664,
          source => "puppet:///modules/mobi_arlanda/juing_files/ftplogparser-eng.properties",
        }

        file { "/opt/tomcat/conf/Catalina":
        ensure => "directory",
          owner  => "rtv",
          group  => "rtv",
          mode   => 664,
        }

        file { "/opt/tomcat/conf/Catalina/localhost":
        ensure => "directory",
          owner  => "rtv",
          group  => "rtv",
          mode   => 664,
        }

        file { "/opt/tomcat/conf/Catalina/localhost/ingestor.xml":
          replace => true,
        ensure => "file",
          owner  => "rtv",
          group  => "rtv",
          mode   => 664,
          source => "puppet:///modules/mobi_arlanda/juing_files/ingestor-eng.xml",
        }

        Package["Ingestor"] ->
        File["/opt/arlanda/conf"] ->
        File["/opt/arlanda/conf/ftplogparser"] ->
        File["/opt/arlanda/conf/ftplogparser/ftplogparser.properties"] ->
        File["/opt/tomcat/conf/Catalina"] ->
        File["/opt/tomcat/conf/Catalina/localhost"] ->
        File["/opt/tomcat/conf/Catalina/localhost/ingestor.xml"] ->
        Class[mobi-arlanda::juing::start]
      }

      Class['mobi-arlanda::appbase'] ->
  Class['mobi-arlanda::jukebox::install'] ->
  Class['mobi-arlanda::tomcat::install'] ->
  Package["FTP Log Parser"] ->
  Package["Solr Search"] ->
  Package["Search Agent"] ->
  Package["Ingestor"] ->
  Class["mobi_arlanda::juing::start"]
    }
