class mobi_arlanda::jukebox_syslog {

    package { syslog-ng:
      ensure => present,
      name => "syslog-ng",
      provider => yum,
    }

    file { "/etc/syslog-ng/syslog-ng.conf":
      ensure => "file",
      owner  => "root",
      group  => "root",
      mode   => 644,
      source => "puppet:///modules/mobi_arlanda/jukebox_syslog/syslog-ng.conf",
      notify => Service["syslog-ng"],
    }

    service { syslog-ng:
      ensure => "running",
      enable => "true",
  }

}
