class mobi_arlanda::juarn::start {
  include(mobi_arlanda::tomcat::start)

  notify { "JUARNStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

  Notify['JUARNStarted'] ->
  Class['mobi-arlanda::tomcat::start']
}
