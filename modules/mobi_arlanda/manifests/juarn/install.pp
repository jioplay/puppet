class mobi_arlanda::juarn::install {
  include(mobi_arlanda::appbase)
    include(mobi_arlanda::tomcat::install)
    include(mobi_arlanda::juarn::start)

    package { "DAM":
      ensure => $mobi_arlanda::rpmversions::mobi_arlanda_dam_version,
      name => "mobi-arlanda-dam",
      provider => yum,
      notify   => Service["tomcat"],
    }

    package { "WFM":
      ensure => $mobi_arlanda::rpmversions::mobi_arlanda_wfm_version,
      name => "mobi-arlanda-wfm",
      provider => yum,
      notify   => Service["tomcat"],
    }

    package { "MP":
      ensure => $mobi_arlanda::rpmversions::mobi_arlanda_mp_version,
      name => "mobi-arlanda-mp",
      provider => yum,
      notify   => Service["tomcat"],
    }

    package { "Folder Manager":
      ensure => $mobi_arlanda::rpmversions::mobi_arlanda_fm_version,
      name => "mobi-arlanda-fm",
      provider => yum,
      notify   => Service["tomcat"],
    }

  file { "/opt/tomcat/conf/Catalina/localhost/dam.xml":
    replace => true,
    owner  => "rtv",
    group  => "rtv",
    mode => "444",
    source => "puppet:///modules/mobi_arlanda/juarn_files/dam-eng.xml",
      notify   => Service["tomcat"],
  }

  file { "/opt/tomcat/conf/Catalina/localhost/wfm.xml":
    replace => true,
    owner  => "rtv",
    group  => "rtv",
    mode => "444",
    source => "puppet:///modules/mobi_arlanda/juarn_files/wfm-eng.xml",
      notify   => Service["tomcat"],
  }

  file { "/opt/tomcat/conf/Catalina/localhost/mp.xml":
    replace => true,
    owner  => "rtv",
     group  => "rtv",
    mode => "444",
    source => "puppet:///modules/mobi_arlanda/juarn_files/mp-eng.xml",
      notify   => Service["tomcat"],
  }

  file { "/opt/tomcat/conf/Catalina/localhost/foldermanager.xml":
    replace => true,
    owner  => "rtv",
    group  => "rtv",
    mode => "444",
    source => "puppet:///modules/mobi_arlanda/juarn_files/foldermanager-eng.xml",
      notify   => Service["tomcat"],
  }

  Class['mobi-arlanda::appbase'] ->
  Class['mobi-arlanda::tomcat::install'] ->
  Package["MP"] ->
  Package["WFM"] ->
  Package["DAM"] ->
  Package["Folder Manager"] ->
  File["/opt/tomcat/conf/Catalina/localhost/mp.xml"] ->
  File["/opt/tomcat/conf/Catalina/localhost/wfm.xml"] ->
  File["/opt/tomcat/conf/Catalina/localhost/dam.xml"] ->
  File["/opt/tomcat/conf/Catalina/localhost/foldermanager.xml"] ->
  Class['mobi-arlanda::juarn::start']
}
