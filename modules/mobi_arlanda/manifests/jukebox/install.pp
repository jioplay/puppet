class mobi_arlanda::jukebox::install {
    exec { 'Uninstall PHP PEAR on Juing':
      command => "/bin/rpm -e --nodeps php-pear",
      onlyif => "/usr/bin/test `/bin/rpm -qa php-pear | wc -l` -gt 0",
    }

    package {"mobi-libcurl-compat":
    ensure => $mobi_arlanda::rpmversions::mobi_libcurl_compat_version,
      name => "mobi-libcurl-compat",
      provider => yum,
    }

    package {"sox":
    ensure => $mobi_arlanda::rpmversions::sox_version,
      name => "sox",
      provider => yum,
    }

    package {"t1lib":
    ensure => $mobi_arlanda::rpmversions::t1lib_version,
      name => "t1lib",
      provider => yum,
    }

    package {"Oracle Instant Client":
    ensure => $mobi_arlanda::rpmversions::oracle_instant_client_version,
      name => "oracle_instant_client",
      provider => yum,
    }

    package {"libxml2":
    ensure => $mobi_arlanda::rpmversions::libxml2_version,
      name => "libxml2",
      provider => yum,
    }

    package {"libxml2-python":
    ensure => $mobi_arlanda::rpmversions::libxml2_version,
      name => "libxml2-python",
      provider => yum,
    }

    package {"libxml2-devel":
    ensure => $mobi_arlanda::rpmversions::libxml2_version,
      name => "libxml2-devel",
      provider => yum,
    }

    # exec { "libxml2-devel":
    #   command => "/bin/rpm --replacepkgs --force -Uvh http://10.172.97.42/redhat/rhes4.5/RedHat/RPMS/libxml2-devel-2.6.16-10.i386.rpm",
    #   onlyif => "/usr/bin/test `/bin/rpm -qa libxml2-devel | grep libxml2-devel-2.6.16-10 | wc -l` -eq 0",
    # }

    package {"zlib":
    ensure => $mobi_arlanda::rpmversions::zlib_version,
      name => "zlib",
      provider => yum,
    }

    package {"zlib-devel":
    ensure => $mobi_arlanda::rpmversions::zlib_version,
      name => "zlib-devel",
      provider => yum,
    }

    package {"APR":
    ensure => $mobi_arlanda::rpmversions::apr_version,
      name => "apr",
      provider => yum,
    }

    package {"APR_Util":
    ensure => $mobi_arlanda::rpmversions::apr_util_version,
      name => "apr-util",
      provider => yum,
    }

    package {"curl":
    ensure => $mobi_arlanda::rpmversions::curl_version,
      name => "curl",
      provider => yum,
    }

    package {"HTTPD":
    ensure => $mobi_arlanda::rpmversions::httpd_version,
      name => "httpd",
      provider => yum,
    }

    exec { "PHP":
      command => "/bin/rpm --replacepkgs --force -Uvh http://build.corp/gold_masters/thirdparty/php-5.2.0-56224.i686.rpm",
      onlyif => "/usr/bin/test `/bin/rpm -qa php | grep php-5.2.0-56224 | wc -l` -eq 0",
    }

    package {"Jukebox":
    ensure => $mobi_arlanda::rpmversions::mobi_arlanda_jukebox_version,
      name => "mobi-arlanda-jukebox",
      provider => yum,
    }

    Exec['Uninstall PHP PEAR on Juing'] ->
    Package["sox"] ->
    Package["t1lib"] ->
    Package["mobi-libcurl-compat"] ->
    Package["zlib"] ->
    Package["zlib-devel"] ->
    Package["libxml2"] ->
    Package["libxml2-python"] ->
    Package["libxml2-devel"] ->
    Package["curl"] ->
    Package["Oracle Instant Client"] ->
    Package["HTTPD"] ->
    Exec["PHP"] ->
    Package["Jukebox"]
  }
