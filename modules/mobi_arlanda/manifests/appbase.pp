class mobi_arlanda::appbase {
    include(user::apache)
    include(mobi_arlanda::rtv)
    include(os::git-branch-commit)
    include(mobi_arlanda::create_yum_repo)
    include(mobi_arlanda::appserverhosts)
    include(mobi_arlanda::filesystem)
    include(mobi_arlanda::filesystem_ci)
    include(mobi_arlanda::iptables)
    include(mobi_arlanda::rpmversions)
    include(mobi_arlanda::java)

    notify { "AppBaseStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

    notify { "AppBaseFinished": } # Used to make sure anything with a dependency on this class will run all the include resources first

    Notify['AppBaseStarted'] ->
    Class['user::apache'] ->
    Class['mobi-arlanda::rtv'] ->
    Class["os::git-branch-commit"] ->
    Class['mobi-arlanda::create_yum_repo'] ->
    Class['mobi-arlanda::appserverhosts'] ->
    Class['mobi-arlanda::filesystem'] ->
    Class['mobi-arlanda::filesystem_ci'] ->
    Class['mobi-arlanda::iptables'] ->
    Class['mobi-arlanda::rpmversions'] ->
    Class['mobi-arlanda::java'] ->
    Notify['AppBaseFinished']
  }
