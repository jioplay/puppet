# Class: passenger
#
# This class installs passenger
#
# Parameters:
#
# Actions:
#   - Install passenger rpm
#
# Requires:
#
# Sample Usage:
#
class passenger::install {
    package { $::passenger::package:
        ensure  => $::passenger::package_ensure,
        require => Package["httpd"],
    }
}
