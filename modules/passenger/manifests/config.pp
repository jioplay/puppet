# Class: passenger
#
# This class installs passenger
#
# Parameters:
#
# Actions:
#   - install passenger config file
#
# Requires:
#   - passenger::install
#
# Sample Usage:
#
class passenger::config {
  file { '/etc/httpd/conf.d/passenger.conf':
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    content => template("passenger/passenger.conf.erb"),
    require => Class["passenger::install"],
  }
}
