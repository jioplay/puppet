# Class: passenger::params
#
# This class manages parameters for the Passenger module
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class passenger::params {

  ### install.pp
  $package = "mod_passenger"
  $package_ensure = present
}
