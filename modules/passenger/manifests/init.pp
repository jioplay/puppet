# Class: passenger
#
# This class installs passenger
#
# Parameters:
#
# Actions:
#   - Install passenger package
#
# Requires:
#
# Sample Usage:
#
class passenger (

  $package = $passenger::params::package,
  $package_ensure = $passenger::params::package_ensure,

) inherits passenger::params {

  anchor { "passenger::begin": } ->
  class { "passenger::install": } ->
  class { "passenger::config": } ->
  anchor { "passenger::end": } ->

  # include module name in motd
  os::motd::register { "passenger": }
}
