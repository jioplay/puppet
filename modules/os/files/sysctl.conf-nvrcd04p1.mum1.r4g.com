# Kernel sysctl configuration file for Red Hat Linux
#
# For binary values, 0 is disabled, 1 is enabled.  See sysctl(8) and
# sysctl.conf(5) for more details.

# Controls IP packet forwarding
net.ipv4.ip_forward = 0

# Controls source route verification
# Enabling reverse path filtering
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.lo.rp_filter = 1
net.ipv4.conf.all.rp_filter = 1

# Do not accept source routing
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.conf.lo.accept_source_route = 0

# Controls the System Request debugging functionality of the kernel
kernel.sysrq = 0

# Controls whether core dumps will append the PID to the core filename
# Useful for debugging multi-threaded applications
kernel.core_uses_pid = 1

# Controls the use of TCP syncookies
net.ipv4.tcp_syncookies = 1

# http://utcc.utoronto.ca/~cks/space/blog/linux/SemSysctlExplained
kernel.sem = 1034 132352 1034 128

# Controls the maximum size of a message, in bytes
kernel.msgmnb = 65536

# Controls the default maxmimum size of a mesage queue
kernel.msgmax = 65536

# Controls the maximum shared segment size, in bytes
kernel.shmmax = 68719476736

# Controls the maximum number of shared memory segments, in pages
kernel.shmall = 4294967296

# MobiTV:
# Network: Disable IPv6
#net.ipv6.conf.all.disable_ipv6 = 1
#net.ipv6.conf.default.disable_ipv6 = 1
#net.ipv6.conf.lo.disable_ipv6 = 1
# Network: tuning
net.core.netdev_max_backlog=2500
net.core.rmem_max=1024000
net.core.wmem_max=1024000
net.core.rmem_default=262144
net.core.wmem_default=262144
net.core.optmem_max=40960
#net.ipv4.tcp_rmem=262144 262144 8388608
#net.ipv4.tcp_wmem=262144 262144 8388608
#net.ipv4.tcp_mem=8388608 8388608 8388608
net.ipv4.tcp_rmem=4096 87380 33554432
net.ipv4.tcp_wmem=4096 87380 33554432
net.ipv4.tcp_mem=366048 488064 33554432
net.ipv4.tcp_no_metrics_save=1
net.ipv4.tcp_max_syn_backlog=4096
net.ipv4.ip_local_port_range = 1024 65000
# Security
# Disable ICMP Redirect Acceptance
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.lo.accept_redirects = 0
net.ipv4.conf.lo.send_redirects = 0
#net.ipv6.conf.default.accept_redirects = 0
#net.ipv6.conf.all.accept_redirects = 0
#net.ipv6.conf.lo.accept_redirects = 0
# Enable Log Spoofed Packets, Source Routed Packets, Redirect Packets
net.ipv4.conf.all.log_martians = 1
net.ipv4.conf.lo.log_martians = 1
net.ipv4.conf.default.log_martians = 1
# Turning off path MTU discovery
net.ipv4.ip_no_pmtu_disc = 1
#Set the location & pattern of the coredumps
kernel.core_pattern = /var/crash/%e.%p.%t

# increase system fd max
fs.file-max = 999999
net.ipv4.tcp_keepalive_time = 120
net.ipv4.tcp_keepalive_intvl = 10
net.ipv4.tcp_keepalive_probes = 3

net.ipv4.conf.default.secure_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.icmp_ignore_bogus_error_responses = 1

