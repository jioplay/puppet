# == Class: os
#
# System related global defaults. As a general rule if it is related to the
# core OS and is not a standalone daemon then it belongs in this module.
#
# === Parameters
#
# Document parameters here
#
# [*resolv_nameservers*]
#   required: Determines nameservers to use in /etc/resolv.conf
#
# [*resolv_search*]
#   optional: defaults to $::domain fact, determines search domains to use in
#   /etc/resolv.conf
#
# [*sendmail_service_ensure*]
#   optional: defaults to stopped. Determines if sendmail service should be
#   running.
#
# [*sendmail_service_enable*]
#   optional: defaults to false. Determines if sendmail service should start
#   on boot.
#
# [*snmpd_service_ensure*]
#   optional: defaults to stopped. Determines if snmpd service should be
#   running.
#
# [*snmpd_service_enable*]
#   optional: defaults to false. Determines if snmpd service should start
#   on boot.
#
# === Examples
#
#   class { "os": resolv_nameservers => [ "10.0.0.1", "10.0.0.2" ] }
#
# === Authors
#
# Ryan Bowlby rbowlby@mobitv.com
#
# === Copyright
#
# Copyright 2013 MobiTV
#
class os (
    $resolv_nameservers = hiera('os::resolv_nameservers',undef),
    $resolv_search = hiera('os::resolv_search',$::domain),

    # os::security::services
    $sendmail_service_ensure = hiera('os::sendmail_service_ensure',stopped),
    $sendmail_service_enable = hiera('os::sendmail_service_enable',false),
    $snmpd_service_ensure = hiera('os::snmpd_service_ensure',stopped),
    $snmpd_service_enable = hiera('os::snmpd_service_enable',false),
) {

    class { "os::sysctl": }
    class { "os::issue": }
    class { "os::resolv": }
    class { "os::bash": }

    # security related defaults
    class { "os::security::accounts": }
    class { "os::security::auditd": }
    class { "os::security::permissions": }
    class { "os::security::services":
        sendmail_service_ensure => stopped,
        sendmail_service_enable => false,
        snmpd_service_ensure    => stopped,
        snmpd_service_enable    => false,
    }
    class { "os::security::control-alt-delete": }
    class { "os::security::profile": }

    # include module name in motd
    os::motd::register { "os": }

    #include os::hosts
    #include nsswitch
}
