# == Class: os::motd
#
# This class puts our default values into the /etc/motd file.
#
# === Parameters
#
# $owner::      owner of the machine
#
# $owneremail:: owner of the machines email
#
# $stack::      what stack does this belong to
#
# $servertype:: what component is this
#
# $hosttype::   vm or bare metal
#
# &respondtoVIP::   public IP or name
#
# === Actions:
#     Installs the motd file
#
# === Requires:
#
# === Example
#
#       class { "os::motd":
#               $owner => "me",
#               $owneremail => "me@mobitv.com",
#               $stack => "QA NFL 1",
#             }
#
# [Remember: No empty lines between comments and class definition]
#
class os::motd (

  $owner = hiera('os::motd::owner',"Default (Change Me)"),
  $owneremail = hiera('os::motd::owneremail',"Default (Change Me)"),
  $stack = hiera('os::motd::stack',"Default (Change Me)"),
  $servertype = hiera('os::motd::servertype',"Default (Change Me)"),
  $hosttype = hiera('os::motd::hosttype',"vm"),
  $respondtoVIP = hiera('os::motd::respondtoVIP',"Default (Change Me)"),
  $motd = hiera('os::motd::motd',"/etc/motd"),
  $motd_modules = hiera('os::motd::motd_modules',"/var/lib/puppet/motd_modules"),
) {

   # include module name in motd (before concatenation)
   #file { "/var/lib/puppet/motd_os::motd":
   #  backup  => false,
   #  ensure  => file,
   #  content => "-- os::motd\n",
   #  before  => Exec["motd_modules"],
   #}

   # recreate motd_modules file with list of modules to include in motd
   # only if that list of modules has changed. Remove files older than 7
   # days to keep removed modules from showing up (hack I know).
   exec { "motd_modules":
     command => "/bin/cat /var/lib/puppet/motd-* > $motd_modules 2>&1 ; exit 0",
     unless  => "/bin/touch $motd_modules; /usr/bin/find /var/lib/puppet/ -type f -name motd-* -maxdepth 1 -mtime +7 -exec rm {} ; && /bin/cat /var/lib/puppet/motd-* | /usr/bin/diff $motd_modules - > /dev/null 2>&1",
     #before  => Class["concat::setup"],
   }

   #class { "concat::setup": }

   concat { $motd:
      owner => root,
      group => root,
      mode  => 644
   }

   if $::environment == "production" {
     $template_name = "motd-prod.erb"
   } else {
     $template_name = "motd-qa.erb"
   }

   concat::fragment { "motd_header":
      target => $motd,
      content => template("os/${template_name}"),
      order   => 01,
   }

   concat::fragment { "motd_modules":
     ensure => $motd_modules,
     target => $motd,
     order  => 10,
   }

   concat::fragment { "motd_seperator":
      target => $motd,
      content => "\n\n------------------------ static /etc/motd.local follows -------------------------\n\n",
      order   => 14,
   }

   # local users on the machine can append to motd by just creating
   # /etc/motd.local
   concat::fragment { "motd_local":
      ensure  => "/etc/motd.local",
      target => $motd,
      order   => 15
   }
}
