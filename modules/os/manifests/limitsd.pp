class os::limitsd (
    $override_limits = undef

)
 {

  if ($::architecture == "x86_64") and ($override_limits) and (! empty($override_limits))  {
    file { '/etc/security/limits.d':
      ensure    => directory ,
      owner     => root ,
      group     => root ,
      mode      => 0644 ,
    }

    file {'/etc/security/limits.d/override.conf':
      ensure     => file,
      owner      => root,
      group      => root,
      mode       => 0644,
      content    => template("os/override.conf.erb"),
      require    => File['/etc/security/limits.d'],
    }

    file {'/etc/security/limits.d/nofiles.conf':
      ensure     => absent,
    }
  } else {

   file { '/etc/security/limits.d':
      ensure  => directory ,
      recurse => true ,
      owner    => root ,
      group    => root ,
      mode    => 0644 ,
      source  => "puppet:///modules/os/limits.d" ,
    }
 }
}
