class os::motd {

    file { '/etc/motd':
        ensure  => present,
        content => template('os/motd.erb'),
    }
}

