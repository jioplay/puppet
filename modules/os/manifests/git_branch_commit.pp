class os::git_branch_commit {

    file { '/etc/git-branch-commit':
        ensure => present,
        owner => root,
        group => root,
        mode => "0444",
        source => "puppet://$::puppetserver/modules/os/git-branch-commit",
    }
}
