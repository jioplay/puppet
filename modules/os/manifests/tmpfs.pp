# Defines a new tmpfs mount point
#
# == Parameters
#
# [*name*]
#   The name of the new mount point.
#
# [*dest*]
#   The destination directory, where to mount the device.
#
# [*size*]
#   The size in GB, defaults to 90% available mem.
#
# == Examples
#
# Invoke with the name and dest for the new db, as in:
#   os::tmpfs { "ramdisk": dest => "/mnt/ramdisk" }
#
#   os::tmpfs { "ramdisk":
#     dest => "/mnt/ramdisk" }
#     size => "20" }
#
define os::tmpfs(
  $dest = "/mnt/${name}",
  $size = inline_template("<%= (memorysize.to_i * 0.9).round -%>"),
  $mode = "0644",
) {

  file { "$dest":
    ensure  => directory,
    owner   => "root",
    group   => "root",
    mode    => $mode,
  }

  mount { "$dest":
      ensure  => mounted,
      atboot  => true,
      device  => "tmpfs",
      fstype  => "tmpfs",
      options => "rw,size=${size}G",
      require => File["$dest"],
  }
}
