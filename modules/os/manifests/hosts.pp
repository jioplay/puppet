# == Class: os::hosts
#
# This module manages /etc/hosts
#
# === Parameters
#
#  $local_hosts::  list (["host1","host2",...]) of hosts to be mapped to localhost
#  $remot_hosts::  list (["ip1 host1","ip2 host2",...]) of ip host pairs to be mapped. These are taken verbatum
#
# === Actions
#
#   Sets up /etc/hosts with the localhost definition as well as the
#   host definitions passed in two lists.  Requires:
#
# === Examples
#   class { "os::hosts":
#         localhosts => ["db.oak1.mobitv","be.mobitv.com"],
#         otherhosts => ["123.456.78.9 svn.qa.dmz"],
#   }
#
# [Remember: No empty lines between comments and class definition]
#
class os::hosts (
    $local_hosts = [],
    $remote_hosts = [],
)
{
    file{ '/etc/hosts':
        ensure => present,
        owner => "root",
        group => "root",
        mode  => "644",
        content => template("os/hosts.erb"),
    }
}
