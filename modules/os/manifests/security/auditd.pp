# == Class: os::security::auditd
#
# Verifies audit package is installed and running.
#
# === Parameters
#
# Document parameters here
#
# [*none*]
#   None at this time.
#
# === Examples
#
#   class { "os::security::auditd" }
#
# === Authors
#
# Ryan Bowlby rbowlby@mobitv.com
#
# === Copyright
#
# Copyright 2013 MobiTV
#
class os::security::auditd {

    package { "audit": ensure => present }
    service { "auditd":
        enable  => true,
        ensure  => running,
        require => Package["audit"],
    }
    file { "/etc/audit/audit.rules":
        ensure  => present,
        owner  => "root",
        group  => "root",
        mode  => "0640",
        source  => "puppet://$puppetserver/modules/os/audit.rules",
    }
}

