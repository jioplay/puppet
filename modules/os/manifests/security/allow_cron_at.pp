# == Class: os::security::allow_cron_at
#
# Adds additional users to cron.allow and at.allow, also called within:
# os::security::cron_at
#
# === Parameters
#
# Document parameters here
#
# [*namevar*]
#   The name of the user to be added to the cron.allow and at.allow files.
#
# === Examples
#
#   os::security::allow_cron_at { "foouser": }
#
# === Authors
#
# Ryan Bowlby rbowlby@mobitv.com
#
# === Copyright
#
# Copyright 2013 MobiTV
#
define os::security::allow_cron_at {

    file_line { "cron_${name}":
        line => $name,
        path => "/etc/cron.allow",
    }

    file_line { "at_${name}":
        line => $name,
        path => "/etc/at.allow",
    }

}
