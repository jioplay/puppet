# == Class: os::security::permissions
#
# Further restricts permissions for certain directories and files beyond
# the system defaults.
#
# === Parameters
#
# Document parameters here
#
# [*none*]
#   None at this time.
#
# === Examples
#
#   class { "os::security::permissions" }
#
# === Authors
#
# Ryan Bowlby rbowlby@mobitv.com
#
# === Copyright
#
# Copyright 2013 MobiTV
#
class os::security::permissions {

    file { "/var/log/wtmp":
        ensure => file,
        owner  => "root",
        group  => "utmp",
        mode   => "0644",
    }

    file { "/var/log/secure":
        ensure => file,
        owner  => "root",
        group  => "root", 
        mode   => "0644",
    }

    file { "/var/log/messages":
        ensure => file,
        owner  => "root",
        group  => "root", 
        mode   => "0644",
    }

    file { "/etc/pam.d":
        ensure => directory,
        owner  => "root",
        group  => "root",
        mode   => "0755",
    }

    file { "/var/log/lastlog":
        ensure => file,
        owner  => "root",
        group  => "root", 
        mode   => "0644",
    }

    file { "/var/log":
        ensure => directory,
        owner  => "root",
        group  => "root",
        mode   => "0755",
    }

    file { "/var/spool/cron":
        ensure => directory,
        owner  => "root",
        group  => "root",
        mode   => "0622",
    }

    file { "/etc/sysconfig/":
        ensure => directory,
        owner  => "root",
        group  => "root",
        mode   => "0755",
    }

    file { "/etc/passwd":
        ensure => file,
        owner  => "root",
        group  => "root",
        mode   => "0644",
    }

    file { "/etc/shadow":
        ensure => file,
        owner  => "root",
        group  => "root",
        mode   => "0400",
    }

    file { "/etc/group":
        ensure => file,
        owner  => "root",
        group  => "root",
        mode   => "0644",
    }

    file { "/etc/gshadow":
        ensure => file,
        owner  => "root",
        group  => "root",
        mode   => "0400",
    }

    file { "/etc/securetty":
        ensure => file,
        owner  => "root",
        group  => "root",
        mode   => "0600",
    }
}
