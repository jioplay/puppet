class os::security::profile {
    file { "/etc/profile":
        ensure  => present,
        owner  => "root",
        group  => "root",
        mode  => "0644",
        source  => "puppet://$puppetserver/modules/os/profile",
    }
}
