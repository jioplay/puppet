# == Class: os::security::cron_at
#
# Creates cron and at allow/deny files with default allow list.
# Makes use of os::security::allow_cron_at define for addition.
#
# === Parameters
#
# Document parameters here
#
# [*none*]
#   None at this time.
#
# === Examples
#
#   class { "os::security::cron_at" }
#
# === Authors
#
# Ryan Bowlby rbowlby@mobitv.com
#
# === Copyright
#
# Copyright 2013 MobiTV
#
class os::security::cron_at {

    File {
        ensure => file,
        owner  => root,
        group  => root,
        mode   => "0660",
    }

    file { [ "/etc/cron.deny", "/etc/at.deny" ]:
        content => "all",
    }

    file { [ "/etc/cron.allow", "/etc/at.allow" ]:
    }

    # default allowed crontabs
    $allowed_users = [ "rtv", "arlanda" ]
    os::security::allow_cron_at { $allowed_users: }

}
