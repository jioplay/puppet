# == Class: os::security::services
#
# Account related security modifications to default CentOS.
#
# === Parameters
#
# Document parameters here
#
# [*sendmail_service_ensure*]
#   optional: defaults to stopped. Determines if sendmail service should be
#   running.
#
# [*sendmail_service_enable*]
#   optional: defaults to false. Determines if sendmail service should start
#   on boot.
#
# [*snmpd_service_ensure*]
#   optional: defaults to stopped. Determines if snmpd service should be
#   running.
#
# [*snmpd_service_enable*]
#   optional: defaults to false. Determines if snmpd service should start
#   on boot.
#
# === Examples
#
#   class { "os::security::services" }
#
# === Authors
#
# Ryan Bowlby rbowlby@mobitv.com
#
# === Copyright
#
# Copyright 2013 MobiTV
#
class os::security::services (
    $sendmail_service_ensure = stopped,
    $sendmail_service_enable = false,

    $snmpd_service_ensure = stopped,
    $snmpd_service_enable = false,
){
    service { "sendmail":
        ensure => $sendmail_service_ensure,
        enable => $sendmail_service_enable,
    }

    service { "snmpd":
        ensure => $snmpd_service_ensure,
        enable => $snmpd_service_enable,
    }

    service { "cups":
        ensure => stopped,
        enable => false,
    }
    service { "iptables":
        ensure => stopped,
        enable => false,
    }
    service { "ip6tables":
        ensure => stopped,
        enable => false,
    }
    service { "kdump":
        ensure => stopped,
        enable => false,
    }
    service { "netconsole":
        ensure => stopped,
        enable => false,
    }
    service { "nscd":
        ensure => stopped,
        enable => false,
    }
    service { "rdisc":
        ensure => stopped,
        enable => false,
    }
    service { "smartd":
        ensure => stopped,
        enable => false,
    }
    service { "winbind":
        ensure => stopped,
        enable => false,
    }
    service { "postfix":
        ensure => stopped,
        enable => false,
    }
    service { "abrtd":
        ensure => stopped,
        enable => false,
    }
       exec { "chkconfig_on_auditd":
                path    => ["/bin","/usr/bin","/sbin"],
                command => "/sbin/chkconfig auditd on  --level 0123456",
        }
       exec { "usermod_shutdown":
                path    => ["/bin","/usr/bin","/sbin"],
                command => "/usr/sbin/usermod -s /sbin/nologin  shutdown",
        }
       exec { "usermod_halt":
                path    => ["/bin","/usr/bin","/sbin"],
                command => "/usr/sbin/usermod -s /sbin/nologin  halt",
        }
       exec { "usermod_sync":
                path    => ["/bin","/usr/bin","/sbin"],
                command => "/usr/sbin/usermod -s /sbin/nologin  sync",
        }
	
}
