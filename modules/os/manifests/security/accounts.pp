# == Class: os::security::accounts
#
# Account related security modifications to default CentOS.
#
# === Parameters
#
# Document parameters here
#
# [*none*]
#   None at this time.
#
# === Examples
#
#   class { "os::security::accounts" }
#
# === Authors
#
# Ryan Bowlby rbowlby@mobitv.com
#
# === Copyright
#
# Copyright 2013 MobiTV
#
class os::security::accounts {

    # increase from RH default of 5
    augeas { "password_length":
        changes => [ 'set /files/etc/login.defs/PASS_MIN_LEN "8"' ],
    }

    augeas { "password_max_days":
        changes => [ 'set /files/etc/login.defs/PASS_MAX_DAYS "45"' ],
    }

    # only wheel group users allowed to su, ril audit recommendation bs
    augeas { "pam_su_wheel_only":
        context => "/files/etc/pam.d/su",
        changes => [
            "ins 2 after *[module =~ regexp('.*pam_rootok.so')][last()]",
            'set 2[1]/type auth',
            'set 2[1]/control required',
            'set 2[1]/module pam_wheel.so',
            'set 2[1]/argument[1] use_uid',
        ],
        onlyif => "match *[module =~ regexp('.*pam_wheel.so')] size == 0",
    }

    # realize news user/group from accounts module, ensure => absent
    # ril security audit compliance
    include ::accounts
    realize(Accounts::User["news"])
    realize(Accounts::Group["news"])

}
