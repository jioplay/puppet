class os::security::control-alt-delete {
    file { "/etc/init/control-alt-delete.conf":
        ensure  => present,
        owner  => "root",
        group  => "root",
        mode  => "0644",
        source  => "puppet://$puppetserver/modules/os/control-alt-delete.conf",
    }
}
