class os::bash {
    exec { "fqdn_prompt":
        command => '/bin/sed -i \'s/.*PS1=.*/PS1="\\u@$(hostname -f): \\w\\\\$ "/g\' /etc/bashrc',
        onlyif  => '/bin/grep -qe \'&& PS1\' /etc/bashrc || /bin/grep -qe \'PS1=.*\[\' /etc/bashrc || /bin/grep -qe \'PS1=.*\\\w\\\\\\\$\' /etc/bashrc ',
    }
}
