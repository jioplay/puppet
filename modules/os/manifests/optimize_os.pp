class os::optimize_os {

    $sysctl_context = "/files/etc/sysctl.conf"

    augeas { "sysctl_conf/net.ipv4.tcp_keepalive_time":
      context => $sysctl_context,
      onlyif  => "get net.ipv4.tcp_keepalive_time != 60",
      changes => "set net.ipv4.tcp_keepalive_time 60",
      notify  => Exec["sysctl"],
    }
    augeas { "sysctl_conf/net.ipv4.tcp_keepalive_probes":
      context => $sysctl_context,
      onlyif  => "get net.ipv4.tcp_keepalive_probes != 3",
      changes => "set net.ipv4.tcp_keepalive_probes 3",
      notify  => Exec["sysctl"],
    }
    augeas { "sysctl_conf/net.ipv4.tcp_keepalive_intvl":
      context => $sysctl_context,
      onlyif  => "get net.ipv4.tcp_keepalive_intvl != 10",
      changes => "set net.ipv4.tcp_keepalive_intvl 10",
      notify  => Exec["sysctl"],
    }

    exec { "/sbin/sysctl -p":
      alias       => "sysctl",
      refreshonly => true,
    }

}
