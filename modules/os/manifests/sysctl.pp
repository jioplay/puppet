# Class: sysctl
#
# This class manages sysctl, which is included in the $operatingsystem module
#
class os::sysctl {

    # host version then fallback to OS version
    file { "/etc/sysctl.conf":
        source => [ "puppet:///modules/os/sysctl.conf-$fqdn",
                    "puppet:///modules/os/sysctl.conf-$operatingsystem",
                    "puppet:///modules/os/sysctl.conf" ],
        mode   => "640",
        owner  => "root",
        group  => "root",
        notify => Exec["reload sysctl"],
        links  => follow,
    }

    exec{ "reload sysctl":
        command     => "/sbin/sysctl -p",
        refreshonly => "true",
    }
}
