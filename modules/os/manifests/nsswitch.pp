class nsswitch {
    file { "/etc/nsswitch.conf":
        ensure  => present,
        owner  => "root",
        group  => "root",
        mode  => "0644",
        source  => "puppet://$puppetserver/modules/os/nsswitch.conf",
    }
}
