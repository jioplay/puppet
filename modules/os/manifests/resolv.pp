class os::resolv {
  require os

  if $::os::resolv_nameservers and $::os::resolv_search {
      file { "/etc/resolv.conf":
        ensure  => file,
        owner   => root,
        group   => root,
        mode    => 0644,
        content => template("os/resolv.erb"),
      }
  } else {
      notice("${name}: resolv.conf not managed without supplying resolv_search & resolv_nameservers.")
  }
}
