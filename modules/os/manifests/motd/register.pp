# used by other modules to register themselves in the motd

define os::motd::register($content = "") {

   if $content == "" {
      $body = $name
   } else {
      $body = $content
   }

   file { "/var/lib/puppet/motd-${name}":
     backup  => false,
     ensure  => file,
     content => "# -- ${body}\n",
   }
}

