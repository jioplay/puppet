# setup host security warning in /etc/issue for ssh log banner
class os::issue {
  file{ '/etc/issue':
    ensure  => file,
    owner   => "root",
    group   => "root",
    mode    => "644",
    source  => "puppet://$puppetserver/modules/os/issue",
  }
}
