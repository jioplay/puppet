# Class: activemq
#
# This module manages activemq
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class activemq (

  ### install.pp
  $package         = $activemq::params::package,
  $package_ensure  = $activemq::params::package_ensure,

  ### config.pp
  $config          = $activemq::params::config,
  $config_template = $activemq::params::config_template,
  $db_driver       = $activemq::params::db_driver,
  $jdbc_url        = $activemq::params::jdbc_url,
  $db_user         = $activemq::params::db_user,
  $db_passwd       = $activemq::params::db_passwd,
  $config_path     = $activemq::params::config_path,
  $broker_name     = $activemq::params::broker_name,

  ### service.pp
  $service         = $activemq::params::service,
  $service_ensure  = $activemq::params::service_ensure,
  $service_enable  = $activemq::params::service_enable,

  ### icinga.pp
  $icinga           = $activemq::params::icinga,
  $icinga_instance  = $activemq::params::icinga_instance,

) inherits activemq::params {

    anchor { "activemq::begin": } ->
    class  { "activemq::install": } ->
    class  { "activemq::config": } ->
    class  { "activemq::icinga": } ->
    anchor { "activemq::end": }

    class { "activemq::service": }

    # include module name in motd
    os::motd::register { $name : }
}
