class activemq::service {
  service { "activemq":
    name       => $::activemq::service,
    ensure     => $::activemq::service_ensure,
    enable     => $::activemq::service_enable,
    hasstatus  => true,
    hasrestart => true,
    subscribe  => [ Class["activemq::install"], Class["activemq::config"] ],
  }
}
