class activemq::install {
  package { "activemq":
      name   => $::activemq::package,
      ensure => $::activemq::package_ensure,
  }
}
