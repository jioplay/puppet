# Class: activemq::params
#
# This class manages Activemq parameters
#
# Parameters:
#  $package:: is the name of the Activemq package
#  $package_ensure:: is the ensure parameter to the activemq package resource.
#  $service:: is the activemq init script name.
#  $service_ensure:: is the activemq service resource ensure parameter.
#  $service_enable:: is the activemq service resource enable parameter.
#  $ssl_package:: is the name of the SSL module for Activemq
#  $ssl_package_ensure:: is the ensure parameter to the activemq ssl package.
#  $devel_package:: is the name of the Activemq development libraries package
#  $devel_package_ensure:: is the ensure parameter to the activemq devel package.
#  $mpm:: is the name of the Activemq MPM type to use, one of: prefork, worker, event
#  $listen:: is the address listed in the main httpd.conf listen directive.
#  $config:: is the config file to use.
#  $config_template:: is the template to use instead of a static file.
#  $db_driver:: is the database driver only used if template is in use
#  $jdbc_url::  is the jdbc url only used if template is in use
#  $db_user::   is the database user only used if template is in use
#  $db_passwd:: is the database password only used if template is in use
#  $broker_name:: The name of the amq broker
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class activemq::params {

  #############################################################################
  ###  activemq class params
  #############################################################################

  ### install.pp
  $package         = "activemq"
  $package_ensure  = present

  ### service.pp
  $service         = "activemq"
  $service_ensure  = running
  $service_enable  = true

  ### config.pp
  $config          = "activemq.mcollective"
  $config_template = undef
  $config_path     = "/etc/activemq/activemq.xml"
  $db_driver       = "com.mysql.jdbc.Driver"
  $jdbc_url        = "jdbc:mysql://sqdtb01p1/activemq"
  $db_user         = "activemq_a"
  $db_passwd       = "ku9xw_efKJe54vf"
  $broker_name     = "localhost"

  ### icinga.pp
  $icinga          = true
  $icinga_instance = "icinga"

}
