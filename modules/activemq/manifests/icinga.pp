class activemq::icinga {

  if $::activemq::icinga {

    if ! $::activemq::icinga_instance {
      fail("Must provide icinga_instance parameter to activemq module when icinga = true")
    }

    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    @@nagios_service { "check_http_${::fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_proc_activemq!",
      service_description   => "check_proc_activemq",
      notes                 => "Executed via NRPE",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::activemq::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

  }
}
