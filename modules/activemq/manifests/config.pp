class activemq::config {

    File {
        owner   => "rtv",
        group   => "rtv",
        mode    => "0644",
        require => Class["activemq::install"],
    }

    if ( $activemq::config_template )
    {
        file { $activemq::config_path:
            ensure  => file,
            content => template("activemq/${activemq::config_template}"),
            notify  => Class["activemq::service"],
        }
    }
    else
    {
        file { $activemq::config_path:
            ensure  => file,
            source => "puppet:///modules/activemq/${activemq::config}",
            notify  => Class["activemq::service"],
        }
    }
}
