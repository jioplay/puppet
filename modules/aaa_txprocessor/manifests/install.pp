class aaa_txprocessor::install {
  package { "mobi-aaa-transaction-processor":
    ensure => "${aaa_txprocessor::txprocessor_ver}",
    notify => Class["tomcat::service"]
  }
}