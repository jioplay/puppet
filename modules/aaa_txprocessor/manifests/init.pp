# == Class: aaa_txprocessor
#
#  installs transaction processor ng component
#
# === Parameters:
#
#    $txprocessor_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "aaa_txprocessor" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class aaa_txprocessor (
    ###icinga.pp
    $icinga = $aaa_txprocessor::params::icinga,
    $icinga_instance = $aaa_txprocessor::params::icinga_instance,
    $icinga_cmd_args = $aaa_txprocessor::params::icinga_cmd_args,
    ###end icinga.pp
    $txprocessor_ver = $aaa_txprocessor::params::txprocessor_ver,
    $db_url = $aaa_txprocessor::params::db_url,
    $db_username = $aaa_txprocessor::params::db_username,
    $db_password = $aaa_txprocessor::params::db_password,
    $db_driver = $aaa_txprocessor::params::db_driver,
    $hibernate_dialect = $aaa_txprocessor::params::hibernate_dialect,
    $hibernate_show_sql = $aaa_txprocessor::params::hibernate_show_sql,
    $mrc_enable = $aaa_txprocessor::params::mrc_enable,
    $mrc_cron = $aaa_txprocessor::params::mrc_cron,
    $pending_enable = $aaa_txprocessor::params::pending_enable,
    $jersey_restclient_connection_timeout = $aaa_txprocessor::params::jersey_restclient_connection_timeout,
    $jersey_restclient_read_timeout = $aaa_txprocessor::params::jersey_restclient_read_timeout,
    $cm_transaction_processor_jerseyClient_connectionTimeoutSeconds = $aaa_txprocessor::params::cm_transaction_processor_jerseyClient_connectionTimeoutSeconds,
    $cm_transaction_processor_jerseyClient_readTimeoutSeconds = $aaa_txprocessor::params::cm_transaction_processor_jerseyClient_readTimeoutSeconds,
    $pending_cron = $aaa_txprocessor::params::pending_cron,
    $txprocessor_appserver_url = $aaa_txprocessor::params::txprocessor_appserver_url,
    $config_file = $aaa_txprocessor::params::config_file,
    $purchase_manager_endpoint_url = $aaa_txprocessor::params::purchase_manager_endpoint_url,
    $offer_manager_endpoint_url = $aaa_txprocessor::params::offer_manager_endpoint_url,
    $adapter_mappings = $aaa_txprocessor::params::adapter_mappings,

)
inherits aaa_txprocessor::params {
    include aaa_txprocessor::install, aaa_txprocessor::config
    anchor { "aaa_txprocessor::begin": } ->
    Class["aaa_txprocessor::install"] ->
    class { "aaa_txprocessor::icinga":} ->
    Class["aaa_txprocessor::config"] ->
    anchor { "aaa_txprocessor::end" :}
    os::motd::register { $name : }
}
