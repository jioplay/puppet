class aaa_txprocessor::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-aaa-transaction-processor/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $txprocessor_ver = "5.1.0-195249"
    $db_url = undef
    $db_username = undef
    $db_password = undef
    $db_driver = undef
    $hibernate_dialect = undef
    $hibernate_show_sql = "false"
    $mrc_enable = undef
    $mrc_cron = undef
    $jersey_restclient_connection_timeout = undef
    $jersey_restclient_read_timeout = undef
    $cm_transaction_processor_jerseyClient_connectionTimeoutSeconds = undef
    $cm_transaction_processor_jerseyClient_readTimeoutSeconds = undef
    $pending_enable = undef
    $pending_cron = undef
    $txprocessor_appserver_url = undef
    $config_file = "/opt/mobi-aaa-transaction-processor/conf/application-config-override.xml"
    $purchase_manager_endpoint_url = undef
    $offer_manager_endpoint_url = "http://offermanagervip:8080/mobi-aaa-offer-manager-stub"

    $adapter_mappings = undef
}
