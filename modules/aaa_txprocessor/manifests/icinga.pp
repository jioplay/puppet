class aaa_txprocessor::icinga {

  if $::aaa_txprocessor::icinga {

    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::aaa_txprocessor::icinga_instance {
      fail("Must provide icinga_instance parameter to aaa_txprocessor module when icinga = true")
    }

    @@nagios_service { "check_http_aaa_txprocessor_${fqdn}":
      host_name             => "$::fqdn",
      check_command         => "check_http! ${::aaa_txprocessor::icinga_cmd_args}",
      service_description   => "check_http_aaa_txprocessor",
      notes => "PATH: ${::aaa_txprocessor::icinga_cmd_args}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::aaa_txprocessor::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

  }
}
