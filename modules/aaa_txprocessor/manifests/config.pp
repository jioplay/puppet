class aaa_txprocessor::config {

    $txprocessor_ver = $aaa_txprocessor::txprocessor_ver
    $db_url = $aaa_txprocessor::db_url
    $db_username = $aaa_txprocessor::db_username
    $db_password = $aaa_txprocessor::db_password
    $db_driver = $aaa_txprocessor::db_driver
    $hibernate_dialect = $aaa_txprocessor::hibernate_dialect
    $hibernate_show_sql = $aaa_txprocessor::hibernate_show_sql
    $mrc_enable = $aaa_txprocessor::mrc_enable
    $jersey_restclient_connection_timeout = $aaa_txprocessor::jersey_restclient_connection_timeout
    $jersey_restclient_read_timeout = $aaa_txprocessor::jersey_restclient_read_timeout
    $cm_transaction_processor_jerseyClient_connectionTimeoutSeconds = $aaa_txprocessor::cm_transaction_processor_jerseyClient_connectionTimeoutSeconds
    $cm_transaction_processor_jerseyClient_readTimeoutSeconds = $aaa_txprocessor::cm_transaction_processor_jerseyClient_readTimeoutSeconds
    $mrc_cron = $aaa_txprocessor::mrc_cron
    $pending_enable = $aaa_txprocessor::pending_enable
    $pending_cron = $aaa_txprocessor::pending_cron
    $txprocessor_appserver_url = $aaa_txprocessor::txprocessor_appserver_url
    $purchase_manager_endpoint_url = $aaa_txprocessor::purchase_manager_endpoint_url
    $offer_manager_endpoint_url = $aaa_txprocessor::offer_manager_endpoint_url

    $adapter_mappings = $aaa_txprocessor::adapter_mappings

    file { "${::aaa_txprocessor::config_file}":
      ensure => file,
      content => template("aaa_txprocessor/applicationProperties.xml.erb"),
      notify => Class["tomcat::service"],
      require => Class["aaa_txprocessor::install"],
    }

    file { "/opt/mobi-aaa-transaction-processor/conf/adapter-mapping.properties":
      ensure => present,
      content => template("aaa_txprocessor/adapter-mapping.properties.erb"),
      require => Class["aaa_txprocessor::install"],
      notify => Class["tomcat::service"],
    }

}
