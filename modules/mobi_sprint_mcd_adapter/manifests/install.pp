class mobi_sprint_mcd_adapter::install {

    $rpmname="mobi-sprint-mcd-adaptor"
    $pkg_name = $mobi_sprint_mcd_adapter::version ? {
        undef => $mobi_sprint_mcd_adapter::install::rpmname,
        default => "${mobi_sprint_mcd_adapter::install::rpmname}-${mobi_sprint_mcd_adapter::version}",
    }
    package { $mobi_sprint_mcd_adapter::install::pkg_name :
        ensure => $mobi_sprint_mcd_adapter::pkg_ensure,
    }
}
