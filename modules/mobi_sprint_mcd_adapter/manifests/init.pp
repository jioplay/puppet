# Class: mobi_sprint_mcd_adapter
#
# This module manages mobi_sprint_mcd_adapter
#
# Parameters:
#
#     version:: the version of the rpm to install
#     pkg_ensure:: the ensure property (D: present)
#
# Requires:
#
#     Java
#
# Sample Usage:
#
#     class { "mobi_sprint_mcd_adapter":
#             version => '5.1.5-216731',
#           }
#
#
# [Remember: No empty lines between comments and class definition]
class mobi_sprint_mcd_adapter (
$version = $mobi_sprint_mcd_adapter::params::version,
    $pkg_ensure = $mobi_sprint_mcd_adapter::params::pkg_ensure,
)
inherits mobi_sprint_mcd_adapter::params {

    anchor{"mobi_sprint_mcd_adapter::begin" :} ->
    class {"mobi_sprint_mcd_adapter::install" :} ->
    anchor{"mobi_sprint_mcd_adapter::end" :}
    os::motd::register{ $name :}
}
