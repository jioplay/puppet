class mobi_aaa_ril_rights_manager_stub::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-aaa-ril-rights-manager-stub/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $mobi_aaa_ril_rights_manager_stub_version = "5.0.0-221381-SNAPSHOT.noarch"
}
