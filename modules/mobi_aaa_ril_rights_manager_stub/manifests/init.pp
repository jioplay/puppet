class mobi_aaa_ril_rights_manager_stub(
    ###icinga.pp
    $icinga = $mobi_aaa_ril_rights_manager_stub::params::icinga,
    $icinga_instance = $mobi_aaa_ril_rights_manager_stub::params::icinga_instance,
    $icinga_cmd_args = $mobi_aaa_ril_rights_manager_stub::params::icinga_cmd_args,
    ###end icinga.pp
   $mobi_aaa_ril_rights_manager_stub_version = $mobi_aaa_ril_rights_manager_stub::params::mobi_aaa_ril_rights_manager_stub_version,
)
inherits mobi_aaa_ril_rights_manager_stub::params{
    include mobi_aaa_ril_rights_manager_stub::install, mobi_aaa_ril_rights_manager_stub::config

    anchor { "mobi_aaa_ril_rights_manager_stub::begin":} ->
    Class["mobi_aaa_ril_rights_manager_stub::install"] ->
    Class["mobi_aaa_ril_rights_manager_stub::config"] ->
    class { "mobi_aaa_ril_rights_manager_stub::icinga":} ->
    anchor { "mobi_aaa_ril_rights_manager_stub::end": }

    os::motd::register { $name : }
}
