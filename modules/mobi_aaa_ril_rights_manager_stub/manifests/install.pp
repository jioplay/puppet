class mobi_aaa_ril_rights_manager_stub::install {
    package { "mobi-aaa-ril-rights-manager-stub-${mobi_aaa_ril_rights_manager_stub::mobi_aaa_ril_rights_manager_stub_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
