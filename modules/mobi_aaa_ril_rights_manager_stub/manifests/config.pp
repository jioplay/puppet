class mobi_aaa_ril_rights_manager_stub::config {

    file { "/var/www/cgi-bin/rights":
        owner => "apache",
        group => "apache",
        mode => "0755",
        source => "puppet://$puppetserver/modules/mobi_aaa_ril_rights_manager_stub/rights",
        require => Class["mobi_aaa_ril_rights_manager_stub::install"],
    }

}
