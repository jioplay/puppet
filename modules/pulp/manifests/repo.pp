class pulp::repo inherits pulp::params {

  file { "repo":
    path   => "/etc/yum.repos.d/${::pulp::params::repo}",
    ensure => present,
    owner  => "root",
    group  => "root",
    mode   => "0644",
    source => "puppet://$puppetserver/modules/pulp/${::pulp::params::repo}",
  }
}
