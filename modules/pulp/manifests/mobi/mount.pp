class pulp::mobi::mount {

  file { "/pulp":
    ensure  => directory,
    owner   => "apache",
    group   => "apache",
    mode    => "0755",
  }

  mount { "pulp-vmdk":
    name    => "/pulp",
    ensure  => mounted,
    atboot  => true,
    device  => "/dev/sdb1",
    fstype  => "ext4",
    options => "defaults",
    require => File["/pulp"],
  }

  file { ["/pulp/pulp", "/pulp/mongodb"]:
    ensure  => directory,
    require => Mount["pulp-vmdk"],
  }

  file { "/var/lib/pulp":
    ensure  => link,
    target  => "/pulp/pulp",
    require => File["/pulp/pulp"],
  }

  file { "/var/lib/mongodb":
    ensure  => link,
    target  => "/pulp/mongodb",
    require => File["/pulp/mongodb"],
  }

}
