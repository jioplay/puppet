class pulp::config {

  File {
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => Class["pulp::install"],
  }

  # patched rc script that works with puppet better
  file { "/etc/init.d/pulp-server":
    mode    => "0755",
    backup => ".puppetsave",
    source => "puppet://$puppetserver/modules/pulp/pulp-server.init",
  }

  file { "/etc/pulp/pulp.conf":
    owner   => "apache",
    group   => "apache",
    content => template("pulp/pulp.conf.erb"),
  }

  file { "/etc/httpd/conf.d/pulp.conf":
    owner   => "apache",
    group   => "apache",
    content => "puppet://$puppetserver/modules/pulp/pulp.conf.apache",
  }

}
