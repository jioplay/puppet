class pulp::service {

  service { "pulp":
    name       => $::pulp::pulp_service,
    ensure     => $::pulp::pulp_service_ensure,
    enable     => $::pulp::pulp_service_enable,
    hasstatus  => true,
    hasrestart => true,
    subscribe  => Class[pulp::config],
  }

  service { "mongod":
    name       => $::pulp::mongod_service,
    ensure     => $::pulp::mongod_service_ensure,
    enable     => $::pulp::mongod_service_enable,
    hasstatus  => true,
    hasrestart => true,
    require    => Service["pulp"],
  }

  # setup saved credentials certificate for user root
  exec { "save_creds":
    command => "/usr/bin/pulp-admin auth login --username=${::pulp::default_login} --password=${::pulp::default_password}",
    cwd     => "/root",
    require => [ Service["mongod"], Service["pulp"] ],
  }
}
