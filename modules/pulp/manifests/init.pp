# Class: pulp
#
# This module manages pulp
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class pulp (

  $pulp_package = $::pulp::params::pulp_package,
  $pulp_package_ensure = $::pulp::params::pulp_package_ensure,

  $pulp_service = $::pulp::params::pulp_service,
  $pulp_service_ensure = $::pulp::params::pulp_service_ensure,
  $pulp_service_enable = $::pulp::params::pulp_service_enable,

  $mongod_service = $::pulp::params::mongod_service,
  $mongod_service_ensure = $::pulp::params::mongod_service_ensure,
  $mongod_service_enable = $::pulp::params::mongod_service_enable,

  $default_login = $::pulp::params::default_login,
  $default_password = $::pulp::params::default_password,

) inherits pulp::params {

    anchor {  "pulp::begin": } ->
    class { "pulp::install": } ->
    class { "pulp::config": } ->
    anchor { "pulp::end": }

    class { "pulp::service": }

  # include module name in motd
  os::motd::register { "${name}": }

}
