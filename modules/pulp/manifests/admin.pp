class pulp::admin (

  $admin_package = $pulp::params::admin_package,
  $admin_package_ensure = $pulp::params::admin_package_ensure,

) inherits pulp::params {

  class { "pulp::repo": }

  package { "pulp-admin":
    name    => $admin_package,
    ensure  => $admin_package_ensure,
    require => Class["pulp::repo"],
  }

  file { "/etc/pulp/admin/admin.conf":
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    backup  => ".puppetsave",
    content => template("pulp/admin.conf.erb"),
    require => Package["pulp-admin"],
  }

  # include module name in motd
  os::motd::register { "${name}": }
}
