class pulp::consumer (

  $consumer_package = $pulp::params::consumer_package,
  $consumer_package_ensure = $pulp::params::consumer_package_ensure,

) inherits pulp::params {

  include pulp::repo

  package { "pulp-consumer":
    name    => $consumer_package,
    ensure  => $consumer_manage_package,
    require => Class["pulp::repo"],
  }

  file { "/etc/pulp/consumer/consumer.conf":
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    backup  => ".puppetsave",
    content => template("pulp/consumer.conf.erb"),
    require => Package["pulp-consumer"],
  }

  # include module name in motd
  os::motd::register { "${name}": }
}
