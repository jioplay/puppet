class pulp::cds (

  $cds_package = $pulp::params::cds_package,
  $cds_package_ensure = $pulp::params::cds_package_ensure,
  $pulp_server = $pulp::params::pulp_server,

) inherits pulp::params {

  class { "pulp::repo": }

  if ! $pulp_server {
    fail("\$pulp_server is a required parameter within the pulp::cds class.")
  }

  package { "pulp-cds":
    name    => $cds_package,
    ensure  => $cds_package_ensure,
    require => Class["pulp::repo"],
  }

  file { "/etc/pulp/cds.conf":
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    backup  => ".puppetsave",
    content => template("pulp/cds.conf.erb"),
    require => Package["pulp-cds"],
  }

  # include module name in motd
  os::motd::register { "${name}": }
}
