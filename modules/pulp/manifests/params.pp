# Class: pulp::params
#
# This module manages pulp paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class pulp::params {

  ### install.pp
  $pulp_package = "pulp"
  $pulp_package_ensure = "present"

  ### service.pp
  $pulp_service = "pulp-server"
  $pulp_service_ensure = "running"
  $pulp_service_enable = "true"

  $mongod_service = "mongod"
  $mongod_service_ensure = "running"
  $mongod_service_enable = "true"

  ### config.pp
  # /etc/pulp/pulp.conf.erb
  $default_login = "admin"
  $default_password = "m0b1r0ck5!"

  ### repo.pp
  case $::operatingsystem {
    "centos", "redhat", "scientific", "linux": {
      $repo = "rhel-pulp.repo"
    }
    "fedora": {
      $repo = "fedora-pulp.repo"
    }
  }

  ### Optional subclasses

  ### admin.pp
  $admin_package = "pulp-admin"
  $admin_package_ensure = "present"

  ### consumer.pp
  $consumer_package = "pulp-consumer"
  $consumer_package_ensure = "present"

  ### cds.pp
  $cds_package = "pulp-cds"
  $cds_package_ensure = "present"
  $pulp_server = undef

}
