class pulp::install {
  package { $::pulp::pulp_package:
    ensure  => $::pulp::pulp_package_ensure,
    require => Class["pulp::repo"],
  }
}
