class mobi_guide_manager(
    ###icinga.pp
    $icinga = $mobi_guide_manager::params::icinga,
    $icinga_instance = $mobi_guide_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_guide_manager::params::icinga_cmd_args,
    $carrier =  $mobi_guide_manager::params::carrier,
    ###end icinga.pp
    $mobi_guide_manager_version = $mobi_guide_manager::params::mobi_guide_manager_version,
    $config_file =  $mobi_guide_manager::params::config_file,
    $external_properties = $mobi_guide_manager::params::external_properties,
    $mobi_guide_manager_package = "mobi-guide-manager",
    $solr_max_rows = $mobi_guide_manager::params::solr_max_rows,
    $thumbnail_cache_max_age = $mobi_guide_manager::params::thumbnail_cache_max_age,
    $thumbnail_404_max_age = $mobi_guide_manager::params::thumbnail_404_max_age,
)
inherits mobi_guide_manager::params{
  include mobi_guide_manager::install
  anchor { "mobi_guide_manager::begin":} -> Class["mobi_guide_manager::install"] ->
  class{"mobi_guide_manager::config":} ->
    class { "mobi_guide_manager::icinga":} ->
  anchor { "mobi_guide_manager::end": }
  os::motd::register { $name : }
}
