class mobi_guide_manager::install {
    package { "${mobi_guide_manager::mobi_guide_manager_package}":
      ensure => "${mobi_guide_manager::mobi_guide_manager_version}",
      notify => [ Class["tomcat::service"], ]
    }
}
