class mobi_guide_manager::config {

  if ($::mobi_guide_manager::carrier){

    if (! $::mobi_guide_manager::config_file){
      fail("\$carrier variable needs to be defined in the node manifest")
    }
    $filename = inline_template("<%= @config_file.split('/')[-1] -%>")
    $external_filename = inline_template("<%= @external_properties.split('/')[-1] -%>")
    file { "${::mobi_guide_manager::config_file}":
      source => "puppet:///modules/mobi_guide_manager/${::mobi_guide_manager::carrier}/${filename}",
      owner  => "rtv",
      group  => "rtv",
      notify => Class["tomcat::service"],
    }

    file { "${::mobi_guide_manager::external_properties}":
      source => "puppet:///modules/mobi_guide_manager/${::mobi_guide_manager::carrier}/${external_filename}",
      owner  => "rtv",
      group  => "rtv",
      ensure => "file",
      notify => Class["tomcat::service"],
    }

    file { "guidemanager.properties":
      path => "/opt/mobi-guide-manager/config/guidemanager.properties",
      #source => "puppet:///modules/mobi_guide_manager/${::mobi_guide_manager::carrier}/guidemanager.properties",
      content  => template("mobi_guide_manager/guidemanager.properties.erb"),
      owner  => "rtv",
      group  => "rtv",
      ensure => "file",
      notify => Class["tomcat::service"],
    }

  }

  #file_line { 'solr_max_rows':
  #    line    => "solr.max.rows=${mobi_guide_manager::solr_max_rows}",
  #    match   => "^solr\.max\.rows.*",
  #    path    => '/opt/mobi-guide-manager/config/guidemanager.properties',
  #    require => Class["mobi_guide_manager::install"],
  #}

}
