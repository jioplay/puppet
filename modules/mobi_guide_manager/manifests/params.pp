class mobi_guide_manager::params {
    ###icinga.pp
    $icinga = true
    $carrier = undef
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-guide-manager/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $mobi_guide_manager_version = "*"
    $config_file = "/opt/mobi-guide-manager/override.d/customConfig.d/infotel-r4g-5.0-default-solr_field.csv"
    $external_properties = "/opt/mobi-guide-manager/override.d/customConfig.d/infotel-r4g-5.0-external_mcd.properties"

    $solr_max_rows = "1000"
    $thumbnail_cache_max_age = "604800"
    $thumbnail_404_max_age = "14400"
}
