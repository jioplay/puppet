class mobi_aaa_ril_rights_manager_adapter::install {
    package { "mobi-aaa-ril-adapter-rights-manager-ng-${mobi_aaa_ril_rights_manager_adapter::mobi_aaa_ril_rights_manager_adapter_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
