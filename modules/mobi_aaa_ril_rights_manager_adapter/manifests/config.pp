class mobi_aaa_ril_rights_manager_adapter::config {

    file { "/opt/mobi-aaa-ril-adapter-rights-manager-ng/webapp/WEB-INF/classes/default.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_aaa_ril_rights_manager_adapter/default.xml.erb'),
        require => Class["mobi_aaa_ril_rights_manager_adapter::install"],
        notify   => Class["tomcat::service"],
    }
}