class mobi_aaa_ril_rights_manager_adapter(
   $mobi_aaa_ril_rights_manager_adapter_version =
   $mobi_aaa_ril_rights_manager_adapter::params::mobi_aaa_ril_rights_manager_adapter_version,

    $cache_control_max_age =
    $mobi_aaa_ril_rights_manager_adapter::params::cache_control_max_age,
    $identity_manager_url =
    $mobi_aaa_ril_rights_manager_adapter::params::identity_manager_url,
    $purchase_manager_url =
    $mobi_aaa_ril_rights_manager_adapter::params::purchase_manager_url,
    $solr_url = $mobi_aaa_ril_rights_manager_adapter::params::solr_url,
    $jerseyclient_connectiont_timeout_Seconds =
    $mobi_aaa_ril_rights_manager_adapter::params::jerseyclient_connectiont_timeout_Seconds,
    $jerseyclient_readtimeout_Seconds =
    $mobi_aaa_ril_rights_manager_adapter::params::jerseyclient_readtimeout_Seconds,
    $acceptscope_coreservices =
    $mobi_aaa_ril_rights_manager_adapter::params::acceptscope_coreservices,
    $live_end_date_grace_period =
    $mobi_aaa_ril_rights_manager_adapter::params::live_end_date_grace_period,
    $vod_end_date_grace_period =
    $mobi_aaa_ril_rights_manager_adapter::params::vod_end_date_grace_period,
    $default_end_date_grace_period =
    $mobi_aaa_ril_rights_manager_adapter::params::default_end_date_grace_period,
    $health_http_connection_timeout =
    $mobi_aaa_ril_rights_manager_adapter::params::health_http_connection_timeout,
    $health_http_read_timeout =
    $mobi_aaa_ril_rights_manager_adapter::params::health_http_read_timeout,
)
inherits mobi_aaa_ril_rights_manager_adapter::params{
  include mobi_aaa_ril_rights_manager_adapter::install
  anchor { "mobi_aaa_ril_rights_manager_adapter::begin":} -> Class["mobi_aaa_ril_rights_manager_adapter::install"] ->
  anchor { "mobi_aaa_ril_rights_manager_adapter::end": }
  os::motd::register { $name : }
}
