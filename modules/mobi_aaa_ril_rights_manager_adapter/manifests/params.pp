class mobi_aaa_ril_rights_manager_adapter::params {

    $mobi_aaa_ril_rights_manager_adapter_version = "5.0.0-221381-SNAPSHOT.noarch"
    $cache_control_max_age = "1000"
    $identity_manager_url =  "http://identitymanagervip:8080/mobi-aaa-ril-identity-manager-oauth2"
    $purchase_manager_url = "http://purchasemanagervip:8080/mobi-aaa-ril-purchase-manager"
    $solr_url = "http://solrmastervip:8080/mobi-solr/"
    $jerseyclient_connectiont_timeout_Seconds = "15"
    $jerseyclient_readtimeout_Seconds = "15"
    $acceptscope_coreservices = "core_services"
    $live_end_date_grace_period = "15"
    $vod_end_date_grace_period = "2"
    $default_end_date_grace_period = "15"
    $health_http_connection_timeout = "5"
    $health_http_read_timeout = "5"
}