class mobi_recordingwriter::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 4888 -u /recordingwriter/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    # install.pp variables
    $package = "mobi-recordingwriter"
    $version = ""

    # config.pp variables
    $rw_user_name="rtv"
    $rw_group_name="rtv"
    $rw_rest_port="4888"
    $shard_amq_broker_hosts=["nvamq01","nvamq02"]
    $shard_request_channel_name="SHARD.REQUESTS"
    $shard_control_channel_name="SHARD.CONTROL"
    $rw_amq_ttl="0"
    $rw_cpu_load_threshold="80.0"
    $max_job_age_sec="300"
    $rw_recordingmaster_vip="recordingmastervip"
    $rw_recordingmaster_uri="recording_master"
    $sku_template="LIVEPROGRAM_CHANNEL_\${channel id}"
    $fragment_file_length="180"
    $fragment_length="6"
    $rw_storage_root="/var/Jukebox/recordings/\${shard_id}"
    $hls_segment_uri_pattern=false
    $hls_playlist_uri_pattern=false
    $frag_cache_size_sec=false
    $frag_cache_workers=false
    $fragcontroller_url=false
    $fragcontroller_rest_port="4311"
    $max_recordings=false
    $consumer_prefetch_size = 0
}
