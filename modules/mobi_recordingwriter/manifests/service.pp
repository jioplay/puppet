class mobi_recordingwriter::service {

    service { "recording_writer":
        ensure => "${mobi_recordingwriter::service_ensure}",
        provider => "redhat",
        enable => true,
        hasstatus => true,
        hasrestart => true,
        require => Class["mobi_recordingwriter::install"],
        subscribe => Class["mobi_recordingwriter::config"],
    }
  }
