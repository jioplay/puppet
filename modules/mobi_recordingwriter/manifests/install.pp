class mobi_recordingwriter::install {
  package { "$mobi_recordingwriter::package":
    ensure => "$mobi_recordingwriter::version",
    notify => Class["mobi_recordingwriter::service"],
  }
}
