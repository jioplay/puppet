# == Class: mobi_recordingwriter
#
#  installs mobi_recordingwriter
#
# === Parameters:
#
#    $package::        The mobi_recordingwriter package name (D: mobi-recordingwriter)
#
#    $version::        The mobi_recordingwriter package version (D: N/A)
#
#    $rw_user_name::     The user account that the service should drop privileges to. (D: rtv)
#                        May be left empty in which case the service will keep running as root
#                        If assigned then that user account must exists and the group identifier
#                        cannot be empty
#
#    $rw_group_name::    The user group that the service should drop privileges to. (D: rtv)
#                        May be left empty in which case the service will keep running as root
#                        If assigned then that user group must exists and the user identifier
#                        cannot be empty
#
#    $rw_rest_nic::      The NIC to bind the REST api to. (D: N/A)
#                        May be left empty in which case REST calls will be accepted on any NIC.
#
#    $rw_rest_port::     The port that the REST api is listening to (D: 4888)
#
#    $shard_amq_broker_hosts::  cnames that should be listed as shard specific brokers. (D: ["SHARD_AMQ_CNAME_1","SHARD_AMQ_CNAME_2"])
#
#    $shard_request_channel_name:: The name of the ActiveMQ queue that the recording writer will listen for
#                                  requests from the job distributor (D: SHARD.REQUESTS)
#    $shard_control_channel_name:: The name of the ActiveMQ topic that the recording writer will listen for
#                                  control message from the job distributor (D: SHARD.CONTROL)
#
#    $rw_amq_ttl::       The default TTL of messages sent from recording writer (D: 0)
#
#    $rw_cpu_load_threshold::  The CPU threshold value where recording writer will no longer accept
#                              more job requests (D: 80.0)
#
#    $max_job_age_sec::        The max time recordingwriter will try to complete its current task.
#
#    $rw_recordingmaster_vip::  The vip name of the recording master (D: recordingmastervip)
#    $rw_recordingmaster_port:: The port of the recording master (D: N/A)
#                               May be empty in which case the port will not be defined in the url
#    $rw_recordingmaster_uri::  The uri name of the recording master (D: recording_master)
#
#                               The url to the recording master will be defined by concatenating the
#                               above value. Examples
#                                http://$rw_recordingmaster_vip[:$rw_recordingmaster_port]/$rw_recordingmaster_uri
#                                http://recordingmastervip/record_master (default values)
#
#    $sku_template::   The SKU template pattern (D: LIVEPROGRAM_CHANNEL_${channel id})
#
#    $fragment_file_length::   The number of seconds in a fragment file (D: 180)
#
#    $fragment_length::        The number of seconds for a fragment (D: 6)
#
#    $rw_variants::        The variants that should be recorded (D: N/A)
#                          These variants MUST be matching the variants that has been configured on the
#                          recording master host. If not some or all variants will get 404 and not be recorded.
#
#    $rw_storage_root::    The storage root for the recording writer (D: /var/Jukebox/recordings/${shard_id})
#                          This storage root must allow rw_user_name to read, write, and remove
#
#    $hls_segment_uri_pattern:: The uri pattern that will be written in the variant specific playlists
#                               (D: /recordings_hls/${channel}/${variant}/${segment_time}.ts?uid=${user_id}&recid=${recording_id}&shard=${shard_id})
#    $hls_playlist_uri_pattern:: The uri pattern that will be written in the multi-variant playlist
#                               (D: /recordings_hls/${channel}/SWITCH_GROUP/${recording_id}.m3u8?encoding=${variant}&uid=${user_id}&recid=${recording_id}&shard=${shard_id})
#    $frag_cache_size_sec::  (D: undef)
#    $frag_cache_workers::  (D: undef)
#    $service_ensure:: Passed into the ensure parameter of the service. Defaults to running
#
#    $fragcontroller_url:: replaces rw_variants, url to fragcontroller (D: undef)
#
#    $fragcontroller_rest_port:: REST-api port of fragcontroller (D:4311)
#
# === Requires
#
#     -
#
# === Sample Usage
#
#  class { "mobi_recordingwriter" :
#      shard_amq_broker_hosts => ["nvamq01","nvamq02"],
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_recordingwriter (
    ###icinga.pp
    $icinga = hiera('mobi_recordingwriter::icinga',$mobi_recordingwriter::params::icinga),
    $icinga_instance = hiera('icinga_instance',$mobi_recordingwriter::params::icinga_instance),
    $icinga_cmd_args = hiera('mobi_recordingwriter::icinga_cmd_args',$mobi_recordingwriter::params::icinga_cmd_args),
    ###end icinga.pp
    $package = hiera('mobi_recordingwriter::package',$mobi_recordingwriter::params::package),
    $version = hiera('mobi_recordingwriter::version',$mobi_recordingwriter::params::version),
    $rw_user_name=hiera('mobi_recordingwriter::rw_user_name',$mobi_recordingwriter::params::rw_user_name),
    $rw_group_name=hiera('mobi_recordingwriter::rw_group_name',$mobi_recordingwriter::params::rw_group_name),
    $rw_rest_nic=hiera('mobi_recordingwriter::rw_rest_nic',$mobi_recordingwriter::params::rw_rest_nic),
    $rw_rest_port=hiera('mobi_recordingwriter::rw_rest_port',$mobi_recordingwriter::params::rw_rest_port),
    $shard_amq_broker_hosts=hiera('mobi_recordingwriter::shard_amq_broker_hosts',$mobi_recordingwriter::params::shard_amq_broker_hosts),
    $shard_request_channel_name=hiera('mobi_recordingwriter::shard_request_channel_name',$mobi_recordingwriter::params::shard_request_channel_name),
    $shard_control_channel_name=hiera('mobi_recordingwriter::shard_control_channel_name',$mobi_recordingwriter::params::shard_control_channel_name),
    $rw_amq_ttl=hiera('mobi_recordingwriter::rw_amq_ttl',$mobi_recordingwriter::params::rw_amq_ttl),
    $rw_cpu_load_threshold=hiera('mobi_recordingwriter::rw_cpu_load_threshold',$mobi_recordingwriter::params::rw_cpu_load_threshold),
    $max_job_age_sec=hiera('mobi_recordingwriter::max_job_age_sec',$mobi_recordingwriter::params::max_job_age_sec),
    $rw_recordingmaster_vip=hiera('mobi_recordingwriter::rw_recordingmaster_vip',$mobi_recordingwriter::params::rw_recordingmaster_vip),
    $rw_recordingmaster_port=hiera('mobi_recordingwriter::rw_recordingmaster_port',$mobi_recordingwriter::params::rw_recordingmaster_port),
    $rw_recordingmaster_uri=hiera('mobi_recordingwriter::rw_recordingmaster_uri',$mobi_recordingwriter::params::rw_recordingmaster_uri),
    $sku_template=hiera('mobi_recordingwriter::sku_template',$mobi_recordingwriter::params::sku_template),
    $fragment_file_length=hiera('mobi_recordingwriter::fragment_file_length',$mobi_recordingwriter::params::fragment_file_length),
    $fragment_length=hiera('mobi_recordingwriter::fragment_length',$mobi_recordingwriter::params::fragment_length),
    $rw_variants=hiera('mobi_recordingwriter::rw_variants',$mobi_recordingwriter::params::rw_variants),
    $rw_storage_root=hiera('mobi_recordingwriter::rw_storage_root',$mobi_recordingwriter::params::rw_storage_root),
    $hls_segment_uri_pattern=hiera('mobi_recordingwriter::hls_segment_uri_pattern',$mobi_recordingwriter::params::hls_segment_uri_pattern),
    $hls_playlist_uri_pattern=hiera('mobi_recordingwriter::hls_playlist_uri_pattern',$mobi_recordingwriter::params::hls_playlist_uri_pattern),
    $frag_cache_size_sec=hiera('mobi_recordingwriter::frag_cache_size_sec',$mobi_recordingwriter::params::frag_cache_size_sec),
    $frag_cache_workers=hiera('mobi_recordingwriter::frag_cache_workers',$mobi_recordingwriter::params::frag_cache_workers),
    $fragcontroller_url=hiera('mobi_recordingwriter::fragcontroller_url',$mobi_recordingwriter::params::fragcontroller_url),
    $fragcontroller_rest_port=hiera('mobi_recordingwriter::fragcontroller_rest_port',$mobi_recordingwriter::params::fragcontroller_rest_port),
    $service_ensure=hiera('mobi_recordingwriter::service_ensure',"running"),
    $max_recordings=hiera('mobi_recordingwriter::max_recordings',$mobi_recordingwriter::params::max_recordings),
    $consumer_prefetch_size=hiera('mobi_recordingwriter::consumer_prefetch_size',$mobi_recordingwriter::params::consumer_prefetch_size),
)
inherits mobi_recordingwriter::params {

    include mobi_recordingwriter::install, mobi_recordingwriter::config, mobi_recordingwriter::service

    anchor { "mobi_recordingwriter::begin": } -> Class["mobi_recordingwriter::install"]
    Class["mobi_recordingwriter::config"] ~> Service["recording_writer"] ->
    class { "mobi_recordingwriter::icinga":} ->
    anchor { "mobi_recordingwriter::end": }

    os::motd::register { $name : }
}
