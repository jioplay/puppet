class mobi_recordingwriter::config {

    File {
        owner => root,
        group => root,
        mode => "0644",
    }

    $rw_user_name=$mobi_recordingwriter::rw_user_name
    $rw_group_name=$mobi_recordingwriter::rw_group_name
    $rw_rest_nic=$mobi_recordingwriter::rw_rest_nic
    $rw_rest_port=$mobi_recordingwriter::rw_rest_port
    $shard_amq_broker_hosts=$mobi_recordingwriter::shard_amq_broker_hosts
    $shard_request_channel_name=$mobi_recordingwriter::shard_request_channel_name
    $shard_control_channel_name=$mobi_recordingwriter::shard_control_channel_name
    $rw_amq_ttl=$mobi_recordingwriter::rw_amq_ttl
    $rw_cpu_load_threshold=$mobi_recordingwriter::rw_cpu_load_threshold
    $max_job_age_sec=$mobi_recordingwriter::max_job_age_sec
    $rw_recordingmaster_vip=$mobi_recordingwriter::rw_recordingmaster_vip
    $rw_recordingmaster_port=$mobi_recordingwriter::rw_recordingmaster_port
    $rw_recordingmaster_uri=$mobi_recordingwriter::rw_recordingmaster_uri
    $sku_template=$mobi_recordingwriter::sku_template
    $fragment_file_length=$mobi_recordingwriter::fragment_file_length
    $fragment_length=$mobi_recordingwriter::fragment_length
    $rw_variants=$mobi_recordingwriter::rw_variants
    $rw_storage_root=$mobi_recordingwriter::rw_storage_root
    $hls_segment_uri_pattern=$mobi_recordingwriter::hls_segment_uri_pattern
    $hls_playlist_uri_pattern=$mobi_recordingwriter::hls_playlist_uri_pattern
    $frag_cache_size_sec=$mobi_recordingwriter::frag_cache_size_sec
    $frag_cache_workers=$mobi_recordingwriter::frag_cache_workers
    $fragcontroller_url=$mobi_recordingwriter::fragcontroller_url
    $fragcontroller_rest_port=$mobi_recordingwriter::fragcontroller_rest_port

    file { "/usr/local/ods/recording_writer/conf.d/recording_writer_config.xml":
        ensure  => present,
        content => template("mobi_recordingwriter/recording_writer_config.xml.erb"),
        require => Class["mobi_recordingwriter::install"],
        notify  => Class["mobi_recordingwriter::service"],
    }

    file { "/etc/logrotate.d/recordingwriter":
        source => "puppet://$puppetserver/modules/mobi_recordingwriter/recordingwriter.logrotate",
    }

    # add rsyslog files if rsyslog being used
    if defined(Class["rsyslog"]) {
        file { "/etc/rsyslog.d/mobi_recordingwriter.conf":
            ensure => present,
            notify => Class["rsyslog::service"],
            source => "puppet:///modules/mobi_recordingwriter/rsyslog/mobi_recordingwriter.conf",
        }

        file { "/etc/rsyslog.d/mobi_recordingwriter_debug.conf":
            ensure => present,
            notify => Class["rsyslog::service"],
            source => "puppet:///modules/mobi_recordingwriter/rsyslog/mobi_recordingwriter_debug.conf",
        }
    }
}
