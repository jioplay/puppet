class mobi_arlanda_database {

    include yum::repo::clean
    case $operatingsystem
    {
        "centos": {
            $osrepo = "centos"
            include yum::repo::centos
        }
        "redhat": {
            $osrepo = "rhel"
            include yum::repo::rhel
        }
        default: { fail("Unrecognized operating system for yum config") }
    }
    include os::static-network
    include oracle::deps
    include oracle::install_dbs
    include oracle::start
    include oracle::clean
    include mobi_arlanda::appserverhosts
    include mobi_arlanda::database::install
    include puppet::disable

    Class ["yum::repo::clean"] ->
    Class["yum::repo::${osrepo}"]  ->
    Class['os::static-network'] ->
    Class["oracle::deps"] ->
    Class['oracle::install_dbs'] ->
    Class['oracle::start'] ->
    Class['oracle::clean'] ->
    Class['mobi-arlanda::appserverhosts'] ->
    Class['mobi-arlanda::database::install'] ->
    Class['puppet::disable']

}
