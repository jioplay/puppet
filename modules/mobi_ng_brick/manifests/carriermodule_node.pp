class mobi_ng_brick::carriermodule_node ( $version ="latest" )
{
    package { 'mobi-node-carriermodule-ng':
        ensure => "${version}",
    }

    #restart some service or do something else here
}
