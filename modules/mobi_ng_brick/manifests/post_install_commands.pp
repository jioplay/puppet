class mobi_ng_brick::post_install_commands
{
  
  $post_install_cmds = [ "/opt/mobi-notification-trigger-ng/bin/activation.sh on", "/etc/init.d/memcached restart", "/sbin/chkconfig --add httpd", "/sbin/chkconfig --levels 3456 httpd on", "/sbin/chkconfig --add tomcat", "/sbin/chkconfig --levels 3456 tomcat on", "/sbin/chkconfig --add memcached", "/sbin/chkconfig --levels 3456 memcached on", ]
  
  exec { $post_install_cmds: }
  
  exec { 'remove_pub_configrepo_file':
         command => "/bin/rm -rf /opt/mobi-publisher/mobi-publisher-transaction-configrepo/config/endpoint.host.json",
  }
  
  file { 'create_pub_configrepo_file':
          require => Exec['remove_pub_configrepo_file'],
          path => "/opt/mobi-publisher/mobi-publisher-transaction-configrepo/config/endpoint.host.json",
          ensure => present,
          mode => 777,
          content => "[\n\"msfrn01p8.smf1.mobitv\"\n]",
  }
  
  exec { 'remove_pub_devdetect_file':
         command => "/bin/rm -rf /opt/mobi-publisher/mobi-publisher-transaction-devdetect/config/endpoint.host.json",
  }
  
  file { 'create_pub_devdetect_file':
         require => Exec['remove_pub_devdetect_file'],
         path => "/opt/mobi-publisher/mobi-publisher-transaction-devdetect/config/endpoint.host.json",
         ensure => present,
         mode => 777,
         content => "[\n\"msfrn01p8.smf1.mobitv\"\n]",
  }
  
  exec { 'remove_pub_frequent_file':
         command => "/bin/rm -rf /opt/mobi-publisher/mobi-publisher-transaction-frequent/config/endpoint.host.json",
  }
  
  file { 'create_pub_frequent_file':
         require => Exec['remove_pub_frequent_file'],
         path => "/opt/mobi-publisher/mobi-publisher-transaction-frequent/config/endpoint.host.json",
         ensure => present,
         mode => 777,
         content => "[\n\"msfrn01p8.smf1.mobitv\"\n]",
  }
  
  exec { 'remove_pub_icon_file':
         command => "/bin/rm -rf /opt/mobi-publisher/mobi-publisher-transaction-icon/config/endpoint.host.json",
  }
  
  file { 'create_pub_icon_file':
         require => Exec['remove_pub_icon_file'],
         path => "/opt/mobi-publisher/mobi-publisher-transaction-icon/config/endpoint.host.json",
         ensure => present,
         mode => 777,
         content => "[\n\"msfrn01p8.smf1.mobitv\"\n]",
  }
  
  exec { 'remove_pub_nightly_file':
         command => "/bin/rm -rf /opt/mobi-publisher/mobi-publisher-transaction-nightly/config/endpoint.host.json",
  }
  
  file { 'create_pub_nightly_file':
         require => Exec['remove_pub_nightly_file'],
         path => "/opt/mobi-publisher/mobi-publisher-transaction-nightly/config/endpoint.host.json",
         ensure => present,
         mode => 777,
         content => "[\n\"msfrn01p8.smf1.mobitv\"\n]",
  }
  
  exec { 'remove_pub_staticres_file':
         command => "/bin/rm -rf /opt/mobi-publisher/mobi-publisher-transaction-staticres/config/endpoint.host.json",
  }
  
  file { 'create_pub_staticres_file':
         require => Exec['remove_pub_staticres_file'],
         path => "/opt/mobi-publisher/mobi-publisher-transaction-staticres/config/endpoint.host.json",
         ensure => present,
         mode => 777,
         content => "[\n\"msfrn01p8.smf1.mobitv\"\n]",
  }
  
  exec { 'remove_pub_tiles_file':
         command => "/bin/rm -rf /opt/mobi-publisher/mobi-publisher-transaction-tiles/config/endpoint.host.json",
  }
  
  file { 'create_pub_tiles_file':
         require => Exec['remove_pub_tiles_file'],
         path => "/opt/mobi-publisher/mobi-publisher-transaction-tiles/config/endpoint.host.json",
         ensure => present,
         mode => 777,
         content => "[\n\"msfrn01p8.smf1.mobitv\"\n]",
  }
  
  
  exec { 'delete_svn_workspace_dir': 
         command => "/bin/rm -rf /var/mobi-publisher/svn-workspace//",
  }
  
  exec { 'delete_svn_workspace_files': 
         command => "/bin/rm -rf /var/mobi-publisher/svn-workspace/*/.svn",
  }
  
  exec { 'make_svn_workspace_dir':
         command => "/bin/mkdir /var/mobi-publisher/svn-workspace",
  }
  
  exec { 'make_svn_workspace_devdetect_dir':
         command => "/bin/mkdir /var/mobi-publisher/svn-workspace/devdetect",
  }
  
  exec { 'make_svn_workspace_staticres_dir':
         command => "/bin/mkdir /var/mobi-publisher/svn-workspace/staticres",
  }
  
  exec { 'make_svn_workspace_icon_dir':
         command => "/bin/mkdir /var/mobi-publisher/svn-workspace/icon",
         
  }
  
  exec { 'make_svn_workspace_template_dir':
         command => "/bin/mkdir /var/mobi-publisher/svn-workspace/template-rule",
  }  
  
  exec { 'svn_workspace_change_permission':
         command => "/bin/chmod 777 /var/mobi-publisher/svn-workspace",
  }
  
  exec { 'svn_workspace_devdetect_change_permission':
         command => "/bin/chmod 777 /var/mobi-publisher/svn-workspace/devdetect",
  }
  
  exec { 'svn_workspace_staticres_change_permission':
         command => "/bin/chmod 777 /var/mobi-publisher/svn-workspace/staticres",
  }
  
  exec { 'svn_workspace_icon_change_permission':
         command => "/bin/chmod 777 /var/mobi-publisher/svn-workspace/icon",
  }
  
  exec { 'svn_workspace_template_change_permission':
         command => "/bin/chmod 777 /var/mobi-publisher/svn-workspace/template-rule",
  }
  
  
  file { "/opt/tomcat/webapps/mobi-itunes-carriermodule/WEB-INF/config.properties": ensure => present, }  
  
  Exec['delete_svn_workspace_files'] -> Exec['delete_svn_workspace_dir'] -> File['create_pub_configrepo_file'] -> File['create_pub_devdetect_file'] -> File['create_pub_frequent_file'] -> File['create_pub_icon_file'] -> File['create_pub_nightly_file'] -> File['create_pub_staticres_file'] -> File['create_pub_tiles_file'] -> Exec['make_svn_workspace_dir'] -> Exec['svn_workspace_change_permission'] -> Exec['make_svn_workspace_devdetect_dir'] -> Exec['svn_workspace_devdetect_change_permission'] -> Exec['make_svn_workspace_staticres_dir'] -> Exec['svn_workspace_staticres_change_permission'] -> Exec['make_svn_workspace_icon_dir'] -> Exec['svn_workspace_icon_change_permission'] -> Exec['make_svn_workspace_template_dir'] -> Exec['svn_workspace_template_change_permission']

}
