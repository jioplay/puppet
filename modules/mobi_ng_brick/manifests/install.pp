#here are only the node packages that we want to install so I can
#figure out what dependencies are missing
class mobi_ng_brick::install
{
    class { 'mobi_ng_brick::frontend_node':
        version =>  "4.7.40-164901"
    }

    class { 'mobi_ng_brick::carriermodule_node':
        version =>  "4.7.40-164901"
    }

    class { 'mobi_ng_brick::publisher_guide_node':
        version =>  "4.7.40-164901"
    }

    class { 'mobi_ng_brick::trigger_notification_node':
        version =>  "4.7.40-164901"
    }

    Class['mobi-ng-brick::frontend-node'] -> Class['mobi-ng-brick::carriermodule-node'] -> Class ['mobi-ng-brick::publisher-guide-node'] -> Class['mobi-ng-brick::trigger-notification-node']

}
