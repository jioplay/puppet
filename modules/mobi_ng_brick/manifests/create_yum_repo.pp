class mobi_ng_brick::create_yum_repo {

    include yum::repo::clean
    include yum::repo::untested
    include yum::repo::goldmaster

    case $operatingsystem
    {
        "centos": {
            $osrepo = "centos"
            include yum::repo::centos }
        "redhat": {
            $osrepo = "rhel"
            include yum::repo::rhel }
        default: { fail("Unrecognized operating system for yum config") }
    }

    package { "mobi-user-rtv":
        ensure => "latest",
    }

    #  package { 'mobi_mcv_lineup_tool':
    #    ensure => "latest",
    #    }


    Class["yum::repo::clean"] -> Class["yum::repo::goldmaster"] -> Class["yum::repo::untested"] -> Class["yum::repo::${osrepo}"] -> Package["mobi-user-rtv"] #-> Package['mobi-mcv-lineup-tool']

}
