class mobi_ng_brick::run_publisher {
  
    define append ($file, $line, $ensure = 'present') {
        case $ensure {
            default: { err ("unknown ensure value ${ensure}" ) }
            present: {
                exec { "/bin/echo '${line}' >> '${file}'":
                    unless => "/bin/grep '${line}' '${file}'"
                }
            }
            absent: {
                exec { "/bin/grep -vFx '${line}' '${file}' | /usr/bin/tee '${file}' > /dev/null 2>&1":
                    onlyif => "/bin/grep '${line}' '${file}'"
                }
            }
        }
    }
    
    define delete($file, $pattern) {
        exec { "/bin/sed -i -r -e '/$pattern/d' $file":
            onlyif => "/bin/grep -E '$pattern' '$file'",
        }
    }
    
    delete { post_install_01:
        file => "/opt/tomcat/webapps/mobi-itunes-carriermodule/WEB-INF/config.properties",
        pattern => "itunes.verification.url",
    }
    
    append { post_install_02:
        require => Delete[post_install_01],
        file => "/opt/tomcat/webapps/mobi-itunes-carriermodule/WEB-INF/config.properties",
        line => "itunes.verification.url=http://itunesstub/verifyReceipt",
    }
    
    exec { 'run_httpd':
        command => "/etc/init.d/httpd restart",
        logoutput => true,
    }
    
    
    exec { 'graceful_httpd':
        command => "/etc/init.d/publisher graceful",
    } 
    
    # service { 'httpd':
    #     ensure => running,
    #     path => "/etc/init.d/",
    # }
    
    service { 'tomcat9080':
        ensure => running ,
  enable => true ,
        path => "/etc/init.d/",
    }
    
    exec { 'run_frequent_publisher':
        command => "/etc/init.d/publisher publish frequent",
        timeout => 600,
        logoutput => true,
    }
    
    exec { 'run_nightly_publisher':
        require => Exec['run_frequent_publisher'],
        command => "/etc/init.d/publisher publish nightly",
        timeout => 600,
        logoutput => true,
    }
    
    exec { 'run_icon_publisher':
        require => Exec['run_nightly_publisher'],
        command => "/etc/init.d/publisher publish icon",
        timeout => 600,
        logoutput => true,
    }
    
    exec { 'run_staticres_publisher':
        require => Exec['run_icon_publisher'],
        command => "/etc/init.d/publisher publish staticres",
        timeout => 600,
        logoutput => true,
    }
    
    exec { 'run_configrepo_publisher':
        require => Exec[ 'run_staticres_publisher'], 
        command => "/etc/init.d/publisher publish configrepo",
        timeout => 600,
        logoutput => true,
    }
    
    exec { 'run_devdetect_publisher':
        require => Exec['run_configrepo_publisher'],
        command => "/etc/init.d/publisher publish devdetect",
        timeout => 600,
        logoutput => true,
    }
    
    Append[post_install_02] -> Exec['graceful_httpd'] -> Exec['run_httpd'] -> Service['httpd'] ->  Service['tomcat9080'] -> Exec['run_devdetect_publisher']

}

