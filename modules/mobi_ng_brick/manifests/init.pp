class mobi_ng_brick
{
    include "os::git-branch-commit"
    include os::motd
    include mobi_ng_brick::create_yum_repo
    include apache
    include mobi_ng_brick::edithost
    include mobi_ng_brick::pre_install_commands
    include mobi_ng_brick::post_install_commands
    include mobi_ng_brick::run_publisher
    include mobi_ng_brick::healthcheck
    include mobi_ng_brick::run_carrier_module_healthcheck

    Class["mobi_ng_brick::create_yum_repo"] -> Package["httpd"]

    Class["mobi_ng_brick::create_yum_repo"] ->  Class["mobi_ng_brick::edithost"] -> Class["mobi_ng_brick::pre_install_commands"] -> Class["mobi_ng_brick::post_install_commands"] -> Class["mobi_ng_brick::run_publisher"] -> Class["mobi_ng_brick::healthcheck"] -> Class["mobi_ng_brick::run_carrier_module_healthcheck"]
}
