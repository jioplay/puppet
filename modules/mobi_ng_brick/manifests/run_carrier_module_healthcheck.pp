class mobi_ng_brick::run_carrier_module_healthcheck {
    exec { 'reload_carrier_module':
        command => "/usr/bin/curl -u snaptv:dragon 'http://localhost:8080/manager/reload?path=/mobi-itunes-carriermodule'",
    }
  
  
    exec { 'carrier_module_health':
        command => "/usr/bin/curl 'http://localhost:8080/mobi-itunes-carriermodule/status'",
        logoutput => true,
    }
    
    
    Exec['reload_carrier_module'] -> Exec['carrier_module_health'] 
}

