class mobi_ng_brick::healthcheck
{
    exec { 'publisher_status':
        command => "/etc/init.d/publisher status",
        logoutput => true,
    }
    
    exec { 'core_v3_health':
        command => "/usr/bin/curl 'http://localhost/core/v3/health'",
        logoutput => true,
    }
    
    exec { 'ng_carrier_module_health':
        command => "/usr/bin/curl 'http://localhost:8080/mobi-carrier-module-ng/health'",
        logoutput => true,
    }
    
    exec { 'nba_carrier_module_health':
        command => "/usr/bin/curl 'http://localhost:8080/mobi-carrier-module-nba/mobi-carrier-module-nba?method=status'",
        logoutput => true,
    }
    
    exec { 'min_management_module_health':
        command => "/usr/bin/curl 'http://localhost:9080/mobi-min-management/health'",
        logoutput => true,
    }
    
    exec { 'notification_manager_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-notification-manager/healthcheck'",
        logoutput => true,
    }
    
    
    exec { 'notification_worker_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-notification-worker/healthcheck'",
        logoutput => true,
    }
    
    exec { 'favorites_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-favorites-manager/healthcheck'",
        logoutput => true,
    }
    
    exec { 'iplookup_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-ip-lookup/status'",
        logoutput => true,
    }
    
    exec { 'location_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-location/healthcheck'",
        logoutput => true,
    }
    
    exec { 'blackout_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-bc-server/healthcheck'",
        logoutput => true,
    }
    
    exec { 'userprefs_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-user-prefs/healthcheck'",
        logoutput => true,
    }
    
    
    exec { 'account_management_lb_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-account-management/loadbalancer/health'",
        logoutput => true,
    }
    
    exec { 'account_management_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-account-management/monitoring/health'",
        logoutput => true,
    }
    
    exec { 'sprint_carrier_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-sprint-carrier-module/abeInterface?call=status'",
        logoutput => true,
    }
    
    exec { 'subscription_module_health':
        command => "/usr/bin/curl -i 'http://localhost:8080/mobi-subscription-manager/monitoring/health'",
        logoutput => true,
    }
    
    Exec['publisher_status'] -> Exec['core_v3_health'] -> Exec['ng_carrier_module_health'] -> Exec['nba_carrier_module_health'] -> Exec['min_management_module_health'] -> Exec['notification_manager_module_health'] -> Exec['notification_worker_module_health'] -> Exec['favorites_module_health'] -> Exec['iplookup_module_health'] -> Exec['location_module_health'] -> Exec['blackout_module_health'] -> Exec['userprefs_module_health'] -> Exec['account_management_lb_module_health'] -> Exec['account_management_module_health'] -> Exec['sprint_carrier_module_health'] -> Exec['subscription_module_health']
}
