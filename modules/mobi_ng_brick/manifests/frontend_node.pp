class mobi_ng_brick::frontend_node ( $version ="latest" )
{
    package { 'mobi-node-frontend-ng':
        ensure => "${version}",
    }

    #restart some service or do something else here
}
