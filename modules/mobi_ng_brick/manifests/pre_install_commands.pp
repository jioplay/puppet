class mobi_ng_brick::pre_install_commands
{

    service { "tomcat":
        ensure => running,
        path => "/etc/init.d/",
        enable => true,
    }

    exec { "httpd_log_permission_change":
        command => "/bin/chmod -R 755 /var/log/httpd",
        require => [ Package["httpd"] ]
    }

    package { "jdk":
        ensure => "1.6.0_06-fcs",
    }

    exec { "install_jce":
        command => "/bin/rpm --force --replacepkgs -ivh http://build.corp/untested/mobi/mobi-jce-6.0.0-96669.i686.rpm",
        require => [ Package["jdk"], ],
    }


    package { "mobi-tomcat-heapsize-increased":
        ensure => "6.0.16-121157"
    }

    exec  { "install-mobi-ng-brick-node":
        path => "/usr/bin:/bin",
        command => "yum install --exclude mobi-nfl* -y -d 0 -e 0 mobi-ng-brick-node",
        require => Package["httpd"],
        notify => [ Service["tomcat"], Service["httpd"] ]
    }

    package { "mobi-node-4sports-frontend":
        ensure => "4.5.90-127983"
    }

    exec { "install_libevent":
        command => "/bin/rpm --force --replacepkgs -ivh http://build.corp/gold_masters/mobi/libevent-1.3.0-1b.el3.rf.i386.rpm",
         onlyif => "/bin/rpm -qa libevent",
    }

    package { "memcached":
        ensure => latest,
    }

   Exec["httpd_log_permission_change"] -> Exec["install_jce"] -> Package["mobi-tomcat-heapsize-increased"] -> Exec["install-mobi-ng-brick-node"] -> Package["mobi-node-4sports-frontend"] -> Exec["install_libevent"] -> Package["memcached"]
}
