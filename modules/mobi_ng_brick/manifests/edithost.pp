class mobi_ng_brick::edithost
{
    class { "user::rtv": }
    class { "os::hosts":
        local_hosts  => [
                         "msath01p8.smf1.mobitv msath01p8",
                         "msath02p8.smf1.mobitv msath02p8",
                         "msawk01p8.smf1.mobitv msawk01p8",
                         "msawk02p8.smf1.mobitv msawk02p8",
                         "msfrn01p8.smf1.mobitv msfrn01p8",
                         "msfrn02p8.smf1.mobitv msfrn02p8",
                         "msfrn03p8.smf1.mobitv msfrn03p8",
                         "msfrn04p8.smf1.mobitv msfrn04p8",
                         "msfrn05p8.smf1.mobitv msfrn05p8",
                         "msfrn06p8.smf1.mobitv msfrn06p8",
                         "msfrn07p8.smf1.mobitv msfrn07p8",
                         "msfrn08p8.smf1.mobitv msfrn08p8",
                         "msprf01p8.smf1.mobitv msprf01p8",
                         "msprf02p8.smf1.mobitv msprf02p8",
                         "mspub03p8.smf1.mobitv mspub03p8",
                         "mspub04p8.smf1.mobitv mspub04p8",
                         "ms4cmvip",
                         "ms4lbsvip",
                         "ms4searchmastervip",
                         "ms4apnpoolvip",
                         "ms4smspoolvip",
                         "ms4vmpsmspoolvip",
                         "ms4appvip",
                         "iplocvip",
                         "mailhost.smf1.mobitv",
                         "sprintcmvip",
                         "vmweb01p1.smf1.mobitv",
                         ],

        remote_hosts => [
                         "10.172.64.135 lab135.oak1.mobitv dbhost dbprdspt dbprdspt.smf1.mobitv",
                         "10.172.64.124 arlandasearch judrmvip itunesstub",
                         "10.172.65.113 configrepo.smf1.mobitv",
                         ],
    }


}
