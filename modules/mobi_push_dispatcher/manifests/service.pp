class mobi_push_dispatcher::service {
    service { "mobi-push-dispatcher":
        enable => true,
        require => Class["mobi_push_dispatcher::install"],
        hasrestart => true,
        ensure => running,
    }
}
