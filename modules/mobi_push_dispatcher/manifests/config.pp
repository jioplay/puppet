class mobi_push_dispatcher::config {

    file { "/opt/mobi-push-dispatcher/bin/dispatcher.properties":
        ensure => present,
        path => "/opt/mobi-push-dispatcher/bin/dispatcher.properties",
        owner => "rtv",
        group => "rtv",
        content => template("mobi_push_dispatcher/dispatcher.properties.erb"),
        notify => Class["mobi_push_dispatcher::service"],
    }
}
