# == Class: mobi_push_dispatcher
#
#  Installs Push Dispatcher component
#
# === Parameters:
#
#    mobi_push_dispatcher_version::   version of the push dispatcher
#
# === Requires:
#
#    java
#
# === Sample Usage
#
#       class { 'mobi_push_dispatcher':
#           mobi_push_dispatcher_package => 'mobi-push-dispatcher-5.4.0-250222.SNAPSHOT',
#           mobi_push_dispatcher_package_ensure => 'present',
#           dispatcher_host_name => 'vmserver31a.oak1.mobitv',
#           xmpp_domain_name => 'pushdev.net',
#       }
#
# Remember: No empty lines between comments and class definition
#
class mobi_push_dispatcher(
    ###icinga.pp
    $icinga = $mobi_push_dispatcher::params::icinga,
    $icinga_instance = $mobi_push_dispatcher::params::icinga_instance,
    $icinga_cmd_args = $mobi_push_dispatcher::params::icinga_cmd_args,
    ###end icinga.pp
    $mobi_push_dispatcher_package = $mobi_push_dispatcher::params::mobi_push_dispatcher_package,
    $mobi_push_dispatcher_package_ensure = $mobi_push_dispatcher::params::mobi_push_dispatcher_package_ensure,
    $dispatcher_host_name,
    $xmpp_domain_name,
)
inherits mobi_push_dispatcher::params{

    anchor { "mobi_push_dispatcher::begin":} ->
    class{"mobi_push_dispatcher::install":} ->
    class{"mobi_push_dispatcher::config":} ->
    class { "mobi_push_dispatcher::icinga":} ->
    anchor { "mobi_push_dispatcher::end": }
    class{"mobi_push_dispatcher::service":}

    os::motd::register { $name : }
}
