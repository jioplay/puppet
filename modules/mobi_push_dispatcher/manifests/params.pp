class mobi_push_dispatcher::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-push-dispatcher/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $mobi_push_dispatcher_version = "*"
    $mobi_push_dispatcher_package = "mobi-push-dispatcher"
    $mobi_push_dispatcher_package_ensure = present
}
