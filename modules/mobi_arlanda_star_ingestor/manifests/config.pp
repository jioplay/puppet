class mobi_arlanda_star_ingestor::config {
  File {
    owner   => rtv,
    group  => rtv,
    mode   => 0644,
  }
  file {"${mobi_arlanda_star_ingestor::template_file_path}":
    ensure  => directory,
    require => Class["mobi_arlanda_star_ingestor::install"],
    source  => "puppet:///modules/mobi_arlanda_star_ingestor/templates",
    recurse => true,
  }
  file { "/opt/arlanda/conf/star-ingestor/star-ingestor.properties":
    ensure => file,
    content => template("mobi_arlanda_star_ingestor/star-ingestor-properties.erb"),
    require => Class["mobi_arlanda_star_ingestor::install"],
  }
  file { "/opt/arlanda/conf/star-ingestor/star-shows.properties":
    ensure => file,
    content => template("mobi_arlanda_star_ingestor/star-shows-properties.erb"),
    require => Class["mobi_arlanda_star_ingestor::install"],
  }
}
