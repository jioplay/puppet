class mobi_arlanda_star_ingestor::params {
  $package='mobi-arlanda-star-ingestor'
  $version='5.4.0-253943'
  $mq_broker_url='failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100'
  $template_file_path='/opt/arlanda/conf/star-ingestor'
  $ftp_upload_path='/var/Jukebox/ftpupload/star/TV SHOWS/STAR TV'
  $originals_upload_path='/var/Jukebox/uploads/originals/5'
  $path_in_provider='TV SHOWS/STAR/TV'
  $provider_name='star'
  $source_path='/var/Jukebox/studios/star/DEV'
  $file_ext_to_be_processed='mxf'
  $segment_file_ext_to_be_processed='mxf'
  $file_transfer='move'
  $no_of_files_to_be_processed='8'
  $transit_file_extension='aspx'
  $last_segment='_L'
  $isilon_path="\\\\idc1isln1vip.mum1.r4g.com\\uploads\\originals\\5"
  $no_of_last_segments_to_be_processed='5'
  $shows= { 'YMAA'      => 'Har Yug Mein Aayega Ek Arjun',
             'NB5'      => 'Nach baliye',
             'SNS'      => 'Saath Nibhaana Saathiya',
             'SAC'      => 'Saraswati Chandra',
             'EHMMBH'   => 'Ek Hazaaron Mein Meri Behnaa Hai',
             'DABH'     => 'Diya Aur Baati Hum',
             'YRKKH'    => 'Yeh Rishta Kya Kehlata Hai',
             'PKDHMMPP' => 'Pyar Ka Dard Hai Meetha Meetha Pyara Pyara',
             'VEERA'    => 'VEERA',
             'DEKEDEMA' => 'Devon ke Dev Mahadev',
             'KAALI'    => 'Kaali-Ek Punar Avtaar',
  }
}
