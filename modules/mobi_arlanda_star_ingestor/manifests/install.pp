class mobi_arlanda_star_ingestor::install {
  package { "${mobi_arlanda_star_ingestor::params::package}":
  ensure => "$mobi_arlanda_star_ingestor::version",
  require => Class["nfs"],
  }
}
