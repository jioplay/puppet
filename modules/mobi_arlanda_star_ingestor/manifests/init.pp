## == Class: mobi_arlanda_star_ingestor
#
# Manage the mobi_arlanda_star_ingestor component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
# package: (mobi-arlanda-star-ingestor)
# version: the rpm version (5.4.0-253943)
# mq_broker_url:           (failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100)
# template_file_path:      (/opt/arlanda/conf/star-ingestor/templates)
# ftp_upload_path:         (/var/Jukebox/ftpupload/star/TV SHOWS/STAR TV)
# originals_upload_path:   (/var/Jukebox/uploads/originals/5)
# path_in_provider:        (TV SHOWS/STAR TV)
# provider_name:           (star)
# source_path:             (/var/Jukebox/studios/star/DEV)
# file_ext_to_be_processed: (mxf)
# segment_file_ext_to_be_processed: (mxf)
# file_transfer:            (move)
# no_of_files_to_be_processed: (8)
# transit_file_extension:
# last_segment: (_L)
# isilon_path: (\\\\idc1isln1vip.mum1.r4g.com\\uploads\\originals\\5)
# === Actions:
#
# Describe the actions of this class here
# module creates a cron job to ingest star content
#
# === Examples:
#
# Put example of class usage in node manifest here
#  class { "mobi_arlanda_star_ingestor": }  creates class with all defaults
#
#  class { "mobi_arlanda_star_ingestor":
#     version       => "5.4.0-253943",
#     provider_name => "star",
#     no_of_files_to_be_processed => 8,
#     source_path => "/var/Jukebox/studios/star/DEV",
#     originals_upload_path => "/var/Jukebox/ftpupload/star/TV SHOWS/STAR TV",
#   }
# [Remember: No empty lines between comments and class definition]
#
class mobi_arlanda_star_ingestor (
  $package= $mobi_arlanda_star_ingestor::params::package,
  $version= $mobi_arlanda_star_ingestor::params::version,
  $mq_broker_url= $mobi_arlanda_star_ingestor::params::mq_broker_url,
  $template_file_path= $mobi_arlanda_star_ingestor::params::template_file_path,
  $ftp_upload_path= $mobi_arlanda_star_ingestor::params::ftp_upload_path,
  $originals_upload_path= $mobi_arlanda_star_ingestor::params::originals_upload_path,
  $path_in_provider= $mobi_arlanda_star_ingestor::params::path_in_provider,
  $provider_name= $mobi_arlanda_star_ingestor::params::provider_name,
  $source_path= $mobi_arlanda_star_ingestor::params::source_path,
  $file_ext_to_be_processed= $mobi_arlanda_star_ingestor::params::file_ext_to_be_processed,
  $segment_file_ext_to_be_processed= $mobi_arlanda_star_ingestor::params::segment_file_ext_to_be_processed,
  $file_transfer= $mobi_arlanda_star_ingestor::params::file_transfer,
  $no_of_files_to_be_processed= $mobi_arlanda_star_ingestor::params::no_of_files_to_be_processed,
  $transit_file_extension= $mobi_arlanda_star_ingestor::params::transit_file_extension,
  $last_segment= $mobi_arlanda_star_ingestor::params::last_segment,
  $isilon_path= $mobi_arlanda_star_ingestor::params::isilon_path,
  $no_of_last_segments_to_be_processed= $mobi_arlanda_star_ingestor::params::no_of_last_segments_to_be_processed,
  $shows= $mobi_arlanda_star_ingestor::params::shows,

) inherits mobi_arlanda_star_ingestor::params {

    anchor { "mobi_arlanda_star_ingestor::begin": } ->
    class { "mobi_arlanda_star_ingestor::install": } ->
    class { "mobi_arlanda_star_ingestor::config": } ->
    anchor { "mobi_arlanda_star_ingestor::end": }

    os::motd::register{ "${name}":}
}
