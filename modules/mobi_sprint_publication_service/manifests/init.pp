## == Class: mobi_sprint_publication_service
#
# Manage the mobi_sprint_publication_service component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_sprint_publication_service (
  $package = $mobi_sprint_publication_service::params::package,
  $package_ensure = $mobi_sprint_publication_service::params::package_ensure,
) inherits mobi_sprint_publication_service::params {

    anchor { "mobi_sprint_publication_service::begin": } ->
    class { "mobi_sprint_publication_service::install": } ->
    class { "mobi_sprint_publication_service::config": } ->
    anchor { "mobi_sprint_publication_service::end": }

    os::motd::register{ "${name}":}
}
