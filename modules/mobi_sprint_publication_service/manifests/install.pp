class mobi_sprint_publication_service::install {
  package { "${::mobi_sprint_publication_service::package}":
        ensure => $::mobi_sprint_publication_service::package_ensure,
        notify => Class["tomcat::service"],
  }

}
