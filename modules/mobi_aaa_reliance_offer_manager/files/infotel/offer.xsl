<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" xmlns:Reliance="http://www.tmforum.org/xml/tip/BusEnt/Prod">

	<xsl:output method="xml" indent="yes" />

	<xsl:template match="/">
		<offers>
			<xsl:for-each select="Reliance:EnterpriseProductCatalog/Reliance:PlanOffering/Reliance:item" >		
				<xsl:variable name="category">
					<xsl:for-each select="../../Reliance:PlanSpecCharacterstic/Reliance:item">
						<xsl:variable name="name" select="string(Reliance:name)"/>					 
							<xsl:if test="$name = 'Category'">
								<xsl:value-of select="Reliance:id" />			
							</xsl:if> 
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="validity">
					<xsl:for-each select="../../Reliance:PlanSpecCharacterstic/Reliance:item">
						<xsl:variable name="name" select="string(Reliance:name)"/>					 
							<xsl:if test="$name = 'Validity'">
								<xsl:value-of select="Reliance:id" />			
							</xsl:if> 
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="items">
					<xsl:for-each select="../../Reliance:PlanSpecCharacterstic/Reliance:item">
						<xsl:variable name="name" select="string(Reliance:name)"/>					 
							<xsl:if test="$name = 'Items'">
								<xsl:value-of select="Reliance:id" />			
							</xsl:if> 
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="numberOfdevices">
					<xsl:for-each select="../../Reliance:PlanSpecCharacterstic/Reliance:item">
						<xsl:variable name="name" select="string(Reliance:name)"/>					 
							<xsl:if test="$name = 'No of Handheld Devices allowed'">
								<xsl:value-of select="Reliance:id" />			
							</xsl:if> 
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="activeStatus">
					<xsl:for-each select="../../Reliance:ReferenceValueMaster/Reliance:item">
						<xsl:variable name="name" select="string(Reliance:entityName)"/>					 
							<xsl:if test="$name = 'ALL'">
								<xsl:for-each select="Reliance:itemAttributes/Reliance:itemAttribute">
									<xsl:variable name="attriName" select="string(Reliance:attributeName)"/>
										<xsl:if test="$attriName = 'status'">
											<xsl:for-each select="Reliance:refValueDescriptionItems">
												<xsl:variable name="active" select="string(Reliance:description)"/>
													<xsl:if test="$active = 'ACTIVE'">
														<xsl:value-of select="Reliance:value"/>
													</xsl:if>
												</xsl:for-each>
										</xsl:if>
								</xsl:for-each>
							</xsl:if> 
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="currencyCode">
					<xsl:for-each select="../../Reliance:ReferenceValueMaster/Reliance:item">
						<xsl:variable name="name" select="string(Reliance:entityName)"/>					 
							<xsl:if test="$name = 'CompositePrice'">
								<xsl:for-each select="Reliance:itemAttributes/Reliance:itemAttribute">
									<xsl:variable name="attriName" select="string(Reliance:attributeName)"/>
										<xsl:if test="$attriName = 'currency'">
											<xsl:for-each select="Reliance:refValueDescriptionItems">
												<xsl:variable name="desc" select="string(Reliance:description)"/>
													<xsl:if test="$desc = 'RUPEE'">
														<xsl:value-of select="Reliance:value"/>
													</xsl:if>
												</xsl:for-each>
										</xsl:if>
								</xsl:for-each>
							</xsl:if> 
					</xsl:for-each>
				</xsl:variable>
				<offer>
					<xsl:element name="OFFER_ID">
						 <xsl:value-of select="Reliance:id" /> 
					</xsl:element>
					<xsl:element name="OFFER_TITLE">
						 <xsl:value-of select="Reliance:name" /> 
					</xsl:element>
					<xsl:element name="OFFER_DESCRIPTION">
						 <xsl:value-of select="Reliance:description" /> 
					</xsl:element>
					<xsl:element name="STATUS">
						<xsl:variable name="status" select="string(Reliance:status)"/>
						<xsl:choose>
							<xsl:when test="$status = $activeStatus">
								<xsl:value-of select="'ACTIVE'" /> 
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'INACTIVE'" /> 
							</xsl:otherwise>
						</xsl:choose>
						</xsl:element>
				<!--	 <xsl:variable name="planSpecificationid" select="string(Reliance:planOfferingToPlanSpecMapping/Reliance:item/Reliance:planSpecificationid)"/> -->
					 <xsl:variable name="planSpecificationid">
						<xsl:for-each select="Reliance:planOfferingToPlanSpecMapping/Reliance:item">
							<xsl:variable name="planSpecificationid" select="string(Reliance:planSpecificationid)"/>
								<xsl:if test="starts-with($planSpecificationid, 'ML')">
									<xsl:value-of select="$planSpecificationid"/>
								</xsl:if> 
						</xsl:for-each>
				  	 </xsl:variable>
					 <xsl:variable name="compositePriceId" select="string(Reliance:compositePriceId)"/>
					 
					 <xsl:for-each select="../../Reliance:CompositePrice/Reliance:item">						
							<xsl:variable name="id" select="string(Reliance:id)"/>
								<xsl:if test="$id = $compositePriceId">
									<xsl:element name="PRICE">
										<xsl:value-of select="Reliance:price"/>
									 </xsl:element>
									  <xsl:element name="CURRENCY">
										  <xsl:variable name="currency" select="string(Reliance:currency)"/>
											<xsl:if test="$currency = $currencyCode">
												<xsl:value-of select="'INR'" /> 
											</xsl:if>										
									 </xsl:element>
								</xsl:if>
					</xsl:for-each>

					 <xsl:for-each select="../../Reliance:PlanSpecification/Reliance:item">
						<xsl:variable name="id" select="string(Reliance:id)"/>
							<xsl:if test="$id = $planSpecificationid">
								 <xsl:element name="SERVICE_TYPE">
									<xsl:value-of select="Reliance:serviceType" /> 
								 </xsl:element>
								 <xsl:for-each select="Reliance:planSpecToPlanCharacteristicMapping/Reliance:item">
									 <xsl:variable name="planSpecCharacteristicId" select="string(Reliance:planSpecCharacteristicId)"/>
									  <xsl:choose>
										 <xsl:when test="$planSpecCharacteristicId = $category">
										   <xsl:element name="CATEGORY_ID">
												<xsl:value-of select="Reliance:value"/>
											</xsl:element>
										 </xsl:when>
										 <xsl:when test="$planSpecCharacteristicId = $items">											
												<EXTENDED_ATTRIBUTE>
													<xsl:element name="fieldName">
														<xsl:value-of select="'Itmes'"/>
													</xsl:element>
													<xsl:element name="fieldValue">
														<xsl:value-of select="Reliance:value"/>
													</xsl:element>
												</EXTENDED_ATTRIBUTE>
										 </xsl:when>
										 <xsl:when test="$planSpecCharacteristicId = $validity">
										 <!-- <xsl:element name="OFFER_VALIDITY">
												<xsl:value-of select="Reliance:value"/>
											</xsl:element> -->
												<EXTENDED_ATTRIBUTE>
													<xsl:element name="fieldName">
														<xsl:value-of select="'OFFER_VALIDITY'"/>
													</xsl:element>
													<xsl:element name="fieldValue">
														<xsl:value-of select="Reliance:value"/>
													</xsl:element>
												</EXTENDED_ATTRIBUTE>
										 </xsl:when>
										 <xsl:when test="$planSpecCharacteristicId = $numberOfdevices">
										  <!-- <xsl:element name="ALLOWED_HANDHELD_DEVICES">
												<xsl:value-of select="Reliance:value"/>
											</xsl:element> -->
											<EXTENDED_ATTRIBUTE>
													<xsl:element name="fieldName">
														<xsl:value-of select="'ALLOWED_HANDHELD_DEVICES'"/>
													</xsl:element>
													<xsl:element name="fieldValue">
														<xsl:value-of select="Reliance:value"/>
													</xsl:element>
												</EXTENDED_ATTRIBUTE>											
										 </xsl:when>
										 <xsl:otherwise>
										 </xsl:otherwise>
									   </xsl:choose>
								 </xsl:for-each>
							</xsl:if>
					 </xsl:for-each> 
				</offer>
			</xsl:for-each>
		</offers>
</xsl:template>
</xsl:stylesheet>



