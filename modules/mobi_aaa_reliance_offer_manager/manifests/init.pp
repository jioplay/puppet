class mobi_aaa_reliance_offer_manager(
   $mobi_aaa_reliance_offer_manager_version = $mobi_aaa_reliance_offer_manager::params::mobi_aaa_reliance_offer_manager_version,
   $solr_server_url = $mobi_aaa_reliance_offer_manager::params::solr_server_url,
   $solr_server_connection_timeout = $mobi_aaa_reliance_offer_manager::params::solr_server_connection_timeout,
   $solr_server_max_connections = $mobi_aaa_reliance_offer_manager::params::solr_server_max_connections,
   $solr_server_max_retries = $mobi_aaa_reliance_offer_manager::params::solr_server_max_retries,
   $offermanagement_sku_vid = $mobi_aaa_reliance_offer_manager::params::offermanagement_sku_vid,
   $fixed_offer_id_for_liveservice = $mobi_aaa_reliance_offer_manager::params::fixed_offer_id_for_liveservice,
   $fixed_package_id_for_non_movie_vod = $mobi_aaa_reliance_offer_manager::params::fixed_package_id_for_non_movie_vod,
   $default_id_for_rental = $mobi_aaa_reliance_offer_manager::params::default_id_for_rental,
   $default_id_for_trial = $mobi_aaa_reliance_offer_manager::params::default_id_for_trial,
   $default_id_for_global_subscription = $mobi_aaa_reliance_offer_manager::params::default_id_for_global_subscription,
   $default_offer_id = $mobi_aaa_reliance_offer_manager::params::default_offer_id,
   $offers_repo_file = $mobi_aaa_reliance_offer_manager::params::offers_repo_file,
   $offers_assettype_mapping_file = $mobi_aaa_reliance_offer_manager::params::offers_assettype_mapping_file,
   $storage_recording_provider = $mobi_aaa_reliance_offer_manager::params::storage_recording_provider,
   $jersey_restclient_connectionTimeout = $mobi_aaa_reliance_offer_manager::params::jersey_restclient_connectionTimeout,
   $jersey_restclient_readTimeout = $mobi_aaa_reliance_offer_manager::params::jersey_restclient_readTimeout,
   $jersey_restclient_maxConnections = $mobi_aaa_reliance_offer_manager::params::jersey_restclient_maxConnections,
   $jersey_restclient_enableClientLog = $mobi_aaa_reliance_offer_manager::params::jersey_restclient_enableClientLog,
   $external_offer_metadata_job_enabled = $mobi_aaa_reliance_offer_manager::params::external_offer_metadata_job_enabled,
   $external_offer_metadata_cron_expression = $mobi_aaa_reliance_offer_manager::params::external_offer_metadata_cron_expression,
   $external_offer_metadata_xml_file_path = $mobi_aaa_reliance_offer_manager::params::external_offer_metadata_xml_file_path,
   $external_offer_metadata_xml_file_archive_path = $mobi_aaa_reliance_offer_manager::params::external_offer_metadata_xml_file_archive_path,
   $external_offer_metadata_xsl_file_path =  $mobi_aaa_reliance_offer_manager::params::external_offer_metadata_xsl_file_path,
)
inherits mobi_aaa_reliance_offer_manager::params{
  include mobi_aaa_reliance_offer_manager::install, mobi_aaa_reliance_offer_manager::config
  anchor { "mobi_aaa_reliance_offer_manager::begin":} -> Class["mobi_aaa_reliance_offer_manager::install"] ->
  Class["mobi_aaa_reliance_offer_manager::config"] ->
  anchor { "mobi_aaa_reliance_offer_manager::end": } -> os::motd::register { $name : }
}
