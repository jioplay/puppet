class mobi_aaa_reliance_offer_manager::install {
    package { "mobi-aaa-ril-offer-manager-${mobi_aaa_reliance_offer_manager::mobi_aaa_reliance_offer_manager_version}":
      ensure   => present,
      provider => yum,
      notify   => [ Class["tomcat::service"], ]
    }
}
