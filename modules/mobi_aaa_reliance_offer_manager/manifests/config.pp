class mobi_aaa_reliance_offer_manager::config {

    file { "/opt/mobi-aaa-ril-offer-manager/webapp/WEB-INF/classes/default.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_aaa_reliance_offer_manager/default.xml.erb'),
        require => Class["mobi_aaa_reliance_offer_manager::install"],
        notify   => Class["tomcat::service"],
    }
 file {"/opt/mobi-aaa-ril-offer-manager/conf/offers.properties":
      source => "puppet:///modules/mobi_aaa_reliance_offer_manager/infotel/offers.properties",
      replace => true,
      owner  => "rtv",
      group  => "rtv",
      notify => Class["tomcat::service"],
    }

 file {"/opt/mobi-aaa-ril-offer-manager/conf/offersToAssetTypeMapping.properties":
      source => "puppet:///modules/mobi_aaa_reliance_offer_manager/infotel/offersToAssetTypeMapping.properties",
      replace => true,
      owner  => "rtv",
      group  => "rtv",
      notify => Class["tomcat::service"],
    }

 file {"/opt/mobi-aaa-ril-offer-manager/webapp/WEB-INF/classes/xslt/offer.xsl":
      source => "puppet:///modules/mobi_aaa_reliance_offer_manager/infotel/offer.xsl",
      replace => true,
      owner  => "rtv",
      group  => "rtv",
      notify => Class["tomcat::service"],
    }


}
