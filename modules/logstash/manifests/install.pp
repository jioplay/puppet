class logstash::install inherits logstash {

  package { "logstash":
    ensure => installed,
  }

  file { '/usr/lib64' :
    ensure => directory,
  }

}
