class logstash::config {

  # Create configuration directories
  file { ["/etc/logstash/", "/etc/logstash/conf.d" ]:
    ensure => directory,
    recurse => true,
  }

  file { '/etc/init.d/logstash' :
    ensure => file,
    mode   => "0755",
    source => "puppet://$puppetserver/modules/logstash/logstash",
  }

  # Place virtual resource templates passed to me into a collected directory,
  # Then take the ends of the names off so there's only one for each type of log
  exec { "place_collected" :
    cwd         => "/etc/logstash/",
    path        => ["/usr/bin", "/usr/sbin", "/bin", "/sbin"],
    command     => 'bash -c \'files=`ls -1 /etc/logstash/collected/ `; for f in $files; do short=$( echo $f | cut -d"." -f1-2); cp -f /etc/logstash/collected/${f} /etc/logstash/conf.d/${short}; done\'',
    refreshonly => true,
  }


  # Check to see if this is a relay node for zmq
  # If it is, point to a different config file and give it a different name.
  if $logstash::isRelay {
    $config_file = $logstash::relay_config
    $config_name = "relay.conf"

    file { "/etc/logstash/collected/":
      ensure => directory,
      purge  => true,
      recurse => true,
      notify => Exec['place_collected'],
    }

    file { "/etc/logstash/filters_collected/" :
      ensure  => directory,
      purge   => true,
      recurse => true,
      notify  => Exec['place_filter_configs']
    }

    file { "/etc/logstash/grok_collected/" :
      ensure  => directory,
      purge   => true,
      recurse => true,
      notify  => Exec['place_grok_configs']
    }

    # Pull in virtual resources
    File <<| tag == 'logstash_relay' |>> {
      require => File['/etc/logstash/collected/'],
      notify  => Exec["place_collected"],
    }

  }
  else
  {
    $config_name = "logstash.conf"
  }

  # Plugins and filters
  file { ["/etc/logstash/plugins","/etc/logstash/plugins/logstash"] :
    ensure  => directory,
    recurse => true,
  }

  # Logrotate directives
  file { "/etc/logrotate.d/logstash" :
    ensure => file,
    source => "puppet://$puppetserver/modules/logstash/${logstash::logrotate_file}",
  }

  if $logstash::static_config {
    file { "/etc/logstash/conf.d/logstash.conf":
      ensure  => file,
      source  => "puppet://$puppetserver/modules/logstash/${logstash::config_file}"
    }
  }
  else {  # Use standard template instead
    file { "/etc/logstash/conf.d/${config_name}":
      ensure  => file,
      content => template("logstash/${config_file}"),
    }
  }
}
