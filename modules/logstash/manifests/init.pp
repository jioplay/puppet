# Class: logstash
#
# This module manages logstash
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class logstash (

  $service = $logstash::params::service,
  $manage_service_ensure = $logstash::params::manage_service_ensure,
  $manage_service_enable = $logstash::params::manage_service_enable,

  $config_file = $logstash::params::config_file,
  $fileinputs = $logstash::params::fileinputs,

  $static_config = false,

  $isRelay    = false,
  $amqp_relay = $logstash::params::relay,
  $zmqhost    = $logstash::params::zmqhost,
  $zmq_queues = $logstash::params::zmq_queues,

  $parser     = false,

  # Names of the template files for config inputs
  $parser_config_input = $logstash::params::parser_config_input,
  $parser_config_output = $logstash::params::parser_config_output,

  # AMQP key_value hashes for inputs, outputs
  $parser_inputs = $logstash::params::parser_inputs,
  $parser_outputs = $logstash::params::parser_outputs,

) inherits logstash::params {

  anchor { "logstash::begin": } ->
  class { "logstash::install": } ->
  class { "logstash::config": } ->
  class { "logstash::parser": } ->
  anchor { "logstash::end": }


  class { "logstash::service": }

  # include module name in motd
  os::motd::register { "logstash": }
}
