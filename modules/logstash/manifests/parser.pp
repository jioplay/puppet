define logstash::patterns {

  File {
    ensure  => present,
    owner   => "logstash",
    group   => "root",
    mode    => "0644",
    require => Package["logstash"],
  }

  # Install grok patterns directory
  file { ["/etc/logstash/grok", "/etc/logstash/grok_collected"] :
    ensure  => directory,
    recurse => true,
    notify  => Class["logstash::service"],
  }

  file { "/etc/logstash/filters_collected":
    ensure => directory,
    notify => Class["logstash::service"],
  }

  # Install base patterns
  file { "/etc/logstash/grok/base" :
    ensure  => present,
    require => File['/etc/logstash/grok'],
    source  => "puppet://$puppetserver/modules/logstash/parser/grok_patterns/base",
  }

  # Install filter plugins
  file { "/etc/logstash/plugins/logstash/filters/" :
    ensure  => directory,
    source  => "puppet://$puppetserver/modules/logstash/parser/filters",
    recurse => true,
    require => File['/etc/logstash/plugins', '/etc/logstash/plugins/logstash'],
  }

  # Pull in virtual resources for extra grok patterns
  if $::logstash::parser {
    File <<| tag == 'grok' |>> {
      require => File['/etc/logstash/grok', '/etc/logstash/grok_collected'],
      notify  => Exec['place_grok_configs'],
    }

    # Pull in virtual resources for extra filters
    File <<| tag == 'logstash_filters' |>> {
      require => File['/etc/logstash/filters_collected'],
      notify  => Exec['place_filter_configs'],
    }
  }

}

define logstash::filters {

  # Take all the collected filter configs and make sure they're unique per model

  # Place filters collected from logstash modules
  File <<| tag == 'logstash_filters' |>> {
    require => File['/etc/logstash/conf.d', '/etc/logstash/filters_collected'],
    notify  => Exec['place_filter_configs'],
  }
}

class logstash::parser {

  exec { 'place_grok_configs' :
    cwd         => "/etc/logstash",
    path        => ["/usr/bin", "/usr/sbin", "/bin", "/sbin"],
    command     => 'bash -c \'files=`ls -1 /etc/logstash/grok_collected/ `; for f in $files; do short=$(echo $f | cut -d"." -f1-2); cp -f /etc/logstash/grok_collected/${f} /etc/logstash/grok/${short}; done\'',
    refreshonly => true,
  }

  exec { 'place_filter_configs' :
    cwd         => "/etc/logstash",
    path        => ["/usr/bin", "/usr/sbin", "/bin", "/sbin"],
    command     => 'bash -c \'files=`ls -1 /etc/logstash/filters_collected/ `; for f in $files; do short=$(echo $f | cut -d"." -f1-2); cp -f /etc/logstash/filters_collected/${f} /etc/logstash/conf.d/${short}; done\'',
    refreshonly => true,
  }

  if $logstash::parser {
    logstash::filters { "logstash_filters${::fqdn}" : }
    logstash::patterns { "logstash_patterns${::fqdn}" : }

    file { "/etc/logstash/conf.d/parser_input.conf" :
      ensure  => file,
      content => template("logstash/${logstash::parser_config_input}"),
    }

    file { "/etc/logstash/conf.d/parser_output.conf" :
      ensure  => file,
      content => template("logstash/${logstash::parser_config_output}"),
    }
  }

}
