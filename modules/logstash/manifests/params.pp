# Class: logstash::params
#
# This module manages logstash paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class logstash::params {

  ### install.pp

  ### service.pp

  $service = 'logstash'


  $manage_service_ensure = $logstash_manage_service_ensure ? {
    ""      => "running",
    default => $logstash_manage_service_ensure,
  }

  $manage_service_enable = $logstash_manage_service_enable ? {
    ""      => "true",
    default => $logstash_manage_service_enable,
  }

  ### config.pp

  $zmqhost = "tcp://logcollector.smf1.mobitv:5555"
  $config_directory = "/etc/logstash/conf.d"
  $plugin_directory = "/etc/logstash/plugins"
  $error_log = "/var/log/logstash.err"
  $config_file = "logstash.conf.generic.erb"
  $relay_config = "logstash.conf.relay.erb"
  $supervisor_conf = "logstash.supervisor.conf.erb"
  $fileinputs = []
  $relay_out_config = "logstash.conf.relay.out.erb"

  #  Example:
  #
  #  $fileinputs = [{ "type" => "logtype",
  #        "path"            => "/var/log/messages" },
  #     { "type"               => "filetype2",
  #       "path"           => "/var/log/secure"}
  #  ]

  $test_input =     [{ "type" => "test",
                       "path" => "/tmp/testlog",
                       "tags" => '["test"]'}]

  $logrotate_file = "logstash.logrotate"

  # Allows the addition of new queues to listen to on the
  # relay host.
  $zmq_queues = ["logstash"]
  $amqp_host  = "logcollector.smf1.mobitv"
  $exchange_name = "logstash"
  $relay_inputs = [
    { 'address'  => '"tcp://*:5554"',
      'type'     => '"logstash"',
      'topology' => '"pushpull"',
      'mode'     => '"server"',
    },
  ]

  ### AMQP parameters


  $relay_output         = {
    "host"          => '"localhost"',
    "name"          => '"logstash"',
    "port"          => 5673,
    "user"          => '"guest"',
    "password"      => '"guest"',
    "exchange_type" => '"fanout"',
  }

  ### parser.pp
  # Configure inputs, template files for parser
  $parser_config_input = "logstash.conf.parser_in.erb"
  $parser_config_output = "logstash.conf.parser_out.erb"
  # Default amqp inputs
  $parser_inputs = [{
    "durable"  => 'true',
    "type"     => '"to_be_parsed"',
    "host"     => '"logcollector.smf1.mobitv"',
    "name"     => '"logstash"',
  }]
  $parser_outputs = [{
    "durable"       => 'true',
    "exchange_type" => '"fanout"',
    "name"          => '"logstash_parsed"',
    "host"          => '"logcollector.smf1.mobitv"',
  }]

}
