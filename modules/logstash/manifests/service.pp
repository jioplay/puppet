class logstash::service {

  $parameters = {
    "command"     => "java -jar /usr/bin/logstash/logstash-monolithic.jar agent -f ${logstash::config_directory} --pluginpath ${logstash::plugin_directory}",
    "autostart"   => "true",
    "autorestart" => "true",
  }

  supervisor::addservice{ "logstash":
    service_parameters => $parameters,
  }

}
