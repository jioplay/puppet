require "rubygems"
require "uri"
require "logstash/namespace"
require "logstash/time"

# The xmltojson filter takes an xml message, gets rid of pesky escape characters,
# fixes quotes, and converts to json.
# 
# The use case it was designed for was a conversion from xml paired with the JSON
# filter to allow us to parse big XML documents.
#
class LogStash::Filters::UnescapeURL < LogStash::Filters::Base

  config_name "unescape"
  plugin_status "beta"

  config :outfield, :validate => :string, :default => "u"
  config :infield, :validate => :string, :default => "u"

  public
  def register
    # Nothing to do
  end # def register

  public
  def filter(event)
    return unless filter?(event)

    events = []
    
    # Replace the escapes in the message with single quotes, then escape ampersands
    original_value = ""
    begin
      original_value = event["@fields"][@infield]
	  if original_value == nil
	    new_string = original_value
	  else
	    new_string = URI::unescape(original_value)
        filter_matched(event)
	  end
    rescue => e
      new_string = original_value
      event.tags << "_unescapeFailure"
      @logger.warn("Trouble unescaping URL " + event["@message"], :exception => e, :backtrace => e.backtrace)
    end
    # If for some reason the field is an array of values, take the first only.
    original_value = original_value.first if original_value.is_a?(Array)

    event["@fields"][@outfield] = new_string
    # Push this new event onto the stack at the LogStash::FilterWorker
    #event[@infield] = ''
    # Cancel this event, we'll use the newly generated ones above.
  end # def filter
end # class LogStash::Filters::UnescapeURL
