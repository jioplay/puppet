class mobi_policy_manager::juarn::params {
    #$policy_manager_version = "5.4.0-20130316.034532-13-241090"
    $policy_manager_version = "*"

    $policy_username = "pm_user"
    $policy_password = "sld9_wlori5TER_k8d"

    # Global database params
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $hibernate_database_vendor = "MYSQL"
    $jdbc_driver = "com.mysql.jdbc.Driver"
    $min_pool_size = "5"
    $max_pool_size = "100"

    $fm_oracle_native_queries_enabled = "false"
    $policy_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/POLICY_MGR?autoReconnect=true"

    $policy_manager_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100"
    $policy_manager_max_connections = "10"
    $policy_manager_queue = "POLICY_MANAGER.QUEUE"
    $policy_manager_queue_size = "1"

    $ril = false
}
