class mobi_policy_manager::juarn::install {

    package { "mobi-policy-manager":
        ensure => "${mobi_policy_manager::juarn::policy_manager_version}",
        notify   => Class["tomcat::service"],
    }

}
