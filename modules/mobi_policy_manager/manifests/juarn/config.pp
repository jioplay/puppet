class mobi_policy_manager::juarn::config {


    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/mobi-policy-manager.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content =>template('mobi_policy_manager/juarn/policymanager-tomcat-context.xml.erb'),
        require => Class["mobi_policy_manager::juarn::install"],
        notify   => Class["tomcat::service"],
    }


    file { "/opt/mobi-policy-manager/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_policy_manager/juarn/policy-manager-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_policy_manager::juarn::install"],
    }

    if($mobi_policy_manager::juarn::ril){
         file { "/opt/arlanda/conf/policymanager/policy-manager.properties":
            replace => true,
            owner  => "rtv",
            group  => "rtv",
            mode => "0444",
            content => template('mobi_policy_manager/juarn/policymanager.ril.properties.erb'),
            notify   => Class["tomcat::service"],
            require => Class["mobi_policy_manager::juarn::install"],
         }
    } else {
        file { "/opt/arlanda/conf/policymanager/policy-manager.properties":
            replace => true,
            owner  => "rtv",
            group  => "rtv",
            mode => "0444",
            content => template('mobi_policy_manager/juarn/policymanager.properties.erb'),
            notify   => Class["tomcat::service"],
            require => Class["mobi_policy_manager::juarn::install"],
        }
   }
}
