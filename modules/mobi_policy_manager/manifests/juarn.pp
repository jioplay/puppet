# == Class: mobi_policy_manager::juarn
#
#  installs juarn component
#
# === Parameters:
#
#
#    $policy_manager_version::
#    $dam_username::
#    $dam_password::
#    $hibernate_database_dialect::
#    $hibernate_database_vendor::
#    $jdbc_driver::
#    $fm_oracle_native_queries_enabled::
#    $dam_db_jdbc_url::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_policy_manager::install::juarn" :
#           policy_manager_version => "5.0.0-178058",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_policy_manager::juarn (
    $policy_manager_version = $mobi_policy_manager::juarn::params::policy_manager_version,
    $policy_username = $mobi_policy_manager::juarn::params::policy_username,
    $policy_password = $mobi_policy_manager::juarn::params::policy_password,
    $policy_db_jdbc_url = $mobi_policy_manager::juarn::params::policy_db_jdbc_url,
    $dam_username = $mobi_policy_manager::juarn::params::dam_username,
    $dam_password = $mobi_policy_manager::juarn::params::dam_password,
    $hibernate_database_dialect = $mobi_policy_manager::juarn::params::hibernate_database_dialect,
    $hibernate_database_vendor = $mobi_policy_manager::juarn::params::hibernate_database_vendor,
    $jdbc_driver = $mobi_policy_manager::juarn::params::jdbc_driver,
    $min_pool_size = $mobi_policy_manager::juarn::params::min_pool_size,
    $max_pool_size = $mobi_policy_manager::juarn::params::max_pool_size,
    $fm_oracle_native_queries_enabled = $mobi_policy_manager::juarn::params::fm_oracle_native_queries_enabled,
    $dam_db_jdbc_url = $mobi_policy_manager::juarn::params::dam_db_jdbc_url,
    $policy_manager_broker_url = $mobi_policy_manager::juarn::params::policy_manager_broker_url,
    $policy_manager_max_connections = $mobi_policy_manager::juarn::params::policy_manager_max_connections,
    $policy_manager_queue = $mobi_policy_manager::juarn::params::policy_manager_queue,
    $policy_manager_queue_size = $mobi_policy_manager::juarn::params::policy_manager_queue_size,
    $ril = $mobi_policy_manager::juarn::params::ril,
)

inherits mobi_policy_manager::juarn::params {
    include mobi_policy_manager::juarn::install, mobi_policy_manager::juarn::config, mobi_cms_us::tomcat_config
    anchor { "mobi_policy_manager::juarn::begin": } -> Class["mobi_policy_manager::juarn::install"] ->
    Class["mobi_policy_manager::juarn::config"] -> Class["mobi_cms_us::tomcat_config"] ->
    anchor { "mobi_policy_manager::juarn::end" :} -> os::motd::register { $name : }
}
