# == Class: mobi_policy_manager::database
#
#  installs policy database
#
# === Parameters:
#
#   $database_root_password::
#   $policy_username::
#   $policy_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_policy_manager::database" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_policy_manager::database (
    $database_root_password = $mobi_policy_manager::database::params::database_root_password,
    $policy_username = $mobi_policy_manager::database::params::policy_username,
    $policy_password = $mobi_policy_manager::database::params::policy_password,
)
  inherits mobi_policy_manager::database::params {
    include mobi_policy_manager::database::install, mobi_policy_manager::database::config
    anchor { "mobi_policy_manager::database::begin":} -> Class["mobi_policy_manager::database::install"] ->
    Class["mobi_policy_manager::database::config"] -> anchor { "mobi_policy_manager::database::end": } ->
    os::motd::register { $name : }
}
