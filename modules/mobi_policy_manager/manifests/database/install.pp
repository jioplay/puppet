class mobi_policy_manager::database::install {

    file { "/var/mobi-policy-manager":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi-policy-manager/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_policy_manager/database",
        require => File["/var/mobi-policy-manager"],
    }

    file { "/var/mobi-policy-manager/database/install.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_policy_manager/database/install.sh.erb'),
        require => File["/var/mobi-policy-manager"],
    }

    file { "/var/mobi-policy-manager/database/uninstall.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_policy_manager/database/uninstall.sh.erb'),
        require => File["/var/mobi-policy-manager"],
    }

    file { "/var/mobi-policy-manager/database/create_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_policy_manager/database/create_databases.sql.erb'),
        require => File["/var/mobi-policy-manager"],
    }

    file { "/var/mobi-policy-manager/database/delete_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_policy_manager/database/delete_databases.sql.erb'),
        require => File["/var/mobi-policy-manager"],
    }
}
