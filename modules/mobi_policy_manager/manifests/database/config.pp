class mobi_policy_manager::database::config {


    #test to see  if we have been run
    exec { "Install  Policy Manager Database ":
        command => "/bin/sh /var/mobi-policy-manager/database/install.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi-policy-manager/database/installation.done",
        logoutput => "true",
        require => File["/var/mobi-policy-manager/database"],
    }
}
