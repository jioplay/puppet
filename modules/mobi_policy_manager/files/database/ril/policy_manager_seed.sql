use POLICY_MGR;

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(-1,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp">
       <o-ex:agreement>
              <o-ex:permission>
                     <o-dd:play/>
                     <o-dd:save/>
              </o-ex:permission>
       </o-ex:agreement>
</o-ex:rights>
', 'C','No Restrictions Policy','No restrictions  policy',NULL,0,0,'2013-01-30 12:16:55',0,NULL,NULL); 

INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000001, '\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="quality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>10240</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>1080</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>1920</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                         <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pad-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'S','Service Policy-Baseline Service Policy','Service Policy-Baseline Service Policy',NULL,0,0,'infotel-r4g-5.0-rest','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000002, '\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="quality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>10240</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>1080</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>1920</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                         <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pad-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Baseline Content Policy','Content Policy-Baseline Content Policy','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';



INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000003, '\0',3,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="quality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>10240</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>1080</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>1920</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                         <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pad-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Baseline Content Policy','Content Policy-Baseline Content Policy','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000004, '\0',4,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="quality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>10240</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>1080</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>1920</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                         <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pad-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Baseline Content Policy','Content Policy-Baseline Content Policy','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000005, '\0',5,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="quality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>10240</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>1080</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>1920</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                         <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pad-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Baseline Content Policy','Content Policy-Baseline Content Policy','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000006, '\0',6,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="quality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>10240</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>1080</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>1920</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                         <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pad-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Baseline Content Policy','Content Policy-Baseline Content Policy','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000007, '\0',7,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="quality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>10240</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>1080</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>1920</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                         <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pad-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Baseline Content Policy','Content Policy-Baseline Content Policy','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000008, '\0',8,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="quality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>10240</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>1080</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>1920</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                         <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pad-iOS"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Baseline Content Policy','Content Policy-Baseline Content Policy','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

COMMIT;

