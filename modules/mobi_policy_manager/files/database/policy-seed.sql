INSERT INTO `POLICY` (`POLICY_ID`, `COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000001, '\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Baseline Content Policy','Content Policy-Baseline Content Policy','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000002,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>400</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>272</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>480</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Quality-Max_H3','Content Policy-Quality-Max_H3','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000003,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="concurrent-streams">
			<o-ex-mobi:maxconcurrentstreams>2</o-ex-mobi:maxconcurrentstreams>
		  </o-ex:constraint>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>900</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>360</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>640</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Quality-Max_H9','Content Policy-Quality-Max_H9','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000004,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Device_pc-other','Content Policy-Streaming Restrictions-Device_pc-other','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000005,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Device_phone-android','Content Policy-Streaming Restrictions-Device_phone-android','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000006,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Device_phone-ios','Content Policy-Streaming Restrictions-Device_phone-ios','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000007,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Device_stb-other','Content Policy-Streaming Restrictions-Device_stb-other','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000008,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Device_tablet-android','Content Policy-Streaming Restrictions-Device_tablet-android','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000009,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Device_tablet-ios','Content Policy-Streaming Restrictions-Device_tablet-ios','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000010,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Location_India','Content Policy-Streaming Restrictions-Location_India','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000011,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Location_United States','Content Policy-Streaming Restrictions-Location_United States','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000012,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Location_United States Minor Outlying Islands','Content Policy-Streaming Restrictions-Location_United States Minor Outlying Islands','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000013,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Network_GSM-CDMA','Content Policy-Streaming Restrictions-Network_GSM-CDMA','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000014,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Network_LTE','Content Policy-Streaming Restrictions-Network_LTE','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000015,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Network_WiFi-Eth','Content Policy-Streaming Restrictions-Network_WiFi-Eth','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000016,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Screen_ExternalDisplay','Content Policy-Streaming Restrictions-Screen_ExternalDisplay','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000017,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="HDCP"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Screen_HDCP','Content Policy-Streaming Restrictions-Screen_HDCP','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';


INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000018,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Screen_Phone','Content Policy-Streaming Restrictions-Screen_Phone','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000019,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="STB"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Screen_STB','Content Policy-Streaming Restrictions-Screen_STB','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000020,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Streaming Restrictions-Screen_Tablet','Content Policy-Streaming Restrictions-Screen_Tablet','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000500,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Baseline Channel Policy','Channel Policy-Baseline Channel Policy','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000501,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>400</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>272</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>480</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Quality-Max_H3','Channel Policy-Quality-Max_H3','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000502,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="concurrent-streams">
			<o-ex-mobi:maxconcurrentstreams>2</o-ex-mobi:maxconcurrentstreams>
		  </o-ex:constraint>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>900</o-ex-mobi:maxbitrate>
                        <o-ex-mobi:maxheight>360</o-ex-mobi:maxheight>
                        <o-ex-mobi:maxwidth>640</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Quality-Max_H9','Channel Policy-Quality-Max_H9','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000503,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Device_pc-other','Channel Policy-Streaming Restrictions-Device_pc-other','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000504,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Device_phone-android','Channel Policy-Streaming Restrictions-Device_phone-android','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000505,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Device_phone-ios','Channel Policy-Streaming Restrictions-Device_phone-ios','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000506,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Device_stb-other','Channel Policy-Streaming Restrictions-Device_stb-other','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000507,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Device_tablet-android','Channel Policy-Streaming Restrictions-Device_tablet-android','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000508,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Device_tablet-ios','Channel Policy-Streaming Restrictions-Device_tablet-ios','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000509,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Location_India','Channel Policy-Streaming Restrictions-Location_India','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000510,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Location_USA','Channel Policy-Streaming Restrictions-Location_USA','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000511,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Location_UMI','Channel Policy-Streaming Restrictions-Location_UMI','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000512,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Network_GSM-CDMA','Channel Policy-Streaming Restrictions-Network_GSM-CDMA','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000513,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Network_LTE','Channel Policy-Streaming Restrictions-Network_LTE','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000514,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Network_WiFi-ETH','Channel Policy-Streaming Restrictions-Network_WiFi-ETH','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000515,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Screen_ExternalDisplay','Channel Policy-Streaming Restrictions-Screen_ExternalDisplay','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000516,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="HDCP"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Screen_HDCP','Channel Policy-Streaming Restrictions-Screen_HDCP','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000517,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Screen_Phone','Channel Policy-Streaming Restrictions-Screen_Phone','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000518,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="STB"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Screen_STB','Channel Policy-Streaming Restrictions-Screen_STB','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000519,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Streaming Restrictions-Screen_Tablet','Channel Policy-Streaming Restrictions-Screen_Tablet','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000999,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
			   <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
			   <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
	 	      </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'S','Service Policy-Baseline Service Policy','Service Policy-Baseline Service Policy',NULL,0,0,'mobitv-reference-5.0-rest','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';


INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000000','2000000000', 'folder' , '2000001', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000001','2000000001', 'folder' , '2000004', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000002','2000000002', 'folder' , '2000005', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000003','2000000003', 'folder' , '2000006', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000004','2000000004', 'folder' , '2000007', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000005','2000000005', 'folder' , '2000008', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000006','2000000006', 'folder' , '2000009', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000007','2000000007', 'folder' , '2000010', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000008','2000000008', 'folder' , '2000011', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000009','2000000009', 'folder' , '2000012', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000010','2000000010', 'folder' , '2000002', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000011','2000000011', 'folder' , '2000003', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000012','2000000012', 'folder' , '2000013', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000013','2000000013', 'folder' , '2000014', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000014','2000000014', 'folder' , '2000015', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000015','2000000015', 'folder' , '2000016', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000016','2000000016', 'folder' , '2000017', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000017','2000000017', 'folder' , '2000018', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000018','2000000018', 'folder' , '2000019', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('2000000019','2000000019', 'folder' , '2000020', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;


INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000021,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxheight>400</o-ex-mobi:maxheight>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Quality-Max_Height_400','Content Policy-Quality-Max_Height_400','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';


INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000022,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                       <o-ex-mobi:maxwidth>300</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Quality-Max_Width_300','Content Policy-Quality-Max_Width_300','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';


INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000023,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>300</o-ex-mobi:maxbitrate>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Quality-Max_Bitrate_300','Content Policy-Quality-Max_Bitrate_300','VOD',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';


INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000520,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="concurrent-streams">
			<o-ex-mobi:maxconcurrentstreams>2</o-ex-mobi:maxconcurrentstreams>
		  </o-ex:constraint>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxheight>400</o-ex-mobi:maxheight>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Quality-Max_Height_400','Channel Policy-Quality-Max_Height_400','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';

INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000521,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="concurrent-streams">
			<o-ex-mobi:maxconcurrentstreams>2</o-ex-mobi:maxconcurrentstreams>
		  </o-ex:constraint>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxwidth>300</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Quality-Max_Width_300','Channel Policy-Quality-Max_Width_300','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';


INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000522,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="concurrent-streams">
			<o-ex-mobi:maxconcurrentstreams>2</o-ex-mobi:maxconcurrentstreams>
		  </o-ex:constraint>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="IND"/>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxbitrate>300</o-ex-mobi:maxbitrate>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Channel Policy-Quality-Max_Bitrate_300','Channel Policy-Quality-Max_Bitrate_300','LIVE',1,0,NULL,'2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';


INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('1999999990','1999999990', 'folder' , '2000021', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script ' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('1999999991','1999999991', 'folder' , '2000022', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('1999999992','1999999992', 'folder' , '2000023', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;


INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000024,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                        <o-ex-mobi:maxheight>360</o-ex-mobi:maxheight>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Quality-Max_Height_360','Content Policy-Quality-Max_Height_360','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';


INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`VID`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(2000025,'\0',2,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX">
    <o-ex:agreement>
        <o-ex:permission>
            <o-dd:play>
                <o-ex:constraint o-ex:id="network">
                    <o-ex:container o-ex:type="in-or">
                        <o-dd:network o-ex:id="gsm"/>
                        <o-dd:network o-ex:id="cdma"/>
                        <o-dd:network o-ex:id="wifi"/>
                        <o-dd:network o-ex:id="eth"/>
                        <o-dd:network o-ex:id="lte"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="device-tuple">
                    <o-ex:container o-ex:type="ex-or">
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="phone-ios"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="pc-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="stb-other"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-android"/>
                        <o-ex-mobi:device-platform-tuple o-ex-mobi:blackscreen="false" o-ex:id="tablet-ios"/>
                    </o-ex:container>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="whitelisted-regions">
                    <o-ex-mobi:spatial-white-position>
                        <o-ex-mobi:regions>
                            <o-ex-mobi:region o-ex:id="USA"/>
                            <o-ex-mobi:region o-ex:id="UMI"/>
                            <o-ex-mobi:region o-ex:id="IND"/>
                        </o-ex-mobi:regions>
                    </o-ex-mobi:spatial-white-position>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="maxquality">
                    <o-ex-mobi:quality>
                       <o-ex-mobi:maxwidth>480</o-ex-mobi:maxwidth>
                    </o-ex-mobi:quality>
                </o-ex:constraint>
                <o-ex:constraint o-ex:id="hdcp">
                    <o-ex:container o-ex:type="ex-or">
                        <o-dd:screen o-ex:id="ExternalDisplay"/>
                        <o-dd:screen o-ex:id="HDCP"/>
                        <o-dd:screen o-ex:id="Phone"/>
                        <o-dd:screen o-ex:id="STB"/>
                        <o-dd:screen o-ex:id="Tablet"/>
                    </o-ex:container>
                </o-ex:constraint>
            </o-dd:play>
        </o-ex:permission>
    </o-ex:agreement>
</o-ex:rights>', 'C','Content Policy-Quality-Max_Width_480','Content Policy-Quality-Max_Width_480','VOD',1,0,'ALL','2013-03-30 12:00:00','PM seed script','2013-03-30 12:00:00','PM seed script') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script';
 

INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('1999999993','1999999993', 'folder' , '2000024', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;
INSERT INTO `POLICY_MGR`.`POLICY_INVENTORY_MAP` (`POLICY_INVENTORY_ID`,`INVENTORY_ID`, `INVENTORY_TYPE`,`POLICY_ID`, `IS_EXPLICIT`, `CREATED_TIME`, `CREATED_BY`, `IS_DELETED`) VALUES ('1999999994','1999999994', 'folder' , '2000025', '1', '2013-09-10 08:33:15', 'PM seed script', '0') ON DUPLICATE KEY UPDATE LAST_UPDATED_BY = 'PM seed script' ;

update `POLICY` set VID = 'ALL' where IS_DEFAULT = true;
 
