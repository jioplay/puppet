CREATE DATABASE IF NOT EXISTS POLICY_MGR;

USE POLICY_MGR;
 
CREATE TABLE IF NOT EXISTS POLICY(
  POLICY_ID BIGINT(20) NOT NULL AUTO_INCREMENT,
  VID VARCHAR(255) DEFAULT NULL,
  COMPLETE_IND CHAR(1) NOT NULL,
  PROVIDER_ID BIGINT(20) DEFAULT NULL,  
  ODRL_CONTENT TEXT,
  POLICY_TYPE VARCHAR(10) NOT NULL,  
  POLICY_NAME VARCHAR(255) NOT NULL,
  DESCRIPTION VARCHAR(500) DEFAULT NULL,
  POLICY_SUB_TYPE VARCHAR(50) DEFAULT NULL,
  IS_DEFAULT TINYINT(1) NOT NULL,
  IS_DELETED TINYINT(1) NOT NULL,  
  CREATED_TIME DATETIME NOT NULL,
  CREATED_BY VARCHAR(255) NOT NULL,
  LAST_UPDATED_TIME DATETIME DEFAULT NULL,
  LAST_UPDATED_BY VARCHAR(255) DEFAULT NULL,  
  PRIMARY KEY (POLICY_ID) USING BTREE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

 
CREATE TABLE IF NOT EXISTS POLICY_INVENTORY_MAP(
  POLICY_INVENTORY_ID BIGINT(20) NOT NULL AUTO_INCREMENT,
  INVENTORY_ID VARCHAR(50) NOT NULL,
  INVENTORY_TYPE VARCHAR(50) NOT NULL,
  POLICY_ID BIGINT(20) NOT NULL,
  IS_EXPLICIT BOOLEAN NOT NULL,
  CREATED_TIME DATETIME NOT NULL,
  CREATED_BY VARCHAR(255) NOT NULL,
  IS_DELETED TINYINT(1) NOT NULL,
  LAST_UPDATED_TIME DATETIME DEFAULT NULL,
  LAST_UPDATED_BY VARCHAR(255) DEFAULT NULL,  
  PRIMARY KEY (POLICY_INVENTORY_ID),
  KEY IDX_POLICY_INVENTORY_MAP_1 (POLICY_ID),
  CONSTRAINT FK_POLICY_INVENTORY_MAP_1 FOREIGN KEY (POLICY_ID) REFERENCES POLICY(POLICY_ID)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



ALTER TABLE POLICY ADD INDEX(VID, IS_DELETED);
ALTER TABLE POLICY ADD INDEX(POLICY_TYPE, POLICY_SUB_TYPE, IS_DELETED);
ALTER TABLE POLICY_INVENTORY_MAP ADD INDEX(INVENTORY_ID, INVENTORY_TYPE, IS_DELETED);



INSERT INTO `POLICY` (`POLICY_ID`,`COMPLETE_IND`,`PROVIDER_ID`,`ODRL_CONTENT`,`POLICY_TYPE`,`POLICY_NAME`,`DESCRIPTION`,`POLICY_SUB_TYPE`,`IS_DEFAULT`,`IS_DELETED`,`CREATED_TIME`,`CREATED_BY`,`LAST_UPDATED_TIME`,`LAST_UPDATED_BY`) VALUES
(-1,'\0',NULL,'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<o-ex:rights xmlns:o-dd="http://odrl.net/1.1/ODRL-DD" xmlns:o-ex="http://odrl.net/1.1/ODRL-EX" xmlns:o-ex-mobi="http://www.mobitv.com/schemas/odrl_ex_cmp">
       <o-ex:agreement>
              <o-ex:permission>
                     <o-dd:play/>
                     <o-dd:save/>
              </o-ex:permission>
       </o-ex:agreement>
</o-ex:rights>
', 'C','No Restrictions Policy','No restrictions  policy',NULL,0,0,'2013-01-30 12:16:55',0,NULL,NULL); 
