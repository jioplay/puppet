class mobi_roapserver::install {
  package { "$mobi_roapserver::package":
    ensure => "${mobi_roapserver::version}",
    notify => Class["tomcat::service"],
  }
}
