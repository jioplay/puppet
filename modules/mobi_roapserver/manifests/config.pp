class mobi_roapserver::config {

    File {
        owner => rtv,
        group => rtv,
        mode => "0644",
    }

    $lm_user = $mobi_roapserver::lm_user
    $lm_user_pass = $mobi_roapserver::lm_user_pass
    $jdbc_driver = $mobi_roapserver::jdbc_driver
    $validation_query = $mobi_roapserver::validation_query
    $db_url = $mobi_roapserver::db_url ? {
        undef => die("You must set the LM database url"),
        default => $mobi_roapserver::db_url
    }

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/roapserver.xml":
        ensure => present,
        content => template("mobi_roapserver/roapserver.xml.erb"),
        require => Class["mobi_roapserver::install"],
        notify => Class["tomcat::service"],
    }
}
