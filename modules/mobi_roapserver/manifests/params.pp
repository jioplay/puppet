class mobi_roapserver::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /roapserver/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $package = "mobi-server-drm-roapserver"
    $version = ""
    $lm_user="mobitv"
    $lm_user_pass="m0bir0ck5!"
    $jdbc_driver="com.psql.jdbc.Driver" # check
    $validation_query="select 1" #check
    $db_url=undef
}
