# == Class: mobi_roapserver
#
# installs mobi_roapserver
#
# === Parameters:
#
#    $package::      the mobi_roapserver package name
#
#    $version::      the mobi_roapserver package version
#
#    $lm_user:: database user
#
#    $lm_user_pass:: database password
#
#    $jdbc_driver:: java db driver
#
#    $validation_query::
#
#    $db_url:: database url
#
# === Requires
#
#    Java
#    Tomcat
#    Mobi_licensemanager2
#
# === Sample Usage
#
#  class { "mobi_roapserver" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_roapserver (
    ###icinga.pp
    $icinga = $mobi_roapserver::params::icinga,
    $icinga_instance = $mobi_roapserver::params::icinga_instance,
    $icinga_cmd_args = $mobi_roapserver::params::icinga_cmd_args,
    ###end icinga.pp
    $package = $mobi_roapserver::params::package,
    $version = $mobi_roapserver::params::version,
    $lm_user=$mobi_roapserver::params::lm_user,
    $lm_user_pass=$mobi_roapserver::params::lm_user_pass,
    $jdbc_driver=$mobi_roapserver::params::jdbc_driver,
    $validation_query=$mobi_roapserver::params::validation_query,
    $db_url=$mobi_roapserver::params::db_url,
)
inherits mobi_roapserver::params {
    include mobi_roapserver::install, mobi_roapserver::config

    anchor { "mobi_roapserver::begin": } -> Class["mobi_roapserver::install"]
    class { "mobi_roapserver::icinga":} ->
    Class["mobi_roapserver::config"] -> anchor { "mobi_roapserver::end": }

    os::motd::register { $name : }
}
