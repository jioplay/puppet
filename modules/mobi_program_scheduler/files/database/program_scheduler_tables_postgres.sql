-- Thanks to Patrick Lightbody for submitting this...
--
-- In your Quartz properties file, you'll need to set 
-- org.quartz.jobStore.driverDelegateClass = org.quartz.impl.jdbcjobstore.PostgreSQLDelegate

--------------------------------------------------------
-- drop objetcs
--------------------------------------------------------

drop schema program cascade;
drop database if exists program_scheduler;
drop tablespace if exists program_data;
drop tablespace if exists program_index;
drop user if exists program_user;
drop user if exists dba;
drop user if exists tblsp_owner;

--------------------------------------------------------
-- create super users
--------------------------------------------------------
CREATE USER dba WITH SUPERUSER LOGIN PASSWORD 'dba';
CREATE USER tblsp_owner WITH SUPERUSER NOLOGIN PASSWORD 'owner';

--------------------------------------------------------
-- create tablespaces
--------------------------------------------------------
SET role tblsp_owner;

CREATE TABLESPACE program_data OWNER tblsp_owner LOCATION '/var/lib/pgsql/9.1/program/data/pg_tblspc/program_data';
CREATE TABLESPACE program_index OWNER tblsp_owner LOCATION '/var/lib/pgsql/9.1/program/data/pg_tblspc/program_index';

--------------------------------------------------------
-- create database
--------------------------------------------------------
SET role dba; 
CREATE DATABASE program_scheduler WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE 'en_US.UTF-8' LC_CTYPE 'en_US.UTF-8' TABLESPACE program_data; 

--------------------------------------------------------
-- drop/create schema
--------------------------------------------------------
\c program_scheduler
SET role dba;

create schema program AUTHORIZATION dba;

CREATE TABLE program.qrtz_job_details
  (
    JOB_NAME VARCHAR(200) NOT NULL,
    JOB_GROUP VARCHAR(200) NOT NULL,
    DESCRIPTION VARCHAR(250) NULL,
    JOB_CLASS_NAME   VARCHAR(250) NOT NULL, 
    IS_DURABLE BOOL NOT NULL,
    IS_VOLATILE BOOL NOT NULL,
    IS_STATEFUL BOOL NOT NULL,
    REQUESTS_RECOVERY BOOL NOT NULL,
    JOB_DATA BYTEA NULL,
	CONSTRAINT pk_qrtz_job_details PRIMARY KEY (JOB_NAME,JOB_GROUP)
) TABLESPACE program_data;

create index idx_qrtz_job_details_01 on program.QRTZ_JOB_DETAILS(REQUESTS_RECOVERY) tablespace program_index;

CREATE TABLE program.qrtz_job_listeners
  (
    JOB_NAME VARCHAR(200) NOT NULL, 
    JOB_GROUP VARCHAR(200) NOT NULL,
    JOB_LISTENER VARCHAR(200) NOT NULL,
	CONSTRAINT pk_qrtz_job_listeners PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER),
	CONSTRAINT fk_qrtz_job_listeners FOREIGN KEY (JOB_NAME,JOB_GROUP) REFERENCES QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP)
) TABLESPACE program_data;

create index idx_QRTZ_JOB_LISTENERS_fk on program.QRTZ_JOB_LISTENERS(JOB_NAME, JOB_GROUP) tablespace program_index;

CREATE TABLE program.qrtz_triggers
  (
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    JOB_NAME  VARCHAR(200) NOT NULL, 
    JOB_GROUP VARCHAR(200) NOT NULL,
    IS_VOLATILE BOOL NOT NULL,
    DESCRIPTION VARCHAR(250) NULL,
    NEXT_FIRE_TIME BIGINT NULL,
    PREV_FIRE_TIME BIGINT NULL,
    PRIORITY INTEGER NULL,
    TRIGGER_STATE VARCHAR(16) NOT NULL,
    TRIGGER_TYPE VARCHAR(8) NOT NULL,
    START_TIME BIGINT NOT NULL,
    END_TIME BIGINT NULL,
    CALENDAR_NAME VARCHAR(200) NULL,
    MISFIRE_INSTR SMALLINT NULL,
    JOB_DATA BYTEA NULL,
	CONSTRAINT pk_qrtz_triggers PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
	CONSTRAINT fk_qrtz_triggers FOREIGN KEY (JOB_NAME,JOB_GROUP) REFERENCES QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP)
) TABLESPACE program_data;

create index idx_QRTZ_TRIGGERS_fk on program.QRTZ_TRIGGERS(JOB_NAME, JOB_GROUP) tablespace program_index;
create index idx_QRTZ_TRIGGERS_02 on program.QRTZ_TRIGGERS (NEXT_FIRE_TIME) tablespace program_index;
create index idx_QRTZ_TRIGGERS_03 on program.QRTZ_TRIGGERS (IS_VOLATILE) tablespace program_index;
create index idx_QRTZ_TRIGGERS_04 on program.QRTZ_TRIGGERS (TRIGGER_STATE) tablespace program_index;
create index idx_QRTZ_TRIGGERS_05 on program.QRTZ_TRIGGERS (NEXT_FIRE_TIME, TRIGGER_STATE) tablespace program_index;

CREATE TABLE program.qrtz_simple_triggers
  (
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    REPEAT_COUNT BIGINT NOT NULL,
    REPEAT_INTERVAL BIGINT NOT NULL,
    TIMES_TRIGGERED BIGINT NOT NULL,
	CONSTRAINT pk_qrtz_simple_triggers PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
	CONSTRAINT fk_qrtz_simple_triggers FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE program_data;

create index idx_QRTZ_SIMPLE_TRIGGERS_fk on program.QRTZ_SIMPLE_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP) tablespace program_index;

CREATE TABLE program.qrtz_cron_triggers
  (
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    CRON_EXPRESSION VARCHAR(120) NOT NULL,
    TIME_ZONE_ID VARCHAR(80),
	CONSTRAINT pk_qrtz_cron_triggers PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
	CONSTRAINT fk_qrtz_cron_triggers FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE program_data;

create index idx_QRTZ_CRON_TRIGGERS_fk on program.QRTZ_CRON_TRIGGERS(TRIGGER_NAME, TRIGGER_GROUP) tablespace program_index;

CREATE TABLE program.qrtz_blob_triggers
  (
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    BLOB_DATA BYTEA NULL,
	CONSTRAINT pk_qrtz_blob_triggers PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
	CONSTRAINT fk_qrtz_blob_triggers FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE program_data;

create index idx_QRTZ_BLOB_TRIGGERS_fk on program.QRTZ_BLOB_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP) tablespace program_index;

CREATE TABLE program.qrtz_trigger_listeners
  (
    TRIGGER_NAME  VARCHAR(200) NOT NULL, 
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    TRIGGER_LISTENER VARCHAR(200) NOT NULL,
	CONSTRAINT pk_qrtz_trigger_listeners PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER),
	CONSTRAINT fk_qrtz_trigger_listeners FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP) REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE program_data;

create index idx_QRTZ_TRIGGER_LISTENERS_fk on program.QRTZ_TRIGGER_LISTENERS(TRIGGER_NAME,TRIGGER_GROUP)
tablespace program_index;

CREATE TABLE program.qrtz_calendars
  (
    CALENDAR_NAME  VARCHAR(200) NOT NULL, 
    CALENDAR BYTEA NOT NULL,
	CONSTRAINT pk_qrtz_calendars PRIMARY KEY (CALENDAR_NAME)
) TABLESPACE program_data;



CREATE TABLE program.qrtz_paused_trigger_grps
  (
    TRIGGER_GROUP  VARCHAR(200) NOT NULL, 
	CONSTRAINT pk_qrtz_paused_trigger_grps PRIMARY KEY (TRIGGER_GROUP)
) TABLESPACE program_data;

CREATE TABLE program.qrtz_fired_triggers 
  (
    ENTRY_ID VARCHAR(95) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    IS_VOLATILE BOOL NOT NULL,
    INSTANCE_NAME VARCHAR(200) NOT NULL,
    FIRED_TIME BIGINT NOT NULL,
    PRIORITY INTEGER NOT NULL,
    STATE VARCHAR(16) NOT NULL,
    JOB_NAME VARCHAR(200) NULL,
    JOB_GROUP VARCHAR(200) NULL,
    IS_STATEFUL BOOL NULL,
    REQUESTS_RECOVERY BOOL NULL,
	CONSTRAINT pk_qrtz_fired_triggers PRIMARY KEY (ENTRY_ID)
) TABLESPACE program_data;

create index idx_QRTZ_FIRED_TRIGGERS_01 on program.QRTZ_FIRED_TRIGGERS (IS_VOLATILE) tablespace program_index;
create index idx_QRTZ_FIRED_TRIGGERS_02 on program.QRTZ_FIRED_TRIGGERS (TRIGGER_NAME, TRIGGER_GROUP) tablespace program_index;
create index idx_QRTZ_FIRED_TRIGGERS_03 on program.QRTZ_FIRED_TRIGGERS (JOB_NAME) tablespace program_index;
create index idx_QRTZ_FIRED_TRIGGERS_04 on program.QRTZ_FIRED_TRIGGERS (JOB_GROUP) tablespace program_index;
create index idx_QRTZ_FIRED_TRIGGERS_05 on program.QRTZ_FIRED_TRIGGERS (IS_STATEFUL) tablespace program_index;
create index idx_QRTZ_FIRED_TRIGGERS_06 on program.QRTZ_FIRED_TRIGGERS (TRIGGER_GROUP) tablespace program_index;
create index idx_QRTZ_FIRED_TRIGGERS_07 on program.QRTZ_FIRED_TRIGGERS (TRIGGER_NAME) tablespace program_index;
create index idx_QRTZ_FIRED_TRIGGERS_09 on program.QRTZ_FIRED_TRIGGERS (REQUESTS_RECOVERY) tablespace program_index;
create index idx_QRTZ_FIRED_TRIGGERS_10 on program.QRTZ_FIRED_TRIGGERS (INSTANCE_NAME) tablespace program_index;

CREATE TABLE program.qrtz_scheduler_state 
  (
    INSTANCE_NAME VARCHAR(200) NOT NULL,
    LAST_CHECKIN_TIME BIGINT NOT NULL,
    CHECKIN_INTERVAL BIGINT NOT NULL,
	CONSTRAINT pk_qrtz_scheduler_state PRIMARY KEY (INSTANCE_NAME)
) TABLESPACE program_data;

CREATE TABLE program.qrtz_locks
  (
    LOCK_NAME  VARCHAR(40) NOT NULL, 
	CONSTRAINT pk_qrtz_locks PRIMARY KEY (LOCK_NAME)
) TABLESPACE program_data;


--------------------------------------------------------
--  insert seed data
--------------------------------------------------------

INSERT INTO program.qrtz_locks values('TRIGGER_ACCESS');
INSERT INTO program.qrtz_locks values('JOB_ACCESS');
INSERT INTO program.qrtz_locks values('CALENDAR_ACCESS');
INSERT INTO program.qrtz_locks values('STATE_ACCESS');
INSERT INTO program.qrtz_locks values('MISFIRE_ACCESS');

commit;

--------------------------------------------------------
--  ddl create user and grant permissions
--------------------------------------------------------
create user program_user LOGIN PASSWORD 'program_user';
grant connect on database program_scheduler to program_user;
grant usage on schema program to program_user;
grant select, insert, update, delete on all tables in schema program to program_user;

