set verify off
--variable db_sid varchar2(2);
--accept db_sid prompt 'Please enter SID: '
set autoprint on


CREATE TABLESPACE  program_data
    LOGGING 
    DATAFILE '/data2/oradata/prdjb/program_data01.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT  100M MAXSIZE  2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

CREATE TABLESPACE  program_index
    LOGGING 
    DATAFILE '/data2/oradata/prdjb/program_index1.dbf' SIZE 100M 
    REUSE AUTOEXTEND 
    ON NEXT 100M MAXSIZE 2000M EXTENT MANAGEMENT LOCAL SEGMENT 
    SPACE MANAGEMENT  AUTO;

-- program role
-- grant object privs to this role

-- program_user user
--CREATE USER program  PROFILE "DEFAULT" 
--    IDENTIFIED BY program 
--    DEFAULT TABLESPACE program_data 
--    TEMPORARY TABLESPACE "TEMP" 
--    ACCOUNT UNLOCK;
--grant connect to program;
--alter user program quota unlimited on program_data;
--alter user program quota unlimited on program_index;

create role program_role;

CREATE USER program_user  PROFILE "DEFAULT" 
    IDENTIFIED BY program_user 
    DEFAULT TABLESPACE program_data 
    TEMPORARY TABLESPACE "TEMP" 
    ACCOUNT UNLOCK;
grant connect to program_user;
grant program_role to program_user;
alter user program_user quota unlimited on program_data;
alter user program_user quota unlimited on program_index;


--DROP TABLE program_user.QRTZ_JOB_LISTENERS;
--DROP TABLE program_user.QRTZ_TRIGGER_LISTENERS;
--DROP TABLE program_user.QRTZ_FIRED_TRIGGERS;
--DROP TABLE program_user.QRTZ_PAUSED_TRIGGER_GRPS;
--DROP TABLE program_user.QRTZ_SCHEDULER_STATE;
--DROP TABLE program_user.QRTZ_LOCKS;
--DROP TABLE program_user.QRTZ_SIMPLE_TRIGGERS;
--DROP TABLE program_user.QRTZ_CRON_TRIGGERS;
--DROP TABLE program_user.QRTZ_BLOB_TRIGGERS;
--DROP TABLE program_user.QRTZ_TRIGGERS;
--DROP TABLE program_user.QRTZ_JOB_DETAILS;
--DROP TABLE program_user.QRTZ_CALENDARS;

CREATE TABLE program_user.QRTZ_JOB_DETAILS(
JOB_NAME VARCHAR2(200) NOT NULL,
JOB_GROUP VARCHAR2(200) NOT NULL,
DESCRIPTION VARCHAR2(250) NULL,
JOB_CLASS_NAME VARCHAR2(250) NOT NULL,
IS_DURABLE VARCHAR2(1) NOT NULL,
IS_VOLATILE VARCHAR2(1) NOT NULL,
IS_STATEFUL VARCHAR2(1) NOT NULL,
REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
JOB_DATA BLOB NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_JOB_DETAILS
add constraint pk_QRTZ_JOB_DETAILS
primary key (JOB_NAME,JOB_GROUP)
using index tablespace program_index;

CREATE TABLE program_user.QRTZ_JOB_LISTENERS (
JOB_NAME VARCHAR2(200) NOT NULL,
JOB_GROUP VARCHAR2(200) NOT NULL,
JOB_LISTENER VARCHAR2(200) NOT NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_JOB_LISTENERS
add constraint pk_QRTZ_JOB_LISTENERS
primary key (JOB_NAME,JOB_GROUP,JOB_LISTENER)
using index tablespace program_index;

alter table program_user.QRTZ_JOB_LISTENERS
add constraint fk_QRTZ_JOB_LISTENERS
foreign key (JOB_NAME,JOB_GROUP) references program_user.QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP);

create index program_user.idx_QRTZ_JOB_LISTENERS on program_user.QRTZ_JOB_LISTENERS(JOB_NAME, JOB_GROUP)
tablespace program_index;


CREATE TABLE program_user.QRTZ_TRIGGERS (
TRIGGER_NAME VARCHAR2(200) NOT NULL,
TRIGGER_GROUP VARCHAR2(200) NOT NULL,
JOB_NAME VARCHAR2(200) NOT NULL,
JOB_GROUP VARCHAR2(200) NOT NULL,
IS_VOLATILE VARCHAR2(1) NOT NULL,
DESCRIPTION VARCHAR2(250) NULL,
NEXT_FIRE_TIME NUMBER(19) NULL,
PREV_FIRE_TIME NUMBER(19) NULL,
PRIORITY INTEGER NULL,
TRIGGER_STATE VARCHAR2(16) NOT NULL,
TRIGGER_TYPE VARCHAR2(8) NOT NULL,
START_TIME NUMBER(19) NOT NULL,
END_TIME NUMBER(19) NULL,
CALENDAR_NAME VARCHAR2(200) NULL,
MISFIRE_INSTR NUMBER(5) NULL,
JOB_DATA BLOB NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_TRIGGERS
add constraint pk_QRTZ_TRIGGERS
primary key (TRIGGER_NAME,TRIGGER_GROUP)
using index tablespace program_index;

alter table program_user.QRTZ_TRIGGERS
add constraint fk_QRTZ_TRIGGERS
foreign key (JOB_NAME,JOB_GROUP) references program_user.QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP);

create index program_user.idx_QRTZ_TRIGGERS on program_user.QRTZ_TRIGGERS(JOB_NAME, JOB_GROUP)
tablespace program_index;



CREATE TABLE program_user.QRTZ_SIMPLE_TRIGGERS (
TRIGGER_NAME VARCHAR2(200) NOT NULL,
TRIGGER_GROUP VARCHAR2(200) NOT NULL,
REPEAT_COUNT NUMBER(19) NOT NULL,
REPEAT_INTERVAL NUMBER(19) NOT NULL,
TIMES_TRIGGERED NUMBER(19) NOT NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_SIMPLE_TRIGGERS
add constraint pk_QRTZ_SIMPLE_TRIGGERS
primary key (TRIGGER_NAME,TRIGGER_GROUP)
using index tablespace program_index;

alter table program_user.QRTZ_SIMPLE_TRIGGERS
add constraint fk_program_QRTZ_SIM_TRIGRS_01
foreign key (TRIGGER_NAME,TRIGGER_GROUP) references program_user.QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP);




CREATE TABLE program_user.QRTZ_CRON_TRIGGERS (
TRIGGER_NAME VARCHAR2(200) NOT NULL,
TRIGGER_GROUP VARCHAR2(200) NOT NULL,
CRON_EXPRESSION VARCHAR2(120) NOT NULL,
TIME_ZONE_ID VARCHAR2(80))
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_CRON_TRIGGERS
add constraint pk_QRTZ_CRON_TRIGGERS
primary key (TRIGGER_NAME,TRIGGER_GROUP)
using index tablespace program_index;

alter table program_user.QRTZ_CRON_TRIGGERS
add constraint fk_QRTZ_CRON_TRIGRS_01
foreign key (TRIGGER_NAME, TRIGGER_GROUP) references program_user.QRTZ_TRIGGERS(TRIGGER_NAME, TRIGGER_GROUP);



CREATE TABLE program_user.QRTZ_BLOB_TRIGGERS (
TRIGGER_NAME VARCHAR2(200) NOT NULL,
TRIGGER_GROUP VARCHAR2(200) NOT NULL,
BLOB_DATA BLOB NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_BLOB_TRIGGERS
add constraint pk_QRTZ_BLOB_TRIGGERS
primary key (TRIGGER_NAME,TRIGGER_GROUP)
using index tablespace program_index;

alter table program_user.QRTZ_BLOB_TRIGGERS
add constraint fk_QRTZ_BLOB_TRIGRS_01
foreign key (TRIGGER_NAME,TRIGGER_GROUP) references program_user.QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP);



CREATE TABLE program_user.QRTZ_TRIGGER_LISTENERS (
TRIGGER_NAME VARCHAR2(200) NOT NULL,
TRIGGER_GROUP VARCHAR2(200) NOT NULL,
TRIGGER_LISTENER VARCHAR2(200) NOT NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_TRIGGER_LISTENERS
add constraint pk_QRTZ_TRIGGER_LISTENERS
primary key (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER)
using index tablespace program_index;

alter table program_user.QRTZ_TRIGGER_LISTENERS
add constraint fk_QRTZ_TRIGGER_LISTENERS
foreign key (TRIGGER_NAME,TRIGGER_GROUP) references program_user.QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP);

create index program_user.idx_QRTZ_TRIGGER_LISTENERS on program_user.QRTZ_TRIGGER_LISTENERS(TRIGGER_NAME, TRIGGER_GROUP)
tablespace program_index;



CREATE TABLE program_user.QRTZ_CALENDARS (
CALENDAR_NAME VARCHAR2(200) NOT NULL,
CALENDAR BLOB NOT NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_CALENDARS
add constraint pk_QRTZ_CALENDARS
primary key (CALENDAR_NAME)
using index tablespace program_index;


CREATE TABLE program_user.QRTZ_PAUSED_TRIGGER_GRPS (
TRIGGER_GROUP VARCHAR2(200) NOT NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_PAUSED_TRIGGER_GRPS
add constraint pk_QRTZ_PAUSED_TRIGGER_GRPS
primary key (TRIGGER_GROUP)
using index tablespace program_index;



CREATE TABLE program_user.QRTZ_FIRED_TRIGGERS (
ENTRY_ID VARCHAR2(95) NOT NULL,
TRIGGER_NAME VARCHAR2(200) NOT NULL,
TRIGGER_GROUP VARCHAR2(200) NOT NULL,
IS_VOLATILE VARCHAR2(1) NOT NULL,
INSTANCE_NAME VARCHAR2(200) NOT NULL,
FIRED_TIME NUMBER(19) NOT NULL,
PRIORITY INTEGER NOT NULL,
STATE VARCHAR2(16) NOT NULL,
JOB_NAME VARCHAR2(200) NULL,
JOB_GROUP VARCHAR2(200) NULL,
IS_STATEFUL VARCHAR2(1) NULL,
REQUESTS_RECOVERY VARCHAR2(1) NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_FIRED_TRIGGERS
add constraint pk_QRTZ_FIRED_TRIGGERS
primary key (ENTRY_ID)
using index tablespace program_index;



CREATE TABLE program_user.QRTZ_SCHEDULER_STATE (
INSTANCE_NAME VARCHAR2(200) NOT NULL,
LAST_CHECKIN_TIME NUMBER(19) NOT NULL,
CHECKIN_INTERVAL NUMBER(19) NOT NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_SCHEDULER_STATE
add constraint pk_QRTZ_SCHEDULER_STATE
primary key (INSTANCE_NAME)
using index tablespace program_index;


CREATE TABLE program_user.QRTZ_LOCKS (
LOCK_NAME VARCHAR2(40) NOT NULL)
TABLESPACE program_data
STORAGE ( INITIAL 10K);

alter table program_user.QRTZ_LOCKS
add constraint pk_QRTZ_LOCKS
primary key (LOCK_NAME)
using index tablespace program_index;


INSERT INTO program_user.QRTZ_LOCKS values('TRIGGER_ACCESS');
INSERT INTO program_user.QRTZ_LOCKS values('JOB_ACCESS');
INSERT INTO program_user.QRTZ_LOCKS values('CALENDAR_ACCESS');
INSERT INTO program_user.QRTZ_LOCKS values('STATE_ACCESS');
INSERT INTO program_user.QRTZ_LOCKS values('MISFIRE_ACCESS');
commit; 


----grant privileges
---@grant_priv.sql LM_ROLE LM;
---@grant_priv.sql DAM_ROLE DAM;
---@grant_priv.sql MP_ROLE MP;
---@grant_priv.sql WFM_ROLE WFM;
---@grant_priv.sql NETPVR_ROLE NETPVR;


set serveroutput on
set echo on
set feed on
set serverout on size 1000000

--- LM
DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_tab is
        select table_name, owner
        from all_tables
        where owner = 'LM';
cursor get_vw is
        select view_name, owner
        from all_views
        where owner = 'LM';

cursor get_seq is
        select sequence_name, sequence_owner
        from all_sequences
        where sequence_owner = 'LM';


BEGIN

cursor_id:=dbms_sql.open_cursor;

FOR tab_rec in get_tab LOOP
   
        l_sql := 'grant select, insert, update, delete on ' || tab_rec.owner || '.' || tab_rec.table_name || ' to LM_ROLE';

        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

FOR vw_rec in get_vw LOOP
        l_sql := 'grant select on ' || vw_rec.owner || '.' || vw_rec.view_name || ' to LM_ROLE';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        begin
           result := dbms_sql.execute(cursor_id);
        end;

END LOOP;

FOR seq_rec in get_seq LOOP

        l_sql := 'grant select on ' || seq_rec.sequence_owner || '.' || seq_rec.sequence_name || ' to LM_ROLE';
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,1);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/


exit

