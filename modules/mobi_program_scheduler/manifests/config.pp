class mobi_program_scheduler::config {
    file { "/opt/mobi-program-scheduler/webapp/WEB-INF/classes/program-scheduler.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_program_scheduler/programscheduler.properties.erb'),
        require => Class["mobi_program_scheduler::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobitv/conf/mobi-program-scheduler/program-scheduler.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_program_scheduler/programscheduler.properties.erb'),
        require => Class["mobi_program_scheduler::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-program-scheduler/webapp/WEB-INF/classes/database.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_program_scheduler/database.properties.erb'),
        require => Class["mobi_program_scheduler::install"],
        notify   => Class["tomcat::service"],
    }
    
    file { "/opt/mobitv/conf/mobi-program-scheduler/database.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_program_scheduler/database.properties.erb'),
        require => Class["mobi_program_scheduler::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-program-scheduler/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_program_scheduler/persistence.xml.erb'),
        require => Class["mobi_program_scheduler::install"],
        notify   => Class["tomcat::service"],
    }
    file { "/opt/mobi-program-scheduler/webapp/WEB-INF/classes/spring/quartzContext.xml":
        replace => $::mobi_program_scheduler::replace_quartz,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_program_scheduler/quartzContext.xml.erb'),
        require => Class["mobi_program_scheduler::install"],
        notify   => Class["tomcat::service"],
    }

     if ($mobi_program_scheduler::ril == true){
        file {"/opt/mobi-program-scheduler/webapp/WEB-INF/classes/spring/dataAccessContext.xml":
            replace => true,
            owner   => "rtv",
            group   => "rtv",
            mode    => "0755",
            source  => "puppet:///modules/mobi_program_scheduler/ril/dataAccessContext.xml",
            require => Class["mobi_program_scheduler::install"],
            notify  => Class["tomcat::service"],
        }
    }
}
