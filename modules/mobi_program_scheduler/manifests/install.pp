class mobi_program_scheduler::install {
  package { "${::mobi_program_scheduler::package}":
    ensure => $::mobi_program_scheduler::package_ensure,
    notify => Class["tomcat::service"],
  }
}
