# == Class: mobi_program_scheduler
#
#  installs programscheduler component
#
# === Parameters:
#
#    $solr_server_url::
#    $legacy_solr_master_url::
#    $update_legacy_solr::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_program_scheduler" :
#           package_ensure => "5.0.0-178058",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_program_scheduler
(
    ###icinga.pp
    $icinga = $mobi_program_scheduler::params::icinga,
    $icinga_instance = $mobi_program_scheduler::params::icinga_instance,
    $icinga_cmd_args = $mobi_program_scheduler::params::icinga_cmd_args,
    ###end icinga.pp
    $amq_broker_url = $mobi_program_scheduler::params::amq_broker_url,
    $vid_carrier = $mobi_program_scheduler::params::vid_carrier,
    $vid_product = $mobi_program_scheduler::params::vid_product,
    $vid_version = $mobi_program_scheduler::params::vid_version,
    $vid_guideprovider = $mobi_program_scheduler::params::vid_guideprovider,
    $solr_server_url = $mobi_program_scheduler::params::solr_server_url,
    $solr_server_timeout = $mobi_program_scheduler::params::solr_server_timeout,
    $solr_server_max_connections = $mobi_program_scheduler::params::solr_server_max_connections,
    $solr_server_max_retries = $mobi_program_scheduler::params::solr_server_max_retries,
    $notification_programstart_enabled = $mobi_program_scheduler::params::notification_programstart_enabled,
    $notification_programstart_cron = $mobi_program_scheduler::params::notification_programstart_cron,
    $notification_programstart_interval = $mobi_program_scheduler::params::notification_programstart_interval,
    $notification_preauthcheck_enabled = $mobi_program_scheduler::params::notification_preauthcheck_enabled,
    $notification_preauthcheck_cron = $mobi_program_scheduler::params::notification_preauthcheck_cron,
    $notification_preauthcheck_interval = $mobi_program_scheduler::params::notification_preauthcheck_interval,
    $notification_housekeepingshared_cron = $mobi_program_scheduler::params::notification_housekeepingshared_cron,
    $notification_housekeepingprivate_cron = $mobi_program_scheduler::params::notification_housekeepingprivate_cron,
    $notification_housekeepingquarantine_cron = $mobi_program_scheduler::params::notification_housekeepingquarantine_cron,
    $notification_housekeepingtstv_cron = $mobi_program_scheduler::params::notification_housekeepingtstv_cron,
    $guide_manager_url = $mobi_program_scheduler::params::guide_manager_url,
    $db_username = $mobi_program_scheduler::params::db_username,
    $db_password = $mobi_program_scheduler::params::db_password,
    $hibernate_database_dialect = $mobi_program_scheduler::params::hibernate_database_dialect,
    $jdbc_driver = $mobi_program_scheduler::params::jdbc_driver,
    $jdbc_url = $mobi_program_scheduler::params::jdbc_url,
    $package_ensure = $::mobi_program_scheduler::params::package_ensure,
    $package = $::mobi_program_scheduler::params::package,
    $application_quarantine_enabled = $::mobi_program_scheduler::params::application_quarantine_enabled,
    $notification_programstart_onepermessage = $::mobi_program_scheduler::params::notification_programstart_onepermessage,
    $application_quarantine_maxperiodindays = $::mobi_program_scheduler::params::application_quarantine_maxperiodindays,
    $application_fetch_interval = $::mobi_program_scheduler::params::application_fetch_interval,
    $db_type = $::mobi_program_scheduler::params::db_type,
    $jdbcjobstore = $::mobi_program_scheduler::params::jdbcjobstore,
    $is_eit_listener_enabled = $::mobi_program_scheduler::params::is_eit_listener_enabled,
    $eit_multicast_address = $::mobi_program_scheduler::params::eit_multicast_address,
    $eit_port = $::mobi_program_scheduler::params::eit_port,
    $eit_config_url = $::mobi_program_scheduler::params::eit_config_url,
    $replace_quartz = $::mobi_program_scheduler::params::replace_quartz,
    $ril = $mobi_program_scheduler::params::ril,
    $program_queue_eit_messages_name = $mobi_program_scheduler::params::program_queue_eit_messages_name,
    $consumer_queue_program_eit_messages_size = $mobi_program_scheduler::params::consumer_queue_program_eit_messages_size,
    $guide_manager_health_url = $mobi_program_scheduler::params::guide_manager_health_url,
)
inherits mobi_program_scheduler::params
{
    anchor { "begin" :} ->
      class { "mobi_program_scheduler::install" : } ->
      class { "mobi_program_scheduler::config" : } ->
    anchor {"end":} ->
    os::motd::register { $name :}
}
