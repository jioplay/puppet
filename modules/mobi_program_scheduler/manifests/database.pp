# == Class: mobi_program_scheduler::database
#
# === Parameters:
#
#   $database_root_password::
#   $db_type::
#   $db_username::
#   $db_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_program_scheduler::database" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_program_scheduler::database (
    $database_root_password = $mobi_program_scheduler::database::params::database_root_password,
    $db_type = $mobi_program_scheduler::database::params::db_type,
    $db_username = $mobi_program_scheduler::database::params::db_username,
    $db_password = $mobi_program_scheduler::database::params::db_password,
    $oracle_system_username = $mobi_program_scheduler::database::params::oracle_system_username,
    $oracle_system_password = $mobi_program_scheduler::database::params::oracle_system_password,
) inherits mobi_program_scheduler::database::params {
    include mobi_program_scheduler::database::install, mobi_program_scheduler::database::config
    anchor { "mobi_program_scheduler::database::begin":} -> Class["mobi_program_scheduler::database::install"]
    Class["mobi_program_scheduler::database::install"] -> Class["mobi_program_scheduler::database::config"] -> anchor { "mobi_program_scheduler::database::end": }
    os::motd::register { $name :}
}
