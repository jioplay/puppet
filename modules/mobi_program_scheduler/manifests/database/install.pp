class mobi_program_scheduler::database::install {

    file { "/var/mobi-program-scheduler":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi-program-scheduler/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_program_scheduler/database",
        require => File["/var/mobi-program-scheduler"],
    }

    file { "/var/mobi-program-scheduler/database/${db_type}":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_program_scheduler/database",
        require => File["/var/mobi-program-scheduler/database"],
    }

    file { "/var/mobi-program-scheduler/database/${db_type}/install.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template("mobi_program_scheduler/${db_type}_database/install.sh.erb"),
        require => File["/var/mobi-program-scheduler/database/${db_type}"],
    }

    file { "/var/mobi-program-scheduler/database/${db_type}/uninstall.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template("mobi_program_scheduler/${db_type}_database/uninstall.sh.erb"),
        require => File["/var/mobi-program-scheduler/database/${db_type}"],
    }

    if ($db_type == "postgres")
    {
        file { "/var/mobi-program-scheduler/database/postgres/postgres_template.sh":
            ensure => present,
            owner  => "rtv",
            group  => "rtv",
            mode   => "0775",
            content => template("mobi_program_scheduler/postgres_database/postgres_template.sh.erb"),
            require => File["/var/mobi-program-scheduler/database/${db_type}"],
        }
    }

    if ($db_type == "mysql")
    {
        file { "/var/mobi-program-scheduler/database/mysql/create_databases.sql":
            ensure => present,
            owner  => "rtv",
            group  => "rtv",
            mode   => "0664",
            content => template("mobi_program_scheduler/mysql_database/create_databases.sql.erb"),
            require => File["/var/mobi-program-scheduler/database/${db_type}"],
        }
    }

    if ($db_type == "mysql")
    {
        file { "/var/mobi-program-scheduler/database/mysql/delete_databases.sql":
            ensure => present,
            owner  => "rtv",
            group  => "rtv",
            mode   => "0664",
            content => template("mobi_program_scheduler/mysql_database/delete_databases.sql.erb"),
            require => File["/var/mobi-program-scheduler/database/${db_type}"],
        }
    }

    if ($db_type == "oracle")
    {
        file { "/var/mobi-program-scheduler/database/oracle/drop_program_database.sql":
            ensure => present,
            owner  => "rtv",
            group  => "rtv",
            mode   => "0664",
            content => template("mobi_program_scheduler/oracle_database/drop_program_database.sql.erb"),
            require => File["/var/mobi-program-scheduler/database/${db_type}"],
        }
    }

   #test to see if we have been run
    if ($db_type == "mysql")
    {
        exec { "Install MYSQL DB":
            command => "/bin/sh /var/mobi-program-scheduler/database/mysql/install.sh",
            onlyif => "/usr/bin/test ! -e /var/mobi-program-scheduler/database/mysql/installation.done",
            logoutput => "true",
            require => File["/var/mobi-program-scheduler/database/${db_type}/install.sh"],
        }
    }
    if ($db_type == "postgres")
    {
        exec { "Install POSTGRES DB STEP 1":
            command => "/bin/sh /var/mobi-program-scheduler/database/postgres/postgres_template.sh",
            user => "postgres",
            require => File["/var/mobi-program-scheduler/database/postgres/postgres_template.sh"],
        }
        exec { "Install POSTGRES DB STEP 2":
            command => "/sbin/service postgresql-9.1 restart",
            logoutput => "true",
            require => Exec["Install POSTGRES DB STEP 1"],
        }
        exec { "Install POSTGRES DB STEP 3":
            command => "/usr/pgsql-9.1/bin/postgres -D /var/lib/pgsql/9.1/program/data &",
            user => "postgres",
            require => Exec["Install POSTGRES DB STEP 2"],
        }
        exec { "Install POSTGRES DB STEP 4":
            command => "/bin/sleep 2",
            logoutput => "true",
            require => Exec["Install POSTGRES DB STEP 3"],
        }
        exec { "Install POSTGRES DB STEP 5":
            command => "/usr/pgsql-9.1/bin/psql -p 5438 -f /var/mobi-program-scheduler/database/postgres/program_scheduler_tables_postgres.sql",
            user => "postgres",
            require => Exec["Install POSTGRES DB STEP 4"],
        }
    }
    if ($db_type == "oracle")
    {
        exec { "Install ORACLE DB":
            command => "sudo -u oracle /var/app/oracle/product/10204/bin/sqlplus -s $oracle_system_username/$oracle_system_password  @/var/mobi-program-scheduler/database/oracle/program_scheduler_tables_oracle_10g.sql",
            user => "oracle",
        }
    }
}
