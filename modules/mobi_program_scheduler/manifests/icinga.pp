# monitoring plugin for icinga
class mobi_program_scheduler::icinga {

  if $::mobi_program_scheduler::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::mobi_program_scheduler::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_program_scheduler module when icinga = true")
    }

    @@nagios_service { "check_http_mobi_program_scheduler_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_program_scheduler::icinga_cmd_args}",
      service_description => "check_http_mobi_program_scheduler",
      notes => "PATH: ${::mobi_program_scheduler::icinga_cmd_args}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_program_scheduler::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }

  }
}

