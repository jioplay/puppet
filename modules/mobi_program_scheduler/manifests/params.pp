class mobi_program_scheduler::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-program-scheduler/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $package = "mobi-program-scheduler"
    $package_ensure = "latest"
    $version = "5.0.0-210408"
    $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100&startupMaxReconnectAttempts=10"
    $vid_carrier = "sprint"
    $vid_product = "mobitv"
    $vid_version = "5.0"
    $vid_guideprovider = "default"

    $solr_server_url = "http://solrmastervip:8080/mobi-solr/"
    $solr_server_timeout = "10000"
    $solr_server_max_connections = "100"
    $solr_server_max_retries = "1"

    $guide_manager_url = "http://guidemanagervip:8080/mobi-guide-manager/guide/v5/lineup"

    $db_username = "program_user"
    $db_password = "program_user"
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $jdbc_driver = "com.mysql.jdbc.Driver"
    $jdbc_url = "jdbc:mysql://mydtbvip/program_scheduler?autoReconnect=true"

    $notification_programstart_enabled = "true"
    $notification_programstart_onepermessage = "true"
    $notification_programstart_cron = "0 0/5 * * * ?"
    $notification_programstart_interval = "5"

    $notification_preauthcheck_enabled = "false"
    $notification_preauthcheck_cron = "0 20 02 ? * *"
    $notification_preauthcheck_interval = "20"

    $notification_housekeepingshared_cron = "0 0 6 ? * *"

    $notification_housekeepingprivate_cron = "0 10 6 ? * *"

    $notification_housekeepingquarantine_cron = "0 20 6 ? * *"

    $notification_housekeepingtstv_cron = "0 20 6 ? * *"

    $application_quarantine_enabled = "true"
    $application_quarantine_maxperiodindays = "30"

    $application_fetch_interval = undef
    $db_type = "mysql"
    $jdbcjobstore = "org.quartz.impl.jdbcjobstore.StdJDBCDelegate"
    $is_eit_listener_enabled = "true"
    $eit_multicast_address = "239.27.59.120"
    $eit_port = "2011"
    $eit_config_url = "/opt/mobitv/conf/mobi-program-scheduler/stubs/channelConfig.xml"
    $replace_quartz = true

    $ril = false

    $program_queue_eit_messages_name = "PROGRAM.EIT_MESSAGES.QUEUE"
    $consumer_queue_program_eit_messages_size = "1"
    $guide_manager_health_url = "http://guidemanagervip:8080/mobi-guide-manager/loadbalancer/health"
}
