# Class: fragwriter::params
#
# This module manages fragwriter paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class fragwriter::params {

    #install.pp
    $package = "mobi-fragwriter"
    $package_ensure = "present"

}
