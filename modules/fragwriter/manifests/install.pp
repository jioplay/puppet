class fragwriter::install {
  package { $::fragwriter::package:
    ensure => $::fragwriter::package_ensure,
  }
}
