# Class: fragwriter
#
# This module manages fragwriter
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class fragwriter (

    $package = $fragwriter::params::package,
    $package_ensure = $fragwriter::params::package_ensure,

) inherits fragwriter::params {
    class { "fragwriter::install": }

    # include module name in motd
    os::motd::register { $name: }
}
