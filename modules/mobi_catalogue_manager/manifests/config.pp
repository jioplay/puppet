class mobi_catalogue_manager::config {

file{ "/opt/mobi-catalogue-manager/webapp/config/catalogueManager.properties":
   ensure   => present,
   owner    => "rtv",
   group    => "rtv",
   mode     => "0755",
   content  => template("mobi_catalogue_manager/catalogueManager.properties.erb"),
  }
}
