## == Class: mobi_catalogue_manager
#
# Manage the mobi_catalogue_manager component.
#
# === Parameters:
#     package
#     package_ensure
#
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_catalogue_manager (
  $package = $mobi_catalogue_manager::params::package,
  $package_ensure =  $mobi_catalogue_manager::params::package_ensure,
  $ipaddress =  $mobi_catalogue_manager::params::ipaddress,
  $social_live_program =  $mobi_catalogue_manager::params::social_live_program,
  $social_live_channel =  $mobi_catalogue_manager::params::social_live_channel,
  $social_linear_epg =  $mobi_catalogue_manager::params::social_linear_epg,
  $social_vod_url =  $mobi_catalogue_manager::params::social_vod_url,
  $fb_appid = $mobi_catalogue_manager::params::fb_appid,
  $carrier = $mobi_catalogue_manager::params::carrier,
  $product = $mobi_catalogue_manager::params::product,
  $version = $mobi_catalogue_manager::params::version,
) inherits mobi_catalogue_manager::params {
    anchor { "mobi_catalogue_manager::begin": } ->
    class { "mobi_catalogue_manager::install": } ->
    class { "mobi_catalogue_manager::config": } ->
    anchor { "mobi_catalogue_manager::end": }

    os::motd::register{ "${name}":}
}
