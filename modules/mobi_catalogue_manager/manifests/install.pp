class mobi_catalogue_manager::install {
  package { "${::mobi_catalogue_manager::package}":
    ensure => $::mobi_catalogue_manager::package_ensure,
    notify => Class["tomcat::service"],
  }
}
