class mobi_catalogue_manager::params {
  $package = "mobi-catalogue-manager"
  $package_ensure = present
  $ipaddress = undef
  $social_live_program = undef
  $social_live_channel = undef
  $social_linear_epg = undef
  $social_vod_url = undef
  $fb_appid = undef
  $carrier = undef
  $product = undef
  $version = undef
}
