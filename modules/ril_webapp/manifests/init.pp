# Class: ril_webapp
#
# This module manages ril_webapp
#
# Parameters:
# version
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class ril_webapp (

 $upgrade = $ril_webapp::params::upgrade,
 $package = $ril_webapp::params::package,
 $version = $ril_webapp::params::version,
 $rewriteRule =  $ril_webapp::params::rewriteRule,
 $DocumentRoot =  $ril_webapp::params::DocumentRoot,
 $proxy_pass_list = $ril_webapp::params::proxy_pass_list,
 $is_sms = $ril_webapp::params::is_sms,


) inherits ril_webapp::params {

  anchor {"ril_webapp::begin":}->
    class{ "ril_webapp::install" :}->
    class{ "ril_webapp::config" :} ->
  anchor {"ril_webapp::end":}

  # include module name in motd
  os::motd::register { "ril_webapp": }
}
