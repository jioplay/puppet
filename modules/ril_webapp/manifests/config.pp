class ril_webapp::config {
  $version = $::ril_webapp::version
  $WebContentDir = "${::ril_webapp::DocumentRoot}/${::ril_webapp::package}"
  file{ "/etc/httpd/conf.d/${::ril_webapp::package}.conf":
      ensure   => present,
      owner    => "root",
      group    => "root",
      mode     => "0644",
      #content  => template("ril_webapp/reliance-trial-html.conf.erb"),
      content  => template("ril_webapp/reliance-trial-html.conf.erb"),
      notify => Class["apache::service"],
    }

  if($ril_webapp::is_sms){
   file{["/var/www/mobitv","/var/www/mobitv/resource","/var/www/mobitv/resource/sprint","/var/www/mobitv/resource/sprint/mobitv","/var/www/mobitv/resource/sprint/mobitv/5.0"]:
    ensure=> directory,
   }
   ->
   file{ "/var/www/mobitv/resource/sprint/mobitv/5.0/sprint_config_file.json":
      ensure   => present,
      owner    => "apache",
      group    => "apache",
      mode     => "0644",
      source  => "puppet:///modules/ril_webapp/sms/sprint_config_file.json",
      notify => Class["apache::service"],
    }
  }
}
