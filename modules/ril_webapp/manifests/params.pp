#
# Class: ril_webapp::params
#
# This module manages ril_webapp paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class ril_webapp::params {

  #############################################################################
  ###  ril_webapp class params
  #############################################################################

  ### install.pp
  $version = undef
  $rewriteRule = undef
  $package = "reliance-trial-html"
  $upgrade = true
  $DocumentRoot = "/var/www/html"
  $proxy_pass_list = []
  $is_sms = false
}
