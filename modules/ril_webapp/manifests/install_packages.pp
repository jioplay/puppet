define ril_webapp::install_packages(){
  package { "${::ril_webapp::package}-${::ril_webapp::version}":
        ensure => present,
        notify => Class["apache::service"],
  }

}
