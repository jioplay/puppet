class ril_webapp::install {
  if ! $::ril_webapp::version{
    fail("Must provide version number of reliance-trial-html rpm")
  }
  else{
    if($::ril_webapp::upgrade){
      ril_webapp::install_packages{"install_upgrade":}
    }
    else{
      ril_webapp::uninstall_packages{"uninstall":}
      ->
      ril_webapp::install_packages{"install":}
    }
  }
}
