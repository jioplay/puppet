define ril_webapp::uninstall_packages(){
  package { $::ril_webapp::package:
        ensure => purged,
  }

}
