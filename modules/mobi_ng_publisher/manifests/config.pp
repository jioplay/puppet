class mobi_ng_publisher::config {

    if $mobi_ng_publisher::pub_txn_configmgr_ng_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_configmgr_ng_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-configmanager/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_configmgr_ng_endpointlist
        }
    }

    if $mobi_ng_publisher::pub_txn_configrepo_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_configrepo_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-configrepo/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_configrepo_endpointlist
        }
    }

    if $mobi_ng_publisher::pub_txn_devdetect_ng_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_devdetect_ng_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-devdetect/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_devdetect_ng_endpointlist
        }
    }

    if $mobi_ng_publisher::pub_txn_freq_ng_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_freq_ng_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-frequent/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_freq_ng_endpointlist
        }
    }

    if $mobi_ng_publisher::pub_txn_icon_ng_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_icon_ng_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-icon/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_icon_ng_endpointlist
        }
    }

    if $mobi_ng_publisher::pub_txn_nightly_ng_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_nightly_ng_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-nightly/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_nightly_ng_endpointlist
        }
    }

    if $mobi_ng_publisher::pub_txn_staticres_ng_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_staticres_ng_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-staticres/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_staticres_ng_endpointlist
        }
    }

    if $mobi_ng_publisher::pub_txn_tiles_ng_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_tiles_ng_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-tiles/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_tiles_ng_endpointlist
        }
    }

    if $mobi_ng_publisher::pub_txn_widgets_ng_endpointlist {
        mobi_ng_publisher::config_helper { "mobi_ng_publisher::pub_txn_widgets_ng_endpointlist":
            filename => "/opt/mobi-publisher/mobi-publisher-transaction-widgets/config/endpoint.host.json",
            endpoint_list => $mobi_ng_publisher::pub_txn_widgets_ng_endpointlist
        }
    }


}

