class mobi_ng_publisher::params {
    $pub_framework_ng_ver="4.7.40-158089"
    $pub_txn_configmgr_ng_ver=undef
    $pub_txn_configrepo_ver=undef
    $pub_txn_devdetect_ng_ver=undef
    $pub_txn_freq_config_ng_ver=undef
    $pub_txn_freq_ng_ver=undef
    $pub_txn_icon_ng_ver=undef
    $pub_txn_nightly_ng_ver=undef
    $pub_txn_staticres_ng_ver=undef
    $pub_txn_tiles_ng_ver=undef
    $pub_txn_widgets_ng_ver=undef

    $pub_txn_configmgr_ng_endpointlist=undef
    $pub_txn_configrepo_endpointlist=undef
    $pub_txn_devdetect_ng_endpointlist=undef
    $pub_txn_freq_ng_endpointlist=undef
    $pub_txn_icon_ng_endpointlist=undef
    $pub_txn_nightly_ng_endpointlist=undef
    $pub_txn_staticres_ng_endpointlist=undef
    $pub_txn_tiles_ng_endpointlist=undef
    $pub_txn_widgets_ng_endpointlist=undef

}
