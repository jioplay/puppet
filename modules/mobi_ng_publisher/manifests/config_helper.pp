define mobi_ng_publisher::config_helper ( $filename , $endpoint_list ) {
    $ng_pub_endpoint_list = $endpoint_list
    file { "${filename}_${endpoint_list}" :
        path => $filename,
        ensure => present,
        owner => "rtv",
        group => "rtv",
        mode => "644",
        content => template("mobi_ng_publisher/list.json.erb"),
        require => Class["mobi_ng_publisher::install"],
    }
}
