# == Class: mobi_ng_publisher
#
# installs mobi_ng_publisher framework and selected transactions
# for each transaction, the caller must provide the RPM version and host enpoint list.
#
# === Parameters:
#
#
# === Requires
#
#    java
#    python
#
# === Sample Usage
#
# Remember: No empty lines between comments and class definition
#
class mobi_ng_publisher (
    $pub_framework_ng_ver = $mobi_ng_publisher::params::pub_framework_ng_ver,
    $pub_txn_configmgr_ng_ver = $mobi_ng_publisher::params::pub_txn_configmgr_ng_ver,
    $pub_txn_configrepo_ver = $mobi_ng_publisher::params::pub_txn_configrepo_ver,
    $pub_txn_devdetect_ng_ver = $mobi_ng_publisher::params::pub_txn_devdetect_ng_ver,
    $pub_txn_freq_config_ng_ver = $mobi_ng_publisher::params::pub_txn_freq_config_ng_ver,
    $pub_txn_freq_ng_ver = $mobi_ng_publisher::params::pub_txn_freq_ng_ver,
    $pub_txn_icon_ng_ver = $mobi_ng_publisher::params::pub_txn_icon_ng_ver,
    $pub_txn_nightly_ng_ver = $mobi_ng_publisher::params::pub_txn_nightly_ng_ver,
    $pub_txn_staticres_ng_ver = $mobi_ng_publisher::params::pub_txn_staticres_ng_ver,
    $pub_txn_tiles_ng_ver = $mobi_ng_publisher::params::pub_txn_tiles_ng_ver,
    $pub_txn_widgets_ng_ver = $mobi_ng_publisher::params::pub_txn_widgets_ng_ver,

    $pub_txn_configmgr_ng_endpointlist = $mobi_ng_publisher::params::pub_txn_configmgr_ng_endpointlist,
    $pub_txn_configrepo_endpointlist = $mobi_ng_publisher::params::pub_txn_configrepo_endpointlist,
    $pub_txn_devdetect_ng_endpointlist = $mobi_ng_publisher::params::pub_txn_devdetect_ng_endpointlist,
    $pub_txn_freq_ng_endpointlist = $mobi_ng_publisher::params::pub_txn_freq_ng_endpointlist,
    $pub_txn_icon_ng_endpointlist = $mobi_ng_publisher::params::pub_txn_icon_ng_endpointlist,
    $pub_txn_nightly_ng_endpointlist = $mobi_ng_publisher::params::pub_txn_nightly_ng_endpointlist,
    $pub_txn_staticres_ng_endpointlist = $mobi_ng_publisher::params::pub_txn_staticres_ng_endpointlist,
    $pub_txn_tiles_ng_endpointlist = $mobi_ng_publisher::params::pub_txn_tiles_ng_endpointlist,
    $pub_txn_widgets_ng_endpointlist = $mobi_ng_publisher::params::pub_txn_widgets_ng_endpointlist,
)
inherits mobi_ng_publisher::params {
    include mobi_ng_publisher::install, mobi_ng_publisher::config
    anchor { "mobi_ng_publisher::begin": } -> Class["mobi_ng_publisher::install"]
    Class["mobi_ng_publisher::config"] -> anchor { "mobi_ng_publisher::end": }
    os::motd::register { $name : }
}
