class mobi_ng_publisher::install {

    package { "mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}":
        ensure => present,
    }

    if $mobi_ng_publisher::pub_txn_configmgr_ng_ver {
        package { "mobi-publisher-transaction-configmanager-ng-${mobi_ng_publisher::pub_txn_configmgr_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
    }

    if $mobi_ng_publisher::pub_txn_configrepo_ver {
        package { "mobi-publisher-transaction-configrepo-${mobi_ng_publisher::pub_txn_configrepo_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
    }

    if $mobi_ng_publisher::pub_txn_devdetect_ng_ver {
        package { "mobi-publisher-transaction-devdetect-ng-${mobi_ng_publisher::pub_txn_devdetect_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
    }

    if $mobi_ng_publisher::pub_txn_freq_ng_ver and $mobi_ng_publisher::pub_txn_freq_config_ng_ver {
        package { "mobi-publisher-transaction-frequent-ng-${mobi_ng_publisher::pub_txn_freq_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
        package { "mobi-publisher-transaction-frequent-config-ng-${mobi_ng_publisher::pub_txn_freq_config_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }


    }

    if $mobi_ng_publisher::pub_txn_icon_ng_ver {
        package { "mobi-publisher-transaction-icon-ng-${mobi_ng_publisher::pub_txn_icon_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
    }


    if $mobi_ng_publisher::pub_txn_nightly_ng_ver {
        package { "mobi-publisher-transaction-nightly-ng-${mobi_ng_publisher::pub_txn_nightly_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
    }

    if $mobi_ng_publisher::pub_txn_staticres_ng_ver {
        package { "mobi-publisher-transaction-staticres-ng-${mobi_ng_publisher::pub_txn_staticres_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
    }

    if $mobi_ng_publisher::pub_txn_tiles_ng_ver {
        package { "mobi-publisher-transaction-tiles-ng-${mobi_ng_publisher::pub_txn_tiles_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
    }

    if $mobi_ng_publisher::pub_txn_widgets_ng_ver {
        package { "mobi-publisher-transaction-widgets-ng-${mobi_ng_publisher::pub_txn_widgets_ng_ver}":
            ensure => present,
            require => Package["mobi-publisher-framework-ng-${mobi_ng_publisher::pub_framework_ng_ver}"],
        }
    }

    package { "mobi-publisher-transaction-configrepo-nfl":
        ensure => absent ,
    }

    package { "mobi-publisher-transaction-staticres-nfl":
        ensure => absent ,
    }

}
