
CREATE DATABASE IF NOT EXISTS sch;
use sch;

drop table if exists SCH_CHANNEL;
drop table if exists SCH_CHANNEL_SCHEDULE;
drop table if exists SCH_LOCK;
drop table if exists SCH_NODE;
drop table if exists SCH_NODE_CHANNEL;
drop table if exists SCH_PLAYLIST;
drop table if exists SCH_PLAYLIST_CLIP;
drop table if exists SCH_PLAYLIST_RULE;

create table IF NOT EXISTS SCH_CHANNEL (ID bigint not null auto_increment, CREATED_TIME datetime, CREATED_BY varchar(255), LAST_UPDATED_BY varchar(255), LAST_UPDATED_TIME datetime, VERSION integer, CURRENT_PLAYLIST_ID bigint, PROVIDER_ID bigint not null, CHANNEL_AD_INTERVAL bigint, NAME varchar(255) not null, BACKEND_CHANNEL varchar(255), ENCODING varchar(255), ENABLED bit, AUDIO_ONLY bit, primary key (ID), unique (NAME));

create table IF NOT EXISTS SCH_CHANNEL_SCHEDULE (ID bigint not null auto_increment unique, CREATED_TIME datetime, CREATED_BY varchar(255), LAST_UPDATED_BY varchar(255), LAST_UPDATED_TIME datetime, VERSION integer, CRON_EXPRESSION varchar(50), PLAYLIST_ID bigint not null, CHANNEL_ID bigint not null, primary key (ID));

create table IF NOT EXISTS SCH_LOCK (ID bigint not null auto_increment unique, LOCK_TYPE varchar(20) not null, primary key (ID));

create table IF NOT EXISTS SCH_NODE (ID bigint not null auto_increment, INSTANCE_NAME varchar(255) not null, INSTANCE_URL varchar(255) not null, LAST_CHECKIN_TIME bigint not null, primary key (ID));

create table IF NOT EXISTS SCH_NODE_CHANNEL (ID bigint not null auto_increment, CHANNEL_NAME varchar(255) not null, NODE_ID bigint not null, primary key (ID));

create table IF NOT EXISTS SCH_PLAYLIST (ID bigint not null auto_increment, CREATED_TIME datetime, CREATED_BY varchar(255), LAST_UPDATED_BY varchar(255), LAST_UPDATED_TIME datetime, VERSION integer, INVENTORY_ID bigint, PROVIDER_ID bigint not null, NAME varchar(255), DISPLAY_NAME varchar(255), DESCRIPTION text, PLAYLIST_TYPE integer, RECURRENCE_TYPE integer, LOCKED_TYPE integer, primary key (ID), unique (NAME, PROVIDER_ID));

create table IF NOT EXISTS SCH_PLAYLIST_CLIP (ID bigint not null auto_increment, INVENTORY_ID bigint not null, PLAYLIST_ID bigint not null, primary key (ID));

create table IF NOT EXISTS SCH_PLAYLIST_RULE (ID bigint not null auto_increment, RULE_CLASS_NAME varchar(100) not null, RULE_ARGS varchar(100), PLAYLIST_ID bigint not null, primary key (ID));

alter table SCH_CHANNEL_SCHEDULE add index FK_CHANNEL_SCHEDULE_1 (PLAYLIST_ID), add constraint FK_CHANNEL_SCHEDULE_1 foreign key (PLAYLIST_ID) references SCH_PLAYLIST (ID);

alter table SCH_CHANNEL_SCHEDULE add index FK_CHANNEL_SCHEDULE_2 (CHANNEL_ID), add constraint FK_CHANNEL_SCHEDULE_2 foreign key (CHANNEL_ID) references SCH_CHANNEL (ID);

alter table SCH_NODE_CHANNEL add index FK_NODE_CHANNEL_1 (NODE_ID), add constraint FK_NODE_CHANNEL_1 foreign key (NODE_ID) references SCH_NODE (ID);

alter table SCH_PLAYLIST_CLIP add index FK_PLAYLIST_CLIP_1 (PLAYLIST_ID), add constraint FK_PLAYLIST_CLIP_1 foreign key (PLAYLIST_ID) references SCH_PLAYLIST (ID);

alter table SCH_PLAYLIST_RULE add index FK_PLAYLIST_RULE_1 (PLAYLIST_ID), add constraint FK_PLAYLIST_RULE_1 foreign key (PLAYLIST_ID) references SCH_PLAYLIST (ID);
