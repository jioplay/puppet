## == Class: mobi_arlanda_scheduler
#
# Manage the mobi_arlanda_scheduler component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_arlanda_scheduler (
  $package_name = $mobi_arlanda_scheduler::params::package_name,
  $package_ensure = $mobi_arlanda_scheduler::params::package_ensure,
  $dam_webservice_url = $mobi_arlanda_scheduler::params::dam_webservice,
  $fm_webservice_url = $mobi_arlanda_scheduler::params::fm_webservice_url,
  $cache_cluster_mode = $mobi_arlanda_scheduler::params::cache_cluster_mode,
  $node_checkin_interval = $mobi_arlanda_scheduler::params::node_checkin_interval,
  $folderplaylist_interval = $mobi_arlanda_scheduler::params::folderplaylist_interval,
  $legacy_ad_interval = $mobi_arlanda_scheduler::params::legacy_ad_interval,
  $guide_interval = $mobi_arlanda_scheduler::params::guide_interval,
  $playout_minutes = $mobi_arlanda_scheduler::params::playout_minutes,
  $playout_insertlist_gap = $mobi_arlanda_scheduler::params::playout_insertlist_gap,
  $playout_error_minutes = $mobi_arlanda_scheduler::params::playout_error_minutes,
  $initial_sleep_time = $mobi_arlanda_scheduler::params::initial_sleep_time,
) inherits mobi_arlanda_scheduler::params {

    anchor { "mobi_arlanda_scheduler::begin": } ->
    class { "mobi_arlanda_scheduler::install": } ->
    class { "mobi_arlanda_scheduler::config": } ->
    anchor { "mobi_arlanda_scheduler::end": }

    os::motd::register{ "${name}":}
}
