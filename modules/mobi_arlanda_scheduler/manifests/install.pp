class mobi_arlanda_scheduler::install {
    package { "${::mobi_arlanda_scheduler::package_name}":
        ensure => "${::mobi_arlanda_scheduler::package_ensure}",
        notify => Class["tomcat::service"],
    }
}
