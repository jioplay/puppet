class mobi_arlanda_scheduler::config {
   file{ "/opt/arlanda/conf/mobi-arlanda-scheduler/scheduler.properties":
        owner  => "rtv",
        group  => "rtv",
        mode => "0755",
        content => template('mobi_arlanda_scheduler/scheduler.properties.erb'),
        notify   => Class["tomcat::service"],
   }
}
