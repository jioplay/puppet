define keepalived::vrrp_instance (
  $instance_name,
  $virt_ip,
  $priority,
  $virtual_router_id,
  $master_or_backup,
  $check_script,
  $no_preempt = false,
  $interval,
  $weight,
  $smtp_server,
){
  if (!defined(Class['keepalived::base_config'])){
    fail('vrrp instances depends on keepalived class')
  }
  if $virt_ip == undef {
    fail("virt_ip required")
  } elsif $instance_name == undef {
    fail("instance_name required")
  } elsif $priority == undef {
    fail("priority required")
  } elsif $virtual_router_id == undef {
    fail("virtual_router_id required")
  } elsif $master_or_backup == undef {
    fail("master_or_backup required")
  } elsif $check_script == undef {
    fail("check_script required")
  }

  $keepalived_conf_path = $keepalived::params::keepalived_conf_path
  concat::fragment{ "keepalived_frag_${instance_name}_${priority}":
      target  => $keepalived_conf_path,
      content => template("${module_name}/vrrp_instance/keepalived.conf.frag.erb"),
      order   => $virtual_router_id,
      notify  => Class['keepalived::service'],
  }
}
