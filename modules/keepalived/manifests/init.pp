class keepalived (
  $instance_name = hiera('keepalived::instance_name',$keepalived::params::instance_name),
  $virt_ip = hiera('keepalived::virt_ip',$keepalived::params::virt_ip),
  $notify_email = hiera('keepalived::notify_email',$keepalived::params::notify_email),
  $priority = hiera('keepalived::priority',$keepalived::params::priority),
  $virtual_router_id = hiera('keepalived::virtual_router_id',$keepalived::params::virtual_router_id),
  $master_or_backup = hiera('keepalived::master_or_backup',$keepalived::params::master_or_backup),
  $notify_from = hiera('keepalived::notify_from',$keepalived::params::notify_from),
  $check_script = hiera('keepalived::check_script',$keepalived::params::check_script),
  $multiple_vrrp_instances = hiera('keepalived::multiple_vrrp_instances',$keepalived::params::multiple_vrrp_instances),
  $interval = hiera('keepalived::interval',$keepalived::params::interval),
  $weight = hiera('keepalived::weight',$keepalived::params::weight),
  $smtp_server = hiera('keepalived::smtp_server',$keepalived::params::smtp_server),
) inherits keepalived::params
{
  include keepalived::install, keepalived::service

  if (!$multiple_vrrp_instances) {
    include keepalived::config
  } else {
    include keepalived::base_config
  }

  # include module name in motd
  os::motd::register { "keepalived": }

}

