# This class file is not called directly
class keepalived::params {

  ### install.pp
  $package = "keepalived"
  $manage_package = "present"

  ### service.pp
  $service = "keepalived"
  $manage_service_ensure = "running"
  $manage_service_enable = "true"

  ### config.pp
  $instance_name = false
  $virt_ip = false
  $priority = false
  $virtual_router_id = false
  $master_or_backup = false
  $notify_email = "devops@mobitv.com"
  $notify_from = "keepalived@${fqdn}"
  $check_script = "/etc/init.d/httpd status"

  $multiple_vrrp_instances = fales
  $keepalived_conf_path = '/etc/keepalived/keepalived.conf'

  $interval = "2"
  $weight = "0"
  $smtp_server = "127.0.0.1"
}
