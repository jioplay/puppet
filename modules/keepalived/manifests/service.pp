class keepalived::service {
    service { $::keepalived::service:
        enable     => $::keepalived::manage_service_enable,
        ensure     => $::keepalived::manage_service_ensure,
        hasstatus  => true,
        hasrestart => true,
        require    => Class["keepalived::install"],
    }
}
