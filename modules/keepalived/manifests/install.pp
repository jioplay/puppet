class keepalived::install {
    package { $::keepalived::package:
        ensure => $::keepalived::manage_package,
    }
}
