class keepalived::base_config {

  $keepalived_conf_path = $keepalived::params::keepalived_conf_path
  concat { $keepalived_conf_path:
    owner => 'root',
    group => 'root',
    mode  => '0644',
    require => Class["keepalived::install"],
    notify  => Class["keepalived::service"],
  }
  if (!defined(Class['concat::setup'])) {
      class { 'concat::setup': }
  }
  concat::fragment{ "keepalived_frag_${instance_name}_${priority}":
      target  => $keepalived_conf_path,
      content => template("${module_name}/vrrp_instance/keepalived.conf.base.erb"),
      order   => '01',
  }
}
