class keepalived::config {

  File {
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
  }

  if keepalived::virt_ip == undef {
    fail("virt_ip required")
  } elsif keepalived::instance_name == undef {
    fail("instance_name required")
  } elsif keepalived::priority == undef {
    fail("priority required")
  } elsif keepalived::virtual_router_id == undef {
    fail("virtual_router_id required")
  } elsif keepalived::master_or_backup == undef {
    fail("master_or_backup required")
  }

  file { "/etc/keepalived/keepalived.conf":
    content => template("keepalived/keepalived.conf.erb"),
    require => Class["keepalived::install"],
    notify  => Class['keepalived::service'],
  }

}
