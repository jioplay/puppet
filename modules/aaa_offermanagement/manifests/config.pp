class aaa_offermanagement::config {

    $database_url = $aaa_offermanagement::database_url
    $database_username = $aaa_offermanagement::database_username
    $cache_package_details = $aaa_offermanagement::cache_package_details

    file { "/opt/mobi-aaa/config/offer-management-mobi2-adapter/applicationProperties.xml":
    ensure => file,
    content => template("aaa_offermanagement/applicationProperties.xml.erb"),
    notify => Class["tomcat::service"],
    require => Class["aaa_offermanagement::install"],
    }
}
