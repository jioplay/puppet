# == Class: aaa_offermanagement
#
#  installs offer_management ng component
#
# === Parameters:
#
#    $offer_management_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "aaa_offermanagement" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class aaa_offermanagement (
    $offer_management_ver = $aaa_offermanagement::params::offer_management_ver,
    $database_url = $aaa_offermanagement::params::database_url,
    $database_username = $aaa_offermanagement::params::database_username,
    $database_password = $aaa_offermanagement::params::database_password,
    $cache_package_details = $aaa_offermanagement::params::cache_package_details,
)
inherits aaa_offermanagement::params {
    include aaa_offermanagement::install, aaa_offermanagement::config
    anchor { "aaa_offermanagement::begin": } -> Class["aaa_offermanagement::install"]
    Class["aaa_offermanagement::config"] -> anchor { "aaa_offermanagement::end" :}
    os::motd::register { $name : }
}
