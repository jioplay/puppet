class opscenter_free::repo {
    yumrepo { $opscenter_free::repo_name:
      name    => $opscenter_free::repo_name,
      baseurl => $opscenter_free::repo_baseurl,
      enabled => 1,
      gpgcheck => 0
   }
}
