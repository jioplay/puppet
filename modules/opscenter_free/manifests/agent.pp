class opscenter_free::agent {
  exec {"unzip opscenter_agent":
    command => "/bin/tar xvzf /usr/share/opscenter/agent.tar.gz -C /tmp; /tmp/agent/bin/install_agent.sh /tmp/agent/opscenter-agent.rpm ${fqdn}",
    unless => "/bin/rpm -qa|/bin/grep -c opscenter-agent",
  }
}
