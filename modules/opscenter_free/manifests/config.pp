class opscenter_free::config {
    File {
        owner => rtv,
        group => rtv,
        mode => "0644",
    }

    file { "/etc/opscenter/opscenterd.conf":
        ensure => present,
        content => template("opscenter_free/opscenterd.conf.erb"),
    }
}
