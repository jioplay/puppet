class opscenter_free::service {
    service { "opscenterd":
        ensure     => running,
        enable     => true,
        hasrestart => true,
        hasstatus  => true,
    }
}
