class opscenter_free::params {

    ##opscenter_free_icinga.pp
    $icinga_opscenter_free = false
    $icinga_instance_opscenter_free = undef
    $icinga_cmd_args_opscenter_free = "-I $::ipaddress -p 8080 -u /opscenter-free/monitoring/health -w 5 -c 10"
    ##opscenter_free_icinga.pp
    $package_opscenter_free = opscenter-free
    $package_ensure_opscenter_free = "present"

    $repo_name = "DataStax Repo for Apache Cassandra"
    $repo_baseurl = "http://rpm.datastax.com/community"

    $opscenterd_conf_hash = {}
}
