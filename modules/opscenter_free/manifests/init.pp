## == Class: opscenter_free
#
# Manage the opscenter_free component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
#
# === Actions:
#
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class opscenter_free (

    ##opscenter_free_icinga.pp
    $icinga_opscenter_free = $opscenter_free::params::icinga_opscenter_free,
    $icinga_instance_opscenter_free = $opscenter_free::params::icinga_instance_opscenter_free,
    $icinga_cmd_args_opscenter_free = $opscenter_free::params::icinga_cmd_argsopscenter_free,
    ##opscenter_free_icinga.pp
    $package_opscenter_free = $opscenter_free::params::package_opscenter_free,
    $package_ensure_opscenter_free = $opscenter_free::params::package_ensure_opscenter_free,

    $repo_name = $opscenter_free::params::repo_name,
    $repo_baseurl = $opscenter_free::params::repo_baseurl,

    $opscenterd_conf_hash =  $opscenter_free::params::opscenterd_conf_hash,

) inherits opscenter_free::params {

    anchor { "opscenter_free::begin": } ->
    class { "opscenter_free::repo": } ->
    class { "opscenter_free::install": } ->
    class { "opscenter_free::config": } ->
    class { "opscenter_free::agent": } ->
    class { "opscenter_free::service": } ->
    anchor { "opscenter_free::end": }

    os::motd::register{ "${name}":}
}
