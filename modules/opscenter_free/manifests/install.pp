class opscenter_free::install {
  package { $opscenter_free::package_opscenter_free:
    ensure => $opscenter_free::package_ensure_opscenter_free,
  }
}
