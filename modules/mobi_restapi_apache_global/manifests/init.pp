#init for mobi_restapi_apache_global
class mobi_restapi_apache_global(
   $mobi_restapi_apache_global_version = $mobi_restapi_apache_global::params::mobi_restapi_apache_global_version,
)
inherits mobi_restapi_apache_global::params{
  include mobi_restapi_apache_global::install
  anchor { "mobi_restapi_apache_global::begin":} -> Class["mobi_restapi_apache_global::install"] ->
  anchor { "mobi_restapi_apache_global::end": }
  os::motd::register { $name : }
}
