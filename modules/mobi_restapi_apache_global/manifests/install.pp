class mobi_restapi_apache_global::install {
    package { "mobi-restapi-apache-global-${mobi_restapi_apache_global::mobi_restapi_apache_global_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
