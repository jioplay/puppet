class nfs::config {

    if ! $nfs::server_ensure {
        $server_exports_ensure = absent
    }
    else
    {
        $server_exports_ensure = present
    }

    if server_ensure {
        file { "/etc/exports":
            ensure  => $nfs::config::server_exports_ensure,
            content => template("nfs/exports.erb"),
            owner   => root,
            group   => root,
            mode    => "0644",
            require => Package[$nfs::nfsutil_pkg]
        }
    }

    #should this be the config in the top of the class
    file { "/etc/sysconfig/nfs":
        ensure  => present,
        source  => "puppet://${::puppetserver}/modules/nfs/nfs.client",
        owner   => root,
        group   => root,
        mode    => "0644",
        require => Package[$nfs::nfsutil_pkg]
    }

    $client_bg = $nfs::client_bg
    $client_intr = $nfs::client_intr
    $client_ghost = $nfs::client_ghost
    $client_rsize = $nfs::client_rsize
    $client_wsize = $nfs::client_wsize
    $client_timeout = $nfs::client_timeout
    $client_adhoc_config = $nfs::client_adhoc_config

    if client_ensure {
         file { "/etc/auto.master":
            ensure  => present,
            content => template("nfs/auto.master.erb"),
            owner   => root,
            group   => root,
            mode    => "0644",
            require => Package[$nfs::autofs_pkg],
        }

        $mount_names = keys($nfs::client_mounts)
        nfs::mount_config {  $nfs::config::mount_names :
            hash => $nfs::client_mounts
        }
    }

}
