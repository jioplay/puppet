class nfs::params {
    $server_ensure = stopped
    $server_enabled = false
    $server_config = ""
    $server_exports = []

    $client_ensure = stopped
    $client_enabled = false
    $client_config = ""
    $client_mounts = {}
    $client_ghost = true
    $client_bg = true
    $client_intr = true
    $client_hard = false
    $client_rsize = undef
    $client_wsize = undef
    $client_timeout = "86400"
    $client_adhoc_config = undef

    $nfsutil_pkg = "nfs-utils"
    $nfsutil_ensure = purged

    $autofs_pkg  = "autofs"
    $autofs_ensure  = purged


    $portmap_name = $::operatingsystemrelease ? {
        /5.*/ => "portmap",
        /6.*/ => "rpcbind",
        default => fail("unknown operatingsystem version ${::operatingsystemrelease}")
    }

    $portmap_pkg = $nfs::params::portmap_name
    $portmap_srvc = $nfs::params::portmap_name
    $portmap_ensure = purged
}
