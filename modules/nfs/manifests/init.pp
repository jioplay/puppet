# == Class: nfs
#
#
# This class installs the nfs clioent and or server
#
# === Parameters
#
#    $config::
#
#    $server_ensure::  boolean (true|false) install the server and turn it on
#    $server_enable::  boolean (true|false) start the server at boot
#    $server_exports:: list ([]) of directories to export
#
#    $client_ensure::  boolean (true|false) install the client and turn it on
#    $client_enable::  boolean (true|false) start the client at boot
#    $client_mounts::  hash ( { <name> => <mount> }) of autofs mount name and location
#    $client_bg::      (true|false) add bg option (D:true)
#    $client_hard::    (true|false) add hard option (D:false)
#    $client_intr::    (true|false) add intr option (D:true)
#    $client_ghost::   (true|false) add --ghost option (D:true)
#    $client_rsize::   read size for mounts   (D: undef) undef disables
#    $client_wsize::   write size for mounts  (D: undef) undef disables
#    $client_timeout:: timeout for nfs mounts (D: 86400) undef disables
#    $client_adhoc_config::   list of other options to implement in the nfs client (e.g. ["udp","soft"] )
#
#    $nfsutil_pkg::    string ("nfs_util_1.1") the main nfs package (D:"nfs_util")
#    $nfsutil_ensure:: ensure (present|purged|latest...) for package resource (D:purged)
#
#    $autofs_pkg::     string ("autofs-1.1") the main nfs package (D:"autofs")
#    $autofs_ensure::  ensure (present|purged|latest...) for package resource (D:purged)
#
#    $portmap_pkg::    string ("portmap-1.1") the main nfs package (D:"portmap")
#    $portmap_srvc::    set to portmap name based on os test.
#    $portmap_ensure:: ensure (present|purged|latest...) for package resource (D:purged)
#
# === Actions
#
#     Installs and configures the nfs client and or server packages.
#
# === Requires
#
# === Example
#
#     class {"nfs":
#         client_ensure => true,
#         client_enable => true,
#         client_mounts => { 'qaid_nas' => '/qaid_nas' }
#         nfsutil_ensure => latest,
#         autofs_ensure  => latest,
#         portmap_ensure => latest,
#     }
#
# [Remember: No empty lines between comments and class definition]
#
class nfs (
    $config  = $nfs::params::config,
    $server_ensure  = $nfs::params::server_ensure,
    $server_enable = $nfs::params::server_enabled,
    $server_exports = $nfs::params::server_exports,
    $client_ensure  = $nfs::params::client_ensure,
    $client_enable  = $nfs::params::client_enable,
    $client_mounts  = $nfs::params::client_mounts,
    $client_bg  = $nfs::params::client_bg,
    $client_intr  = $nfs::params::client_intr,
    $client_hard  = $nfs::params::client_hard,
    $client_ghost  = $nfs::params::client_ghost,
    $client_rsize  = $nfs::params::client_rsize,
    $client_wsize  = $nfs::params::client_wsize,
    $client_timeout  = $nfs::params::client_timeout,
    $client_adhoc_config  = $nfs::params::client_adhoc_config,
    $nfsutil_pkg = $nfs::params::nfsutil_pkg,
    $nfsutil_ensure = $nfs::params::nfsutil_ensure,
    $autofs_pkg  = $nfs::params::autofs_pkg,
    $autofs_ensure  = $nfs::params::autofs_ensure,
    $portmap_pkg = $nfs::params::portmap_pkg,
    $portmap_srvc = $nfs::params::portmap_srvc,
    $portmap_ensure = $nfs::params::portmap_ensure
)
inherits nfs::params {
    include nfs::install, nfs::config, nfs::service
    anchor { "nfs::begin": } -> Class["nfs::install"]
    Class["nfs::config"] -> anchor { "nfs::end": }
    os::motd::register { $name : }
}
