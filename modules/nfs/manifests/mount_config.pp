define nfs::mount_config ( $hash ) {

    $filesource = "puppet:///modules/nfs/auto.${name}"

    #get rid of the mount point forward slash
    $mount_point = regsubst(regsubst($hash[$name],"/",""),"/","_","G")

    file { "/etc/auto.${mount_point}":
      #ensure => present,
      ensure => file,
        source => $filesource,
        owner => root,
        group => root,
        mode => "0644",
    }
}
