class nfs::install {

    package { $nfs::nfsutil_pkg :
        ensure => $nfs::nfsutil_ensure,
    }

    package { $nfs::portmap_pkg :
        ensure => $nfs::portmap_ensure,
    }

    package { $nfs::autofs_pkg :
        ensure => $nfs::autofs_ensure,
    }
}
