class nfs::service {
    #need portmap and nfslock for server or client
    if $nfs::client_ensure or $nfs::server_ensure
    {
        $support_ensure = running
        $support_enable = true
    }
    else
    {
        $support_ensure = stopped
        $support_enable = false
    }

    service { $nfs::portmap_srvc :
        ensure => $nfs::service::support_ensure,
        enable => $nfs::service::support_enable,
        require => Package[$nfs::portmap_pkg],
        subscribe => File["/etc/sysconfig/nfs"],
    }

    service { "nfslock":
        ensure =>  $nfs::service::support_ensure,
        enable =>  $nfs::service::support_enable,
        require => Service[$nfs::portmap_srvc],
        subscribe => [ Class["nfs::install"], File["/etc/sysconfig/nfs"] ],
    }

    service { "nfs":
        ensure => $nfs::server_ensure,
        enable => $nfs::server_enable,
        require => Service["nfslock"],
        subscribe => [ Class["nfs::install"], File["/etc/sysconfig/nfs"] ],
    }

    $mount_names = keys($nfs::client_mounts)
    service { "autofs":
        ensure => $nfs::client_ensure,
        enable => $nfs::client_enable,
        require => [ Service["nfs"],  Nfs::Mount_config[$nfs::service::mount_names] ],
        subscribe => [ Class["nfs::install"], File["/etc/auto.master"], Nfs::Mount_config[$nfs::service::mount_names] ],
    }


}
