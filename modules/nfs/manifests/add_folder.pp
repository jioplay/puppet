define nfs::add_folder( $top_level_directory_name, $folder_name, $folder_owner, $folder_group, $folder_mode ) {
    file { "${nfs::add_folder::top_level_directory_name}/${nfs::add_folder::folder_name}":
        owner => "${nfs::add_folder::folder_owner}",
        group => "${nfs::add_folder::folder_group}",
        mode  => "${nfs::add_folder::folder_mode}",
        ensure => "directory",
    }
}
