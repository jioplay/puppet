define nfs::generate_encodings(
  $vod_base_dir = "/var/Jukebox/vod",
  $encoding = undef
){

  $ret_arr = split($title,':')
  $variant = $ret_arr[0]
  $encoding_dir = $ret_arr[1]

  file{ "${vod_base_dir}/${encoding_dir}":
      ensure => directory,
      owner  => "rtv",
      group  => "arlanda",
  }

  if(! $encoding){
    file{ "${vod_base_dir}/${variant}":
       owner  => "rtv",
       group  => "arlanda",
       mode   => "0777",
       ensure => link,
       target => "${vod_base_dir}/${encoding_dir}",
       before => "File[${vod_base_dir}/${encoding_dir}]",
    }
  }
  else{
    #${vod_base_dir}/${encoding}/ should already exist e.g /var/Jukebox/vod/FMP4
    if ! defined(File["${vod_base_dir}/${encoding}"]){
       file{ "${vod_base_dir}/${encoding}":
          ensure => directory,
          before => File["${vod_base_dir}/${encoding}/${variant}"],
       }
    }
    file{ "${vod_base_dir}/${encoding}/${variant}":
       owner  => "rtv",
       group  => "arlanda",
       mode   => "0777",
       ensure => link,
       target => "${vod_base_dir}/${encoding_dir}",
       before => "File[${vod_base_dir}/${encoding_dir}]",
    }
  }
}
