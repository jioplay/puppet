#init for mobi_publisher_endpoint
class mobi_publisher_endpoint(
   $mobi_publisher_endpoint_version = $mobi_publisher_endpoint::params::mobi_publisher_endpoint_version,
)
inherits mobi_publisher_endpoint::params{
  include mobi_publisher_endpoint::install
  anchor { "mobi_publisher_endpoint::begin":} -> Class["mobi_publisher_endpoint::install"] ->
  anchor { "mobi_publisher_endpoint::end": }
  os::motd::register { $name : }
}
