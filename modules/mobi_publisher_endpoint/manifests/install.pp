class mobi_publisher_endpoint::install {
    package { "mobi-publisher-endpoint-${mobi_publisher_endpoint::mobi_publisher_endpoint_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
