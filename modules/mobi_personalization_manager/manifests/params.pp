class mobi_personalization_manager::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-personalization-manager/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    $package = "mobi-personalization-manager"
    $package_ensure = "present"
    $carrier = "sprint"
    $auth_manager_url="http://authmanagervip:8080/mobi-aaa-stub-identity-manager-oauth2"
    $rights_manager_url="http://rightsmanagervip:8080/mobi-aaa-rights-manager"
    $pm_log_cleanup = "35"
    $auth_log_cleanup = "35"

    $resource_server_type = "application_service"
}
