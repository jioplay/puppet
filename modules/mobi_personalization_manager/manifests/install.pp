class mobi_personalization_manager::install {
    $package_name = $::mobi_personalization_manager::package
    $package_ensure = $::mobi_personalization_manager::package_ensure

    package { "${package_name}":
      ensure => $package_ensure,
      notify => [ Class["tomcat::service"], ]
    }
}
