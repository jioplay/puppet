# Docs needed.
class mobi_personalization_manager(
    ###icinga.pp
    $icinga = $mobi_personalization_manager::params::icinga,
    $icinga_instance = $mobi_personalization_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_personalization_manager::params::icinga_cmd_args,
    ###end icinga.pp
   $carrier =  $mobi_personalization_manager::params::carrier,
   $package = $::mobi_personalization_manager::params::package,
   $package_ensure = $::mobi_personalization_manager::params::package_ensure,
   $auth_manager_url = $::mobi_personalization_manager::params::auth_manager_url,
   $rights_manager_url = $::mobi_personalization_manager::params::rights_manager_url,
   $pm_log_cleanup = $::mobi_personalization_manager::params::pm_log_cleanup,
   $auth_log_cleanup = $::mobi_personalization_manager::params::auth_log_cleanup,
   $resource_server_type = $mobi_personalization_manager::params::resource_server_type,
)
inherits mobi_personalization_manager::params{

  anchor { "mobi_personalization_manager::begin":} ->
    class{"mobi_personalization_manager::install":} ->
    class{"mobi_personalization_manager::config":} ->
    class{"mobi_personalization_manager::icinga":} ->
  anchor { "mobi_personalization_manager::end": }
  os::motd::register { $name : }
}
