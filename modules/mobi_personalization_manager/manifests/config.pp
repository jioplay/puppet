class mobi_personalization_manager::config {
  $carrier =  $mobi_personalization_manager::carrier

  file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes/mobi-personalization-manager.properties":
    replace => true,
    owner  => "rtv",
    group  => "rtv",
    content => template("mobi_personalization_manager/${carrier}/config/mobi-personalization-manager.properties.erb"),
    require => Class["mobi_personalization_manager::install"],
    notify => Class["tomcat::service"],
  }
  if ($carrier == 'sprint'){
   file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes/spring/context/healthcheck.xml":
    source => "puppet:///modules/mobi_personalization_manager/${carrier}/spring/context/healthcheck.xml",
    recurse => true,
    owner  => "rtv",
    group  => "rtv",
    notify => Class["tomcat::service"],
   }
  file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes/spring/context/applicationContext.xml":
    source => "puppet:///modules/mobi_personalization_manager/${carrier}/spring/context/applicationContext.xml",
    recurse => true,
    owner  => "rtv",
    group  => "rtv",
    notify => Class["tomcat::service"],
   }
  }

  if ($carrier == 'managed_services'){
    file {"/opt/mobi-personalization-manager/webapp/WEB-INF/classes/default-adapter.properties":
      source => "puppet:///modules/mobi_personalization_manager/managed_services/properties.d/default-adapter.properties",
      owner  => "rtv",
      group  => "rtv",
      notify => Class["tomcat::service"],
    }
  }

  file { "/opt/mobi-personalization-manager/spring.d":
    source => "puppet:///modules/mobi_personalization_manager/${carrier}/spring.d",
    recurse => true,
    owner  => "rtv",
    group  => "rtv",
    notify => Class["tomcat::service"],
  }

  if ($carrier != 'sprint'){
  file { "/etc/cron.daily/mobi-personalization-manager-cleanup-log":
    replace => true,
    owner  => "root",
    group  => "root",
    mode   => "755",
    content => template("mobi_personalization_manager/${carrier}/cron/mobi-personalization-manager-cleanup-log.erb"),
    require => Class["mobi_personalization_manager::install"],
  }
  }
  if ($carrier == 'infotel'){
      file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes/authnz.xml":
          replace => true,
          owner   => "rtv",
          group   => "rtv",
          mode    => "0755",
          content => template("mobi_personalization_manager/${carrier}/config/authnz.xml.erb"),
          require => Class["mobi_personalization_manager::install"],
      }

    file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes/applicationContext.xml":
        source => "puppet:///modules/mobi_personalization_manager/${carrier}/spring/context/applicationContext.xml",
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode    => "0755",
        require => Class["mobi_personalization_manager::install"],
        notify => Class["tomcat::service"],
     }

      file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes/spring/context/authnz.xml":
          replace => true,
          owner   => "rtv",
          group   => "rtv",
          mode    => "0755",
          content => template("mobi_personalization_manager/${carrier}/config/authnz.xml.erb"),
          require => Class["mobi_personalization_manager::install"],
      }
      file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes/spring/context/applicationContext.xml":
          source => "puppet:///modules/mobi_personalization_manager/${carrier}/spring/context/applicationContext.xml",
          recurse => true,
          owner  => "rtv",
          group  => "rtv",
          notify => Class["tomcat::service"],
      }
  }
}
