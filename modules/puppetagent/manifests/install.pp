class puppetagent::install {
  package { $puppetagent::package:
    ensure   => $puppetagent::package_ensure,
    provider => yum,
  }
}
