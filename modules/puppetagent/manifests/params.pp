# Class: puppet::params
#
# This module manages puppet paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class puppetagent::params {

  ### install.pp
  $package          = "puppet"
  $package_ensure   = present

  ### service.pp
  $service          = "puppet"
  $service_ensure   = running
  $service_enable   = true

  ### config.pp
  $server_name      = "puppet"
  $config_template  = "puppet.conf.erb"
  $runinterval      = 3600
  $configtimeout    = 480
  $waitforcert      = 500
  $environment      = $::environment
  $agent_noop       = false
  $rsyslog          = true
  $manage_config    = true
  $manage_sysconfig = true

  ### icinga.pp
  $icinga           = true
  $icinga_instance  = undef

}
