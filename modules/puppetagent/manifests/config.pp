class puppetagent::config {

    $manage_config    = $puppetagent::manage_config
    $manage_sysconfig = $puppetagent::manage_sysconfig
    $rsyslog          = $puppetagent::rsyslog
    $server_name      = $puppetagent::server_name
    $runinterval      = $puppetagent::runinterval
    $configtimeout    = $puppetagent::configtimeout
    $waitforcert      = $puppetagent::waitforcert
    $environment      = $puppetagent::environment
    $agent_noop       = $puppetagent::agent_noop

    File {
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => 0644,
        require => Class["puppetagent::install"],
    }

    if $puppetagent::manage_config {
        file { "/etc/puppet/puppet.conf":
            content => template("puppetagent/puppet.conf.erb"),
        }
    }

    if $puppetagent::manage_sysconfig {
        file { "/etc/sysconfig/puppet":
            content => template("puppetagent/puppet.erb"),
        }
    }

    if $puppetagent::rsyslog {
        file { "/etc/rsyslog.d/puppet.conf":
            source => "puppet://$puppetserver/modules/puppetagent/rsyslog/puppet.conf",
            notify => Service["rsyslog"],
        }
    }
}
