class puppetagent::icinga {

  if $::puppetagent::icinga {

    if ! $::puppetagent::icinga_instance {
      fail("Must provide icinga_instance parameter to puppet module when icinga = true")
    }

    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    @@nagios_service { "check_proc_puppet_${::fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_proc_puppet!",
      service_description   => "check_proc_puppet",
      normal_check_interval => "20", # less aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::puppetagent::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }
  }
}
