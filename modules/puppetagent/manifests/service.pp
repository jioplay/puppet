class puppetagent::service {
  service { $puppetagent::service:
    ensure      => $puppetagent::service_ensure,
    enable      => $puppetagent::service_enable,
    hasstatus   => true,
    hasrestart  => true,
    subscribe   => Class[puppetagent::config],
  }
}
