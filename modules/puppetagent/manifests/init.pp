# Class: puppetagent
#
# This module manages puppet agent settings
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class puppetagent (

    # install.pp
    $package          = $puppetagent::params::package,
    $package_ensure   = $puppetagent::params::package_ensure,

    # service.pp
    $service          = $puppetagent::params::service,
    $service_ensure   = $puppetagent::params::service_ensure,
    $service_enable   = $puppetagent::params::service_enable,

    # config.pp
    $manage_config    = $puppetagent::params::manage_config,
    $manage_sysconfig = $puppetagent::params::manage_sysconfig,
    $rsyslog          = $puppetagent::params::rsyslog,
    $server_name      = $puppetagent::params::server_name,
    $runinterval      = $puppetagent::params::runinterval,
    $configtimeout    = $puppetagent::params::configtimeout,
    $waitforcert      = $puppetagent::params::waitforcert,
    $environment      = $puppetagent::params::environment,
    $agent_noop       = $puppetagent::params::agent_noop,

    # icinga.pp
    $icinga           = $puppetagent::params::icinga,
    $icinga_instance  = $puppetagent::params::icinga_instance,

) inherits puppetagent::params {

  anchor { "puppetagent::begin": } ->
  class  { "puppetagent::install": } ->
  class  { "puppetagent::config": } ->
  class  { "puppetagent::service": } ->
  class  { "puppetagent::icinga": } ->
  anchor { "puppetagent::end": }

  # include module name in motd
  os::motd::register { $name: }
}
