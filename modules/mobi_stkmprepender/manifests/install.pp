class mobi_stkmprepender::install {

    $package_name = $mobi_stkmprepender::version ? {
        "" => $mobi_stkmprepender::package,
        default => "${mobi_stkmprepender::package}-${mobi_stkmprepender::version}"
    }

    package { "${mobi_stkmprepender::install::package_name}":
        ensure => present,
    }
}
