class mobi_stkmprepender::params {
    ###icinga.pp
    $icinga = false
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-stkmprepender/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    # install.pp variables
    $package = "mobi-stkmprepender"
    $version = ""

    # config.pp variables
    $stkm_service_prefix="stkmprep"
    $content_root=["/var/Jukebox/FMP4"]
    $sku_max_ages=["VODPROGRAM:604800", "VODSERVICE:3600"]
    $default_max_age="120"
}
