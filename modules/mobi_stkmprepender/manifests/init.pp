# == Class: mobi_stkmprepender
#
#  installs mobi_stkmprepender
#
# === Parameters:
#
#    $package::        The mobi_stkmprepender package name (D: mobi-hlstransmuxer)
#
#    $version::        The mobi_stkmprepender package version (D: N/A)
#
#    $stkm_service_prefix::    The prefix that the STKM prepender will listen to (D: stkmprep)
#
#    $content_root::  List of all content roots to search for content (D: ["/var/Jukebox/FMP4"])
#
#    $sku_max_ages:: This is a space separated list of tuples <pattern>:<value> where
#                    <pattern> is an identifier that is search for in the supplied sku.
#                       If this pattern is found then the corresponding value is used as max-age
#                       Note! this is *not* a regexp but a plain string that should be found somewhere
#                             in the supplied sku identifier
#                    <value> an integer expressing the max-age in seconds for the pattern
#
#                    (D: ["VODPROGRAM:604800", "VODSERVICE:3600"] )
#
#    $default_max_age:: Used if no pattern in list above is found in the supplied sku identifier
#                       Value is seconds. (D: 120)
#
# === Requires
#
#     httpd
#
# === Sample Usage
#
#  class { "mobi_stkmprepender" :
#      require => Class["apache::httpd"],
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_stkmprepender (
    ###icinga.pp
    $icinga = $mobi_stkmprepender::params::icinga,
    $icinga_instance = $mobi_stkmprepender::params::icinga_instance,
    $icinga_cmd_args = $mobi_stkmprepender::params::icinga_cmd_args,
    ###end icinga.pp
    $package = $mobi_stkmprepender::params::package,
    $version = $mobi_stkmprepender::params::version,


    $stkm_service_prefix=$mobi_stkmprepender::params::stkm_service_prefix,
    $content_root=$mobi_stkmprepender::params::content_root,
    $sku_max_ages=$mobi_stkmprepender::params::sku_max_ages,
    $default_max_age=$mobi_stkmprepender::params::default_max_age,
)
inherits mobi_stkmprepender::params {

    include mobi_stkmprepender::install, mobi_stkmprepender::config
    anchor { "mobi_stkmprepender::begin": } ->
       Class["mobi_stkmprepender::install"] ->
       Class["mobi_stkmprepender::config"] ->
    anchor { "mobi_stkmprepender::end": }

    os::motd::register { $name : }
}
