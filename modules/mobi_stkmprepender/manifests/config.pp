class mobi_stkmprepender::config {

    File {
        owner => root,
        group => root,
        mode => "0644",
    }

    $stkm_service_prefix=$mobi_stkmprepender::stkm_service_prefix
    $content_root=$mobi_stkmprepender::content_root
    $sku_max_ages=$mobi_stkmprepender::sku_max_ages
    $default_max_age=$mobi_stkmprepender::default_max_age

    file { "/etc/httpd/conf.d/mod_stkmprepender.conf":
        ensure => present,
        content => template("mobi_stkmprepender/mod_stkmprepender.conf.erb"),
        require => Class["mobi_stkmprepender::install"],
        notify => Class["apache::service"],
    }
}