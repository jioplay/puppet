# == Class: mobi_facebook_og_adapter
#
#  installs
#
# === Parameters:
#
#    version::   version of the package
#
# === Requires:
#
#     tomcat
#
# === Sample Usage
#
#  class { 'mobi_facebook_og_adapter':
#           version => '5.0.2-214419'
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_facebook_og_adapter (
    $version = $mobi_facebook_og_adapter::params::version,
)
inherits mobi_facebook_og_adapter::params {

    include mobi_facebook_og_adapter::install
    anchor { "mobi_facebook_og_adapter::begin": } ->
    Class[ "mobi_facebook_og_adapter::install"] ->
    anchor { "mobi_facebook_og_adapter::end": }

    os::motd::register{ $name :}
}
