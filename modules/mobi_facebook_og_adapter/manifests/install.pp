class mobi_facebook_og_adapter::install {

    package { "FacebookAdapter":
      ensure => $mobi_facebook_og_adapter::version,
      name => "mobi-facebook-adapter",
      provider => yum,
      notify => Class["tomcat::service"],
    }

}
