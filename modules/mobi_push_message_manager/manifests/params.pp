class mobi_push_message_manager::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-push-message-manager/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $mobi_push_message_manager_version = "*"
    $mobi_push_message_manager_package = "mobi-push-message-manager"
    $mobi_push_message_manager_package_ensure = present
}
