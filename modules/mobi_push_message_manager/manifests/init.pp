# == Class: mobi_push_message_manager
#
#  Installs Push Message Manager component
#
# === Parameters:
#
#    mobi_push_message_manager_version::   version of the push message manager
#
# === Requires:
#
#    java
#    tomcat
#
# === Sample Usage
#
#       class { 'mobi_push_message_manager':
#           mobi_push_message_manager_version=> '5.4.0-227128',
#       }
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_push_message_manager(
    ###icinga.pp
    $icinga = $mobi_push_message_manager::params::icinga,
    $icinga_instance = $mobi_push_message_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_push_message_manager::params::icinga_cmd_args,
    ###end icinga.pp
    $mobi_push_message_manager_package = $mobi_push_message_manager::params::mobi_push_message_manager_package,
    $mobi_push_message_manager_package_ensure = $mobi_push_message_manager::params::mobi_push_message_manager_package_ensure,
)
inherits mobi_push_message_manager::params{

    anchor { "mobi_push_message_manager::begin":} ->
    class {"mobi_push_message_manager::install": } ->
    class { "mobi_push_message_manager::icinga":} ->
    anchor { "mobi_push_message_manager::end": }

    os::motd::register { $name : }
}
