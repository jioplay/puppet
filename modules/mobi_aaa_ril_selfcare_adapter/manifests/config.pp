class mobi_aaa_ril_selfcare_adapter::config {
 file { "/opt/mobi-aaa-ril-selfcare-adapter/conf/application-config-override.xml":
    ensure => present,
    content => template("mobi_aaa_ril_selfcare_adapter/application-config-override.xml.erb"),
    require => Class["mobi_aaa_ril_selfcare_adapter::install"],
    notify => Class["tomcat::service"],
  }
}
