class mobi_aaa_ril_selfcare_adapter::install {
    package {"${$mobi_aaa_ril_selfcare_adapter::package}-${$mobi_aaa_ril_selfcare_adapter::version}":
      ensure => present,
      require => Class["tomcat::install"],
      notify  => Class["tomcat::service"],
    }
 package { "mobi-aaa-ril-selfcare-adapter":
    ensure => "${mobi_aaa_ril_selfcare_adapter::version}",
    notify => [ Class["tomcat::service"], ]
  }
}
