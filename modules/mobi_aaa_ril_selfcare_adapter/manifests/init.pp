## == Class: mobi_aaa_ril_selfcare_adapter
#
# Manage the mobi_aaa_ril_selfcare_adapter component.
#
# === Parameters:
#List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_aaa_ril_selfcare_adapter (
    $version = $mobi_aaa_ril_selfcare_adapter::params::version,
    $package = $mobi_aaa_ril_selfcare_adapter::params::package,
    $version = $mobi_aaa_ril_selfcare_adapter::params::mobi_aaa_ril_selfcare_adapter_version,
    $database_url = $mobi_aaa_ril_selfcare_adapter::params::database_url,
    $database_user_name = $mobi_aaa_ril_selfcare_adapter::params::database_user_name,
    $database_password = $mobi_aaa_ril_selfcare_adapter::params::database_password,
    $database_driver = $mobi_aaa_ril_selfcare_adapter::params::database_driver,
    $hibernate_dialect = $mobi_aaa_ril_selfcare_adapter::params::hibernate_dialect,
    $hibernate_show_sql = $mobi_aaa_ril_selfcare_adapter::params::hibernate_show_sql,
    $jersey_restclient_connection_timeout = $mobi_aaa_ril_selfcare_adapter::params::jersey_restclient_connection_timeout,
    $jersey_restclient_read_timeout = $mobi_aaa_ril_selfcare_adapter::params::jersey_restclient_read_timeout,
    $jersey_restclient_max_connections = $mobi_aaa_ril_selfcare_adapter::params::jersey_restclient_max_connections,
    $jersey_restclient_enable_client_log = $mobi_aaa_ril_selfcare_adapter::params::jersey_restclient_enable_client_log,
    $offer_manager_endpoint_url = $mobi_aaa_ril_selfcare_adapter::params::offer_manager_endpoint_url,
    $rights_manager_endpoint_url = $mobi_aaa_ril_selfcare_adapter::params::rights_manager_endpoint_url,
    $purchase_manager_endpoint_url = $mobi_aaa_ril_selfcare_adapter::params::purchase_manager_endpoint_url,
) inherits mobi_aaa_ril_selfcare_adapter::params {

    anchor { "mobi_aaa_ril_selfcare_adapter::begin": } ->
    class { "mobi_aaa_ril_selfcare_adapter::install": } ->
    class { "mobi_aaa_ril_selfcare_adapter::config": } ->
    anchor { "mobi_aaa_ril_selfcare_adapter::end": }

    os::motd::register{ "${name}":}
}
