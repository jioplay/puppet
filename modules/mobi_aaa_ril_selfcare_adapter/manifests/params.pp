class mobi_aaa_ril_selfcare_adapter::params {
    $version = undef
    $package = "mobi-aaa-ril-selfcare-adapter"
    $mobi_aaa_ril_selfcare_version = ""
    $database_url = undef
    $database_user_name = undef
    $database_password = undef
    $database_driver = undef
    $hibernate_dialect = undef
    $hibernate_show_sql = "false"
    $jersey_restclient_connection_timeout = undef
    $jersey_restclient_read_timeout = undef
    $jersey_restclient_max_connections = undef
    $jersey_restclient_enable_client_log = undef
    $offer_manager_endpoint_url = undef
    $rights_manager_endpoint_url = undef
    $purchase_manager_endpoint_url = undef
}
