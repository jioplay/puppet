######################################
# Puppet controlled file
######################################
backend fmp4_live_01_backend_01 {
  .host="stfml08p1.mum1.r4g.com";
  .port="80";
  .probe=fmp4_health;
}

director fmp4_live_01 round-robin {
  {
    .backend=fmp4_live_01_backend_01;
  }
}

backend fmp4_live_02_backend_01 {
  .host="stfml01p2.mum1.r4g.com";
  .port="80";
  .probe=fmp4_health;
}

director fmp4_live_02 round-robin {
  {
    .backend=fmp4_live_02_backend_01;
  }
}

backend fmp4_live_03_backend_01 {
  .host="stfml01p3.mum1.r4g.com";
  .port="80";
  .probe=fmp4_health;
}

director fmp4_live_03 round-robin {
  {
    .backend=fmp4_live_03_backend_01;
  }
}

backend fmp4_live_04_backend_01 {
  .host="stfml01p4.mum1.r4g.com";
  .port="80";
  .probe=fmp4_health;
}

director fmp4_live_04 round-robin {
  {
    .backend=fmp4_live_04_backend_01;
  }
}

backend fmp4_variant_01_backend_01 {
  .host="stfml08p1.mum1.r4g.com";
  .port="4311";
  .probe=fragcontroller_health;
}

director fmp4_variant_01 round-robin {
  {
    .backend=fmp4_variant_01_backend_01;
  }
}

backend fmp4_variant_02_backend_01 {
  .host="stfml01p2.mum1.r4g.com";
  .port="4311";
  .probe=fragcontroller_health;
}

director fmp4_variant_02 round-robin {
  {
    .backend=fmp4_variant_02_backend_01;
  }
}

backend fmp4_variant_03_backend_01 {
  .host="stfml01p3.mum1.r4g.com";
  .port="4311";
  .probe=fragcontroller_health;
}

director fmp4_variant_03 round-robin {
  {
    .backend=fmp4_variant_03_backend_01;
  }
}

backend fmp4_variant_04_backend_01 {
  .host="stfml01p4.mum1.r4g.com";
  .port="4311";
  .probe=fragcontroller_health;
}

director fmp4_variant_04 round-robin {
  {
    .backend=fmp4_variant_04_backend_01;
  }
}

backend hls_live_01_backend_01 {
  .host="stmxl03p1.mum1.r4g.com";
  .port="80";
  .probe=hls_health;
}

backend hls_live_01_backend_02 {
  .host="stmxl04p1.mum1.r4g.com";
  .port="80";
  .probe=hls_health;
}

director hls_live_01 round-robin {
  {
    .backend=hls_live_01_backend_01;
  }
  {
    .backend=hls_live_01_backend_02;
  }
}



