class haproxy::service inherits haproxy {

  # /sbin/service fails, changing from redhat provider to init
  service { $service:
    ensure      => $manage_service_ensure,
    enable      => $manage_service_enable,
    hasstatus   => true,
    hasrestart  => false,
    restart     => "/etc/init.d/haproxy reload",
    subscribe   => [Class[haproxy::config], Class[haproxy::fmp4live_files]],
  }
}
