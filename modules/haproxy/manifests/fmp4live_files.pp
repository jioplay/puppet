class haproxy::fmp4live_files (

  $fmp4live_servers_txt          = $haproxy::fmp4live_servers_txt,
  $fmp4live_channels_txt         = $haproxy::fmp4live_channels_txt,
  $fmp4live_variants_txt         = $haproxy::fmp4live_variants_txt,
  $fmp4live_special_channels_txt = $haproxy::fmp4live_special_channels_txt,
) {

  if $::haproxy::fmp4live_files and $::haproxy::manage_config_in_node_manifests {

    file { "/etc/haproxy/fmp4live_files":
      ensure  => directory,
      owner   => "root",
      group   => "root",
      mode    => 0700,
    }

    file { "/etc/haproxy/fmp4live_files/servers.txt":
      ensure    => file,
      content   => $fmp4live_servers_txt,
      checksum  => "md5",
      notify    => Exec["generate_haproxy_cfg_for_fmp4live"],
      require   => File["/etc/haproxy/fmp4live_files"],
    }
    file { "/etc/haproxy/fmp4live_files/channels.txt":
      ensure    => file,
      content   => $fmp4live_channels_txt,
      checksum  => "md5",
      notify    => Exec["generate_haproxy_cfg_for_fmp4live"],
      require   => File["/etc/haproxy/fmp4live_files"],
    }
    file { "/etc/haproxy/fmp4live_files/variants.txt":
      ensure    => file,
      content   => $fmp4live_variants_txt,
      checksum  => "md5",
      notify    => Exec["generate_haproxy_cfg_for_fmp4live"],
      require   => File["/etc/haproxy/fmp4live_files"],
    }
    file { "/etc/haproxy/fmp4live_files/special_channels.txt":
      ensure    => file,
      content   => $fmp4live_special_channels_txt,
      checksum  => "md5",
      notify    => Exec["generate_haproxy_cfg_for_fmp4live"],
      require   => File["/etc/haproxy/fmp4live_files"],
    }
    file { "/etc/haproxy/fmp4live_files/updateConfig.pl":
      ensure    => file,
      source    => "puppet:///modules/haproxy/FMP4Live/scripts/updateConfig.pl",
      mode      => "0755",
      checksum  => "md5",
      notify    => Exec["generate_haproxy_cfg_for_fmp4live"],
      require   => File["/etc/haproxy/fmp4live_files"],
    }
    file { "/etc/haproxy/fmp4live_files/haproxy.cfg-FMP4Live.template":
      ensure    => file,
      source    => "puppet:///modules/haproxy/FMP4Live/scripts/haproxy.cfg-FMP4Live.template",
      checksum  => "md5",
      notify    => Exec["generate_haproxy_cfg_for_fmp4live"],
      require   => File["/etc/haproxy/fmp4live_files"],
    }

    exec { "generate_haproxy_cfg_for_fmp4live":
      path        => ["/bin","/usr/bin"],
      command     => '/bin/bash -c "cd /etc/haproxy/fmp4live_files && /etc/haproxy/fmp4live_files/updateConfig.pl"',
      refreshonly => true,
    }
  }

}
