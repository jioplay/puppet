class haproxy::config inherits haproxy {

  File {
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => Class["haproxy::install"],
  }

  # If we are not managing this by hand.
  if ! $::haproxy::manage_config_in_node_manifests {
      # This must be a static config file.
      if ! $::haproxy::config_template {
          file { "/etc/haproxy/haproxy.cfg":
              source => "puppet://$puppetserver/modules/haproxy/${config_file}",
          }
      }
      # Or it is a template.
      else
      {
          file { "/etc/haproxy/haproxy.cfg":
              content => template("haproxy/${::haproxy::config_template}")
          }
      }
  }

  # place haproxy files
  if $rule_files != undef {
    haproxy::files { $rule_files: }
  }

}
