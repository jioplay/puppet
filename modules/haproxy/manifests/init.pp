# Class: haproxy
#
# This module manages haproxy
#
# Parameters:
#
#   package: Name of the package.
#   manage_package: The ensure parameter for the package.
#   config_file: The static file to use for config.
#   config_template:  The template file to use (D:undef)
#   balance_list:  List of backends (D:[]). Only used if template in use.
#   bind_port: The port HAProxy listenbs on (D:80). Only used if template in use.
#   backend_port: The port that the backends are listening on (D:8080). Only used if template in use.
#   balance_mode: The blance mode (D:uri). Only used if template in use.
#   check_inter: The health check interval (D:10s). Only used if template in use.
#   check_url: The health check interval (D:/varnish/loadbalancer). Only used if template in use.
#   check_method: The health check interval (D:HEAD). Only used if template in use.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class haproxy (

  $package = $haproxy::params::package,
  $manage_package = $haproxy::params::manage_package,

  $service = $haproxy::params::service,
  $manage_service_ensure = $haproxy::params::manage_service_ensure,
  $manage_service_enable = $haproxy::params::manage_service_enable,

  ### config files
  $config_file = $haproxy::params::config_file,
  $config_template = $haproxy::params::config_template,
  $balance_list = $haproxy::params::balance_list,
  $backend_port= $haproxy::params::backend_port,
  $bind_port=$haproxy::params::bind_port,
  $check_inter=$haproxy::params::check_inter,
  $check_method=$haproxy::params::check_method,
  $check_url=$haproxy::params::check_url,
  $balance_mode=$haproxy::params::balance_mode,
  $rule_files = $haproxy::params::rule_files,
  $manage_config_in_node_manifests = $haproxy::params::manage_config_in_node_manifests,

  $rsyslog = true,
  $logstash = false,

  ### fmp4live_files
  $fmp4live_files = false,
  $fmp4live_servers_txt          = $haproxy::params::fmp4live_servers_txt,
  $fmp4live_channels_txt         = $haproxy::params::fmp4live_channels_txt,
  $fmp4live_variants_txt         = $haproxy::params::fmp4live_variants_txt,
  $fmp4live_special_channels_txt = $haproxy::params::fmp4live_special_channels_txt

) inherits haproxy::params {

  anchor { "haproxy::begin": }    ->
  class { "haproxy::install": }   ->
  class { "haproxy::config": }    ->
  class { "haproxy::logstash": }  ->
  anchor { "haproxy::end": }

  class { "haproxy::fmp4live_files": }
  class {"haproxy::service": }

  # include module name in motd
  os::motd::register { "haproxy": }
}
