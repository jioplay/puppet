# Class: haproxy::params
#
# This module manages haproxy paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class haproxy::params {
  ### install.pp
  $package = "haproxy"
  $manage_package = "present"

  ### service.pp
  $service = "haproxy"
  $manage_service_ensure = "running"
  $manage_service_enable = "true"

  ### config.pp
  $config_file = "haproxy.cfg"
  $config_template = undef
  $rsyslog_config_file = "haproxy.rsyslog"
  $rule_files = undef
  $manage_config_in_node_manifests = undef
  $local_config_template = 'logstash.conf.local.erb'
  $balance_list = []
  $backend_port="8080"
  $bind_port="80"
  $check_inter="10s"
  $check_method="HEAD"
  $check_url="/varnish/loadbalancer/health"
  $balance_mode="uri"

  ### fmp4live_files.pp
  $fmp4live_servers_txt = undef
  $fmp4live_channels_txt = undef
  $fmp4live_variants_txt = undef
  $fmp4live_special_channels_txt = undef
}
