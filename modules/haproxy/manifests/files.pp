define haproxy::files {
  $basename = inline_template("<%= name.split('/')[-1] -%>")
  file { "/etc/haproxy/${basename}":
    ensure => file,
    source => "puppet://$puppetserver/modules/haproxy/${name}",
  }
}
