class haproxy::install inherits haproxy {
  package { $package:
    ensure => $manage_package,
  }
}
