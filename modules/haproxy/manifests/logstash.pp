define haproxy::relay_config($lstags = undef, $lstype = undef) {
  @@file { "/etc/logstash/collected/haproxy-relay.conf.${::fqdn}" :
    ensure  => present,
    content => template("logstash/${logstash::params::relay_out_config}"),
    tag     => "logstash_relay",
  }
}

define haproxy::filter_config {
  @@file { "/etc/logstash/filters_collected/haproxy-filter.conf.${::fqdn}" :
    ensure => present,
    source => "puppet://$puppetserver/modules/haproxy/haproxy.logstash.filters",
    tag    => 'logstash_filters'
  }
}

define haproxy::grok_pattern {
  @@file { "/etc/logstash/grok_collected/haproxy.pattern.${::fqdn}" :
    ensure => present,
    source => "puppet://$puppetserver/modules/haproxy/haproxy.logstash.pattern",
    tag    => 'grok',
  }
}

class haproxy::logstash (

  $relay_out_config = $::logstash::params::relay_out_config,

) inherits logstash::params { # inherited so that these params can be in a central location

  if $::haproxy::logstash {

    file { "/etc/logstash/conf.d/haproxy.conf" :
      source  => "puppet://$puppetserver/modules/haproxy/haproxy.logstash",
      owner   => "logstash",
      group   => "logstash",
      notify  => Service["logstash"],
      require => Class["logstash::install"],
    }
  }


  # Create virtual resources for filter config, relay config, and grok pattern
  haproxy::relay_config { "haproxy_logstash${::fqdn}" :
    lstags => ["haproxy"],
    lstype => "haproxy",
  }

  haproxy::filter_config { "haproxy_logstash_filter${::fqdn}" : }
  haproxy::grok_pattern { "haproxy_grok${::fqdn}" : }

}

