#!/usr/bin/perl -w

use strict;
use Cwd;
use vars qw($dir);

$dir = getcwd;
my $pat_dir = "$dir\/patternFiles";


my $variants = `cat variants.txt`;
my @channels = `cat channels.txt`;
chomp($variants);
chomp(@channels);

my @variants2 = split(/\|/, $variants);

foreach my $line (@channels) {
  chomp($line);
  next unless($line =~ /^([A,B,C])\:(.+)$/);
  my $key = $1;
  my $cha = $2;
  my @channels2 = split(/\|/, $cha);

  open(NEWPAT, ">$pat_dir\/patthern_$key");

  foreach my $cha2 (@channels2) {
    foreach my $vari (@variants2) {
      print NEWPAT "/frag-mpm/$vari/$cha2/\n";
      print NEWPAT "/frag/$vari/$cha2/\n";
    }
  }
  close(NEWPAT);
}

print "Done\n";


exit;



