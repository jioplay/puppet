#!/usr/bin/perl -w

use strict;
use Cwd;
use vars qw($dir $conf);

$dir = getcwd;
my $pat_dir = "$dir\/patternFiles";
$conf     = "$dir\/..\/haproxy.cfg-FMP4Live-path-beg";
my $template = "$dir\/haproxy.cfg-FMP4Live.template";


open(CONF,">$conf");
open(TEMP,"$template");

while(<TEMP>) {
  print CONF;
  if(/^\#\#\#BEGIN path_beg\#\#\#/) {
    last;
  }
}

my $variants = `cat variants.txt`;
my @channels = `cat channels.txt`;
chomp($variants);
chomp(@channels);

my @variants2 = split(/\|/, $variants);

foreach my $line (@channels) {
  chomp($line);
  next unless($line =~ /^([A,B,C,D])\:(.+)$/);
  my $key = $1;
  my $cha = $2;
  my @channels2 = split(/\|/, $cha);

  foreach my $cha2 (@channels2) {
    foreach my $vari (@variants2) {
      print CONF "    acl path_fmp4live-$key path_beg /frag/$vari/$cha2/\n";
      print CONF "    acl path_fmp4live-$key path_beg /frag-mpm/$vari/$cha2/\n";
    }
  }
}

##
## Special channels
##
print CONF "\n\n";
print CONF "    ##################\n";
print CONF "    # SPECIAL CHANNELS\n";
print CONF "    ##################\n";

my @specials = `cat special_channels.txt`;
my $variants_special;
my @channels_special;
foreach my $line (@specials) {
    chomp($line);
    if($line =~ /^[A,B,C,D]\:/) {
        push(@channels_special, $line);
    }
    elsif($line =~ /^VARIANTS\:(.+)$/) {
        $variants_special = $1;
    }
}
my @variants_special2 = split(/\|/, $variants_special);
foreach my $line (@channels_special) {
    chomp($line);
    next unless($line =~ /^([A,B,C,D])\:(.+)$/);
    my $key = $1;
    my $cha = $2;
    my @channels_special2 = split(/\|/, $cha);
    foreach my $cha2 (@channels_special2) {
        foreach my $vari (@variants_special2) {
            print CONF "    acl path_fmp4live-$key path_beg /frag/$vari/$cha2/\n";
            print CONF "    acl path_fmp4live-$key path_beg /frag-mpm/$vari/$cha2/\n";
        }
    }
}





my $check;
while(<TEMP>) {
  if(/^\#\#\#END path_beg\#\#\#/) {
    $check = 1;
    print CONF;
    next;
  }
  next unless($check);
  print CONF;
}
close(TEMP);
close(CONF);

print "haproxy.conf has been updated\n";


exit;



