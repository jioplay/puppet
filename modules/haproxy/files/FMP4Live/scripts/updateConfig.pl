#!/usr/bin/perl -w

use strict;
use Cwd;
use vars qw($dir $conf $info $date $conf_back $conf_prod);

chomp($date = `/bin/date +%F_%H:%M:%S`);

$dir = getcwd;
my $pat_dir   = "$dir\/patternFiles";
$conf         = "$dir\/..\/haproxy.cfg-FMP4Live-path-beg";
$conf_back    = "$dir\/..\/haproxy.cfg.$date";
$conf_prod    = "/etc/haproxy/haproxy.cfg";
my $template  = "$dir\/haproxy.cfg-FMP4Live.template";


open(CONF,">$conf");
open(TEMP,"$template");

while(<TEMP>) {
  print CONF;
  if(/^\#\#\#BEGIN path_beg\#\#\#/) {
    last;
  }
}

my $variants = `cat variants.txt | egrep -v ^\#`;
my @channels = `cat channels.txt | egrep -v ^\#`;
chomp($variants);
chomp(@channels);

my @variants2 = split(/\|/, $variants);

foreach my $line (@channels) {
  chomp($line);
  next unless($line =~ /^([A-Z])\:(.+)$/);
  my $key = $1;
  my $cha = $2;
  my @channels2 = split(/\|/, $cha);

  foreach my $cha2 (@channels2) {
    foreach my $vari (@variants2) {
      print CONF "    acl path_fmp4live-$key path_beg /frag/$vari/$cha2/\n";
      print CONF "    acl path_fmp4live-$key path_beg /frag-mpm/$vari/$cha2/\n";
    }
  }
}

##
## Special channels
##
print CONF "\n\n";
print CONF "    ##################\n";
print CONF "    # SPECIAL CHANNELS\n";
print CONF "    ##################\n";

my @specials = `cat special_channels.txt | egrep -v ^\#`;
my $variants_special;
my @channels_special;
foreach my $line (@specials) {
    chomp($line);
    if($line =~ /^[A-Z]\:/) {
        push(@channels_special, $line);
    }
    elsif($line =~ /^VARIANTS\:(.+)$/) {
        $variants_special = $1;
    }
}

if($variants_special) {
  my @variants_special2 = split(/\|/, $variants_special);
  foreach my $line (@channels_special) {
      chomp($line);
      next unless($line =~ /^([A-Z])\:(.+)$/);
      my $key = $1;
      my $cha = $2;
      my @channels_special2 = split(/\|/, $cha);
      foreach my $cha2 (@channels_special2) {
          foreach my $vari (@variants_special2) {
              print CONF "    acl path_fmp4live-$key path_beg /frag/$vari/$cha2/\n";
              print CONF "    acl path_fmp4live-$key path_beg /frag-mpm/$vari/$cha2/\n";
          }
      }
  }
}

##
## Get servers info
##
&get_servers("$dir\/servers.txt");


my $check;
while(<TEMP>) {
    if(/^\#\#\#END path_beg\#\#\#/) {
        $check = 1;
        print CONF;
        next;
    }
    next unless($check);

    if(/^\#\#\#BEGIN use_backend\#\#\#/) {
        print CONF;
        foreach my $group (@{$info->{'groups'}}) {
            print CONF "    use_backend fmp4live-$group if path_fmp4live-$group\n";
        }
        my $nextline = <TEMP>;
        print CONF "$nextline";
        next;
    }

    if(/^\# BACKEND$/) {
        print CONF;
        my $nextline = <TEMP>;
        print CONF "$nextline";

        foreach my $group (@{$info->{'groups'}}) {
            print CONF "backend fmp4live-$group\n";
            print CONF "    balance roundrobin\n";
            print CONF "    option httpchk HEAD /check.txt HTTP/1.0\n";
            print CONF "    http-check disable-on-404\n";

            foreach my $host_ip (@{$info->{'host_ip'}{$group}}) {
                my($host,$ip) = split(/\:/, $host_ip);
                print CONF "    server $host $ip\:80 check inter 10s\n";
            }
            print CONF "\n";
        }
        next;
    }

    if(/^\s+\#stats scope www$/) {
        print CONF;
        foreach my $group (@{$info->{'groups'}}) {
            print CONF "    stats scope fmp4live-$group\n";
        }
        next;
    }

    print CONF;
}
close(TEMP);
close(CONF);


unless(&final_check) {
  print "$conf has error(s) with haproxy syntax check!\n";
  exit(1);
}

print "haproxy.conf has been updated\n";


exit;




sub get_servers($) {
    use strict;
    use vars qw();

    my $txt = shift;
    chomp($txt);
    my @servers = `cat $txt | egrep -v ^\#`;

    foreach my $line (@servers) {
        next unless($line =~ /^([A-Z])\:(\S+\:\d+\.\d+\.\d+\.\d+)$/);
        my $group   = $1;
        my $host_ip = $2;

        push(@{$info->{'groups'}}, $group) unless(grep {/$group/} @{$info->{'groups'}});
        push(@{$info->{'host_ip'}{$group}}, $host_ip);
    }

    return 1;
}


sub final_check() {
  use strict;
  use vars qw();

  my $haproxy = "/usr/sbin/haproxy";
  unless(system("$haproxy -c -f $conf") == 0) {
    print "$conf has error(s) with haproxy syntax check!\n";
    return(undef);
  }

  system "cp -pf $conf_prod $conf_back";
  system "cp -pf $conf ${conf_prod}";
  print "haproxy.conf has been updated\n";

  return 1;
}


