define mobi_epg_ingestor::create_test_file(
  $dir_list = ["/opt/mobi-epg-ingestor/config/mobi_test/",
               "/opt/mobi-epg-ingestor/config/mobi_test/xsl/",
               "/var/tmp/mobi-epg-ingestor/mobi_test/",
               "/var/data/mobi-epg-ingestor/archive/processed/mobi_test/",
               "/var/data/mobi-epg-ingestor/archive/processed/mobi_test/program/",
               "/var/data/mobi-epg-ingestor/archive/error/mobi_test/",
               "/var/data/mobi-epg-ingestor/archive/error/mobi_test/program/",]
){
  File {
    owner  => "rtv",
    group  => "rtv",
    mode   => "0755",
  }

  file { $dir_list:
        ensure => "directory",
  }
  ->
  file{ "/opt/mobi-epg-ingestor/override.d/mobi_test-provider-ingestor.properties":
      ensure => present,
      source => "puppet:///modules/mobi_epg_ingestor/mobi_test-provider-ingestor.properties",
      require => [Class["tomcat::service"]],
  }
  ->
  file{ "/opt/mobi-epg-ingestor/config/mobi_test/xsl/mobi_test.xsl":
      ensure => present,
      source => "puppet:///modules/mobi_epg_ingestor/mobi_test.xsl",
      require => [Class["tomcat::service"]],
  }
  ->
  file{ "/opt/mobi-epg-ingestor/config/mobi_test/providerdata.conf":
      ensure => present,
      content => template("mobi_epg_ingestor/providerdata.conf.erb"),
      require => [Class["tomcat::service"]],
  }
}