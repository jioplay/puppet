define mobi_epg_ingestor::ril::create_static_file(
  $file_path = undef,
  $file_source = undef,
  $mode = undef,
  $owner = undef,
  $group = undef,
  $ensure = undef,
  $command = undef
){
  $exec_title = "ingest_xml_data"
  if (! ($file_path or $file_source or $mode or $owner or $group or $ensure or $command)){
    fail("Need to define \$file_path or \$file_source or \$mode or \$owner or \$group \$ensure or \$command variable")
  }

  file{ $file_path:
      ensure  => present,
      owner   => $owner,
      group   => $group,
      mode    => $mode,
      source  => $file_source,
      notify  => Exec[$exec_title],
  }

  exec{ $exec_title:
    refreshonly => true,
    command => $command,
  }
}
