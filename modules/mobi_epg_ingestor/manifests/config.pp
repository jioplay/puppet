class mobi_epg_ingestor::config {

   file{ "${mobi_epg_ingestor::config_file}":
    ensure   => present,
    owner    => "rtv",
    group    => "rtv",
    mode     => "0744",
    content  => template("mobi_epg_ingestor/epg_ingestor.properties.erb"),
  }

  file { ["/var/tmp/mobi-epg-ingestor/"]:
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
  }
  file { ["/var/data/mobi-publisher-externallinkdata","/var/data/mobi-publisher-externallinkdata/externallink"]:
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
  }

 if $::mobi_epg_ingestor::mobi_channel_configuration=="dam" {
   file { "/opt/mobi-epg-ingestor/config/${mobi_channel_configuration}":
     ensure => "directory",
     owner  => "rtv",
     group  => "rtv",
     mode   => "0755",
   }
   file { "/opt/mobi-epg-ingestor/config/${mobi_channel_configuration}/channel":
     source => "puppet:///modules/mobi_core_convergence/mobi_epg_ingestor/channel/${mobi_channel_configuration}",
     recurse => true,
     owner  => "rtv",
     group  => "rtv",
     mode => "0755",
     notify   => Class["tomcat::service"],
     require => Class["mobi_epg_ingestor::install"],
   }
 }
 if $::mobi_epg_ingestor::mobi_channel_configuration=="static" {
      file { "/var/data/mobi-publisher-channeldata/channel/channel_partner.xml":
        source => "puppet:///modules/mobi_core_convergence/mobi_epg_ingestor/channel/channel_partner.xml",
        recurse => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0755",
        notify   => Class["tomcat::service"],
        require => Class["mobi_epg_ingestor::install"],
     }
   }
}
