define mobi_epg_ingestor::create_property_file(
  $property_list = undef
){

  if (! $property_list){
    fail("Need to define \$property_list variable")
  }

  file { ["/opt/mobi-epg-ingestor/override.d/"]:
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
  }
  ->
  file{ "/opt/mobi-epg-ingestor/override.d/${name}-provider-ingestor.properties":
      ensure => present,
      owner  => "rtv",
      group  => "rtv",
      mode   => "0744",
      content => template("mobi_epg_ingestor/create_property_file.erb"),
      require => [Class["tomcat::service"]],
  }
}
