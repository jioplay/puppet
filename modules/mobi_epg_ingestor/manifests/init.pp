class mobi_epg_ingestor(
    ###icinga.pp
    $icinga = $mobi_epg_ingestor::params::icinga,
    $icinga_instance = $mobi_epg_ingestor::params::icinga_instance,
    $icinga_cmd_args = $mobi_epg_ingestor::params::icinga_cmd_args,
    ###end icinga.pp
   $package = $mobi_epg_ingestor::params::package,
   $package_ensure = $mobi_epg_ingestor::params::package_ensure,
   $ingestor_enabled = $mobi_epg_ingestor::params::ingestor_enabled,
   $channel_data_file = $mobi_epg_ingestor::params::channel_data_file,
   $partner_xsl_file = $mobi_epg_ingestor::params::partner_xsl_file,
   $source_data_file = $mobi_epg_ingestor::params::source_data_file,
   $tms_stage_folder = $mobi_epg_ingestor::params::tms_stage_folder,
   $partner_stage_folder = $mobi_epg_ingestor::params::partner_stage_folder,
   $xsl_file = $mobi_epg_ingestor::params::xsl_file,
   $solr_archive_server_url = $mobi_epg_ingestor::params::solr_archive_server_url,
   $config_folder = $mobi_epg_ingestor::params::config_folder,
   $channel_data_config = $mobi_epg_ingestor::params::channel_data_config,
   $externallink_xsl_file = $mobi_epg_ingestor::params::externallink_xsl_file,
   $partner_config_folder = $mobi_epg_ingestor::params::partner_config_folder,
   $ftpupload_queue_consumers_size = $mobi_epg_ingestor::params::ftpupload_queue_consumers_size,
   $channel_xsl_file = $mobi_epg_ingestor::params::channel_xsl_file,
   $externallink_config_folder = $mobi_epg_ingestor::params::externallink_config_folder,
   $externallink_stage_folder  = $mobi_epg_ingestor::params::externallink_stage_folder ,
   $dam_list_channels_url = $mobi_epg_ingestor::params::dam_list_channels_url,
   $mq_broker_url = $mobi_epg_ingestor::params::mq_broker_url,
   $metadataprocessing_queue_consumers_size = $mobi_epg_ingestor::params::metadataprocessing_queue_consumers_size,
   $mobi_externallink_configuration = $mobi_epg_ingestor::params::mobi_externallink_configuration,
   $channel_config_folder = $mobi_epg_ingestor::params::channel_config_folder,
   $dam_list_cliplinears_url = $mobi_epg_ingestor::params::dam_list_cliplinears_url,
   $tms_config_folder = $mobi_epg_ingestor::params::tms_config_folder,
   $dam_url = $mobi_epg_ingestor::params::dam_url,
   $dam_list_externallinks_url = $mobi_epg_ingestor::params::dam_list_externallinks_url,
   $channel_stage_folder  = $mobi_epg_ingestor::params::channel_stage_folder ,
   $mobi_channel_configuration = $mobi_epg_ingestor::params::mobi_channel_configuration,
   $oms_live_media_skus_lookup_url = $mobi_epg_ingestor::params::oms_live_media_skus_lookup_url,
   $search_policies_for_asset_url = $mobi_epg_ingestor::params::search_policies_for_asset_url,
   $tms_config_file = $mobi_epg_ingestor::params::tms_config_file,
   $externallink_data_config = $mobi_epg_ingestor::params::externallink_data_config,
   $channel_dam_config = $mobi_epg_ingestor::params::channel_dam_config,
   $provider_conf_location = $mobi_epg_ingestor::params::provider_conf_location,

   #channel ingestion properties
   $solr_server_url=$mobi_epg_ingestor::params::solr_server_url,
   $solr_socket_timeout=$mobi_epg_ingestor::params::solr_socket_timeout,
   $solr_conn_timeout=$mobi_epg_ingestor::params::solr_conn_timeout,
   $solr_conn_per_host=$mobi_epg_ingestor::params::solr_conn_per_host,
   $solr_total_conn=$mobi_epg_ingestor::params::solr_total_conn,
   $solr_max_retries=$mobi_epg_ingestor::params::solr_max_retries,
   $solr_max_rows=$mobi_epg_ingestor::params::solr_max_rows,

   $config_file = $mobi_epg_ingestor::params::config_file,
   #SMS specific configs
   $carrier_vid = $mobi_epg_ingestor::params::carrier_vid,

   #Reliance specific configs
   $buffer = $mobi_epg_ingestor::params::buffer,
   $master = $mobi_epg_ingestor::params::master,
   $ccr = $mobi_epg_ingestor::params::ccr,
   $rtcr = $mobi_epg_ingestor::params::rtcr,
   $archive_enabled=$mobi_epg_ingestor::params::archive_enabled,
)
inherits mobi_epg_ingestor::params{
  anchor { "mobi_epg_ingestor::begin":} ->
    class {"mobi_epg_ingestor::install":} ->
    class {"mobi_epg_ingestor::config":} ->
    class { "mobi_epg_ingestor::icinga":} ->
  anchor { "mobi_epg_ingestor::end": }
  os::motd::register { $name : }
}
