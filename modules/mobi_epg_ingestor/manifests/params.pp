class mobi_epg_ingestor::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-epg-ingestor/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    $package = "mobi-epg-ingestor"
    $package_ensure = present
    $ingestor_enabled = undef
    $channel_data_file = undef
    $partner_xsl_file = undef
    #SMS specific configs
    $carrier_vid = undef
    $source_data_file = undef
    $tms_stage_folder = undef
    $partner_stage_folder = undef
    $xsl_file = undef
    $solr_archive_server_url = undef
    $config_folder = undef
    $channel_data_config = undef
    $externallink_xsl_file = undef
    $partner_config_folder = undef
    $ftpupload_queue_consumers_size = undef
    $channel_xsl_file = undef
    $externallink_config_folder = undef
    $externallink_stage_folder  = undef
    $dam_list_channels_url = undef
    $mq_broker_url = undef
    $metadataprocessing_queue_consumers_size = undef
    $mobi_externallink_configuration = undef
    $channel_config_folder = undef
    $dam_list_cliplinears_url = undef
    $tms_config_folder = undef
    $dam_url = undef
    $dam_list_externallinks_url = undef
    $channel_stage_folder  = undef
    $mobi_channel_configuration = undef
    $oms_live_media_skus_lookup_url = undef
    $search_policies_for_asset_url = undef
    $tms_config_file = undef
    $externallink_data_config = undef
    $channel_dam_config = undef
    $config_file = "/opt/mobi-epg-ingestor/webapp/config/epg-ingestor.properties"
    $provider_conf_location = undef
    $solr_server_url = undef
    $solr_socket_timeout = undef
    $solr_conn_timeout = undef
    $solr_conn_per_host = undef
    $solr_total_conn = undef
    $solr_max_retries = undef
    $solr_max_rows = undef

    #Reliance specific configs
    $buffer = undef
    $master = undef
    $ccr = undef
    $rtcr = undef
    $archive_enabled = undef
}
