class mobi_epg_ingestor::install {
    package { "${::mobi_epg_ingestor::package}":
      ensure => $::mobi_epg_ingestor::package_ensure,
      notify => [ Class["tomcat::service"], ]
    }
}
