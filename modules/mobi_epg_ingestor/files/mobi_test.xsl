<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="xml" /> 
 <xsl:template match="/">
 <xsl:variable name="lowercases" select="'abcdefghijklmnopqrstuvwxyz'" />
 <xsl:variable name="uppercases" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
 <xsl:variable name="default_lang" select="'en'"></xsl:variable>
 <xsl:element name="programs">            
  <xsl:for-each select="mpg/mpg_chnl/mpg_seg">      	 
     <xsl:element name="programme">
	  <xsl:element name="channel_id">
         	 <xsl:value-of select="../stn" /> 
          </xsl:element>
	  <xsl:element name="allow_recording">
                 <xsl:value-of select="allow_recording" />
          </xsl:element>
          <xsl:element name="start_time">
	 	 <xsl:value-of select="start_gmt_tm" /> 
          </xsl:element>
	  <xsl:element name="end_time">
                 <xsl:value-of select="end_gmt_tm" />
          </xsl:element>
	  <xsl:element name="is_episode">
                 <xsl:value-of select="is_episode" />
          </xsl:element>
	  <xsl:element name="is_live">
                 <xsl:value-of select="is_live" />
          </xsl:element>
	  <xsl:element name="is_repeat">
                 <xsl:value-of select="is_repeat" />
          </xsl:element>
	  <xsl:element name="thumbnail_url">
                 <xsl:value-of select="thumbnail_url" />
          </xsl:element>
	  <xsl:element name="thumbnail_id">
                 <xsl:value-of select="thumbnail_id" />
          </xsl:element>
	  <xsl:element name="thumbnail_formats">
                 <xsl:value-of select="thumbnail_formats" />
          </xsl:element>
          
      <xsl:for-each select="name" >
		<xsl:variable name="lang" select="@lang"></xsl:variable>
			<xsl:choose>
    			<xsl:when test="$lang !=''">
       				<xsl:choose>
       					<xsl:when test="$lang = $default_lang">
          					<xsl:element name="name">
            					<xsl:value-of select="." />
          					</xsl:element>
       					</xsl:when>
         			<xsl:otherwise>
         				 <xsl:element name="ml_name_{$lang}">
         				     <xsl:attribute name="datatype">Text</xsl:attribute>
							 <xsl:attribute name="multivalue">false</xsl:attribute>
							 <xsl:attribute name="indexed">true</xsl:attribute>	
           					 <xsl:value-of select="." />
          				</xsl:element>
         			</xsl:otherwise>
      		 		</xsl:choose>
    			</xsl:when>
    		<xsl:otherwise>
      				<xsl:element name="name">
           				 <xsl:value-of select="." />
       				</xsl:element>
    		</xsl:otherwise>
			</xsl:choose>
       </xsl:for-each>
          
	 <xsl:element name="sub_title">
                 <xsl:value-of select="sub_title" />
          </xsl:element>

       <xsl:for-each select="genre_list" >
		<xsl:variable name="lang" select="@lang"></xsl:variable>
			<xsl:choose>
    			<xsl:when test="$lang !=''">
       				<xsl:choose>
       					<xsl:when test="$lang = $default_lang">
          					<xsl:element name="genre_list">
            					<xsl:value-of select="." />
          					</xsl:element>
       					</xsl:when>
         			<xsl:otherwise>
         				 <xsl:element name="ml_genre_list_{$lang}">
         				     <xsl:attribute name="datatype">String</xsl:attribute>
							 <xsl:attribute name="multivalue">true</xsl:attribute>
							 <xsl:attribute name="indexed">true</xsl:attribute>	
           					 <xsl:value-of select="." />
          				</xsl:element>
         			</xsl:otherwise>
      		 		</xsl:choose>
    			</xsl:when>
    		<xsl:otherwise>
      				<xsl:element name="genre_list">
           				 <xsl:value-of select="." />
       				</xsl:element>
    		</xsl:otherwise>
			</xsl:choose>
       </xsl:for-each>

	  <xsl:element name="year">
                 <xsl:value-of select="year" />
          </xsl:element>
	  <xsl:element name="aspect_ratio">
                 <xsl:value-of select="aspect_ratio" />
          </xsl:element>
          
        <xsl:for-each select="series_description" >
		<xsl:variable name="lang" select="@lang"></xsl:variable>
			<xsl:choose>
    			<xsl:when test="$lang !=''">
       				<xsl:choose>
       					<xsl:when test="$lang = $default_lang">
          					<xsl:element name="series_description">
            					<xsl:value-of select="." />
          					</xsl:element>
       					</xsl:when>
         			<xsl:otherwise>
         				 <xsl:element name="ml_series_description_{$lang}">
         				     <xsl:attribute name="datatype">String</xsl:attribute>
							 <xsl:attribute name="multivalue">false</xsl:attribute>
							 <xsl:attribute name="indexed">true</xsl:attribute>	
           					 <xsl:value-of select="." />
          				</xsl:element>
         			</xsl:otherwise>
      		 		</xsl:choose>
    			</xsl:when>
    		<xsl:otherwise>
      				<xsl:element name="series_description">
           				 <xsl:value-of select="." />
       				</xsl:element>
    		</xsl:otherwise>
			</xsl:choose>
       </xsl:for-each>
          
	  <xsl:element name="advisory_list">
                 <xsl:value-of select="advisory_list" />
          </xsl:element>
	  <xsl:element name="original_air_date">
                 <xsl:value-of select="original_air_date" />
          </xsl:element>
	 <xsl:element name="live_tape_delay">
                 <xsl:value-of select="live_tape_delay" />
          </xsl:element>
		  <xsl:element name="audio_flags">
                 <xsl:value-of select="audio_flags" />
          </xsl:element>
		  <xsl:element name="video_flags">
                 <xsl:value-of select="video_flags" />
          </xsl:element>
		  <xsl:element name="premiere_finale_flag">
                 <xsl:value-of select="premiere_finale_flag" />
          </xsl:element>
          <xsl:element name="id">
	 	  </xsl:element>
		  <xsl:element name="letter_box">
                 <xsl:value-of select="letter_box" />
          </xsl:element>
          
           <xsl:element name="audio_languages_list">
                 <xsl:value-of select="audio_languages_list" />
          </xsl:element>
          
		  <xsl:element name="episode_id">
                 <xsl:value-of select="episode_id" />
          </xsl:element>
          <xsl:element name="season_number">
                 <xsl:value-of select="episode_season_number" />
          </xsl:element>
          <xsl:element name="episode_sub_title">
                 <xsl:value-of select="episode_sub_title" />
          </xsl:element>
        <xsl:for-each select="episode_name" >
		<xsl:variable name="lang" select="@lang"></xsl:variable>
			<xsl:choose>
    			<xsl:when test="$lang !=''">
       				<xsl:choose>
       					<xsl:when test="$lang = $default_lang">
          					<xsl:element name="episode_name">
            					<xsl:value-of select="." />
          					</xsl:element>
       					</xsl:when>
         			<xsl:otherwise>
         				 <xsl:element name="ml_episode_name_{$lang}">
         				  <xsl:attribute name="datatype">String</xsl:attribute>
						  <xsl:attribute name="multivalue">false</xsl:attribute>
						  <xsl:attribute name="indexed">true</xsl:attribute>	
           				  <xsl:value-of select="." />
          				</xsl:element>
         			</xsl:otherwise>
      		 		</xsl:choose>
    			</xsl:when>
    		<xsl:otherwise>
      				<xsl:element name="episode_name">
           				 <xsl:value-of select="." />
       				</xsl:element>
    		</xsl:otherwise>
			</xsl:choose>
       </xsl:for-each>
		  
		  <xsl:element name="episode_number">
                 <xsl:value-of select="episode_number" />
          </xsl:element>
		  <xsl:element name="episode_minimum_age">
                 <xsl:value-of select="episode_minimum_age" />
          </xsl:element>
		   <xsl:element name="episode_countries_of_production_list">
                 <xsl:value-of select="episode_countries_of_production_list" />
          </xsl:element>
		    <xsl:element name="episode_audio_languages_list">
                 <xsl:value-of select="episode_audio_languages_list" />
          </xsl:element>
          <xsl:element name="directors_list">
                 <xsl:value-of select="episode_directors_list" />
          </xsl:element>
		   <xsl:element name="episode_scriptwriters_list">
                 <xsl:value-of select="episode_scriptwriters_list" />
          </xsl:element>
		   <xsl:element name="episode_actors_list">
                 <xsl:value-of select="episode_actors_list" />
          </xsl:element>
          
        <xsl:for-each select="category" >
		<xsl:variable name="lang" select="@lang"></xsl:variable>
		<xsl:variable name="category_value" select="." />
			<xsl:choose>
    			<xsl:when test="$lang !=''">
       				<xsl:choose>
       					<xsl:when test="$lang = $default_lang">
          					<xsl:element name="category">
            					<xsl:value-of select="translate($category_value, $uppercases, $lowercases)"/>
          					</xsl:element>
       					</xsl:when>
         			<xsl:otherwise>
         				 <xsl:element name="ml_category_{$lang}">
         				   <xsl:attribute name="datatype">String</xsl:attribute>
						   <xsl:attribute name="multivalue">true</xsl:attribute>
						   <xsl:attribute name="indexed">true</xsl:attribute>	
           				   <xsl:value-of select="." />
          				</xsl:element>
         			</xsl:otherwise>
      		 		</xsl:choose>
    			</xsl:when>
    		<xsl:otherwise>
      				<xsl:element name="category">
           				 <xsl:value-of select="translate($category_value, $uppercases, $lowercases)"/>
       				</xsl:element>
    		</xsl:otherwise>
			</xsl:choose>
       </xsl:for-each>
         
		  <xsl:element name="duration">
          	 <xsl:value-of select="duration_sec" /> 
          </xsl:element>
          
         <xsl:for-each select="dsc" >
		<xsl:variable name="lang" select="@lang"></xsl:variable>
			<xsl:choose>
    			<xsl:when test="$lang !=''">
       				<xsl:choose>
       					<xsl:when test="$lang = $default_lang">
          					<xsl:element name="description">
            					<xsl:value-of select="." />
          					</xsl:element>
       					</xsl:when>
         			<xsl:otherwise>
         				 <xsl:element name="ml_description_{$lang}">
         				  <xsl:attribute name="datatype">Text</xsl:attribute>
						  <xsl:attribute name="multivalue">false</xsl:attribute>
						  <xsl:attribute name="indexed">true</xsl:attribute>	
           				  <xsl:value-of select="." />
          				</xsl:element>
         			</xsl:otherwise>
      		 		</xsl:choose>
    			</xsl:when>
    		<xsl:otherwise>
      				<xsl:element name="description">
           				 <xsl:value-of select="." />
       				</xsl:element>
    		</xsl:otherwise>
			</xsl:choose>
       </xsl:for-each>  
               
          <xsl:element name="duration">
          	<xsl:value-of select="duration_sec" /> 
          </xsl:element>
	  <xsl:element name="series_id">
          	<xsl:value-of select="series_id" />
          </xsl:element>                            

           <xsl:element name="child_protection_rating">
          	<xsl:variable name="child_protection_rating" select="child_protection_rating" />
          	<xsl:value-of select="translate($child_protection_rating, $lowercases, $uppercases)" /> 
          </xsl:element>      
 
          <xsl:for-each select="vid">
           		 <xsl:element name="vid">
      				<xsl:value-of select="."/>
      			 </xsl:element>
    	  </xsl:for-each>               
	  <xsl:element name="em_extended_matadata_element_1">
		<xsl:attribute name="datatype">String</xsl:attribute>
		<xsl:attribute name="multivalue">false</xsl:attribute>
		<xsl:attribute name="indexed">false</xsl:attribute>				
		<xsl:value-of select="extended_matadata_element_1"/>
          </xsl:element>			
	  <xsl:element name="em_support_cast">
		<xsl:attribute name="datatype">String</xsl:attribute>
		<xsl:attribute name="multivalue">true</xsl:attribute>
		<xsl:attribute name="indexed">true</xsl:attribute>				
		<xsl:value-of select="support_cast"/>
	  </xsl:element>
	  <xsl:for-each select="images/image"> 
		<xsl:element name="image">
			<xsl:attribute name="image_id"><xsl:value-of select="image_id"/></xsl:attribute>
			<xsl:attribute name="image_formats">
			<xsl:value-of select="image_formats"/></xsl:attribute>
			<xsl:value-of select="image_name"/>
		</xsl:element>
	  </xsl:for-each>
	</xsl:element>
  </xsl:for-each>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
