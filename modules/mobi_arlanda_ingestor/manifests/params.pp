class mobi_arlanda_ingestor::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /ingestor/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $version = "5.0.0-210408"
    $php_soap_version = "present"

    $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100"
    $dam_url = "http://juarnvip:8080/dam/ws?wsdl"
    $folder_manager_rest_url = "http://juarnvip:8080/foldermanager/"

    $ing_db_username = "ing_user"
    $ing_db_password = "ing_user"

    $mobi2_db_username = "ing_user"
    $mobi2_db_password = "ing_user"

    $fm_db_username = "fm_user"
    $fm_db_password = "fm_user"

    $aspera_db_username = "sa"
    $aspera_db_password = ""

    $dam_db_username = "dam_user"
    $dam_db_password = "dam_user"

    $offer_management_vod_url = "http://offermanagervip:8080/mobi-aaa-offer-management-mobi2-adapter/guide/v5/asset/vod/{MID}.json?includeClipChannels=true"
    $offer_management_episode_url = "http://offermanagervip:8080/mobi-aaa-offer-management-mobi2-adapter/guide/v5/asset/episode/{MID}.json"
    $offer_management_series_url = "http://offermanagervip:8080/mobi-aaa-offer-management-mobi2-adapter/guide/v5/asset/series/{MID}.json"

    $policy_management_url = "http://policymanagervip:8080/mobi-aaa-offer-management-mobi2-adapter/services/core/v5/"

    $asset_resource_file_handling = "move"

    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $hibernate_database_vendor = "MYSQL"
    $connection_test_query = "select 1 from dual"

    $ing_jdbc_driver = "com.mysql.jdbc.Driver"
    $dam_jdbc_driver = "com.mysql.jdbc.Driver"
    $mobi2_jdbc_driver = "com.mysql.jdbc.Driver"
    $aspera_jdbc_driver = "org.hsqldb.jdbcDriver"

    $ing_db_jdbc_url = "jdbc:mysql://mydtbtransientvip:3307/ing?autoReconnect=true"
    $mobi2_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/mobi2lite?autoReconnect=true"
    $aspera_db_jdbc_url = "jdbc:hsqldb:mem:hsql"
    $dam_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/dam?autoReconnect=true"

    $sftp_host = "inftp01p4.smf2.mobitv"
    $sftp_port = "20022"
    $sftp_user = "mobi_cms_us"
    $sftp_password = "e0rtv12"

    $offer_management_timeout = "30"
    $policy_management_timeout = "30"

    $dam_datasource = true

    $subconvertor_location = undef
    $subconvertor_timeout = undef
    $offer_management_url = undef
    $policy_manager_timeout = undef
    $policy_manager_url = undef

    $provider_orig_metadata_feed_dir = undef
    $create_input_feed_file = undef

    $ril = false
}
