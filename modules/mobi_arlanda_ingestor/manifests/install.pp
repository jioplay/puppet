class mobi_arlanda_ingestor::install {
    package {"php-soap":
        ensure => $mobi_arlanda_ingestor::php_soap_version,
    }

    package { "mobi-arlanda-ingestor":
        ensure => "${mobi_arlanda_ingestor::version}",
        notify => Class["tomcat::service"],
    }

    file { ["/opt/mobi-arlanda-ingestor", "/opt/mobi-arlanda-ingestor/database", "/opt/mobi-arlanda-ingestor/database/postgres"]:
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
    }

    file { "/opt/mobi-arlanda-ingestor/database/postgres/ing_seed.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_ingestor/ing_seed.sql.erb'),
        require => File["/opt/mobi-arlanda-ingestor/database/postgres"],
    }

    file { "/opt/mobi-arlanda-ingestor/database/postgres/ingesor_ing_schema.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_ingestor/ingesor_ing_schema.sql.erb'),
        require => File["/opt/mobi-arlanda-ingestor/database/postgres"],
    }

    #file { "/opt/mobi-arlanda-ingestor/database/postgres/ingestor_mobi2lite_schema.sql":
    #    ensure => present,
    #    owner  => "rtv",
    #    group  => "rtv",
    #    mode   => "0775",
    #    content => template('mobi_arlanda_ingestor/ingestor_mobi2lite_schema.sql.erb'),
    #}

    #file { "/opt/mobi-arlanda-ingestor/database/postgres/mobi2lite_seed.sql":
    #    ensure => present,
    #    owner  => "rtv",
    #    group  => "rtv",
    #    mode   => "0775",
    #    content => template('mobi_arlanda_ingestor/mobi2lite_seed.sql.erb'),
    #}
}
