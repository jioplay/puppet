# == Class: mobi_arlanda_ingestor
#
#  installs ingestor component
#
# === Parameters:
#
#    $version::
#    $amq_broker_url::
#    $dam_url::
#    $folder_manager_rest_url::
#    $ing_db_username::
#    $ing_db_password::
#    $mobi2_db_username::
#    $mobi2_db_password::
#    $dam_db_username::
#    $dam_db_password::
#    $aspera_db_username::
#    $aspera_db_password::
#    $ing_jdbc_driver::
#    $mobi2_jdbc_driver::
#    $aspera_jdbc_driver::
#    $ing_db_jdbc_url::
#    $mobi2_db_jdbc_url::
#    $aspera_db_jdbc_url::
#    $hibernate_database_dialect
#    $hibernate_database_vendor
#    $sftp_host
#    $sftp_port
#    $sftp_user
#    $sftp_password
#    $offer_management_url
#    $asset_resource_file_handling
#    $offer_management_timeout
#    $subconvertor_location
#    $subconvertor_timeout
#    $offer_management_url
#    $policy_manager_timeout
#    $policy_manager_url
#    $provider_orig_metadata_feed_dir
#    $create_input_feed_file
#
#
# === Requires:
#
#     java
#     tomcat
#     apache
#     php
#
# === Sample Usage
#
#   class { "mobi_cms_us::juarn" :
#           version => "5.0.0-178058",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_ingestor (
    ###icinga.pp
    $icinga = $mobi_arlanda_ingestor::params::icinga,
    $icinga_instance = $mobi_arlanda_ingestor::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_ingestor::params::icinga_cmd_args,
    ###end icinga.pp
    $version = $mobi_arlanda_ingestor::params::version,
    $php_soap_version = $mobi_arlanda_ingestor::params::php_soap_version,
    $amq_broker_url = $mobi_arlanda_ingestor::params::amq_broker_url,
    $dam_url = $mobi_arlanda_ingestor::params::dam_url,
    $folder_manager_rest_url = $mobi_arlanda_ingestor::params::folder_manager_rest_url,
    $ing_db_username = $mobi_arlanda_ingestor::params::ing_db_username,
    $ing_db_password = $mobi_arlanda_ingestor::params::ing_db_password,
    $mobi2_db_username = $mobi_arlanda_ingestor::params::mobi2_db_username,
    $mobi2_db_password = $mobi_arlanda_ingestor::params::mobi2_db_password,
    $aspera_db_username = $mobi_arlanda_ingestor::params::aspera_db_username,
    $aspera_db_password = $mobi_arlanda_ingestor::params::aspera_db_password,
    $ing_jdbc_driver = $mobi_arlanda_ingestor::params::ing_jdbc_driver,
    $mobi2_jdbc_driver = $mobi_arlanda_ingestor::params::mobi2_jdbc_driver,
    $aspera_jdbc_driver = $mobi_arlanda_ingestor::params::aspera_jdbc_driver,
    $dam_jdbc_driver = $mobi_arlanda_ingestor::params::dam_jdbc_driver,
    $ing_db_jdbc_url = $mobi_arlanda_ingestor::params::ing_db_jdbc_url,
    $mobi2_db_jdbc_url = $mobi_arlanda_ingestor::params::mobi2_db_jdbc_url,
    $dam_db_jdbc_url = $mobi_arlanda_ingestor::params::dam_db_jdbc_url,
    $aspera_db_jdbc_url = $mobi_arlanda_ingestor::params::aspera_db_jdbc_url,
    $connection_test_query = $mobi_arlanda_ingestor::params::connection_test_query,
    $hibernate_database_dialect = $mobi_arlanda_ingestor::params::hibernate_database_dialect,
    $hibernate_database_vendor = $mobi_arlanda_ingestor::params::hibernate_database_vendor,
    $sftp_host = $mobi_arlanda_ingestor::params::sftp_host,
    $sftp_port = $mobi_arlanda_ingestor::params::sftp_port,
    $sftp_user = $mobi_arlanda_ingestor::params::sftp_user,
    $sftp_password = $mobi_arlanda_ingestor::params::sftp_password,
    $offer_management_vod_url = $mobi_arlanda_ingestor::params::offer_management_vod_url,
    $offer_management_episode_url = $mobi_arlanda_ingestor::params::offer_management_episode_url,
    $offer_management_series_url = $mobi_arlanda_ingestor::params::offer_management_series_url,
    $policy_management_url = $mobi_arlanda_ingestor::params::policy_management_url,
    $asset_resource_file_handling = $mobi_arlanda_ingestor::params::asset_resource_file_handling,
    $offer_management_timeout = $mobi_arlanda_ingestor::params::offer_management_timeout,
    $policy_management_timeout = $mobi_arlanda_ingestor::params::policy_management_timeout,
    $fm_db_username = $mobi_arlanda_ingestor::params::fm_db_username,
    $fm_db_password = $mobi_arlanda_ingestor::params::fm_db_password,
    $dam_db_username = $mobi_arlanda_ingestor::params::dam_db_username,
    $dam_db_password = $mobi_arlanda_ingestor::params::dam_db_password,
    $external_filesystem = false,
    $dam_datasource = $mobi_arlanda_ingestor::params::dam_datasource,
    $subconvertor_location = $mobi_arlanda_ingestor::params::subconventor_location,
    $subconvertor_timeout = $mobi_arlanda_ingestor::params::subconventor_timeout,
    $offer_management_url = $mobi_arlanda_ingestor::params::offer_management_url,
    $policy_manager_timeout = $mobi_arlanda_ingestor::params::policy_manager_timeout,
    $policy_manager_url = $mobi_arlanda_ingestor::params::policy_manager_url,
    $provider_orig_metadata_feed_dir = $mobi_arlanda_ingestor::params::provider_orig_metadata_feed_dir,
    $create_input_feed_file = $mobi_arlanda_ingestor::params::provider_orig_metadata_feed_dir,
    $ril = $mobi_arlanda_ingestor::params::ril,
)

inherits mobi_arlanda_ingestor::params {
    anchor { "mobi_arlanda_ingestor::begin": } ->
    class{"mobi_arlanda_ingestor::install":} ->
    class{"mobi_arlanda_ingestor::config":} ->
    class { "mobi_arlanda_ingestor::icinga":} ->
    anchor { "mobi_arlanda_ingestor::end" :} -> os::motd::register { $name : }
}

