class mobi_arlanda_ingestor::config {

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/ingestor.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_ingestor/ingestor-tomcat-context.xml.erb'),
        require => Class["mobi_arlanda_ingestor::install"],
        notify   => Class["tomcat::service"],
    }

    # Database persistence files
    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/legacy-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_ingestor/ingestor-legacy-persistence.xml.erb'),
        require => Class["mobi_arlanda_ingestor::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/ingestor-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_ingestor/ingestor-ing-persistence.xml.erb'),
        require => Class["mobi_arlanda_ingestor::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/aspera-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_ingestor/ingestor-aspera-persistence.xml.erb'),
        require => Class["mobi_arlanda_ingestor::install"],
        notify   => Class["tomcat::service"],
    }

    # Customized Quartz Spring configs
    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/spring/quartzContext.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_ingestor/ingestor-quartzContext.xml.erb'),
        require => Class["mobi_arlanda_ingestor::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/arlanda/conf/ingestor/ingestor.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_ingestor/ingestor.properties.erb'),
        require => Class["mobi_arlanda_ingestor::install"],
        notify   => Class["tomcat::service"],
    }

    if ($mobi_arlanda_ingestor::ril == true) {
        file {"/var/Jukebox/uploads/search_transform/2.xslt":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/2.xslt",
        }
        file {"/var/Jukebox/uploads/search_transform/3.xslt":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/2.xslt",
        }
        file {"/var/Jukebox/uploads/search_transform/4.xslt":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/2.xslt",
        }
        file {"/var/Jukebox/uploads/search_transform/5.xslt":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/2.xslt",
        }
        file {"/var/Jukebox/uploads/search_transform/6.xslt":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/2.xslt",
        }
        file {"/var/Jukebox/uploads/search_transform/7.xslt":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/2.xslt",
        }
        file {"/var/Jukebox/uploads/search_transform/8.xslt":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/2.xslt",
        }
        file {"/var/Jukebox/uploads/search_transform/vantage.xsl":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/vantage.xsl",
        }
        file {"/var/Jukebox/uploads/search_transform/vantagetoplatform.xsl":
            replace => true,
            owner   => "rtv",
            group   => "arlanda",
            mode    => "0644",
            source  => "puppet:///modules/mobi_arlanda_ingestor/ril/vantagetoplatform.xsl",
        }


    } else {

        if (! $mobi_arlanda_ingestor::external_filesystem) {
          file { "/var/Jukebox/uploads/search_transform/search_default_transformation.xslt":
              replace => true,
              owner  => "rtv",
              group  => "rtv",
              mode => "0444",
              source => "puppet:///modules/mobi_arlanda_ingestor/search_default_transformation.xslt",
              require => Class["mobi_arlanda_ingestor::install"],
          }

          file { '/var/Jukebox/uploads/search_transform/2.xslt':
             ensure => 'link',
             target => '/var/Jukebox/uploads/search_transform/search_default_transformation.xslt',
             require => File["/var/Jukebox/uploads/search_transform/search_default_transformation.xslt"],
          }
       }
    }
}
