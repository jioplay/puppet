<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:media="http://search.yahoo.com/mrss/" xmlns:ez="http://www.everyzing.com/rss" version="1.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/">
    <normalizedProviderMetadata>
      <pair>
    <xsl:for-each select="ProgramDescription/ProgramInformationTable/ProgramInformation/BasicDescription/Entitlement">
        <metadata>
          <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
          <xsl:attribute name="key">em_offer_groups</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
        </metadata>
    </xsl:for-each>
      </pair>
    </normalizedProviderMetadata>
  </xsl:template>
</xsl:stylesheet>