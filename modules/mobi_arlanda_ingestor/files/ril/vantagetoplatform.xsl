<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0"
encoding="iso-8859-1" indent="yes"/>


<xsl:template match="/">
  <MobiAdSpots>
          <TimeStamps>
                <!--<xsl:apply-templates select="Clip/child::*"/> -->
                <xsl:for-each select="Clip/child::*" >
                  <xsl:variable name="counter">
                        <xsl:value-of select="position()-1" />
                  </xsl:variable>
                  <xsl:variable name="totalAds">
                        <xsl:value-of select="./../TotalAddSpots +1" />
                  </xsl:variable>

                  <xsl:if test="$counter &lt; $totalAds and name() != 'TotalAddSpots'">
                        <TimeStamp><xsl:value-of select="." /></TimeStamp>
                  </xsl:if>
                </xsl:for-each>
          </TimeStamps>
	<xsl:element name="VideoFrameRate">
		<xsl:value-of select="/Clip/VideoFrameRate" />
	 </xsl:element>
  </MobiAdSpots>
</xsl:template>
<xsl:template match="node()">
  <xsl:if test="name() != 'TotalAddSpots'">
        <TimeStamp><xsl:value-of select="." /></TimeStamp>
  </xsl:if>
</xsl:template>
</xsl:stylesheet>
