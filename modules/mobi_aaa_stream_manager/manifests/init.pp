# == class: stream_manager
#
#  install stream manager
#
# === Parameters:
#    $stream_manager_version::
#    $memcached_servers::
#    $auth_oauth2_server_url::
#    $auth_rights_server_url::
#    $recording_manager_host::
#    $dt_eons_tstv_recording::
#
# === Requires:
#
#    java
#    tomcat
#    memcached
#
# === Sample Usage
#
#
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_stream_manager(
    ###icinga.pp
    $icinga = $mobi_aaa_stream_manager::params::icinga,
    $icinga_instance = $mobi_aaa_stream_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_aaa_stream_manager::params::icinga_cmd_args,
    ###end icinga.pp
  $stream_manager_version = $mobi_aaa_stream_manager::params::stream_manager_version,
  $memcached_servers = $mobi_aaa_stream_manager::params::memcached_servers,
  $memcached_mutex_lockattempts = $mobi_aaa_stream_manager::params::memcached_mutex_lockattempts,
  $memcached_mutex_maxtimeout = $mobi_aaa_stream_manager::params::memcached_mutex_maxtimeout,
  $memcached_access_get_timeout = $mobi_aaa_stream_manager::params::memcached_access_get_timeout,
  $memcached_access_getbulk_timeout = $mobi_aaa_stream_manager::params::memcached_access_getbulk_timeout,
  $memcached_health_check_enabled = $mobi_aaa_stream_manager::params::memcached_health_check_enabled,
  $auth_caching_policy = $mobi_aaa_stream_manager::params::auth_caching_policy,
  $auth_resource_server_type = $mobi_aaa_stream_manager::params::auth_resource_server_type,
  $auth_connection_timeout = $mobi_aaa_stream_manager::params::auth_connection_timeout,
  $auth_read_timeout = $mobi_aaa_stream_manager::params::auth_read_timeout,
  $auth_oauth2_server_url = $mobi_aaa_stream_manager::params::auth_oauth2_server_url,
  $auth_rights_server_url = $mobi_aaa_stream_manager::params::auth_rights_server_url,
  $auth_session_server_url = $mobi_aaa_stream_manager::params::auth_session_server_url,
  $auth_enable_session_manager = $mobi_aaa_stream_manager::params::auth_enable_session_manager,
  $recording_manager_host = $mobi_aaa_stream_manager::params::recording_manager_host,
  $dt_eons_tstv_recording = $mobi_aaa_stream_manager::params::dt_eons_tstv_recording,
  $idm_health_check_url = $mobi_aaa_stream_manager::params::idm_health_check_url,
  $rights_health_check_url = $mobi_aaa_stream_manager::params::rights_health_check_url,
  $recording_manager_health_check_url = $mobi_aaa_stream_manager::params::recording_manager_health_check_url,
  $health_check_connection_timeout = $mobi_aaa_stream_manager::params::health_check_connection_timeout,
  $health_check_read_timeout = $mobi_aaa_stream_manager::params::health_check_read_timeout,
  $recording_manager_create_tstv_recording_url = $mobi_aaa_stream_manager::params::recording_manager_create_tstv_recording_url,
  $recording_manager_delete_tstv_recording_url = $mobi_aaa_stream_manager::params::recording_manager_delete_tstv_recording_url,
  $dt_eons_timeout = $mobi_aaa_stream_manager::params::dt_eons_timeout,
  $dt_eons_heartbeat = $mobi_aaa_stream_manager::params::dt_eons_heartbeat,
  $sprint_mobitv_timeout = $mobi_aaa_stream_manager::params::sprint_mobitv_timeout,
  $sprint_mobitv_heartbeat = $mobi_aaa_stream_manager::params::sprint_mobitv_heartbeat,
  $sprint_mobitv_tstv_recording = $mobi_aaa_stream_manager::params::sprint_mobitv_tstv_recording,
  $platform_reference_timeout = $mobi_aaa_stream_manager::params::platform_reference_timeout,
  $platform_reference_heartbeat = $mobi_aaa_stream_manager::params::platform_reference_heartbeat,
  $platform_reference_tstv_recording = $mobi_aaa_stream_manager::params::platform_reference_tstv_recording,
  $technicolor_mgo_timeout = $mobi_aaa_stream_manager::params::technicolor_mgo_timeout,
  $technicolor_mgo_heartbeat = $mobi_aaa_stream_manager::params::technicolor_mgo_heartbeat,
  $infotel_r4g_timeout = $mobi_aaa_stream_manager::params::infotel_r4g_timeout,
  $infotel_r4g_heartbeat = $mobi_aaa_stream_manager::params::infotel_r4g_heartbeat,
  $jersey_restclient_maxConnections = $mobi_aaa_stream_manager::params::jersey_restclient_maxConnections,
)
inherits mobi_aaa_stream_manager::params {
  include mobi_aaa_stream_manager::install, mobi_aaa_stream_manager::config
  anchor { "mobi_aaa_stream_manager::begin": } -> Class["mobi_aaa_stream_manager::install"]
    class { "mobi_aaa_stream_manager::icinga":} ->
  Class["mobi_aaa_stream_manager::config"] -> anchor { "mobi_aaa_stream_manager::end" :}
  os::motd::register { $name : }
}

