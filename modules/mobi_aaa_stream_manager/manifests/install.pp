class mobi_aaa_stream_manager::install {

    package {
    "mobi-aaa-stream-manager":
      ensure => "${mobi_aaa_stream_manager::stream_manager_version}",
      notify =>[ Class["tomcat::service"], ]
  }

#    file { "/opt/mobi-aaa-stream-manager/webapp/WEB-INF/classes/spring/context/resources/":
#        ensure => "directory",
#        owner  => "rtv",
#        group  => "rtv",
#        mode   => "0755",
#        recurse => true,
#        source => "puppet:///modules/mobi_aaa_stream_manager",
#        require => File["/opt/mobi-aaa-stream-manager/webapp/WEB-INF/classes/spring/context/resources"],
#    }

}
