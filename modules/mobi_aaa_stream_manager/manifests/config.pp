class mobi_aaa_stream_manager::config {

  file {
    "/opt/mobi-aaa/config/stream-manager/applicationProperties.xml":
      ensure => file,
      content => template('mobi_aaa_stream_manager/applicationProperties.xml.erb'),
      owner => "rtv",
      group => "rtv",
      mode => "755",
      notify => Class["tomcat::service"],
      require => Class["mobi_aaa_stream_manager::install"],
  }

  file { "/opt/mobi-aaa-stream-manager/webapp/WEB-INF/classes/spring/context/resources/":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
      recurse => true,
      source => "puppet:///modules/mobi_aaa_stream_manager",
      require => File["/opt/mobi-aaa-stream-manager/webapp/WEB-INF/classes/spring/context/resources"],
  }


#  file {
#    "/opt/mobi-aaa-stream-manager/webapp/WEB-INF/classes/spring/context/resources/contentPolicies.json":
#      ensure => file,
#      content => template('mobi_aaa_stream_manager/contentPolicies.json.erb'),
#      owner => "rtv",
#      group => "rtv",
#      mode => "755",
#      notify => Class["tomcat::service"],
#      require => Class["mobi_aaa_stream_manager::install"],
#  }

}
