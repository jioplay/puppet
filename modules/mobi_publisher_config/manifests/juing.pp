# == Class: mobi_publisher_config::juing
#
#  installs juing component
#
# === Parameters:
#
#    $mobi_live_channel_config_version::
#    $mobi_epg_dt_adapter_server_version::
#    $mobi_dt_publication_service_version::
#    $mobi_dt_publication_service_client_version::
#    $mobi_config_manager_version::
#    $rssh_version::
#    $mobi_publisher_framework_ng_version::
#    $mobi_publisher_transaction_configrepo_version::
#    $mobi_publisher_transaction_icon_ng_version::
#    $mobi_publisher_transaction_staticres_ng_version::
#    $mobi_publisher_transaction_configmanager_ng_version::
#    $eit_packet_generator_required::
#    $mobi_epg_dt_EITGenerator_version::
#    $ng_publisher_mode::
#
# === Requires:
#
#     java
#     tomcat
#     group::arlanda
#     user::apache
#     user::rtv
#     mobi_dt::filesystem or nf mounts setup
#
# === Sample Usage
#
#  class { "mobi_publisher_config::juing" :
#           ingestor_version => "5.0.0-179808",
#           searchagent_version => "5.0.0-181163",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_publisher_config::juing (
  $mobi_config_manager_version = $mobi_dt::juing::params::mobi_config_manager_version,
    $mobi_publisher_framework_ng_version = $mobi_dt::juing::params::mobi_publisher_framework_ng_version,
    $mobi_publisher_transaction_configrepo_version = $mobi_dt::juing::params::mobi_publisher_transaction_configrepo_version,
    $mobi_publisher_transaction_icon_ng_version = $mobi_dt::juing::params::mobi_publisher_transaction_icon_ng_version,
  $mobi_publisher_transaction_staticres_ng_version = $mobi_dt::juing::params::mobi_publisher_transaction_staticres_ng_version,
  $mobi_publisher_transaction_configmanager_ng_version = $mobi_dt::juing::params::mobi_publisher_transaction_configmanager_ng_version,
    $ng_publisher_mode = $mobi_dt::juing::params::ng_publisher_mode,
)
inherits mobi_publisher_config::juing::params {
    include mobi_publisher_config::juing::install, mobi_publisher_config::juing::config
    anchor { "mobi_publisher_config::juing::begin":} -> Class["mobi_publisher_config::juing::install"]
    Class["mobi_publisher_config::juing::config"] -> anchor { "mobi_publisher_config::juing::end": }
    os::motd::register { $name : }
}
