class mobi_publisher_config::juing::config {

    file { "/opt/mobi-publisher/mobi-publisher-transaction-configrepo/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_publisher_config/ng_publisher_resources/brick/endpoint.host.json",
      require => Package["mobi-publisher-transaction-configrepo-${mobi_publisher_config::juing::mobi_publisher_transaction_configrepo_version}"],
    }

    file { "/opt/mobi-publisher/mobi-publisher-transaction-icon/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_publisher_config/ng_publisher_resources/brick/endpoint.host.json",
      require => Package["mobi-publisher-transaction-icon-ng-${mobi_publisher_config::juing::mobi_publisher_transaction_icon_ng_version}"],
    }

    file { "/opt/mobi-publisher/mobi-publisher-transaction-staticres/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_publisher_config/ng_publisher_resources/brick/endpoint.host.json",
      require => Package["mobi-publisher-transaction-staticres-ng-${mobi_publisher_config::juing::mobi_publisher_transaction_staticres_ng_version}"],
    }

    file { "/opt/mobi-publisher/mobi-publisher-transaction-configmanager/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_publisher_config/ng_publisher_resources/brick/endpoint.host.json",
      require => Package["mobi-publisher-transaction-configmanager-ng-${mobi_publisher_config::juing::mobi_publisher_transaction_configmanager_ng_version}"],
    }
}
