class mobi_publisher_config::juing::start {
    include(mobi_publisher_config::tomcat::start)

  notify { "JUINGStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

  Notify['JUINGStarted'] ->
  Class['mobi_publisher_config::tomcat::start']
}
