class mobi_publisher_config::juing::install {

    package {"mobi-config-manager-${mobi_publisher_config::juing::mobi_config_manager_version}":
      ensure => present,
    provider => yum,
      notify => Class["tomcat::service"],
    }

    package {"mobi-publisher-framework-ng-${mobi_publisher_config::juing::mobi_publisher_framework_ng_version}":
      ensure => present,
    }

    package {"mobi-publisher-transaction-configrepo-${mobi_publisher_config::juing::mobi_publisher_transaction_configrepo_version}":
      ensure => present,
    }

    package {"mobi-publisher-transaction-icon-ng-${mobi_publisher_config::juing::mobi_publisher_transaction_icon_ng_version}":
      ensure => present,
    }

    package {"mobi-publisher-transaction-staticres-ng-${mobi_publisher_config::juing::mobi_publisher_transaction_staticres_ng_version}":
      ensure => present,
    }

    package {"mobi-publisher-transaction-configmanager-ng-${mobi_publisher_config::juing::mobi_publisher_transaction_configmanager_ng_version}":
      ensure => present,
    }

}
