class mobi_aaa_purchase_manager::install {
  package { "${mobi_aaa_purchase_manager::mobi_aaa_purchase_manager_package_name}":
    ensure => "${mobi_aaa_purchase_manager::version}",
    notify => [ Class["tomcat::service"], ]
  }
}
