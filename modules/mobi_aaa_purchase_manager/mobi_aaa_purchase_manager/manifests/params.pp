class mobi_aaa_purchase_manager::params {
  ###icinga.pp
  $icinga = true
  $icinga_instance = "icinga"
  $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-aaa-purchase-manager/monitoring/health -w 5 -c 10"
  ##icinga.pp
  $mobi_aaa_purchase_manager_package_name = "mobi-aaa-purchase-manager"
  $mobi_aaa_purchase_manager_version = ""
  $database_url = undef
  $database_user_name = undef
  $database_password = undef
  $database_driver = undef
  $hibernate_dialect = undef
  $hibernate_show_sql = "false"
  $jersey_restclient_connection_timeout = undef
  $jersey_restclient_read_timeout = undef
  $jersey_restclient_max_connections = undef
  $jersey_restclient_enable_client_log = undef
  $auth_manager_endpoint_url = undef
  $identity_manager_endpoint_url = undef
  $offer_manager_endpoint_url = undef
  $rights_manager_endpoint_url = undef
  $transaction_processor_endpoint_url = undef
  $session_manager_endpoint_url = undef
  $carrier_vendor = undef
  $zookeeper_connect_string = undef
  $rights_manager_service_name = undef
  $auth_enable_session_manager = undef
  $auth_use_legacy_verify_tokens_endpoint = undef
  $purchase_manager_service_name = undef
}
