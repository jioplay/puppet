## == Class: mobi_aaa_purchase_manager
#
# Manage the mobi_aaa_purchase_manager component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_aaa_purchase_manager (
  ###icinga.pp
  $icinga = $mobi_aaa_purchase_manager::params::icinga,
  $icinga_instance = $mobi_aaa_purchase_manager::params::icinga_instance,
  $icinga_cmd_args = $mobi_aaa_purchase_manager::params::icinga_cmd_args,
  ###end icinga.pp
  $version = $mobi_aaa_purchase_manager::params::mobi_aaa_purchase_manager_version,
  $mobi_aaa_purchase_manager_package_name = $mobi_aaa_purchase_manager::params::mobi_aaa_purchase_manager_package_name,
  $database_url = $mobi_aaa_purchase_manager::params::database_url,
  $database_user_name = $mobi_aaa_purchase_manager::params::database_user_name,
  $database_password = $mobi_aaa_purchase_manager::params::database_password,
  $database_driver = $mobi_aaa_purchase_manager::params::database_driver,
  $hibernate_dialect = $mobi_aaa_purchase_manager::params::hibernate_dialect,
  $hibernate_show_sql = $mobi_aaa_purchase_manager::params::hibernate_show_sql,
  $jersey_restclient_connection_timeout = $mobi_aaa_purchase_manager::params::jersey_restclient_connection_timeout,
  $jersey_restclient_read_timeout = $mobi_aaa_purchase_manager::params::jersey_restclient_read_timeout,
  $jersey_restclient_max_connections = $mobi_aaa_purchase_manager::params::jersey_restclient_max_connections,
  $jersey_restclient_enable_client_log = $mobi_aaa_purchase_manager::params::jersey_restclient_enable_client_log,
  $auth_manager_endpoint_url = $mobi_aaa_purchase_manager::params::auth_manager_endpoint_url,
  $identity_manager_endpoint_url = $mobi_aaa_purchase_manager::params::identity_manager_endpoint_url,
  $offer_manager_endpoint_url = $mobi_aaa_purchase_manager::params::offer_manager_endpoint_url,
  $rights_manager_endpoint_url = $mobi_aaa_purchase_manager::params::rights_manager_endpoint_url,
  $transaction_processor_endpoint_url = $mobi_aaa_purchase_manager::params::transaction_processor_endpoint_url,
  $session_manager_endpoint_url = $mobi_aaa_purchase_manager::params::session_manager_endpoint_url,
  $carrier_vendor = $mobi_aaa_purchase_manager::params::carrier_vendor,
  $zookeeper_connect_string = $mobi_aaa_purchase_manager::params::zookeeper_connect_string,
  $rights_manager_service_name = $mobi_aaa_purchase_manager::params::rights_manager_service_name,
  $auth_enable_session_manager = $mobi_aaa_purchase_manager::params::auth_enable_session_manager,
  $auth_use_legacy_verify_tokens_endpoint = $mobi_aaa_purchase_manager::params::auth_use_legacy_verify_tokens_endpoint,
  $purchase_manager_service_name = $mobi_aaa_purchase_manager::params::purchase_manager_service_name,

) inherits mobi_aaa_purchase_manager::params {

    anchor { "mobi_aaa_purchase_manager::begin": } ->
    class { "mobi_aaa_purchase_manager::install": } ->
    class { "mobi_aaa_purchase_manager::config": } ->
    class { "mobi_aaa_purchase_manager::icinga":} ->
    anchor { "mobi_aaa_purchase_manager::end": }

    os::motd::register{ "${name}":}
}
