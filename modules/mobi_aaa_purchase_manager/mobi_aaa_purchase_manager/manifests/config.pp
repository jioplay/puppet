class mobi_aaa_purchase_manager::config {
  File {
    owner => rtv,
    group => rtv,
    mode => "0644",
  }

  $database_url = $mobi_aaa_purchase_manager::database_url
  $database_user_name = $mobi_aaa_purchase_manager::database_user_name
  $database_password = $mobi_aaa_purchase_manager::database_password
  $database_driver = $mobi_aaa_purchase_manager::database_driver
  $hibernate_dialect = $mobi_aaa_purchase_manager::hibernate_dialect
  $hibernate_show_sql = $mobi_aaa_purchase_manager::hibernate_show_sql
  $jersey_restclient_connection_timeout = $mobi_aaa_purchase_manager::jersey_restclient_connection_timeout
  $jersey_restclient_read_timeout = $mobi_aaa_purchase_manager::jersey_restclient_read_timeout
  $jersey_restclient_max_connections = $mobi_aaa_purchase_manager::jersey_restclient_max_connections
  $jersey_restclient_enable_client_log = $mobi_aaa_purchase_manager::jersey_restclient_enable_client_log
  $auth_manager_endpoint_url = $mobi_aaa_purchase_manager::auth_manager_endpoint_url
  $identity_manager_endpoint_url = $mobi_aaa_purchase_manager::identity_manager_endpoint_url
  $offer_manager_endpoint_url = $mobi_aaa_purchase_manager::offer_manager_endpoint_url
  $rights_manager_endpoint_url = $mobi_aaa_purchase_manager::rights_manager_endpoint_url
  $transaction_processor_endpoint_url = $mobi_aaa_purchase_manager::transaction_processor_endpoint_url
  $session_manager_endpoint_url = $mobi_aaa_purchase_manager::session_manager_endpoint_url
  $carrier_vendor = $mobi_aaa_purchase_manager::carrier_vendor
  $zookeeper_connect_string = $mobi_aaa_purchase_manager::zookeeper_connect_string
  $rights_manager_service_name = $mobi_aaa_purchase_manager::rights_manager_service_name
  $auth_enable_session_manager = $mobi_aaa_purchase_manager::auth_enable_session_manager
  $auth_use_legacy_verify_tokens_endpoint = $mobi_aaa_purchase_manager::auth_use_legacy_verify_tokens_endpoint
  $purchase_manager_service_name = $mobi_aaa_purchase_manager::purchase_manager_service_name

  file { "/opt/${mobi_aaa_purchase_manager::mobi_aaa_purchase_manager_package_name}/conf/application-config-override.xml":
    ensure => present,
    content => template("mobi_aaa_purchase_manager/application-config-override.xml.erb"),
    require => Class["mobi_aaa_purchase_manager::install"],
    notify => Class["tomcat::service"],
  }

  file { "/opt/${mobi_aaa_purchase_manager::mobi_aaa_purchase_manager_package_name}/conf/carrier-vendor-mapping.properties":
    ensure => present,
    content => template("mobi_aaa_purchase_manager/carrier-vendor-mapping.properties.erb"),
    require => Class["mobi_aaa_purchase_manager::install"],
    notify => Class["tomcat::service"],
  }
  # Adjustment to support ril custom package with custom name but mostly similar parameters
  file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/mobi-aaa-purchase-manager.xml":
    ensure => present,
    content => template("mobi_aaa_purchase_manager/mobi-aaa-purchase-manager.xml.erb"),
    require => Class["mobi_aaa_purchase_manager::install"],
    notify => Class["tomcat::service"],
  }

}
