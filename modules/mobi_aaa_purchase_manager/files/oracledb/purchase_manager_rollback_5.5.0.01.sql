/* Oracle command to backup database : */
expdp system/mobitv DIRECTORY=data_pump_dir DUMPFILE=usr2020.dmp SCHEMAS=PURCHASE_MANAGER LOGFILE=usr2020_exp.log JOB_NAME=usr2020_exp

/* Oracle command to rollback the database */
impdp system/mobitv DIRECTORY=data_pump_dir DUMPFILE=usr2020.dmp TABLE_EXISTS_ACTION=REPLACE LOGFILE=usr2020_imp.log JOB_NAME=usr2020_imp

