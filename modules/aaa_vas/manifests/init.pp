# == Class: aaa_vas
#
#  installs vending app server ng component
#
# === Parameters:
#
#    $vas_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "aaa_vas" :
#  }
#
# Remember: No empty lines between comments and class definition
#
#
class aaa_vas (
    $vas_ver = $aaa_vas::params::vas_ver,
    $db_url = $aaa_vas::params::db_url,
    $db_username = $aaa_vas::params::db_username,
    $db_password = $aaa_vas::params::db_password,
    $recocile_url = $aaa_vas::params::reconcile_url,
    $pending_transaction_url = $aaa_vas::params::pending_transaction_url,
    $trial_sub_notif_url = $aaa_vas::params::trial_sub_notif_url,
    $trial_unsub_notif_url = $aaa_vas::params::trial_unsub_notif_url,
)
inherits aaa_vas::params {
    include aaa_vas::install, aaa_vas::config
    anchor { "aaa_vas::begin": } -> Class["aaa_vas::install"]
    Class["aaa_vas::config"] -> anchor { "aaa_vas::end" :}
    os::motd::register { $name : }
}
