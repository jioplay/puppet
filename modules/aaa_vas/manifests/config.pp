class aaa_vas::config {

    $vas_ver = $aaa_vas::vas_ver
    $db_url = $aaa_vas::db_url
    $db_username = $aaa_vas::db_username
    $db_password = $aaa_vas::db_password
    $recocile_url = $aaa_vas::reconcile_url
    $pending_transaction_url = $aaa_vas::pending_transaction_url
    $trial_sub_notif_url = $aaa_vas::trial_sub_notif_url
    $trial_unsub_notif_url = $aaa_vas::trial_unsub_notif_url

    file { "/opt/mobi-aaa/config/vending-app-server/applicationProperties.xml":
    ensure => file,
    content => template("aaa_vas/applicationProperties.xml.erb"),
    notify => Class["tomcat::service"],
    require => Class["aaa_vas::install"],
    }
}
