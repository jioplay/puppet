class aaa_vas::install {
    package { "mobi-aaa-vending-app-server-${aaa_vas::vas_ver}" :
        ensure => present,
  notify => Class["tomcat::service"]
    }
}