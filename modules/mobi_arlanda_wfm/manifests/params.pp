class mobi_arlanda_wfm::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /wfm/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $version = "5.0.0-189083"

    $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100"

    $dam_url = "http://juarnvip:8080/dam/ws?wsdl"
    $ad_broker_url = "http://adrequestorvip:8080/mobi-ad-requestor/services/AdService?wsdl"
    $folder_manager_webservice_url = "http://juarnvip:8080/foldermanager/ws?wsdl"
    $folder_manager_rest_url = "http://juarnvip:8080/foldermanager/"

    $db_username = "wfm_user"
    $db_password = "wfm_user"

    # Global database params
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $hibernate_database_vendor = "MYSQL"
    $jdbc_driver = "com.mysql.jdbc.Driver"
    $connection_test_query = "select 1 from dual"

    $wfm_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/wfm?autoReconnect=true"
    $vantage_workflow_name = "default"
    $smtp_host = "mailhost.smf1.mobitv"
    $mediainfo_timeout = "30000"

    $vantage_to_platform_xsl = undef
    $adspot_basedir = undef
    $provider_adspot_converter_path = undef
    $provider_metadata_path = undef
    $ril = false
    $provider_files = ["2","3"]
    $default_video_frame_rate = "25"
}
