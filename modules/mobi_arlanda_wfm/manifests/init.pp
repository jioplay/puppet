# == Class: mobi_arlanda_wfm
#
#  installs juarn component
#
# === Parameters:
#
#    $version::
#    $db_username::
#    $db_password::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_arlanda_wfm" :
#           version => "5.0.0-180924",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_wfm (
    ###icinga.pp
    $icinga = $mobi_arlanda_wfm::params::icinga,
    $icinga_instance = $mobi_arlanda_wfm::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_wfm::params::icinga_cmd_args,
    ###end icinga.pp
    $version = $mobi_arlanda_wfm::params::version,
    $amq_broker_url = $mobi_arlanda_wfm::params::amq_broker_url,
    $db_username = $mobi_arlanda_wfm::params::db_username,
    $db_password = $mobi_arlanda_wfm::params::db_password,
    $hibernate_database_dialect = $mobi_arlanda_wfm::params::hibernate_database_dialect,
    $hibernate_database_vendor = $mobi_arlanda_wfm::params::hibernate_database_vendor,
    $jdbc_driver = $mobi_arlanda_wfm::params::jdbc_driver,
    $connection_test_query = $mobi_arlanda_wfm::params::connection_test_query,
    $wfm_db_jdbc_url = $mobi_arlanda_wfm::params::wfm_db_jdbc_url,
    $dam_url = $mobi_arlanda_wfm::params::dam_url,
    $ad_broker_url = $mobi_arlanda_wfm::params::ad_broker_url,
    $folder_manager_webservice_url = $mobi_arlanda_wfm::params::folder_manager_webservice_url,
    $folder_manager_rest_url = $mobi_arlanda_wfm::params::folder_manager_rest_url,
    $vantage_workflow_name = $mobi_arlanda_wfm::params::vantage_workflow_name,
    $smtp_host = $mobi_arlanda_wfm::params::smtp_host,
    $mediainfo_timeout = $mobi_arlanda_wfm::params::mediainfo_timeout,
    $vantage_to_platform_xsl = $mobi_arlanda_wfm::params::vantage_to_platform_xsl,
    $adspot_basedir = $mobi_arlanda_wfm::params::adspot_basedir,
    $provider_adspot_converter_path = $mobi_arlanda_wfm::params::provider_adspot_converter_path,
    $provider_metadata_path = $mobi_arlanda_wfm::params::provider_metadata_path,
    $ril = $mobi_arlanda_wfm::params::ril,
    $provider_files = $mobi_arlanda_wfm::params::provider_files,
    $default_video_frame_rate =  $mobi_arlanda_wfm::params::default_video_frame_rate,
)

inherits mobi_arlanda_wfm::params {
    include mobi_arlanda_wfm::install, mobi_arlanda_wfm::config, mobi_cms_us::tomcat_config
    anchor { "mobi_arlanda_wfm::begin": } -> Class["mobi_arlanda_wfm::install"] ->
    Class["mobi_arlanda_wfm::config"] ->
    Class["mobi_cms_us::tomcat_config"] ->
    class { "mobi_arlanda_wfm::icinga":} ->
    anchor { "mobi_arlanda_wfm::end" :} -> os::motd::register { $name : }
}

