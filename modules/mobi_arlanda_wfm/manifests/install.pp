class mobi_arlanda_wfm::install {

    package { "mobi-arlanda-wfm" :
        ensure => "${mobi_arlanda_wfm::version}",
        notify   => Class["tomcat::service"],
    }

    #file { "/opt/mobi-arlanda-wfm/database":
    #    ensure => "directory",
    #    owner  => "rtv",
    #    group  => "rtv",
    #    mode   => "0755",
    #    require => File["/opt/mobi-arlanda-wfm"],
    #}

    #file { "/opt/mobi-arlanda-wfm/database/postgres":
    #    ensure => "directory",
    #    owner  => "rtv",
    #    group  => "rtv",
    #    mode   => "0755",
    #    require => File["/opt/mobi-arlanda-wfm/database"],
    #}

    file { ["/opt/mobi-arlanda-wfm", "/opt/mobi-arlanda-wfm/database", "/opt/mobi-arlanda-wfm/database/postgres", "/opt/mobi-arlanda-wfm/database/postgres/eesimulator"]:
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
    }

    file { "/opt/mobi-arlanda-wfm/database/postgres/wfm_schema.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_wfm/wfm_schema.sql.erb'),
        require => File["/opt/mobi-arlanda-wfm/database/postgres"],
    }

    file { "/opt/mobi-arlanda-wfm/database/postgres/wfm_seed.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_wfm/wfm_seed.sql.erb'),
        require => File["/opt/mobi-arlanda-wfm/database/postgres"],
    }

    file { "/opt/mobi-arlanda-wfm/database/postgres/eesimulator/wfm_seed.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_wfm/eesimulator/wfm_seed.sql.erb'),
        require => File["/opt/mobi-arlanda-wfm/database/postgres/eesimulator"],
    }

}
