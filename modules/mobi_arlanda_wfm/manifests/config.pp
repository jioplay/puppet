class mobi_arlanda_wfm::config {
    # For RIL
    define provider_file {
            file { "/var/Jukebox/uploads/search_transform/adspot/$name":
                ensure => directory,
                mode   => "0664",
                owner  => "rtv",
                group  => "arlanda",
            }

            file { "/var/Jukebox/uploads/search_transform/adspot/$name/$name.xml":
                ensure => file,
                mode    => "0664",
                owner   => "rtv",
                group   => "arlanda",
                source  => "puppet:///modules/mobi_arlanda_wfm/ril/$name.xml",
                require => File["/var/Jukebox/uploads/search_transform/adspot/$name"],
            }

        }

    define smil_file {
        file { "/var/Jukebox/uploads/smil/templates/$name.xml":
            ensure   => file,
            replace  => false,
            mode     => "0664",
            owner    => "rtv",
            group    => "arlanda",
            source   => "puppet:///modules/mobi_arlanda_wfm/ril/smil/$name.xml",
            require  => Class["mobi_arlanda_wfm::install"],
        }
    }
    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/wfm.xml":
        replace => true,
        owner  => "rtv",
        group  => "arlanda",
        mode => "0664",
        content => template('mobi_arlanda_wfm/wfm-tomcat-context.xml.erb'),
        require => Class["mobi_arlanda_wfm::install"],
        notify   => Class["tomcat::service"],
    }

    # Database persistence files
    file { "/opt/mobi-arlanda-wfm/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "arlanda",
        mode => "0664",
        content => template('mobi_arlanda_wfm/wfm-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_arlanda_wfm::install"],
    }


    # Customized Quartz Spring configs
    file { "/opt/mobi-arlanda-wfm/webapp/WEB-INF/classes/spring/quartzContext.xml":
        replace => true,
        owner  => "rtv",
        group  => "arlanda",
        mode => "0664",
        content => template('mobi_arlanda_wfm/wfm-quartzContext.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_arlanda_wfm::install"],
    }

    file { "/opt/arlanda/conf/wfm/wfm.properties":
        replace => true,
        owner  => "rtv",
        group  => "arlanda",
        mode => "0664",
        content => template('mobi_arlanda_wfm/wfm.properties.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_arlanda_wfm::install"],
    }

    if($mobi_arlanda_wfm::ril){
 #       provider_file { $mobi_arlanda_wfm::provider_files: }
 #       smil_file { $mobi_arlanda_wfm::provider_files: }

#        file {"/var/Jukebox/uploads/search_transform/vantage.xsl":
#            replace => true,
#            owner   => "rtv",
#            group   => "arlanda",
#            mode    => "0664",
#            source  => "puppet:///modules/mobi_arlanda_wfm/ril/vantage.xsl",
#        }

#        file {"/var/Jukebox/uploads/search_transform/vantagetoplatform.xsl":
#            replace => true,
#            owner   => "rtv",
#            group   => "arlanda",
#            mode    => "0664",
#            source  => "puppet:///modules/mobi_arlanda_wfm/ril/vantagetoplatform.xsl",
#        }
    }
}
