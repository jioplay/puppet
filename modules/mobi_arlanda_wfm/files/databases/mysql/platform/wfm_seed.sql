Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (141,'FragmentedMp4','Fragmented MP4 encoding set',0) ON DUPLICATE KEY UPDATE ID = 141;

Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (225,'Core_Platform_4x3','Core_Platform_4x3 encoding set.',0) ON DUPLICATE KEY UPDATE ID = 225;
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (226,'Core_Platform_16x9','Core_Platform_16x9 encoding set.',0) ON DUPLICATE KEY UPDATE ID = 226; 
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (227,'Core Platform 16:9 (HE-AAC)','Core_Platform_16x9 HE AAC encoding set.',0) ON DUPLICATE KEY UPDATE NAME = 'Core Platform 16:9 (HE-AAC)'; 


Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (69,11) ON DUPLICATE KEY UPDATE ID = 69;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (61,11) ON DUPLICATE KEY UPDATE ID = 61;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (401,1) ON DUPLICATE KEY UPDATE ID = 401;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (402,2) ON DUPLICATE KEY UPDATE ID = 402;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (403,3) ON DUPLICATE KEY UPDATE ID = 403;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (404,4) ON DUPLICATE KEY UPDATE ID = 404;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (405,5) ON DUPLICATE KEY UPDATE ID = 405;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (406,6) ON DUPLICATE KEY UPDATE ID = 406;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (626,65) ON DUPLICATE KEY UPDATE ID = 626;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (521,94) ON DUPLICATE KEY UPDATE ID = 521;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (407,7) ON DUPLICATE KEY UPDATE ID = 407;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (408,8) ON DUPLICATE KEY UPDATE ID = 408;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (522,91) ON DUPLICATE KEY UPDATE ID = 522;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (627,66) ON DUPLICATE KEY UPDATE ID = 627;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (74,100) ON DUPLICATE KEY UPDATE ID = 74;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (628,75) ON DUPLICATE KEY UPDATE ID = 628;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (409,12) ON DUPLICATE KEY UPDATE ID = 409;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (410,13) ON DUPLICATE KEY UPDATE ID = 410;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (411,14) ON DUPLICATE KEY UPDATE ID = 411;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (412,15) ON DUPLICATE KEY UPDATE ID = 412;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (413,18) ON DUPLICATE KEY UPDATE ID = 413;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (569,70) ON DUPLICATE KEY UPDATE ID = 569;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (570,71) ON DUPLICATE KEY UPDATE ID = 570;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (181,102) ON DUPLICATE KEY UPDATE ID = 181;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (182,101) ON DUPLICATE KEY UPDATE ID = 182;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (571,77) ON DUPLICATE KEY UPDATE ID = 571;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (572,78) ON DUPLICATE KEY UPDATE ID = 572;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (573,79) ON DUPLICATE KEY UPDATE ID = 573;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (574,72) ON DUPLICATE KEY UPDATE ID = 574;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (575,73) ON DUPLICATE KEY UPDATE ID = 575;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (576,74) ON DUPLICATE KEY UPDATE ID = 576;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (462,21) ON DUPLICATE KEY UPDATE ID = 462;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (463,23) ON DUPLICATE KEY UPDATE ID = 463;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (464,22) ON DUPLICATE KEY UPDATE ID = 464;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (465,25) ON DUPLICATE KEY UPDATE ID = 465;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (466,24) ON DUPLICATE KEY UPDATE ID = 466;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (467,27) ON DUPLICATE KEY UPDATE ID = 467;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (468,26) ON DUPLICATE KEY UPDATE ID = 468;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (361,73) ON DUPLICATE KEY UPDATE ID = 361; 
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (523,93) ON DUPLICATE KEY UPDATE ID = 523;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (524,95) ON DUPLICATE KEY UPDATE ID = 524;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (601,103) ON DUPLICATE KEY UPDATE ID = 601;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (121,41) ON DUPLICATE KEY UPDATE ID = 121;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (122,40) ON DUPLICATE KEY UPDATE ID = 122;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (577,68) ON DUPLICATE KEY UPDATE ID = 577;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (578,69) ON DUPLICATE KEY UPDATE ID = 578;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (579,64) ON DUPLICATE KEY UPDATE ID = 579;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (580,67) ON DUPLICATE KEY UPDATE ID = 580;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (581,62) ON DUPLICATE KEY UPDATE ID = 581;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (582,61) ON DUPLICATE KEY UPDATE ID = 582;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (583,60) ON DUPLICATE KEY UPDATE ID = 583;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (602,104) ON DUPLICATE KEY UPDATE ID = 602;

Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6002,6002) ON DUPLICATE KEY UPDATE ID = 6002; 
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6003,6003) ON DUPLICATE KEY UPDATE ID = 6003;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6004,6004) ON DUPLICATE KEY UPDATE ID = 6004;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6005,6005) ON DUPLICATE KEY UPDATE ID = 6005;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6006,6006) ON DUPLICATE KEY UPDATE ID = 6006;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6007,6007) ON DUPLICATE KEY UPDATE ID = 6007;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6008,6008) ON DUPLICATE KEY UPDATE ID = 6008;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6009,6009) ON DUPLICATE KEY UPDATE ID = 6009;

Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6052,6052) ON DUPLICATE KEY UPDATE ID = 6052;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6053,6053) ON DUPLICATE KEY UPDATE ID = 6053;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6054,6054) ON DUPLICATE KEY UPDATE ID = 6054;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6055,6055) ON DUPLICATE KEY UPDATE ID = 6055;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6056,6056) ON DUPLICATE KEY UPDATE ID = 6056;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6057,6057) ON DUPLICATE KEY UPDATE ID = 6057;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6058,6058) ON DUPLICATE KEY UPDATE ID = 6058;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6059,6059) ON DUPLICATE KEY UPDATE ID = 6059;

Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6072,6072) ON DUPLICATE KEY UPDATE ID = 6072;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6073,6073) ON DUPLICATE KEY UPDATE ID = 6073;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6074,6074) ON DUPLICATE KEY UPDATE ID = 6074;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6075,6075) ON DUPLICATE KEY UPDATE ID = 6075;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6076,6076) ON DUPLICATE KEY UPDATE ID = 6076;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6077,6077) ON DUPLICATE KEY UPDATE ID = 6077;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6078,6078) ON DUPLICATE KEY UPDATE ID = 6078;
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6079,6079) ON DUPLICATE KEY UPDATE ID = 6079;

INSERT INTO WFM_ENCODING_JOB (ID, ENCODING_JOB_ID) VALUES (6060, 6060) ON DUPLICATE KEY UPDATE ID = 6060;
INSERT INTO WFM_ENCODING_JOB (ID, ENCODING_JOB_ID) VALUES (6061, 6061) ON DUPLICATE KEY UPDATE ID = 6061;
INSERT INTO WFM_ENCODING_JOB (ID, ENCODING_JOB_ID) VALUES (6062, 6062) ON DUPLICATE KEY UPDATE ID = 6062;
INSERT INTO WFM_ENCODING_JOB (ID, ENCODING_JOB_ID) VALUES (6063, 6063) ON DUPLICATE KEY UPDATE ID = 6063;
INSERT INTO WFM_ENCODING_JOB (ID, ENCODING_JOB_ID) VALUES (6080, 6080) ON DUPLICATE KEY UPDATE ID = 6080;
INSERT INTO WFM_ENCODING_JOB (ID, ENCODING_JOB_ID) VALUES (6081, 6081) ON DUPLICATE KEY UPDATE ID = 6081;


Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (225,6002) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 225;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (225,6003) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 225;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (225,6004) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 225;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (225,6005) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 225;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (225,6006) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 225;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (225,6007) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 225;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (225,6008) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 225;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (225,6009) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 225;


Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (226,6052) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (226,6053) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (226,6054) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (226,6055) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (226,6056) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (226,6057) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (226,6058) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (226,6059) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;

Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (227,6072) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (227,6073) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (227,6074) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (227,6075) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (227,6076) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (227,6077) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (227,6078) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (227,6079) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;

INSERT INTO WFM_ENCODING_MAP (WFM_ENCODING_ID, MP_ENCODING_ID) VALUES (226, 6060) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
INSERT INTO WFM_ENCODING_MAP (WFM_ENCODING_ID, MP_ENCODING_ID) VALUES (226, 6061) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
INSERT INTO WFM_ENCODING_MAP (WFM_ENCODING_ID, MP_ENCODING_ID) VALUES (226, 6062) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
INSERT INTO WFM_ENCODING_MAP (WFM_ENCODING_ID, MP_ENCODING_ID) VALUES (226, 6063) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 226;
INSERT INTO WFM_ENCODING_MAP (WFM_ENCODING_ID, MP_ENCODING_ID) VALUES (227, 6080) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;
INSERT INTO WFM_ENCODING_MAP (WFM_ENCODING_ID, MP_ENCODING_ID) VALUES (227, 6081) ON DUPLICATE KEY UPDATE WFM_ENCODING_ID = 227;


Insert into WFM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('workflowCleanupJob','DEFAULT',null,'com.mobitv.arlanda.wfm.tasks.CleanupTask','0','0','0','0') ON DUPLICATE KEY UPDATE JOB_NAME = 'workflowCleanupJob';

Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (61,'TranscodeWinMo','Winmo transcode workflow','TranscodeWinMo','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>WinMo</value>
                </dataitem>
            </taskdata>
        </task>
    </tasklist>
</workflowdescription>
') ON DUPLICATE KEY UPDATE ID = 61;
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (2,'DefaultTranscode','Default content transcoding workflow','Transcode','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
     <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
        <dependency>GetAssetInfo</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>GetAssetInfo</id>
      <class>GetAssetInfoTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>') ON DUPLICATE KEY UPDATE ID = 2; 
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (81,'TranscodeRTSP','RTSP transcode workflow','TranscodeRTSP','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>RTSP</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
') ON DUPLICATE KEY UPDATE ID = 81; 
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (121,'DefaultAudioIngest','Default audio-only content ingestion workflow','IngestAudioContent','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>XCodeAudioSetup</id>
            <class>XCodeAudioSetup</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeAudioSetup</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
') ON DUPLICATE KEY UPDATE ID = 121;
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (161,'TranscodeProgressiveDownload','Progressive Download transcode workflow','TranscodeProgressiveDownload','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>ProgressiveDownload</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
') ON DUPLICATE KEY UPDATE ID = 161; 
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (1,'DefaultIngest','Default content ingestion workflow','IngestContent','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
        <dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
        <dependency>XCodePreflight</dependency>
      </dependencies>
     <taskdata>
	 <dataitem>
	 <key>xcodeTaskName</key>
	 <value>ElementalXCodeTask</value>
	 </dataitem>
	 <dataitem>
	 <key>produceEncodingsForAllAspectRatios</key>
	 <value>false</value>
	 </dataitem>
	 </taskdata>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>') ON DUPLICATE KEY UPDATE ID = 1;
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (201,'TranscodePCTV','PCTV transcode workflow','TranscodePCTV','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>PCTV</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
') ON DUPLICATE KEY UPDATE ID = 201; 
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (141,'RegenerateProviderSMILDocuments','Default SMIL document regeneration workflow','RegenerateProviderSMILDocuments','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>RegenerateProviderSMILDocuments</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>RegenerateProviderSMILDocuments</id>
            <class>RegenerateProviderSMILDocumentsTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
') ON DUPLICATE KEY UPDATE ID = 141; 
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (181,'IngestWithAds','Default ingestion workflow with ads','IngestContent','<?xml version="1.0"
  encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
<dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
<dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
<dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
<dependency>XCodePreflight</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>') ON DUPLICATE KEY UPDATE ID = 181; 
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (182,'TranscodeWithAds','Default transcode workflow with ads','Transcode','<?xml version="1.0"
  encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
<dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
<dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
<dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
<dependency>XCodePreflight</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>') ON DUPLICATE KEY UPDATE ID = 182; 

Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (191,'RegenerateProviderMidRollSMILDocuments','Default Mid Roll SMIL document regeneration workflow','RegenerateProviderMidRollSMILDocuments','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>RegenerateProviderMidRollSMILDocuments</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>RegenerateProviderMidRollSMILDocuments</id>
            <class>RegenerateProviderMidRollSMILDocumentsTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
') ON DUPLICATE KEY UPDATE ID = 191; 

Insert into WFM_PROVIDER_CONTEXT (ID,PRIORITY,CONTENT_AUTH_MODE,TRANSCODING_CLUSTER,ADVERTISING_SOURCE,ENCRYPTION_REQUIRED) values (2,500,'NotRequired','simulator',null,'FALSE') ON DUPLICATE KEY UPDATE ID = 2; 

Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,141) ON DUPLICATE KEY UPDATE PROVIDER_ID = 2;
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,225) ON DUPLICATE KEY UPDATE PROVIDER_ID = 2;
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,226) ON DUPLICATE KEY UPDATE PROVIDER_ID = 2;
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,227) ON DUPLICATE KEY UPDATE PROVIDER_ID = 2;

Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (2,1) ON DUPLICATE KEY UPDATE PROVIDER_ID = 2;

commit;
