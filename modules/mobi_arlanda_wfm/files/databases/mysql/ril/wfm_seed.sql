use wfm;

INSERT INTO WFM_QRTZ_LOCKS values('TRIGGER_ACCESS');
INSERT INTO WFM_QRTZ_LOCKS values('JOB_ACCESS');
INSERT INTO WFM_QRTZ_LOCKS values('CALENDAR_ACCESS');
INSERT INTO WFM_QRTZ_LOCKS values('STATE_ACCESS');
INSERT INTO WFM_QRTZ_LOCKS values('MISFIRE_ACCESS');

Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (1,'Core','Core QVGA encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (2,'Core 16x9','Core WQVGA encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (3,'AppleSegmentedVod','Apple Segmented Vod encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (4,'FragmentedMp4','Fragmented MP4 encoding set',0);

Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (1,1);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (2,2);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (3,3);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (4,4);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (5,5);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6,6);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (7,7);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (8,8);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (9,9);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (10,10);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (11,11);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (12,12);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (13,13);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (14,14);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (15,15);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (16,16);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (17,17);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (18,18);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (19,19);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (20,20);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (21,21);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (22,22);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (23,23);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (24,24);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (25,25);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (26,26);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (27,27);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (28,28);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (29,29);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (30,30);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (31,31);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (32,32);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (33,33);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (34,34);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (35,35);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (36,36);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (37,37);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (38,38);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (39,39);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (40,40);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (41,41);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (42,42);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (43,43);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (44,44);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (45,45);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (46,46);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (47,47);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (48,48);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (49,49);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (50,50);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (51,51);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (52,52);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (53,53);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (54,54);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (55,55);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (56,56);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (57,57);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (58,58);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (59,59);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (60,60);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (61,61);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (62,62);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (63,63);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (64,64);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (65,65);

Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,1);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,2);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,3);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,4);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,5);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,6);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,7);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,8);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,9);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,10);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,11);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,12);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,13);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,14);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,15);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,16);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,17);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,18);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,19);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,20);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,21);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,22);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,23);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,24);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,25);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,26);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,27);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,28);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,29);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,30);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,31);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,32);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,33);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,34);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,35);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,36);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,37);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,38);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,39);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,40);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,41);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,42);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,43);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,44);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,45);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,46);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,47);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,48);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,49);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,50);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,51);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,52);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,53);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,54);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,55);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,56);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,57);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,58);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,59);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,60);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,61);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,62);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,63);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,64);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,65);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,1);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,2);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,3);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,4);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,5);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,6);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,7);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,8);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,9);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,10);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,11);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,12);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,13);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,14);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,15);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,16);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,17);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,18);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,19);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,20);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,21);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,22);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,23);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,24);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,25);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,26);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,27);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,28);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,29);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,30);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,31);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,32);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,33);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,34);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,35);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,36);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,37);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,38);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,39);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,40);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,41);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,42);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,43);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,44);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,45);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,46);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,47);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,48);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,49);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,50);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,51);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,52);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,53);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,54);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,55);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,56);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,57);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,58);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,59);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,60);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,61);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,62);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,63);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,64);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,65);


INSERT INTO `WFM_QRTZ_JOB_DETAILS` VALUES ('workflowCleanupJob','DEFAULT',NULL,'com.mobitv.arlanda.wfm.tasks.CleanupTask','0','0','0','0','��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xp\0sr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0\0x\0');

Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (1,'Default content ingestion workflow','DefaultIngest','IngestContent','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
 <tasklist>
 <task>
 <id>Finished</id>
 <class>FinishedTask</class>
 <dependencies>
 <dependency>MakeAssetAvailable</dependency>
 </dependencies>
 <taskdata/>
 </task>
 <task>
 <id>MakeAssetAvailable</id>
 <class>MakeAssetAvailableTask</class>
 <dependencies>
 <dependency>MidRollSMILGenerator</dependency>
 </dependencies>
 <taskdata/>
 </task>
<!-- <task>
 <id>CreateSMILDocumentForAsset</id>
 <class>CreateSMILDocumentForAssetTask</class>
 <dependencies>
 <dependency>DeleteEncoding</dependency>
 </dependencies>
 <taskdata/>
 </task> -->
 <task>
 <id>MidRollSMILGenerator</id>
 <class>MidRollSMILGeneratorTask</class>
 <dependencies>
 <dependency>DeleteEncoding</dependency>
 </dependencies>
 <taskdata/>
 </task>
 <task>
 <id>DeleteEncoding</id>
 <class>DeleteEncodingTask</class>
 <dependencies>
 <dependency>XCodeSetup</dependency>
 </dependencies>
 <taskdata>
 <dataitem>
 <key>encodingName</key>
 <value>Core</value>
 </dataitem>
 </taskdata>
 </task>
 <task>
 <id>XCodeSetup</id>
 <class>XCodeSetup</class>
 <dependencies>
 <dependency>XCodePreflight</dependency>
 </dependencies>
 <taskdata>
 <dataitem>
 <key>xcodeTaskName</key>
 <value>VantageXCodeTask</value>
 </dataitem>
 <dataitem>
 <key>produceEncodingsForAllAspectRatios</key>
 <value>false</value>
 </dataitem>
 </taskdata>
 </task>
 <task>
 <id>XCodePreflight</id>
 <class>XCodePreflight</class>
 <dependencies>
 <dependency>SelectAdvertisements</dependency>
 </dependencies>
 <taskdata/>
 </task>
 <task>
 <id>SelectAdvertisements</id>
 <class>SelectAdvertisementsTask</class>
 <dependencies/>
 <taskdata/>
 </task>
 </tasklist>
</workflowdescription>');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (2,'Default content transcoding workflow','DefaultTranscode','Transcode','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
        <dependency>GetAssetInfo</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>GetAssetInfo</id>
      <class>GetAssetInfoTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');

Insert into WFM_PROVIDER_CONTEXT (ID,ADVERTISING_SOURCE,CONTENT_AUTH_MODE,ENCRYPTION_REQUIRED,PRIORITY,TRANSCODING_CLUSTER,TRANSCODING_WORKFLOW_NAME) values (2,NULL,'NotRequired','TRUE',500,'simulator','Movies_workflow_Part1');
Insert into WFM_PROVIDER_CONTEXT (ID,ADVERTISING_SOURCE,CONTENT_AUTH_MODE,ENCRYPTION_REQUIRED,PRIORITY,TRANSCODING_CLUSTER,TRANSCODING_WORKFLOW_NAME) values (3,NULL,'NotRequired','FALSE',500,'simulator','Movies_workflow_Part1');
Insert into WFM_PROVIDER_CONTEXT (ID,ADVERTISING_SOURCE,CONTENT_AUTH_MODE,ENCRYPTION_REQUIRED,PRIORITY,TRANSCODING_CLUSTER,TRANSCODING_WORKFLOW_NAME) values (4,NULL,'NotRequired','FALSE',500,'simulator','YuMe_Workflow');
Insert into WFM_PROVIDER_CONTEXT (ID,ADVERTISING_SOURCE,CONTENT_AUTH_MODE,ENCRYPTION_REQUIRED,PRIORITY,TRANSCODING_CLUSTER,TRANSCODING_WORKFLOW_NAME) values (5,'AdRequestor','NotRequired','FALSE',500,'simulator','Star_TVShow_Workflow');
Insert into WFM_PROVIDER_CONTEXT (ID,ADVERTISING_SOURCE,CONTENT_AUTH_MODE,ENCRYPTION_REQUIRED,PRIORITY,TRANSCODING_CLUSTER,TRANSCODING_WORKFLOW_NAME) values (6,NULL,'NotRequired','TRUE',500,'simulator','Movies_workflow_Part1');
Insert into WFM_PROVIDER_CONTEXT (ID,ADVERTISING_SOURCE,CONTENT_AUTH_MODE,ENCRYPTION_REQUIRED,PRIORITY,TRANSCODING_CLUSTER,TRANSCODING_WORKFLOW_NAME) values (7,NULL,'NotRequired','FALSE',500,'simulator','Movies_workflow_Part1');
Insert into WFM_PROVIDER_CONTEXT (ID,ADVERTISING_SOURCE,CONTENT_AUTH_MODE,ENCRYPTION_REQUIRED,PRIORITY,TRANSCODING_CLUSTER,TRANSCODING_WORKFLOW_NAME) values (8,NULL,'NotRequired','FALSE',500,'simulator','TVShow_Workflow');
INSERT INTO `WFM_PROVIDER_CONTEXT` VALUES (2,NULL,'NotRequired','TRUE',500,'simulator','Movies_workflow_Part1'),(3,NULL,'NotRequired','FALSE',500,'simulator','Movies_workflow_Part1'),(4,NULL,'NotRequired','FALSE',500,'simulator','YuMe_Workflow'),(5,'AdRequestor','NotRequired','FALSE',500,'simulator','Star_TVShow_Workflow_withFrameRate'),(6,NULL,'NotRequired','TRUE',500,'simulator','Movies_workflow_Part1'),(7,NULL,'NotRequired','FALSE',500,'simulator','Movies_workflow_Part1'),(8,NULL,'NotRequired','FALSE',500,'simulator','TVShow_Workflow');


Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,1);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (3,1);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (4,1);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (5,1);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (6,1);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (7,1);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (8,1);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,4);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (3,4);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (4,4);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (5,4);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (6,4);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (7,4);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (8,4);

Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (2,1);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (2,2);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (3,1);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (3,2);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (4,1);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (4,2);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (5,1);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (5,2);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (6,1);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (6,2);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (7,1);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (7,2);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (8,1);
Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (8,2);

commit;
