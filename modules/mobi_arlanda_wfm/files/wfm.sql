
-- PostgreSQL database dump
--
\c wfm;

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: wfm_asset_context; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_asset_context (
    id bigint NOT NULL,
    asset_id bigint,
    carrier_approved boolean,
    expires character varying(255),
    inventory_id bigint,
    media_aspect_ratio character varying(255),
    media_audio_channels character varying(255),
    media_container_format character varying(255),
    media_url character varying(1023),
    metadata_url character varying(1023),
    post_roll_url character varying(1023),
    pre_roll_url character varying(1023),
    provider_id bigint,
    version integer
);


ALTER TABLE public.wfm_asset_context OWNER TO <%= db_username %>;

--
-- Name: wfm_asset_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_asset_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_asset_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_asset_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_asset_seq', 1000, false);


--
-- Name: wfm_email_notification; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_email_notification (
    id bigint NOT NULL,
    email_address character varying(1023) NOT NULL,
    notification_type character varying(64) NOT NULL
);


ALTER TABLE public.wfm_email_notification OWNER TO <%= db_username %>;

--
-- Name: wfm_email_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_email_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_email_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_email_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_email_seq', 1, false);


--
-- Name: wfm_encoding; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_encoding (
    id bigint NOT NULL,
    description character varying(1023),
    is_encrypted boolean,
    name character varying(255) NOT NULL
);


ALTER TABLE public.wfm_encoding OWNER TO <%= db_username %>;

--
-- Name: wfm_encoding_job; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_encoding_job (
    id bigint NOT NULL,
    encoding_job_id bigint NOT NULL
);


ALTER TABLE public.wfm_encoding_job OWNER TO <%= db_username %>;

--
-- Name: wfm_encoding_job_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_encoding_job_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_encoding_job_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_encoding_job_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_encoding_job_seq', 1, false);


--
-- Name: wfm_encoding_map; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_encoding_map (
    wfm_encoding_id bigint NOT NULL,
    mp_encoding_id bigint NOT NULL
);


ALTER TABLE public.wfm_encoding_map OWNER TO <%= db_username %>;

--
-- Name: wfm_encoding_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_encoding_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_encoding_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_encoding_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_encoding_seq', 1, false);


--
-- Name: wfm_message_map; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_message_map (
    id bigint NOT NULL,
    correlation_id character varying(255) NOT NULL,
    message_id character varying(255) NOT NULL,
    task_id bigint NOT NULL
);


ALTER TABLE public.wfm_message_map OWNER TO <%= db_username %>;

--
-- Name: wfm_provider_context; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_provider_context (
    id bigint NOT NULL,
    advertising_source character varying(255),
    content_auth_mode character varying(255) NOT NULL,
    encryption_required character varying(255) NOT NULL,
    priority integer NOT NULL,
    transcoding_cluster character varying(255) NOT NULL
);


ALTER TABLE public.wfm_provider_context OWNER TO <%= db_username %>;

--
-- Name: wfm_provider_encoding; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_provider_encoding (
    provider_id bigint NOT NULL,
    encoding_id bigint NOT NULL
);


ALTER TABLE public.wfm_provider_encoding OWNER TO <%= db_username %>;

--
-- Name: wfm_provider_notification; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_provider_notification (
    provider_id bigint NOT NULL,
    notification_id bigint NOT NULL
);


ALTER TABLE public.wfm_provider_notification OWNER TO <%= db_username %>;

--
-- Name: wfm_provider_workflow; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_provider_workflow (
    provider_id bigint NOT NULL,
    workflow_id bigint NOT NULL
);


ALTER TABLE public.wfm_provider_workflow OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_blob_triggers; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_blob_triggers (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    blob_data bytea
);


ALTER TABLE public.wfm_qrtz_blob_triggers OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_calendars; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_calendars (
    calendar_name character varying(80) NOT NULL,
    calendar bytea NOT NULL
);


ALTER TABLE public.wfm_qrtz_calendars OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_cron_triggers; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_cron_triggers (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    cron_expression character varying(120) NOT NULL,
    time_zone_id character varying(80)
);


ALTER TABLE public.wfm_qrtz_cron_triggers OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_fired_triggers; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_fired_triggers (
    entry_id character varying(95) NOT NULL,
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    is_volatile character varying(10) NOT NULL,
    instance_name character varying(80) NOT NULL,
    fired_time bigint NOT NULL,
    priority integer NOT NULL,
    state character varying(16) NOT NULL,
    job_name character varying(80),
    job_group character varying(80),
    is_stateful character varying(10),
    requests_recovery character varying(10)
);


ALTER TABLE public.wfm_qrtz_fired_triggers OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_job_details; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_job_details (
    job_name character varying(80) NOT NULL,
    job_group character varying(80) NOT NULL,
    description character varying(120),
    job_class_name character varying(128) NOT NULL,
    is_durable character varying(10) NOT NULL,
    is_volatile character varying(10) NOT NULL,
    is_stateful character varying(10) NOT NULL,
    requests_recovery character varying(10) NOT NULL,
    job_data bytea
);


ALTER TABLE public.wfm_qrtz_job_details OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_job_listeners; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_job_listeners (
    job_name character varying(80) NOT NULL,
    job_group character varying(80) NOT NULL,
    job_listener character varying(80) NOT NULL
);


ALTER TABLE public.wfm_qrtz_job_listeners OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_locks; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_locks (
    lock_name character varying(40) NOT NULL
);


ALTER TABLE public.wfm_qrtz_locks OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_paused_trigger_grps; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_paused_trigger_grps (
    trigger_group character varying(80) NOT NULL
);


ALTER TABLE public.wfm_qrtz_paused_trigger_grps OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_scheduler_state; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_scheduler_state (
    instance_name character varying(80) NOT NULL,
    last_checkin_time bigint NOT NULL,
    checkin_interval bigint NOT NULL
);


ALTER TABLE public.wfm_qrtz_scheduler_state OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_simple_triggers; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_simple_triggers (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    repeat_count bigint NOT NULL,
    repeat_interval bigint NOT NULL,
    times_triggered bigint NOT NULL
);


ALTER TABLE public.wfm_qrtz_simple_triggers OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_trigger_listeners; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_trigger_listeners (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    trigger_listener character varying(80) NOT NULL
);


ALTER TABLE public.wfm_qrtz_trigger_listeners OWNER TO <%= db_username %>;

--
-- Name: wfm_qrtz_triggers; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_qrtz_triggers (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    job_name character varying(80) NOT NULL,
    job_group character varying(80) NOT NULL,
    is_volatile character varying(10) NOT NULL,
    description character varying(120),
    next_fire_time bigint,
    prev_fire_time bigint,
    priority integer,
    trigger_state character varying(16) NOT NULL,
    trigger_type character varying(8) NOT NULL,
    start_time bigint NOT NULL,
    end_time bigint,
    calendar_name character varying(80),
    misfire_instr smallint,
    job_data bytea
);


ALTER TABLE public.wfm_qrtz_triggers OWNER TO <%= db_username %>;

--
-- Name: wfm_task_data; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_task_data (
    id bigint NOT NULL,
    "KEY" character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.wfm_task_data OWNER TO <%= db_username %>;

--
-- Name: wfm_task_data_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_task_data_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_task_data_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_task_data_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_task_data_seq', 1000, false);


--
-- Name: wfm_task_instance; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_task_instance (
    id bigint NOT NULL,
    finished_at timestamp without time zone,
    provider_id bigint,
    queued_at timestamp without time zone,
    started_at timestamp without time zone,
    task_status character varying(64) NOT NULL,
    task_type character varying(64) NOT NULL,
    optlock integer
);


ALTER TABLE public.wfm_task_instance OWNER TO <%= db_username %>;

--
-- Name: wfm_task_instance_data; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_task_instance_data (
    task_instance_id bigint NOT NULL,
    task_data_id bigint NOT NULL
);


ALTER TABLE public.wfm_task_instance_data OWNER TO <%= db_username %>;

--
-- Name: wfm_task_instance_dependency; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_task_instance_dependency (
    task_instance_id bigint NOT NULL,
    task_depends_on_id bigint NOT NULL
);


ALTER TABLE public.wfm_task_instance_dependency OWNER TO <%= db_username %>;

--
-- Name: wfm_task_instance_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_task_instance_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_task_instance_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_task_instance_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_task_instance_seq', 1000, false);


--
-- Name: wfm_task_message_map_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_task_message_map_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_task_message_map_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_task_message_map_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_task_message_map_seq', 1, false);


--
-- Name: wfm_workflow; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_workflow (
    id bigint NOT NULL,
    description character varying(1023),
    name character varying(255) NOT NULL,
    workflow_type character varying(255) NOT NULL,
    xml_template text
);


ALTER TABLE public.wfm_workflow OWNER TO <%= db_username %>;

--
-- Name: wfm_workflow_instance; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_workflow_instance (
    id bigint NOT NULL,
    finished_at timestamp without time zone,
    started_at timestamp without time zone,
    optlock integer,
    workflow_status character varying(64) NOT NULL,
    asset_context_id bigint NOT NULL,
    provider_context_id bigint NOT NULL,
    workflow_id bigint NOT NULL
);


ALTER TABLE public.wfm_workflow_instance OWNER TO <%= db_username %>;

--
-- Name: wfm_workflow_instance_task; Type: TABLE; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

CREATE TABLE wfm_workflow_instance_task (
    workflow_instance_id bigint NOT NULL,
    task_instance_id bigint
);


ALTER TABLE public.wfm_workflow_instance_task OWNER TO <%= db_username %>;

--
-- Name: wfm_workflow_intstance_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_workflow_intstance_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_workflow_intstance_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_workflow_intstance_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_workflow_intstance_seq', 1, false);


--
-- Name: wfm_workflow_seq; Type: SEQUENCE; Schema: public; Owner: <%= db_username %>
--

CREATE SEQUENCE wfm_workflow_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wfm_workflow_seq OWNER TO <%= db_username %>;

--
-- Name: wfm_workflow_seq; Type: SEQUENCE SET; Schema: public; Owner: <%= db_username %>
--

SELECT pg_catalog.setval('wfm_workflow_seq', 1, false);

--
-- Name: wfm_asset_context_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_asset_context
    ADD CONSTRAINT wfm_asset_context_pkey PRIMARY KEY (id);


--
-- Name: wfm_email_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_email_notification
    ADD CONSTRAINT wfm_email_notification_pkey PRIMARY KEY (id);


--
-- Name: wfm_encoding_job_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_encoding_job
    ADD CONSTRAINT wfm_encoding_job_pkey PRIMARY KEY (id);


--
-- Name: wfm_encoding_map_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_encoding_map
    ADD CONSTRAINT wfm_encoding_map_pkey PRIMARY KEY (wfm_encoding_id, mp_encoding_id);


--
-- Name: wfm_encoding_name_key; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_encoding
    ADD CONSTRAINT wfm_encoding_name_key UNIQUE (name);


--
-- Name: wfm_encoding_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_encoding
    ADD CONSTRAINT wfm_encoding_pkey PRIMARY KEY (id);


--
-- Name: wfm_message_map_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_message_map
    ADD CONSTRAINT wfm_message_map_pkey PRIMARY KEY (id);


--
-- Name: wfm_provider_context_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_provider_context
    ADD CONSTRAINT wfm_provider_context_pkey PRIMARY KEY (id);


--
-- Name: wfm_provider_encoding_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_provider_encoding
    ADD CONSTRAINT wfm_provider_encoding_pkey PRIMARY KEY (provider_id, encoding_id);


--
-- Name: wfm_provider_notification_notification_id_key; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_provider_notification
    ADD CONSTRAINT wfm_provider_notification_notification_id_key UNIQUE (notification_id);


--
-- Name: wfm_provider_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_provider_notification
    ADD CONSTRAINT wfm_provider_notification_pkey PRIMARY KEY (provider_id, notification_id);


--
-- Name: wfm_provider_workflow_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_provider_workflow
    ADD CONSTRAINT wfm_provider_workflow_pkey PRIMARY KEY (provider_id, workflow_id);


--
-- Name: wfm_qrtz_blob_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_blob_triggers
    ADD CONSTRAINT wfm_qrtz_blob_triggers_pkey PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: wfm_qrtz_calendars_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_calendars
    ADD CONSTRAINT wfm_qrtz_calendars_pkey PRIMARY KEY (calendar_name);


--
-- Name: wfm_qrtz_cron_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_cron_triggers
    ADD CONSTRAINT wfm_qrtz_cron_triggers_pkey PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: wfm_qrtz_fired_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_fired_triggers
    ADD CONSTRAINT wfm_qrtz_fired_triggers_pkey PRIMARY KEY (entry_id);


--
-- Name: wfm_qrtz_job_details_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_job_details
    ADD CONSTRAINT wfm_qrtz_job_details_pkey PRIMARY KEY (job_name, job_group);


--
-- Name: wfm_qrtz_job_listeners_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_job_listeners
    ADD CONSTRAINT wfm_qrtz_job_listeners_pkey PRIMARY KEY (job_name, job_group, job_listener);


--
-- Name: wfm_qrtz_locks_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_locks
    ADD CONSTRAINT wfm_qrtz_locks_pkey PRIMARY KEY (lock_name);


--
-- Name: wfm_qrtz_paused_trigger_grps_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_paused_trigger_grps
    ADD CONSTRAINT wfm_qrtz_paused_trigger_grps_pkey PRIMARY KEY (trigger_group);


--
-- Name: wfm_qrtz_scheduler_state_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_scheduler_state
    ADD CONSTRAINT wfm_qrtz_scheduler_state_pkey PRIMARY KEY (instance_name);


--
-- Name: wfm_qrtz_simple_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_simple_triggers
    ADD CONSTRAINT wfm_qrtz_simple_triggers_pkey PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: wfm_qrtz_trigger_listeners_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_trigger_listeners
    ADD CONSTRAINT wfm_qrtz_trigger_listeners_pkey PRIMARY KEY (trigger_name, trigger_group, trigger_listener);


--
-- Name: wfm_qrtz_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_qrtz_triggers
    ADD CONSTRAINT wfm_qrtz_triggers_pkey PRIMARY KEY (trigger_name, trigger_group);


--
-- Name: wfm_task_data_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_task_data
    ADD CONSTRAINT wfm_task_data_pkey PRIMARY KEY (id);


--
-- Name: wfm_task_instance_data_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_task_instance_data
    ADD CONSTRAINT wfm_task_instance_data_pkey PRIMARY KEY (task_instance_id, task_data_id);


--
-- Name: wfm_task_instance_data_task_data_id_key; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_task_instance_data
    ADD CONSTRAINT wfm_task_instance_data_task_data_id_key UNIQUE (task_data_id);


--
-- Name: wfm_task_instance_dependency_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_task_instance_dependency
    ADD CONSTRAINT wfm_task_instance_dependency_pkey PRIMARY KEY (task_instance_id, task_depends_on_id);


--
-- Name: wfm_task_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_task_instance
    ADD CONSTRAINT wfm_task_instance_pkey PRIMARY KEY (id);


--
-- Name: wfm_workflow_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_workflow_instance
    ADD CONSTRAINT wfm_workflow_instance_pkey PRIMARY KEY (id);


--
-- Name: wfm_workflow_instance_task_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_workflow_instance_task
    ADD CONSTRAINT wfm_workflow_instance_task_pkey PRIMARY KEY (workflow_instance_id);


--
-- Name: wfm_workflow_name_key; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_workflow
    ADD CONSTRAINT wfm_workflow_name_key UNIQUE (name);


--
-- Name: wfm_workflow_pkey; Type: CONSTRAINT; Schema: public; Owner: <%= db_username %>; Tablespace: 
--

ALTER TABLE ONLY wfm_workflow
    ADD CONSTRAINT wfm_workflow_pkey PRIMARY KEY (id);


--
-- Name: fk_encoding_map_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_encoding_map
    ADD CONSTRAINT fk_encoding_map_01 FOREIGN KEY (wfm_encoding_id) REFERENCES wfm_encoding(id);


--
-- Name: fk_encoding_map_02; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_encoding_map
    ADD CONSTRAINT fk_encoding_map_02 FOREIGN KEY (mp_encoding_id) REFERENCES wfm_encoding_job(id);


--
-- Name: fk_provider_encoding_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_provider_encoding
    ADD CONSTRAINT fk_provider_encoding_01 FOREIGN KEY (provider_id) REFERENCES wfm_provider_context(id);


--
-- Name: fk_provider_encoding_02; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_provider_encoding
    ADD CONSTRAINT fk_provider_encoding_02 FOREIGN KEY (encoding_id) REFERENCES wfm_encoding(id);


--
-- Name: fk_provider_notification_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_provider_notification
    ADD CONSTRAINT fk_provider_notification_01 FOREIGN KEY (provider_id) REFERENCES wfm_provider_context(id);


--
-- Name: fk_provider_notification_02; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_provider_notification
    ADD CONSTRAINT fk_provider_notification_02 FOREIGN KEY (notification_id) REFERENCES wfm_email_notification(id);


--
-- Name: fk_provider_workflow_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_provider_workflow
    ADD CONSTRAINT fk_provider_workflow_01 FOREIGN KEY (provider_id) REFERENCES wfm_provider_context(id);


--
-- Name: fk_provider_workflow_02; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_provider_workflow
    ADD CONSTRAINT fk_provider_workflow_02 FOREIGN KEY (workflow_id) REFERENCES wfm_workflow(id);


--
-- Name: fk_task_instance_data_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_task_instance_data
    ADD CONSTRAINT fk_task_instance_data_01 FOREIGN KEY (task_instance_id) REFERENCES wfm_task_instance(id);


--
-- Name: fk_task_instance_data_02; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_task_instance_data
    ADD CONSTRAINT fk_task_instance_data_02 FOREIGN KEY (task_data_id) REFERENCES wfm_task_data(id);


--
-- Name: fk_task_instance_dependency_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_task_instance_dependency
    ADD CONSTRAINT fk_task_instance_dependency_01 FOREIGN KEY (task_depends_on_id) REFERENCES wfm_task_instance(id);


--
-- Name: fk_task_instance_dependency_02; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_task_instance_dependency
    ADD CONSTRAINT fk_task_instance_dependency_02 FOREIGN KEY (task_instance_id) REFERENCES wfm_task_instance(id);


--
-- Name: fk_task_message_map_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_message_map
    ADD CONSTRAINT fk_task_message_map_01 FOREIGN KEY (task_id) REFERENCES wfm_task_instance(id);


--
-- Name: fk_workflow_instance_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_workflow_instance
    ADD CONSTRAINT fk_workflow_instance_01 FOREIGN KEY (workflow_id) REFERENCES wfm_workflow(id);


--
-- Name: fk_workflow_instance_02; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_workflow_instance
    ADD CONSTRAINT fk_workflow_instance_02 FOREIGN KEY (asset_context_id) REFERENCES wfm_asset_context(id);


--
-- Name: fk_workflow_instance_03; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_workflow_instance
    ADD CONSTRAINT fk_workflow_instance_03 FOREIGN KEY (provider_context_id) REFERENCES wfm_provider_context(id);


--
-- Name: fk_workflow_instance_task_01; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_workflow_instance_task
    ADD CONSTRAINT fk_workflow_instance_task_01 FOREIGN KEY (task_instance_id) REFERENCES wfm_workflow_instance(id);


--
-- Name: fk_workflow_instance_task_02; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_workflow_instance_task
    ADD CONSTRAINT fk_workflow_instance_task_02 FOREIGN KEY (workflow_instance_id) REFERENCES wfm_task_instance(id);


--
-- Name: wfm_qrtz_blob_triggers_trigger_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_qrtz_blob_triggers
    ADD CONSTRAINT wfm_qrtz_blob_triggers_trigger_name_fkey FOREIGN KEY (trigger_name, trigger_group) REFERENCES wfm_qrtz_triggers(trigger_name, trigger_group);


--
-- Name: wfm_qrtz_cron_triggers_trigger_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_qrtz_cron_triggers
    ADD CONSTRAINT wfm_qrtz_cron_triggers_trigger_name_fkey FOREIGN KEY (trigger_name, trigger_group) REFERENCES wfm_qrtz_triggers(trigger_name, trigger_group);


--
-- Name: wfm_qrtz_job_listeners_job_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_qrtz_job_listeners
    ADD CONSTRAINT wfm_qrtz_job_listeners_job_name_fkey FOREIGN KEY (job_name, job_group) REFERENCES wfm_qrtz_job_details(job_name, job_group);


--
-- Name: wfm_qrtz_simple_triggers_trigger_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_qrtz_simple_triggers
    ADD CONSTRAINT wfm_qrtz_simple_triggers_trigger_name_fkey FOREIGN KEY (trigger_name, trigger_group) REFERENCES wfm_qrtz_triggers(trigger_name, trigger_group);


--
-- Name: wfm_qrtz_trigger_listeners_trigger_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_qrtz_trigger_listeners
    ADD CONSTRAINT wfm_qrtz_trigger_listeners_trigger_name_fkey FOREIGN KEY (trigger_name, trigger_group) REFERENCES wfm_qrtz_triggers(trigger_name, trigger_group);


--
-- Name: wfm_qrtz_triggers_job_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: <%= db_username %>
--

ALTER TABLE ONLY wfm_qrtz_triggers
    ADD CONSTRAINT wfm_qrtz_triggers_job_name_fkey FOREIGN KEY (job_name, job_group) REFERENCES wfm_qrtz_job_details(job_name, job_group);


--
-- Name: public; Type: ACL; Schema: -; Owner: <%= db_username %>
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM <%= db_username %>;
GRANT ALL ON SCHEMA public TO <%= db_username %>;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- postgresSQL database dump complete
--



