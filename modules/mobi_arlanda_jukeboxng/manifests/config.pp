class mobi_arlanda_jukeboxng::config {

    include apache

    file { "/opt/mobi-cms-provider-ui/config/initializers/jukebox_config.rb":
      replace => true,
      owner  => "rtv",
      group  => "rtv",
      mode => "0444",
      source => "puppet:///modules/mobi_arlanda_jukeboxng/jukebox_config.rb",
      notify   => Class["apache::service"],
    }

}
