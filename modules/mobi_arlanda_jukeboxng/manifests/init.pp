# == Class: mobi_arlanda_jukeboxng
#
#  installs jukeboxng component
#
# === Parameters:
#
#    version::   version of the package
#    propfile::  properties file to use
#
# === Requires:
#
#     httpd
#
# === Sample Usage
#
#  class { "mobi_arlanda_jukeboxng" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_jukeboxng (
    ###icinga.pp
    $icinga = $mobi_arlanda_jukeboxng::params::icinga,
    $icinga_instance = $mobi_arlanda_jukeboxng::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_jukeboxng::params::icinga_cmd_args,
    ###end icinga.pp
    $php_version = $mobi_arlanda_jukeboxng::params::php_version,
    $cms_ui_version = $mobi_arlanda_jukeboxng::params::cms_ui_version,
    $ext_version = $mobi_arlanda_jukeboxng::params::ext_version,
    $apr_version = $mobi_arlanda_jukeboxng::params::apr_version,
    $httpd_version = $mobi_arlanda_jukeboxng::params::httpd_version,
    $postgresql_libs_version = $mobi_arlanda_jukeboxng::params::postgresql_libs_version,
    $gmp_version = $mobi_arlanda_jukeboxng::params::gmp_version,
    $libxslt_version = $mobi_arlanda_jukeboxng::params::libxslt_version,
    $ruby_rpm_url = $mobi_arlanda_jukeboxng::params::ruby_rpm_url,
    $ruby_version = $mobi_arlanda_jukeboxng::params::ruby_version,
    $rubygems_rpm_url = $mobi_arlanda_jukeboxng::params::rubygems_rpm_url,
    $rubygems_version = $mobi_arlanda_jukeboxng::params::rubygems_version,
    $cms_ui_qaconf_rpm_url = $mobi_arlanda_jukeboxng::params::cms_ui_qaconf_rpm_url,
    $cms_ui_qaconf_version = $mobi_arlanda_jukeboxng::params::cms_ui_qaconf_version,
    $rtv_version = $mobi_arlanda_jukeboxng::params::rtv_version,
)
inherits mobi_arlanda_jukeboxng::params {

    include mobi_arlanda_jukeboxng::install, mobi_arlanda_jukeboxng::config

    anchor { "mobi_arlanda_jukeboxng::begin": } ->
    Class["mobi_arlanda_jukeboxng::install"] ->
    Class["mobi_arlanda_jukeboxng::config"] ->
    class { "mobi_arlanda_jukeboxng::icinga":} ->
    anchor { "mobi_arlanda_jukeboxng::end": }
    os::motd::register { $name : }
}

