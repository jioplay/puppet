class mobi_client_log_manager::install {
    package { "mobi-client-log-manager-${mobi_client_log_manager::mobi_client_log_manager_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
