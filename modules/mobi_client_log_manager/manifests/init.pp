class mobi_client_log_manager(
    ###icinga.pp
    $icinga = $mobi_client_log_manager::params::icinga,
    $icinga_instance = $mobi_client_log_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_client_log_manager::params::icinga_cmd_args,
    ###end icinga.pp
   $mobi_client_log_manager_version = $mobi_config_manager::params::mobi_client_log_manager_version,
)
inherits mobi_client_log_manager::params{
  include mobi_client_log_manager::install
  anchor { "mobi_client_log_manager::begin":} -> Class["mobi_client_log_manager::install"] ->
    class { "mobi_client_log_manager::icinga":} ->
  anchor { "mobi_client_log_manager::end": }
  os::motd::register { $name : }
}
