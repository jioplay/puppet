class mobi_client_log_manager::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-client-log-manager/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    $mobi_client_log_manager_version = "*"
}
