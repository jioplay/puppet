class mobi_catchup_post_processor::config {
 file { "/opt/mobitv/conf/mobi-catchup-post-processor/catchup-post-processor.properties":
    ensure => present,
    content => template("mobi_catchup_post_processor/catchup-post-processor.properties.erb"),
    require => Class["mobi_catchup_post_processor::install"],
    notify => Class["tomcat::service"],
  }
}

