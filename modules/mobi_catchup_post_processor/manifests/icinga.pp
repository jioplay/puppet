class mobi_catchup_post_processor::icinga {

  if $::mobi_catchup_post_processor::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::mobi_catchup_post_processor::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_ril_recommendation_adapter module when icinga = true")
    }

    @@nagios_service { "check_http_mobi_catchup_post_processor_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_catchup_post_processor::icinga_cmd_args}",
      service_description => "check_http_mobi_catchup_post_processor",
      notes => "PATH: ${::mobi_catchup_post_processor::icinga_cmd_args}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_catchup_post_processor::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }

  }
}
