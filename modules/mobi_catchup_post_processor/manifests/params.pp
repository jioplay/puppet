class mobi_catchup_post_processor::params {
    $package = "mobi-catchup-post-processor"
    $version = undef

    $solr_server_url = undef
    $amq_broker_url = undef
    $core_pool_size = undef
    $max_pool_size = undef
    $task_queue_capacity = undef
    $consumer_queue_catchup_post_processor_name = undef
    $catchup_queue_concurrent_consumers = undef
    $catchup_recording_base_location = undef
    $catchup_recording_file_suffix = undef
    $atomhopper_url = undef
    $connection_timeout = undef
    $read_timeout = undef

    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-catchup-post-processor/monitoring/health -w 5 -c 10"
}

