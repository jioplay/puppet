class mobi_catchup_post_processor (
    $package = $mobi_catchup_post_processor::params::package,
    $version = $mobi_catchup_post_processor::params::version,

    $solr_server_url = $mobi_catchup_post_processor::params::solr_server_url,
    $amq_broker_url = $mobi_catchup_post_processor::params::amq_broker_url,
    $core_pool_size = $mobi_catchup_post_processor::params::core_pool_size,
    $max_pool_size = $mobi_catchup_post_processor::params::max_pool_size,
    $task_queue_capacity = $mobi_catchup_post_processor::params::task_queue_capacity,
    $consumer_queue_catchup_post_processor_name = $mobi_catchup_post_processor::params::consumer_queue_catchup_post_processor_name,
    $catchup_queue_concurrent_consumers = $mobi_catchup_post_processor::params::catchup_queue_concurrent_consumers,
    $catchup_recording_base_location = $mobi_catchup_post_processor::params::catchup_recording_base_location,
    $catchup_recording_file_suffix = $mobi_catchup_post_processor::params::catchup_recording_file_suffix,
    $atomhopper_url = $mobi_catchup_post_processor::params::atomhopper_url,
    $connection_timeout = $mobi_catchup_post_processor::params::connection_timeout,
    $read_timeout = $mobi_catchup_post_processor::params::read_timeout,

    $icinga = $mobi_catchup_post_processor::params::icinga,
    $icinga_instance = $mobi_catchup_post_processor::params::icinga_instance,
    $icinga_cmd_args = $mobi_catchup_post_processor::params::icinga_cmd_args,

) inherits mobi_catchup_post_processor::params {

    anchor { "mobi_catchup_post_processor::begin": } ->
    class { "mobi_catchup_post_processor::install": } ->
    class { "mobi_catchup_post_processor::config": } ->
    class { "mobi_catchup_post_processor::icinga":} ->
    anchor { "mobi_catchup_post_processor::end": }

    os::motd::register{"${name}":}
}
