class mobi_catchup_post_processor::install {
    package {"${$mobi_catchup_post_processor::package}-${$mobi_catchup_post_processor::version}":
      ensure => present,
      require => Class["tomcat::install"],
      notify  => Class["tomcat::service"],
    }
}
