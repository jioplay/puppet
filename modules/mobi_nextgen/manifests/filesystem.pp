class mobi_nextgen::filesystem {

    file { "/var/log/ReachTV":
      ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => 755,
    }

    file { "/var/log/ReachTV/Farm":
      ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => 755,
    }

}
