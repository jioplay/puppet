class mobi_nextgen::appserverhosts {
  # This module will configure the /etc/motd and /etc/hosts files
  # These show the owner of the machine and configure accessible hosts

  # split the IP Adress components to get the last quadrant
  #$ip_parts = split($ipaddress, '[.]')
  # add one to the IP since the convention is to have the database host use an IP one greater than the app_server
  #$ip_parts[3] = $ip_parts[3] + 1

  if ($domain == "oak1.mobitv") {
    $hosts_lh_hostnames = "localhost.localdomain localhost msfrn01p1 msfrn01p1.smf1.mobitv"
    $hosts_svn_ip = "10.172.65.113"
    $hosts_svn_hostnames = "qlr506d.oak1.mobitv configrepo.smf1.mobitv"
    $hosts_db1_ip = "10.172.65.51"
    $hosts_db1_hostnames = "vmserver09b.oak1.mobitv dbhost hostmanager"
    $modt_owner = "Srinivas R"
    $modt_stackname = "Mobi Nextgen Server"
  }

  file { "/etc/hosts":
          ensure  => present,
    owner => root,
    group => root,
    mode => 644,
    content => template("mobi-nextgen/hosts.erb"),
      }

  file { '/etc/motd':
          ensure  => present,
    owner => root,
    group => root,
    mode => 644,
          content => template('mobi-nextgen/motd.erb'),
  }
}
