class mobi_nextgen::mspub::install {

  include(mobi_nextgen::appbase)
  include(mobi_nextgen::mspub::start)

  package {"NextGen publishing framework RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_framework_ng_version,
    name => "mobi-publisher-framework-ng",
    provider => yum,
  }
  package {"NextGen frequent publishing transaction RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_frequent_ng_version,
    name => "mobi-publisher-transaction-frequent-ng",
    provider => yum,
  }
  package {"NextGen frequent config RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_frequent_config_ng_version,
    name => "mobi-publisher-transaction-frequent-config-ng",
    provider => yum,
  }
  package {"NextGen nightly publishing transaction RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_nightly_ng_version,
    name => "mobi-publisher-transaction-nightly-ng",
    provider => yum,
  }
  package {"NextGen configrepo publishing transaction RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_configrepo_version,
    name => "mobi-publisher-transaction-configrepo",
    provider => yum,
  }
  package {"NextGen icon publishing transaction RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_icon_ng_version,
    name => "mobi-publisher-transaction-icon-ng",
    provider => yum,
  }
  package {"NextGen widgets publishing transaction RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_widgets_ng_version,
    name => "mobi-publisher-transaction-widgets-ng",
    provider => yum,
  }
  package {"NextGen staticres publishing transaction RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_staticres_ng_version,
    name => "mobi-publisher-transaction-staticres-ng",
    provider => yum,
  }
  package {"NextGen tiles publishing transaction RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_tiles_ng_version,
    name => "mobi-publisher-transaction-tiles-ng",
    provider => yum,
  }
  package {"NextGen devdetect publishing transaction RPM":
    ensure => $mobi_nextgen::rpmversions::mobi_publisher_transaction_devdetect_ng_version,
    name => "mobi-publisher-transaction-devdetect-ng",
    provider => yum,
  }

  # Dev & QA environment config
  # This is not required or desirable in the Production environment.
  if ($domain == "oak1.mobitv") or ($domain == "dev.dmz") or ($domain == "qa.dmz") {
    file { "/opt/mobi-publisher/mobi-publisher-transaction-frequent/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/endpoint.host.json",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-nightly/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/endpoint.host.json",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-configrepo/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/endpoint.host.json",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-icon/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/endpoint.host.json",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-widgets/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/endpoint.host.json",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-staticres/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/endpoint.host.json",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-tiles/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/endpoint.host.json",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-devdetect/config/endpoint.host.json":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/endpoint.host.json",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-tiles/config/config.txt":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/tiles/config.txt",
    }
    file { "/opt/mobi-publisher/mobi-publisher-transaction-widgets/config/config.txt":
      ensure => "file",
      owner  => "rtv",
      group  => "rtv",
      mode   => 644,
      source => "puppet:///modules/mobi_nextgen/ng_publisher_resources/brick/widgets/config.txt",
    }



    Package["NextGen devdetect publishing transaction RPM"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-frequent/config/endpoint.host.json"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-nightly/config/endpoint.host.json"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-configrepo/config/endpoint.host.json"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-icon/config/endpoint.host.json"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-widgets/config/endpoint.host.json"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-staticres/config/endpoint.host.json"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-tiles/config/endpoint.host.json"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-devdetect/config/endpoint.host.json"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-tiles/config/config.txt"] ->
    File["/opt/mobi-publisher/mobi-publisher-transaction-widgets/config/config.txt"]
  }

  Class['mobi-nextgen::appbase'] ->
  Package["NextGen publishing framework RPM"] ->
  Package["NextGen frequent publishing transaction RPM"] ->
  Package["NextGen frequent config RPM"] ->
  Package["NextGen nightly publishing transaction RPM"] ->
  Package["NextGen configrepo publishing transaction RPM"] ->
  Package["NextGen icon publishing transaction RPM"] ->
  Package["NextGen widgets publishing transaction RPM"] ->
  Package["NextGen staticres publishing transaction RPM"] ->
  Package["NextGen tiles publishing transaction RPM"] ->
  Package["NextGen devdetect publishing transaction RPM"] ->
  Class[mobi-nextgen::mspub::start]

}
