class mobi_nextgen::rpmversions{
  #Listed in order should be installed

  #Java
  $jdk_version = "1.6.0_06-fcs"
  $mobi_jce_version = "6.0.0-96669"

  #msfrn
  $mod_python_version = "3.2.8-3.1"
  $httpd_version = "2.2.3-53.el5.centos.3"
  $mobi_publisher_endpoint_version = "4.6.80-142454"

  #mspub
  $mobi_publisher_framework_ng_version = "5.0.0-180367"
  $mobi_publisher_transaction_frequent_ng_version = "5.0.0-180374"
  $mobi_publisher_transaction_frequent_config_ng_version = "4.7.20-158826"
  $mobi_publisher_transaction_nightly_ng_version = "5.0.0-180374"
  $mobi_publisher_transaction_configrepo_version = "4.7.40-158089"
  $mobi_publisher_transaction_icon_ng_version = "4.7.40-158089"
  $mobi_publisher_transaction_widgets_ng_version = "4.7.40-159072"
  $mobi_publisher_transaction_staticres_ng_version = "4.7.40-158119"
  $mobi_publisher_transaction_tiles_ng_version = "4.7.40-161293"
  $mobi_publisher_transaction_devdetect_ng_version = "4.7.40-158119"

  }

