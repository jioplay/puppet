class mobi_nextgen::rtv {

    require(group::rtv)
    require("user::rtv")

    exec { 'Install RTV User RPM':
      command => "/bin/rpm -Uvh http://build.corp/gold_masters/mobi/mobi-user-rtv-1.2.3-79211.i686.rpm",
      onlyif => "/usr/bin/test `/bin/rpm -qa mobi-user-rtv | wc -l` -eq 0",
    }
  }
