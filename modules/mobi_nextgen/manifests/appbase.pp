class mobi_nextgen::appbase {
  include(user::apache)
  include(mobi_nextgen::rtv)
  include(os::git-branch-commit)
  include(mobi_nextgen::create_yum_repo)
  include(mobi_nextgen::appserverhosts)
  include(mobi_nextgen::filesystem)
  include(mobi_nextgen::rpmversions)
  include(mobi_nextgen::java)

  case $domain
  {
    "smf1.mobitv": { class {"syslog-ng": env_type => "production"} }
    default: { class {"syslog-ng": env_type => "test"} }
  }

  notify { "AppBaseStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last
  notify { "AppBaseFinished": } # Used to make sure anything with a dependency on this class will run all the include resources first
  Notify['AppBaseStarted'] ->
  Class['user::apache'] ->
  Class['mobi-nextgen::rtv'] ->
  Class["os::git-branch-commit"] ->
  Class['mobi-nextgen::create_yum_repo'] ->
  Class['mobi-nextgen::appserverhosts'] ->
  Class['mobi-nextgen::filesystem'] ->
  Class['mobi-nextgen::rpmversions'] ->
  Class['mobi-nextgen::java'] ->
  Notify['AppBaseFinished']
}
