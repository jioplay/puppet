class mobi_nextgen::create_yum_repo
  {
    include yum::repo::clean
    include yum::repo::centos
    include yum::repo::goldmaster
    include yum::repo::untested

    notify { "YumStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

    notify { "YumFinished": } # Used to make sure anything with a dependency on this class will run all the include resources first

    Notify['YumStarted'] ->
    Class["yum::repo::clean"] ->
    Class["yum::repo::centos"] ->
    Class["yum::repo::goldmaster"] ->
    Class["yum::repo::untested"] ->
    Notify['YumFinished']

    if ( (($domain == "oak1.mobitv") or ($domain == "dev.dmz")) ) {
      include yum::repo::unstable
      Notify['YumStarted'] ->
      Class["yum::repo::untested"] ->
      Class["yum::repo::unstable"] ->
      Notify['YumFinished']
    }
}
