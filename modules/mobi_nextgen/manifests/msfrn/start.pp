class mobi_nextgen::msfrn::start {

    service { "httpd":
      enable => true,
      ensure => running,
    }

    notify { "MSFRNStarted": } # Used to make sure anything with a dependency on this class will run all the include resources last

    Notify["MSFRNStarted"] ->
    Service["httpd"]
  }
