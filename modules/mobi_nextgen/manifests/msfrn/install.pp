class mobi_nextgen::msfrn::install {
    include(mobi_nextgen::appbase)
    include(mobi_nextgen::msfrn::start)

    # We want to install httpd-2.2.3-53.el5.centos.3 from centos-5-x86_64-updates (ie http://172.16.253.200/cobbler/repo_mirror/centos-5-x86_64-updates/RPMS)
    # However, we have had problems with getting that in addition to httpd-2.2.3-31.el5 from thirdparty_untested
    package { "Python Mod": # Needed to start httpd, see /etc/httpd/conf.d/mobi-restapi-conv-v1-live-thumnail.conf
      ensure => $mobi_nextgen::rpmversions::mod_python_version,
      name => "mod_python",
      provider => yum,
      notify   => Service["httpd"],
    }

    package { "Apache Httpd":
      ensure => $mobi_nextgen::rpmversions::httpd_version,
      name => "httpd",
      provider => yum,
      notify   => Service["httpd"],
    }

    package { "Mobi Publisher Endpoint":
      ensure => $mobi_nextgen::rpmversions::mobi_publisher_endpoint_version,
      name => "mobi-publisher-endpoint",
      provider => yum,
      notify   => Service["httpd"],
    }

    Class['mobi-nextgen::appbase'] ->
    Package["Python Mod"] ->
    Package["Apache Httpd"] ->
    Package["Mobi Publisher Endpoint"] ->
    Class[mobi-nextgen::msfrn::start]
  }
