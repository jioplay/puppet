class mobi_nextgen::java {

  exec { "Remove GCJ":
    command => "/bin/rpm -e --nodeps gcj-java",
    onlyif => "/usr/bin/test `/bin/rpm -qa gcj-java | wc -l` -eq 1",
  }

  exec { "Remove GCJ Compat":
    command => "/bin/rpm -e --nodeps java-1.4.2-gcj-compat",
    onlyif => "/usr/bin/test `/bin/rpm -qa java-1.4.2-gcj-compat | wc -l` -eq 1",
  }

  package {"JDK":
    ensure => $mobi_nextgen::rpmversions::jdk_version,
    name => "jdk",
    provider => yum,
  }

  # For some reason can't use package "JCE" here, gives some type of conflict with jdk, must use rpm force/replace packages
  exec { "Install JCE":
    command => "/bin/rpm --force --replacepkgs -ivh http://build.corp/untested/mobi/mobi-jce-${mobi_nextgen::rpmversions::mobi_jce_version}.i686.rpm",
    onlyif => "/usr/bin/test `/bin/rpm -qa mobi-jce | grep mobi-jce-${mobi_nextgen::rpmversions::mobi_jce_version} | wc -l` -eq 0",
  }

  Exec["Remove GCJ"] ->
  Exec["Remove GCJ Compat"] ->
  Package["JDK"] ->
  Exec["Install JCE"] # Requires correct JDK to be installed
}
