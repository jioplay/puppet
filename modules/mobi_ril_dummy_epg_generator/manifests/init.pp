## == Class: mobi_ril_dummy_epg_generator
#
# Manage the mobi_ril_dummy_epg_generator component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_ril_dummy_epg_generator (
    $package_name = $mobi_ril_dummy_epg_generator::params::package_name,
    $package_ensure = $mobi_ril_dummy_epg_generator::params::package_ensure,
    $icinga = $mobi_ril_dummy_epg_generator::params::icinga,
    $icinga_instance = $mobi_ril_dummy_epg_generator::params::icinga_instance,
    $icinga_cmd_args = $mobi_ril_dummy_epg_generator::params::icinga_cmd_args,
    $properties_context = $mobi_ril_dummy_epg_generator::params::properties_context,
    $solr_server_url = $mobi_ril_dummy_epg_generator::params::solr_server_url,
    $solr_socket_timeout = $mobi_ril_dummy_epg_generator::params::solr_socket_timeout,
    $solr_conn_timeout = $mobi_ril_dummy_epg_generator::params::solr_conn_timeout,
    $solr_conn_per_host =$mobi_ril_dummy_epg_generator::params::solr_conn_per_host,
    $solr_total_conn = $mobi_ril_dummy_epg_generator::params::solr_total_conn,
    $solr_max_retries = $mobi_ril_dummy_epg_generator::params::solr_max_retries,
    $solr_max_rows = $mobi_ril_dummy_epg_generator::params::solr_max_rows,
    $solr_read_timeout = $mobi_ril_dummy_epg_generator::params::solr_read_timeout,
    $dummy_epg_generator_job_enabled = $mobi_ril_dummy_epg_generator::params::dummy_epg_generator_job_enabled,
    $dummy_epg_generator_cron_expression = $mobi_ril_dummy_epg_generator::params::dummy_epg_generator_cron_expression,
    $dummy_epg_generator_channel_vid = $mobi_ril_dummy_epg_generator::params::dummy_epg_generator_channel_vid,
    $epg_data_shared_location =$mobi_ril_dummy_epg_generator::params::epg_data_shared_location,
    $channel_epg_duration_file = $mobi_ril_dummy_epg_generator::params::channel_epg_duration_file,
    $channel_epg_default_duration = $mobi_ril_dummy_epg_generator::params::channel_epg_default_duration,
    $channel_ids_program_name = $mobi_ril_dummy_epg_generator::params::channel_ids_program_name,
    $external_channel_id_extension = $mobi_ril_dummy_epg_generator::params::external_channel_id_extension,
    $default_number_of_days = $mobi_ril_dummy_epg_generator::params::default_number_of_days,
    $dummy_program_description = $mobi_ril_dummy_epg_generator::params::dummy_program_description,

) inherits mobi_ril_dummy_epg_generator::params {

    anchor { "mobi_ril_dummy_epg_generator::begin": } ->
    class { "mobi_ril_dummy_epg_generator::install": } ->
    class { "mobi_ril_dummy_epg_generator::config": } ->
    class { "mobi_ril_dummy_epg_generator::icinga": } ->
    anchor { "mobi_ril_dummy_epg_generator::end": }

    os::motd::register{ "${name}":}
}
