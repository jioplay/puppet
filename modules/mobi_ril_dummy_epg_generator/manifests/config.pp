class mobi_ril_dummy_epg_generator::config {
    file { "/opt/${mobi_ril_dummy_epg_generator::package_name}/conf/application-config-override.xml":
        ensure  => present,
        notify  => Class["tomcat::service"],
        require => Class["mobi_ril_dummy_epg_generator::install"],
        content => template("mobi_ril_dummy_epg_generator/application-config-override.xml.erb"),
    }
}
