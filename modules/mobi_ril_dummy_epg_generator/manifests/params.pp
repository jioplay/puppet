class mobi_ril_dummy_epg_generator::params {
    $package_name = "mobi-ril-dummy-epg-generator"
    $package_ensure = undef

    ###icinga.pp

    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-ril-dummy-epg-generator/monitoring/health -w 5 -c 10"
    $properties_context = "default"
    $solr_server_url = "http://solrmastervip:8080/mobi-solr/"
    $solr_socket_timeout = "1500"
    $solr_conn_timeout = "1500"
    $solr_conn_per_host = "150"
    $solr_total_conn = "150"
    $solr_max_retries = "1"
    $solr_max_rows = "1000"
    $solr_read_timeout = "5000"
    $dummy_epg_generator_job_enabled = true
    $dummy_epg_generator_cron_expression = "0 0/10 * * * ?"
    $dummy_epg_generator_channel_vid = "infotel-r4g-5.0-rest"
    $epg_data_shared_location="/var/Jukebox/ftpupload/whatsonindia/"
    $channel_epg_duration_file = "/opt/mobi-ril-dummy-epg-generator/conf/channel-epg-duration-file.properties"
    $channel_epg_default_duration= "60"
    $channel_ids_program_name=""
    $external_channel_id_extension="DUM"
    $default_number_of_days="8"
    $dummy_program_description="Dummy Program"
}
