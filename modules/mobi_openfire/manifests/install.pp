class mobi_openfire::install {
    if $::mobi_openfire::mobi_openfire_package_ensure !~ /^(absent|present|latest|ensure)$/ {
        fail("$name expects \$mobi_openfire_package_ensure to be one of: absent, present, latest, or ensure.\nPlease add version to version param instead.")
    }
    package { "${::mobi_openfire::mobi_openfire_package}":
        ensure => $::mobi_openfire::mobi_openfire_package_ensure,
        /*notify => Class["mobi_openfire::service"],*/
    }
}
