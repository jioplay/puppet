class mobi_openfire::params {
    ## icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 9090 -u /login.jsp -w 5 -c 10"

    $mobi_openfire_package = "mobi-openfire"
    $mobi_openfire_package_ensure = present
}
