class mobi_openfire::service {
    service { "openfire":
        ensure     => running,
        enable     => true,
        hasrestart => true,
        hasstatus  => true,
        require    => Class["mobi_openfire::install"],
    }
}
