# == Class: mobi_openfire
#
#  Installs openfire, makes config changes in openfire.script, starts openfire
#
# === Parameters:
#
#    mobi_openfire_package::   package name  of openfire component
#    mobi_openfire_package_ensure:: present or latest
#    xmpp_domain_name::   domain name for this openfire server
#    admin_email:: email of the admin user
#    memcached_servers:: list of memcached servers
#
# === Requires:
#
#    Java
#
# === Sample Usage
#
#       class { 'mobi_openfire':
#           mobi_openfire_package => 'mobi-openfire-3.8.0-249751.SNAPSHOT',
#           mobi_openfire_package_ensure => 'present'
#           xmpp_domain_name => 'xmpp-conv-oak1-03.mobitv.com',
#           admin_email => 'sbommareddy@mobitv.com',
#           memcached_servers => 'msliv01p1:11211,msliv02p1:11211',
#       }
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_openfire(
    ## icinga.pp
    $icinga                       = $mobi_openfire::params::icinga,
    $icinga_instance              = $mobi_openfire::params::icinga_instance,
    $icinga_cmd_args              = $mobi_openfire::params::icinga_cmd_args,

    $mobi_openfire_package        = $mobi_openfire::params::mobi_openfire_package,
    $mobi_openfire_package_ensure = $mobi_openfire::params::mobi_openfire_package_ensure,
    $xmpp_domain_name,
    $admin_email,
    $memcached_servers,
)

inherits mobi_openfire::params{

    anchor { "mobi_openfire::begin": } ->
    class  { "mobi_openfire::install": } ->
    class  { "mobi_openfire::config": } ->
    class { "mobi_openfire::icinga":} ->
    anchor { "mobi_openfire::end": }

    class { "mobi_openfire::service": }

    os::motd::register { $name: }
}
