class mobi_openfire::config {
    file { "/opt/openfire/embedded-db/openfire.script":
        ensure   => present,
        path     => "/opt/openfire/embedded-db/openfire.script",
        owner    => "openfire",
        group    => "openfire",
        content  => template("mobi_openfire/openfire.script.erb"),
        #notify => Class["mobi_openfire::service"],
    }
}
