# == Class: mobi_arlanda_fragfileprocessor
#
#  installs fragfileprocessor component
#
# === Parameters:
#
#    $version::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_arlanda_fragfileprocessor" :
#           version => "5.0.0-178058",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_fragfileprocessor (
    ###icinga.pp
    $icinga = $mobi_arlanda_fragfileprocessor::params::icinga,
    $icinga_instance = $mobi_arlanda_fragfileprocessor::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_fragfileprocessor::params::icinga_cmd_args,
    ###end icinga.pp
    $version = $mobi_arlanda_fragfileprocessor::params::version,
    $amq_broker_url = $mobi_arlanda_fragfileprocessor::params::amq_broker_url,
    $drm1_enabled = $mobi_arlanda_fragfileprocessor::params::drm1_enabled,
    $drm2_enabled = $mobi_arlanda_fragfileprocessor::params::drm2_enabled,
    $encryption_type = $mobi_arlanda_fragfileprocessor::params::encryption_type,
    $drm_real_packages_enabled = $mobi_arlanda_fragfileprocessor::params::drm_real_packages_enabled,
    $thread_pool_core_size = $mobi_arlanda_fragfileprocessor::params::thread_pool_core_size,
    $thread_pool_max_size = $mobi_arlanda_fragfileprocessor::params::thread_pool_max_size,
    $lm2_url = $mobi_arlanda_fragfileprocessor::params::lm2_url,
    $lm1_url = $mobi_arlanda_fragfileprocessor::params::lm1_url,
    $dam_url = $mobi_arlanda_fragfileprocessor::params::dam_url,
    $offer_management_url = $mobi_arlanda_fragfileprocessor::params::offer_management_url,
    $temp_directory = $mobi_arlanda_fragfileprocessor::params::temp_directory,
    $folder_manager_rest_url = $mobi_arlanda_fragfileprocessor::params::folder_manager_rest_url,
    $fmp4_input_encodings = $mobi_arlanda_fragfileprocessor::params::fmp4_input_encodings,
    $encodings_file = $mobi_arlanda_fragfileprocessor::params::encodings_file,
    $jms_fragmentation_queue = $mobi_arlanda_fragfileprocessor::params::jms_fragmentation_queue,
    $jms_media2go_queue = $mobi_arlanda_fragfileprocessor::params::jms_media2go_queue,
    $offer_management_timeout = $mobi_arlanda_fragfileprocessor::params::offer_management_timeout,
    $license_file_encoding_id = $mobi_arlanda_fragfileprocessor::params::license_file_encoding_id,
    $segmenter_timeout = $mobi_arlanda_fragfileprocessor::params::segmenter_timeout,
    $hls_vod_base_url = $mobi_arlanda_fragfileprocessor::params::hls_vod_base_url,
    $generate_fragments_command = $mobi_arlanda_fragfileprocessor::params::generate_fragments_command,
    $generate_fragments_with_closecaptioningsource_command = $mobi_arlanda_fragfileprocessor::params::generate_fragments_with_closecaptioningsource_command,
    $generate_fragments_encrypted_command = $mobi_arlanda_fragfileprocessor::params::generate_fragments_encrypted_command,
    $generate_fragments_encrypted_with_closecaptioningsource_command = $mobi_arlanda_fragfileprocessor::params::generate_fragments_encrypted_with_closecaptioningsource_command,
    $segment_executor_command = $mobi_arlanda_fragfileprocessor::params::segment_executor_command,
    $segment_executor_with_closecaptioningsource_command = $mobi_arlanda_fragfileprocessor::params::segment_executor_with_closecaptioningsource_command,
    $segment_executor_encrypted_command = $mobi_arlanda_fragfileprocessor::params::segment_executor_encrypted_command,
    $segment_executor_encrypted_with_closecaptioningsource_command = $mobi_arlanda_fragfileprocessor::params::segment_executor_encrypted_with_closecaptioningsource_command,
    $m2g_encryptor_command = $mobi_arlanda_fragfileprocessor::params::m2g_encryptor_command,
)

inherits mobi_arlanda_fragfileprocessor::params {
    include mobi_arlanda_fragfileprocessor::install, mobi_arlanda_fragfileprocessor::config
    anchor { "mobi_arlanda_fragfileprocessor::begin": } ->
    Class["mobi_arlanda_fragfileprocessor::install"] ->
    Class["mobi_arlanda_fragfileprocessor::config"] ->
    class { "mobi_arlanda_fragfileprocessor::icinga":} ->
    anchor { "mobi_arlanda_fragfileprocessor::end" :} -> os::motd::register { $name : }
}

