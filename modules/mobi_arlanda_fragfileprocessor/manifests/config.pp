class mobi_arlanda_fragfileprocessor::config {

    file { "/opt/arlanda/conf/fragfileprocessor/fragfileprocessor.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_fragfileprocessor/fragfileprocessor.properties.erb'),
        notify   => Class["tomcat::service"],
    }

    file { "/opt/arlanda/conf/fragfileprocessor/encodings.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_arlanda_fragfileprocessor/${mobi_arlanda_fragfileprocessor::encodings_file}",
        notify   => Class["tomcat::service"],
    }
}
