class mobi_arlanda_fragfileprocessor::install {

  package { "mobi-arlanda-fragfileprocessor":
    ensure => "${mobi_arlanda_fragfileprocessor::version}",
    notify => Class["tomcat::service"],
  }

}
