class mobi_ocspresponder::install {
  if !$mobi_ocspresponder::drm2_1 {
    package { "$mobi_ocspresponder::package":
      provider => yum,
      ensure   => "$mobi_ocspresponder::version",
    }
  } else {
    package { "$mobi_ocspresponder::package2_1":
      provider => yum,
      ensure   => "$mobi_ocspresponder::version",
    }
  }
}
