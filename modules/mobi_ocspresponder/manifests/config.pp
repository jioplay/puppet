class mobi_ocspresponder::config {

    File {
        owner => rtv,
        group => rtv,
        mode => "0644",
    }
    $drm2_1 = $mobi_ocspresponder::drm2_1
    if ! $drm2_1 {
        #set to local variables for the template and check for required
        $cert_root = $mobi_ocspresponder::cert_root
        if ! $mobi_ocspresponder::cert_root  {
            fail("You must configure the cert root")
            }
        $output_root = $mobi_ocspresponder::output_root
        $use_revocation = $mobi_ocspresponder::use_revocation

        file { "/etc/httpd/conf.d/mod_ocspresponder.conf":
            ensure => present,
            content => template("mobi_ocspresponder/mod_ocspresponder.conf.erb"),
            notify => Class["apache::service"],
            require => Class["mobi_ocspresponder::install"],
            }
    } else {
        $licensemanager_keystore_dir = $mobi_ocspresponder::licensemanager_keystore_dir
        if ! $mobi_ocspresponder::licensemanager_keystore_dir {
            fail("You must configure the licensemanager keystore directory")
            }
        $jdbc_database_type = $mobi_ocspresponder::jdbc_database_type
        $use_revocation2 = $mobi_ocspresponder::use_revocation2
        file { "/opt/drm/conf/ocspresponder.properties":
            ensure => present,
            content => template("mobi_ocspresponder/ocspresponder.properties.erb"),
            require => Class["mobi_ocspresponder::install"],
            }
        $lm_user = $mobi_ocspresponder::lm_user
        $lm_user_pass = $mobi_ocspresponder::lm_user_pass
        $jdbc_driver = $mobi_ocspresponder::jdbc_driver
        $max_active = $mobi_ocspresponder::max_active
        $max_idle = $mobi_ocspresponder::max_idle
        $max_wait = $mobi_ocspresponder::max_wait
        $validation_query = $mobi_ocspresponder::validation_query
        $test_on_borrow = $mobi_ocspresponder::test_on_borrow
        $pool_prepared_statements = $mobi_ocspresponder::pool_prepared_statements
        $max_open_prepared_statements = $mobi_ocspresponder::max_open_prepared_statements
        $db_url = $mobi_ocspresponder::db_url
        file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/ocspresponder.xml":
            ensure => present,
            content => template("mobi_ocspresponder/ocspresponder.xml.erb"),
            require => Class["mobi_ocspresponder::install"],
            }
     }
 }
