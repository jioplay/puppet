class mobi_ocspresponder::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /ocspresponder/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    # install.pp variables
    $package = "mobi-ocspresponder"
    $version = ""
    $package2_1 = "mobi-server-drm-ocspresponder"

    #toggle-switch between the implementations (D: apache module implementation)
    $drm2_1 = false

    # config.pp variables for the apache module implementation
    $cert_root=undef
    $output_root=undef
    $use_revoction="Off"
    # config.pp variables for the java implementation
    $jdbc_database_type="POSTGRESQL"
    $licensemanager_keystore_dir="/opt/drm/conf/keystore"
    $use_revoction2=true
    $lm_user="ocsp_user"
    $lm_user_pass="ocsp_user"
    $jdbc_driver="org.postgresql.Driver"
    $max_active="300"
    $max_idle="20"
    $max_wait="-1"
    $validation_query="select 1"
    $test_on_borrow=true
    $pool_prepared_statements="30"
    $max_open_prepared_statements="30"
    $db_url="jdbc:postgresql://10.178.220.50:5432/ocspresponder"
}
