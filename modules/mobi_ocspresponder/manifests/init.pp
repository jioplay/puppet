# == Class: mobi_ocspresponder
#
#  This module either installs the apache module mobi-ocspresponder or the java implementation
#  pending on the paramter drm2_1: true for java-version, false for apache module.
#  installs mobi_oscpresponder
#
# === Parameters:
#
#    $package::        The mobi_ocspresponder package name (mobi-ocspresponder || mobi-server-drm-ocspresponder)
#
#    $version::        The mobi_ocspresponder package version (D: N/A)
#
#    Apache module (mobi-ocspresponder) parameters:
#
#    $cert_root::      The full path to the root where to find the necessary certificates
#                      and the private key used to sign OCSP responses
#
#    $output_root::    The full path where to store incoming OCSP Requests and corresponding
#                      Responses. This is useful for debugging. If not set no output files
#                      will be written.
#
#    $use_revocation:: The OCSP Responder makes use of MySQL in order to store the list of revoked
#                      certificates. The database must be set up prior to starting the application.
#
#    java version (mobi-server-drm-ocspresponder) parameters:
#
#    $licensemanager_keystore_dir::      The full path to the root where to find the necessary certificates
#                                        and the private key used to sign OCSP responses (D: /opt/drm/conf/keystore)
#
#    $use_revocation2:: The OCSP Responder makes use of MySQL in order to store the list of revoked
#                      certificates. The database must be set up prior to starting the application. (D: true)
#
#    $lm_user::         Licensemanager user name. (D: ocsp_user)
#
#    $lm_user_pass::    Licensemanager user password. (D: ocsp_user)
#
#    $jdbc_driver::     Database driver (D: org.postgresql.Driver)
#
#    $max_active::      (D:300)
#
#    $max_idle::        (D:20)
#
#    $max_wait::        (D: -1)
#
#    $validation_query::  (D: select 1)
#
#    $test_on_borrow::  (D: true)
#
#    $pool_prepared_statements (D: true)
#
#    $max_open_prepared_statements  (D: 30)
#
#    $db_url:: (D: jdbc:postgresql://10.178.220.50:5432/ocspresponder)
#
#
#
# === Requires
#
#    Either Apache Web Server or mobi-tomcat-config-8080 and jdk >= 1.6.0
#
# === Sample Usage
#
#  class { "mobi_ocspresponder" :
#      drm2_1 => false,
#      cert_root => "/opt/drm/conf/keystore/ca",
#      output_root => "/usr/local/drm/output",
#      use_revocation => "On",
#      require => Class["apache"]
#  }
#
#  class { "mobi_ocspresponder" :
#      drm2_1 => true,
#      cert_root_dir => "/opt/drm/conf/keystore",
#      use_revocation2 => true,
#      lm_url => "http://10.178.220.50:8080/licensemanager2",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_ocspresponder (
    ###icinga.pp
    $icinga = $mobi_ocspresponder::params::icinga,
    $icinga_instance = $mobi_ocspresponder::params::icinga_instance,
    $icinga_cmd_args = $mobi_ocspresponder::params::icinga_cmd_args,
    ###end icinga.pp
    $package = $mobi_ocspresponder::params::package,
    $version = $mobi_ocspresponder::params::version,
    $drm2_1 = $mobi_ocspresponder::params::drm2_1,
    #Apache module:
    $cert_root = $mobi_ocspresponder::params::cert_root,
    $output_root = $mobi_ocspresponder::params::output_root,
    $use_revocation = $mobi_ocspresponder::params::use_revocation,
    #Java implementation:
    $jdbc_database_type = $mobi_ocspresponder::params::jdbc_database_type,
    $licensemanager_keystore_dir = $mobi_ocspresponder::params::licensemanager_keystore_dir,
    $use_revocation2 = $mobi_ocspresponder::params::use_revocation2,
    $lm_user = $mobi_ocspresponder::params::lm_user,
    $lm_user_pass = $mobi_ocspresponder::params::lm_user_pass,
    $jdbc_driver = $mobi_ocspresponder::params::jdbc_driver,
    $max_active = $mobi_ocspresponder::params::max_active,
    $max_idle = $mobi_ocspresponder::params::max_ilde,
    $max_wait = $mobi_ocspresponder::params::max_wait,
    $validation_query = $mobi_ocspresponder::params::validation_query,
    $test_on_borrow = $mobi_ocspresponder::params::test_on_borrow,
    $pool_prepared_statements = $mobi_ocspresponder::params::pool_prepared_statements,
    $max_open_prepared_statements = $mobi_ocspresponder::params::max_open_prepared_statements,
    $db_url = $mobi_ocspresponder::params::db_url,
)
inherits mobi_ocspresponder::params {

    include mobi_ocspresponder::install, mobi_ocspresponder::config
    anchor { "mobi_ocspresponder::begin": } -> Class["mobi_ocspresponder::install"]
    class { "mobi_ocspresponder::icinga":} -> 
    Class["mobi_ocspresponder::config"] -> anchor { "mobi_ocspresponder::end": }

    os::motd::register { $name : }
}
