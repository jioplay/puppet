# == Class: mobi_limelight_urlencoder
#
#  installs
#
# === Parameters:
#
#    version::   version of the package
#
# === Requires:
#
#     tomcat
#
# === Sample Usage
#
#  class { 'mobi_limelight_urlencoder':
#           version => '1.0.0-204828'
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_limelight_urlencoder (
    $version = $mobi_limelight_urlencoder::params::version,
)
inherits mobi_limelight_urlencoder::params {

    include mobi_limelight_urlencoder::install
    anchor { "mobi_limelight_urlencoder::begin": } ->
    Class["mobi_limelight_urlencoder::install"] ->
    anchor { "mobi_limelight_urlencoder::end": }

    os::motd::register{ $name :}
}
