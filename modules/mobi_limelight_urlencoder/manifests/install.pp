class mobi_limelight_urlencoder::install {

    package { "LimelightURLEncoder":
      ensure => $mobi_limelight_urlencoder::version,
      name => "mobi-limelight-urlencoder",
      provider => yum,
      notify => Class["tomcat::service"],
    }

}
