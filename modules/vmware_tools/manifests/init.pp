# Puppet module to manage vmware nodes
# https://github.com/vchoi/vmware
#
# Author: Vitor Choi Feitosa <vchoi@vchoi.org>
# Based on Eric Plaster's work posted on
# http://projects.puppetlabs.com/projects/1/wiki/VMWare_Tools.
#
class vmware_tools {

  include packages
  realize(Package["gcc"])
  realize(Package["kernel-devel"])

  # include module name in motd
  os::motd::register { $name : }

  $vmtoolstgz = 'VMwareTools-8.3.7-341836.tar.gz'

  # don't use a directory that gets wiped after every reboot!
  $workdir = '/usr/local/src/puppet-vmwaretools'

  # passed to vmware_tools_install
  $install_prefix = '/usr/local'

  # remove open_vm_tools
  $openvmtools = ["open-vm-source", "open-vm-tools"]
  $openvmtools_desired_state = $operatingsystem ? {
    ubuntu => purged,
    debian => purged,
    centos => absent,
    redhat => absent,
  }

  package { $openvmtools:
    ensure => $openvmtools_desired_state,
  }

  # Copy files to workdir
  file { $workdir:
    ensure => "directory",
    owner => "root",
    mode => "700",
  }

  file { "$workdir/$vmtoolstgz":
    owner => root,
    group => root,
    mode => 660,
    source => "puppet:///files/$vmtoolstgz",
    notify => Exec['unpack vmwaretools']
  }

  exec { "unpack vmwaretools":
    creates => "$workdir/vmware-tools-distrib",
    cwd => $workdir,
    environment => ["PAGER=/bin/cat","DISPLAY=:9"],
    command => "/bin/tar xzf $vmtoolstgz",
    logoutput => true,
    timeout => 120,
    require => [ File["$workdir/$vmtoolstgz"], Package["kernel-devel"] ],
    notify => Exec['install vmwaretools']
  }

  exec { "install vmwaretools":
    creates  => "/etc/init.d/vmware-tools",
    environment => ["PAGER=/bin/cat","DISPLAY=:9"],
    cwd      => "$workdir/vmware-tools-distrib",
    command  => "$workdir/vmware-tools-distrib/vmware-install.pl -d --prefix=$install_prefix",
    logoutput => true,
    timeout  => 300,
    require  => [ Exec["unpack vmwaretools"], Package["kernel-devel"], Package["gcc"] ],
  }

  exec { "reconfigure vmwaretools":
    creates => "/lib/modules/$kernelrelease/misc/vsock.o",
    onlyif => "/usr/bin/test -x $install_prefix/bin/vmware-config-tools.pl",
    environment => ["PAGER=/bin/cat","DISPLAY=:9"],
    cwd      => "$workdir/vmware-tools-distrib",
    command  => "$install_prefix/bin/vmware-config-tools.pl -d",
    logoutput => false,
    timeout  => 300,
  }

  service { "vmware-tools":
    ensure => running,
    enable => true,
    hasstatus => true,
    require => [ Exec["unpack vmwaretools"], Exec["reconfigure vmwaretools"] ],
  }
}
