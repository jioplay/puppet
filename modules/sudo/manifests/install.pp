class sudo::install {
    package { "sudo" :
        ensure => $sudo::ensure,
    }
}
