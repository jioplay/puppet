class sudo::config {

    $users = $sudo::users
    $groups = $sudo::groups

    file { '/etc/sudoers':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0440',
        content => template("sudo/sudoers.erb"),
        require => Package["sudo"],
    }

    file { "/etc/sudoers.d":
        ensure => directory,
        owner  => "root",
        group  => "root",
        mode   => "0755",
    }
}
