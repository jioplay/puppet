# = Class: sudo
#
# This class installs the snmpd package and an override configuration.  It configures
# the snmpd service to start_up by default.
#
# == Parameters:
#
# $users::          what users should have full sudo access
#
# $groups::         what groups should have full sudo access
#
# $ensure::         the version of the package desired (D: present)
#
# == Actions:
#     Installs the sudo package and allows us to add users and groups
#
# == Requires:
#
# == Sample Usage
#       class { "sudo":
#               $users => ['alex','bob'],
#             }
#
# Remember: No empty lines between comments and class definition
#
class sudo (
    $users = $sudo::params::users,
    $groups = $sudo::params::groups,
    $ensure = $sudo::params::ensure,
) inherits sudo::params {
    anchor { "sudo::begin": } ->
    class { "sudo::install": } ->
    class { "sudo::config": } ->
    anchor { "sudo::end": }
    # include module name in motd
    os::motd::register { "${name}": }
}
