define sudo::group_add () {
  file { "/etc/sudoers.d/${name}":
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => 0440,
    content => "%${name} ALL=(ALL) ALL",
  }
}
