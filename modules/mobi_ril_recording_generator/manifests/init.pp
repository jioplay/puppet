## == Class: mobi_ril_recording_generator
#
# Manage the mobi_ril_recording_generator component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_ril_recording_generator (
  $package_name = $mobi_ril_recording_generator::params::package_name,
  $package_ensure = $mobi_ril_recording_generator::params::package_ensure,
  $icinga = $mobi_ril_recording_generator::params::icinga,
  $icinga_cmd_args = $mobi_ril_recording_generator::params::icinga_cmd_args,
  $icinga_instance = $mobi_ril_recording_generator::params::icinga_instance,
  $recording_generator_job_enabled = $mobi_ril_recording_generator::params::recording_generator_job_enabled,
  $recording_channel_ids = $mobi_ril_recording_generator::params::recording_channel_ids,

) inherits mobi_ril_recording_generator::params {

    anchor { "mobi_ril_recording_generator::begin": } ->
    class { "mobi_ril_recording_generator::install": } ->
    class { "mobi_ril_recording_generator::config": } ->
    class { "mobi_ril_recording_generator::icinga": } ->
    anchor { "mobi_ril_recording_generator::end": }

    os::motd::register{ "${name}":}
}
