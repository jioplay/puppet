class mobi_ril_recording_generator::config {

  file { "/opt/mobi-ril-recording-generator/config/application-config-override.xml":
        owner => "rtv",
        group => "rtv",
        mode  => "0644",
        replace => true,
        content => template("mobi_ril_recording_generator/application-config-override.xml.erb"),
        require => Class["mobi_ril_recording_generator::install"],
        notify  => Class["tomcat::service"],
  }

}
