class mobi_ril_recording_generator::params {
    $package_name = "mobi-ril-recording-generator"
    $package_ensure = undef

    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-ril-recording-generator/monitoring/health -w 5 -c 10"

    $recording_generator_job_enabled = false
    $recording_channel_ids = "5,10,15,20,25,266,267,268,269,270"
}
