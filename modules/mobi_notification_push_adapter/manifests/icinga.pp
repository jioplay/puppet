class mobi_notification_push_adapter::icinga {

  if $::mobi_notification_push_adapter::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::mobi_notification_push_adapter::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_notification_push_adapter module when icinga = true")
    }

    @@nagios_service { "check_http_mobi_push_adapter_apn_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_notification_push_adapter::icinga_cmd_args_apn}",
      service_description => "check_http_mobi_push_adapter_apn",
      notes => "PATH: ${::mobi_notification_push_adapter::icinga_cmd_args_apn}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_notification_push_adapter::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

    @@nagios_service { "check_http_mobi_push_adapter_gcm_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_notification_push_adapter::icinga_cmd_args_gcm}",
      service_description => "check_http_mobi_push_adapter_gcm",
      notes => "PATH: ${::mobi_notification_push_adapter::icinga_cmd_args_gcm}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_notification_push_adapter::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }


    @@nagios_service { "check_http_mobi_push_adapter_mock_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_notification_push_adapter::icinga_cmd_args_mock}",
      service_description => "check_http_mobi_push_adapter_mock",
      notes => "PATH: ${::mobi_notification_push_adapter::icinga_cmd_args_mock}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_notification_push_adapter::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

  }
}
