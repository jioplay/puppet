class mobi_notification_push_adapter::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args_apn = "-I $::ipaddress -p 8080 -u /mobi-push-adapter-apn/monitoring/health -w 5 -c 10"
    $icinga_cmd_args_gcm = "-I $::ipaddress -p 8080 -u /mobi-push-adapter-gcm/monitoring/health -w 5 -c 10"
    $icinga_cmd_args_mock = "-I $::ipaddress -p 8080 -u /mobi-push-adapter-mock/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    $mobi_push_adapter_gcm_package = "mobi-push-adapter-gcm"
    $mobi_push_adapter_gcm_package_ensure = present

    $mobi_push_adapter_apn_package = "mobi-push-adapter-apn"
    $mobi_push_adapter_apn_package_ensure = present

    $mobi_push_adapter_mock_package = "mobi-push-adapter-mock"
    $mobi_push_adapter_mock_package_ensure = present

}
