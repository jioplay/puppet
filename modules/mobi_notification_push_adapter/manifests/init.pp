class mobi_notification_push_adapter(
    ###icinga.pp
    $icinga = $mobi_notification_push_adapter::params::icinga,
    $icinga_instance = $mobi_notification_push_adapter::params::icinga_instance,
    $icinga_cmd_args_apn = $mobi_notification_push_adapter::params::icinga_cmd_args_apn,
    $icinga_cmd_args_gcm = $mobi_notification_push_adapter::params::icinga_cmd_args_gcm,
    $icinga_cmd_args_mock = $mobi_notification_push_adapter::params::icinga_cmd_args_mock,
    ###end icinga.pp
     $mobi_push_adapter_gcm_package = $mobi_notification_push_adapter::params::mobi_push_adapter_gcm_package,
     $mobi_push_adapter_gcm_package_ensure = $mobi_notification_push_adapter::params::mobi_push_adapter_gcm_package_ensure,

     $mobi_push_adapter_apn_package = $mobi_notification_push_adapter::params::mobi_push_adapter_apn_package,
     $mobi_push_adapter_apn_package_ensure = $mobi_notification_push_adapter::params::mobi_push_adapter_apn_package_ensure,

     $mobi_push_adapter_mock_package = $mobi_notification_push_adapter::params::mobi_push_adapter_mock_package,
     $mobi_push_adapter_mock_package_ensure = $mobi_notification_push_adapter::params::mobi_push_adapter_mock_package_ensure,

)
inherits mobi_notification_push_adapter::params{

  anchor { "mobi_notification_push_adapter::begin":} ->
    class{"mobi_notification_push_adapter::install":} ->
    class { "mobi_notification_push_adapter::icinga":} ->
  anchor { "mobi_notification_push_adapter::end": }
  os::motd::register { $name : }
}
