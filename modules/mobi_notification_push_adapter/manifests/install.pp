class mobi_notification_push_adapter::install {

    package { "${::mobi_notification_push_adapter::mobi_push_adapter_gcm_package}":
      ensure => $::mobi_notification_push_adapter::mobi_push_adapter_gcm_package_ensure,
      notify => Class["tomcat::service"],
    }

    package { "${::mobi_notification_push_adapter::mobi_push_adapter_apn_package}":
      ensure => $::mobi_notification_push_adapter::mobi_push_adapter_apn_package_ensure,
      notify => Class["tomcat::service"],
    }

    package { "${::mobi_notification_push_adapter::mobi_push_adapter_mock_package}":
      ensure => $::mobi_notification_push_adapter::mobi_push_adapter_mock_package_ensure,
      notify => [ Class["tomcat::service"], ]
    }
}
