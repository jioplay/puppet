class icinga::nrpe (

    $nrpe_package                    = $icinga::params::nrpe_package,
    $nrpe_manage_package             = $icinga::params::nrpe_manage_package,

    $pluginspackage                  = $icinga::params::pluginspackage,
    $manage_pluginspackage           = $icinga::params::manage_pluginspackage,

    $nrpe_service                    = $icinga::params::nrpe_service,
    $nrpe_manage_service_ensure      = $icinga::params::nrpe_manage_service_ensure,
    $nrpe_manage_service_enable      = $icinga::params::nrpe_manage_service_enable,

    $nrpe_monitoring                 = $icinga::params::nrpe_monitoring,
    $icinga_instance                 = $icinga::params::icinga_instance,

    #expose NRPE disk warning and critical variables
    $nrpe_disk_warning_threshold     = $icinga::params::nrpe_disk_warning_threshold,
    $nrpe_disk_critical_threshold    = $icinga::params::nrpe_disk_critical_threshold,
    $add_nagios_exchange_checks      = $icinga::params::add_nagios_exchange_checks,


) inherits icinga::params {

    # correctly qualify path to plugins in nrpe.cfg.erb template
    if $::architecture == 'x86_64' {
        $naglibdir = 'lib64'
    } else {
        $naglibdir = 'lib'
    }

    # include module name in motd
    os::motd::register { "icinga::nrpe": }

    # get rid of nagios_nrpe conflicting package if exists
    package { "nagios-nrpe":
        ensure   => absent,
        provider => yum,
    }

    package { $nrpe_package:
        ensure   => $nrpe_manage_package,
        provider => yum,
        require  => Package["nagios-nrpe"],
    }

    package { $pluginspackage:
        ensure   => $manage_pluginspackage,
        provider => yum,
    }

    class {"icinga::plugins::memcached":}

    file { "/etc/nagios/nrpe.cfg":
        ensure  => file,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        content => template("icinga/nrpe.cfg.erb"),
        require => Package[$nrpe_package],
    }

    service { $nrpe_service:
        ensure      => $nrpe_manage_service_ensure,
        enable      => $nrpe_manage_service_enable,
        hasstatus   => true,
        hasrestart  => true,
        subscribe   => File["/etc/nagios/nrpe.cfg"],
    }

    if $icinga_instance == undef {
        fail("Error: \$icinga_instance parameter must be defined when using icinga::nrpe.")
    }

     if ($add_nagios_exchange_checks){
      file { "/usr/lib64/nagios/plugins/nagios_exchange":
        ensure  => directory,
        owner   => "root",
        group   => "root",
        mode    => "0755",
        recurse => true,
        purge   => false,
        source  => "puppet://$puppetserver/modules/icinga/plugins/nagios_exchange",
      }
      package { "sysstat":
         ensure => present
      }

    }

    # unless told otherwise automatically monitor this host
    if $nrpe_monitoring {

        @@nagios_service { "check_disks_${::fqdn}":
            host_name             => $::fqdn,
            use                   => 'generic-service',
            target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
            notify                => Exec["icinga_reload"],
            check_command         => "check_disks! -w ${$nrpe_disk_warning_threshold} -c ${$nrpe_disk_critical_threshold}",
            service_description   => 'check_disks',
            normal_check_interval => '20', # less aggressive
            tag                   => "icinga_instance:${icinga_instance}",
        }

        @@nagios_service { "check_swap_${::fqdn}":
            host_name             => $::fqdn,
            use                   => 'generic-service',
            target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
            notify                => Exec["icinga_reload"],
            check_command         => 'check_swap!',
            service_description   => 'check_swap',
            normal_check_interval => '15', # less aggressive
            tag                   => "icinga_instance:${icinga_instance}",
        }

        @@nagios_service { "check_load_${::fqdn}":
            host_name             => $::fqdn,
            use                   => 'generic-service',
            target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
            notify                => Exec["icinga_reload"],
            check_command         => 'check_load!',
            service_description   => 'check_load',
            normal_check_interval => '10', # less aggressive
            tag                   => "icinga_instance:${icinga_instance}",
        }

        @@nagios_service { "check_zombie_procs_${::fqdn}":
            host_name             => $::fqdn,
            use                   => 'generic-service',
            target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
            notify                => Exec["icinga_reload"],
            check_command         => 'check_zombie_procs!',
            service_description   => 'check_zombie_procs',
            normal_check_interval => '20', # less aggressive
            tag                   => "icinga_instance:${icinga_instance}",
        }
    }

}
