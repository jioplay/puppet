class icinga::install {
  package { $::icinga::icinga_package:
    ensure  => $::icinga::icinga_manage_package,
  }
}
