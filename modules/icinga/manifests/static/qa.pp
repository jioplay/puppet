#
# Not all hosts use puppet, e.g. esxi. Manually add those hosts here.
#
class icinga::static::qa {

    $icinga_instance = "icinga.qa.smf1.mobitv"

    # include module name in motd
    os::motd::register { $name : }

    if ! $icinga_instance {
        fail("Error: \$icinga_instance parameter must be defined when using ${name}.")
    }

    file { "/usr/share/icinga/images/logos/static.png":
        ensure  => file,
        owner   => "root",
        group   => "root",
        mode    => "0664",
        source  => "puppet://$puppetserver/modules/icinga/static.png",
        require => Class["icinga::install"],
    }

    # create hostgroups
    $domains = [
          "dmz",
          "dev.dmz",
          "qa.dmz",
          "mgoint-mobidev.smf2.mobitv",
          "mgoint-mobiqa.smf2.mobitv",
          "mobitv.corp",
          "oak1.mobitv",
          "qa.smf1.mobitv",
          "prodcp.smf1.mobitv",
          "ppd-ril.qa.smf1.mobitv",
          "func-sms.qa.smf1.mobitv",
          "func-cp.qa.smf1.mobitv",
          "func-nfn.qa.smf1.mobitv",
          "ci-ril.dev.smf1.mobitv",
          "func-ril.qa.smf1.mobitv",
          "poc-bt.devops.smf1.mobitv",
          "ppd-cp.devops.smf1.mobitv",
          "ppd-nfn.devops.smf1.mobitv",
          "vagrant.mobitv",
          "ci-tmo.dev.smf1.mobitv",
          "ci-cp.dev.oak1.mobitv",
          "ci-bi.dev.oak1.mobitv",
          "ci-cp.dev.smf1.mobitv",
          "ci-nfn.dev.smf1.mobitv",
          "ci-sms.dev.smf1.mobitv",
          "ci-ms.dev.smf1.mobitv",
          "func-ms.qa.smf1.mobitv",
          "ppd-sms.devops.smf1.mobitv",
          "func-embms.qa.smf1.mobitv",
          "ppd-embms.qa.smf1.mobitv",
          "ci-embms.dev.smf1.mobitv",
          "ppd-embms.devops.smf1.mobitv",
          "ci-att.dev.smf1.mobitv",
          "ci-dt.dev.smf1.mobitv",
          "ci-vzfoa.dev.smf1.mobitv",
          "func-vzfoa.qa.smf1.mobitv",
          "ci-cp-dt.dev.smf1.mobitv",
          "ci-infra.devops.smf1.mobitv",
          "ci-infra.dev.smf1.mobitv",
    ]

    icinga::add_hostgroup { $domains :
        icinga_instance => "icinga.qa.smf1.mobitv",
    }


    ############################################################
    # Static icinga configs for non-puppetized hosts / services
    ############################################################

    # it's shit like this that makes me hate the puppet dsl
    $esxi_hosts = [
        "lab606.oak1.mobitv:10.172.64.174",
        "lab604.oak1.mobitv:10.172.64.165",
        "lab501.oak1.mobitv:10.172.64.197",
        "lab601.oak1.mobitv:10.172.64.200",
        "lab502.oak1.mobitv:10.172.64.231",
        "lab603.oak1.mobitv:10.172.64.164",
        "vmx09.dmz:172.16.252.41",
        "vmx01.dmz:172.16.252.33",
        "vmx02.dmz:172.16.252.34",
        "vmx04.dmz:172.16.252.36",
        "vmx05.dmz:172.16.252.37",
        "vmx06.dmz:172.16.252.38",
        "vmx10.dmz:172.16.252.42",
        "vmx11.dmz:172.16.252.43",
        "vmx12.dmz:172.16.252.44",
        "vmx14.dmz:172.16.252.46",
        "vmserver17.oak1.mobitv:10.172.64.118",
        "vmserver16.oak1.mobitv:10.172.64.110",
        "vmserver18.oak1.mobitv:10.172.64.119",
        "vmserver09.oak1.mobitv:10.172.64.170",
        "vmserver02.oak1.mobitv:10.172.64.45",
        "lab602.oak1.mobitv:10.172.64.201",
        "vxhstlab121.oak1.mobitv:10.172.64.120",
        "vxhstqaid01.dmz:172.16.252.32",
        "vxhstlab122.oak1.mobitv:10.172.65.210",
        "vxhstlab127.oak1.mobitv:10.172.65.212",
        "vxhstlab128.oak1.mobitv:10.172.65.211",
        "vmserver13.oak1.mobitv:10.172.65.76",
        "vxhstqabld01.mobitv.corp:172.16.1.139",
        "vmx08.dmz:172.16.252.40",
        "vmx13.dmz:172.16.252.45",
        "vmserver19.oak1.mobitv:10.172.64.89",
        "vmx07.dmz:172.16.252.39",
        "vmserver23.oak1.mobitv:10.172.64.19",
        "vmserver24.oak1.mobitv:10.172.64.24",
        "vmserver25.oak1.mobitv:10.172.64.25",
        "vmserver27.oak1.mobitv:10.172.64.27",
        "vmserver28.oak1.mobitv:10.172.64.28",
        "vmserver29.oak1.mobitv:10.172.64.17",
        "vmserver22.oak1.mobitv:10.172.64.18",
        "vmserver26.oak1.mobitv:10.172.64.26",
        "vmserver30.oak1.mobitv:10.172.64.30",
        "vmserver31.oak1.mobitv:10.172.64.31",
        "vmserver11.oak1.mobitv:10.172.65.63",
        "vmserver20.oak1.mobitv:10.172.64.218",
        "vmserver32.oak1.mobitv:10.172.64.55",
        "vmserver33.oak1.mobitv:10.172.64.33",
        "vmserver34.oak1.mobitv:10.172.64.34",
        "vmserver01.oak1.mobitv:10.172.64.44",
        "vxhstqaid03.qa.smf1.mobitv:10.174.97.103",
        "vxhstqaid-mgmt02.qa.smf1.mobitv:10.174.97.42",
        "vxhstqaid06.qa.smf1.mobitv:10.174.97.106",
        "vxhstqaid07.qa.smf1.mobitv:10.174.97.107",
        "vxhstqaid09.qa.smf1.mobitv:10.174.97.109",
        "vxhstqaid-mgmt01.qa.smf1.mobitv:10.174.97.41",
        "vxhstqaid04.qa.smf1.mobitv:10.174.97.104",
        "vxhstqaid05.qa.smf1.mobitv:10.174.97.105",
        "vxhstqaid08.qa.smf1.mobitv:10.174.97.108",
        "vxhstqaid18.qa.smf1.mobitv:10.174.97.118",
        "vxhstqaid16.qa.smf1.mobitv:10.174.97.116",
        "vxhstqaid01.qa.smf1.mobitv:10.174.97.101",
        "vxhstqaid12.qa.smf1.mobitv:10.174.97.112",
        "vxhstqaid11.qa.smf1.mobitv:10.174.97.111",
        "vxhstqaid10.qa.smf1.mobitv:10.174.97.110",
        "vxhstqaid19.qa.smf1.mobitv:10.174.97.119",
        "vxhstqaid21.qa.smf1.mobitv:10.174.97.121",
        "vxhstqaid17.qa.smf1.mobitv:10.174.97.117",
        "vxhstqaid02.qa.smf1.mobitv:10.174.97.102",
        "vxhstqaid13.qa.smf1.mobitv:10.174.97.113",
        "vxhstqaid20.qa.smf1.mobitv:10.174.97.120",
        "vxhstqaid15.qa.smf1.mobitv:10.174.97.115",
        "vxhstqaid22.qa.smf1.mobitv:10.174.97.122",
        "vxhstqaid14.qa.smf1.mobitv:10.174.97.114",
        "vxhstqabld02.mobitv.corp:172.16.2.183",
        "vmserver35.oak1.mobitv:10.172.64.35",
        "vmserver36.oak1.mobitv:10.172.64.36",
        "vxhstmobi02.mgoint-mobi.smf2.mobitv:10.182.96.102",
        "vxhstmobiadmin.mgoint-mobi.smf2.mobitv:10.182.96.100",
        "vxhstmobi01.mgoint-mobi.smf2.mobitv:10.182.96.101",
        "vxhstqadb02.qa.smf1.mobitv:10.174.97.124",
        "vxhstqadb01.qa.smf1.mobitv:10.174.97.123",
        "vxhstmobi03.mgoint-mobi.smf2.mobitv:10.182.96.103",
        "vxhstmobi04.mgoint-mobi.smf2.mobitv:10.182.96.104",
        "vmserver37.oak1.mobitv:10.172.64.47",
        "vmserver39.oak1.mobitv:10.172.64.61",
        "vmserver41.oak1.mobitv:10.172.64.66",
        "vmserver42.oak1.mobitv:10.172.64.70",
        "vmserver44.oak1.mobitv:10.172.64.53",
        "vmserver40.oak1.mobitv:10.172.64.49",
        "vmserver45.oak1.mobitv:10.172.64.37",
        "vmserver46.oak1.mobitv:10.172.64.38",
        "vmserver47.oak1.mobitv:10.172.64.39",
        "vmserver49.oak1.mobitv:10.172.64.199",
        "vmserver38.oak1.mobitv:10.172.64.60",
        "vmserver51.oak1.mobitv:10.172.64.192",
        "vmserver48.oak1.mobitv:10.172.64.198",
        "vmserver52.oak1.mobitv:10.172.64.193",
        "vmserver50.oak1.mobitv:10.172.64.191",
        "vmserver53.oak1.mobitv:10.172.64.194",
        "vmserver54.oak1.mobitv:10.172.64.189",
        "vmserver43.oak1.mobitv:10.172.64.52",
        "vmserver55.oak1.mobitv:10.172.64.73",
        "vmserver56.oak1.mobitv:10.172.64.186",
        "vmserver57.oak1.mobitv:10.172.64.187",
        "vmserver58.oak1.mobitv:10.172.24.76",
        "vmserver59.oak1.mobitv:10.172.24.77",
        "vmserver60.oak1.mobitv:10.172.64.83",
        "vmserver61.oak1.mobitv:10.172.64.136",
        "vmserver62.oak1.mobitv:10.172.64.97",
        "vmserver63.oak1.mobitv:10.172.64.234",
        "vmserver64.oak1.mobitv:10.172.64.100",
        "vmserver65.oak1.mobitv:10.172.64.102",
        "vmserver66.oak1.mobitv:10.172.64.147",
    ]

    icinga::static::monitor_esxi { $esxi_hosts: }

    # defaults class wide

    Nagios_host {
        ensure => present,
        use    => "generic-host-static",
        tag    => "icinga_instance:${icinga_instance}",
        target => "/etc/icinga/conf.d/${::fqdn}.cfg",
        notify => Exec["icinga_reload"],
    }

    Nagios_service {
        notification_period => "24x7",
        use                 => "generic-service",
        tag                 => "icinga_instance:${icinga_instance}",
        target              => "/etc/icinga/conf.d/${::fqdn}.cfg",
        notify              => Exec["icinga_reload"],
    }

    ############################################################
    # build-01.mobitv.corp
    ############################################################

    @@nagios_host { "build-01.mobitv.corp":
        alias   => "build-01.mobitv.corp",
        address => "172.16.2.197",
    }

    @@nagios_service { "check_ping_build-01.mobitv.corp":
        host_name           => "build-01.mobitv.corp",
        check_command       => "check_ping!100.0,20%!500.0,60%",
        service_description => "check_ping",
    }

    @@nagios_service { "check_disks_build-01.mobitv.corp":
      host_name             => "build-01.mobitv.corp",
      check_command         => "check_disks!",
      service_description   => "check_disks",
      normal_check_interval => "20", # less aggressive
    }

    @@nagios_service { "check_swap_build-01.mobitv.corp":
      host_name             => "build-01.mobitv.corp",
      check_command         => "check_swap!",
      service_description   => "check_swap",
      normal_check_interval => "15", # less aggressive
    }

    @@nagios_service { "check_load_build-01.mobitv.corp":
      host_name             => "build-01.mobitv.corp",
      check_command         => "check_load!",
      service_description   => "check_load",
      normal_check_interval => "10", # less aggressive
    }

    @@nagios_service { "check_zombie_procs_build-01.mobitv.corp":
      host_name             => "build-01.mobitv.corp",
      check_command         => "check_zombie_procs!",
      service_description   => "check_zombie_procs",
      normal_check_interval => "20", # less aggressive
    }

    ############################################################
    # build-10.mobitv.corp
    ############################################################

    @@nagios_host { "build-10.mobitv.corp":
        alias   => "build-10.mobitv.corp",
        address => "172.16.2.41",
    }

    @@nagios_service { "check_ping_build-10.mobitv.corp":
        host_name           => "build-10.mobitv.corp",
        check_command       => "check_ping!100.0,20%!500.0,60%",
        service_description => "check_ping",
    }

    @@nagios_service { "check_disks_build-10.mobitv.corp":
      host_name             => "build-10.mobitv.corp",
      check_command         => "check_disks!",
      service_description   => "check_disks",
      normal_check_interval => "20", # less aggressive
    }

    @@nagios_service { "check_swap_build-10.mobitv.corp":
      host_name             => "build-10.mobitv.corp",
      check_command         => "check_swap!",
      service_description   => "check_swap",
      normal_check_interval => "15", # less aggressive
    }

    @@nagios_service { "check_load_build-10.mobitv.corp":
      host_name             => "build-10.mobitv.corp",
      check_command         => "check_load!",
      service_description   => "check_load",
      normal_check_interval => "10", # less aggressive
    }

    @@nagios_service { "check_zombie_procs_build-10.mobitv.corp":
      host_name             => "build-10.mobitv.corp",
      check_command         => "check_zombie_procs!",
      service_description   => "check_zombie_procs",
      normal_check_interval => "20", # less aggressive
    }

    ############################################################
    # denc10.dev.dmz
    ############################################################

    @@nagios_host { "denc10.dev.dmz":
        alias   => "denc10.dev.dmz",
        address => "172.16.252.221",
    }

    @@nagios_service { "check_ping_denc10.dev.dmz":
        host_name           => "denc10.dev.dmz",
        check_command       => "check_ping!100.0,20%!500.0,60%",
        service_description => "check_ping",
    }

    @@nagios_service { "check_disks_denc10.dev.dmz":
      host_name             => "denc10.dev.dmz",
      check_command         => "check_disks!",
      service_description   => "check_disks",
      normal_check_interval => "20", # less aggressive
    }

    @@nagios_service { "check_swap_denc10.dev.dmz":
      host_name             => "denc10.dev.dmz",
      check_command         => "check_swap!",
      service_description   => "check_swap",
      normal_check_interval => "15", # less aggressive
    }

    @@nagios_service { "check_load_denc10.dev.dmz":
      host_name             => "denc10.dev.dmz",
      check_command         => "check_load!",
      service_description   => "check_load",
      normal_check_interval => "10", # less aggressive
    }

    @@nagios_service { "check_zombie_procs_denc10.dev.dmz":
      host_name             => "denc10.dev.dmz",
      check_command         => "check_zombie_procs!",
      service_description   => "check_zombie_procs",
      normal_check_interval => "20", # less aggressive
    }

    ############################################################
    # build-11.mobitv.corp
    ############################################################

    @@nagios_host { "build-11.mobitv.corp":
        alias   => "build-11.mobitv.corp",
        address => "172.16.128.63",
    }

    @@nagios_service { "check_ping_build-11.mobitv.corp":
        host_name           => "build-11.mobitv.corp",
        check_command       => "check_ping!100.0,20%!500.0,60%",
        service_description => "check_ping",
    }

    ############################################################
    # build-12.mobitv.corp
    ############################################################

    @@nagios_host { "build-12.mobitv.corp":
        alias   => "build-12.mobitv.corp",
        address => "172.16.128.66",
    }

    @@nagios_service { "check_ping_build-12.mobitv.corp":
        host_name           => "build-12.mobitv.corp",
        check_command       => "check_ping!100.0,20%!500.0,60%",
        service_description => "check_ping",
    }

}
