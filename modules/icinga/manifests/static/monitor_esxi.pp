define icinga::static::monitor_esxi (
    $icinga_instance = "icinga.qa.smf1.mobitv",
) {

    # title is hostname:ipaddress and needs split as the define type does not
    # support iteration of a list of hashes
    $esxi_host = inline_template("<%= title.split(':')[0] -%>")
    $esxi_address = inline_template("<%= title.split(':')[1] -%>")

    if ! $esxi_host { fail("\$esxi_host not defined in ${name}.") }
    if ! $esxi_address { fail("\$esxi_address not defined in ${name}.") }

    @@nagios_host { "${esxi_host}":
        ensure  => present,
        alias   => $esxi_host,
        address => $esxi_address,
        use     => "generic-host-static",
        tag     => "icinga_instance:${icinga_instance}",
        target  => "/etc/icinga/conf.d/${esxi_host}.cfg",
        notify  => Exec["icinga_reload"],
    }

    Nagios_service {
        host_name           => $esxi_host,
        notification_period => "24x7",
        use                 => "generic-service",
        tag                 => "icinga_instance:${icinga_instance}",
        target              => "/etc/icinga/conf.d/${esxi_host}.cfg",
        notify              => Exec["icinga_reload"],
    }

    @@nagios_service { "check_ping_${esxi_host}":
        check_command       => "check_ping!100.0,20%!500.0,60%",
        service_description => "check_ping",
    }

    @@nagios_service { "check_esx_cpu_${esxi_host}":
        check_command       => "check_esx_cpu!100.0,20%!500.0,60%",
        service_description => "check_esx_cpu",
    }

    @@nagios_service { "check_esx_mem_${esxi_host}":
        check_command       => "check_esx_mem!100.0,20%!500.0,60%",
        service_description => "check_esx_mem",
    }

    @@nagios_service { "check_esx_net_${esxi_host}":
        check_command       => "check_esx_net!100.0,20%!500.0,60%",
        service_description => "check_esx_net",
    }

    @@nagios_service { "check_esx_runtime_${esxi_host}":
        check_command       => "check_esx_runtime!100.0,20%!500.0,60%",
        service_description => "check_esx_runtime",
    }

    @@nagios_service { "check_esx_ioread_${esxi_host}":
        check_command       => "check_esx_ioread!100.0,20%!500.0,60%",
        service_description => "check_esx_ioread",
    }

    @@nagios_service { "check_esx_iowrite_${esxi_host}":
        check_command       => "check_esx_iowrite!100.0,20%!500.0,60%",
        service_description => "check_esx_iowrite",
    }

}
