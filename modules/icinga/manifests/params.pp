#
# Class: icinga::params
#
# This module manages icinga paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class icinga::params {

  #############################################################################
  ###  icinga class params
  #############################################################################

  ### install.pp
  $icinga_package = [ "icinga",
                      "icinga-gui",
                      "nagios-plugins-nrpe", ]

  $icinga_manage_package = "present"


  ### service.pp
  $icinga_service = "icinga"
  $icinga_manage_service_ensure = "running"
  $icinga_manage_service_enable = true


  ### config.pp

  # icinga.cfg template variables, also used in templates.cfg for
  # generic-{host,service} definitions.
  # 1 = enable, 0 = disable
  $execute_service_checks = "1"
  $execute_host_checks = "1"

  # enable notifications by default
  $enable_notifications = "1"

  # disable processing of perf by default
  $process_performance_data = "0"


  # icinga package owned
  $resource_conf = "resource.cfg"

  # icinga_gui package owned
  $htpasswd_conf = "htpasswd.users"
  $cgi_conf = "cgi.cfg"
  $cgiauth_conf = "cgiauth.cfg"
  $apache_conf = "icinga.conf"

  # Is this icinag instance a "central" instance? Should it receive ALL configs?
  $central = false


  #############################################################################
  ###  optional subclass params
  #############################################################################

  ### nsca.pp
  $nsca_package = "nsca"
  $nsca_manage_package = "present"

  $nsca_service = "nsca"
  $nsca_manage_service_ensure = "running"
  $nsca_manage_service_enable = true

  $nsca_conf = "nsca.cfg"

  ### send_nsca.pp
  $send_nsca_package = "nsca-client"
  $send_nsca_manage_package = "present"
  $send_nsca_conf = "send_nsca.cfg"

  ### ocp_daemon.pp
  $ocp_bindir = "/usr/local/bin"
  $ocp_fifodir = "/var/spool/icinga"
  $destination = undef

  ### nrpe.pp
  $nrpe_package = "nrpe"
  $nrpe_manage_package = "present"

  $pluginspackage = "nagios-plugins-all"
  $manage_pluginspackage = "present"

  $nrpe_service = "nrpe"
  $nrpe_manage_service_ensure = "running"
  $nrpe_manage_service_enable = true

  $nrpe_monitoring = true

  $nrpe_disk_warning_threshold = "20%"
  $nrpe_disk_critical_threshold = "10%"

  ### register.pp and nrpe.pp ... as if life wasn't enough for you already
  # icinga instance that should monitor this node, based on icinga fqdn
  $icinga_instance = "icinga"
  $icinga_hostgroup = $::domain

  $add_nagios_exchange_checks = false
}
