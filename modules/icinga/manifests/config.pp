class icinga::config {

  File {
    ensure  => present,
    owner   => "icinga",
    group   => "icinga",
    mode    => "0644",
    require => Class["icinga::install"],
  }

  file { "/usr/share/icinga/images/logos/puppet.png":
    owner  => "root",
    group  => "root",
    mode   => "0664",
    source => "puppet://$puppetserver/modules/icinga/puppet.png",
  }

  file { "/etc/icinga/icinga.cfg":
    content => template("icinga/icinga.cfg.erb"),
  }

  file { "/etc/icinga/resource.cfg":
    source  => "puppet://$puppetserver/modules/icinga/${::icinga::resource_conf}",
  }

  file { "/etc/icinga/cgi.cfg":
    source  => "puppet://$puppetserver/modules/icinga/${::icinga::cgi_conf}",
  }

  file { "/etc/icinga/cgiauth.cfg":
    source  => "puppet://$puppetserver/modules/icinga/${::icinga::cgiauth_conf}",
  }

  file { "/etc/icinga/htpasswd.users":
    source  => "puppet://$puppetserver/modules/icinga/${::icinga::htpasswd_conf}",
  }

  file { "/etc/httpd/conf.d/icinga.conf":
    owner   => "root",
    group   => "root",
    mode    => "0644",
    source  => "puppet://$puppetserver/modules/icinga/${::icinga::apache_conf}",
    notify  => Service["httpd"],
  }

  file { "/usr/lib64/nagios/plugins/thirdparty":
    ensure  => directory,
    owner   => "root",
    group   => "root",
    mode    => "0755",
    recurse => true,
    purge   => false,
    source  => "puppet://$puppetserver/modules/icinga/plugins",
  }

  file { "/etc/icinga/objects":
    ensure  => directory,
    recurse => true,
    purge   => true,
    source  => "puppet://$puppetserver/modules/icinga/objects",
  }

  file { "/etc/icinga/conf.d":
      ensure  => directory,
      owner   => "icinga",
      group   => "icinga",
      mode    => "0644",
      recurse => true,
  }

  resources { ['nagios_host', 'nagios_hostgroup', 'nagios_service']:
      purge => true,
  }

  file { "/etc/icinga/objects/templates.cfg":
    ensure  => present,
    content => template("icinga/templates.cfg.erb"),
  }

  # icinga epel rpms moved from using /var/icinga to /var/spool/icinga in 1.7.X
  # just make sure the directory and symlink exist for backwards compatibility
  file { "/var/spool/icinga":
    ensure => directory,
  }

  file { '/var/icinga':
    ensure => link,
    target => '/var/spool/icinga',
  }

  if $icinga::central {
    # Collect/realize ALL exported nagios resources, I'm a central icinga server.
    Nagios_host <<| |>> {
      notify => Exec["icinga_reload"],
    }

    Nagios_hostgroup <<| |>> {
      notify => Exec["icinga_reload"],
    }

    Nagios_service <<| |>> {
      notify => Exec["icinga_reload"],
    }
  } else {
    # collect/realize all exported nagios resources tagged for this intance of icinga
    Nagios_host <<| tag == "icinga_instance:${::hostname}" |>> {
      notify => Exec["icinga_reload"],
    }

    Nagios_hostgroup <<| tag == "icinga_instance:${::hostname}" |>> {
      notify => Exec["icinga_reload"],
    }

    Nagios_service <<| tag == "icinga_instance:${::hostname}" |>> {
      notify => Exec["icinga_reload"],
    }
  }

  # in case no exported resources are realized we still need blank configs
  file { [
      "/etc/nagios/nagios_host.cfg",
      "/etc/nagios/nagios_hostgroup.cfg",
      "/etc/nagios/nagios_service.cfg",
      "/etc/icinga/nagios_host.cfg",
      "/etc/icinga/nagios_hostgroup.cfg",
      "/etc/icinga/nagios_service.cfg",
      ]:
    ensure  => file,
    content => "",
    replace => false,
    before  => Exec["icinga_reload"],
  }

  exec { "icinga_reload":
    command     => "/bin/bash -c 'cp -f /etc/nagios/nagios_{host,hostgroup,service}.cfg /etc/icinga/ && chown -R icinga:icinga /etc/icinga'",
    refreshonly => true,
  }

}
