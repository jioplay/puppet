class icinga::send_nsca (

  $send_nsca_package = $icinga::params::send_nsca_package,
  $send_nsca_manage_package = $icinga::params::send_nsca_manage_package,

  $send_nsca_conf = $icinga::params::send_nsca_conf,

) inherits icinga::params {


  # get rid of nagios_nrpe conflicting package if exists
  package { "nagios-nsca-client":
    ensure   => absent,
  }

  package { "send_nsca":
    name    => $send_nsca_package,
    ensure  => $send_nsca_manage_package,
    require => Package["nagios-nsca-client"],
  }

  file { "send_nsca":
    name    => "/etc/nagios/send_nsca.cfg",
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    source  => "puppet://$puppetserver/modules/icinga/${send_nsca_conf}",
    require => Package["send_nsca"],
  }

  # include module name in motd
  os::motd::register { "icinga::send_nsca": }
}
