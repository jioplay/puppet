class icinga::service {

  service { "icinga":
    name       => $::icinga::icinga_service,
    ensure     => $::icinga::icinga_manage_service_ensure,
    enable     => $::icinga::icinga_manage_service_enable,
    hasstatus  => true,
    hasrestart => true,
    subscribe  => Class[icinga::config],
  }

}
