class icinga::plugins::memcached {
  package { "perl-Cache-Memcached":
    ensure => present,
    provider => yum,
  }
}
