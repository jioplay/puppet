# Class: icinga
#
# This module manages icinga
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class icinga (

  $icinga_package = $icinga::params::icinga_package,
  $icinga_manage_package = $icinga::params::manage_package,

  $icinga_service = $icinga::params::icinga_service,
  $icinga_manage_service_ensure = $icinga::params::icinga_manage_service_ensure,
  $icinga_manage_service_enable = $icinga::params::icinga_manage_service_enable,

  # icinga.cfg template variables
  $execute_service_checks = $icinga::params::execute_service_checks,
  $execute_host_checks = $icinga::params::execute_host_checks,
  $process_performance_data = $icinga::params::process_performance_data,
  $enable_notifications = $icinga::params::enable_notifications,

  $resource_conf = $icinga::params::resource_conf,

  $cgi_conf = $icinga::params::cgi_conf,
  $cgiauth_conf = $icinga::params::cgiauth_conf,
  $htpasswd_conf = $icinga::params::htpasswd_conf,
  $apache_conf = $icinga::params::apache_conf,

  # Is this icinag instance a "central" instance? Should it receive ALL configs?
  $central = $icinga::params::central,

) inherits icinga::params {
  include icinga::install, icinga::config, icinga::service

  # include module name in motd
  os::motd::register { "icinga": }
}
