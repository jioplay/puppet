class icinga::nsca (

  $nsca_package = $icinga::params::nsca_package,
  $nsca_manage_package = $icinga::params::nsca_manage_package,
  $nsca_service = $icinga::params::nsca_service,

  $nsca_manage_service_ensure = $icinga::params::nsca_manage_service_ensure,
  $nsca_manage_service_enable = $icinga::params::nsca_manage_service_enable,

  $nsca_conf = $icinga::params::nsca_conf,

) inherits icinga::params {


  # get rid of nagios_nsca conflicting package if exists
  package { "nagios-nsca":
    ensure   => absent,
  }

  package { "nsca":
    name   => $nsca_package,
    ensure => $nsca_manage_package,
    require => Package["nagios-nsca"],
  }

  File {
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    backup  => ".puppetsave",
    require => Package["nsca"],
  }

  file { "nsca":
    name    => "/etc/nagios/nsca.cfg",
    source  => "puppet://$puppetserver/modules/icinga/${nsca_conf}",
  }

  # guarantee use of cmd after icinga restart by restarting nsca
  # since icinga removes cmd on restart and nsca switches to dumpfile
  # and sometimes doesn't switch back, leaving central server stale
  service { "nsca":
    name       => $nsca_service,
    ensure     => $nsca_manage_service_ensure,
    enable     => $nsca_manage_service_enable,
    hasstatus  => false,
    hasrestart => true,
    pattern    => "/usr/sbin/nsca",
    subscribe  => [ File["nsca"], Package["nsca"], Service["icinga"] ],
  }

  # include module name in motd
  os::motd::register { "icinga::nsca": }
}
