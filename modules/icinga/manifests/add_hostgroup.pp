define icinga::add_hostgroup (
    $icinga_instance,
) {

    @@nagios_hostgroup { $title :
        ensure => present,
        alias  => $title,
        tag    => "icinga_instance:${icinga_instance}",
        target => "/etc/icinga/conf.d/hostgroups.cfg",
        notify => Exec["icinga_reload"],
    }

}
