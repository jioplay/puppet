class icinga::ocp_daemon (

  $ocp_bindir = $icinga::params::ocp_bindir,
  $ocp_fifodir = $icinga::params::ocp_fifodir,
  $destination = $icinga::params::destination,

) inherits icinga::params {

  # fail hard if destination undefined
  if $destination == undef {
    fail("\$destination param is required when using the ocp_daemon subclass!")
  }

  $ocp_name = "ocp_daemon.pl"
  $ocp_cmd = "/usr/bin/nohup ${ocp_bindir}/${ocp_name} -f ${ocp_fifodir}/host-perfdata.fifo,${ocp_fifodir}/service-perfdata.fifo -n /usr/sbin/send_nsca -H $destination -c /etc/nagios/send_nsca.cfg -r 1 > /var/log/ocp_daemon.log 2>&1 &"

  # create named pipes for icinga event broker check results -> ocp_daemon.pl
  exec { "perfdata_fifos":
    command => "/usr/bin/mkfifo ${ocp_fifodir}/{host,service}-perfdata.fifo",
    unless  => "/bin/bash -c '[ -p ${ocp_fifodir}/host-perfdata.fifo -a -p ${ocp_fifodir}/service-perfdata.fifo ]'",
  }

  # setup correct perms on named pipes created above
  file { "hostperf_fifo":
    path    => "${ocp_fifodir}/host-perfdata.fifo",
    ensure  => present,
    owner   => "icinga",
    group   => "icinga",
    mode    => "0644",
    require => Exec["perfdata_fifos"],
  }

  # setup correct perms on named pipes created above
  file { "serviceperf_fifo":
    path    => "${ocp_fifodir}/service-perfdata.fifo",
    ensure  => present,
    owner   => "icinga",
    group   => "icinga",
    mode    => "0644",
    require => Exec["perfdata_fifos"],
  }

  # install ocp_daemon perl script, no RPM exists
  file { "ocp_daemon":
    name   => "${ocp_bindir}/${ocp_name}",
    ensure => present,
    owner  => "root",
    group  => "root",
    mode   => "0755",
    source => "puppet://$puppetserver/modules/icinga/ocp_daemon.pl",
  }

  # perl library required for ocp_daemon.pl
  package { "perl-Event-Lib":
    ensure => present,
  }

  # add ocp_daemon to rc.local to launch on startup, no init exists
  exec { "ocp_daemon_boot":
    command => "/bin/sed -i '/ocp_daemon/d' /etc/rc.d/rc.local && /bin/echo -e '\n# launch ocp_daemon on startup\n${ocp_cmd}\n' >> /etc/rc.d/rc.local",
    unless  => "/bin/grep -q '${ocp_cmd}' /etc/rc.d/rc.local",
  }

  # execute ocp_daemon perl script if not running
  exec { "ocp_daemon":
    command => $ocp_cmd,
    unless  => "/usr/bin/pgrep ${ocp_name}",
    require => [ Package["perl-Event-Lib"],
                 Package["send_nsca"],
                 File["ocp_daemon"],
                 File["hostperf_fifo"],
                 File["serviceperf_fifo"], ],
  }

  # include module name in motd
  os::motd::register { "icinga::ocp_daemon": }
}
