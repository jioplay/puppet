# This class exports icinga host and service check resources
# NRPE related checks are part of the nrpe class.
class icinga::register (

    # icinga instance that should monitor this node, based on icinga fqdn
    $icinga_instance = $icinga::params::icinga_instance,

    # defaults to $::domain
    $icinga_hostgroup = $icinga::params::icinga_hostgroup,

    ) inherits icinga::params  {

    if $icinga_instance == undef {
        fail("Error: \$icinga_instance parameter must be defined when using icinga::register.")
    }

    @@nagios_host { "${::fqdn}":
        ensure     => present,
        alias      => $::fqdn,
        address    => $::ipaddress,
        hostgroups => [ $icinga_hostgroup ],
        use        => "generic-host",
        tag        => "icinga_instance:${icinga_instance}",
        target     => "/etc/icinga/conf.d/${::fqdn}.cfg",
        notify     => Exec["icinga_reload"],
    }

    @@nagios_service { "check_ping_${::fqdn}":
        host_name           => $::fqdn,
        check_command       => "check_ping!100.0,20%!500.0,60%",
        notification_period => "24x7",
        service_description => "check_ping",
        use                 => "generic-service",
        tag                 => "icinga_instance:${icinga_instance}",
        target              => "/etc/icinga/conf.d/${::fqdn}.cfg",
        notify              => Exec["icinga_reload"],
    }

    # include module name in motd
    os::motd::register { $name: }

}
