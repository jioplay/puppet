# == Class: aaa_sprintproxy
#
#  installs transaction processor ng component
#
# === Parameters:
#
#    $sprintproxy_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
# Remember: No empty lines between comments and class definition
#
class aaa_sprintproxy (

    $sprintproxy_ver = $aaa_sprintproxy::sprintproxy_ver,
)
inherits aaa_sprintproxy::params {
    include aaa_sprintproxy::install, aaa_sprintproxy::config
    anchor { "aaa_sprintproxy::begin": } -> Class["aaa_sprintproxy::install"]
    Class["aaa_sprintproxy::config"] -> anchor { "aaa_sprintproxy::end" :}
    os::motd::register { $name : }
}
