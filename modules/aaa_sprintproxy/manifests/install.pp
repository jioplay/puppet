class aaa_sprintproxy::install {
    package { "mobi-aaa-sprint-proxy-${aaa_sprintproxy::sprintproxy_ver}" :
        ensure => present,
  notify => Class["tomcat::service"]
    }
}
