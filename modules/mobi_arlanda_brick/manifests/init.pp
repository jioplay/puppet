class mobi_arlanda_brick {
    include('mobi-arlanda::juamq::install')
    include('mobi-arlanda::juing::install')
    include('mobi-arlanda::juarn::install')
    include('mobi-arlanda::jutra::install')
    include('mobi-arlanda::jusch::install')
    include('mobi-arlanda::judrm::install')
    include('mobi-arlanda::eesimulator::install')
    include('mobi-arlanda::playlistcomposer::install')

  # Database services must be start before juamq, juing, juarn, judrm
  # SOLR services must be start before juing (publisher)
  # Msfrn services must be start before juing
  # Juamq services must be start before juing
  # Juamq services must be start before juarn
  Class['mobi-arlanda::juamq::install'] ->
  Class["mobi_arlanda::juamq::start"] -> # Start ActiveMQ
  Class['mobi-arlanda::juing::install'] -> # Includes solr/jukebox, jukebox includes httpd
  Class['mobi-arlanda::juarn::install'] ->
  Class['mobi-arlanda::jutra::install'] ->
  Class['mobi-arlanda::jusch::install'] ->
  Class['mobi-arlanda::judrm::install'] ->
  Class['mobi-arlanda::eesimulator::install'] ->
  Class['mobi-arlanda::playlistcomposer::install']
}
