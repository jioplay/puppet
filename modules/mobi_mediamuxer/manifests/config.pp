class mobi_mediamuxer::config {

    File {
        owner => root,
        group => root,
        mode => "0644",
    }

    $md3 = $mobi_mediamuxer::md3
    if ! $md3 {
        $livebackend_fragmentserver = $mobi_mediamuxer::livebackend_fragmentserver
        $vodbackend_fragmentserver = $mobi_mediamuxer::vodbackend_fragmentserver
        $recordingbackend_fragmentserver = $mobi_mediamuxer::recordingbackend_fragmentserver
        $licensemanagerserver = $mobi_mediamuxer::licensemanagerserver

        file { "/etc/httpd/conf.d/mod_mediamuxer.conf":
            ensure => present,
            content => template("mobi_mediamuxer/mod_mediamuxer.conf.erb"),
            notify => Class["apache::service"],
            require => Class["apache::install"],
        }
       file { "/etc/httpd/conf.d/mobi-httpd-mediamuxer-hls-live.conf":
            ensure => present,
            content => template("mobi_mediamuxer/mobi-httpd-mediamuxer-hls-live.conf.erb"),
            notify => Service["httpd"],
            require => Class["apache::install"],
            }

    } else {
        $serve_live = $mobi_mediamuxer::serve_live
        $livecontentroot = $mobi_mediamuxer::livecontentroot
        $serve_vod = $mobi_mediamuxer::serve_vod
        $vodcontentroot = $mobi_mediamuxer::vodcontentroot
        $serve_recordings = $mobi_mediamuxer::serve_recordings
        $recordingscontentroot = $mobi_mediamuxer::recordingscontentroot
        $deploymentserverurl = $mobi_mediamuxer::deploymentserverurl
        $licensemanagerserver = $mobi_mediamuxer::licensemanagerserver

        file { "/etc/httpd/conf.d/mod_mediamuxer.conf":
            ensure => present,
            content => template("mobi_mediamuxer/mod_mediamuxer.conf.sample.erb"),
            notify => Service["httpd"],
            require => Class["apache::install"],
            }

       file { "/etc/httpd/conf.d/md_access.conf":
            ensure => present,
            content => template("mobi_mediamuxer/md_access.conf.erb"),
            notify => Service["httpd"],
            require => Class["apache::install"],
            }

      }
}
