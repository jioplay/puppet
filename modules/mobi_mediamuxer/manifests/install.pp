class mobi_mediamuxer::install {
  package { "$mobi_mediamuxer::package":
    provider => yum,
    ensure   => "$mobi_mediamuxer::version",
    notify   => Class["apache::service"],
  }
}
