class mobi_mediamuxer::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 80 -u /mediamuxer/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $package = "mobi-mediamuxer"
    $version = ""
    $livebackend_fragmentserver=undef
    $vodbackend_fragmentserver=undef
    $recordingbackend_fragmentserver=undef
    $licensemanagerserver=undef
    $md3 = false
    $serve_vod = "On"
    $serve_recordings = "On"
    $serve_live = "On"
    $livecontentroot = "/var/www/dash-live"
    $vodcontentroot = "/var/Jukebox/vod"
    $recordingscontentroot = "/var/Jukebox/recordings"
    $deploymentserverurl = "http://10.178.220.50:8080/deploymentserver"
}
