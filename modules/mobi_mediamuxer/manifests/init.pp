# == Class: mobi_mediamuxer
#
# installs mobi_mediamuxer
#
# === Parameters:
#
#    $package::      the mobi_mediamuxer package name
#
#    $version::      the mobi_mediamuxer package version
#
#    $livebackend_fragmentserver:: (v)ip or hostname to the livebackend-fragmentserver
#
#    $vodbackend_fragmentserver:: (v)ip or hostname to the vodbackend-fragmentserver
#
#    $recordingbackend_fragmentserver:: (v)ip or hostname to the recordingbackend-fragmentserver
#
#    $licensemanagerserver:: (v)ip or hostname to the licensemanager-server
#
#    $encrypt_hls:: On/Off
#
#    $forwardedit:: On/Off
#
#    $livemanifest_size:: Number of playlist items in Live Manifests  [Default: 10]
#
#    $auto_update:: true/false (Wrapper for ensure => latest, )
#
#    $md3:: true/false (switch for which version of mediamuxer to use, true is the new for mediadelivery3.0 [D: false]
#
#    $serve_live::  Whether or not to serve incoming live requests (D: On)
#
#    $livecontentroot:: Where to look for live content [D: /var/www/dash-live]
#
#    $serve_vod::   Serve incoming vod requests or not. (D: On)
#
#    $vodcontentroot::  where to look for vod content [D: /var/Jukebox/vod]
#
#    $serve_recordings:: Serve incoming recordings requests or not. (D: On)
#
#    $recordingscontentroot:: Where to look for recordings [D: /var/Jukebox/recordings]
#
#    $deploymentserverurl:: Url to the deployment server
#
# === Requires
#
#    Apache
#
# === Sample Usage
#
#  class { "mobi_mediamuxer" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mediamuxer (
    ###icinga.pp
    $icinga = $mobi_mediamuxer::params::icinga,
    $icinga_instance = $mobi_mediamuxer::params::icinga_instance,
    $icinga_cmd_args = $mobi_mediamuxer::params::icinga_cmd_args,
    ###end icinga.pp
    $package = $mobi_mediamuxer::params::package,
    $version = $mobi_mediamuxer::params::version,
    $livebackend_fragmentserver=$mobi_mediamuxer::params::livebackend_fragmentserver,
    $vodbackend_fragmentserver=$mobi_mediamuxer::params::vodbackend_fragmentserver,
    $recordingbackend_fragmentserver=$mobi_mediamuxer::params::recordingbackend_fragmentserver,
    $licensemanagerserver=$mobi_mediamuxer::params::licensemanagerserver,
    ###config parameters for mediamuxer version > 0.8.0
    $md3=$mobi_mediamuxer::params::md3,
    $serve_live = $mobi_mediamuxer::params::serve_live,
    $livecontentroot=$mobi_mediamuxer::params::livecontentroot,
    $serve_vod = $mobi_mediamuxer::params::serve_vod,
    $vodcontentroot=$mobi_mediamuxer::params::vodcontentroot,
    $serve_recordings = $mobi_mediamuxer::params::serve_recordings,
    $recordingscontentroot=$mobi_mediamuxer::params::recordingscontentroot,
    $deploymentserverurl=$mobi_mediamuxer::params::deploymentserverurl,
)
inherits mobi_mediamuxer::params {
    include mobi_mediamuxer::install, mobi_mediamuxer::config

    anchor { "mobi_mediamuxer::begin": } -> Class["mobi_mediamuxer::install"]
    class { "mobi_mediamuxer::icinga":} ->
    Class["mobi_mediamuxer::config"] -> anchor { "mobi_mediamuxer::end": }

    os::motd::register { $name : }
}
