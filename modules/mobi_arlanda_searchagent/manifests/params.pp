class mobi_arlanda_searchagent::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /searchagent/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $version = "5.0.0-210408"
    $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100"
    $update_legacy_solr = "false"
    $solr_master_url = "http://solrmastervip:8080/mobi-solr/"
    $solr_connection_timeout = "10000"
    $solr_max_connections = "100"
    $solr_max_retires = "1"
    $legacy_solr_master_url = "http://arlandasearch:8080/arlanda-search/"
    $update_thumbnail_url = "arlanda:/"
    $legacy_excluded_fields = "sprint_media2go_enabled,original_air_date,audio_languages_list,category"
    $solr_retry_connect_count = "3"
    $retry_connect_solr = "true"
    $wait_time_between_retries = "5000"
}
