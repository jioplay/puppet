# == Class: mobi_arlanda_searchagent
#
#  installs searchagent component
#
# === Parameters:
#
#    $version::
#    $solr_master_url::
#    $legacy_solr_master_url::
#    $update_legacy_solr::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_arlanda_searchagent" :
#           version => "5.0.0-178058",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_searchagent (
    ###icinga.pp
    $icinga = $mobi_arlanda_searchagent::params::icinga,
    $icinga_instance = $mobi_arlanda_searchagent::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_searchagent::params::icinga_cmd_args,
    ###end icinga.pp
    $version = $mobi_arlanda_searchagent::params::version,
    $amq_broker_url = $mobi_arlanda_searchagent::params::amq_broker_url,
    $solr_master_url = $mobi_arlanda_searchagent::params::solr_master_url,
    $solr_connection_timeout = $mobi_arlanda_searchagent::params::solr_connection_timeout,
    $solr_max_connections = $mobi_arlanda_searchagent::params::solr_max_connections,
    $solr_max_retires = $mobi_arlanda_searchagent::params::solr_max_retires,
    $legacy_solr_master_url = $mobi_arlanda_searchagent::params::legacy_solr_master_url,
    $update_legacy_solr = $mobi_arlanda_searchagent::params::update_legacy_solr,
    $update_thumbnail_url = $mobi_arlanda_searchagent::params::update_thumbnail_url,
    $legacy_excluded_fields = $mobi_arlanda_searchagent::params::legacy_excluded_fields,
    $solr_retry_connect_count = $mobi_arlanda_searchagent::params::solr_retry_connect_count,
    $retry_connect_solr = $mobi_arlanda_searchagent::params::retry_connect_solr,
    $wait_time_between_retries = $mobi_arlanda_searchagent::params::wait_time_between_retries,
)

inherits mobi_arlanda_searchagent::params {
    include mobi_arlanda_searchagent::install, mobi_arlanda_searchagent::config
    anchor { "mobi_arlanda_searchagent::begin": } -> Class["mobi_arlanda_searchagent::install"] ->
    Class["mobi_arlanda_searchagent::config"] ->
    class { "mobi_arlanda_searchagent::icinga":} ->
    anchor { "mobi_arlanda_searchagent::end" :} -> os::motd::register { $name : }
}

