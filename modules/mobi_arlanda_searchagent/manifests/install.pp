class mobi_arlanda_searchagent::install {
  package { "mobi-arlanda-searchagent":
    ensure => "${mobi_arlanda_searchagent::version}",
    notify => Class["tomcat::service"],
  }
}
