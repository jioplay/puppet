class mobi_arlanda_searchagent::config {
    file { "/opt/arlanda/conf/searchagent/searchagent.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_searchagent/searchagent.properties.erb'),
        require => Class["mobi_arlanda_searchagent::install"],
        notify   => Class["tomcat::service"],
    }
}
