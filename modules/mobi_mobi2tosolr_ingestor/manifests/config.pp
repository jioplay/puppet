class mobi_mobi2tosolr_ingestor::config {
# enable generic messages
  if $::mobi_mobi2tosolr_ingestor::mobi_mobi2tosolr_ingestor_enabled {
  file { "/opt/mobi-mobi2tosolr-ingestor/config/configuration.properties":
      ensure => present,
      source => "puppet://$puppetserver/modules/mobi_mobi2tosolr_ingestor/configuration.properties",
      owner   => "rtv",
      group   => "rtv",
      mode    => "0744",
      }
  }
  }
