class mobi_mobi2tosolr_ingestor::install {

    $rpmname="mobi-mobi2tosolr-ingestor"
    $pkg_name = $mobi_mobi2tosolr_ingestor::version ? {
        undef => $mobi_mobi2tosolr_ingestor::install::rpmname,
        default => "${mobi_mobi2tosolr_ingestor::install::rpmname}-${mobi_mobi2tosolr_ingestor::version}",
    }

    package { $mobi_mobi2tosolr_ingestor::install::pkg_name :
        ensure => $mobi_mobi2tosolr_ingestor::pkg_ensure,
    }
}
