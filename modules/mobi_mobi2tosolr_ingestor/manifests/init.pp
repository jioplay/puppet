# Class: mobi_mobi2tosolr_ingestor
#
# This module manages mobi_mobi2tosolr_ingestor
#
# Parameters:
#
#     version:: the version of the rpm to install
#     pkg_ensure:: the ensure property (D: present)
#
# Requires:
#
#     Java
#
# Sample Usage:
#
#     class { "mobi_mobi2tosolr_ingestor":
#             version => '5.1.5-216731',
#           }
#
#
# [Remember: No empty lines between comments and class definition]
class mobi_mobi2tosolr_ingestor (
    $version = $mobi_mobi2tosolr_ingestor::params::version,
    $pkg_ensure = $mobi_mobi2tosolr_ingestor::params::pkg_ensure,
    $mobi_mobi2tosolr_ingestor_enabled=$mobi_mobi2tosolr_ingestor::params::mobi_mobi2tosolr_ingestor_enabled,
    )
inherits mobi_mobi2tosolr_ingestor::params {
include mobi_mobi2tosolr_ingestor::install
include mobi_mobi2tosolr_ingestor::config
anchor{"mobi_mobi2tosolr_ingestor::begin" :} ->
Class ["mobi_mobi2tosolr_ingestor::install"] ->
    Class["mobi_mobi2tosolr_ingestor::config"] ->
    anchor{"mobi_mobi2tosolr_ingestor::end" :}
    os::motd::register{ $name :}
    }
