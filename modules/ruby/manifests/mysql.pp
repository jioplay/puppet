# Class: ruby::mysql
#
# This class installs Ruby mysql libraries
#
# Parameters:
#
# Actions:
#   - Install mysql libraries
#
# Requires:
#
# Sample Usage:
#
class ruby::mysql {
  require ruby
  include ruby::params

  package { $ruby::params::ruby_mysql:
    ensure => installed,
  }

  exec {'gem_install_mysql':
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    command     => 'gem install mysql',
    refreshonly => true,
  }

  # include module name in motd
  os::motd::register { "ruby::mysql": }
}
