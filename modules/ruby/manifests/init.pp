# Class: ruby
#
# This class installs Ruby
#
# Parameters:
#
# Actions:
#   - Install Ruby
#   - Install Ruby Gems
#
# Requires:
#
# Sample Usage:
#
class ruby {
  include ruby::params

  package{ $ruby::params::ruby_packages:
     ensure => present,
  }


  # include module name in motd
  os::motd::register { "ruby": }
}
