# Class: ruby::params
#
# This class handles the Ruby module parameters
#
# Parameters:
#   $ruby_dev = the name of the Ruby development libraries
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class ruby::params {

    $ruby_dev      = "ruby-devel"
    $ruby_mysql    = "ruby-mysql"
    if $::operatingsystemrelease >= 6 {
        $ruby_packages = [
            "ruby-1.8.7.352",
            "ruby-libs",
            "ruby-rdoc",
            "ruby-ri",
            "ruby-irb",
            "rubygems",
            "ruby-augeas",
        ]
    } else{
        $ruby_packages = [
            "ruby-1.8.7.370",
            "ruby-libs",
            "ruby-rdoc",
            "ruby-ri",
            "ruby-irb",
            "rubygems",
            "ruby-augeas",
        ]
    }

}

