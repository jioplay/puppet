#
# the base Cloudera hadoop distribution is Cloudera Distribution for Hadoop:
# CDH3
#
# there are versions of the management tools:
# Cloudera Management Suite 3.5.2 - undef
# Cloudera Manager 3.7.1 - cloudera::manager
#

class cloudera {
  include cloudera::cdh3
  include cloudera::manager::client
}
