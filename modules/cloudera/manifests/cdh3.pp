class cloudera::cdh3 {

  $base_pkgs = [  'jdk' ,
      'hadoop-0.20' ,
      'hadoop-0.20-native.x86_64' ,
      'hadoop-0.20-sbin.x86_64' ,
      'hue-plugins' ,
      'hadoop-zookeeper' ,
      'hadoop-hbase' ,
      'oozie' ,
      'oozie-client' ,
      ]
  package { $base_pkgs:
    ensure  => latest ,
    require  => Class['yum::cdh3'] ,
    }
}
