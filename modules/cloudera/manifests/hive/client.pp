class cloudera::hive::client inherits cloudera::hive {

  file { '/etc/hive/conf/hive-site.xml':
    content  => template('cloudera/hive-site.xml.erb') ,
    ensure  => file ,
  }
}
