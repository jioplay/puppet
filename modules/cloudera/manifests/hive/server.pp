class cloudera::hive::server inherits cloudera::hive {

  include mysql::server

  mysql::server::mysqldb { "hive":
    user  => $hive_dbuser ,
    password  => $hive_dbpass ,
  }

  exec { 'create-hive-schema':
    path  => ["/bin", "/usr/bin"] ,
    unless  => "test `mysql -s -u${hive_dbuser} -p${hive_dbpass} -e 'show tables' hive|wc -l ` -gt 0" ,
    command  =>  "/usr/bin/mysql -u${hive_dbuser} -p${hive_dbpass} hive < /usr/lib/hive/scripts/metastore/upgrade/mysql/hive-schema-0.7.0.mysql.sql " ,
    require  => Mysql::Server::Mysqldb["hive"] ,
  }

# mysql acl crap, not working
# mysql::server::mysqldb makes mysql user@locahost
# user needs grant all and revoke alter,create
# user needs to connect remotely and localhost.
#  exec { 'fix_hive_grant':
#    refreshonly  => true ,
#    subscribe  => Exec['create_hive_schema'] ,
#    command    => "/usr/bin/mysql -u${hive_dbuser} -p${hive_dbpass} -h localhost -e \"REVOKE ALTER,CREATE ON hive.* FROM 'hive'@'%' \" " ,
#  }
}
