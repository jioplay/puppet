class cloudera::hive {
  include mysql::client
  $pkgs  = [  'hadoop-hive' ,
    ]

  $hive_dbuser  = 'hive'
  $hive_dbpass  = 'mysqlhive'
  $hive_dbhost  = 'h01p1.mobitv.corp'

  package { $pkgs:
    ensure  => latest ,
  }

  file { '/usr/lib/hive/lib/mysql-connector-java.jar':
    ensure  => '/usr/share/java/mysql-connector-java.jar' ,
  }
}
