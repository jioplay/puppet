class cloudera::manager::server {

  include cloudera::manager::client
  include mysql::server

  $cmf_dbuser = 'cmf'
  $cmf_dbpass = 'mysqlcmf'

  $pkgs = [  'cloudera-manager-daemons' ,
      'cloudera-manager-server' ,
      ]
  package { $pkgs:
    ensure  => latest ,
    require  => [  Class['cloudera::cdh3'] ,
      ]
    }

        service { 'cloudera-scm-server':
                name    => 'cloudera-scm-server',
                ensure  => running,
                enable  => true,
                subscribe       => File['cloudera-scm-server-conf'],
        }

        file { 'cloudera-scm-server-conf':
                path    => '/etc/cloudera-scm-server/db.properties' ,
                ensure  => file,
    owner  => 'cloudera-scm' ,
    mode  => 600 ,
                require => Package[$pkgs],
                source  => "puppet:///modules/cloudera/cloudera_scm_server/db.properties",
        }

  exec { 'create-cmf-db':
    unless  => "/usr/bin/mysql -u${cmf_dbuser} -p${cmf_dbpass} -e 'use cmf'" ,
    command  => "/usr/share/cmf/schema/scm_prepare_database.sh -p${mysql::server::mysql_password} mysql cmf ${cmf_dbuser} ${cmf_dbpass}" ,
    require => Service["mysqld"],
  }

        mysql::server::mysqldb { "activity_monitor":
                user    => "activity_monitor" ,
                password        => "mysqlactivity_monitor" ,
        }

        mysql::server::mysqldb { "service_monitor":
                user    => "service_monitor" ,
                password        => "mysqlservice_monitor" ,
        }

        mysql::server::mysqldb { "resource_manager":
                user    => "resource_manager" ,
                password        => "mysqlresource_manager" ,
        }


}
