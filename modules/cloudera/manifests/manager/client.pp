class cloudera::manager::client {

  $pkgs = [  'cloudera-manager-agent' ,
      'cloudera-manager-plugins' ,
      'hue-hadoop-auth-plugin' ,
      ]
  package { $pkgs:
    ensure  => latest ,
    require  => [  Class['cloudera::cdh3'] ,
      ]
    }

        service { 'cloudera-scm-agent':
                name    => 'cloudera-scm-agent',
                ensure  => running,
                enable  => true,
                subscribe       => File['cloudera-scm-agent-conf'],
        }

        file { 'cloudera-scm-agent-conf':
                path    => '/etc/cloudera-scm-agent/config.ini' ,
                ensure  => file,
                require => Package[$pkgs],
                source  => "puppet:///modules/cloudera/cloudera_scm_agent/config.ini",
        }


  # todo:
  # set server_host and server_port in /etc/cloudera_scm_agent/config.ini
  #
  # service def for cloudera_scm_agent
}
