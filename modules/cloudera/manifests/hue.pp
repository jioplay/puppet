# hue server

class cloudera::hue {

  $pkgs = [  'hue' ,
      'hue-about' ,
      'hue-beeswax' ,
      'hue-common' ,
      'hue-filebrowser' ,
      'hue-help' ,
      'hue-jobbrowser' ,
      'hue-jobsub' ,
      ## done in cloudera::cdh3 'hue_plugins' ,
      'hue-proxy' ,
      'hue-shell' ,
      'hue-useradmin' ,
      ]
  package { $pkgs:
    ensure  => latest ,
    require  => [  Class['cloudera::cdh3'] ,
      ]
    }
}
