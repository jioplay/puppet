class foreman::setpasswd {

  exec {'set_admin_passwd':
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    command     => "echo \"UPDATE users SET password_hash = 'ab364d022eacdee6f9aa0b53147dc49b758ddd2a', password_salt = '66ae15b04d783d3ff4e256b1a0c0eea0b046e939' WHERE id = 1\" | mysql -h $::foreman::foreman_dbserver -u puppet puppet -pTE1WeuHiflzmLMxi && /bin/touch /root/foreman_pass_done.txt",
    #command     => "echo \"UPDATE users SET password_hash = 'ab364d022eacdee6f9aa0b53147dc49b758ddd2a', password_salt = '66ae15b04d783d3ff4e256b1a0c0eea0b046e939' WHERE id = 1\" | mysql -h localhost -u puppet puppet -pm0b1r0ck5! && /bin/touch /root/foreman_pass_done.txt",
    #require     => Mysql::Grant["puppet_local"],
    #refreshonly => true,
    unless      => "/usr/bin/test -f /root/foreman_pass_done.txt",
  }
}
