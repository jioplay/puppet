class foreman::params {

  ### install.pp
  $package = "foreman"
  $package_ensure = "present"

  ### service.pp
  $service_ensure = "stopped"
  $service_enable = false

  ### config.pp
  $foreman_vip = "${::ipaddress}"
  $foreman_altip = 'UNDEFINED'
  $foreman_servername = "foreman.${::domain}"
  $foreman_serveralias = "foreman"
  $foreman_sslcertfile_fqdn = "puppet.${::domain}"
  $foreman_dbserver = "puppetdb.${::domain}"

  # Basic configurations
  # The following $foreman_url should be https, in order to send foreman report properly
  #  under apache+mod_proxy+balancer envoironment
  $foreman_url  = "https://foreman.${::domain}"
  # Should foreman act as an external node classifier (manage puppet class assignments)
  $enc          = true
  # Should foreman receive reports from puppet
  $reports      = true
  # Should foreman recive facts from puppet
  $facts        = true
  # Do you use storeconfig (and run foreman on the same database) ? (note: not required)
  $storeconfigs = false
  # should foreman manage host provisioning as well
  $unattended   = true
  # Enable users authentication (default user:admin pw:changeme)
  $authentication = true

  # force SSL (note: requires passenger)
  # This should be disable, but we manage the foreman ssl using mod_proxy and mod_rewrite
  # Using this option enabled and mod_proxy results infinity redirection loop.
  # 18 Apr 2012  Junichi/devops
  $ssl          = false

  # Advance configurations - no need to change anything here by default
  # allow usage of test / RC rpms as well
  $use_testing = true
  $railspath   = '/usr/share'
  $app_root    = "${railspath}/foreman"
  $user        = 'foreman'
  $environment = 'production'

  # OS specific paths
  $puppet_basedir  = '/usr/lib/ruby/site_ruby/1.8/puppet'
  $apache_conf_dir = '/etc/httpd/conf.d'
  $puppet_home = '/var/lib/puppet'
}

