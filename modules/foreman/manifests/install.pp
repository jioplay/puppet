class foreman::install {
  require passenger

  package { $::foreman::package:
    ensure  => $::foreman::package_ensure,
  }

  # certain portions of foreman require 1.1.0
  package { "rack":
    provider => gem,
    ensure   => "1.1.0"
  }

  # foreman::config requires patch command on system
  include packages
  realize(Package["patch"])

}
