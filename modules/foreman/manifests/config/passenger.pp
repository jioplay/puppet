class foreman::config::passenger {

  file {'foreman_vhost_frontend':
    path    => "${foreman::params::apache_conf_dir}/foreman-frontend.conf",
    content => template('foreman/foreman-frontend.conf.erb'),
    mode    => '0644',
    require => Package["httpd"],
    notify  => Service["httpd"],
  }

  file {'foreman_vhost_backend':
    path    => "${foreman::params::apache_conf_dir}/foreman-backend.conf",
    content => template('foreman/foreman-backend.conf.erb'),
    mode    => '0644',
    require => Package["httpd"],
    notify  => Service["httpd"],
  }

  exec {'restart_foreman':
    command     => "/bin/touch ${foreman::params::app_root}/tmp/restart.txt",
    refreshonly => true,
    cwd         => $foreman::params::app_root,
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
  }

  # passenger ~2.10 will not load the app if a config.ru doesn't exist in the app
  # root. Also, passenger will run as suid to the owner of the config.ru file.
  file { "$foreman::params::app_root/config.ru":
    ensure => link,
    owner  => $foreman::params::user,
    target => "${foreman::params::app_root}/vendor/rails/railties/dispatches/config.ru",
  }

  file { "$foreman::params::app_root/config/environment.rb":
    owner   => $foreman::params::user,
    require => Class['foreman::install'],
  }

}
