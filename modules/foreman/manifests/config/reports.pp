class foreman::config::reports {

  # foreman reporter
  file {"${foreman::params::puppet_basedir}/reports/foreman.rb":
    mode     => '0444',
    owner    => 'puppet',
    group    => 'puppet',
    content  => template('foreman/foreman-report.rb.erb'),
    require => Package["httpd"],
    notify  => Service["httpd"],
  }

  cron { 'expire_old_reports':
    command => "(/usr/local/bin/foreman_rack_version_reset.sh 1.2.5 1.1.0 && cd ${foreman::params::app_root} && RAILS_ENV=${foreman::params::environment} rake reports:expire; /usr/local/bin/foreman_rack_version_reset.sh 1.1.0 1.2.5)",
    minute  => '30',
    hour    => '7',
  }

  cron { 'daily summary':
    command => "(/usr/local/bin/foreman_rack_version_reset.sh 1.2.5 1.1.0 && cd ${foreman::params::app_root} && RAILS_ENV=${foreman::params::environment} rake reports:summarize; /usr/local/bin/foreman_rack_version_reset.sh 1.1.0 1.2.5)",
    minute  => '31',
    hour    => '7',
  }

}
