class foreman::service {
  service { $::foreman::service:
    ensure    => $::foreman::service_ensure,
    enable    => $::foreman::service_enabled,
    hasstatus => true,
    notify    => Service['httpd'],
  }
}
