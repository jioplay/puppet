class foreman (

  ### install.pp
  $package = $::foreman::params::package,
  $package_ensure = $::foreman::params::package_ensure,

  ### config.pp
  $foreman_vip = $::foreman::params::foreman_vip,
  $foreman_altip = $::foreman::params::foreman_altip,
  $foreman_servername = $::foreman::params::foreman_servername,
  $foreman_serveralias = $::foreman::params::foreman_serveralias,
  $foreman_sslcertfile_fqdn = $::foreman::params::foreman_sslcertfile_fqdn,
  $foreman_dbserver = $::foreman::params::foreman_dbserver,

  ### service.pp
  $service = $::foreman::params::service,
  $service_ensure = $::foreman::params::service_ensure,
  $service_enable = $::foreman::params::service_enable,

) inherits foreman::params {

  include foreman::install
  include foreman::config
  include foreman::service
  include foreman::setpasswd

  # include module name in motd
  os::motd::register { "foreman": }
}
