class foreman::config {

  include ruby::mysql

  Cron {
    require     => User[$foreman::params::user],
    user        => $foreman::params::user,
    environment => "RAILS_ENV=${foreman::params::environment}",
  }

  file {'/usr/share/foreman/vendor/rails/actionpack/lib':
    ensure  => directory,
    recurse => true,
    owner   => $foreman::params::user,
    group   => $foreman::params::user,
    require => User[$foreman::params::user],
  }

  file {'/etc/foreman/settings.yaml':
    content => template('foreman/settings.yaml.erb'),
    notify  => Class['foreman::service'],
    owner   => $foreman::params::user,
    group   => $foreman::params::user,
    require => User[$foreman::params::user],
  }

  file {'/etc/foreman/email.yaml':
    content => template('foreman/email.yaml.erb'),
    notify  => Class['foreman::service'],
    owner   => $foreman::params::user,
    group   => $foreman::params::user,
    require => User[$foreman::params::user],
  }

  # We need to manage this file in order to use rack-1.2.5
  # which is our puppetmaster uses and different version of what foreman rpm has,
  # which is 1.1.0
  #                    18 April 2012   junichi/devops
  file {'foreman_action_controller_rb':
    path    => '/usr/share/foreman/vendor/rails/actionpack/lib/action_controller.rb',
    source  => "puppet://$puppetserver/modules/foreman/action_controller.rb",
    notify  => Class['foreman::service'],
    owner   => $foreman::params::user,
    group   => $foreman::params::user,
    require => [User[$foreman::params::user],
                Exec['db_migrate']],
  }

  # We need to manage this file in order to use LDAP Auth
  # Since mobitv LDAP doesn't have 'mail' attr, the original ldap auth fails
  # Also the initial logged in user will have "Viewer" role
  # How to setup:
  #     1. Login Forman as 'admin' user
  #     2. Go to 'more' --> 'LDAP auth'
  #     3. Create new ldap source as follows
  #         Name:  mobitv
  #         Host:  ldapslave.smf1.mobitv
  #         Port:  389
  #         basedn: ou=People,dc=mobitv,dc=com
  #         Check 'On_the_fly user creation'
  #         Attr login:  uid
  #         Attr firstname:  givenName
  #         Attr lastname: sn
  #         Attr mail:  uid
  #
  #     20 April 2012   junichi/devops
  file {'foreman_app_models_user_rb':
    path    => '/usr/share/foreman/app/models/user.rb',
    source  => "puppet://$puppetserver/modules/foreman/user.rb",
    notify  => Service['httpd'],
    owner   => $foreman::params::user,
    group   => $foreman::params::user,
    require => User[$foreman::params::user],
  }

  # Fixed bug
  #   Problem:  No host report is shown when you click "Good Host Reports in the last 30 minutes" in dashboard screen
  #   How to solve:  Change from single quote to double quote of search string.
  #     e.g.
  #       Current:  last_report > '30 minutes ago' and status.enabled = true and status.interesting  = false
  #       New:      last_report > "30 minutes ago" and status.enabled = true and status.interesting  = false
  #
  #     23 April 2012   junichi/devops
  file {'app_views_dashboard_index.html.erb':
    path    => '/usr/share/foreman/app/views/dashboard/index.html.erb',
    source  => "puppet://$puppetserver/modules/foreman/app_views_dashboard_index.html.erb",
    notify  => Service['httpd'],
    owner   => $foreman::params::user,
    group   => $foreman::params::user,
    require => User[$foreman::params::user],
  }

  # Fixed bug
  #   Problem:  Foreman --> Statistics --> Hardware --> then click one of hardware types.
  #             It doesn't show anything because the mysql search is case_sensitive
  #   Fix:
  #     1) Apply "utf8_unicode_ci" CHAR SET
  #         mysql> alter table fact_values collate utf8_unicode_ci;
  #         mysql> alter table fact_values modify value text COLLATE utf8_unicode_ci;
  #
  #     2) Remove "BINARY" from mysql SEARCH sql
  #         Updated:  /usr/share/foreman/vendor/plugins/scoped_search/lib/scoped_search/query_builder.rb
  #
  #     27 April 2012   junichi/devops
  ###   file {'query_builder_rb':
  ###     path    => '/usr/share/foreman/vendor/plugins/scoped_search/lib/scoped_search/query_builder.rb',
  ###     source  => "puppet://$puppetserver/modules/foreman/query_builder.rb",
  ###     notify  => Service['httpd'],
  ###     owner   => $foreman::params::user,
  ###     require => User[$foreman::params::user],
  ###   }
  file {'file_patch_query_builder_rb':
    path    => '/root/query_builder.rb.patch',
    source  => "puppet://$puppetserver/modules/foreman/patches/query_builder.rb.patch",
    owner   => "root",
    group   => "root",
  }
  exec {'exec_patch_query_builder_rb':
    path    => ["/bin", "/usr/bin"] ,
    command => "cat /root/query_builder.rb.patch | patch -p0 /usr/share/foreman/vendor/plugins/scoped_search/lib/scoped_search/query_builder.rb",
   # onlyif  => "egrep -q \"\\\"#{SQL_OPERATORS\\\[operator\\\]} BINARY\\\"\" /usr/share/foreman/vendor/plugins/scoped_search/lib/scoped_search/query_builder.rb",
    onlyif  => 'egrep -q \'^\s+"#{SQL_OPERATORS\[operator\]} BINARY"\' /usr/share/foreman/vendor/plugins/scoped_search/lib/scoped_search/query_builder.rb',
    require => [File["file_patch_query_builder_rb"],User[$foreman::params::user]],
    notify  => Service['httpd'],
  }

  # rack version reset script
  file {'/usr/local/bin/foreman_rack_version_reset.sh':
    ensure  => present,
    source  => "puppet://$puppetserver/modules/foreman/foreman_rack_version_reset.sh",
    owner   => "root",
    group   => "root",
    mode    => 0755,
  }

  #Configure the Debian database with some defaults
  case $::operatingsystem {
    Debian,Ubuntu,CentOS: {
      file {'/etc/foreman/database.yml':
        content => template('foreman/database.yaml.erb'),
        notify  => [Class['foreman::service'],
                    Exec['db_migrate']],
        #Exec['gem_devel_install'],
        #Exec['gem_install_mysql'],
        owner   => $foreman::params::user,
        group   => $foreman::params::user,
        require => [User[$foreman::params::user],
                    Package['foreman']],
      }
    }
    default: { }
  }

  file { $foreman::params::app_root:
    ensure  => directory,
  }

  user { $foreman::params::user:
    ensure  => 'present',
    shell   => '/sbin/nologin',
    comment => 'Foreman',
    home    => $foreman::params::app_root,
    require => Class['foreman::install'],
  }

  #exec {'gem_devel_install':
  #  path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
  #  command     => 'yum _y install __disablerepo=mobi_thirdparty rubygems ruby_devel mysql_devel gcc make',
  #  refreshonly => true,
  #}

  ### Exec['gem_install_mysql'] has been moved to ruby::mysql
  ### exec {'gem_install_mysql':
  ### path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
  ### command     => 'gem install mysql',
  ### refreshonly => true,
  ### }
  exec {'db_migrate':
    cwd         => $foreman::params::app_root,
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    command     => '/usr/local/bin/foreman_rack_version_reset.sh 1.2.5 1.1.0 && rake db:migrate --trace 2>&1 | tee /tmp/foreman-db-migrate.log',
    environment => "RAILS_ENV=production",
    require     => File['/usr/local/bin/foreman_rack_version_reset.sh'],
    notify      => [Class['foreman::service'], Class['foreman::setpasswd']],
    refreshonly => true,
  }


  # cleans up the session entries in the database
  # if you are using fact or report importers, this creates a session per request
  # which can easily result with a lot of old and unrequired in your database
  # eventually slowing it down.
  cron{'clear_session_table':
    command => "(/usr/local/bin/foreman_rack_version_reset.sh 1.2.5 1.1.0 && cd ${foreman::params::app_root} && RAILS_ENV=${foreman::params::environment} rake db:sessions:clear; /usr/local/bin/foreman_rack_version_reset.sh 1.1.0 1.2.5)",
    minute  => '15',
    hour    => '23',
  }

  if $foreman::params::reports { include foreman::config::reports }
  if $foreman::params::enc     { include foreman::config::enc }
  include foreman::config::passenger
}
