#!/bin/bash

ORG_VER=$1
NEW_VER=$2

BASE="/usr/share/foreman/vendor/rails/actionpack/lib/action_controller.rb"
SED="/bin/sed"
CP="/bin/cp"
PID="$$"

# BACKUP
$CP $BASE ${BASE}.BACKUP

# Change rack version
$SED -i -e "s/^gem 'rack', '~> $ORG_VER'/gem 'rack', '~> $NEW_VER'/" $BASE

#echo "rack version is changed from $ORG_VER to $NEW_VER in $BASE"
