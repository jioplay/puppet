class php::devel (

  $devel_package = $::php::params::devel_package,
  $devel_package_ensure = $::php::params::devel_package_ensure,

) inherits php::params {
  include php

  package { $devel_package:
    ensure  => $devel_package_ensure,
  }

}
