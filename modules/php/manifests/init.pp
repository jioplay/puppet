class php {

  anchor { "php::begin": } ->
  class { "php::install": } ->
  anchor { "php::end": } ->

  # include module name in motd
  os::motd::register { "${name}": }
}


