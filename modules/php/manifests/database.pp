class php::database (

  $db_package = $::php::params::db_package,
  $db_package_ensure = $::php::params::db_package_ensure,

) inherits php::params {
  include php

  package { $db_package:
    ensure  => $db_package_ensure,
  }

}
