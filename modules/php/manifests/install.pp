class php::install (

  $package = $::php::params::package,
  $package_ensure = $::php::params::package_ensure,

) inherits php::params {

  package { $package:
    ensure  => $package_ensure,
    notify  => Service["httpd"],
    require => Class["apache"],
  }

}
