# Class: php::params
#
# This class handles the php module parameters
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class php::params {

  ### install.pp
  $package_ensure = "present"

  if $::operatingsystemrelease >= 6 {
    $package = ["php",
                "php-common",
                "php-pear",
                "php-cli",
                "php-zts",]
  } else {
    $package = ["php",
                "php-common",
                "php-pear",
                "php-cli",]
  }

  ### devel.pp
  $devel_package_ensure = "present"
  $devel_package = "php-devel"


  ### database.pp
  $db_package_ensure = "present"
  $db_package = ["php-pdo",
                 "php-pear-MDB2",
                 "php-pear-MDB2-Driver-mysql",
                 "php-mysql",]

}
