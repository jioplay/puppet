class mobi_aaa_dt_adapter_cus::config {

        file {
      "/opt/mobi-aaa/config/adapter-dt-cus/applicationProperties.xml":
              ensure => file,
               content => template('mobi_aaa_dt_adapter_cus/applicationProperties.xml.erb'),
           owner => "rtv",
        group => "rtv",
        mode => "755",
        notify => Class["tomcat::service"],
        require => Class["mobi_aaa_dt_adapter_cus::install"],
        }

}

