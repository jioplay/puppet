class mobi_aaa_dt_adapter_cus::install {

        package {
      "mobi-aaa-adapter-dt-cus-${mobi_aaa_dt_adapter_cus::adapter_cus_version}":
                ensure => present,
                notify =>[ Class["tomcat::service"], ]
        }

}

