# == class: mobi_aaa_dt_adapter_cus
#
#  install cus adapter
#
# === Parameters:
#    $adapter_cus_version::
#   $auth_oauth2_server_url::
#   $auth_rights_server_url
#   $dt_cus_url
#
# === Requires:
#
#    java
#    tomcat
#
# === Sample Usage
#
#
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_dt_adapter_cus(
    $adapter_cus_version = $mobi_aaa_dt_adapter_cus::params::adapter_cus_version,
  $auth_oauth2_server_url = $mobi_aaa_dt_adapter_cus::params::auth_oauth2_server_url,
  $auth_rights_server_url = $mobi_aaa_dt_adapter_cus::params::auth_rights_server_url,
  $dt_cus_url = $mobi_aaa_dt_adapter_cus::params::dt_cus_url,
)
inherits mobi_aaa_dt_adapter_cus::params {
    include mobi_aaa_dt_adapter_cus::install, mobi_aaa_dt_adapter_cus::config
    anchor { "mobi_aaa_dt_adapter_cus::begin": } -> Class["mobi_aaa_dt_adapter_cus::install"]
    Class["mobi_aaa_dt_adapter_cus::config"] -> anchor { "mobi_aaa_dt_adapter_cus::end" :}
    os::motd::register { $name : }
}

