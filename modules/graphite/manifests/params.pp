# Class: graphite::params
#
# This module manages graphite paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class graphite::params {

  ### install.pp
  $package_graphite = "graphite-web"
  $package_graphite_ensure = "present"

  $package_carbon = "carbon"
  $package_carbon_ensure = "present"

  $package_whisper = "whisper"
  $package_whisper_ensure = "present"


  ### service.pp
  $service_carbon_cache = "carbon-cache"
  $service_carbon_cache_ensure = "running"
  $service_carbon_cache_enable = "true"

  $service_carbon_aggregator = "carbon-aggregator"
  $service_carbon_aggregator_ensure = "running"
  $service_carbon_aggregator_enable = "true"

  $service_carbon_relay = "carbon-relay"
  $service_carbon_relay_ensure = "running"
  $service_carbon_relay_enable = "true"


  ### config.pp
  $config_carbon = "carbon.conf"
  $config_carbon_relay = "relay-rules.conf"
  $config_whisper = "storage-schemas.conf"

  $config_graphite_httpd = "graphite-web.conf"

  $sysconfig_carbon_cache = "carbon-cache.sysconfig"
  $sysconfig_carbon_aggregator = "carbon-aggregator.sysconfig"
  $sysconfig_carbon_relay = "carbon-relay.sysconfig"

}
