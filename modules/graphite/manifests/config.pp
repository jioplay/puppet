class graphite::config {

  File {
    ensure  => file,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => Class["graphite::install"],
  }

  file { "/etc/carbon/carbon.conf":
    source => "puppet://$puppetserver/modules/graphite/${::graphite::config_carbon}",
  }

  file { "/etc/carbon/relay-rules.conf":
    source => "puppet://$puppetserver/modules/graphite/${::graphite::config_carbon_relay}",
  }

  file { "/etc/carbon/storage-schemas.conf":
    source => "puppet://$puppetserver/modules/graphite/${::graphite::config_whisper}",
  }

  file { "/etc/sysconfig/carbon-cache":
    source => "puppet://$puppetserver/modules/graphite/${::graphite::sysconfig_carbon_cache}",
  }

  file { "/etc/sysconfig/carbon-aggregator":
    source => "puppet://$puppetserver/modules/graphite/${::graphite::sysconfig_carbon_aggregator}",
  }

  file { "/etc/sysconfig/carbon-relay":
    source => "puppet://$puppetserver/modules/graphite/${::graphite::sysconfig_carbon_relay}",
  }

  file { "/etc/httpd/conf.d/graphite-web.conf":
    source  => "puppet://$puppetserver/modules/graphite/${::graphite::config_graphite_httpd}",
    notify  => Service["httpd"],
    require => [ Class["apache"], Class["graphite::install"] ],
  }

}
