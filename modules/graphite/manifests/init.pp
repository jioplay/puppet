# Class: graphite
#
# This module manages graphite
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class graphite (

  ### install.pp
  $package_graphite = $graphite::params::package_graphite,
  $package_graphite_ensure = $graphite::params::package_graphite_ensure,

  $package_carbon = $graphite::params::package_carbon,
  $package_carbon_ensure = $graphite::params::package_carbon_ensure,

  $package_whisper = $graphite::params::package_whisper,
  $package_whisper_ensure = $graphite::params::package_whisper_ensure,


  ### service.pp
  $service_carbon_cache = $graphite::params::service_carbon_cache,
  $service_carbon_cache_ensure = $graphite::params::service_carbon_cache_ensure,
  $service_carbon_cache_enable = $graphite::params::service_carbon_cache_enable,

  $service_carbon_aggregator = $graphite::params::service_carbon_aggregator,
  $service_carbon_aggregator_ensure = $graphite::params::service_carbon_aggregator_ensure,
  $service_carbon_aggregator_enable = $graphite::params::service_carbon_aggregator_enable,

  $service_carbon_relay = $graphite::params::service_carbon_relay,
  $service_carbon_relay_ensure = $graphite::params::service_carbon_relay_ensure,
  $service_carbon_relay_enable = $graphite::params::service_carbon_relay_enable,


  ### config.pp
  $config_carbon = $graphite::params::config_carbon,
  $config_carbon_relay = $graphite::params::config_carbon_relay,
  $config_whisper = $graphite::params::config_whisper,

  $config_graphite_httpd = $graphite::params::config_graphite_httpd,

  $sysconfig_carbon_cache = $graphite::params::sysconfig_carbon_cache,
  $sysconfig_carbon_aggregator = $graphite::params::sysconfig_carbon_aggregator,
  $sysconfig_carbon_relay = $graphite::params::sysconfig_carbon_relay,

) inherits graphite::params {

  anchor { "graphite::begin": } ->
  class { "graphite::install": } ->
  class { "graphite::config": } ->
  class { "graphite::service": } ->
  anchor { "graphite::end": }

  # include module name in motd
  os::motd::register { "graphite": }
}
