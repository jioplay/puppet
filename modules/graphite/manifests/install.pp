class graphite::install {

  package { $package_graphite:
    ensure  => $package_graphite_ensure,
    require => Class["apache"],
  }

  package { $package_carbon:
    ensure => $package_carbon_ensure,
  }

  package { $package_whisper:
    ensure => $package_whisper_ensure,
  }

}
