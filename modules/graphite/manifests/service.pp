class graphite::service {

  service { $service_carbon_cache:
    ensure      => $service_carbon_cache_ensure,
    enable      => $service_carbon_cache_enable,
    hasstatus   => true,
    hasrestart  => true,
    subscribe => Class[graphite::config],
  }

  service { $service_carbon_aggregator:
    ensure      => $service_carbon_aggregator_ensure,
    enable      => $service_carbon_aggregator_enable,
    hasstatus   => true,
    hasrestart  => true,
    subscribe => Class[graphite::config],
  }

  service { $service_carbon_relay:
    ensure      => $service_carbon_relay_ensure,
    enable      => $service_carbon_relay_enable,
    hasstatus   => true,
    hasrestart  => true,
    subscribe => Class[graphite::config],
  }

}
