# == Class: mobi_recording_manager
#
#  installs recording manager component
#
# === Parameters:
#
#    $mobi_recording_manager_version::
#    $solr_master_url::
#    $legacy_solr_master_url::
#    $update_legacy_solr::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_recording_manager" :
#           version => "5.1.0-223756",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_recording_manager (
    ###icinga.pp
    $icinga = $mobi_recording_manager::params::icinga,
    $icinga_instance = $mobi_recording_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_recording_manager::params::icinga_cmd_args,
    ###end icinga.pp
    $mobi_recording_manager_version = $mobi_recording_manager::params::mobi_recording_manager_version,
    $vid_carrier = $mobi_recording_manager::params::vid_carrier,
    $vid_product = $mobi_recording_manager::params::vid_product,
    $vid_version = $mobi_recording_manager::params::vid_version,
    $vid_guideprovider = $mobi_recording_manager::params::vid_guideprovider,
    $default_media_type = $mobi_recording_manager::params::default_media_type,
    $default_media_type_storage = $mobi_recording_manager::params::default_media_type_storage,
    $series_recording_limited_to_single_channel = $mobi_recording_manager::params::series_recording_limited_to_single_channel,
    $max_recording_time_for_tstv_in_minutes = $mobi_recording_manager::params::max_recording_time_for_tstv_in_minutes,
    $is_auth_enabled = $mobi_recording_manager::params::is_auth_enabled,
    $is_carrier_system_enabled = $mobi_recording_manager::params::is_carrier_system_enabled,
    $is_shared_recording_enabled = $mobi_recording_manager::params::is_shared_recording_enabled,
    $shared_account_id = $mobi_recording_manager::params::shared_account_id,
    $default_storage_size_inminutes = $mobi_recording_manager::params::default_storage_size_inminutes,
    $adapter_recording_provider_carrier = $mobi_recording_manager::params::adapter_recording_provider_carrier,
    $recording_auth_service_name = $mobi_recording_manager::params::recording_auth_service_name,
    $recording_carrier_auth_service_name = $mobi_recording_manager::params::recording_carrier_auth_service_name,
    $guide_manager_url = $mobi_recording_manager::params::guide_manager_url,
    $storage_manager_url = $mobi_recording_manager::params::storage_manager_url,
    $solr_master_url = $mobi_recording_manager::params::solr_master_url,
    $amq_broker_recordings_url = $mobi_recording_manager::params::amq_broker_recordings_url,
    $amq_broker_netpvr_url = $mobi_recording_manager::params::amq_broker_netpvr_url,
    $amq_recordings_core_pool_size = $mobi_recording_manager::params::amq_recordings_core_pool_size,
    $amq_recordings_max_pool_size = $mobi_recording_manager::params::amq_recordings_max_pool_size,
    $amq_recordings_task_queue_capacity = $mobi_recording_manager::params::amq_recordings_task_queue_capacity,
    $identity_manager_url = $mobi_recording_manager::params::identity_manager_url,
    $rights_manager_url = $mobi_recording_manager::params::rights_manager_url,
    $max_retention_period = $mobi_recording_manager::params::max_retention_period,
    $max_catchup_period = $mobi_recording_manager::params::max_catchup_period,
    $is_quarantine_enabled = $mobi_recording_manager::params::is_quarantine_enabled,
    $quarantine_recording_max_period_in_days = $mobi_recording_manager::params::quarantine_recording_max_period_in_days,
    $is_tstv_enabled = $mobi_recording_manager::params::is_tstv_enabled,
    $tstv_recording_max_period_in_hours = $mobi_recording_manager::params::tstv_recording_max_period_in_hours,
    $recording_start_buffer = $mobi_recording_manager::params::recording_start_buffer,
    $solr_master_timeout = $mobi_recording_manager::params::solr_master_timeout,
    $threshold_time_inteval = $mobi_recording_manager::params::threshold_time_inteval,
    $webclient_featurePojoMapping = $mobi_recording_manager::params::webclient_featurePojoMapping,
    $webclient_logging = $mobi_recording_manager::params::webclient_logging,
    $webclient_connectionTimeout = $mobi_recording_manager::params::webclient_connectionTimeout,
    $webclient_socketTimeout = $mobi_recording_manager::params::webclient_socketTimeout,
    $webclient_defaultMaxPerRoute = $mobi_recording_manager::params::webclient_defaultMaxPerRoute,
    $webclient_maxTotal = $mobi_recording_manager::params::webclient_maxTotal,
    )

inherits mobi_recording_manager::params {
    include mobi_recording_manager::install, mobi_recording_manager::config
    anchor { "mobi_recording_manager::begin": } -> Class["mobi_recording_manager::install"] ->
    Class["mobi_recording_manager::config"] ->
    class { "mobi_recording_manager::icinga":} ->
    anchor { "mobi_recording_manager::end" :} -> os::motd::register { $name : }
}
