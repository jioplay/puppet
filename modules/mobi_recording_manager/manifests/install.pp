class mobi_recording_manager::install {
  package { "mobi-recording-manager":
    ensure => "${mobi_recording_manager::mobi_recording_manager_version}",
    notify => Class["tomcat::service"],
  }
}
