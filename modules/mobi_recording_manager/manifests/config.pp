class mobi_recording_manager::config {
    file { "/opt/mobitv/conf/mobi-recording-manager/recording-manager.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_recording_manager/recording-manager.properties.erb'),
        require => Class["mobi_recording_manager::install"],
        notify   => Class["tomcat::service"],
    }
}
