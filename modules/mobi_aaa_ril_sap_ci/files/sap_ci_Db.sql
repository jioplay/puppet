CREATE DATABASE IF NOT EXISTS sap_ci CHARACTER SET=utf8;

CREATE USER 'sap_ci_user'@'localhost' IDENTIFIED BY 'sap_ci_user';

GRANT Select ON purchase_manager.* TO 'sap_ci_user'@'%';

GRANT Select ON rights_manager.* TO 'sap_ci_user'@'%';

GRANT USAGE ON *.* TO 'sap_ci_user'@'%' IDENTIFIED BY 'sap_ci_user';

GRANT SELECT, INSERT, UPDATE, DELETE ON sap_ci.* TO 'sap_ci_user'@'%';

FLUSH PRIVILEGES;

use sap_ci;
# In your Quartz properties file, you will need to set 
# org.quartz.jobStore.driverDelegateClass = org.quartz.impl.jdbcjobstore.StdJDBCDelegate

DROP TABLE IF EXISTS QRTZ_JOB_LISTENERS;
DROP TABLE IF EXISTS QRTZ_TRIGGER_LISTENERS;
DROP TABLE IF EXISTS QRTZ_FIRED_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_PAUSED_TRIGGER_GRPS;
DROP TABLE IF EXISTS QRTZ_SCHEDULER_STATE;
DROP TABLE IF EXISTS QRTZ_LOCKS;
DROP TABLE IF EXISTS QRTZ_SIMPLE_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_CRON_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_BLOB_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_JOB_DETAILS;
DROP TABLE IF EXISTS QRTZ_CALENDARS;

DROP TABLE IF EXISTS DAILY_EXECUTION;
DROP TABLE IF EXISTS BATCH_EXECUTION;
DROP TABLE IF EXISTS REVENUE_RECOGNITION_LOG;

CREATE TABLE QRTZ_JOB_DETAILS(
	JOB_NAME VARCHAR(200) NOT NULL,
	JOB_GROUP VARCHAR(200) NOT NULL,
	DESCRIPTION VARCHAR(250) NULL,
	JOB_CLASS_NAME VARCHAR(250) NOT NULL,
	IS_DURABLE VARCHAR(1) NOT NULL,
	IS_VOLATILE VARCHAR(1) NOT NULL,
	IS_STATEFUL VARCHAR(1) NOT NULL,
	REQUESTS_RECOVERY VARCHAR(1) NOT NULL,
	JOB_DATA BLOB NULL,
	PRIMARY KEY (JOB_NAME,JOB_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_JOB_LISTENERS (
	JOB_NAME VARCHAR(200) NOT NULL,
	JOB_GROUP VARCHAR(200) NOT NULL,
	JOB_LISTENER VARCHAR(200) NOT NULL,
	PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER),
	INDEX (JOB_NAME, JOB_GROUP),
	FOREIGN KEY (JOB_NAME,JOB_GROUP)
	REFERENCES QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_TRIGGERS (
	TRIGGER_NAME VARCHAR(200) NOT NULL,
	TRIGGER_GROUP VARCHAR(200) NOT NULL,
	JOB_NAME VARCHAR(200) NOT NULL,
	JOB_GROUP VARCHAR(200) NOT NULL,
	IS_VOLATILE VARCHAR(1) NOT NULL,
	DESCRIPTION VARCHAR(250) NULL,
	NEXT_FIRE_TIME BIGINT(13) NULL,
	PREV_FIRE_TIME BIGINT(13) NULL,
	PRIORITY INTEGER NULL,
	TRIGGER_STATE VARCHAR(16) NOT NULL,
	TRIGGER_TYPE VARCHAR(8) NOT NULL,
	START_TIME BIGINT(13) NOT NULL,
	END_TIME BIGINT(13) NULL,
	CALENDAR_NAME VARCHAR(200) NULL,
	MISFIRE_INSTR SMALLINT(2) NULL,
	JOB_DATA BLOB NULL,
	PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
	INDEX (JOB_NAME, JOB_GROUP),
	FOREIGN KEY (JOB_NAME,JOB_GROUP)
	REFERENCES QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_SIMPLE_TRIGGERS (
	TRIGGER_NAME VARCHAR(200) NOT NULL,
	TRIGGER_GROUP VARCHAR(200) NOT NULL,
	REPEAT_COUNT BIGINT(7) NOT NULL,
	REPEAT_INTERVAL BIGINT(12) NOT NULL,
	TIMES_TRIGGERED BIGINT(10) NOT NULL,
	PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
	INDEX (TRIGGER_NAME, TRIGGER_GROUP),
	FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
	REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_CRON_TRIGGERS (
	TRIGGER_NAME VARCHAR(200) NOT NULL,
	TRIGGER_GROUP VARCHAR(200) NOT NULL,
	CRON_EXPRESSION VARCHAR(120) NOT NULL,
	TIME_ZONE_ID VARCHAR(80),
	PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
	INDEX (TRIGGER_NAME, TRIGGER_GROUP),
	FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
	REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_BLOB_TRIGGERS (
	TRIGGER_NAME VARCHAR(200) NOT NULL,
	TRIGGER_GROUP VARCHAR(200) NOT NULL,
	BLOB_DATA BLOB NULL,
	PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
	INDEX (TRIGGER_NAME, TRIGGER_GROUP),
	FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
	REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_TRIGGER_LISTENERS (
	TRIGGER_NAME VARCHAR(200) NOT NULL,
	TRIGGER_GROUP VARCHAR(200) NOT NULL,
	TRIGGER_LISTENER VARCHAR(200) NOT NULL,
	PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER),
	INDEX (TRIGGER_NAME, TRIGGER_GROUP),
	FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
	REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_CALENDARS (
	CALENDAR_NAME VARCHAR(200) NOT NULL,
	CALENDAR BLOB NOT NULL,
	PRIMARY KEY (CALENDAR_NAME)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_PAUSED_TRIGGER_GRPS (
	TRIGGER_GROUP VARCHAR(200) NOT NULL,
	PRIMARY KEY (TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_FIRED_TRIGGERS (
	ENTRY_ID VARCHAR(95) NOT NULL,
	TRIGGER_NAME VARCHAR(200) NOT NULL,
	TRIGGER_GROUP VARCHAR(200) NOT NULL,
	IS_VOLATILE VARCHAR(1) NOT NULL,
	INSTANCE_NAME VARCHAR(200) NOT NULL,
	FIRED_TIME BIGINT(13) NOT NULL,
	PRIORITY INTEGER NOT NULL,
	STATE VARCHAR(16) NOT NULL,
	JOB_NAME VARCHAR(200) NULL,
	JOB_GROUP VARCHAR(200) NULL,
	IS_STATEFUL VARCHAR(1) NULL,
	REQUESTS_RECOVERY VARCHAR(1) NULL,
	PRIMARY KEY (ENTRY_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_SCHEDULER_STATE (
	INSTANCE_NAME VARCHAR(200) NOT NULL,
	LAST_CHECKIN_TIME BIGINT(13) NOT NULL,
	CHECKIN_INTERVAL BIGINT(13) NOT NULL,
	PRIMARY KEY (INSTANCE_NAME)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_LOCKS (
	LOCK_NAME VARCHAR(40) NOT NULL,
	PRIMARY KEY (LOCK_NAME)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO QRTZ_LOCKS values('TRIGGER_ACCESS');
INSERT INTO QRTZ_LOCKS values('JOB_ACCESS');
INSERT INTO QRTZ_LOCKS values('CALENDAR_ACCESS');
INSERT INTO QRTZ_LOCKS values('STATE_ACCESS');
INSERT INTO QRTZ_LOCKS values('MISFIRE_ACCESS');

CREATE TABLE sap_ci.DAILY_EXECUTION_LOG (
  DAILY_EXECUTION_ID bigint(22) NOT NULL AUTO_INCREMENT,
  RECORD_CREATION_DATE datetime NOT NULL,
  PROCESSED_STATUS varchar(20) NOT NULL,
  RECORDTYPE varchar(10) NOT NULL,
  CREATED_DATE datetime NOT NULL,
  PRIMARY KEY `PK_DAILY_EXECUTION` (DAILY_EXECUTION_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE sap_ci.BATCH_EXECUTION_LOG (
  BATCH_EXECUTION_ID bigint(22) NOT NULL AUTO_INCREMENT,
  DAILY_EXECUTION_ID bigint(22) NOT NULL,
  RECORD_CREATION_DATE datetime NOT NULL,
  PROCESSED_STATUS varchar(20) NOT NULL,
  RECORDTYPE varchar(10) NOT NULL,
  BATCHNO tinyint NOT NULL,
  START_OFFSET bigint(22) DEFAULT NULL,
  END_OFFSET bigint(22) DEFAULT NULL,
  CREATED_DATE datetime NOT NULL,
  PRIMARY KEY `PK_BATCH_EXECUTION` (BATCH_EXECUTION_ID),
  KEY `FK_BATCH_EXECUTION_01` (DAILY_EXECUTION_ID),
  CONSTRAINT `FK_BATCH_EXECUTION_01` FOREIGN KEY (DAILY_EXECUTION_ID) REFERENCES DAILY_EXECUTION_LOG (DAILY_EXECUTION_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE sap_ci.REVENUE_RECOGNITION_LOG (
  REVENUE_RECOGNITION_ID bigint(22) NOT NULL AUTO_INCREMENT,
  SUBSCRIPTION_ID bigint(22) NOT NULL,
  RECOGNITION_DATE datetime NOT NULL,
  REVENUE_REALIZING_EVENT varchar(50) NOT NULL,
  RECOGNIZED_AMOUNT  decimal(22,2) DEFAULT NULL,
  PRIMARY KEY `PK_REVENUE_RECOGNITION` (REVENUE_RECOGNITION_ID),
  INDEX `IND_SUBSCRIPTION_ID` (SUBSCRIPTION_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

commit; 
