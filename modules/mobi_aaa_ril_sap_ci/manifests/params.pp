class mobi_aaa_ril_sap_ci::params {
   $sap_ci_version = ""
   $package_name = "mobi-aaa-ril-sap-ci-adapter"

   $offer_manager_endpoint_url = "http://offermanagervip:8080/mobi-aaa-ril-offer-manager"
   $database_driver = "com.mysql.jdbc.Driver"
   $purchase_database_url = "jdbc:mysql://mydtbvip:3306/purchase_manager"
   $rights_database_url = "jdbc:mysql://mydtbvip:3306/rights_manager"
   $batch_database_url = "jdbc:mysql://mydtbvip:3306/sap_ci"
   $database_username = "sap_ci_user"
   $database_password = "sap_ci_user"

   $jersey_restclient_connectionTimeout = "30"
   $jersey_restclient_readTimeout = "30"
   $jersey_restclient_maxConnections = "300"
   $jersey_restclient_enableClientLog = "false"

   $sapci_generate_payment_record_start_cron = undef
   $sapci_generate_revenue_record_start_cron = undef
   $pdr_paymentChannel_mobiApp = "MOBI001"
   $pdr_paymentChannel_selfcare = "MOBI002"
   $pdr_paymentMethod = "M3"
   $pdr_merchantId = "1011012"
   $pdr_terminalId = "video_application"
   $record_sourceId = "mobi-sap-ci-adapter"

   $max_retries = "2"
   $batch_size = undef
   $batch_execution_count_perday = undef
   $shared_filesystem_path = "/var/mobi-sap-ci-data-shared/"
   $pdr_base_dir_name = "PDR"
   $rdr_base_dir_name = "RDR"
   $default_past_record_creation_window = undef
}
