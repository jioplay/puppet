## == Class: mobi_aaa_ril_sap_ci
#
# Manage the  mobi_aaa_ril_sap_ci component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_aaa_ril_sap_ci (
   $sap_ci_version = $mobi_aaa_ril_sap_ci::params::sap_ci_version,
   $package_name = $mobi_aaa_ril_sap_ci::params::package_name,
   $offer_manager_endpoint_url = $mobi_aaa_ril_sap_ci::params::offer_manager_endpoint_url,
   $database_driver = $mobi_aaa_ril_sap_ci::params::database_driver,
   $purchase_database_url = $mobi_aaa_ril_sap_ci::params::purchase_database_url,
   $rights_database_url = $mobi_aaa_ril_sap_ci::params::rights_database_url,
   $batch_database_url = $mobi_aaa_ril_sap_ci::params::batch_database_url,
   $database_username = $mobi_aaa_ril_sap_ci::params::database_username,
   $database_password = $mobi_aaa_ril_sap_ci::params::database_password,
   $jersey_restclient_connectionTimeout = $mobi_aaa_ril_sap_ci::params::jersey_restclient_connectionTimeout,
   $jersey_restclient_readTimeout = $mobi_aaa_ril_sap_ci::params::jersey_restclient_readTimeout,
   $jersey_restclient_maxConnections = $mobi_aaa_ril_sap_ci::params::jersey_restclient_maxConnections,
   $jersey_restclient_enableClientLog = $mobi_aaa_ril_sap_ci::params::jersey_restclient_enableClientLog,
   $sapci_generate_payment_record_start_cron = $mobi_aaa_ril_sap_ci::params::sapci_generate_payment_record_start_cron,
   $sapci_generate_revenue_record_start_cron = $mobi_aaa_ril_sap_ci::params::sapci_generate_revenue_record_start_cron,
   $pdr_paymentChannel_mobiApp = $mobi_aaa_ril_sap_ci::params::pdr_paymentChannel_mobiApp,
   $pdr_paymentChannel_selfcare = $mobi_aaa_ril_sap_ci::params::pdr_paymentChannel_selfcare,
   $pdr_paymentMethod = $mobi_aaa_ril_sap_ci::params::pdr_paymentMethod,
   $pdr_merchantId = $mobi_aaa_ril_sap_ci::params::pdr_merchantId,
   $pdr_terminalId = $mobi_aaa_ril_sap_ci::params::pdr_terminalId,
   $record_sourceId = $mobi_aaa_ril_sap_ci::params::record_sourceId,
   $max_retries = $mobi_aaa_ril_sap_ci::params::max_retries,
   $batch_size = $mobi_aaa_ril_sap_ci::params::batch_size,
   $batch_execution_count_perday = $mobi_aaa_ril_sap_ci::params::batch_execution_count_perday,
   $shared_filesystem_path = $mobi_aaa_ril_sap_ci::params::shared_filesystem_path,
   $pdr_base_dir_name = $mobi_aaa_ril_sap_ci::params::pdr_base_dir_name,
   $rdr_base_dir_name = $mobi_aaa_ril_sap_ci::params::rdr_base_dir_name,
   $default_past_record_creation_window = $mobi_aaa_ril_sap_ci::params::default_past_record_creation_window,
) inherits mobi_aaa_ril_sap_ci::params {

    anchor { "mobi_aaa_ril_sap_ci::begin": } ->
    class { "mobi_aaa_ril_sap_ci::install": } ->
    class { "mobi_aaa_ril_sap_ci::config": } ->
    anchor { "mobi_aaa_ril_sap_ci::end": }

    os::motd::register{ "${name}":}
}
