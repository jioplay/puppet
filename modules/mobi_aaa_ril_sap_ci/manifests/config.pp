class mobi_aaa_ril_sap_ci::config {
   $offer_manager_endpoint_url = $mobi_aaa_ril_sap_ci::offer_manager_endpoint_url
   $database_driver = $mobi_aaa_ril_sap_ci::database_driver
   $purchase_database_url = $mobi_aaa_ril_sap_ci::purchase_database_url
   $rights_database_url = $mobi_aaa_ril_sap_ci::rights_database_url
   $batch_database_url = $mobi_aaa_ril_sap_ci::batch_database_url
   $database_username = $mobi_aaa_ril_sap_ci::database_username
   $database_password = $mobi_aaa_ril_sap_ci::database_password

   $jersey_restclient_connectionTimeout = $mobi_aaa_ril_sap_ci::jersey_restclient_connectionTimeout
   $jersey_restclient_readTimeout = $mobi_aaa_ril_sap_ci::jersey_restclient_readTimeout
   $jersey_restclient_maxConnections = $mobi_aaa_ril_sap_ci::jersey_restclient_maxConnections
   $jersey_restclient_enableClientLog = $mobi_aaa_ril_sap_ci::jersey_restclient_enableClientLog

   $sapci_generate_payment_record_start_cron = $mobi_aaa_ril_sap_ci::sapci_generate_payment_record_start_cron
   $sapci_generate_revenue_record_start_cron = $mobi_aaa_ril_sap_ci::sapci_generate_revenue_record_start_cron
   $pdr_paymentChannel_mobiApp = $mobi_aaa_ril_sap_ci::pdr_paymentChannel_mobiApp
   $pdr_paymentChannel_selfcare = $mobi_aaa_ril_sap_ci::pdr_paymentChannel_selfcare
   $pdr_paymentMethod = $mobi_aaa_ril_sap_ci::pdr_paymentMethod
   $pdr_merchantId = $mobi_aaa_ril_sap_ci::pdr_merchantId
   $pdr_terminalId = $mobi_aaa_ril_sap_ci::pdr_terminalId
   $record_sourceId = $mobi_aaa_ril_sap_ci::record_sourceId

   $max_retries = $mobi_aaa_ril_sap_ci::max_retries
   $batch_size = $mobi_aaa_ril_sap_ci::batch_size
   $batch_execution_count_perday = $mobi_aaa_ril_sap_ci::batch_execution_count_perday
   $shared_filesystem_path = $mobi_aaa_ril_sap_ci::shared_filesystem_path
   $pdr_base_dir_name = $mobi_aaa_ril_sap_ci::pdr_base_dir_name
   $rdr_base_dir_name = $mobi_aaa_ril_sap_ci::rdr_base_dir_name
   $default_past_record_creation_window = $mobi_aaa_ril_sap_ci::default_past_record_creation_window

  file { "/opt/mobi-aaa-ril-sap-ci-adapter/conf/application-config-override.xml":
    ensure => present,
    content => template("mobi_aaa_ril_sap_ci/application-config-override.xml.erb"),
    require => Class["mobi_aaa_ril_sap_ci::install"],
    notify => Class["tomcat::service"],
  }
}
