class mobi_aaa_ril_sap_ci::install {
    package {"${mobi_aaa_ril_sap_ci::package_name}-${mobi_aaa_ril_sap_ci::sap_ci_version}":
        ensure => present,
        require => [ Class["tomcat"],],
        notify => Class["tomcat::service"],
    }
}
