define network::route_add(
  $netmask = undef,
  $default_gateway = undef,
  $interface = undef
){

  include network

  if ! $netmask { fail("Error in ${name}, \$netmask parameter not defined") }
  if ! $default_gateway { fail("Error in ${name}, \$default_gateway parameter not defined") }
  if ! $interface { fail("Error in ${name}, \$interface parameter not defined") }

  $file = "/etc/sysconfig/network-scripts/route-${interface}"
  $route = "${netmask} via ${default_gateway} dev ${interface}"
  $netmask_escaped = regsubst($netmask,'/','\/')

  exec{"add_route ${netmask} ${default_gateway} ${interface}":
    command     => "sed -i.bak '/${netmask_escaped}/d' ${file}; echo ${route} >> ${file}",
    path        => ['usr/local/bin','opt/local/bin','/usr/bin','/usr/sbin','/bin','/sbin'],
    unless      => "/bin/bash -c \" grep -q '${route}' ${file} && test `grep -c '^${netmask}' ${file} -lt 2 \"",
    notify => Class["network"],
  }
}
