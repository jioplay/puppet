class network::default_gateway (
  $file = "/etc/sysconfig/network",
  $gateway = undef,
){
  include network

  if ! $gateway {
    fail("Error in $name class \$gateway parameter not defined")
  }

  exec{"add_gateway":
    command => "sed -i.bak '/^GATEWAY/d' ${file} && echo GATEWAY=${gateway} >> ${file}",
    path    => ['usr/local/bin','opt/local/bin','/usr/bin','/usr/sbin','/bin','/sbin'],
    unless  => "grep 'GATEWAY=${gateway}' ${file}",
    notify  => Class["network"],
  }

}
