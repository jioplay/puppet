class network::static_network {
    file { 'ifcfg-eth0':
        ensure  => file,
        path    => '/etc/sysconfig/network-scripts/ifcfg-eth0',
        owner   => root,
        group   => root,
        mode    => "0544",
        content => template('network/ifcfg-eth0-static.erb'),
        require => Class['network'],
        notify  => Class['network'],
    }
}
