class network {
  service {"network":
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
