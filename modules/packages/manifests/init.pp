# Class: packages
#
# This module manages packages
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class packages {

  # if a package has a daemon or is in anyway complicated then create a module!
  $packages = [
               "Twisted",
               "automake",
               "binutils",
               "binutils.x86_64",
               "bison",
               "compat-db.i386",
               "compat-glibc.i386",
               "compat-libcap1.x86_64",
               "compat-libgcc-296.i386",
               "compat-libstdc++-296.i386",
               "compat-libstdc++-33.i386",
               "compat-libstdc++-33.i686",
               "compat-libstdc++-33.x86_64",
               "emacs",
               "flex",
               "gcc",
               "gcc-c++",
               "gcc-c++.x86_64",
               "gcc.x86_64",
               "gettext",
               "git",
               "glibc-devel.i386",
               "glibc-devel.i686",
               "glibc-devel.x86_64",
               "glibc.i686",
               "glibc.x86_64",
               "kernel-devel",
               "ksh",
               "libXtst.i386",
               "libXtst.x86_64",
               "libaio-devel.i686",
               "libaio-devel.x86_64",
               "libaio.i686",
               "libaio.x86_64",
               "libcurl-devel",
               "libgcc.i386",
               "libgcc.i686",
               "libgcc.x86_64",
               "libstdc++-devel.i686",
               "libstdc++-devel.x86_64",
               "libstdc++.i386",
               "libstdc++.i686",
               "libstdc++.x86_64",
               "libtool",
               "libxml2-devel",
               "make",
               "mobi-ant",
               "mobi-logrotate-ods-config",
               "nmap",
               "openssl-devel",
               "p4cli",
               "p4v",
               "patch",
               "pkgconfig",
               "qgit",
               "redhat-rpm-config",
               "rpm-build",
               "rpm-helper",
               "rsync",
               "strace",
               "subversion",
               "sysstat.x86_64",
               "urwid",
               "wget",
               "zlib-devel",
               ]

  packages::install { $packages: }

}
