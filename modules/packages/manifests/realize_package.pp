define packages::realize_package {
  if defined(Package[$name]){
    realize(Package[$name])
  }
  else{
    @package { $name:
      ensure => "present",
    }
    realize(Package[$name])
  }
}
