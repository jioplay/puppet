define packages::install {
  @package { $name:
      ensure => "present",
  }
}
