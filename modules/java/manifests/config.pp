class java::config {

  File {
    ensure  => file,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => Class["java::install"],
  }

  # check if we should use the unlimited strength JCE jar files
  # Note: these files are for jdk 6 only. Will need to refactor when we move to jdk7
  if $::java::use_unlimited_strength_jce {
    # replace the following two files in the jdk install
    file { "/usr/java/latest/jre/lib/security/local_policy.jar":
      ensure => present,
      source => "puppet://$puppetserver/modules/java/jdk6/jce/local_policy.jar",
    }
    file { "/usr/java/latest/jre/lib/security/US_export_policy.jar":
      ensure => present,
      source => "puppet://$puppetserver/modules/java/jdk6/jce/US_export_policy.jar",
    }
  }

}
