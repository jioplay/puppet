class java::install {

    package { "gcj-java":
        ensure => purged,
    }

    package { "java-1.4.2-gcj-compat":
        ensure => purged,
    }

    if $java::purge_jce
    {
        package { "mobi-jce":
            ensure => absent ,
        }
    }

    package { "java-install":
        name => "$java::jdk_pkg",
        ensure => $java::jdk_ver,
        require => [ Package["gcj-java"], Package["java-1.4.2-gcj-compat"], ]
    }

}
