class java::params {
    $jdk_pkg = "jdk"
    $jdk_ver = "1.6.0_06-fcs"
    $use_unlimited_strength_jce = true
    $purge_jce = true
}
