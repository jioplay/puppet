# == Class: java
#
# install java
#
# === Parameters:
#
#    $jdk_pkg::      name of jdk package to install
#    $jdk_ver::      version of jdk to install
#    $use_unlimited_strength_jce:: boolean. if true use jce config files
#    $purge_jce:: flag to skip mobi-jce purge (D: true)
#
# === Requires
#
#
# === Sample Usage
#
#  class { "java" :
#    jdk_pkg => "jdk",
#               jdk_ver => "1.6.0_27"
#               use_unlimited_strength_jce => false
#  }
#
# Remember: No empty lines between comments and class definition
#
class java (
    $jdk_pkg = $java::params::jdk_pkg,
    $jdk_ver = $java::params::jdk_ver,
    $use_unlimited_strength_jce = $java::params::use_unlimited_strength_jce,
    $purge_jce = $java::params::purge_jce,
)
inherits java::params {

    anchor { "java::begin": } ->
    class { "java::install": } ->
    class { "java::config": } ->
    anchor { "java::end": }

    os::motd::register { "java": }
}
