class mobi_aaa_reliance_identity_manager::install {
    package { "mobi-aaa-ril-identity-manager-oauth2-${mobi_aaa_reliance_identity_manager::mobi_aaa_reliance_identity_manager_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
