class mobi_aaa_reliance_identity_manager(
    ###icinga.pp
    $icinga = $mobi_aaa_reliance_identity_manager::params::icinga,
    $icinga_instance = $mobi_aaa_reliance_identity_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_aaa_reliance_identity_manager::params::icinga_cmd_args,
    ###end icinga.pp
   $mobi_aaa_reliance_identity_manager_version = $mobi_aaa_reliance_identity_manager::params::mobi_aaa_reliance_identity_manager_version,
   $properties_context = $mobi_aaa_reliance_identity_manager::params::properties_context,
   $end_date = $mobi_aaa_reliance_identity_manager::params::end_date,
   $access_token_lifetime_secs = $mobi_aaa_reliance_identity_manager::params::access_token_lifetime_secs,
   $connection_timeout_secs = $mobi_aaa_reliance_identity_manager::params::connection_timeout_secs,
   $read_timeout_secs = $mobi_aaa_reliance_identity_manager::params::read_timeout_secs,
   $httpconnection_pool_size = $mobi_aaa_reliance_identity_manager::params::httpconnection_pool_size,

   $refresh_token_lifetime_secs = $mobi_aaa_reliance_identity_manager::params::refresh_token_lifetime_secs,
   $reliance_openssl_url = $mobi_aaa_reliance_identity_manager::params::reliance_openssl_url,
   $reliance_api_key = $mobi_aaa_reliance_identity_manager::params::reliance_api_key,
   $reliance_date_format = $mobi_aaa_reliance_identity_manager::params::reliance_date_format,
   $profile_key = $mobi_aaa_reliance_identity_manager::params::profile_key,
   $enable_fail_open = $mobi_aaa_reliance_identity_manager::params::enable_fail_open,
   $reliance_idam_running=$mobi_aaa_reliance_identity_manager::params::reliance_idam_running,
   $access_token_dynamic_attributes=$mobi_aaa_reliance_identity_manager::params::access_token_dynamic_attributes,
   $profile_dynamic_attributes=$mobi_aaa_reliance_identity_manager::params::profile_dynamic_attributes,
   $rpsl_pin_expirytime_milliseconds=$mobi_aaa_reliance_identity_manager::params::rpsl_pin_expirytime_milliseconds,
   $app_user=$mobi_aaa_reliance_identity_manager::params::app_user,
   $app_user_pwd=$mobi_aaa_reliance_identity_manager::params::app_user_pwd,
   $app_user_default_devicetype=$mobi_aaa_reliance_identity_manager::params::app_user_default_devicetype,
   $app_user_default_devicename=$mobi_aaa_reliance_identity_manager::params::app_user_default_devicename,
   $ril_idam_session_details_url=$mobi_aaa_reliance_identity_manager::params::ril_idam_session_details_url,
   $ril_idam_user_details_url=$mobi_aaa_reliance_identity_manager::params::ril_idam_user_details_url,
   $ril_idam_password_login_url=$mobi_aaa_reliance_identity_manager::params::ril_idam_password_login_url,
   $ril_idam_ssotoken_verify_url=$mobi_aaa_reliance_identity_manager::params::ril_idam_ssotoken_verify_url,
   $ril_idam_jtoken_verify_url=$mobi_aaa_reliance_identity_manager::params::ril_idam_jtoken_verify_url,
   $ril_idam_memcached_servers=$mobi_aaa_reliance_identity_manager::params::ril_idam_memcached_servers,
   $ril_idam_memcached_access_timeout=$mobi_aaa_reliance_identity_manager::params::ril_idam_memcached_access_timeout,
   $ril_idam_memcached_access_expireTime=$mobi_aaa_reliance_identity_manager::params::ril_idam_memcached_access_expireTime,
   $method_execution_time=$mobi_aaa_reliance_identity_manager::params::method_execution_time,
)
inherits mobi_aaa_reliance_identity_manager::params{
  include mobi_aaa_reliance_identity_manager::install, mobi_aaa_reliance_identity_manager::config
  anchor { "mobi_aaa_reliance_identity_manager::begin":} -> Class["mobi_aaa_reliance_identity_manager::install"] ->
  Class["mobi_aaa_reliance_identity_manager::config"] ->
  anchor { "mobi_aaa_reliance_identity_manager::end": } -> os::motd::register { $name : }
}
