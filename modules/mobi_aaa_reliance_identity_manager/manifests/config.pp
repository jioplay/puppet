class mobi_aaa_reliance_identity_manager::config {

    file { "/opt/mobi-aaa-ril-identity-manager-oauth2/webapp/WEB-INF/classes/application-config-default.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_aaa_reliance_identity_manager/application-config-default.properties.erb'),
        require => Class["mobi_aaa_reliance_identity_manager::install"],
        notify   => Class["tomcat::service"],
    }
}