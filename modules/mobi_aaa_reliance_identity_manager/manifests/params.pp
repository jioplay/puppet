class mobi_aaa_reliance_identity_manager::params {
    ###icinga.pp
    $icinga = false
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-aaa-reliance-identity-manager/monitoring/health -w 5 -c 10"
     ###end icinga.pp

    $mobi_aaa_reliance_identity_manager_version = "5.0.0-238976-SNAPSHOT.noarch"
    $properties_context = "default"
    $end_date = "15"
    $access_token_lifetime_secs = "1200"
    $connection_timeout_secs = "10"
    $httpconnection_pool_size = "300"
    $read_timeout_secs = "10"
    $refresh_token_lifetime_secs = "3600"
    $reliance_openssl_url ="https://api-preprod.ril.com:8443"
    $reliance_api_key ="l7xxb421ad3f3b6449d598c677865b4e3e3f"
    $reliance_date_format ="yyyyMMddHHmmss"
    $profile_key ="accountKeyaccountKeyaccountKeyac"
    $enable_fail_open ="false"
    $reliance_idam_running="true"
    $access_token_dynamic_attributes="serviceEntitlements,mobile,telephoneNumber,mail,billingSubscriberIds,crmSubscriberId"
    $profile_dynamic_attributes="gender,dob,city,mail,telecomCircle,postalCode,preferredLanguage,mobile"
    $rpsl_pin_expirytime_milliseconds="45000"
    $app_user="abhilash.prasad"
    $app_user_pwd='ril@123'
    $app_user_default_devicetype="android 2.3"
    $app_user_default_devicename="Samsung Tab"
    $ril_idam_session_details_url="/v3/dip/session/details/get"
    $ril_idam_user_details_url="/v3/dip/user/get"
    $ril_idam_password_login_url="/v3/dip/user/unpw/verify"
    $ril_idam_ssotoken_verify_url="/dip/session/verify"
    $ril_idam_jtoken_verify_url="/v3/dip/user/authtoken/verify"
    $ril_idam_memcached_servers="localhost:11211"
    $ril_idam_memcached_access_timeout="3000"
    $ril_idam_memcached_access_expireTime="86400"
    $method_execution_time="1000"
}
