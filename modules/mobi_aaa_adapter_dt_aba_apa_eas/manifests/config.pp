class mobi_aaa_adapter_dt_aba_apa_eas::config{
    file { "/opt/mobi-aaa/config/dt-aba-apa-eas/applicationProperties.xml":
        ensure => file,
        content => template("mobi_aaa_adapter_dt_aba_apa_eas/applicationProperties.xml.erb"),
  notify => Class["tomcat::service"],
  require => Class["mobi_aaa_adapter_dt_aba_apa_eas::install"],
    }
}

