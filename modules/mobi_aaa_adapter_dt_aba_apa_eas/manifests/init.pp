# == class: mobi_aaa_adapter_dt_aba_apa_eas
#
#  install aba apa eas adapter
#
# === Parameters:
#    $aba_apa_eas_adp_ver::
#
# === Requires:
#
#    java
#    tomcat
#
# === Sample Usage
#
#
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_adapter_dt_aba_apa_eas(
    $aba_apa_eas_adp_ver = $mobi_aaa_adapter_dt_aba_apa_eas::params::aba_apa_eas_adp_ver,
    $aba_url = $mobi_aaa_adapter_dt_aba_apa_eas::params::aba_url,
    $apa_url = $mobi_aaa_adapter_dt_aba_apa_eas::params::apa_url,
    $eas_url = $mobi_aaa_adapter_dt_aba_apa_eas::params::eas_url,
    $identity_manager = $mobi_aaa_adapter_dt_aba_apa_eas::params::identity_manager,
    $rights_manager = $mobi_aaa_adapter_dt_aba_apa_eas::params::rights_manager,
    $connection_timeout =  $mobi_aaa_adapter_dt_aba_apa_eas::params::connection_timeout,
    $read_timeout = $mobi_aaa_adapter_dt_aba_apa_eas::params::read_timeout,
    $max_connections = $mobi_aaa_adapter_dt_aba_apa_eas::params::max_connections,
    )
    inherits mobi_aaa_adapter_dt_aba_apa_eas::params {
        include mobi_aaa_adapter_dt_aba_apa_eas::install, mobi_aaa_adapter_dt_aba_apa_eas::config
  anchor { "mobi_aaa_adapter_dt_aba_apa_eas::begin": } -> Class["mobi_aaa_adapter_dt_aba_apa_eas::install"]
  Class["mobi_aaa_adapter_dt_aba_apa_eas::config"] -> anchor { "mobi_aaa_adapter_dt_aba_apa_eas::end" :}
  os::motd::register { $name : }
}
