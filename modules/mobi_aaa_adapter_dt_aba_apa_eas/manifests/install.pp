class mobi_aaa_adapter_dt_aba_apa_eas::install {
    package { "mobi-aaa-adapter-dt-aba-apa-eas-${mobi_aaa_adapter_dt_aba_apa_eas::aba_apa_eas_adp_ver}" :
        ensure => present,
  notify => Class["tomcat::service"]
    }
}
