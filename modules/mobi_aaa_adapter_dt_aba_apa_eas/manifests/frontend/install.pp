class mobi_aaa_adapter_dt_aba_apa_eas::frontend::install {
    package { "mobi-restapi-core-v5-aba-apa-eas-dt-${mobi_aaa_adapter_dt_aba_apa_eas::frontend::aba_apa_eas_adp_frontend_ver}" :
        ensure => present,
  notify => Class["apache::service"]
    }
}
