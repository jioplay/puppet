# == class: mobi_aaa_adapter_dt_aba_apa_eas::frontend
#
#  install aba apa eas adapter
#
# === Parameters:
#    $aba_apa_eas_adp_frontend_ver::
#
# === Requires:
#
#    httpd
#
#
# === Sample Usage
#
#
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_adapter_dt_aba_apa_eas::frontend(
    $aba_apa_eas_adp_frontend_ver = $mobi_aaa_adapter_dt_aba_apa_eas::frontend::params::aba_apa_eas_adp_frontend_ver,
    )
    inherits mobi_aaa_adapter_dt_aba_apa_eas::frontend::params {
        include mobi_aaa_adapter_dt_aba_apa_eas::frontend::install, mobi_aaa_adapter_dt_aba_apa_eas::frontend::config
      anchor { "mobi_aaa_adapter_dt_aba_apa_eas::frontend::begin": } -> Class["mobi_aaa_adapter_dt_aba_apa_eas::frontend::install"]
  Class["mobi_aaa_adapter_dt_aba_apa_eas::frontend::config"] -> anchor { "mobi_aaa_adapter_dt_aba_apa_eas::frontend::end" :}
  os::motd::register { $name : }
}
