class mobi_aaa_adapter_dt_aba_apa_eas::params {
    $aba_apa_eas_adp_ver = "5.0.0-194829"
    $aba_url = ""
    $apa_url = ""
    $eas_url = ""
    $identity_manager = ""
    $rights_manager = ""
    $connection_timeout = ""
    $read_timeout = ""
    $max_connections = ""
}
