DROP DATABASE IF EXISTS ril_purchase_manager;
CREATE DATABASE IF NOT EXISTS ril_purchase_manager;
use ril_purchase_manager

DROP TABLE IF EXISTS `PURCHASES`;
CREATE TABLE `PURCHASES` (
  `purchase_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `carrier` varchar(256),
  `created_by` varchar(256),
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiration_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(256),
  `modified_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `offer_id` varchar(256),
  `product` varchar(256),
  `profile_id` varchar(256),
  `version` varchar(256),
  `sku_id` varchar(256),
  `inventory_id` varchar(256),
  PRIMARY KEY (`purchase_id`),
  KEY `profile_id_idx` (`profile_id`,`offer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
