class mobi_aaa_reliance_purchase_manager::config {

    file { "/opt/mobi-aaa-ril-purchase-manager/webapp/WEB-INF/classes/default.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_aaa_reliance_purchase_manager/default.xml.erb'),
        require => Class["mobi_aaa_reliance_purchase_manager::install"],
        notify   => Class["tomcat::service"],
    }
    file { "/opt/mobi-aaa-ril-purchase-manager/webapp/WEB-INF/classes/application-config-default.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_aaa_reliance_purchase_manager/application-config-default.properties.erb'),
        require => Class["mobi_aaa_reliance_purchase_manager::install"],
        notify   => Class["tomcat::service"],
    }
}