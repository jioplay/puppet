class mobi_aaa_reliance_purchase_manager::params {

    $mobi_aaa_reliance_purchase_manager_version = "5.0.0-221381-SNAPSHOT.noarch"
    $db_username = "purchase_user"
    $db_password = "purchase_user"
    $hibernate_database_dialect = "org.hibernate.dialect.MySQLDialect"
    $jdbc_driver = "com.mysql.jdbc.Driver"
    $jdbc_url = "jdbc:mysql://dbmobi2/ril_purchase_manager?autoReconnect=true"

    $properties_context = "default"
    $application_name = "Purchase manager"
    $expiration_time_hrs = "168"
    $expiration_time_hrs_live_channels = "720"
    $liveservice_offer_id = "offer_123456"
    $auth_identity_manager_url ="http://identitymanagervip:8080/mobi-aaa-ril-identity-manager-oauth2"
    $auth_rights_manager_url = "http://rightsmanagervip:8080/mobi-aaa-rights-manager"
    $offer_manager_url = "http://offermanagervip:8080/mobi-aaa-ril-offer-manager"
    $jerseyclient_connection_timeout_seconds = "10"
    $jersey_client_read_timeout_seconds = "10"
    $jerseyclient_httpconnection_poolsize = "300"
}