class mobi_aaa_reliance_purchase_manager(
   $mobi_aaa_reliance_purchase_manager_version = $mobi_aaa_reliance_purchase_manager::params::mobi_aaa_reliance_purchase_manager_version,
   $db_username = $mobi_aaa_reliance_purchase_manager::params::db_username,
   $db_password = $mobi_aaa_reliance_purchase_manager::params::db_password,
   $hibernate_database_dialect = $mobi_aaa_reliance_purchase_manager::params::hibernate_database_dialect,
   $jdbc_driver = $mobi_aaa_reliance_purchase_manager::params::jdbc_driver,
   $jdbc_url = $mobi_aaa_reliance_purchase_manager::params::jdbc_url,

   $properties_context = $mobi_aaa_reliance_purchase_manager::params::properties_context,
   $application_name = $mobi_aaa_reliance_purchase_manager::params::application_name,
   $expiration_time_hrs = $mobi_aaa_reliance_purchase_manager::params::expiration_time_hrs,
   $expiration_time_hrs_live_channels = $mobi_aaa_reliance_purchase_manager::params::expiration_time_hrs_live_channels,
   $liveservice_offer_id = $mobi_aaa_reliance_purchase_manager::params::liveservice_offer_id,
   $auth_identity_manager_url = $mobi_aaa_reliance_purchase_manager::params::auth_identity_manager_url,
   $auth_rights_manager_url = $mobi_aaa_reliance_purchase_manager::params::auth_rights_manager_url,
    $offer_manager_url = $mobi_aaa_reliance_purchase_manager::params::offer_manager_url,
    $jerseyclient_connection_timeout_seconds = $mobi_aaa_reliance_purchase_manager::params::jerseyclient_connection_timeout_seconds,
    $jersey_client_read_timeout_seconds = $mobi_aaa_reliance_purchase_manager::params::jersey_client_read_timeout_seconds,
   $jerseyclient_httpconnection_poolsize = $mobi_aaa_reliance_purchase_manager::params::jerseyclient_httpconnection_poolsize,
)
inherits mobi_aaa_reliance_purchase_manager::params{
  include mobi_aaa_reliance_purchase_manager::install, mobi_aaa_reliance_purchase_manager::config
  anchor { "mobi_aaa_reliance_purchase_manager::begin":} -> Class["mobi_aaa_reliance_purchase_manager::install"] ->
  Class["mobi_aaa_reliance_purchase_manager::config"] ->
  anchor { "mobi_aaa_reliance_purchase_manager::end": } -> os::motd::register { $name : }
}
