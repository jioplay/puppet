class mobi_aaa_reliance_purchase_manager::install {
    package { "mobi-aaa-ril-purchase-manager-${mobi_aaa_reliance_purchase_manager::mobi_aaa_reliance_purchase_manager_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
