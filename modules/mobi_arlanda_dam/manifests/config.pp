class mobi_arlanda_dam::config {

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/dam.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content =>template('mobi_arlanda_dam/dam-tomcat-context.xml.erb'),
        require => Class["mobi_arlanda_dam::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-arlanda-dam/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_dam/dam-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_arlanda_dam::install"],
    }


}
