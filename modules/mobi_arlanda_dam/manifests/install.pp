class mobi_arlanda_dam::install {
    package { "mobi-arlanda-dam":
        ensure => "${mobi_arlanda_dam::version}",
        notify   => Class["tomcat::service"],
    }

    #file { "/opt/mobi-arlanda-dam/database":
    #    ensure => "directory",
    #    owner  => "rtv",
    #    group  => "rtv",
    #    mode   => "0755",
    #    require => File["/opt/mobi-arlanda-dam"],
    #}

    #file { "/opt/mobi-arlanda-dam/database/postgres":
    #    ensure => "directory",
    #    owner  => "rtv",
    #    group  => "rtv",
    #    mode   => "0755",
    #    require => File["/opt/mobi-arlanda-dam/database"],
    #}

   file { ["/opt/mobi-arlanda-dam", "/opt/mobi-arlanda-dam/database/", "/opt/mobi-arlanda-dam/database/postgres", "/opt/mobi-arlanda-dam/database/postgres/eesimulator"]:
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
    }

    file { "/opt/mobi-arlanda-dam/database/postgres/dam_schema.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_dam/dam_schema.sql.erb'),
        require => File["/opt/mobi-arlanda-dam/database/postgres"],
    }

    file { "/opt/mobi-arlanda-dam/database/postgres/dam_seed.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_dam/dam_seed.sql.erb'),
        require => File["/opt/mobi-arlanda-dam/database/postgres"],
    }

    file { "/opt/mobi-arlanda-dam/database/postgres/dam_mobi2lite_schema.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_dam/dam_mobi2lite_schema.sql.erb'),
        require => File["/opt/mobi-arlanda-dam/database/postgres"],
    }

    file { "/opt/mobi-arlanda-dam/database/postgres/dam_mobi2lite_seed.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_dam/dam_mobi2lite_seed.sql.erb'),
        require => File["/opt/mobi-arlanda-dam/database/postgres"],
    }

   file { "/opt/mobi-arlanda-dam/database/postgres/eesimulator/dam_seed.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_dam/dam_seed.sql.erb'),
        require => File["/opt/mobi-arlanda-dam/database/postgres/eesimulator"],
    }

}
