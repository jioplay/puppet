# == Class: mobi_arlanda_dam
#
#  installs juarn component
#
# === Parameters:
#
#    $dam_version::
#    $dam_username::
#    $dam_password::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_cms_us::juarn" :
#           dam_version => "5.0.0-178058",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_dam (
    ###icinga.pp
    $icinga = $mobi_arlanda_dam::params::icinga,
    $icinga_instance = $mobi_arlanda_dam::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_dam::params::icinga_cmd_args,
    ###end icinga.pp
    $version = $mobi_arlanda_dam::params::version,
    $amq_broker_url = $mobi_arlanda_dam::params::amq_broker_url,
    $dam_db_username = $mobi_arlanda_dam::params::dam_db_username,
    $dam_db_password = $mobi_arlanda_dam::params::dam_db_password,
    $hibernate_database_dialect = $mobi_arlanda_dam::params::hibernate_database_dialect,
    $hibernate_database_vendor = $mobi_arlanda_dam::params::hibernate_database_vendor,
    $jdbc_driver = $mobi_arlanda_dam::params::jdbc_driver,
    $connection_test_query = $mobi_arlanda_dam::params::connection_test_query,
    $dam_db_jdbc_url = $mobi_arlanda_dam::params::dam_db_jdbc_url,
    $fm_db_jdbc_url = $mobi_arlanda_dam::params::fm_db_jdbc_url,
    $install_db = $mobi_arlanda_dam::params::install_db,
    $postgredb_user = $mobi_arlanda_dam::params::postgredb_user,
    $fm_db_username = $mobi_arlanda_dam::params::fm_db_username,
    $fm_db_password = $mobi_arlanda_dam::params::fm_db_password,
    $ing_db_username = $mobi_arlanda_ingestor::params::ing_db_username,
    $ing_db_password = $mobi_arlanda_ingestor::params::ing_db_password,
    $include_fm_resource = $mobi_arlanda_dam::params::include_fm_resource,
    $ril = $mobi_arlanda_dam::params::ril,
)

inherits mobi_arlanda_dam::params {
    include mobi_arlanda_dam::install, mobi_arlanda_dam::config, mobi_cms_us::tomcat_config
    anchor { "mobi_arlanda_dam::begin": } -> Class["mobi_arlanda_dam::install"] ->
    Class["mobi_arlanda_dam::config"] ->
    Class["mobi_cms_us::tomcat_config"] ->
    class { "mobi_arlanda_dam::icinga":} ->
    anchor { "mobi_arlanda_dam::end" :} -> os::motd::register { $name : }
}

