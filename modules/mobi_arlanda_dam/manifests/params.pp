class mobi_arlanda_dam::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /dam/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $version = "5.0.0-183115"

    $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100&amp;maxReconnectAttempts=1"

    $dam_db_username = "dam_user"
    $dam_db_password = "dam_user"

    # Global database params
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $hibernate_database_vendor = "MYSQL"
    $jdbc_driver = "com.mysql.jdbc.Driver"
    $connection_test_query = "select 1 from dual"

    $dam_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/dam?autoReconnect=true"
    $fm_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/mobi2lite?autoReconnect=true"
    $install_db = "false"
    $postgredb_user = "postgres"

    $ing_db_username = "ing_user"
    $ing_db_password = "ing_user"

    $fm_db_username = "fm_user"
    $fm_db_password = "fm_user"

    $include_fm_resource = true
    $ril = false
}
