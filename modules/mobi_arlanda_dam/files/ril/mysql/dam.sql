CREATE DATABASE IF NOT EXISTS dam CHARACTER SET=utf8;
use dam;

CREATE TABLE IF NOT EXISTS `DAM_ASSET` (
  `ASSET_ID` bigint(20) NOT NULL auto_increment,
  `TYPE` varchar(255) NOT NULL,
  `AVAILABLE` bit(1) default NULL,
  `CORRELATION_ID` varchar(1024) default NULL,
  `CRID` varchar(1024) NOT NULL,
  `DOWNLOAD_ATTEMPTS` int(11) default NULL,
  `ENCRYPTION_REQUIRED` varchar(255) NOT NULL,
  `EXPIRED` bit(1) default NULL,
  `EXPIRES` datetime default NULL,
  `GENRE` varchar(1024) default NULL,
  `GROUP_ID` int(11) default NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  `KEYWORD` varchar(1024) default NULL,
  `LAST_MODIFIED` datetime default NULL,
  `MEDIA_RESTRICTIONS` varchar(40) default NULL,
  `POST_ROLL_INVENTORY_ID` bigint(20) default NULL,
  `PRE_ROLL_INVENTORY_ID` bigint(20) default NULL,
  `PROVIDER_ID` int(11) NOT NULL,
  `PUBLISHED` bit(1) default NULL,
  `SHORT_TITLE` varchar(512) default NULL,
  `START_OF_AVAILABILITY` datetime default NULL,
  `STATE` int(11) default NULL,
  `SYNOPSIS` varchar(1024) default NULL,
  `TITLE` varchar(1024) default NULL,
  `TRASHED` bit(1) default NULL,
  `UPLOAD_DATE` datetime default NULL,
  `VERSION` int(11) NOT NULL,
  `EPISODE_NUMBER` bigint(20) NOT NULL,
  `SERIES_NUMBER` bigint(20) NOT NULL,
  PRIMARY KEY  (`ASSET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `DAM_ADDITIONAL_METADATA` (
  `METADATA_ID` bigint(20) NOT NULL auto_increment,
  `METAKEY` varchar(255) NOT NULL,
  `METAVALUE` varchar(1024) default NULL,
  `ASSET_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`METADATA_ID`),
  KEY `FK_DAM_ADDITIONAL_METADATA` (`ASSET_ID`),
  CONSTRAINT `FK_DAM_ADDITIONAL_METADATA` FOREIGN KEY (`ASSET_ID`) REFERENCES `DAM_ASSET` (`ASSET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `DAM_ASSET_INSTANCE` (
  `INSTANCE_ID` bigint(20) NOT NULL auto_increment,
  `BITRATE` double default NULL,
  `CHECKSUM` varchar(255) default NULL,
  `DURATION` double default NULL,
  `ENCODING_ID` int(11) NOT NULL,
  `FORMAT` varchar(24) default NULL,
  `INSTANCE_LINKTYPE` varchar(255) default NULL,
  `IS_MBR` bit(1) default NULL,
  `IS_ORIGINAL` bit(1) default NULL,
  `PATH` varchar(1024) default NULL,
  `SIZE_` double default NULL,
  `TYPE` varchar(255) default NULL,
  `ASSET_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`INSTANCE_ID`),
  KEY `FK_DAM_ASSET_INSTANCE` (`ASSET_ID`),
  CONSTRAINT `FK_DAM_ASSET_INSTANCE` FOREIGN KEY (`ASSET_ID`) REFERENCES `DAM_ASSET` (`ASSET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `DAM_CONFIG` (
  `CONFIG_KEY` varchar(255) NOT NULL,
  `CONFIG_VALUE` varchar(1024) default NULL,
  `DEFAULT_CONFIG_VALUE` varchar(1024) default NULL,
  `DESCRIPTION` varchar(2048) default NULL,
  `LAST_MODIFIED` datetime default NULL,
  PRIMARY KEY  (`CONFIG_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `DAM_TRACK` (
  `TRACK_ID` bigint(20) NOT NULL auto_increment,
  `BITRATE` double default NULL,
  `BITSPERSAMPLE` int(11) default NULL,
  `CHANNELS` int(11) default NULL,
  `CODEC` varchar(255) default NULL,
  `DURATION` double default NULL,
  `FMTP` varchar(255) default NULL,
  `FRAMERATE` double default NULL,
  `HEIGHT` int(11) default NULL,
  `PAYLOAD` varchar(255) default NULL,
  `SAMPLERATE` double default NULL,
  `SIZE_` bigint(20) default NULL,
  `START_` double default NULL,
  `TYPE` varchar(255) default NULL,
  `WIDTH` int(11) default NULL,
  `INSTANCE_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`TRACK_ID`),
  KEY `FK_DAM_TRACK` (`INSTANCE_ID`),
  CONSTRAINT `FK_DAM_TRACK` FOREIGN KEY (`INSTANCE_ID`) REFERENCES `DAM_ASSET_INSTANCE` (`INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
