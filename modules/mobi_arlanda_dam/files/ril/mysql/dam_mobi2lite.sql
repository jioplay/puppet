use dam;

CREATE TABLE IF NOT EXISTS `TV_INVENTORY` (
  `INVENTORY_ID` bigint(20) NOT NULL auto_increment,
  `BACKEND_CHANNEL` bigint(20) NOT NULL default '0',
  `DESCRIPTION` varchar(2047) default NULL,
  `DISPLAY_NAME` varchar(255) NOT NULL,
  `ICON_NAME` varchar(255) default NULL,
  `JUKEBOX_CHANNEL_ID` bigint(20) default NULL,
  `LBS_DEFAULT_RULE` varchar(255) NOT NULL,
  `LONG_NAME` varchar(255) default NULL,
  `MEDIA_RESTRICTIONS` varchar(255) default NULL,
  `MEDIATYPE` varchar(255) NOT NULL,
  `MEDIA_ASPECT_RATIO` varchar(255) default '4x3',
  `NAME` varchar(255) NOT NULL,
  `STATION_ID` varchar(255) default NULL,
  PRIMARY KEY  (`INVENTORY_ID`),
  UNIQUE KEY `UK_TV_INVENTORY` (`INVENTORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `CONTENT` (
  `ACTUAL_LOCATION` varchar(255) NOT NULL,
  `IS_AD` bit(1) default NULL,
  `CHECKSUM` varchar(255) default NULL,
  `CONSUMED` varchar(255) NOT NULL,
  `CRID` varchar(255) NOT NULL,
  `DURATION` float NOT NULL,
  `EXPIRES` datetime default NULL,
  `GENRE` varchar(255) default NULL,
  `IS_INTERSTITIAL` varchar(255) default NULL,
  `KEYWORD` varchar(255) default NULL,
  `LASTMOD` datetime default NULL,
  `NUM_FRAMES` bigint(20) default NULL,
  `ORIGINAL_LOCATION` varchar(255) default NULL,
  `PROVIDER_ID` bigint(20) NOT NULL,
  `SLOT` bigint(20) NOT NULL,
  `START_OF_AVAILABILITY` datetime default NULL,
  `STATUS` bigint(20) NOT NULL,
  `UPLOAD_DATE` datetime NOT NULL,
  `VERSION` int(11) default NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`INVENTORY_ID`),
  KEY `FK_CONTENT` (`INVENTORY_ID`),
  CONSTRAINT `FK_CONTENT` FOREIGN KEY (`INVENTORY_ID`) REFERENCES `TV_INVENTORY` (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `FOLDER_INVENTORY` (
  `CHAPTERIZED_CONTENT` varchar(255) default NULL,
  `EXPIRES` datetime default NULL,
  `GENRE` varchar(255) default NULL,
  `KEYWORD` varchar(255) default NULL,
  `MCDPATH` varchar(255) NOT NULL,
  `NETWORK` varchar(255) default NULL,
  `PLAYLIST_ENABLED` varchar(255) default NULL,
  `PROVIDER_ID` bigint(20) NOT NULL,
  `SHOW` varchar(255) default NULL,
  `START_OF_AVAILABILITY` datetime default NULL,
  `THUMBNAIL_LOCATION` varchar(255) default NULL,
  `TOTAL_NUMBER_OF_PARTS` int(11) default NULL,
  `UI_NAME` varchar(255) NOT NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  `EPISODE_NUMBER` bigint(20) default NULL,
  `SERIES_NUMBER` bigint(20) default NULL,
  PRIMARY KEY  (`INVENTORY_ID`),
  KEY `FK_FOLDER_INVENTORY` (`INVENTORY_ID`),
  CONSTRAINT `FK_FOLDER_INVENTORY` FOREIGN KEY (`INVENTORY_ID`) REFERENCES `TV_INVENTORY` (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `JB_ADVERTISING_FOLDERS` (
  `CONTENT_FOLDER_ID` bigint(20) NOT NULL,
  `PROVIDER_ID` bigint(20) NOT NULL,
  `POST_ROLL_FOLDER_ID` bigint(20) default NULL,
  `PRE_ROLL_FOLDER_ID` bigint(20) default NULL,
  PRIMARY KEY  (`CONTENT_FOLDER_ID`,`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `JB_FOLDER_CLIP_CONFIG` (
  `clip_assign_type` varchar(255) NOT NULL,
  `clip_assign_to_id` bigint(20) NOT NULL,
  `created_by_app` varchar(255) default NULL,
  `folder_inventory_id` bigint(20) NOT NULL,
  `subscription_mode` bigint(20) NOT NULL,
  PRIMARY KEY  (`clip_assign_to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PROVIDERS` (
  `PROVIDER_ID` bigint(20) NOT NULL auto_increment,
  `DFLT_CONTENT_SHELF_LIFE` float default '1825',
  `PROVIDER` varchar(255) default NULL,
  `SECURED_OVER_WIFI` varchar(255) default NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `REQUIRES_XML_METADATA` varchar(10) default 'FALSE',
  `TIME_ZONE` varchar(255) default NULL,
  `MANAGE_CHAPTERIZED_CONTENT` varchar(255) default NULL,
  `TYPE` varchar(30) NOT NULL default 'content',
  `TITLE` varchar(1024) default NULL,
  PRIMARY KEY  (`PROVIDER_ID`),
  UNIQUE KEY `UK_PROVIDER` (`PROVIDER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `FM_SEQ` (
  `PROVIDER_ID` bigint(20) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PROVIDER_METADATA_CONFIG` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `CHECKSUM` varchar(255) default NULL,
  `CRID` varchar(255) default NULL,
  `ENABLE_LOCAL_PLAYBACK` varchar(255) default NULL,
  `ENABLE_WIFI` varchar(255) default NULL,
  `EXPIRES` varchar(255) default NULL,
  `FOLDER` varchar(255) default NULL,
  `GENRE` varchar(255) default NULL,
  `INDIRECT_UPLOAD_URL` varchar(255) default NULL,
  `INDUSTRY` varchar(255) default NULL,
  `KEYWORD` varchar(255) default NULL,
  `SHORT_TITLE` varchar(255) default NULL,
  `SLOT` varchar(255) default NULL,
  `START_OF_AVAILABILITY` varchar(255) default NULL,
  `SYNOPSIS` varchar(255) default NULL,
  `THUMBNAIL_UPLOAD_URL` varchar(255) default NULL,
  `TICKER` varchar(255) default NULL,
  `TITLE` varchar(255) default NULL,
  `CLOSED_CAPTION_URL` varchar(255) default NULL,
  `ENCRYPTION_REQUIRED` varchar(255) default NULL,
  `ASPECT_RATIO` varchar(255) default 'ASPECT_RATIO',
  `EPISODE_NUMBER` varchar(255) default 'EPISODE_NUMBER',
  `SERIES_NUMBER` varchar(255) default 'SERIES_NUMBER',
  PRIMARY KEY  (`PROVIDER_ID`),
  UNIQUE KEY `UK_PROVIDER_METADATA_CONFIG` (`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `TV_FOLDER_ITEM` (
  `ID` bigint(20) NOT NULL auto_increment,
  `CREATED_TIME` datetime NOT NULL,
  `DISPLAY_CODE` varchar(255) NOT NULL,
  `DISPLAY_ORDER` bigint(20) NOT NULL,
  `ITEM_INVENTORY_ID` bigint(20) NOT NULL,
  `LAST_UPDATED_TIME` datetime NOT NULL,
  `LAST_UPDATED_BY` varchar(255) NOT NULL,
  `FOLDER_INVENTORY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `UK_TV_FOLDER_ITEM` (`FOLDER_INVENTORY_ID`,`ITEM_INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `TV_PACKAGE_INVENTORY` (
  `PACKAGE_ID` bigint(20) NOT NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
