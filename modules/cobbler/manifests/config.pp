class cobbler::config {

    file { "/etc/cobbler/settings" :
        ensure => present,
        content => template("cobbler/settings.erb"),

    }

    file { "/etc/cobbler/modules.conf" :
        ensure => present,
        source => "puppet:///modules/cobbler/modules.conf",
    }

    file { "/etc/cobbler/users.digest" :
        ensure => present,
        source => "puppet:///modules/cobbler/modules.conf",
    }
}
