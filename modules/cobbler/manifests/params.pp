class cobbler::params {

    $images_rsync = "rsync://archive.kernel.org/centos-vault/"

    $images = {
        "centos-5.6-i386"   => { path => "5.6/os/i386/", arch => "i386" },
        "centos-5.6-x86_64" => { path => "5.6/os/x86_64/", arch => "x86_64" },
        "centos-6.2-i386"   => { path => "6.2/os/i386", arch => "i386" },
        "centos-6.2-x86_64" => { path => "6.2/os/x86_64/", arch => "x86_64" },
    }
    

    #the repos I want to sync our cobbler with
    $repos= {
        "centos-5-i386-extras"     => {
            path => "mirror.centos.org/centos/5/extras/i386/",
            arch => "i386",
        },
        "centos-6-i386-extras"     => {
            path => "mirror.centos.org/centos/5/extras/i386/",
            arch => "i386",
        },
        "centos-5-i386-updates"    => {
            path => "mirror.centos.org/centos/5/updates/i386/",
            arch => "i386",
        },
        "centos-6-i386-updates"    => {
            path => "mirror.centos.org/centos/6/updates/i386/",
            arch => "i386",
        },
        "centos-5-x86_64-extras"     => {
            path => "mirror.centos.org/centos/5/extras/x86_64/",
            arch => "x86_64",
        },
        "centos-6-x86_64-extras"     => {
            path => "mirror.centos.org/centos/5/extras/x86_64/",
            arch => "x86_64",
        },
        "centos-5-x86_64-updates"    => {
            path => "mirror.centos.org/centos/5/updates/x86_64/",
            arch => "x86_64",
        },
        "centos-6-x86_64-updates"    => {
            path => "mirror.centos.org/centos/6/updates/x86_64/",
            arch => "x86_64",
        },
        "epel-4-i386-updates"        => {
            path => "mirrors.kernel.org/fedora-epel/4/i386/",
            arch => "i386",
        },
        "epel-4-x86_64-updates"      => {
            path => "mirrors.kernel.org/fedora-epel/4/x86_64/",
            arch => "x86_64",
        },
        "epel-5-i386-updates"        => {
            path => "mirrors.kernel.org/fedora-epel/5/i386/",
            arch => "i386",
        },
        "epel-5-x86_64-updates"      => {
            path => "mirrors.kernel.org/fedora-epel/5/x86_64/",
            arch => "x86_64",
        },
        "epel-6-i386-updates"        => {
            path => "mirrors.kernel.org/fedora-epel/6/i386/",
            arch => "i386",
        },
        "epel-6-x86_64-updates"      => {
            path => "mirrors.kernel.org/fedora-epel/6/x86_64/",
            arch => "x86_64"
        },
        "el5-x86_64-rpmforge" => {
            path => "apt.sw.be/redhat/el5/en/x86_64/rpmforge/",
            arch => "x86_64",
        },
        "el5-i386-rpmforge" => {
            path => "apt.sw.be/redhat/el5/en/i386/rpmforge/",
            arch => "i386",
        },
        "el6-x86_64-rpmforge" => {
            path => "apt.sw.be/redhat/el6/en/x86_64/rpmforge/",
            arch => "x86_64",
        },
        "el6-i386-rpmforge" => {
            path => "apt.sw.be/redhat/el6/en/i386/rpmforge/",
            arch => "i386",
        },
        "puppetlabs-5-i386-prosvc"   => {
            path => "yum.puppetlabs.com/el/5/products/i386/",
            arch => "i386",
        },
        "puppetlabs-5-x86_64-prosvc" => {
            path => "yum.puppetlabs.com/el/5/products/x86_64/",
            arch => "x86_64",
        },
        "puppetlabs-6-i386-prosvc"   => {
            path => "yum.puppetlabs.com/el/6/products/i386/",
            arch => "i386",
        },
        "puppetlabs-6-x86_64-prosvc" => {
            path => "yum.puppetlabs.com/el/6/products/x86_64/",
            arch => "x86_64",
        },
    }

}
