class cobbler::hardlink {

    package { "hardlink":
        ensure => latest,
        require => [ Class["cobbler::import"], Class["cobbler::sync"] ],
    }

    exec { "cobbler-hardlink":
        path => "/usr/sbin:/usr/bin",
        command => "cobbler hardlink",
        require => Package["hardlink"],
    }

}
