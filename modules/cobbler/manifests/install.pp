class cobbler::install
{

    package { "cobbler":
        ensure => latest,
        require => Class["cobbler::yum"],
        notify => Service["httpd"],

    }

    package { "cobbler-web":
        ensure => latest,
        require => Class["cobbler::yum"],
        notify => Service["httpd"],

    }

}
