define cobbler::mirror_add($hash) {

    $mirror_path = $hash[$name]["path"]
    $mirror_arch = $hash[$name]["arch"]
    exec { "cobbler-repo-add-$name" :
        path => "/usr/bin:/bin",
        command => "cobbler repo add --name $name --mirror http://${mirror_path} --arch ${mirror_arch} --mirror-locally True --keep-updated True --comment 'Added by puppet install' ",
        unless => [ "test `cobbler repo list | grep -c ${name}` -eq 1" ],
        require => Class["cobbler::service"],
    }
}
