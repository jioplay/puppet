define cobbler::mirror_sync
{
    exec { "cobbler-reposync-$name":
        path => "/usr/bin:/bin",
        command => "cobbler reposync --only $name",
        require => Class["cobbler::service"],
        timeout => 18000,
    }
}
