# == Class: cobbler
#
#    installs cobbler and imports repos and images
#
# === Parameters:
#
#    $images:: hash of images to install
#    $repos::  hash of repos to install
#
# === Requires
#
#    apache
#    sshd
#    git
#
# === Sample Usage
#  class { "cobbler" : }
# Remember: No empty lines between comments and class definition
#
class cobbler  (
    $images = $cobbler::params::images,
    $images_rsync = $cobbler::params::images_rsync,
    $repos = $cobbler::params::repos,
)
inherits cobbler::params {

    anchor { "begin" : } -> 
    class { "cobbler::yum" : } ->
    class { "cobbler::install" : } ->
    class { "cobbler::config" : } ->
    class { "cobbler::service" : } ->
    class { "cobbler::import" :} -> 
    class { "cobbler::sync" : } ->  
    class { "cobbler::hardlink" : } -> 
    anchor { "end" :}

    os::motd::register { $name : }
}
