class cobbler::import
{
    $hash_keys = keys( $cobbler::repos )

    cobbler::import_repo { $cobbler::import::hash_keys:
        hash => $cobbler::repos,
        rsync => $cobbler::import::centos_rsync,
    }

}
