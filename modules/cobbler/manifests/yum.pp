class cobbler::yum
{

    #we have to make sure that the mobi repos are gone for this machine
    file { "/etc/yum.repos.d/mobitv-ops-prod-mobi.repo":
        ensure => absent,
    }
}
