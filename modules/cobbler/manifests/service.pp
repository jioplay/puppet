class cobbler::service
{
    #this is not restarting httpd atthe moment
    #
    #
    service { "cobblerd":
        ensure => running,
        enable => true,
        hasstatus => true,
        hasrestart => true,
        subscribe => File["/etc/cobbler/settings"],
        require => [
                    Class["cobbler::install"],
                    Class["cobbler::config"],
                    ],
    }
}
