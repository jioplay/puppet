define cobbler::import_repo($hash, $rsync) {
    
    $repo_path = $hash[$name]["path"]
    $repo_arch = $hash[$name]["arch"]
    
    exec { "cobbler-import-$name" :
        path => "/usr/bin:/bin",
        command => "cobbler import --name ${name} --breed redhat --path $rsync/${repo_path} --arch ${repo_arch}",
        unless => [ "test `cobbler distro list | grep -c ${name}` -eq 1" ],
        require => Class["cobbler::service"],
        timeout => 18000,
    }
}
