class chkconfig ($serviceName) {
  exec {
    'chkconfig' :
      command => "/sbin/chkconfig --level 3456 ${serviceName} on",
      unless => "/usr/bin/test -h /etc/rc3.d/${serviceName}",
  }
}
