class aaa_sprint_carriermodule::install {
    package { "mobi-sprint-carrier-module-${aaa_sprint_carriermodule::sprint_carriermodule_ver}" :
        ensure => present,
  notify => Class["tomcat::service"]
    }
}
