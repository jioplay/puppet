# == Class: aaa_sprint_carriermodule
#
#  installs transaction processor ng component
#
# === Parameters:
#
#    $sprint_carriermodule_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
# Remember: No empty lines between comments and class definition
#
class aaa_sprint_carriermodule (

    $sprint_carriermodule_ver = $aaa_sprint_carriermodule::sprint_carriermodule_ver,
)
inherits aaa_sprint_carriermodule::params {
    include aaa_sprint_carriermodule::install, aaa_sprint_carriermodule::config
    anchor { "aaa_sprint_carriermodule::begin": } -> Class["aaa_sprint_carriermodule::install"]
    Class["aaa_sprint_carriermodule::config"] -> anchor { "aaa_sprint_carriermodule::end" :}
    os::motd::register { $name : }
}
