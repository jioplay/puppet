class mobi_arlanda_yume::config {

    file { "/opt/arlanda/conf/yume-adapter/yume-adapter.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_yume/yume-adapter.properties.erb'),
        require => Class["mobi_arlanda_yume::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/arlanda/conf/yume-adapter/encodings.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_yume/encodings.properties.erb'),
        require => Class["mobi_arlanda_yume::install"],
        notify   => Class["tomcat::service"],
    }
}
