class mobi_arlanda_yume::params {
    ###icinga.pp
    $icinga = false
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-arlanda-yume/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    $mobi_arlanda_yume_version = "5.0.0-258499.noarch"
    $provider_name = "yume"
    $external_server_name = "dummy.server.name"
}
