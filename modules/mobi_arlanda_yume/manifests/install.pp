class mobi_arlanda_yume::install {
    package { "mobi-arlanda-ril-yume-adapter-${mobi_arlanda_yume::mobi_arlanda_yume_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
