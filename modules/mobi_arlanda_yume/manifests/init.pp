class mobi_arlanda_yume(
    ###icinga.pp
    $icinga = $mobi_arlanda_yume::params::icinga,
    $icinga_instance = $mobi_arlanda_yume::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_yume::params::icinga_cmd_args,
    ###end icinga.pp
   $mobi_arlanda_yume_version = $mobi_arlanda_yume::params::mobi_arlanda_yume_version,
   $provider_name = $mobi_arlanda_yume::params::provider_name,
   $external_server_name = $mobi_arlanda_yume::params::external_server_name,)

inherits mobi_arlanda_yume::params{
    include mobi_arlanda_yume::install, mobi_arlanda_yume::config
    anchor { "mobi_arlanda_yume::begin": } -> Class["mobi_arlanda_yume::install"] ->
    Class["mobi_arlanda_yume::config"] ->
    anchor { "mobi_arlanda_yume::end" :} -> os::motd::register { $name : }
}
