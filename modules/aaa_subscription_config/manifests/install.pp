class aaa_subscription_config::install {
    package { "mobi-aaa-subscription-infrastructure-config-${aaa_subscription_config::subscription_config_ver}" :
        ensure => present,
  notify => Class["tomcat::service"]
    }
}
