# == Class: aaa_subscription_config
#
#  installs subscription config component
#
# === Parameters:
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
# Remember: No empty lines between comments and class definition
#
class aaa_subscription_config (

    $subscription_config_ver = $aaa_subscription_config::subscription_config_ver,
)
inherits aaa_subscription_config::params {
    include aaa_subscription_config::install
    anchor { "aaa_subscription_config::begin": } ->
    Class["aaa_subscription_config::install"] -> anchor { "aaa_subscription_config::end" :}
    os::motd::register { $name : }
}