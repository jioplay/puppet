DROP TABLE If EXISTS RECORDING_DISTRIBUTOR;
CREATE TABLE RECORDING_DISTRIBUTOR
(
     RECORDING_DISTRIBUTOR_ID INTEGER AUTO_INCREMENT,
     CHANNEL_ID VARCHAR(100),
     MESSAGE VARCHAR(100),
     MAX_RECORDING_LENGTH VARCHAR(30),
     OPERATION VARCHAR(30),
     RECORDING_ID VARCHAR(100),
     START_TIME VARCHAR(100),
     END_TIME VARCHAR(100),
     STATUS VARCHAR(30),
     USER_ID VARCHAR(100),
     SHARD VARCHAR(100),
     JMS_REPLY_DESTINATION_TYPE VARCHAR(10),
     JMS_REPLY_DESTINATION_NAME VARCHAR(100),
     PRIMARY KEY RECORDING_DISTRIBUTOR_PK (RECORDING_DISTRIBUTOR_ID),
     INDEX (RECORDING_ID)
);