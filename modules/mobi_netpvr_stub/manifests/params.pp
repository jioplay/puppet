class mobi_netpvr_stub::params {
    $version = "5.0.0-222217"
    $vid_carrier = "dt"
    $vid_product = "eons"
    $vid_version = "1.0"
    $vid_guideprovider = "default"

    $db_username = "netpvr_user"
    $db_password = "netpvr_user"
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $jdbc_driver = "com.mysql.jdbc.Driver"
    $jdbc_url = "jdbc:mysql://hostmanager/netpvr_stub?autoReconnect=true"

    $database_root_password = "TE1WeuHiflzmLMxi"
    $netpvr_username = "netpvr_user"
    $netpvr_password = "netpvr_user"
    $database_type = "mysql"
    $quartz_download_url = "http://quartz-scheduler.org/files/releases/quartz-1.6.6.zip"
}
