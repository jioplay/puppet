# == Class: mobi_netpvr_stub
#
#  installs netpvrstub component
#
# === Parameters:
#
#    $version::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_netpvr_stub" :
#           version => "5.0.0-222217",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_netpvr_stub (
    $version = $mobi_netpvr_stub::params::version,
    $vid_carrier = $mobi_netpvr_stub::params::vid_carrier,
    $vid_product = $mobi_netpvr_stub::params::vid_product,
    $vid_version = $mobi_netpvr_stub::params::vid_version,
    $vid_guideprovider = $mobi_netpvr_stub::params::vid_guideprovider,
    $db_username = $mobi_netpvr_stub::params::db_username,
    $db_password = $mobi_netpvr_stub::params::db_password,
    $hibernate_database_dialect = $mobi_netpvr_stub::params::hibernate_database_dialect,
    $jdbc_driver = $mobi_netpvr_stub::params::jdbc_driver,
    $jdbc_url = $mobi_netpvr_stub::params::jdbc_url,
    $database_root_password = $mobi_netpvr_stub::params::database_root_password,
    $netpvr_username = $mobi_netpvr_stub::params::netpvr_username,
    $netpvr_password = $mobi_netpvr_stub::params::netpvr_password,
    $database_type = $mobi_netpvr_stub::params::database_type,
    $quartz_download_url = $mobi_netpvr_stub::params::quartz_download_url,)

inherits mobi_netpvr_stub::params {
    include mobi_netpvr_stub::install, mobi_netpvr_stub::config, mobi_netpvr_stub::dbinstall
    anchor { "mobi_netpvr_stub::begin": } -> Class["mobi_netpvr_stub::install"] ->
    Class["mobi_netpvr_stub::dbinstall"] -> Class["mobi_netpvr_stub::config"] ->
    anchor { "mobi_netpvr_stub::end" :} -> os::motd::register { $name : }
}
