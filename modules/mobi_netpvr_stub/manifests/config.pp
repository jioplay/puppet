class mobi_netpvr_stub::config {
    file { "/opt/mobi-netpvr-stub/webapp/WEB-INF/classes/database.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_netpvr_stub/database.properties.erb'),
        require => Class["mobi_netpvr_stub::install"],
        notify   => Class["tomcat::service"],
    }
}
