class mobi_netpvr_stub::dbinstall {

    #test to see if we have been run
    exec { "NetPVR Stub Installation":
        command => "/bin/sh /var/mobi-netpvr-stub-database/database/install.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi-netpvr-stub-database/database/installation.done",
        logoutput => "true",
        require => File["/var/mobi-netpvr-stub-database/database"],
    }
}
