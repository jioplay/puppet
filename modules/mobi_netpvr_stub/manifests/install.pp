class mobi_netpvr_stub::install {
    package { "mobi-netpvr-stub-${mobi_netpvr_stub::version}":
    ensure => present,
    notify => Class["tomcat::service"],
   }
    file { "/var/mobi-netpvr-stub-database":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi-netpvr-stub-database/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_netpvr_stub",
        require => File["/var/mobi-netpvr-stub-database"],
    }

    file { "/var/mobi-netpvr-stub-database/database/install.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_netpvr_stub/install.sh.erb'),
        require => File["/var/mobi-netpvr-stub-database"],
    }

    file { "/var/mobi-netpvr-stub-database/database/uninstall.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_netpvr_stub/uninstall.sh.erb'),
        require => File["/var/mobi-netpvr-stub-database"],
    }

    file { "/var/mobi-netpvr-stub-database/database/create_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_netpvr_stub/create_databases.sql.erb'),
        require => File["/var/mobi-netpvr-stub-database"],
    }

    file { "/var/mobi-netpvr-stub-database/database/delete_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_netpvr_stub/delete_databases.sql.erb'),
        require => File["/var/mobi-netpvr-stub-database"],
    }

}
