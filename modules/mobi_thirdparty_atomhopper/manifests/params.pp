class mobi_thirdparty_atomhopper::params {

    $package = "mobi_thirdparty_atomhopper"
    $version = undef

    $database_url = "jdbc:mysql://mydtbvip:3306/atomhopper"
    $database_username = "atom_user"
    $database_password = "atom_user"

    $application_context_config_template = "application-context.erb"
    $atom_server_config_src = "atom-server.cfg.xml"
    $logback_config_src  = "logback.xml"

    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-atom-server/test/feed/ -w 5 -c 10"

}
