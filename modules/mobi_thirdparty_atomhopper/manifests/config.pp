class mobi_thirdparty_atomhopper::config {

    file {"/etc/atomhopper/application-context.xml":
        #source => "puppet:///modules/${module_name}/${::mobi_thirdparty_atomhopper::application_context_config_src}",
        ensure => present,
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        content => template("mobi_thirdparty_atomhopper/${mobi_thirdparty_atomhopper::application_context_config_template}"),
        notify => Class["tomcat::service"],
    }
    file {"/etc/atomhopper/atom-server.cfg.xml":
        source => "puppet:///modules/${module_name}/${::mobi_thirdparty_atomhopper::atom_server_config_src}",
        ensure => present,
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        notify => Class["tomcat::service"],
    }
    file {"/opt/mobi-thirdparty-atomhopper/webapp/WEB-INF/classes/META-INF/atom-server.cfg.xml":
        source => "puppet:///modules/${module_name}/${::mobi_thirdparty_atomhopper::atom_server_config_src}",
        ensure => present,
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        notify => Class["tomcat::service"],
    }
    file {"/etc/atomhopper/logback.xml":
        source => "puppet:///modules/${module_name}/${::mobi_thirdparty_atomhopper::logback_config_src}",
        ensure => present,
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        notify => Class["tomcat::service"],
    }
    file { "/opt/mobi-thirdparty-atomhopper/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
    }
    file { "/opt/mobi-thirdparty-atomhopper/database/atomhopper_database.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_thirdparty_atomhopper/atomhopper_database.erb'),
    }

}
