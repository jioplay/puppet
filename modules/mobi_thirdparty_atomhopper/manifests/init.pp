## == Class: mobi_thirdparty_atomhopper
#
# Manage the mobi_thirdparty_atomhopper component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_thirdparty_atomhopper (

    $package = $mobi_thirdparty_atomhopper::params::package,
    $version = $mobi_thirdparty_atomhopper::params::version,

    $database_url = $mobi_thirdparty_atomhopper::params::database_url,
    $database_username= $mobi_thirdparty_atomhopper::params::database_username,
    $database_password = $mobi_thirdparty_atomhopper::params::database_password,

    $application_context_config_template = $mobi_thirdparty_atomhopper::params::application_context_config_template,
    $atom_server_config_src = $mobi_thirdparty_atomhopper::params::atom_server_config_src,
    $logback_config_src = $mobi_thirdparty_atomhopper::params::logback_config_src,

    $icinga = $mobi_thirdparty_atomhopper::params::icinga,
    $icinga_instance = $mobi_thirdparty_atomhopper::params::icinga_instance,
    $icinga_cmd_args = $mobi_thirdparty_atomhopper::params::icinga_cmd_args,

) inherits mobi_thirdparty_atomhopper::params {

    anchor { "mobi_thirdparty_atomhopper::begin": } ->
    class { "mobi_thirdparty_atomhopper::install": } ->
    class { "mobi_thirdparty_atomhopper::config": } ->
    class { "mobi_thirdparty_atomhopper::icinga":} ->
    anchor { "mobi_thirdparty_atomhopper::end": }

    os::motd::register{ "${name}":}
}
