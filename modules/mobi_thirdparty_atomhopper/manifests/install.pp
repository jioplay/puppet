class mobi_thirdparty_atomhopper::install {
    package {"${$mobi_thirdparty_atomhopper::package}-${$mobi_thirdparty_atomhopper::version}":
      ensure => present,
      require => Class["tomcat::install"],
      notify  => Class["tomcat::service"],
    }
}
