class mobi_config_manager::install {
  if $use_publisher_endpoint == true
  {
    File {
      owner => "apache",
      group => "apache",
      mode => "0755",
      replace => false,
    }

    file { "/var/www/publisher":
      ensure => directory,
    }

    file { "/var/www/publisher/configmanager":
      ensure => directory,
      require => File["/var/www/publisher"]
    }

    package { "mobi-config-manager":
      ensure => "${mobi_config_manager::mobi_config_manager_version}",
      notify => [ Class["tomcat::service"], File["/var/www/publisher/configmanager"]]
    }

    package { "mobi-publisher-endpoint":
      ensure => "${mobi_config_manager::mobi_publisher_endpoint_version}",
      notify => [ Class["apache::service"], File["/var/www/publisher/configmanager"]]
    }
  }
  else
  {
    package { "mobi-config-manager":
      ensure => "${mobi_config_manager::mobi_config_manager_version}",
      notify => [ Class["tomcat::service"],]
    }
  }
}
