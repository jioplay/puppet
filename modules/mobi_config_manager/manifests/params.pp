class mobi_config_manager::params {

    $mobi_config_manager_version = "*"
    $mobi_publisher_endpoint_version = "*"
    $use_publisher_endpoint = true
    ### icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-config-manager/monitoring/health -w 5 -c 10"
}
