class mobi_config_manager(
   $mobi_config_manager_version = $mobi_config_manager::params::mobi_config_manager_version,
   $mobi_publisher_endpoint_version = $mobi_config_manager::params::mobi_publisher_endpoint_version,
   $use_publisher_endpoint = $mobi_config_manager::params::use_publisher_endpoint,
   ### icinga.pp
   $icinga = $mobi_config_manager::params::icinga,
   $icinga_instance = $mobi_config_manager::params::icinga_instance,
   $icinga_cmd_args = $mobi_config_manager::params::icinga_cmd_args,
)
inherits mobi_config_manager::params{
  include mobi_config_manager::install
  anchor { "mobi_config_manager::begin":} -> Class["mobi_config_manager::install"] ->
    class { "mobi_config_manager::icinga":} ->
  anchor { "mobi_config_manager::end": }
  os::motd::register { $name : }
}
