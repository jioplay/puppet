class mobi_build_machine
{
    include "os::git-branch-commit"
    include os::motd
    include bamboo_agent
    include maven
    include yum::repo::goldmaster
    include yum::repo::untested
    include development_tools
    include sshd

    Class["yum::repo::goldmaster"] -> Class["yum::repo::untested"] -> Class["development_tools"]

}
