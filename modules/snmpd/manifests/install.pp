class snmpd::install {

    package { [ "net-snmp", "net-snmp-utils", "net-snmp-libs" ]:
        ensure => $snmpd::package_ensure,
    }
}
