class snmpd::params {

    $config = "snmpd.conf"
    $service_ensure = running
    $service_enabled = true
    $package_ensure = present
}
