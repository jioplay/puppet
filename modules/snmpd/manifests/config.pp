class snmpd::config {

    file { "/etc/snmp/snmpd.conf":
        ensure  => present,
        owner  => "root",
        group  => "root",
        mode  => "0644",
        source  => "puppet://$puppetserver/modules/snmpd/$snmpd::config",
        require  => Class["snmpd::install"],
    }

    # Install files and directoires previously installed via the mobi_ops_snmpd
    # package. Hopefully we can stop using this hacktastic unconventional crap!
    # No need to install that horrible RPM.
    file { "/opt/ReachTV":
        ensure  => directory,
        recurse  => true,
        purge  => false,
        owner  => "root",
        group  => "root",
        mode  => "0755",
        source  => "puppet://$puppetserver/modules/snmpd/opt/ReachTV",
        require  => Class["snmpd::install"],
    }
}

