# = Class: snmpd
#
# This class installs the snmpd package and an override configuration.  It configures
# the snmpd service to start_up by default.
#
# == Parameters:
#
# $config::          Location of the configuration file to install as an override
#                    to what's included in the RPM.
#
# $service_ensure::          running, stopped etc for service
#
# $service_enabled::         enable service at boot
#
# $package_ensure::          latest, "version",  purged etc.
#
# == Actions:
#     Installs the snmpd package.
#
# == Requires:
#
# == Sample Usage
#       class { "snmpd":
#               $config => "snmpd.conf.qa",
#             }
#
# Remember: No empty lines between comments and class definition
#
class snmpd (
    $config = $snmpd::params::config,
    $service_ensure = $snmpd::params::service_ensure,
    $service_enabled = $snmpd::params::service_enabled,
    $package_ensure = $snmpd::params::package_ensure
)
inherits snmpd::params
{
    include snmpd::install, snmpd::config, snmpd::service


  # include module name in motd
  os::motd::register { "snmpd": }
}
