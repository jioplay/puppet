class snmpd::service {
    service { "snmpd":
        enable    => $snmpd::enable,
        ensure    => $snmpd::ensure,
        hasstatus  => true,
        hasrestart  => true,
        require    => Class["snmpd::config"],
        subscribe  => Class["snmpd::config"],
    }
}

