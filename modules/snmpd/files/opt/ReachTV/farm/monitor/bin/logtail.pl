#!/usr/bin/perl
#
# replaces logtail.c -- in Linux ($!&*^$%@*) fseek will merrily
# seek past EOF without returning error, which breaks logtail.
# WTF?
#
# This replacement ought to be a little quicker, too, since it
# can take advantage of perl's buffering.
#
# Austin David, adavid@idetic.com
# Sun May 29 23:07:19 EDT 2005

$log_filename = $ARGV[0];
$offset_filename = $ARGV[1];

if ($#ARGV < 1) {
  print "Usage: $0 <logfile> <offset filename>\n";
}

if (open OFFSET, $offset_filename) {
  $offset = <OFFSET>;
  close OFFSET;
  # print STDERR "Offset: $offset\n";
} else {
  $offset = 0;
}

open LOG, $log_filename
  or die "Error: Can't open log $log_filename for reading\n";

# fseek can't reliably tell us if we've shot past EOF;
# check the filesize
$size = -s $log_filename;
#  print "Mismatch: size $size < offset $offset\n";
if ($size >= $offset) {
  if (! seek LOG, $offset, 0) {
    # printf "Seek failed?\n";
    seek LOG, 0, 0; # rewind if seek failed
  }
}


while (<LOG>) {
  print;
}

$offset = tell LOG;
close LOG;

open OFFSET, "> $offset_filename"
  or die "Can't open offset $offset_filename for writing\n";
print OFFSET $offset;
close OFFSET;
