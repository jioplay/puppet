#!/usr/bin/perl
#
# Austin David, adavid@idetic.com
# Thu Apr 28 16:36:23 PDT 2005
#
# $Id: snmp-mux.pl,v 1.3 2005/07/28 19:54:16 adavid Exp $
#
# in /etc/snmp/snmpd-alt.conf:
# pass_persist .1.3.6.1.4.1.2021.242 /usr/bin/perl /opt/ReachTV/Farm/monitor/snmp-mux.pl
#

$|=1; # perl must be screwing with STDIN ...

# open DEBUG, "> /tmp/snmp-mux.$$";
# $t = select DEBUG;
# $| = 1;
# select $t;

while ($cmd = <STDIN>) {
  # printf DEBUG "%s: $cmd\n", scalar localtime;
  if ($cmd =~ /PING/) {
    print "PONG\n";
    next;
  }
  $oid = <STDIN>;
  # printf DEBUG "%s: OID $oid\n", scalar localtime;
  chomp $oid;
  # reject any "set" requests
  if ($cmd =~ /set/) {
    print "NONE\n";
    next;
  }

  # for "next" requests, there are three possible OIDs:
  #   - base + event + line #
  #   - base + event
  #   - base

  $OID = $oid;
  $event = $line = -1;
  $base = ".1.3.6.1.4.1.2021.242";
  if ($OID =~ /^($base)\.(\d+)\.(\d+)/) {
    $event = $2;
    $line = $3;
  } elsif ($OID =~ /^($base)\.(\d+)/) {
    $event = $2;
  }

  # print "$base : $event : $line\n";

  $DIR = "/opt/ReachTV/Farm/monitor";

  $filename = $DIR . "/" . $event . ".txt";
    # print "No such event\n";

  # getnext
  if ($cmd =~ /getnext/o) {
    if ($event != -1) {
      if ($line != -1) {
        # got base + event + line #

        # increment to the "next" line
        ++ $line;

        # does that line exist in the file?
        $filename = $DIR . "/" . $event . ".txt";
           # does the file exist at all?
        if (! -f $filename) {
          print "NONE\n";
          next;
        }
        open F, $filename;
        @lines = <F>;
        close F;

        if ($line > $#lines) {
          $event = find_next_event($event);
          $line = 0;
        }
        # (fallthrough to get)
      } else {
        # got base + event
        $line = 0;
        # (fallthrough to get)
      }
    } else {
      # only got the base
      $event = find_next_event($event);
      $line = 0;
      # (fallthrough to get)
    }

    $OID = "$base.$event.$line";
  }

  $filename = $DIR . "/" . $event . ".txt";

  # exit unless -f $filename;
  unless (open F, $filename) {
    print "NONE\n";
    next;
  }
  @lines = <F>;
  close F;

  if ($line <= $#lines) {
  #  printf "oid: $OID\ntype: string\nvalue: %s", $lines[$line];
    # printf DEBUG "returning $OID--string--%s\n", $lines[$line];
    printf "$OID\nstring\n%s", $lines[$line];
  } else {
    print "NONE\n";
  }
} # while (<STDIN>)

# end main
exit;


# 
# given a requested $event, figure out (and return) the next
# event.  
#
# Will return "undef" if there isn't a next event.
#
sub find_next_event {
  my ($event) = (@_);

  # print "event $event\n";
  opendir DH, $DIR;
  @events = sort grep s/^(\d+)\.txt$/$1/, readdir DH;
  # print "Events: @events\n";
  while ($#events >= 0 and $events[0] <= $event) {
    # print "event: ", $events[0], "\n";
    shift @events;
  }

  return $events[0];
} # find_next_event($event)
