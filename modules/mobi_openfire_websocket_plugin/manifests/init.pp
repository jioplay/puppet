## == Class: mobi_openfire_websocket_plugin
#
# Manage the mobi_openfire_websocket_plugin component.
#
# === Parameters:
#
#  mobi_openfire_websocket_plugin_package:: package name for the component
#  mobi_openfire_websocket_plugin_package_ensure:: present or latest
#
# === Actions:
#
#  Install websockets plugin for openfire server
#
# === Requires:
#
#  openfire
#
# === Examples:
#
#   class { "mobi_openfire_websocket_plugin":
#       mobi_openfire_websocket_plugin_package => "mobi-openfire-websocket-plugin-3.8.0-244351.SNAPSHOT",
#       mobi_openfire_websocket_plugin_package_ensure => "present",
#       require => Class["mobi_openfire"],
#   }
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_openfire_websocket_plugin (
    $mobi_openfire_websocket_plugin_package = $mobi_openfire_websocket_plugin::params::mobi_openfire_websocket_plugin_package,
    $mobi_openfire_websocket_plugin_package_ensure = $mobi_openfire_websocket_plugin::params::mobi_push_message_manager_package_ensure,
) inherits mobi_openfire_websocket_plugin::params {

    anchor { "mobi_openfire_websocket_plugin::begin": } ->
    class { "mobi_openfire_websocket_plugin::install": } ->
    anchor { "mobi_openfire_websocket_plugin::end": }

    os::motd::register{ "${name}":}
}
