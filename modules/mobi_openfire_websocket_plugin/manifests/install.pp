class mobi_openfire_websocket_plugin::install {
    if $::mobi_openfire_websocket_plugin::mobi_openfire_websocket_plugin_package_ensure !~ /^(absent|present|latest|ensure)$/ {
        fail("$name expects \$mobi_openfire_websocket_plugin_package_ensure to be one of: absent, present, latest, or ensure.\nPlease add version to version param instead.")
    }

    package { "${::mobi_openfire_websocket_plugin::mobi_openfire_websocket_plugin_package}":
        ensure => $::mobi_openfire_websocket_plugin::mobi_openfire_websocket_plugin_package_ensure,
    }

}
