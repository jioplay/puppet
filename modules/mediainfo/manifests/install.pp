class mediainfo::install {
    package { "mediainfo":
      ensure => $mediainfo::mediainfo_version,
    }

    package { "libmediainfo0":
      ensure => $mediainfo::libmediainfo_version,
    }

    package { "libzen0":
      ensure => $mediainfo::libzen_version,
    }
}
