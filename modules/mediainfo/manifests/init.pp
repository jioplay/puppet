# == Class: mediainfo
#
#  installs mediainfo component
#
# === Parameters:
#
#    $mediainfo_version::
#    $libmediainfo_version::
#    $libzen_version::
#
# === Requires:
#
#
# === Sample Usage
#
#   class { "mediainfo" :
#           mediainfo_version => "0.7.58",
#           libmediainfo_version => "0.7.58",
#           libzen_version => "0.4.26",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mediainfo (
    $mediainfo_version = $mediainfo::params::mediainfo_version,
    $libmediainfo_version = $mediainfo::params::libmediainfo_version,
    $libzen_version = $mediainfo::params::libzen_version,
)

inherits mediainfo::params {
    include mediainfo::install
    anchor { "mediainfo::begin": } -> Class["mediainfo::install"] ->
    anchor { "mediainfo::end" :} -> os::motd::register { $name : }
}
