class sshd::allow_root
{
    file { "/etc/ssh/sshd_config":
        ensure => present,
        source => "puppet:///modules/sshd/sshd_config",
        require => Class["sshd::install"]
    }

}
