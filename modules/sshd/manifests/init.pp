class sshd {

    include sshd::install
    include sshd::allow_root
    include sshd::service

}
