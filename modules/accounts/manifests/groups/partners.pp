# Warning
#
# Not including the @ will cause the define instance to NOT be virtual, likely
# creating that group on every system using puppet; don't do that.
#
# Use the tag parameter to accounts::group as a filtering method when collecting
# virtual resources. See the base_mgo class for an example. When in doubt just
# copy cat existing groups in this file.
#
class accounts::groups::partners {

  @accounts::group { "mgo_admin":
    gid  => "25000",
    sudo => "true",
  }

  @accounts::group { "mgo_dbadmin":
    gid  => "25001",
    sudo => "true",
  }

  @accounts::group { "yuj":
    gid  => "15000",
    sudo => "false",
  }

  @accounts::group { "sangips":
    gid  => "15001",
    sudo => "false",
  }

  @accounts::group { "walkenj":
    gid  => "15002",
    sudo => "false",
  }
  @accounts::group { "kanea":
    gid  => "15003",
    sudo => "false",
  }

  @accounts::group { "ahmeds":
    gid  => "15004",
    sudo => "false",
  }

  @accounts::group { "shartzl":
    gid  => "15005",
    sudo => "false",
  }

  @accounts::group { "chirkov":
    gid  => "15006",
    sudo => "false",
  }

  @accounts::group { "arakelia":
    gid  => "15007",
    sudo => "false",
  }

  @accounts::group { "goussar":
    gid  => "15008",
    sudo => "false",
  }

  @accounts::group { "fief":
    gid  => "15009",
    sudo => "false",
  }

  @accounts::group { "cosmea2":
    gid  => "15010",
    sudo => "false",
  }

  @accounts::group { "tenoria":
    gid  => "15012",
    sudo => "false",
  }

  @accounts::group { "chongt":
    gid  => "15013",
    sudo => "false",
  }

  @accounts::group { "beriges":
    gid  => "15014",
    sudo => "false",
  }



}
