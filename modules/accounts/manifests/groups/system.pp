# Warning
#
# Not including the @ will cause the define instance to NOT be virtual, likely
# creating that group on every system using puppet; don't do that.
#
# Use the tag parameter to accounts::group as a filtering method when collecting
# virtual resources. See the base_mgo class for an example. When in doubt just
# copy cat existing groups in this file.
#
class accounts::groups::system {

    @accounts::group { 'enclogs': }

    @accounts::group { 'rtv': }

    @accounts::group { 'apache': }

    @accounts::group { 'bamboo': sudo => true, }

    @accounts::group { 'mysql_replicator': }

    @accounts::group { 'mysql': }

    @accounts::group { 'build': sudo => true, }

    @accounts::group { 'logstash': }

    @accounts::group { 'arlanda':
      gid => '888',
    }

    @accounts::group { 'activemq':
      gid => '92',
    }

    @accounts::group { 'dba': }

    @accounts::group { 'guppet': }

    @accounts::group { 'git-daemon': }

    @accounts::group { 'gitolite': }

    @accounts::group { 'puppet': }

    @accounts::group { 'news': ensure => absent }

    @accounts::group { 'mobi_test': }
    @accounts::group { 'partner':
      gid => '10005',
    }
    @accounts::group { 'zookeeper': }

    @accounts::group { 'infotel': }

    @accounts::group { 'yume': }

    @accounts::group {'rajshri': }

    @accounts::group { 'indiacast': }

    @accounts::group { 'indiacast_tvshow': }

    @accounts::group { 'star': }

    @accounts::group { 'whatsonindia': }
}
