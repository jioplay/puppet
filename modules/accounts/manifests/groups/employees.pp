# Warning
#
# Not including the @ will cause the define instance to NOT be virtual, likely
# creating that group on every system using puppet; don't do that.
#
# Use the tag parameter to accounts::group as a filtering method when collecting
# virtual resources. See the base_mgo class for an example. When in doubt just
# copy cat existing groups in this file.
#
class accounts::groups::employees {

}
