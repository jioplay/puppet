define accounts::group  (

  $ensure = present,
  $sudo = undef,
  $gid = undef,
  $system = false,

) {

  group { $name:
    ensure => $ensure,
    system => $system,
    gid    => $gid,
  }

  if $sudo {
    if $::operatingsystemrelease >= 5.6 {
      file { "/etc/sudoers.d/${name}":
        ensure  => file,
        owner   => root,
        group   => root,
        mode    => 0440,
        content => "%${name} ALL=(ALL) ALL",
      }
    } else {
      file_line { $name:
        path => "/etc/sudoers",
        line => "%${name} ALL=(ALL) ALL"
      }
    }
  }

}
