define accounts::user  (

  $ensure = "present",
  $comment = undef,
  $groups = undef,
  $gid = undef,
  $uid = undef,
  $home = "/home/${name}",
  $managehome = true,
  $shell = "/bin/bash",
  $system = false,
  $pass = undef,

  $sshkey = undef,
  $sshkey_type = "ssh-rsa",

  # Exclude user resource for users in ldap. Useful when an ldap user needs
  # keys installed locally.
  $inldap = false,
  $group_owner = $name,
  $owner       = $name,
$permission = "0650",
) {

  if ! $inldap {
    user { $name:
      ensure     => $ensure,
      comment    => $comment,
      groups     => $groups,
      gid        => $gid,
      uid        => $uid,
      home       => $home,
      managehome => $managehome,
      shell      => $shell,
      system     => $system,
      password   => $pass,
      before     => File["$home"],
    }
  }

  # Managehome doesn't appear to remove homedir when user resource is set to
  # absent. Also explicitly state file resource for $inldap functionality.
  if ( $ensure == absent ) or ( $ensure == "absent") {
    $file_ensure = "absent"
  } else {
    $file_ensure = "directory"
  }

  file { $home:
    ensure  => $file_ensure,
    owner   => $owner,
    group   => $group_owner,
    mode    => $permission,
    recurse => true,
    force   => true,
  }

  if $sshkey {
    ssh_authorized_key { $name:
      ensure  => present,
      type    => $sshkey_type,
      key     => $sshkey,
      user    => $name,
      name    => $name,
      require => File["$home"],
    }
  }
}
