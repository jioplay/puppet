# init.pp for accounts module
#
class accounts {
  include accounts::users::contractors
  include accounts::users::employees
  include accounts::users::partners
  include accounts::users::system

  include accounts::groups::contractors
  include accounts::groups::employees
  include accounts::groups::partners
  include accounts::groups::system
}
