# Warning
#
# Not including the @ will cause the define instance to NOT be virtual, likely
# creating that user on every system using puppet; don't do that.
#
# Use the groups parameter to accounts::user or the metaparameter "tag" as a
# filtering method when collecting virtual resources. See the base_mgo class
# for an example. When in doubt just copy cat existing users in this file.
#
#
#
#
class accounts::users::system {

  @accounts::user { "enclogs":
    groups      => "enclogs",
    sshkey      => 'AAAAB3NzaC1kc3MAAAEBAIFGwJ56DDNHT59Xcd5CukL/Se6wlLyBe+B2OItUMuys8IOIgE/Uf+CZ/a9jXFzJcOPHDTih4ZZEOJGx7YSL7w7o6+sH0g7eAzz36AeR6poiyThqXU8TysEnL5zGNc1g5EGXer2woJYNgAhSXwuF47Os/8FeyD91tcGsvvpYSMDXbCI8+KpajMxQdvN6bf4Bung+4qQuy8TuDtNvYkJje5Iv5EcvD4yWZA/1jbucjcYVX+vL+w8HMwPg2Z5gl4UQvGP3/3iyW1t+OWFw8t+cpnFQyHbZzNslyJifSwfOpRdFJCHjg4ZM4FrxFzfIhISDcvZtAB14pJLk3x2LiRoZU10AAAAVAMFvUowwiLv8IAN2+B0RE3rpAVs9AAABAE9A9Tlx4N/1icCHhUE5sqKku4WtkbrK1HU6f0gVvzuk2J0FfIVPT/bV6XLYeFb5ulq5cp2Of2kIJzgWTm93f0eL/IrPatTU6Tx1gfy8gX+d0BoJ1o1mMsyOIj4GIHkkU7i5DH8zeuA2396AxBO1EHdyqiXEsHTQDXdLeJF0gN65dDIJ+3ALaGDWrPLcJVsev/PbM65M1GHieTdhXcL1VOMHY1+QqfQNFNgZxDO1bHyL/7STwqqRzfvZPNMTj011WWhgTDMZMPHLDtS6GfVtNnlVSgsbdUDJuhiWlYS8mbrkVtCuM6n352yCBaEHxgHG9uAh7ngOmj4+l5W2qTqZCKIAAAEAVA2tD4IworAPY7b+Oo8Ab2lGpMxfuGslj0kk0Le84eUVokQ3aN3VWFRYvZbSzdggjNUvmW2IzrFDK8iD1rIkvpHJyzVVM+wRtrA8SGCvIEptVWkLR66LgjVAWZIw8FyL8AYPBtGV8f9ZFCv26rCIOXoHSngc4FCG6YvBKcJc7kbL1icgOtlM0KJ6oTSKCx8cQrCNoReYjhzVzIqKXCnxSwkL03vDrDjSHr430yLxMwwBNniVMahxCFFGuA2Ql3EtHHk06ZKhyPh6b8o8c4n8fmnocOQyYk8QVisudh1WryUpWM3tYKWiXO9hgWFDPlnTerpN6yJ/tIt2yWt5gclp7g==',
    sshkey_type => "dsa",
    inldap      => true, # skips user resource portion since in ldap; manages key and homedir
    group_owner => "utilities",
  }

  $rtv_pass = $::domain ? {
    /((qa|dev)\.dmz|(oak1|vagrant|(dev|qa|devops)\.smf1|mgoint-mobi(qa|dev)?\.smf2)\.mobitv)/ => '$1$eip1B42Q$zp12iSqCRFJlp7uXlmi6d0',
    default                                           => undef,
  }

  @accounts::user { "rtv":
    comment => "MobiTV default user, forgive me father...",
    groups  => "arlanda",
    uid     => "10001",
    pass    => $rtv_pass,
  }

  @accounts::user { "activemq":
    comment => "RUN as user for activemq",
    uid     => "92",
    gid    => "activemq",
    managehome => false,
  }

  @accounts::user { "guppet":
      comment    => "guppet user for puppetmasters",
      gid => "guppet",
      groups     => ["puppet"],
      managehome => false,
  }

  @accounts::user { "git-daemon":
      comment    => "git-daemon user for static resource server",
      gid => "git-daemon",
      pass => '$1$dGnlINuf$yKluix7dzVfMqr36Etip70',
      managehome => false,
  }

  @accounts::user { "gitolite":
    comment => "gitolite user",
    gid => "gitolite",
    groups  => "gitolite",
    home => "/home/gitolite",
  }


  @accounts::user { "apache":
    comment	=> "apache user",
    uid		=> "48",
    shell	=> "/sbin/nologin",
    groups	=> "arlanda",
    home	=> "/var/www",
    managehome	=> false,
    permission	=> "0755",
  }

  @accounts::user { "bamboo":
    comment     => "bamboo user",
    groups      => [ "wheel", "sudo" ], #this can prob be removed, in accounts/manifests/groups/system.pp I added bamboo group to sudoers.
    sshkey      => 'AAAAB3NzaC1kc3MAAACBAIODfVzqGGoLptNfD5K6oa+vtgEBsGZRn9nNFdJj3ZyN9/kbBpYBdWaaFokwbUFwVQxc3TWYqP4FSESuXgTWH/I6HYcZ9mRVJZ5JQ92meGFC0xD51HPxvWep6j/XVIuU9IYRfFPLtMrzVJHw32vPCrA0GDSCgb1KYkAeFFNx8RdRAAAAFQClYfDipf08QeKyrlvRSHsen2q7QQAAAIAIlwqlPCB3sty401YnQTgEHv3N5FCJ4GLUtzn9N94J2O661yS0pZ6bhnDmDANQwT416rZMVOa458odTOt0wPqfokkJ4Do2uYwU7C0bDnyZlaVoPuVQaqIh45fR6UnCgrSitFHTr0G/bF/3NaEua/lp1BPcj+vME/uaalg1nJgMNgAAAIEAgiBblvshM2YVhuZjVKkPmj8S9LcTQ/7Q6f7n3cxmANke95ixd701Wr8y/2sPcuB1AQf54xg7z16+DUeM+rRghXhZgU1FOQJHl7ASEJy1eIl7pTPevJBcavOmwMgZUGBKfOIQU7xKAQYuttJ3VvtZAF13DvbS0sbqhyAJFv1DJug=',
    sshkey_type => "dsa",
  }

  @accounts::user { 'mysql_replicator':
      comment => 'User for Mysql replication',
      home    => '/home/mysql_replicator',
      permission => '0755',
      pass => '$1$hneoLlWO$d8t2OWnjrmOxRShaq3BZi/',
      sshkey => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDm67WjF57u3isxokx0o+Z8hP6C0ZQZlXBtsP1LcUQ6vh3/g3+eEGFU1hiBs2om9K4H3PIAtYRxatKwQ86tDl+6ZA/tl8prf5CyIFntvqKOhfuAvGT0NJXap+w0YW24w9zmKoW88vxkWAm6hPDGnL2e8tRNXv/kiDB1znz4d97bW7xRoVLdd+V9vVeyGPpUnqOGcnBwy1ZPg21WUZJTqxequqNV7NqrvUuNwLHlfKChf0Rcn8JsvR+4ZRBY01lZ8RLYjGAPEpEu0UnalS4M0pb6toO5lRsymnO2Xgv10GKOOC9r4+OZqlqAIVqXYIUlrCuSSM3B3qSIWDyZwDCrpT0T',
  }

  @accounts::user { "build":
    comment => "mobitv build user",
    pass    => '/AeTptu.ziIp.',
    home    => "/var/bamboo",
  }

  @accounts::user { "logstash":
    comment => "logstash log collection user",
  }

  @accounts::user { "oracle":
    comment => "ORACLE system user",
    pass => "neGe1Tz.i4XZ6",
    home => "/home/oracle",
    gid => "dba",
    group_owner => "dba",
  }

  # default news user should be deleted
  @accounts::user { "news":
      ensure     => absent,
      home       => "/etc/news",
      managehome => false,
  }

  #this is a test user that is needed for ftp part of the cms system
  @accounts::user { "mobitest":
      comment => "FTP test user for MobiTV",
      pass => '$1$ViydBKhD$azV7jYUbjQ7ZjJuewhvez/',
      home => "/var/Jukebox/ftpupload/mobitest",
  }

  # CMS-363
  @accounts::user { "mobi_test":
      comment => "VOD uploads test user for ftplogparser",
      pass => '$1$4VlrnLOK$o0wT/u6fI0u/P/15pcDWk0',
      home => "/home/mobi_test",
      gid => "mobi_test",
      permission => "0777",
  }

  @accounts::user { "partner":
    comment => "Partnert user for EPG ingestor",
    pass => '$1$sxp0KrEL$Bp8itqgUQad2nBwptiiRZ/',
    home => "/home/partner",
    gid => "partner",
    permission => "0777",
  }

  @accounts::user { "zookeeper":
      comment => "Zookeeper user",
      pass => '$1$/O7KhoRf$JN1WJm.c4atXrdlKGHsFQ.',
      home => "/home/zookeeper",
      gid => "zookeeper",
      permission => "0777",
  }

 #CMS-2851, CMS-2858
    @accounts::user { "acc":
     comment => "Content provider acc",
     pass    => '$1$lfN9XoNW$A/X8SuJynR.mvARCg5iNS0',
     home    => "/var/Jukebox/ftpupload/acc",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

  @accounts::user { "animalist":
     comment => "Content provider animalist",
     pass    => '$1$s7qvs6th$T837mBXFVOcnK/TZ/EC2h.',
     home    => "/var/Jukebox/ftpupload/animalist",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

 @accounts::user { "astrologer":
     comment => "Content provider astrologer",
     pass    => '$1$H6KHVenQ$lVjiP3A5zx.WW9rjA//6X1',
     home    => "/var/Jukebox/ftpupload/astrologer",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

 @accounts::user { "ap_news":
     comment => "Content provider ap_news  ",
     pass    => '$1$cwYaqV2L$67hmN4//Q3XhFplFmlbzS0',
     home    => "/var/Jukebox/ftpupload/ap_news",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

 @accounts::user { "ataku":
     comment => "Content provider ataku",
     pass    => '$1$TK/p9eFQ$80xnI7c3O8RDbky6ugleT.',
     home    => "/var/Jukebox/ftpupload/ataku",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

  @accounts::user { "azteca_america":
     comment => "Content provider azteca_america",
     pass    => '$1$eOyo4.uo$tJwwunkn1BNACAaefpTWw1',
     home    => "/var/Jukebox/ftpupload/azteca_america",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }


 @accounts::user { "campus_insiders":
     comment => "Content provider campus_insiders",
     pass    => '$1$teH9tNJ6$cJ9vTO/hr3VGKBSkcoW5V/',
     home    => "/var/Jukebox/ftpupload/campus_insiders",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

 @accounts::user { "comedy_time":
     comment => "Content provider comedy_time",
     pass    => '$1$3puFe5UU$dIVHDR/Yg8M7GZ8H9C4Yg.',
     home    => "/var/Jukebox/ftpupload/comedy_time",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

 @accounts::user { "wsj":
     comment => "Content provider wsj",
     pass    => '$1$DX8M3Q6Q$4QNH4hbH4HfMxCOSGEqyD.',
     home    => "/var/Jukebox/ftpupload/wsj",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

 @accounts::user { "pbs_kids":
     comment => "Content provider pbs_kids",
     pass    => '$1$1eRP3Cd.$wYsxXCa5B09.wnkyDf4PL/',
     home    => "/var/Jukebox/ftpupload/pbs_kids",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

@accounts::user { "rev3":
     comment => "Content provider rev3",
     pass    => '$1$d6OSKNRz$qPAbmfQwT8VAi3yJSsy7K1',
     home    => "/var/Jukebox/ftpupload/rev3",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

@accounts::user { "sourcefed":
     comment => "Content provider sourcefed",
     pass    => '$1$mlhNGj4.$UM4LoXXcoGouO5zusjfRt0',
     home    => "/var/Jukebox/ftpupload/sourcefed",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

@accounts::user { "splash_news":
     comment => "Content provider splash_news",
     pass    => '$1$hapyaqVB$Ek9D3zinLgbpvzhgzdyqD1',
     home    => "/var/Jukebox/ftpupload/splash_news",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

 @accounts::user { "ted":
     comment => "Content provider ted",
     pass    => '$1$mepzfBwr$Y/NN9cELRx4OFRPoB6Awm1',
     home    => "/var/Jukebox/ftpupload/ted",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }

 @accounts::user { "satmorntv_vod":
     comment => "Content provider satmorntv_vod",
     pass    => '$1$Gk1LtEOR$oAE4YkN/KWH4Jvt756WrB/',
     home    => "/var/Jukebox/ftpupload/satmorntv_vod",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }


 @accounts::user { "fox_television":
     comment => "Content provider fox_television",
     pass    => '$1$AOumx4XR$lp9Mp1M8ss5GgY.tsq7CY.',
     home    => "/var/Jukebox/ftpupload/fox_television",
     owner       => "apache",
     group_owner => "arlanda",
     permission  => "0775",
  }



  @accounts::user { "infotel":
      comment => "Infotel User",
      pass    => '$1$r/3B35GY$uJJoxp3H5CAN0Tqr1Md8B/',
      home    => "/var/Jukebox/ftpupload/infotel",
      managehome => false,
      gid   => "arlanda",
      owner => "48",
      group_owner => "888",
  }
  @accounts::user { "yume":
      comment => "Yume User",
      pass    => '$1$3ygr/4vt$fHRg1RXy6wrA/IRvAWyZg1',
      home    => "/home/yume",
      gid     => "yume",

  }

  @accounts::user { "star":
      comment => "Star User",
      pass    => '$1$GGYcgJWw$3tYmFeKojFmBXGt/Mhgzo.',
      home    => "/var/Jukebox/ftpupload/star",
      managehome => false,
      gid   => "arlanda",
      owner => "48",
      group_owner => "888",
  }

  @accounts::user { "rajshri":
      comment => "Rajshri User",
      pass    => '$1$6A.IT5x/$ZibGXQwYEhE3Ae/4J4qS4.',
      home    => "/var/Jukebox/ftpupload/rajshri",
      managehome => false,
      gid   => "arlanda",
      owner => "48",
      group_owner => "888",
  }

  @accounts::user { "indiacast":
      comment => "Indiacast User",
      pass    => '$1$ogzaZe/w$7MrF8YnbwJ8F6HcojGJce.',
      home    => "/var/Jukebox/ftpupload/indiacast",
      managehome => false,
      gid   => "arlanda",
      owner => "48",
      group_owner => "888",
  }

  @accounts::user { "indiacast_tvshow":
      comment => "Indiacast TV_Show User",
      pass    => '$1$sI9LRnFu$G/MaXoKOAuQwhUxuLPjj41',
      home    => "/var/Jukebox/ftpupload/indiacast_tvshow",
      managehome => false,
      gid   => "arlanda",
      owner => "48",
      group_owner => "888",

  }

  @accounts::user { "whatsonindia":
      comment => "FTP user for RIL Guide",
      pass  => '$1$GKNnboIQ$PRxbWKUZUJcYslyTqZef./',
      home  => "/var/Jukebox/ftpupload/whatsonindia",
      managehome => false,
      gid   => "arlanda",
      owner => "48",
      group_owner => "888",
  }
}
