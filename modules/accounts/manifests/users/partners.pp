# Warning
#
# Not including the @ will cause the define instance to NOT be virtual, likely
# creating that user on every system using puppet; don't do that.
#
# Use the groups parameter to accounts::user or the metaparameter "tag" as a
# filtering method when collecting virtual resources. See the base_mgo class
# for an example. When in doubt just copy cat existing users in this file.
#
class accounts::users::partners {

  # mgo accounts
  @accounts::user { "yuj":
    comment => "Josh Yu",
    uid     => "15000",
    gid     => "15000",
    pass    => '$1$BlAwg0$z9tevrDtAXt39u2fy5NFK0',
    groups  => "mgo_admin",
  }

  @accounts::user { "sangips":
    comment => "Siva Sangipa",
    uid     => "15001",
    gid     => "15001",
    pass    => '$1$ntAwg0$IMDrxMVin.Wtju/Cj5kaD1',
    groups  => "mgo_admin",
  }

  @accounts::user { "walkenj":
    comment => "Jon Walkenhorst",
    uid     => "15002",
    gid     => "15002",
    pass    => '$1$LxAwg0$PlAY1VvqaFl/bsSe0PGKi.',
    groups  => "mgo_admin",
  }

  @accounts::user { "kanea":
    comment => "Adam Kane",
    uid     => "15003",
    gid     => "15003",
    pass    => '$1$./Bwg0$dioD1QQ5rHjpvbwZrFDjt/',
    groups  => "mgo_admin",
  }

  @accounts::user { "ahmeds":
    comment => "Samir Ahmed",
    uid     => "15004",
    gid     => "15004",
    pass    => '$1$k2Bwg0$lQlbSGMgbGREtZS3aNMZS0',
    groups  => "mgo_admin",
  }

  @accounts::user { "shartzl":
    comment => "Lee Shartzer",
    uid     => "15005",
    gid     => "15005",
    pass    => '$1$M5Bwg0$3WHakay9rFjZ3mYEap3go/',
    groups  => "mgo_admin",
  }

  @accounts::user { "chirkov":
    comment => "Vadim Chirkov",
    uid     => "15006",
    gid     => "15006",
    pass    => '$1$Q9Bwg0$EwJaWeSYmh96zQAvFiTTX.',
    groups  => "mgo_admin",
  }

  @accounts::user { "arakelia":
    comment => "Sharis Arakelian",
    uid     => "15007",
    gid     => "15007",
    pass    => '$1$OCBwg0$/x7T9JU7yrDRR9nykpYH10',
    groups  => "mgo_admin",
  }

  @accounts::user { "goussar":
    comment => "Guillaume Goussard",
    uid     => "15008",
    gid     => "15008",
    pass    => '$1$qFBwg0$JpHLrcXwaqqMVbCs4BwAt.',
    groups  => "mgo_admin",
  }

  @accounts::user { "fief":
    comment => "Frank Fei",
    uid     => "15009",
    gid     => "15009",
    pass    => '$1$GJBwg0$Ldi5VLRBV/WOEnIcDh6E1/',
    groups  => "mgo_admin",
  }

  @accounts::user { "cosmea2":
    comment => "Arnold Cosme",
    uid     => "15010",
    gid     => "15010",
    pass    => '$1$uLBwg0$/ub4cqM1CU1cDqmMO7Z8q0',
    groups  => [ "mgo_admin", "mgo_dbadmin" ],
  }
  #sudoers = yes (servers = oracle, mysql)

  @accounts::user { "tenoria":
    comment => "Andres Tenorio",
    uid     => "15012",
    gid    => "15012",
    pass    => "$1$SfWFm0$6YFcZ9UKdzPm2XUERqtNG.",
    groups  => "mgo_admin",
  }

  @accounts::user { "chongt":
    comment => "Tony Chong",
    uid     => "15013",
    gid    => "15013",
    pass    => "$1$HkWFm0$tlflEITgcf5.q095oytbN.",
    groups  => "mgo_admin",
  }

  @accounts::user { "beriges":
    comment => "Srikanth Berigeda",
    uid     => "15014",
    gid    => "15014",
    pass    => "$1$CpWFm0$C931lJaJjBhNowBS29BIz1",
    groups  => "mgo_admin",
  }

}
