# Warning
#
# Not including the @ will cause the define instance to NOT be virtual, likely
# creating that user on every system using puppet; don't do that.
#
# Use the groups parameter to accounts::user or the metaparameter "tag" as a
# filtering method when collecting virtual resources. See the base_mgo class
# for an example. When in doubt just copy cat existing users in this file.
#
class accounts::users::contractors {

}
