class puppetmaster::service {
    service { $::puppetmaster::service:
        ensure     => $::puppetmaster::service_ensure,
        enable     => $::puppetmaster::service_enable,
        hasstatus  => true,
        hasrestart => true,
        subscribe  => [ Class["puppetmaster::install"], Class["puppetmaster::config"] ],
        notify     => Class["apache::service"],
    }
}
