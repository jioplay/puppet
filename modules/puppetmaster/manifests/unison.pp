class puppetmaster::unison {

  $unison_package        = $::puppetmaster::unison_package
  $unison_package_ensure = $::puppetmaster::unison_package_ensure
  $unison_ssl_primary    = $::puppetmaster::unison_ssl_primary

  if ! $unison_ssl_primary {
    fail("unison_ssl_primary is a required parameter to the puppetmaster::unison class")
  }

  package { $unison_package:
    ensure => $unison_package_ensure,
  }

  file { "/root/.ssh":
      ensure    => directory,
      owner     => root,
      group     => root,
      mode      => '0700',
      recurse   => true,
      source    => "puppet:///modules/puppetmaster/unison_ssh_key"
  }

  # unison cron
  file { "/etc/cron.d/puppet-ssl-sync":
      content => template("puppetmaster/puppet-ssl-sync.erb"),
      require => File["/root/.ssh"],
  }

}
