class puppetmaster (

  # install.pp
  $package               = $::puppetmaster::params::package,
  $package_ensure        = $::puppetmaster::params::package_ensure,
  $source_path           = $::puppetmaster::params::source_path,

  # service.pp
  $service               = $::puppetmaster::params::service,
  $service_ensure        = $::puppetmaster::params::service_ensure,
  $service_enable        = $::puppetmaster::params::service_enable,

  # config.pp
  $cert_file             = $::puppetmaster::params::cert_file,

  # unison.pp
  $unison_package        = $::puppetmaster::unison_package,
  $unison_package_ensure = $::puppetmaster::unison_package_ensure,
  $unison_ssl_primary    = $::puppetmaster::params::unison_ssl_primary,

) inherits puppetmaster::params {

  anchor { "puppetmaster::begin": } ->
  class  { "puppetmaster::install": } ->
  class  { "puppetmaster::config": } ->
  class  { "puppetmaster::unison": } ->
  class  { "puppetmaster::service": } ->
  anchor { "puppetmaster::end": }

  # include module name in motd
  os::motd::register { $name: }
}
