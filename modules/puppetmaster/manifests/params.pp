# == Class: puppetmaster::parmas
#
# This module manages puppetmaster parameters
#
# === Parameters
#
# Document parameters here
#
# [*package*]
#   Optional. Puppet server package name. In this particular case please
#   specify the entire package filename sans the .rpm extension. Using an
#   exec resource for install for reasons stated within install subclass.
#
# [*package_ensure*]
#   Optional. Puppet server package resource parameter.
#
# [*source_path*]
#   Optional. Puppet server package URL source; used within the rpm command.
#
# [*service*]
#   Optional. Puppet server service resource service name.
#
# [*service_ensure*]
#   Optional. Puppet server service resource ensure parameter.
#
# [*service_enable*]
#   Optional. Puppet server service resource enable parameter.
#
# [*cert_file*]
#   Required. Puppet server ssl certificate file to use in apache configs.
#
# [*unison_package*]
#   Optional. Unison package to use for synchronization of /var/lib/puppet/ssl
#   directories across puppet masters.
#
# [*unison_package_ensure*]
#   Optional. Unison package resource ensure paramter.
#
# [*unison_ssl_primary*]
#   Required. Destination host to periodically sync with.
#
# === Examples
#
# Not called directly, for default parameters only.
#
# === Authors
#
# Puppet::Masters <| tag == devops |>"
#
# === Copyright
#
# Copyright 2013 MobiTV
#
# This class file is not called directly
#
class puppetmaster::params {

  #############################################################################
  ###  icinga class params
  #############################################################################

  ### install.pp
  $package = "puppet-server-omnibus-3.2.3.fpm5-1.x86_64"
  $package_ensure = "present"
  $source_path = 'http://pulp.mobitv.corp/pulp/repos/qa-thirdparty'

  ### service.pp
  $service = "puppetmaster"
  $service_enable = false # off since using mod_passenger
  $service_ensure = stopped # mod_passenger

  ### config.pp
  $cert_file = undef # puppet.conf.erb

  #############################################################################
  ###  optional subclass params
  #############################################################################

  ### unison.pp
  $unison_package = "unison"
  $unison_package_ensure = present
  $unison_ssl_primary = undef # puppet-ssl-sync.erb for cron

}
