# puppet-server-omnibus package "conflicts" with puppet package within
# puppetlabs repository. It's for this reason that the usualy package
# resource yum and rpm provider types will not work. Alternatively,
# if we were to add the "obsoletes" metadata to the puppet-server-omnibus
# package then ALL hosts attempting to install puppet would get that
# package. Alas, you see below and shameful exec whose use should remain
# as scarce as a public restroom in SF.
class puppetmaster::install {

  exec { "omnibus-install":
      command => "/usr/bin/yum -y remove puppet facter hiera && /bin/rpm -i --replacefiles '${::puppetmaster::source_path}/${::puppetmaster::package}.rpm'",
      unless  => "/bin/rpm -qa | grep $::puppetmaster::package",
  }

}
