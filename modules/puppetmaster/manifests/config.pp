class puppetmaster::config {

    $cert_file = $::puppetmaster::cert_file

    # verify required params are defined
    if ! $cert_file {
        fail("cert_file is a required parameter to the puppetmaster class")
    }

    File { require => [ Class["puppetmaster::install"], Class["apache"] ], }

    file { "/etc/puppet":
        ensure  => directory,
        owner   => "puppet",
        group   => "puppet",
        mode    => "0755",
        recurse => true,
        source  => "puppet:///modules/puppetmaster/puppet"
    }

    file { "/etc/httpd/conf.d/puppet.conf":
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        content => template("puppetmaster/puppet.conf.erb"),
    }

    file { "/etc/httpd/conf.d/passenger.conf":
        ensure => present,
        owner  => "root",
        group  => "root",
        mode   => "0644",
        source => "puppet://$puppetserver/modules/puppetmaster/passenger.conf",
    }

    # # puppet master rack configs
    # file { "/etc/puppet/rack":
    #     ensure  => directory,
    #     recurse => true,
    #     owner   => "puppet",
    #     group   => "puppet",
    #     mode    => "0755",
    #     source  => "puppet://$puppetserver/modules/puppetmaster/rack",
    # }

}
