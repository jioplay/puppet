# == class: mobi_aaa_adapter_dt_identity
#
#  install identity adapter (STS)
#
# === Parameters:
#    $identity_ver::
#
# === Requires:
#
#    java
#    tomcat
#
# === Sample Usage
#
#
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_adapter_dt_identity(
    $identity_ver = $mobi_aaa_adapter_dt_identity::params::identity_ver,
    $time_server = $mobi_aaa_adapter_dt_identity::params::time_server,
    $sts_server = $mobi_aaa_adapter_dt_identity::params::sts_server,
    $etg_id = $mobi_aaa_adapter_dt_identity::params::etg_id,
    $etg_secret = $mobi_aaa_adapter_dt_identity::params::etg_secret,
    $recmngr_id = $mobi_aaa_adapter_dt_identity::params::recmngr_id,
    $recmngr_secret = $mobi_aaa_adapter_dt_identity::params::recmngr_secret,
)
inherits mobi_aaa_adapter_dt_identity::params {
    include mobi_aaa_adapter_dt_identity::install, mobi_aaa_adapter_dt_identity::config
    anchor {"mobi_aaa_adapter_dt_identity::begin": } -> Class["mobi_aaa_adapter_dt_identity::install"]
    Class["mobi_aaa_adapter_dt_identity::config"] -> anchor { "mobi_aaa_adapter_dt_identity::end" :}
    os::motd::register { $name : }
}
