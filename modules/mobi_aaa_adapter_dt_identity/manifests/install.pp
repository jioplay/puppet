class mobi_aaa_adapter_dt_identity::install {
    package { "mobi-aaa-dt-identity-manager-oauth2-${mobi_aaa_adapter_dt_identity::identity_ver}" :
        ensure => present,
  notify => Class["tomcat::service"]
    }
}
