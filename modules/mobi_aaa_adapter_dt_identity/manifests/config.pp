class mobi_aaa_adapter_dt_identity::config{
    file { "/opt/mobi-aaa/config/dt-identity-manager-oauth2/application-config-override.properties":
        ensure => file,
  content => template("mobi_aaa_adapter_dt_identity/application-config-override.properties.erb"),
  notify => Class["tomcat::service"],
  require => Class["mobi_aaa_adapter_dt_identity::install"],
    }
}
