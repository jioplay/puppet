define fragserver::locations (
    $location = "/frag",
    $content_root = undef,
    $live_buffer = undef,
    $fragment_duration = undef,
    $variants = [],
    $default_max_age = undef,
    $filter_bndw_box = undef,
    $variants_definition_file = undef,
    $super_variants = [],
    $use_file_locks = undef,
    $set_env = {},
) {

    file { "/etc/httpd/conf.d/${title}" :
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        content => template("fragserver/locations.conf.erb"),
        require => File["/etc/httpd/conf.d/frag.conf"],
        notify  => Class["apache::service"],
  }
}
