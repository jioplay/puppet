class fragserver::config {

    File {
        ensure => present,
        owner  => "root",
        group  => "root",
        mode   => "0644",
        require => Class["apache::install"],
        notify  => Class["apache::service"],
    }

    file { "/etc/httpd/conf.d/frag.conf":
        source  => "puppet://$puppetserver/modules/fragserver/frag-static.conf",
    }

    $variant_name = $::fragserver::variants_def_file
    $basename = inline_template("<%= variant_name.split('/')[-1] -%>")

    file { "/etc/httpd/conf.d/${basename}":
        source  => "puppet://$puppetserver/modules/fragserver/${variant_name}",
    }

    file { "/etc/logrotate.d/fragserver":
        source => "puppet://$puppetserver/modules/fragserver/fragserver.logrotate",
    }

    # add rsyslog files if rsyslog being used
    if defined(Class["rsyslog"]) {
        file { "/etc/rsyslog.d/mobi_fragmentserver.conf":
            ensure => present,
            notify => Class["rsyslog::service"],
            source => "puppet:///modules/fragserver/rsyslog/mobi_fragmentserver.conf",
        }
    }
}
