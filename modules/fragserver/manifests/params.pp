# Class: fragserver::params
#
# This module manages fragserver paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class fragserver::params {

  # package name within repository
  $package = $fragserver_package ? {
    ""      => "mobi-fragmentserver",
    default => $fragserver_package,
  }
  $version = ""

  $variants_def_file = "variants.ini"
}
