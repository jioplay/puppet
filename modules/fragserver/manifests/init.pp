# Class: fragserver
#
# This module manages fragserver
#
# Fragserver now uses a resource for defining most of its configuration, listed below are module parameters and resource parameters
# Module parameters:
#
# $package::           The fragserver package name
#
# $version::           The fragserver package version (if let blank, the latest available version will be installed)
#
# Resource parameters:
#
# $location::          A prefix that filters urls
#
# $content_root::      The full path to the root where to find the fragmented MPEG-4 files. Can
#                      specify multiples separated by space.
#
# $live_buffer::       How many fragments the server will put the client behind the last one
#                      created to allow buffering.
#
# $fragment_duration:: How long are fragments in seconds. Used to determine caching header values.
#
# $variants::          Available variants and bitrates.
#
# $default_max_age::   Max age specified to chches in seconds.
#
# $filter_bndw_box::   Whether or not to filter the bndw box prior sending to the client. On/Off.
#
# $variants_definition_file:: Full path to the file containing definitions of variants.
#
# $super_variants::    Specifies grouping of variants into super variants. This is required in order
#                      for the video resolution switch feature introduced in FMP4-1.5.
#
# $use_file_locks::    Whether or not to lock FMP4 files prior reading.
#
# Actions:
#
# Requires:            Apache module.
#
# Sample Usage:
# fragserver::locations { "frag-live.conf":
#   location => "/live",
#   content_root=>"/var/www/frag-live",
#   variants => [["H1:100", "H3:300", "H5:500"], ["H7:700", "H9:900", "H11:1100"]],
#   fragment_duration=>"6",
#   live_buffer=>"2",
#   default_max_age=>"600",
#   filter_bndw_box=>"On",
#   super_variants => [["H1", "H3", "H5", "H7", "H9", "H11"]],
#   variants_definition_file=>"/etc/httpd/conf.d/variants-example-swe.ini",
#   use_file_locks=>"On",
# }
#
# class { "fragserver":
#   version => "4.7.0-1253",
#        }
#
# [Remember: No empty lines between comments and class definition]
class fragserver (

    $package = $fragserver::params::package,
    $version = $fragserver::params::version,
    $variants_def_file = $fragserver::params::variants_def_file,

) inherits fragserver::params {

    anchor { "fragserver::begin": } ->
    class { "fragserver::install": } ->
    class { "fragserver::config": } ->
    anchor { "fragserver::end": }

  # include module name in motd
  os::motd::register { "$name": }
}
