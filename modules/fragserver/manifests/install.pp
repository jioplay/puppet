class fragserver::install {
  include packages
  realize(Package["mobi-logrotate-ods-config"])

  package { "${fragserver::package}":
    require => Class["apache"],
    ensure  => "$fragserver::version",
  }
}
