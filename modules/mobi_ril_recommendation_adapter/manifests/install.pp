class mobi_ril_recommendation_adapter::install {
    package { "${mobi_ril_recommendation_adapter::package_name}":
        ensure => "${mobi_ril_recommendation_adapter::package_ensure}",
        notify => Class["tomcat::service"],
    }
}
