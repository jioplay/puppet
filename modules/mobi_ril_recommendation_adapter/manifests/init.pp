## == Class: mobi_ril_recommendation_adapter
#
# Manage the mobi_ril_recommendation_adapter component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_ril_recommendation_adapter (
    $package_name = $mobi_ril_recommendation_adapter::params::package_name,
    $package_ensure = $mobi_ril_recommendation_adapter::params::package_ensure,
    $icinga = $mobi_ril_recommendation_adapter::params::icinga,
    $icinga_instance = $mobi_ril_recommendation_adapter::params::icinga_instance,
    $icinga_cmd_args = $mobi_ril_recommendation_adapter::params::icinga_cmd_args,
    $storage_recording_provider = $mobi_ril_recommendation_adapter::params::storage_recording_provider,
    $jersey_restclient_enableClientLog = $mobi_ril_recommendation_adapter::params::jersey_restclient_enableClientLog,
    $recommendation_job_enabled = $mobi_ril_recommendation_adapter::params::recommendation_job_enabled,
    $recommendation_cron_expression = $mobi_ril_recommendation_adapter::params::recommendation_cron_expression,
    $recommendation_csv_file_path =  $mobi_ril_recommendation_adapter::params::recommendation_csv_file_path,
    $recommendation_csv_file_name = $mobi_ril_recommendation_adapter::params::recommendation_csv_file_name,
    $recommendation_default_csv_file_name = $mobi_ril_recommendation_adapter::params::recommendation_default_csv_file_name,
    $default_user_id = $mobi_ril_recommendation_adapter::params::default_user_id,
    $auth_manager_endpoint_url = $mobi_ril_recommendation_adapter::params::auth_manager_endpoint_url,
    $identity_manager_endpoint_url = $mobi_ril_recommendation_adapter::params::identity_manager_endpoint_url,
    $recommendation_cron_expression = $mobi_ril_recommendation_adapter::params::recommendation_cron_expression,
    $recommendation_csv_file_path = $mobi_ril_recommendation_adapter::params::recommendation_csv_file_path,
    $rights_manager_endpoint_url = $mobi_ril_recommendation_adapter::params::rights_manager_endpoint_url,
    $auth_support_core_service = $mobi_ril_recommendation_adapter::params::auth_support_core_service,
    $default_offset = $mobi_ril_recommendation_adapter::params::default_offset,
    $series_ids_file_path = $mobi_ril_recommendation_adapter::params::series_ids_file_path,
    $default_no_of_recommendation = $mobi_ril_recommendation_adapter::params::default_no_of_recommendation,
    $current_service_id = $mobi_ril_recommendation_adapter::params::current_service_id,
    $max_allowed_channel_ids = $mobi_ril_recommendation_adapter::params::max_allowed_channel_ids,

) inherits mobi_ril_recommendation_adapter::params {

    anchor { "mobi_ril_recommendation_adapter::begin": } ->
    class { "mobi_ril_recommendation_adapter::install": } ->
    class { "mobi_ril_recommendation_adapter::config": } ->
    class { "mobi_ril_recommendation_adapter::icinga": } ->
    anchor { "mobi_ril_recommendation_adapter::end": }

    os::motd::register{ "${name}":}
}
