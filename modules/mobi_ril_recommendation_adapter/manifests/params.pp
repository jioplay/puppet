class mobi_ril_recommendation_adapter::params {
    $package_name = "mobi-ril-recommendation-adapter"
    $package_ensure = undef
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-ril-recommendation-adapter/monitoring/health -w 5 -c 10"
    $storage_recording_provider = "http://storagemanagervip:8080/mobi-storage-manager/"
    $jersey_restclient_connectionTimeout = "60"
    $jersey_restclient_readTimeout = "60"
    $jersey_restclient_maxConnections = "300"
    $jersey_restclient_enableClientLog = false
    $recommendation_job_enabled = true
    $recommendation_cron_expression = "0 0/30 * * * ?"
    $recommendation_csv_file_path = "/var/Jukebox/appdata/recommendation"
    $recommendation_csv_file_name = "Jio_Play_Recommend_Channel_Userwisde"
    $recommendation_default_csv_file_name = "Jio_Play_Recommend_Channel_Default"
    $default_user_id = "default_user_id"
    $auth_manager_endpoint_url = "http://authmanagervip:8080/mobi-aaa-auth-manager"
    $identity_manager_endpoint_url = "http://identitymanagervip:8080/mobi-aaa-identity-manager"
    $rights_manager_endpoint_url = "http://rightsmanagervip:8080/mobi-aaa-rights-manager"
    $auth_support_core_service = "false"
    $default_offset = "0"
    $default_no_of_recommendation = "10"
    $current_service_id = "core_v5_prefs"
    $max_allowed_channel_ids = "20"
}
