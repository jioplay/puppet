class mobi_ril_recommendation_adapter::config {
  file {"/opt/mobi-ril-recommendation-adapter/conf/application-config-override.xml":
    ensure  => present,
    replace => true,
    owner   => 'rtv',
    group   => 'rtv',
    mode    => '0644',
    notify  => Class["tomcat::service"],
    require => Class["mobi_ril_recommendation_adapter::install"],
    content => template("mobi_ril_recommendation_adapter/application-config-override.xml.erb"),

  }
}
