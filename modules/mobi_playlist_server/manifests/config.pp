class mobi_playlist_server::config {
    file{ "/etc/httpd/conf.d/mod_mps.conf":
        content => template('mobi_playlist_server/mod_mps.conf.erb'),
        notify   => Class["apache::service"],
   }

}
