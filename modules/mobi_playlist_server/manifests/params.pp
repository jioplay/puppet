class mobi_playlist_server::params {
  $mobi_mps_package_name = 'mobi-mps'
  $mobi_mps_package_ensure = 'present'
  $mobi_mps_conf_package_name = 'mobi-mps-conf'
  $mobi_mps_conf_package_ensure = 'present'
  $mobi_mpw_package_name = 'mobi-mpw'
  $mobi_mpw_package_ensure = 'present'
  $mobi_mpw_conf_package_name = 'mobi-mpw-conf'
  $mobi_mpw_conf_package_ensure = 'present'
  $location_match = "(?i)^/((pls/v./core/(dt|att|tmobile|sprint|verizon|uscellular|mobitv)/[^/]+/[^/]+/(linearPlaylists|staticPlaylist|epgPlaylist)/.*)|(pls/v./core/(dt|att|tmobile|sprint|verizon|uscellular|mobitv)/[^/]+/[^/]+/(version|(loadbalancer|monitoring)/health)))\$"
  $ad_broker_timeout = "0"
  $static_playlist_path = "/var/Jukebox/vod/smil"
  $playlist_path = "/var/Jukebox/linearplaylists"
}
