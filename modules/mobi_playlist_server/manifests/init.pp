## == Class: mobi_playlist_server
#
# Manage the mobi_playlist_server component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_playlist_server (
 $mobi_mps_package_name = $mobi_playlist_server::params::mobi_mps_package_name,
 $mobi_mps_package_ensure = $mobi_playlist_server::params::mobi_mps_package_ensure,
 $mobi_mps_conf_package_name = $mobi_playlist_server::params::mobi_mps_conf_package_name,
 $mobi_mps_conf_package_ensure = $mobi_playlist_server::params::mobi_mps_conf_package_ensure,
 $mobi_mpw_package_name = $mobi_playlist_server::params::mobi_mpw_package_name,
 $mobi_mpw_package_ensure = $mobi_playlist_server::params::mobi_mpw_package_ensure,
 $mobi_mpw_conf_package_name = $mobi_playlist_server::params::mobi_mpw_conf_package_name,
 $mobi_mpw_conf_package_ensure = $mobi_playlist_server::params::mobi_mpw_conf_package_ensure,
 $location_match = $mobi_playlist_server::params::location_match,
 $playlist_path = $mobi_playlist_server::params::playlist_path,
 $static_playlist_path = $mobi_playlist_server::params::static_playlist_path,
) inherits mobi_playlist_server::params {

    anchor { "mobi_playlist_server::begin": } ->
    class { "mobi_playlist_server::install": } ->
    class { "mobi_playlist_server::config": } ->
    anchor { "mobi_playlist_server::end": }

    os::motd::register{ "${name}":}
}
