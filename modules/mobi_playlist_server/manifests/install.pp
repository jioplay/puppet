class mobi_playlist_server::install {
  package{ "${::mobi_playlist_server::mobi_mps_package_name}":
    ensure =>  "${::mobi_playlist_server::mobi_mps_package_ensure}",
    notify => Class["apache::service"]
  }

  package{ "${::mobi_playlist_server::mobi_mps_conf_package_name}":
    ensure =>  "${::mobi_playlist_server::mobi_mps_conf_package_ensure}",
    notify => Class["apache::service"]
  }

  package{ "${::mobi_playlist_server::mobi_mpw_package_name}":
    ensure =>  "${::mobi_playlist_server::mobi_mpw_package_ensure}",
    notify => Class["apache::service"]
  }

  package{ "${::mobi_playlist_server::mobi_mpw_conf_package_name}":
    ensure =>  "${::mobi_playlist_server::mobi_mpw_conf_package_ensure}",
    notify => Class["apache::service"]
  }
}
