class dt::solr::install_solr_slave ($solr_config_version = latest)  {
    include(yum::repo)
    include(dt::rtv)
    include(dt::tomcat::install)
    include(dt::solr::install_solr_base)
    include(dt::solr::start)

    package {"Solr Slave Config RPM":
    ensure => $solr_config_version,
      name => "mobi-solr-slave",
      provider => yum,
      notify => Service["tomcat"],
    }

  }
