class dt::solr::install_solr_repeater ($solr_config_version = latest)  {
    include(yum::repo)
    include(dt::rtv)
    include(dt::tomcat::install)
    include(dt::solr::install_solr_base)
    include(dt::solr::start)

    package {"Solr Repeater Config RPM":
    ensure => $solr_config_version,
      name => "mobi-solr-repeater",
      provider => yum,
      notify => Service["tomcat"],
    }

  }
