class dt::solr::install_solr_base ($solr_base_version = latest)  {
    include(yum::repo)
    include(dt::rtv)
    include(dt::tomcat::install)

    package {"Solr Base RPM":
    ensure => $solr_base_version,
      name => "mobi-solr-base",
      provider => yum,
      notify => Service["tomcat"],
    }

  }
