class hadoop::x4100 {
  Class['cloudera'] -> Class[$title]

  define ddmount () {
    mount { "$title":
      device  => "LABEL=${title}" ,
      fstype  => 'ext3' ,
      ensure  => mounted ,
      options  => 'noatime' ,
      dump  => 1 ,
      pass  => 2 ,
    }
  }

  define dddir ($onmount, $owner = 0, $group = $owner ) {
    file { "$title":
      ensure  => directory ,
      owner   => $owner ,
      group   => $group ,
      require  => Mount["$onmount"]
    }
  }

  define datadisk () {
    ddmount {"$title": }
    dddir { "$title/dfs": onmount => "$title" , owner => hdfs }
    dddir { "$title/dfs/nn": onmount => "$title", owner => hdfs  }
    dddir { "$title/dfs/snn": onmount => "$title", owner => hdfs  }
    dddir { "$title/dfs/dn": onmount => "$title", owner => hdfs  }
    dddir { "$title/mapred": onmount => "$title" , owner => mapred }
    dddir { "$title/mapred/local": onmount => "$title", owner => mapred  }
    dddir { "$title/mapred/jt": onmount => "$title", owner => mapred  }
  }

  datadisk {"/d1": }
  datadisk {"/d2": }
  datadisk {"/d3": }
}
