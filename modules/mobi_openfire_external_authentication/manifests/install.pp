class mobi_openfire_external_authentication::install {
    package { $::mobi_openfire_external_authentication::package:
        ensure => $::mobi_openfire_external_authentication::package_ensure,
        notify => Class["mobi_openfire::service"],
    }
}
