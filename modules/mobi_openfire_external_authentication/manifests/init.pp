## == Class: mobi_openfire_external_authentication
#
# Manage the mobi_openfire_external_authentication component.
#
# === Parameters:
#   package::  package name of the mobi-openfire-external-authentication module
#   package_ensure:: present
#
# === Actions:
#
#   Installs the mobi-openfire-external-authentication rpm
#
# === Examples:
#
#   class { 'mobi_openfire_external_authentication':
#           package => 'mobi-openfire-external-authentication-5.4.0-258057-SNAPSHOT',
#           package_ensure => 'present',
#           require => Class["mobi_openfire"],
#    }
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_openfire_external_authentication (
    $package = $mobi_openfire_external_authentication::params::package,
    $package_ensure = $openfire_external_authentication::params::package_ensure,
) inherits mobi_openfire_external_authentication::params {

    anchor { "mobi_openfire_external_authentication::begin": } ->
    class { "mobi_openfire_external_authentication::install": } ->
    anchor { "mobi_openfire_external_authentication::end": }

    os::motd::register{ "${name}":}
}
