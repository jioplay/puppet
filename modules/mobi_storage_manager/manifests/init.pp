class mobi_storage_manager(
    ###icinga.pp
    $icinga = $mobi_storage_manager::params::icinga,
    $icinga_instance = $mobi_storage_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_storage_manager::params::icinga_cmd_args,
    ###end icinga.pp
   $mobi_storage_manager_version = $mobi_storage_manager::params::mobi_storage_manager_version,
)
inherits mobi_storage_manager::params{
  include  mobi_storage_manager::install
  anchor { "mobi_storage_manager::begin":} -> Class["mobi_storage_manager::install"] ->
    class{"mobi_storage_manager::config":} ->
    class { "mobi_storage_manager::icinga":} ->
  anchor { "mobi_storage_manager::end": }
  os::motd::register { $name : }
}
