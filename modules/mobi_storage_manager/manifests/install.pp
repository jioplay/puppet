class mobi_storage_manager::install {
    package { $::mobi_storage_manager::mobi_storage_manager_package:
      ensure => $::mobi_storage_manager::mobi_storage_manager_version,
      notify => [ Class["tomcat::service"], ]
    }
}
