class mobi_storage_manager::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-storage-manager/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    $mobi_storage_manager_version = "latest"
    $mobi_storage_manager_package = "mobi-storage-manager"
}
