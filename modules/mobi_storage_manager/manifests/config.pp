class mobi_storage_manager::config {
     file {"/opt/mobi-storage-manager/spring.d/applicationContext-010.xml":
            replace => true,
            owner   => "rtv",
            group   => "rtv",
            mode    => "0644",
            source  => "puppet:///modules/mobi_storage_manager/ril/applicationContext-010.xml",
        }
}
