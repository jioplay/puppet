class mobi_push_director::install {

    if $::mobi_push_director::mobi_push_director_package_ensure !~ /^(absent|present|latest|ensure)$/ {
        fail("$name expects \$mobi_push_director_package_ensure to be one of: absent, present, latest, or ensure.\nPlease add version to version param instead.")
    }

    package { "${::mobi_push_director::mobi_push_director_package}":
        ensure => $::mobi_push_director::mobi_push_director_package_ensure,
        notify => Class["tomcat::service"],
    }
}
