class mobi_push_director::config {
    File {
        owner => rtv,
        group => rtv,
        mode => "0644",
    }
    file { "/opt/mobi-push-director/webapp/WEB-INF/classes/mobi-push-director.properties":
        ensure => present,
        content => template("mobi_push_director/mobi-push-director.properties.erb"),
        require => Class["mobi_push_director::install"],
        notify => Class["tomcat::service"],
    }
}
