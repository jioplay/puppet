class mobi_push_director::icinga {

  if $::mobi_push_director::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::mobi_push_director::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_push_director module when icinga = true")
    }

    @@nagios_service { "check_http_mobi_push_director_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_push_director::icinga_cmd_args}",
      service_description => "check_http_mobi_push_director",
      notes => "PATH: ${::mobi_push_director::icinga_cmd_args}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_push_director::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }

  }
}

