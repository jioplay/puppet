# == Class: mobi_push_director
#
#  Installs Push Director component or the Topic Discovery
#
# === Parameters:
#
#    mobi_push_director_package::  package name of the push director component
#    mobi_push_director_package_ensure:: present or latest
#    mobi_push_director_properties_hash:: list of properties to be overridden in mobi-push-director.properties
#
# === Requires:
#
#    java
#    tomcat
#
# === Sample Usage
#
#       class { 'mobi_push_director':
#           mobi_push_director_package => 'mobi-push-director-5.3.0-229950-SNAPSHOT',
#           mobi_push_director_package_ensure => 'present',
#           mobi_push_director_properties_hash => {"auth.manager.url" => "http://authmanagervip:8080/mobi-aaa-stub-identity-manager-oauth2",
#                                                 "rights.manager.url" => "http://rightsmanagervip:8080/mobi-aaa-stub-identity-manager-oauth2",
#                                                 "memcached.servers" => "msliv01p1:11211,msliv02p1:11211",
#                                                },
#       }
#
# Remember: No empty lines between comments and class definition
#
class mobi_push_director(
    ###icinga.pp
    $icinga = $mobi_push_director::params::icinga,
    $icinga_instance = $mobi_push_director::params::icinga_instance,
    $icinga_cmd_args = $mobi_push_director::params::icinga_cmd_args,
    ###end icinga.pp
    $mobi_push_director_package = $mobi_push_director::params::mobi_push_director_package,
    $mobi_push_director_package_ensure = $mobi_push_director::params::mobi_push_director_package_ensure,
    $mobi_push_director_properties_hash = $mobi_push_director::params::mobi_push_director_properties_hash,
)
inherits mobi_push_director::params{

    anchor { "mobi_push_director::begin":} ->
    class{"mobi_push_director::install":} ->
    class{"mobi_push_director::config":} ->
    class { "mobi_push_director::icinga":} ->
    anchor { "mobi_push_director::end": }

    os::motd::register { $name : }
}
