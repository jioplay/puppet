class mobi_push_director::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-push-director/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $mobi_push_director_package = "mobi-push-director"
    $mobi_push_director_package_ensure = present
    $mobi_push_director_properties_hash = {}
}
