class saplistener::service inherits saplistener {

  # /etc/init.d/saplistener returns exit status of 0 regardless, using ps
  # redhat provider (/sbin/service) doesn't work as well, using init
  service { $service:
    ensure     => $manage_service_ensure,
    enable     => $manage_service_enable,
    hasstatus  => false,
    hasrestart => true,
    provider   => init,
    pattern    => "sap.py",
    require    => Class["saplistener::install"],
  }

  # service resources with provider set to init don't support chkconfig
  # till saplistener rc script is fixed....
  exec { "/sbin/chkconfig saplistener on":
    unless => "/usr/bin/test -h /etc/rc3.d/S*saplistener",
  }
}
