class saplistener::install inherits saplistener {

  package { $package:
    ensure => $package_ensure,
  }

  file { [ "/opt/ReachTV",
           "/opt/ReachTV/RTSP",
           "/opt/ReachTV/RTSP/DSS",
           "/opt/ReachTV/RTSP/DSS/movies/" ]:
    ensure => directory,
    owner  => "root",
    mode   => "0755",
  }
}
