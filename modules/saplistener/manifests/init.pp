# Class: saplistener
#
# This module manages saplistener
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class saplistener (
  $package = $saplistener::params::package,
  $manage_package = $saplistener::params::manage_package,
  $service = $saplistener::params::service,
  $manage_service_ensure = $saplistener::params::manage_service_ensure,
  $manage_service_enable = $saplistener::params::manage_service_enable
) inherits saplistener::params {
  include saplistener::install, saplistener::service

  # include module name in motd
  os::motd::register { "saplistener": }
}
