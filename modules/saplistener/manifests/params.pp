# Class: saplistener::params
#
# This module manages saplistener paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class saplistener::params {

  ### install.pp
  $package = "mobi-sap-dss"
  $manage_package = "present"

  ### service.pp
  $service = "saplistener"
  $manage_service_ensure = "running"
  $manage_service_enable = "true"

}
