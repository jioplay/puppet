# == Class: mobi_app_data_storage_manager::springoverride
#
#  Copies the override file and restarts tomcat
#
# === Parameters:
#
#    file  ::   spring file to be overridden
#
# === Requires:
#
#   make sure that you include the common tomcat module in the node file
#
# === Sample Usage
#
#       class { "mobi_app_data_storage_manager::springoverride" :
#           file  => 'applicationContext-010.xml'
#       }
#
# Remember: No empty lines between comments and class definition
#
class mobi_app_data_storage_manager::springoverride($file) {
    file {"/opt/mobi-app-data-storage-manager/spring.d" :
        ensure => directory,
        owner => rtv,
        group => rtv,
        mode => 755,
        recurse => true,
    }

    file {"/opt/mobi-app-data-storage-manager/spring.d/${file}":
        ensure => present,
        replace => true,
        owner => root,
        group => root,
        mode => 755,
        source => "puppet:///modules/mobi_app_data_storage_manager/${file}",
        notify => Class["tomcat::service"],
    }
    notify {"Spring override file copied": }

    File["/opt/mobi-app-data-storage-manager/spring.d"] ->
    File["/opt/mobi-app-data-storage-manager/spring.d/${file}"] ->
    Notify["Spring override file copied"]
}

