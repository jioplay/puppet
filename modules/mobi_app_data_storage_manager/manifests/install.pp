# == Class: mobi_app_data_storage_manager::install
#
#  installs storage manager component
#
# === Parameters:
#
#    version::   version of the package
#
# === Requires:
#
#     tomcat
#
# === Sample Usage
#
#  class { 'mobi_app_data_storage_manager::install':
#           version => '5.0.0-177962'
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_app_data_storage_manager::install($version) {

    package { "StorageManager":
      ensure => $version,
      name => "mobi-app-data-storage-manager",
      provider => yum,
      notify => Class["tomcat::service"],
    }

    notify {"StorageManager Install Complete": }

    Package["StorageManager"] ->
    Notify["StorageManager Install Complete"]
}
