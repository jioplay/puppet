# Class: properties
#
# Writes an artibrary java style properties file
#
# Parameters:
# file the full path to the file
# properties an array of strings containing the name=value pairs to be written to the file
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class properties ($file,
  $properties,
  $owner = "root",
  $group = "root",
  $mode = "644") {
  file {
    $file :
      ensure => present,
      replace => true,
      owner => $owner,
      group => $group,
      mode => $mode,
      content => template("properties/propertyfile.erb"),
      before => Notify["finished"],
  }
  notify {
    "finished" :
      message => "written property file $file"
  }
}
