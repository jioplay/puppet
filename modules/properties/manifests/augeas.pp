# a defined resource type to allow us to add variables to any java properties file
class properties::augeas {

    @file { "/usr/share/augeas/lenses/dist/properties.aug":
        ensure => present,
        replace => true,
        owner => "root",
        group => "root",
        mode => "644",
        source => "puppet://$puppetserver/modules/properties/properties.aug"
    }

    @package { "ruby-properties" :
        name => "ruby-1.8.5",
        ensure => present,
    }

    @package { "ruby-augeas-properties" :
        name => "ruby-augeas-0.4.1",
        ensure => present,
    }

}
