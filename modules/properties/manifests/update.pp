# == Define: properties::update
#
#  allows one to update any specific setting in a java properties file
#  using augeas.
#
# === Parameters:
#
#  $filename:: The file we want to update
#  $settings:: hash of the variable value pairs ( { test => "bob" } )
#
# === Sample Usage:
#
#     properties::update { "end_points":
#        filename => "/opt/sports/common_config/common.properties",
#        settings => {"publisher_endpoints" => "lab606j.oak1.mobitv", "standby" => "false"},
#  require => [ File["/opt/sports/common_config/common.properties"],],
#    }
#
# Remember: No empty lines between comments and class definition
#
define properties::update ($filename, $settings ) {

    include properties::augeas

    realize(File["/usr/share/augeas/lenses/dist/properties.aug"])

    realize(Package["ruby-properties"])
    realize(Package["ruby-augeas-properties"])

    $variables = unique_list(hash_keys($settings),$filename)

    properties::update_helper { $variables :
        filename => $filename,
        hash => $settings,
        lense => "Properties.lns",
        require => [ Package["ruby-properties"], Package["ruby-augeas-properties"], File["/usr/share/augeas/lenses/dist/properties.aug"] ],
    }

}
