# == Define: properties::xml
#
#  allows one to update any specific setting in a xml file using
#  augeas.
#
# === Parameters:
#
#  $filename:: The file we want to update
#  $settings:: hash of the variable value pairs ( { test => "bob" } )
#
# === Sample Usage:
#
#    properties::xml { "jdbc_xml":
#        filename => "/opt/mobi_tomcat_config/mobi_tomcat_config_8080/conf/Catalina/localhost/mobi_notification_trigger_endpoint_nfl.xml",
#        settings => { "Context/Resource/#attribute/url" => $notificaton_trigger_db_url , },
#    require => [
#         File["/opt/mobi_tomcat_config/mobi_tomcat_config_8080/conf/Catalina/localhost/mobi_notification_trigger_endpoint_nfl.xml"],
#                   ],
#    }
#
# Remember: No empty lines between comments and class definition
#
define properties::xml ($filename, $settings) {

    include properties::augeas

    realize(Package["ruby-properties"])
    realize(Package["ruby-augeas-properties"])

    $variables = unique_list(hash_keys($settings),$filename)

    properties::update_helper { $variables :
        filename => $filename,
        hash => $settings,
        lense => "Xml.lns",
        require => [ Package["ruby-augeas-properties"], Package["ruby-properties"] ],
    }

}
