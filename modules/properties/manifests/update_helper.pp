# == Define: update_helper
#
#  This is just a helper define for the properties:update xml and json
#  defines.  I wrote it this way so the others could take a hash
#  settings argument.
#
# === Parameters:
#  $filename:: The file that we need to update
#  $lense::    The augeas lense to use
#  $hash::     The parameter keys and values to be updated
#
# Remember: No empty lines between comments and class definition
#
define properties::update_helper ( $filename, $hash, $lense ) {

    $shortname = strip_string($name,$filename)

    $value = $hash[$shortname]

    augeas { "${filename}_${shortname}" :
        lens    => "$lense",
        incl    => "$filename",
        context => "/files${filename}",
        changes => [
                    "rm '$shortname'",
                    "set '$shortname' '$value'",
                    ],
        onlyif => "match $shortname[.='$value'] size == 0",
    }

}
