CREATE DATABASE IF NOT EXISTS mp;
use mp;

CREATE TABLE IF NOT EXISTS `MP_CONFIG` (
  `CONFIG_KEY` varchar(255) NOT NULL,
  `CONFIG_VALUE` varchar(4096) default NULL,
  `DEFAULT_CONFIG_VALUE` varchar(1024) default NULL,
  `DESCRIPTION` varchar(2048) default NULL,
  `LAST_MODIFIED` datetime default NULL,
  PRIMARY KEY  (`CONFIG_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `MP_ENCODER_MAPPING` (
  `ENCODING_ID` int(11) NOT NULL,
  `CLASS_NAME` varchar(1024) default NULL,
  `DESCRIPTION` varchar(1024) default NULL,
  `FILE_FORMAT` varchar(24) default NULL,
  `FILE_NAME` varchar(512) default NULL,
  PRIMARY KEY  (`ENCODING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
