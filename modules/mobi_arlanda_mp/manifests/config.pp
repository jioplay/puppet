class mobi_arlanda_mp::config {
    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/mp.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_mp/mp-tomcat-context.xml.erb'),
        require => Class["mobi_arlanda_mp::install"],
        notify   => Class["tomcat::service"],
    }

    # Database persistence files
    file { "/opt/mobi-arlanda-mp/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_mp/mp-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_arlanda_mp::install"],
    }

}
