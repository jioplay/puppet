# == Class: mobi_arlanda_mp
#
#  installs juarn component
#
# === Parameters:
#
#    $mp_version::
#    $db_username::
#    $db_password::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_arlanda_mp" :
#           version => "5.0.0-180894",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_mp (
    ###icinga.pp
    $icinga = $mobi_arlanda_mp::params::icinga,
    $icinga_instance = $mobi_arlanda_mp::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_mp::params::icinga_cmd_args,
    ###end icinga.pp
    $version = $mobi_arlanda_mp::params::version,
    $amq_broker_url = $mobi_arlanda_mp::params::amq_broker_url,
    $db_username = $mobi_arlanda_mp::params::db_username,
    $db_password = $mobi_arlanda_mp::params::db_password,
    $hibernate_database_dialect = $mobi_arlanda_mp::params::hibernate_database_dialect,
    $hibernate_database_vendor = $mobi_arlanda_mp::params::hibernate_database_vendor,
    $jdbc_driver = $mobi_arlanda_mp::params::jdbc_driver,
    $connection_test_query = $mobi_arlanda_mp::params::connection_test_query,
    $mp_db_jdbc_url = $mobi_arlanda_mp::params::mp_db_jdbc_url,
    $fragment_queue_name = $mobi_arlanda_mp::params::fragment_queue_name,
    $ril = $mobi_arlanda_mp::params::ril,
)

inherits mobi_arlanda_mp::params {
    include mobi_arlanda_mp::install, mobi_arlanda_mp::config, mobi_cms_us::tomcat_config
    anchor { "mobi_arlanda_mp::begin": } -> Class["mobi_arlanda_mp::install"] ->
    Class["mobi_arlanda_mp::config"] -> Class["mobi_cms_us::tomcat_config"] ->
    class { "mobi_arlanda_mp::icinga":} ->
    anchor { "mobi_arlanda_mp::end" :} -> os::motd::register { $name : }
}

