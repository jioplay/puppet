class mobi_arlanda_mp::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mp/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $version = "5.0.0-187724"

    $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100"

    $db_username = "mp_user"
    $db_password = "mp_user"

    # Add parameter for Fragment Queue
    $fragment_queue_name = "FFP.QUEUE"

    # Global database params
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $hibernate_database_vendor = "MYSQL"
    $jdbc_driver = "com.mysql.jdbc.Driver"
    $connection_test_query = "select 1 from dual"
    $mp_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/mp?autoReconnect=true"
    $ril = false
}
