class mobi_arlanda_mp::install {

    package { "mobi-arlanda-mp" :
        ensure => "${mobi_arlanda_mp::version}",
        notify   => Class["tomcat::service"],
    }

    #file { "/opt/mobi-arlanda-mp/database":
    #    ensure => "directory",
    #    owner  => "rtv",
    #    group  => "rtv",
    #    mode   => "0755",
    #    require => File["/opt/mobi-arlanda-mp"],
    #}

    #file { "/opt/mobi-arlanda-mp/database/postgres":
    #    ensure => "directory",
    #    owner  => "rtv",
    #    group  => "rtv",
    #    mode   => "0755",
    #    require => File["/opt/mobi-arlanda-mp/database"],
    #}

    file { ["/opt/mobi-arlanda-mp" , "/opt/mobi-arlanda-mp/database", "/opt/mobi-arlanda-mp/database/postgres", "/opt/mobi-arlanda-mp/database/postgres/eesimulator"]:
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
    }

    file { "/opt/mobi-arlanda-mp/database/postgres/mp_schema.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_mp/mp_schema.sql.erb'),
        require => File["/opt/mobi-arlanda-mp/database/postgres"],
    }

    file { "/opt/mobi-arlanda-mp/database/postgres/mp_seed.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_mp/mp_seed.sql.erb'),
        require => File["/opt/mobi-arlanda-mp/database/postgres"],
    }

    file { "/opt/mobi-arlanda-mp/database/postgres/eesimulator/mp_seed.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_arlanda_mp/eesimulator/mp_seed.sql.erb'),
        require => File["/opt/mobi-arlanda-mp/database/postgres/eesimulator"],
    }
}
