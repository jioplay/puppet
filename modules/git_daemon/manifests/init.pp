## == Class: git_daemon
#
# Manage the git_daemon component.
#
# === Parameters:
#
# base_path:  Where the repos are located
# repo_list:  A list of repos to create
# git_user:   User to run the daemon as (D: git)
# git_group: Group to run daemon as (D: git)
#
# === Requires:
#
# Packages
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class git_daemon (
    $base_path = $git_daemon::params::base_path,
    $repo_list = $git_daemon::params::repo_list,
    $git_user  = $git_daemon::params::git_user,
    $git_group = $git_daemon::params::git_group,
) inherits git_daemon::params {

    anchor { "git_daemon::begin": } ->
    class { "git_daemon::install": } ->
    class { "git_daemon::config": } ->
    anchor { "git_daemon::end": }

    class { "git_daemon::service": }
    os::motd::register{ "${name}":}
}
