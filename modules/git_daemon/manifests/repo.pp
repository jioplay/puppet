define git_daemon::repo ($directory=$title,$git_user,$git_group,$base_path) {

    $repo = "${base_path}/${directory}"

    #Initialize empty git repo in the directory.
    vcsrepo { $repo:
        ensure   => bare,
        provider => git,
        owner   => $git_user,
        group   => $git_group,
    }

    #Make sure everyone can read the repo?
    file { $repo :
        ensure => directory,
        mode => 0777,
        owner   => $git_user,
        group   => $git_group,
        require => Vcsrepo[$repo],
    }

    #Put the git-daemon file in place
    file { "$repo/git-daemon-export-ok":
        ensure => present,
        content => "This is for git-daemon export",
        mode    => 0777,
        owner   => $git_user,
        group   => $git_group,
        require => [File[$repo],Vcsrepo[$repo],],
        notify  => Class["git_daemon::service"],
    }
}
