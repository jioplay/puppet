class git_daemon::service {
    #start git-daemon
    service { "git_daemon":
        ensure => running,
        enable => true,
    }
}
