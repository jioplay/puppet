class git_daemon::config {

    # Local cast of variables for template
    $base_path = $git_daemon::base_path
    $repo_list = $git_daemon::repo_list
    $git_user  = $git_daemon::git_user
    $git_group = $git_daemon::git_group

    # put the init script in place
    file { "/etc/init.d/git_daemon":
        owner => "root",
        group => "root",
        mode => 0750,
        content => template("git_daemon/git_daemon.init.d.erb"),
    }

    file { $base_path:
        ensure => directory,
        owner => $git_user,
        group => $git_group,
        mode => 0777,
        content => template(),
    }

    #now create the repos
    notify { $repo_list: }
    git_daemon::repo { $repo_list :
        base_path => $base_path,
        git_user => $git_user,
        git_group => $git_group,
        require => [File[$base_path],Notify[$repo_list],File["/etc/init.d/git_daemon"],],
    }

}
