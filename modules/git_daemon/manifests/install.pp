class git_daemon::install {

    #install git-daemon using the packages modules
    realize(Package["git"])

    package { "git-daemon":
        ensure => present,
    }
}
