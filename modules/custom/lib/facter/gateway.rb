# hardware_platform.rb

Facter.add("gateway") do
  setcode do
    %x{route | grep default | awk '{print $2}'}.chomp
  end
end
