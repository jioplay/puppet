# hardware_platform.rb

Facter.add("broadcast") do
        setcode do
    %x{ifconfig eth0 | grep  Bcast | awk '{print $3}' | sed -e s/.*\://}.chomp
        end
end
