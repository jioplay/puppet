module Puppet::Parser::Functions 
  newfunction(:strip_string, :type => :rvalue) do |args| 
    unless args[0].is_a?(String) && args[1].is_a?(String) 
      Puppet.warning "strip_string takes two arguments, the input string and the string to remove" 
      nil 
    else 
      args[0]=args[0].gsub(args[1],"")
      args[0]
    end 
  end 
end
