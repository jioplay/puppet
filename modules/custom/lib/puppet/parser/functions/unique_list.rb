module Puppet::Parser::Functions 
  newfunction(:unique_list, :type => :rvalue) do |args| 
    unless args[0].is_a?(Array) && args[1].is_a?(String) 
      Puppet.warning "unique_list takes two arguments, the input list and the unique identifier" 
      nil 
    else 
      args[0].each do |entry|
        args[0][args[0].index(entry)]=entry+args[1]
      end
      args[0]
    end 
  end 
end
