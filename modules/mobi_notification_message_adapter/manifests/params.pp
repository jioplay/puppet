class mobi_notification_message_adapter::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args_solr = "-I $::ipaddress -p 8080 -u /mobi-message-adapter-solr/monitoring/health -w 5 -c 10"
    $icinga_cmd_args_subscriptions = "-I $::ipaddress -p 8080 -u /mobi-message-adapter-subscriptions/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    $mobi_message_adapter_solr_package = "mobi-message-adapter-solr"
    $mobi_message_adapter_subscriptions_package = "mobi-message-adapter-subscriptions"
    $mobi_message_adapter_solr_package_ensure = present
    $mobi_message_adapter_subscriptions_package_ensure = present
    $enable_generic_messages = false
    $carrier = undef
}
