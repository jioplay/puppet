class mobi_notification_message_adapter::install {

    package { "${::mobi_notification_message_adapter::mobi_message_adapter_solr_package}":
      ensure => $::mobi_notification_message_adapter::mobi_message_adapter_solr_package_ensure,
      notify => Class["tomcat::service"],
    }

    package { "${::mobi_notification_message_adapter::mobi_message_adapter_subscriptions_package}":
      ensure => $::mobi_notification_message_adapter::mobi_message_adapter_subscriptions_package_ensure,
      notify => Class["tomcat::service"],
    }
}
