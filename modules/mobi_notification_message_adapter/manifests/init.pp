class mobi_notification_message_adapter(
    ###icinga.pp
    $icinga = $mobi_notification_message_adapter::params::icinga,
    $icinga_instance = $mobi_notification_message_adapter::params::icinga_instance,
    $icinga_cmd_args_solr = $mobi_notification_message_adapter::params::icinga_cmd_args_solr,
    $icinga_cmd_args_subscriptions = $mobi_notification_message_adapter::params::icinga_cmd_args_subscriptions,
    ###end icinga.pp
   $enable_generic_messages =  $mobi_notification_message_adapter::params::enable_generic_messages,
   $carrier =  $mobi_notification_message_adapter::params::carrier,
   $mobi_message_adapter_solr_package = $mobi_notification_message_adapter::params::mobi_message_adapter_solr_package,
   $mobi_message_adapter_solr_package_ensure = $mobi_notification_message_adapter::params::mobi_message_adapter_solr_package_ensure,
   $mobi_message_adapter_subscriptions_package = $mobi_notification_message_adapter::params::mobi_message_adapter_subscriptions_package,
   $mobi_message_adapter_subscriptions_package_ensure = $mobi_notification_message_adapter::params::mobi_message_adapter_subscriptions_package_ensure,
)
inherits mobi_notification_message_adapter::params{

  anchor { "mobi_notification_message_adapter::begin":} ->
    class{"mobi_notification_message_adapter::install":} ->
    class{"mobi_notification_message_adapter::config":} ->
    class { "mobi_notification_message_adapter::icinga":} ->
  anchor { "mobi_notification_message_adapter::end": }
  os::motd::register { $name : }
}
