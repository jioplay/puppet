class mobi_notification_message_adapter::icinga {

  if $::mobi_notification_message_adapter::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::mobi_notification_message_adapter::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_notification_message_adapter module when icinga = true")
    }

    @@nagios_service { "check_http_mobi_message_adapter_solr_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_notification_message_adapter::icinga_cmd_args_solr}",
      service_description => "check_http_mobi_message_adapter_solr",
      notes => "PATH: ${::mobi_notification_message_adapter::icinga_cmd_args_solr}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_notification_message_adapter::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

    @@nagios_service { "check_http_mobi_message_adapter_subscriptions_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_notification_message_adapter::icinga_cmd_args_subscriptions}",
      service_description => "check_http_mobi_message_adapter_subscriptions",
      notes => "PATH: ${::mobi_notification_message_adapter::icinga_cmd_args_subscriptions}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_notification_message_adapter::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

  }
}
