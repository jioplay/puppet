class mobi_notification_message_adapter::config {

  # enable generic messages
  if $::mobi_notification_message_adapter::enable_generic_messages {
       file { ["/opt/mobi-notification-solr-adapter", "/opt/mobi-notification-solr-adapter/spring.d"]:
     ensure => "directory",
     owner  => "rtv",
     group  => "rtv",
     mode   => "0744",
   }
   file { "/opt/mobi-notification-solr-adapter/spring.d/applicationContext.xml":
      ensure => present,
      source => "puppet://$puppetserver/modules/mobi_notification_message_adapter/applicationContext.xml",
      owner   => "rtv",
      group   => "rtv",
      mode    => "0744",
    }
  }
  if ! $::mobi_notification_message_adapter::enable_generic_messages {
    file { "/opt/mobi-notification-solr-adapter/spring.d/applicationContext.xml":
      ensure => absent,
    }
  }


  # trial notification for managed services
  if ($::mobi_notification_message_adapter::carrier == 'managed_services' ) {

   file { ["/opt/mobi-message-adapter-subscriptions", "/opt/mobi-message-adapter-subscriptions/spring.d"]:
     ensure => "directory",
     owner  => "rtv",
     group  => "rtv",
     mode   => "0744",
   }

   file { "/opt/mobi-message-adapter-subscriptions/spring.d/configurationContext010.xml":
      ensure => present,
      source => "puppet://$puppetserver/modules/mobi_notification_message_adapter/${::mobi_notification_message_adapter::carrier}/configurationContext010.xml",
      owner   => "rtv",
      group   => "rtv",
      mode    => "0744",
      require => File["/opt/mobi-message-adapter-subscriptions/spring.d"],
      notify  => Class["tomcat::service"],
    }

  }

}
