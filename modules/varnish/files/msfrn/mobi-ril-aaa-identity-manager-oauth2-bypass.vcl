probe identity_manager_healthcheck {
   .url = "/mobi-aaa-ril-identity-manager-oauth2/monitoring/health";
   .interval = 5s;
   .timeout = 1 s;
   .window = 4;
   .threshold = 3;
   .initial = 3;
   .expected_response = 200;
}

#### mobi-aaa-ril-identity-manager-oauth2 backend hosts ####
backend identitymanager {
  .host = "identitymanagervip";
  .port = "8080";
  .probe = identity_manager_healthcheck;
}

sub vcl_recv{
   if (req.url ~ "^/identity/v5/oauth2/(.*)$") {
        set req.url = regsub(req.url, "/identity/([^/]+)/oauth2/(.*)", "/mobi-aaa-ril-identity-manager-oauth2/identity/\1/oauth2/\2");
        set req.backend = identitymanager;
        return (pass);
   }
   else if (req.url ~ "^/core/v5/session/(.*)$") {
        set req.url = regsub(req.url, "/core/([^/]+)/session/(.*)", "/mobi-aaa-ril-identity-manager-oauth2/core/\1/session/\2");
        set req.backend = identitymanager;
        return (pass);
   }
}

### Response handling
sub vcl_fetch {
    if (req.url ~ "^/mobi-aaa-ril-identity-manager-oauth2/identity/([^/]+)/oauth2/(.*)$" || req.url ~ "^/mobi-aaa-ril-identity-manager-oauth2/core/([^/]+)/session/(.*)$") {
    if (beresp.http.content-type ~ "json") {
        set beresp.do_gzip = true;
    }
    set beresp.http.Cache-Control = "no-cache";
    }
}