##
# https://www.varnish-cache.org/docs/3.0/tutorial/vcl.html
##
probe recording_manager_healthcheck {
   .url = "/mobi-recording-manager/monitoring/health";
   .interval = 5s;
   .timeout = 1 s;
   .window = 4;
   .threshold = 3;
   .initial = 3;
   .expected_response = 200;
}

#### mobi-recording-manager backend hosts ####
backend recording {
  .host = "recordingmanagervip";
  .port = "8080";
  .probe = recording_manager_healthcheck;
}

sub vcl_recv{
   if (req.url ~ "^/guide/([^/]+)/recording/(.*)$") {
        set req.url = regsub(req.url, "/guide/([^/]+)/recording/(.*)", "/mobi-recording-manager/guide/\1/recording/\2");
        set req.backend = recording;
        return (pass);
   }
}

### Response handling
sub vcl_fetch {
    if (req.url ~ "^/mobi-recording-manager/guide/([^/]+)/recording/(.*)$") {
        if (beresp.http.content-type ~ "json") {
            set beresp.do_gzip = true;
        }
    }
}


