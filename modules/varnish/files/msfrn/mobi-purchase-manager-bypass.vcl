##
# https://www.varnish-cache.org/docs/3.0/tutorial/vcl.html
##
probe purchase_manager_healthcheck {
   .url = "/mobi-aaa-purchase-manager/monitoring/health";
   .interval = 5s;
   .timeout = 1 s;
   .window = 4;
   .threshold = 3;
   .initial = 3;
   .expected_response = 200;
}

#### mobi-aaa-purchase-manager backend hosts ####
backend purchasemanagervip {
  .host = "purchasemanagervip";
  .port = "8080";
  .probe = purchase_manager_healthcheck;
}

sub vcl_recv{
  if (req.url ~ "^/core/v5/purchase/([^/]+)/([^/]+)/([^/]+)/([^/]+)/rentals\.([^/]+)") {
        set req.url = regsub(req.url, "/core/v5/purchase/([^/]+)/([^/]+)/([^/]+)/([^/]+)/rentals.([^/]+)", "/mobi-aaa-purchase-manager/core/v5/purchases/\1/\2/\3/\4/rentals.\5");
        set req.backend = purchasemanagervip;
        return (pass);
  }
  else if (req.url ~ "^/core/([^/]+)/purchase/(.*)$") {
        set req.url = regsub(req.url, "/core/([^/]+)/purchase/(.*)", "/mobi-aaa-purchase-manager/core/\1/purchase/\2");
        set req.backend = purchasemanagervip;
        return (pass);
   }
}


### Response handling
sub vcl_fetch {
    if (req.url ~ "^/mobi-aaa-purchase-manager/core/([^/]+)/purchase/(.*)$" || req.url ~ "^/mobi-aaa-purchase-manager/core/v5/purchases/([^/]+)/([^/]+)/([^/]+)/([^/]+)/rentals\.([^/]+)") {
        if (beresp.http.content-type ~ "json") {
            set beresp.do_gzip = true;
        }
    set beresp.http.Cache-Control = "no-store, no-cache, max-age=0, no-transform";
    set beresp.http.Pragma = "no-cache";
    }
}
