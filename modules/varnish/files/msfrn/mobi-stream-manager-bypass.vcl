probe stream_manager_healthcheck {
   .url = "/mobi-aaa-stream-manager/monitoring/health";
   .interval = 5s;
   .timeout = 1 s;
   .window = 4;
   .threshold = 3;
   .initial = 3;
   .expected_response = 200;
}

#### mobi-stream-manager backend hosts ####
backend streammanager {
  .host = "streammanagervip";
  .port = "8080";
  .probe = stream_manager_healthcheck;
}

sub vcl_recv{
        if (req.url ~ "^/core/([^/]+)/stream_sessions/(.*)$") {
             set req.url = regsub(req.url, "/core/([^/]+)/stream_sessions/(.*)","/mobi-aaa-stream-manager/\2");
             set req.backend = streammanager;
             return (pass);
        }
}

### Response handling
sub vcl_fetch {
    if (req.url ~ "^/mobi-aaa-stream-manager/core/([^/]+)/stream_sessions/(.*)$") {
        if (beresp.http.content-type ~ "json") {
        set beresp.do_gzip = true;
        }
    set beresp.http.Cache-Control = "no-store, no-cache, max-age=0, no-transform";
    set beresp.http.Pragma = "no-cache";
    }
}