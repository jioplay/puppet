##
# https://www.varnish-cache.org/docs/3.0/tutorial/vcl.html
##
probe personalization_manager_healthcheck {
   .url = "/mobi-personalization-manager/monitoring/health";
   .interval = 5s;
   .timeout = 1 s;
   .window = 4;
   .threshold = 3;
   .initial = 3;
   .expected_response = 200;
}

#### mobi-personalization-manager backend hosts ####
backend personalizationmanager {
  .host = "personalizationmanagervip";
  .port = "8080";
  .probe = personalization_manager_healthcheck;
}

sub vcl_recv{
   if (req.url ~ "^/core/v5/prefs/.*$") {
        set req.url = regsub(req.url, "/core/([^/]+)/prefs/(.*)", "/mobi-personalization-manager/core/\1/prefs/\2");
        set req.backend = personalizationmanager;
        return (pass);
   }
}

### Response handling
sub vcl_fetch {
    if (req.url ~ "^/mobi-personalization-manager/core/v5/prefs/.*$") {
       if (beresp.http.content-type ~ "json") {
         set beresp.do_gzip = true;
       }
       if (req.request == "GET" && beresp.status == 200) {
          set beresp.http.Cache-control = "max-age=300, no-transform, private";
       } else {
          set beresp.http.Cache-control = "no-store, no-cache, max-age=0, no-transform";
          set beresp.http.X-Frame-Options = "SAMEORIGIN";
          set beresp.http.Pragma = "no-cache";
       }
    }
}
