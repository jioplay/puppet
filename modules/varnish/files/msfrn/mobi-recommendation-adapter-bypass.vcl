##
# https://www.varnish-cache.org/docs/3.0/tutorial/vcl.html
##
probe recommendation_adapter_healthcheck {
   .url = "/mobi-ril-recommendation-adapter/monitoring/health";
   .interval = 5s;
   .timeout = 1 s;
   .window = 4;
   .threshold = 3;
   .initial = 3;
   .expected_response = 200;
}

#### mobi-ril-recommendation-adapter backend hosts ####
backend recommendationadaptervip {
  .host = "recommendationadaptervip";
  .port = "8080";
  .probe = recommendation_adapter_healthcheck;
}

sub vcl_recv{
   if (req.url ~ "^/core/([^/]+)/recommendations/(.*)$") {
        set req.url = regsub(req.url, "/core/([^/]+)/recommendations/(.*)", "/mobi-ril-recommendation-adapter/core/\1/recommendations/\2");
        set req.backend = recommendationadaptervip;
        return (pass);
   }
}
### Response handling
sub vcl_fetch {
    if (req.url ~ "^/mobi-ril-recommendation-adapter/core/([^/]+)/recommendations/(.*)$") {
       if (beresp.http.content-type ~ "json") {
           set beresp.do_gzip = true;
       }

       if (beresp.status == 200) {
           set beresp.http.Cache-control = "max-age=43200";
       } else {
           set beresp.http.Cache-control = "no-store, no-cache, max-age=0, no-transform";
           set beresp.http.Pragma = "no-cache";
       }
    }
}
