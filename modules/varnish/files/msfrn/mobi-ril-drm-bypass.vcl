probe drm_healthcheck {
   .url = "/licensemanager2/monitoring/health";
   .interval = 5s;
   .timeout = 1 s;
   .window = 4;
   .threshold = 3;
   .initial = 3;
   .expected_response = 200;
}
# drm backend hosts #
backend drm {
  .host = "drmvip";
  .port = "8080";
  .probe = drm_healthcheck;
}

sub vcl_recv{
   if (req.url ~ "^/core/v5/drm2/([^/]+)/([^/]+)/([^/]+)/register/(.*)$") {
        set req.url = regsub(req.url, "/core/v5/drm2/([^/]+)/([^/]+)/([^/]+)/register/(.*)","/licensemanager2/services/client/drmregister/\1/\2/\3/\4");
        set req.backend = drm;
        return (pass);
   }
   else if (req.url ~ "^/core/v5/drm2/([^/]+)/([^/]+)/([^/]+)/switchprofile/(.*)$") {
        set req.url = regsub(req.url, "/core/v5/drm2/([^/]+)/([^/]+)/([^/]+)/switchprofile/(.*)","/licensemanager2/services/client/switchprofile/\1/\2/\3/\4");
        set req.backend = drm;
        return (pass);
   }
   else if (req.url ~ "^/services/roap//(.*)$") {
        set req.url = regsub(req.url, "/services/roap//(.*)", "/roapserver/services/roap/\1/\2");
        set req.backend = drm;
        return (pass);
    }
   else if (req.url ~ "^/services/roap/(.*)$") {
        set req.url = regsub(req.url, "/services/roap/(.*)", "/roapserver/services/roap/\1/\2");
        set req.backend = drm;
        return (pass);
   }

}

### Response handling
sub vcl_fetch {
	if (req.url ~ "^/licensemanager2/services/client/(.*)$" || req.url ~ "^/roapserver/services/roap/(.*)$") {
		if (beresp.http.content-type ~ "json") {
			set beresp.do_gzip = true;
		}
		set beresp.http.Cache-Control = "no-store, no-cache, max-age=0, no-transform";
	}
}

