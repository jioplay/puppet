sub vcl_recv {

  # check to make sure we are looking for live fmp4
  if (req.url ~ "^/vod/.*$" )	 {
    set req.backend = vod_fmp4_pool;    
  # check to make sure we are looking for live hls
  } else if (req.url ~"^/vod_hls.*/.*$") {  
    set req.backend = vod_hls_pool;
  } else {
    error 404 "Not found.";
  }
}

