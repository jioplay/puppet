# Puppet controlled file
# probes
probe vod_fmp4_healthcheck {
  .url = "/vod/loadbalancer/health";
  .interval = 5s;
  .timeout = 1 s;
  .window = 4;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

#what is the vod hls healthcheck?
probe vod_hls_healthcheck {
  .url = "/vod_hls/loadbalancer/health";
  .interval = 5s;
  .timeout = 1 s;
  .window = 4;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

# fmp4 vod backends
backend vod_fmp4_01 {
  .host = "stfmv01p1.mum1.r4g.com";
  .port = "80";
  .probe = vod_fmp4_healthcheck;
}	

backend vod_fmp4_02 {
  .host = "stfmv02p1.mum1.r4g.com";
  .port = "80";
  .probe = vod_fmp4_healthcheck;
}	

backend vod_fmp4_03 {
  .host = "stfmv03p1.mum1.r4g.com";
  .port = "80";
  .probe = vod_fmp4_healthcheck;
}	

backend vod_fmp4_04 {
  .host = "stfmv04p1.mum1.r4g.com";
  .port = "80";
  .probe = vod_fmp4_healthcheck;
}	

#fmp4 pools 
director vod_fmp4_pool round-robin {
  {
    .backend = vod_fmp4_01;
  }
  {
    .backend = vod_fmp4_02;
  }	 
  {
    .backend = vod_fmp4_03;
  }
  {
    .backend = vod_fmp4_04;
  }
}

# hls vod backends
backend vod_hls_01 {
  .host = "stmlv01p1.mum1.r4g.com";
  .port = "80";
  .probe = vod_hls_healthcheck;
}	

backend vod_hls_02 {
  .host = "stmlv02p1.mum1.r4g.com";
  .port = "80";
  .probe = vod_hls_healthcheck;
}

backend vod_hls_03 {
  .host = "stmlv03p1.mum1.r4g.com";
  .port = "80";
  .probe = vod_hls_healthcheck;
}

backend vod_hls_04 {
  .host = "stmlv04p1.mum1.r4g.com";
  .port = "80";
  .probe = vod_hls_healthcheck;
}

#hls vod pool
director vod_hls_pool round-robin {
  {
    .backend = vod_hls_01;
  }
  {
    .backend = vod_hls_02;
  }
  {
    .backend = vod_hls_03;
  }
  {
    .backend = vod_hls_04;
  }
}
