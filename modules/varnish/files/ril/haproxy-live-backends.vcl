######################################
# Puppet controlled file
######################################

# probes
probe live_fmp4_healthcheck {
  .url = "/live/loadbalancer/health";
  .interval = 5s;
  .timeout = 1 s;
  .window = 4;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

#what is the live hls healthcheck? - this is most likely wrong
probe live_hls_healthcheck {
  .url = "/live_hls/loadbalancer/health";
  .interval = 5s;
  .timeout = 1 s;
  .window = 4;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

# fmp4 live backends
backend live_fmp4_01 {
  .host = "stfml01p1.mum1.r4g.com";
  .port = "80";
  .probe = live_fmp4_healthcheck;
}	

backend live_fmp4_02 {
  .host = "stfml02p1.mum1.r4g.com";
  .port = "80";
  .probe = live_fmp4_healthcheck;
}	

backend live_fmp4_03 {
  .host = "stfml03p1.mum1.r4g.com";
  .port = "80";
  .probe = live_fmp4_healthcheck;
}	

backend live_fmp4_04 {
  .host = "stfml04p1.mum1.r4g.com";
  .port = "80";
  .probe = live_fmp4_healthcheck;
}	

#fmp4 pools 
director live_fmp4_pool_01 round-robin {
  {
    .backend = live_fmp4_01;
  }
  {
    .backend = live_fmp4_02;
  }	 
}

director live_fmp4_pool_02 round-robin {
  {
    .backend = live_fmp4_03;
  }
  {
    .backend = live_fmp4_04;
  }
}

# hls live backends
backend live_hls_01 {
  .host = "stmml01p1.mum1.r4g.com";
  .port = "80";
  .probe = live_hls_healthcheck;
}	

backend live_hls_02 {
  .host = "stmml02p1.mum1.r4g.com";
  .port = "80";
  .probe = live_hls_healthcheck;
}

backend live_hls_03 {
  .host = "stmml03p1.mum1.r4g.com";
  .port = "80";
  .probe = live_hls_healthcheck;
}

backend live_hls_04 {
  .host = "stmml04p1.mum1.r4g.com";
  .port = "80";
  .probe = live_hls_healthcheck;
}

#hls live pool
director live_hls_pool round-robin {
  {
    .backend = live_hls_01;
  }
  {
    .backend = live_hls_02;
  }
  {
    .backend = live_hls_03;
  }
  {
    .backend = live_hls_04;
  }
}
