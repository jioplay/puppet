probe drm_healthcheck {
   .url = "/roapserver/loadbalancer/health";
   .interval = 5s;
   .timeout = 1 s;
   .window = 4;
   .threshold = 3;
   .initial = 3;
   .expected_response = 200;
}

#### DRM backend hosts ####
backend drm {
  .host = "drmvip.ci-ril.dev.smf1.mobitv";
  .port = "8080";
  .probe = drm_healthcheck;
}

sub vcl_recv{ 

    # This is to allow ROAP to go through the proxy
    if (req.request == "POST" && req.url ~ "^/services/roap/") {
        set req.url = regsub(req.url, "/services/roap/", "/roapserver/services/roap/");
        set req.backend = drm;
        return (pass);
    }
}   

