######################################
# Puppet controlled file
######################################

# probes
probe live_fmp4_healthcheck {
  .url = "/live/loadbalancer/health";
  .interval = 5s;
  .timeout = 1 s;
  .window = 4;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

#what is the live hls healthcheck? - this is most likely wrong
probe live_hls_healthcheck {
  .url = "/live_hls/loadbalancer/health";
  .interval = 5s;
  .timeout = 1 s;
  .window = 4;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

# fmp4 live backends
backend live_fmp4_01 {
  .host = "ppd2-st-fml-01.qa.smf1.mobitv";
  .port = "80";
  .probe = live_fmp4_healthcheck;
}	

backend live_fmp4_02 {
  .host = "ppd2-st-fml-02.qa.smf1.mobitv";
  .port = "80";
  .probe = live_fmp4_healthcheck;
}	

#fmp4 pools 
director live_fmp4_pool_01 round-robin {
  {
    .backend = live_fmp4_01;
  }
}

director live_fmp4_pool_02 round-robin {
  {
    .backend = live_fmp4_02;
  }
}

# hls live backends
backend live_hls_01 {
  .host = "ppd2-st-hll-01.qa.smf1.mobitv";
  .port = "80";
  .probe = live_hls_healthcheck;
}	

#hls live pool
director live_hls_pool round-robin {
  {
    .backend = live_hls_01;
  }
}
