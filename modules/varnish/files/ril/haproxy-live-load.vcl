sub vcl_recv {

  # check to make sure we are looking for live fmp4
  if (req.url ~ "^/live/.*$" )	 {
    # here we need to set the regex for the backends.
    if ( req.url ~ "^/live/[^/]*/([1-9]|[1][0-9]|2[0-7])/.*$" ) {
      set req.backend = live_fmp4_pool_01;    
    } else if ( req.url ~ "^/live/[^/]*/(2[8-9]|[3-4][0-9]|5[0-3])/.*$" ) {
      set req.backend = live_fmp4_pool_02;    
    } else {
      error 404 "Not found.";
    }
  # check to make sure we are looking for live hls
  } else if (req.url ~"^/live_hls.*/.*$") {  
    set req.backend = live_hls_pool;
  }  
}

sub vcl_deliver 
{
  if (req.url ~ "^/live/.*$" )	 {
    set resp.http.X-MobiTV-Loc = "FMP4 live";
  # check to make sure we are looking for live hls
  } else if (req.url ~"^/live_hls.*/.*$") {  
    set resp.http.X-MobiTV-Loc = "HLS live";
  } 
}
