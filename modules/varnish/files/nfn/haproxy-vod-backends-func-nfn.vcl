# Puppet controlled file
# probes
probe vod_fmp4_healthcheck {
  .url = "/vod/loadbalancer/health";
  .interval = 5s;
  .timeout = 1 s;
  .window = 4;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

#what is the vod hls healthcheck?
probe vod_hls_healthcheck {
  .url = "/vod_hls/loadbalancer/health";
  .interval = 5s;
  .timeout = 1 s;
  .window = 4;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

# fmp4 vod backends
backend vod_fmp4_01 {
  .host = "stfmv01p1.func-nfn.qa.smf1.mobitv";
  .port = "80";
  .probe = vod_fmp4_healthcheck;
}	

#fmp4 pools 
director vod_fmp4_pool round-robin {
  {
    .backend = vod_fmp4_01;
  }
}

# hls vod backends
backend vod_hls_01 {
  .host = "stmxv01p1.func-nfn.qa.smf1.mobitv";
  .port = "80";
  .probe = vod_hls_healthcheck;
}	

#hls vod pool
director vod_hls_pool round-robin {
  {
    .backend = vod_hls_01;
  }
}
