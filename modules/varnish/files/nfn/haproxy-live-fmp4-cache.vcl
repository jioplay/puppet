#fmp4 live specific
import std;
sub vcl_fetch {
  if ( req.url ~ "^/(live|recording_master)/.*$" ) {
    if ( beresp.status == 503 && beresp.http.Cache-Control ~ "max-age") {
# extract max-age with regex and convert it to a duration so it can be assigned to beresp.ttl
      set beresp.ttl = std.duration(regsub(beresp.http.Cache-Control,"^.*max-age=([0-9]+).*$", "\1s"), 1s);
    }
    
    if (beresp.http.Cache-Control ~ "no-cache") {
      return (hit_for_pass);
    }
  }
}
