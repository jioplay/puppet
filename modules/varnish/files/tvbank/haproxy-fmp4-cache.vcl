#fmp4
sub vcl_pass {
  if ( req.url ~ "^/(live|pls|vod|recordings|recording_master|record_master)/.*$" ) {
    # Needed to make sure chunked encoding is not used
    set bereq.http.Accept-Encoding = "identity";
  }
}

#fmp4
sub vcl_miss {
  if ( req.url ~ "^/(live|pls|vod|recording|recording_master|record_master)/.*$"  ) {
    # Needed to make sure chunked encoding is not used
    set bereq.http.Accept-Encoding = "identity";
  }
}

#fmp4
sub vcl_hash {
  if ( req.url ~ "^/(live|pls|vod|recording|recording_master|record_master)/.*$" ) {
    # Exclude query parameters from the hash
    hash_data(regsub(req.url, "\?.*", ""));
    if (req.http.host) {
        hash_data(req.http.host);
    } else {
        hash_data(server.ip);
    }
    return (hash);
  }
}

