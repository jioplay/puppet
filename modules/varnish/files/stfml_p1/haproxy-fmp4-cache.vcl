#fmp4
sub vcl_pass {
  if ( req.url ~ "^/frag(-mpm)?/.*$" ) {
    # Needed to make sure chunked encoding is not used
    set bereq.http.Accept-Encoding = "identity";
  }
}

#fmp4
sub vcl_miss {
  if ( req.url ~ "^/frag(-mpm)?/.*$" ) {
    # Needed to make sure chunked encoding is not used
    set bereq.http.Accept-Encoding = "identity";
  }
}

#fmp4
sub vcl_hash {
  if ( req.url ~ "^/frag(-mpm)?/.*$" ) {
    # Exclude query parameters from the hash
    hash_data(regsub(req.url, "\?.*", ""));
    if (req.http.host) {
        hash_data(req.http.host);
    } else {
        hash_data(server.ip);
    }
    return (hash);
  }
}

sub vcl_recv {
    set req.url = regsub(req.url, "/frag([^/]*)/(.*)/(start|live|continue|moov|init)/(\d+)(\.fmp4)?", "/frag\1/\2/\3/0.fmp4");
}

