import std;
sub vcl_recv {
  #make the XID avaialable in the ncsa log, on on the backend servers.
  set req.http.X-Mobitv-Reqid = req.xid;

  # check if varnish is working
  if(req.url ~ "^/varnish/(loadbalancer|monitoring)/health$") {
    error 200 "OK";
  }
}

sub vcl_error {
    set obj.http.Content-Type = "text/plain; charset=utf-8";
    set obj.http.Retry-After = "5";

    if (obj.status == 750) {
      set obj.status = 200;
      synthetic std.fileread("/etc/varnish/HD.json");
      return (deliver);
    }
    else if (obj.status == 751) {
      set obj.status = 200;
      synthetic std.fileread("/etc/varnish/SD.json");
      return (deliver);
    }

    # do not return HTML or any information about the server
    if(req.url ~ "^/varnish/(loadbalancer|monitoring)/health$") {
        synthetic {"Varnish
Service Running: ok
"};
    } else {
        synthetic obj.response;
    }

    return (deliver);
}

sub vcl_deliver {
    # clean out server-identifying headers. Also add a couple response headers used in logging.
    # Once Varnish 3.0.3 comes out, those logging headers can be replaced with %{VCL_Log:key}x in varnishncsa.
    # but for now, setting headers is the only way to do custom logging with varnishncsa

    # We should never have a response without Cache-Control, so log if we do.
    if (! resp.http.Cache-Control && resp.status == 200 && req.url !~ "^/varnish/(loadbalancer|monitoring)/health$") {
        set resp.http.X-Warn = "missing_cache_control";
    }

    set resp.http.X-Mobitv-Reqid = req.xid;

    unset resp.http.X-Varnish;
    unset resp.http.Via;
    unset resp.http.X-Powered-By;
    unset resp.http.Server;
}
