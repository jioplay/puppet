sub vcl_fetch {
   if (beresp.http.Cache-Control ~ "(no-cache|private)") {
     return (hit_for_pass);
   }
}
