define varnish::place_config(
 $use_template = false,
 $drmvip = "drmvip.${::domain}",
 $template_file = 'drm.vcl.erb'
) {
  $config = inline_template("<%= name.split('/')[-1] -%>")
  if $use_template{
    file { "${config}":
      ensure   => file,
      path     => "/etc/varnish/conf.d/${config}",
      content  => template("${module_name}/${template_file}"),
      require  => File["/etc/varnish/conf.d"],
    }
  }
  else{
    file { $config:
      ensure  => file,
      path    => "/etc/varnish/conf.d/${config}",
      source  => "puppet://$puppetserver/modules/varnish/${name}",
      require => File["/etc/varnish/conf.d"],
    }
  }
}
