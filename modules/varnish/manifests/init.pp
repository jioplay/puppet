# == Class: varnish
#
# This class installs and manages varnish on redhat based systems.
#
# === Parameters:
#
#   ### install.pp
#   $package::  string, name attr of varnish package resource. Default: "varnish"
#   $package_ensure:: string, ensure attribute to the varnish package resource. Default: "running"
#
#   ### config.pp
#   $default_backed_port:: string, port to listen on defined in backend stanza within default.vcl. Default: "8080"
#   $default_connect_timeout::string, Default 0.7
#   $default_first_byte_timeout:: string, Default 60s
#   $default_between_bytes_timeout:: string, Default 60s
#   $varnish_sysconfig:: string, file in source attr of varnish sysconfig file resource. Default: "varnish.sysconfig"
#   $varnishncsa_sysconfig:: string, file in source attr of varnishncsa sysconfig file resource. Default: "varnishncsa.sysconfig"
#   $use_logrotate:: boolean, false removes the varnish logrotate file installed by package. Default: "false"
#   $configs:: array, additional vcl files to include. Files added to conf.d and included in default.vcl, order matters. Default: []
#   $secret:: string, pssst...generates file called secret with string content. Default:
#
#   ### service.pp
#   $service:: string, name attr in varnish service resource. Default: "varnish"
#   $service_ensure:: string, ensure attr to varnish service resource. Default: running
#   $service_enable:: string, enable attr to varnish service resource. Default: true
#   $varnishncsa_service:: string, name attr in varnishncsa service resource. Default: "varnishncsa"
#   $varnishncsa_service_ensure:: string, ensure attr to varnishncsa service resource. Default: running
#   $varnishncsa_service_enable:: string, enable attr to varnishncsa service resource. Default: true
#
#   ### icinga.pp
#   $icinga:: boolean, true will include the specified icinga/nagios service check in monitoring for this host. Default: false
#   $icinga_instance:: string, hostname of icinga/nagios server responsible for performing active checks. Required when using icinga. Default: undef
#   $icinga_cmd_args:: string, defines service check command arguments that will be used to monitor this host. Default: something_sane_touch_at_own_risk
#
# === Actions:
#
# Installs and configures the varnish reverse caching proxy packages and services.
#
# === Requires:
#
# === Examples:
#
#   include varnish
#
#   ----------------------------------------------------------------------------
#
#   An example with additional vcl files. The order determines the order in
#   which the include statements are written to default.vcl. Paths are only used
#   within the "source => " attribute and otherwise stripped. Files are placed
#   in /etc/varnish/conf.d/.
#
#   class { "varnish":
#     config_files  => ["stfmp_p4/mobi.vcl", "other.vcl"] },
#   }
#
#   ----------------------------------------------------------------------------
#
#   Without varnishncsa and with monitoring (exported resources required).
#
#   class { "varnish":
#     use_logrotate              => true,
#     varnishncsa_service_ensure => stopped,
#     varnishncsa_service_enable => false,
#     icinga                     => true,
#   }
#
#
# [Remember: No empty lines between comments and class definition]
#
class varnish (
  # install.pp
  $package = $varnish::params::package,
  $package_ensure = $varnish::params::package_ensure,

  # config.pp
  $configs = $varnish::params::configs,
  $template_configs = $varnish::params::template_configs,
  $varnish_sysconfig = $varnish::params::varnish_sysconfig,
  $varnish_storage_size = $varnish::params::varnish_storage_size,
  $varnish_listen_port = $varnish::params::varnish_listen_port,
  $thread_pools = $varnish::params::thread_pools,
  $session_linger = $varnish::params::session_linger,
  $varnishncsa_sysconfig = $varnish::params::varnishncsa_sysconfig,
  $use_logrotate = $varnish::params::use_logrotate,
  $default_backend_ip = $varnish::params::default_backend_ip,
  $default_backend_port = $varnish::params::default_backend_port,
  $default_connect_timeout = $varnish::params::default_connect_timeout,
  $default_first_byte_timeout = $varnish::params::default_first_byte_timeout,
  $default_between_bytes_timeout = $varnish::params::default_between_bytes_timeout,

  # service.pp
  $service = $varnish::params::service,
  $service_ensure = $varnish::params::service_ensure,
  $service_enable = $varnish::params::service_enable,
  $varnishncsa_service = $varnish::params::varnishncsa_service,
  $varnishncsa_service_ensure = $varnish::params::varnishncsa_service_ensure,
  $varnishncsa_service_enable = $varnish::params::varnishncsa_service_enable,

  # icinga.pp
  $icinga = $varnish::params::icinga,
  $icinga_instance = $varnish::params::icinga_instance,
  $icinga_cmd_path = $varnish::params::icinga_cmd_path,

  ### ganglia.pp
  $ganglia = $varnish::params::ganglia,

) inherits varnish::params {

  anchor { "varnish::begin": } ->
  class { "varnish::install": } ->
  class { "varnish::config": } ->
  class { "varnish::icinga": } ->
  class { "varnish::ganglia": } ->
  anchor { "varnish::end": }

  class { "varnish::service": }

  # include module name in motd
  os::motd::register { "${name}": }
}
