class varnish::install {
  package { $::varnish::package:
    ensure => $::varnish::package_ensure,
  }
}
