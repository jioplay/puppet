define varnish::place_template_config(
 $template_file,
 $file_path,
 $template_hash = {},
) {
  #$parent_directories = generate_parent_directories($file_path)

  #file{ $parent_directories:
  #  ensure => directory,
  #  before => File[$file_path]
  #}

  file { $file_path:
    ensure   => file,
    content  => template("${module_name}/${template_file}"),
    require => File['/etc/varnish/conf.d' ] #making function less generic cos of clash with config.pp :(
  }
}
