class varnish::ganglia {

    if $::varnish::ganglia {

        if ! defined("ganglia::gmond") {
            fail("ganglia set to true in $name but ganglia::gmond not declared, skipping ganglia plugin install.")
        }

        File {
            ensure  => present,
            owner   => "root",
            group   => "root",
            mode    => "0644",
            require => Package["ganglia-gmond"],
            notify  => Service["gmond"],
        }

        file { "/etc/ganglia/conf.d/varnish.pyconf":
            source  => "puppet:///modules/varnish/ganglia/varnish.pyconf",
        }

        file { "/usr/lib64/ganglia/python_modules/varnish.py":
            source => "puppet:///modules/varnish/ganglia/varnish.py",
        }
    }
}
