class varnish::service {
  service { $::varnish::service:
    ensure     => $::varnish::service_ensure,
    enable     => $::varnish::service_enable,
    hasstatus  => true,
    hasrestart => true,
    subscribe  => Class[varnish::config],
  }

  service { $::varnish::varnishncsa_service:
    ensure     => $::varnish::varnishncsa_service_ensure,
    enable     => $::varnish::varnishncsa_service_enable,
    hasstatus  => true,
    hasrestart => true,
    subscribe  => [Service["$::varnish::service"], Class[varnish::config]],
  }
}
