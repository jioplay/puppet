# == Class: varnish::params
#
# This class manages varnish parameters
#
#
class varnish::params {

  ### install.pp
  $package = "varnish"
  $package_ensure = present

  ### config.pp
  $default_backend_ip = "127.0.0.1"
  $default_backend_port = "8080"
  $default_connect_timeout = "0.7s"
  $default_first_byte_timeout = "60s"
  $default_between_bytes_timeout = "60s"
  $varnish_sysconfig = "varnish.sysconfig"
  $varnishncsa_sysconfig = "varnishncsa.sysconfig"
  $varnish_storage_size = undef
  $varnish_listen_port = undef
  $thread_pools = "4"
  $session_linger = "10"
  # we log & rotate with varnishncsa, so remove varnish logrotate file
  $use_logrotate = false

  $configs = []
  $template_configs = {}
  $secret = '0u&pNKf0h'

  ### service.pp
  $service = "varnish"
  $service_ensure = running
  $service_enable = true
  $varnishncsa_service = "varnishncsa"
  $varnishncsa_service_ensure = running
  $varnishncsa_service_enable = true

  ### ganglia.pp
  $ganglia = false

  ### icinga.pp
  $icinga = true
  $icinga_instance = "icinga"
  $icinga_cmd_path = "-u /varnish/loadbalancer/health -w 2 -c 3"

  #############################################################################
  ###  optional subclass params
  #############################################################################

  ### devel.pp
  $package_devel = "varnish-libs-devel"
  $package_devel_ensure = present

}
