# Class: varnish::ssl
#
# This class installs the devel libraries for varnish
#
# Parameters:
# - The $package_devel is the name of the varnish devel package
# - The $package_devel_ensure corresponds to the packages ensure attribute
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
#
class varnish::devel (
  $package_devel = $::varnish::params::pacakge_devel,
  $package_devel_ensure = $::varnish::params::pacakge_devel_ensure,
) inherits varnish::params {

  package { "varnish-devel":
    name    => $package_devel,
    ensure  => $package_devel_ensure,
    require => Class["varnish::install"],
  }

  # include module name in motd
  os::motd::register { "${name}": }
}
