class varnish::icinga {

    if $::varnish::icinga {

        if ! $::varnish::icinga_instance {
            fail("Must provide icinga_instance parameter to apache module when icinga = true")
        }

        if ! defined("icinga::register") {
            fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
        }

        @@nagios_service { "check_http_varnish_${::fqdn}":
            host_name             => $::fqdn,
            check_command         => "check_http! -I $::ipaddress -p $::varnish::varnish_listen_port $::varnish::icinga_cmd_path",
            service_description   => "check_http_varnish",
            normal_check_interval => "15", # mildly aggressive
            use                   => "generic-service",
            tag                   => "icinga_instance:${::varnish::icinga_instance}",
            target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
            notes                 => "Path: -I $::ipaddress -p $::varnish::varnish_listen_port $::varnish::icinga_cmd_path",
            notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
        }
    }
}
