class varnish::config {
  if ! $::varnish::varnish_storage_size {
    fail("\$varnish_storage_size is a required parameter in the varnish class.")
  }

  if ! $::varnish::varnish_listen_port {
    fail("\$varnish_listen_port is a required parameter in the varnish class.")
  }

  File {
    ensure  => file,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => Class["varnish::install"],
  }

  # if using varnishncsa for logging
  if ! $::varnish::use_logrotate {
    file { "/etc/logrotate.d/varnish":
      ensure => absent,
    }
    # cron cleanup: delete older than 45 days, compress older than one day
    file { "/etc/cron.d/varnishncsa_cleanup":
      ensure => present,
      source => "puppet://$puppetserver/modules/varnish/varnishncsa_cleanup.cron",
    }
  }

  file { "/etc/sysconfig/varnish":
    content => template("varnish/haproxy-load.sysconfig.erb"),
    #source => "puppet://$puppetserver/modules/varnish/${::varnish::varnish_sysconfig}",
  }

  file { "/etc/sysconfig/varnishncsa":
    source => "puppet://${::puppetserver}/modules/varnish/${::varnish::varnishncsa_sysconfig}",
  }

  file { "/etc/varnish/secret":
    content => "${::varnish::secret}",
  }

  file { "/etc/varnish/default.vcl":
    ensure  => file,
    content => template("varnish/default.vcl.erb"),
  }

  file { "/etc/varnish/conf.d":
      ensure => directory,
  }
  varnish::place_config { "health.vcl": }

  if ! empty($::varnish::configs) {
    varnish::place_config { $::varnish::configs:
      use_template => $::varnish::use_template
    }
  }
  if ! empty($::varnish::template_configs) {
    create_resources(varnish::place_template_config,$::varnish::template_configs)
  }


  file { "/etc/varnish/conf.d/backends.vcl":
      owner   => root,
      group   => root,
      mode    => "0644",
      content => template("varnish/backends.vcl.erb"),
      require => File["/etc/varnish/conf.d"],
  }

  file { "/etc/varnish/conf.d/backend-healthchecks.vcl":
      owner   => root,
      group   => root,
      mode    => "0644",
      content => template("varnish/healthchecks.vcl.erb"),
      require => File["/etc/varnish/conf.d"],
  }

  file { "/etc/varnish/conf.d/channel_rules.vcl":
      owner   => root,
      group   => root,
      mode    => "0644",
      content => template("varnish/channel_rules.vcl.erb"),
      require => File["/etc/varnish/conf.d"],
  }

  file { "/etc/varnish/conf.d/rewrite_rules.vcl":
      owner   => root,
      group   => root,
      mode    => "0644",
      content => template("varnish/rewrite_rules.vcl.erb"),
      require => File["/etc/varnish/conf.d"],
  }
}
