# Class: fragcontroller
#
# This module manages fragcontroller
#
# Parameters:
#
# $create_root_directory::  If set to true, the FragFile Controller creates the output-base directory if it does not exist on application start. (D: False)
#
# $create_directories::     If set to true, the FragFile Controller creates the output-base directory if it does not exist on application start (D: False)
#
# $fragfile_writer_flags::  This is a list of additional flags that will be passed to the FragFile Writer instances. (D: N/A)
#
# $fragfile_writer_variant_flags:: Similar to the fragfile-writer-flags above but this can be used to specify flags only for some variants. (D: N/A)
#
# $rest_server_port::       Port to access rest-API on. (D: 4311)
#
# $adjust_start_time::      If set to true, start time for the channel will be aligned within the same GOP for the fragments available variants (D: True)
#
# $mode::                   legacy parameter, high-availability or single-node-append (D: high-availability)
#
# $sdp_base::               Specifies the root where to find the sdp files on disk. (D: N/A)
#
# $output_base::            Specifies the root where to write the fragmented MPEG-4 files on disk. This also specifies where to write the live, mfra and content license files. (D: N/A)
#
# $variants::               Specifies which variants that are available. Variants are configured as a list where each variant should be expressed as a string. When the output
#                           format is ts, each variant must be on the form name:bitrate, where bitrate is expressed in kbps. When output format is fmp4, only the name is necessary. (D: N/A)
#
# $filter_configurations::  Specifies whihch filter configuration files to use. Should be a commma-separated list of names. (D: N/A)
#
# $license_manager_v1_url:: Only valid when output format is fmp4. Specifies the host to the 1.2 license manager that the FragFile Controller communicates with in order to get the AES key
#                            and the content license (MBDL) that are used by the FragFile Writer to encrypt the media. (D: N/A)
#
# $license_manager_v2_url:: Valid for both fmp4 and HLS output. Specifies the url to the 2.0 license manager that the FragFile Controller Communicates with in order to get the AES key and
#                            the STKM that are used by the FragFile Writer to encrypt the media. (D: N/A)
#
# $roap_server_url::        Specifies the url to the roap server that clients should communicate with for this generated content. (D: N/A)
#
# $license_manager_timeout:: Only valid when output format is fmp4. Specifies the license manager request timeout in seconds. (D: 3)
#
# $license_manager_retrycount:: Only valid when output format is fmp4. Specifies the number of request attempts the FragFile Controller will do when trying to contact the license manager. (D: 3)
#
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
#  This is a helper-resource for filters
#
#   @fragcontroller::filtergroup { "fmp4-hd.json":
#         variants => [ "H40:2500" ],
#         channels => [ "201" ],
#         arguments => [ "--limit-num-fragments", "--fragment-length=6.0"],
#         fragfile_writer_flags => ["--limit-num-fragments", "--fragment-length=6.0"],
#         override_output_base => "/var/www/frag-live",
#       }
#
# class { "fragcontroller":
#    #node.json parameters:
#    create_directories => "true",
#    fragfile_writer_flags => '["--limit-num-fragments", "--fragment-length=6.0"]',
#    fragfile_writer_variant_flags => '{"H1" : ["--thinning"]}',
#    sdp_base => "/usr/local/ods/frag/sdp",
#    output_base => "/var/www/frag-live",
#    variants => [["H1:100", "H3:300", "H5:500", "H7:700", "H9:900", "H11:1100", "H20:2000", "H25:2500"]],
#    license_manager_v1_url => "http://10.178.220.50:8080",
#    roap_server_url => "http://10.178.220.50:8080",
#    license_manager_v2_url => "http://10.178.220.50:8080",
#    Require => Class["python"],
#    }
#
# [Remember: No empty lines between comments and class definition]
class fragcontroller (

    $create_root_directory = $fragcontroller::params::create_root_directory,
    $create_directories = $fragcontroller::params::create_directories,
    $fragfile_writer_flags = $fragcontroller::params::fragfile_writer_flags,
    $fragfile_writer_variant_flags = $fragcontroller::params::fragfile_writer_variant_flags,
    $rest_server_port = $fragcontroller::params::rest_server_port,
    $adjust_start_time = $fragcontroller::params::adjust_start_time,
    $mode = $fragcontroller::params::mode,
    $sdp_base = $fragcontroller::params::sdp_base,
    $output_base = $fragcontroller::params::output_base,
    $variants = $fragcontroller::params::variants,
    $roap_server_url = $fragcontroller::params::roap_server_url,
    $license_manager_timeout = $fragcontroller::params::license_manager_timeout,
    $license_manager_retrycount = $fragcontroller::params::license_manager_retrycount,
    $override_output_base = $fragcontroller::params::override_output_base,
    $license_manager_url = $fragcontroller::params::license_manager_url,
    $license_manager_v1_url = $fragcontroller::params::license_manager_v1_url,
    $license_manager_v2_url = $fragcontroller::params::license_manager_v2_url,
    $package = $fragcontroller::params::package,
    $package_ensure = $fragcontroller::params::package_ensure,
    $service = $fragcontroller::params::service,
    $service_ensure = $fragcontroller::params::service_ensure,
    $service_enable = $fragcontroller::params::service_enable,
    $use_encryption = $fragcontroller::params::use_encryption,
    $use_pts_start_time = $fragcontroller::params::use_pts_start_time,

) inherits fragcontroller::params {
    include fragcontroller::install, fragcontroller::config, fragcontroller::service


    # include module name in motd
    os::motd::register { $name: }
}
