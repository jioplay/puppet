class fragcontroller::install {

    include packages
    realize(Package["mobi-logrotate-ods-config"])

    package { $::fragcontroller::package:
        ensure => $::fragcontroller::package_ensure,
    }
}
