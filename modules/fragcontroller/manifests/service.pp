class fragcontroller::service {

  # /sbin/service fails, changing from redhat provider to init
  service { $::fragcontroller::service:
    ensure => $::fragcontroller::service_ensure,
    enable     => $::fragcontroller::service_enable,
    hasstatus  => true,
    hasrestart => true,
    start => "/sbin/service fragcontrollerd conftest && /sbin/service fragcontrollerd start",
    restart => "/sbin/service fragcontrollerd graceful",
    subscribe  => Class[fragcontroller::config],
  }
}
