class fragcontroller::config {
    #node.json parameters:
    $create_root_directory         = $fragcontroller::create_root_directory
    $create_directories            = $fragcontroller::create_directories
    $fragfile_writer_flags         = $fragcontroller::fragfile_writer_flags
    $fragfile_writer_variant_flags = $fragcontroller::fragfile_writer_variant_flags
    $rest_server_port              = $fragcontroller::rest_server_port
    $adjust_start_time             = $fragcontroller::adjust_start_time
    $mode                          = $fragcontroller::mode
    $sdp_base                      = $fragcontroller::sdp_base
    $output_base                   = $fragcontroller::output_base
    $variants                      = $fragcontroller::variants
    $filter_files                  = $fragcontroller::filter_files
    $license_manager_v1_url        = $fragcontroller::license_manager_v1_url
    $license_manager_v2_url        = $fragcontroller::license_manager_v2_url
    $roap_server_url               = $fragcontroller::roap_server_url
    $license_manager_timeout       = $fragcontroller::license_manager_timeout
    $license_manager_retrycount    = $fragcontroller::license_manager_retrycount
    $override_output_base          = $fragcontroller::override_output_base
    $http_base_url                 = $fragcontroller::http_base_url
    $skus                          = $fragcontroller::skus
    $use_encryption                = $fragcontroller::use_encryption
    $output_format                 = $fragcontroller::output_format
    $use_pts_start_time            = $fragcontroller::use_pts_start_time

    # fail if mandatory node directives undef
    if ! $mode { fail("'mode' fragcontroller directive not defined") }
    if ! $sdp_base { fail("'sdp_base' fragcontroller directive not defined") }
    if ! $output_base { fail("'output_base' fragcontroller directive not defined") }
    if ! $variants { fail("'variants' fragcontroller directive not defined") }

    File {
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        require => Class["fragcontroller::install"],
    }

    file { "/usr/local/ods/frag/conf.d/MasterConfig.json":
        source => "puppet://$puppetserver/modules/fragcontroller/MasterConfig.json",
    }

    file { "/usr/local/ods/frag/conf.d/node.json":
        content => template("fragcontroller/node.json.erb"),
    }

    file { "/etc/logrotate.d/fragcontroller":
        source => "puppet://$puppetserver/modules/fragcontroller/fragcontroller.logrotate",
    }

    file { "/etc/cron.d/fmp4":
        content => template("fragcontroller/cron/fmp4.erb"),
    }

    file { "/etc/cron.d/hls":
        content => template("fragcontroller/cron/hls.erb"),
    }

    # add rsyslog files if rsyslog being used
    if defined(Class["rsyslog"]) {
        file { "/etc/rsyslog.d/mobi_fragcontroller.conf":
            ensure => present,
            notify => Class["rsyslog::service"],
            source => "puppet:///modules/fragcontroller/rsyslog/mobi_fragcontroller.conf",
        }
    }
}
