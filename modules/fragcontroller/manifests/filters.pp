define fragcontroller::filters {
  file { "/usr/local/ods/frag/conf.d/${name}":
    source => "puppet://$puppetserver/modules/fragcontroller/${name}",
  }
}
