define fragcontroller::filtergroup (
    $channels,
    $variants = undef,
    $use_encryption = undef,
    $output_format = undef,
    $override_output_base = undef,
    $http_base_url = undef,
    $fragfile_writer_flags = [],
    $license_manager_v1_url = undef,
    $license_manager_v2_url = undef,
    $fragfile_writer_flags = undef,
    $create_root_directory = undef,
    $adjust_start_time = undef,
    $roap_server_url = undef,
    $license_manager_timeout = undef,
    $license_manager_retrycount = undef,
    $skus = undef,
    $hls_aes_salt = undef,
    $arguments=[],
) {

    file { "/usr/local/ods/frag/conf.d/${title}" :
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        require => Class["fragcontroller::install"],
        before  => File["/usr/local/ods/frag/conf.d/node.json"],
        notify  => Class["fragcontroller::service"],
        content => template("fragcontroller/filtergroup.json.erb"),
    }
}
