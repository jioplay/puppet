# Class: fragcontroller::params
#
# This module manages fragcontroller paramaters
#
# This class file is not called directly
class fragcontroller::params {

    $package = "mobi-fragcontroller"
    $package_ensure = "present"

    $service = "fragcontrollerd"
    $service_ensure = running
    $service_enable = true

    #node.json parameters:
    $use_encryption = true
    $create_root_directory = false
    $create_directories = false
    $adjust_start_time = true
    $fragfile_writer_flags = []
    $fragfile_writer_variant_flags = []
    $variants = []
    $rest_server_port = "4311"
    $mode = "high-availability"
    $sdp_base = undef
    $output_base = undef
    $roap_server_url = undef
    $license_manager_timeout = "3"
    $license_manager_retrycount = "3"
    $override_output_base = undef
    $license_manager_url = undef
    $license_manager_v1_url = undef
    $license_manager_v2_url = undef
    $use_pts_start_time = undef
}
