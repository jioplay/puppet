# == Define: example_resource
#
# Installs and optionally executes the script responsible for generating
# sdp files for a given fragcontroller configuration.
#
# === Parameters
#
# Document parameters here
#
# [*namevar*]
#   The name of the ts_input configuration file specifying the channel sdp
#   files to generate.
#
# [*sdp_base*]
#   Optional. Name of directory to write sdp files to.
#
# [*script_filename*]
#   Optional. Name of script to be used in generating the sdp files.
#
# [*execute*]
#   Optional. Defaults to true. Whether to execute the script or just install.
#
# === Examples
#
#   fragcontroller::generate_ts_sdp { 'ts_input.conf':
#     script_name => "non-default-script",
#   }
#
# === Authors
#
# Puppet::Masters <| tag == daddy |>
#
# === Copyright
#
# Copyright 2013 MobiTV
#
define fragcontroller::generate_ts_sdp (
    $sdp_base = "/usr/local/ods/frag/sdp.d",
    $ts_input_filename = $title,
    $script_filename = "generate_ts_sdp.sh",
    $execute = true,
) {

    File {
        owner   => "root",
        group   => "root",
        mode    => "0655",
        require => Class["fragcontroller::install"],
    }

    file { $sdp_base:
        ensure => directory,
    }

    file { "/usr/local/ods/frag/scripts/${script_filename}":
        ensure  => present,
        source  => "puppet://$puppetserver/modules/fragcontroller/${script_filename}",
    }

    file { "/usr/local/ods/frag/conf.d/${ts_input_filename}":
        ensure  => present,
        source  => "puppet://$puppetserver/modules/fragcontroller/${ts_input_filename}",
    }

    if $execute {
        exec { "/bin/bash -x /usr/local/ods/frag/scripts/${script_filename} /usr/local/ods/frag/conf.d/${ts_input_filename}":
            cwd         => $sdp_base,
            user        => "root",
            refreshonly => true,
            require     => File[$sdp_base],
            notify => Class["fragcontroller::service"],
            subscribe   => [
                File["/usr/local/ods/frag/conf.d/${ts_input_filename}"],
                File["/usr/local/ods/frag/scripts/${script_filename}"]
            ],
        }
    }
}
