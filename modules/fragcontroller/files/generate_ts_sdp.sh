#!/bin/bash
CONF=${1:-"$(dirname $0)/ts_input.conf"}
awk '/^[0-9]/ { print "Writing",$1; print "v=0 \no=- 17 1 IN IP4 10.170.44.100\ns=session \ni=envivio MPEG Transport Stream \nc=IN IP4 "$2"\nt=0 0 \nm=mpeg-ts "$3" TS 96 \na=control:TrackID=1" > $1 }' < "$CONF"
