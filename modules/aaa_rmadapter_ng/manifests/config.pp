class aaa_rmadapter_ng::config {

    $idm_url = $aaa_rmadapter_ng::idm_url
    $sprint_proxy_url = $aaa_rmadapter_ng::sprint_proxy_url

    file { "/opt/mobi-aaa/config/rights-manager-ng/applicationProperties.xml":
    ensure => file,
    content => template("aaa_rmadapter_ng/applicationProperties.xml.erb"),
    notify => Class["tomcat::service"],
    require => Class["aaa_rmadapter_ng::install"],
    }
}
