# == Class: aaa_rmadapter_ng
#
#  installs rights manager adapter ng component
#
# === Parameters:
#
#    $rights_manager_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "aaa_rmadapter_ng" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class aaa_rmadapter_ng (
    $rights_manager_ver = $aaa_rmadapter_ng::params::rights_manager_ver,
    $idm_url = $aaa_rmadapter_ng::params::idm_url,
    $sprint_proxy_url = $aaa_rmadapter_ng::sprint_proxy_url,
    $solr_server_url = $aaa_rmadapter_ng::solr_server_url,
)
inherits aaa_rmadapter_ng::params {
    include aaa_rmadapter_ng::install, aaa_rmadapter_ng::config
    anchor { "aaa_rmadapter_ng::begin": } -> Class["aaa_rmadapter_ng::install"]
    Class["aaa_rmadapter_ng::config"] -> anchor { "aaa_rmadapter_ng::end" :}
    os::motd::register { $name : }
}
