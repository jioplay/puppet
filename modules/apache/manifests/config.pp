class apache::config {

 File {
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => Class["apache::install"],
  }

  file { "/etc/httpd/conf/httpd.conf":
    ensure  => file,
    content => template("apache/httpd.conf.erb"),
  }

  file { "/etc/httpd/conf.d/disable_trace.conf":
    ensure  => file,
    source => "puppet://$puppetserver/modules/apache/conf.d/disable_trace.conf",
  }

  file { "/etc/httpd/conf.d/disable_X-Frame.conf":
    ensure  => file,
    source => "puppet://$puppetserver/modules/apache/conf.d/disable_X-Frame.conf",
  }

  file { "/etc/httpd/conf.d/welcome.conf":
    ensure  => absent,
  }

  file { "/etc/httpd/conf.d/ops-server-status-info.conf":
    ensure  => absent,
    source => "puppet://$puppetserver/modules/apache/ops-server-status-info.conf",
  }

  # overide default logrotate conf
  file { "/etc/logrotate.d/httpd":
    ensure => file,
    source => "puppet://$puppetserver/modules/apache/httpd.logrotate",
  }

  # set correct perms on logs
  file { "/var/log/httpd":
    ensure  => directory,
    owner   => "apache",
    group   => "apache",
    mode    => "0664",
    recurse => true,
  }

  # The /var/www/html/check.txt is used for HAProxy health_check
  file { "/var/www/html/check.txt":
    ensure  => file,
    owner   => "apache",
    group   => "apache",
    content => $::fqdn,
  }

  # configure Apache MPM type on RH distros
  if $::operatingsystem =~ /(?i:centos|redhat|fedora|scientific|linux)/ {
    file { "/etc/sysconfig/httpd":
      ensure  => file,
      owner   => "root",
      group   => "root",
      content => template("apache/httpd.erb"),
    }
  }

}
