# Class: apache
#
# This module manages apache
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class apache (

  ### install.pp
  $package = $apache::params::package,
  $package_ensure = $apache::params::package_ensure,

  ### config.pp
  $mpm = $apache::params::mpm,
  $listen = $apache::params::listen,
  $servername = $apache::params::servername,
  $template_file = $apache::params::template_file,
  $config_file_path= $apache::params::config_file_path,
  $security_config = $apache::params::security_config,
  $redirect_index_to = $apache::params::redirect_index_to,

  ### service.pp
  $service = $apache::params::service,
  $service_ensure = $apache::params::service_ensure,
  $service_enable = $apache::params::service_enable,

  ### icinga.pp
  $icinga = $apache::params::icinga,
  $icinga_instance = $apache::params::icinga_instance,
  $icinga_cmd_args = $apache::params::icinga_cmd_args,

  ### ganglia.pp
  $ganglia = $apache::params::ganglia,

)
inherits apache::params {

    anchor {  "apache::begin": } ->
    class { "apache::install": } ->
    class { "apache::config": } ->
    class { "apache::icinga": } ->
    class { "apache::ganglia": } ->
    anchor { "apache::end": }

    class { "apache::service": }
    # include module name in motd
    os::motd::register { "apache": }
}
