# Class: apache::ssl
#
# This class installs the SSL packages required for Apache
#
# Parameters:
# - The $package_ssl is the name of the SSL module for Apache
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class apache::devel (
  $devel_package = $apache::params::devel_pacakge,
  $devel_package_ensure = $apache::params::devel_pacakge_ensure,
) inherits apache::params {

  package { "httpd-devel":
    name   => $devel_package,
    ensure => $devel_package_ensure,
    require => Class["apache::install"],
  }

  # include module name in motd
  os::motd::register { "apache::devel": }
}
