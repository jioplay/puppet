class apache::service {
  service { "httpd":
      name       => $::apache::service,
      ensure     => $::apache::service_ensure,
      enable     => $::apache::service_enable,
      hasstatus  => true,
      hasrestart => true,
      start => "/sbin/service httpd graceful",
      restart => "/sbin/service httpd graceful",
      subscribe  => [ Class["apache::install"], Class["apache::config"] ],
  }
}
