# Class: apache::params
#
# This class manages Apache parameters
#
# Parameters:
# - The $package is the name of the Apache package
# - The $package_ensure is the ensure parameter to the apache package resource.
# - The $service is the apache init script name.
# - The $service_ensure is the apache service resource ensure parameter.
# - The $service_enable is the apache service resource enable parameter.
# - The $ssl_package is the name of the SSL module for Apache
# - The $ssl_package_ensure is the ensure parameter to the apache ssl package.
# - The $devel_package is the name of the Apache development libraries package
# - The $devel_package_ensure is the ensure parameter to the apache devel package.
# - The $mpm is the name of the Apache MPM type to use, one of: prefork, worker, event
# - The $listen is the address listed in the main httpd.conf listen directive.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class apache::params {

  #############################################################################
  ###  apache class params
  #############################################################################

  ### install.pp
  $package = "httpd"
  $package_ensure = present

  ### service.pp
  $service = "httpd"
  $service_ensure = running
  $service_enable = true

  ### config.pp
  $mpm = "prefork"
  $listen = "80"

  ### icinga.pp
  $ganglia = false

  ### icinga.pp
  $icinga = true
  $icinga_instance = "icinga"
  $icinga_cmd_args = "-I $::ipaddress -p $listen -u /check.txt -w 2 -c 3"

  #############################################################################
  ###  optional subclass params
  #############################################################################

  ### ssl.pp
  $ssl_package = "mod_ssl"
  $ssl_package_ensure = present
  $ssl_icinga = false
  $ssl_icinga_instance = "icinga"
  $ssl_icinga_cmd_args = "-I $::ipaddress -u /check.txt -w 2 -c 3 -S"

  ### devel.pp
  $devel_package = "httpd-devel"
  $devel_package_ensure = present
}
