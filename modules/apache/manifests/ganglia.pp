class apache::ganglia {

    if $::apache::ganglia {

        if ! defined("ganglia::gmond") {
            fail("ganglia set to true in $name but ganglia::gmond not declared, skipping ganglia plugin install.")
        }

        File {
            ensure  => present,
            owner   => "root",
            group   => "root",
            mode    => "0644",
            require => Package["ganglia-gmond"],
            notify  => Service["gmond"],
        }

        file { "/etc/ganglia/conf.d/apache_statys.pyconf":
            source  => "puppet:///modules/apache/ganglia/apache_status.pyconf",
        }

        file { "/usr/lib64/ganglia/python_modules/apache_status.py":
            source => "puppet:///modules/apache/ganglia/apache_status.py",
        }
    }
}
