class apache::install {
  package { "httpd":
      name   => $::apache::package,
      ensure => $::apache::package_ensure,
  }
}
