class apache::secure (
){
  $redirect_index_to = $apache::redirect_index_to

  File {
    notify  => Class["apache::service"],
    require => Class["apache::install"],
  }

  file { '/etc/httpd/conf.d/welcome.conf':
    ensure  => absent,
  }

  file { '/var/www/html/index.html':
    content => template("${module_name}/index.html.erb"),
  }

  Apache::Static_files {
    require => Class["apache::install"],
    notify  => Class["apache::service"],
  }

  apache::static_files { "disable_listing_directory":
    file_path => '/etc/httpd/conf.d/disable_ServerSignature.conf',
    file_source => 'conf.d/disable_ServerSignature.conf',
  }

  apache::static_files { "disable_trace_method":
    file_path => '/etc/httpd/conf.d/disable_trace.conf',
    file_source => 'conf.d/disable_trace.conf',
  }

  apache::static_files { "disable_cross_frame":
    file_path => '/etc/httpd/conf.d/disable_X-Frame.conf',
    file_source => 'conf.d/disable_X-Frame.conf',
  }
}
