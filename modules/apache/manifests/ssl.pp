# Class: apache::ssl
#
# This class installs the SSL packages required for Apache
#
# Parameters:
# - The $package_ssl is the name of the SSL module for Apache
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class apache::ssl (
  $ssl_package = $apache::params::ssl_package,
  $ssl_package_ensure = $apache::params::ssl_package_ensure,

  $icinga = $apache::params::ssl_icinga,
  $icinga_instance = $apache::params::ssl_icinga_instance,
  $icinga_cmd_args = $apache::params::ssl_icinga_cmd_args,

) inherits apache::params {

  package { "httpd-ssl":
    name   => $ssl_package,
    ensure => $ssl_package_ensure,
    require => Class["apache::install"],
  }

  if $icinga {
    if ! $icinga_instance {
      fail("Must provide icinga_instance parameter to apache::ssl class when icinga = true")
    }

    @@nagios_service { "check_https_${fqdn}":
      host_name             => "$::fqdn",
      check_command         => "check_http! ${icinga_cmd_args}",
      service_description   => "check_https",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "$icinga_instance",
    }

  }

  # include module name in motd
  os::motd::register { "apache::ssl": }
}
