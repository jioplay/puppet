class apache::icinga {

  if $::apache::icinga {

    if ! $::apache::icinga_instance {
      fail("Must provide icinga_instance parameter to apache module when icinga = true")
    }

    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    @@nagios_service { "check_http_${::fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::apache::icinga_cmd_args}",
      service_description   => "check_http",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      notes                 => "Path: ${::apache::icinga_cmd_args}",
      tag                   => "icinga_instance:${::apache::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

  }
}
