# = Class: tomcat::stop
#
# stops tomcat
#
# == Parameters:
#
#    $command:: the tomcat instance to stop e.g. tomcat8008 , tomcat8081 etc
#
# == Sample Usage
#
#  class {
#    "tomcat::stop" :
#      command => $command,
#      stage => "setup",
#  }
#
# Remember: No empty lines between comments and class definition
#
class tomcat::stop ($command) {
  notify {
    "Stopping tomcat: ${command}" :
      before => Exec["tomcat::stop::exec"],
  }
  notify {
    "Stopping tomcat COMPLETE: ${command}" :
      require => Exec["tomcat::stop::exec"],
  }
  exec {
    'tomcat::stop::exec' :
    # the echo on the end of this command is to mask the exit code from the tomcat script
    # puppet is sensitive to non zero exit codes, and the tomcat script appears not to
    # follow the puppet protocol. We should probably reconcole this gap. I'll file a bug
    # for this
      command => "/etc/init.d/${command} stop; echo ''",
      onlyif => "/usr/bin/test -f /etc/init.d/${command}",
  }
}
