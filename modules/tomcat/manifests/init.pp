# == Class: tomcat
#
# installs tomcat and starts it
#
# === Parameters:
#
#    $package::           the tomcat package name
#    $version::           the tomcat package version
#    $override_catalina:: (true|false) option to enable or diable overwriting catalina.sh (D:true)
#    $heapsize::          set the heap size in the config (only used if override_catalina = true)
#    $permgensize::       set the permgen size in the config (only used if override_catalina = true)
#    $catalina_config::   catalina configuration template to use
#    $config_owner::      configuration file owner
#    $config_group::      configuration file group
#    $override_server_config:: (true|false) option to enable or diable overwriting server.xml (D:true)
#    $server_config_loc:: location of server.xml  (only used if override_server_config = true)
#    $server_config_src:: content source for server.xml  (only used if override_server_config = true)
#    $initscript::        the tomcat instance to start e.g. tomcat8008 , tomcat8081 etc
#    $jars::              a list of extra jar files to put in /opt/tomcat/lib
#    $jar_owner::         who owns the jar files
#    $jar_group::         group for jar files
#    $jdbc_driver::       jdbc driver to use (values: 'none', 'oracle', 'mysql')
#
# === Requires
#
#    java
#
# === Sample Usage
# ==== New Tomcat
#    class { "tomcat":
#        package => "mobi_tomcat_config_8080",
#        version => latest,
#        override_catalina => false,
#        override_server_config => true,
#        server_config_loc => "/opt/mobi_tomcat_config/mobi_tomcat_config_8080/conf/server.xml",
#        server_config_src => "server.xml.tomcat8080",
#        config_owner => "rtv",
#        config_group => "rtv",
#        initscript => "tomcat8080",
#        require => Class["java"],
#        jrehome => "",
#    }
# ==== Old Tomcat
#    class { "tomcat":
#        package => "mobi_tomcat_heapsize_increased",
#        version => latest,
#        override_catalina => true,
#        override_server_config => true,
#        server_config_loc => "/opt/tomcat/conf/server.xml",
#        server_config_src => "server.xml",
#        config_owner => "rtv",
#        config_group => "rtv",
#        initscript => "tomcat",
#        require => Class["java"],
#    }
# ==== Notify
# other classes should notify the service class
#
#  notify => Class["tomcat::service"]
#
# Remember: No empty lines between comments and class definition
#
class tomcat (
    $package                = $tomcat::params::package,
    $version                = $tomcat::params::version,
    $override_catalina      = $tomcat::params::override_catalina,
    $catalina_config        = $tomcat::params::catalina_config,
    $config_owner           = $tomcat::params::config_owner,
    $config_group           = $tomcat::params::config_group,
    $override_server_config = $tomcat::params::override_server_config,
    $server_config_loc      = $tomcat::params::server_config_loc,
    $server_config_src      = $tomcat::params::server_config_src,
    $heapsize               = $tomcat::params::heapsize,
    $initheapsize           = $tomcat::params::initheapsize,
    $permgensize            = $tomcat::params::permgensize,
    $initscript             = $tomcat::params::initscript,
    $jrehome                = $tomcat::params::jrehome,
    $jars                   = $tomcat::params::jars,
    $jar_owner              = $tomcat::params::jar_owner,
    $jar_group              = $tomcat::params::jar_group,
    $core_package           = $tomcat::params::core_package,
    $core_version           = $tomcat::params::core_version,
    $jmx_auth_package       = $tomcat::params::jmx_auth_package,
    $jmx_auth_version       = $tomcat::params::jmx_auth_version,
    $service_ensure         = $tomcat::params::service_ensure,
)
inherits tomcat::params {

    anchor { "tomcat::begin": } ->
    class  { "tomcat::install": } ->
    class  { "tomcat::config": } ->
    anchor { "tomcat::end": }

    class { "tomcat::service": }

    os::motd::register { $name : }
}
