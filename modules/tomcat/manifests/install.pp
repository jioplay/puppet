class tomcat::install {

  if $::tomcat::core_package != "mobi-tomcat-heapsize-increased" {
    # these steps we omit for the legacy package

    #install the core and jmx auth packages first
      package { "${::tomcat::core_package}-${::tomcat::core_version}":
          ensure => installed,
          before => Package[$::tomcat::package],
      }

      package { "${::tomcat::jmx_auth_package}-${::tomcat::jmx_auth_version}":
          ensure => latest,
          before => Package[$::tomcat::package],
      }
  }

  #then install the config package
    package { "$::tomcat::package":
        ensure => $::tomcat::version,
    }

    if $::tomcat::jars {
        tomcat::install_jars{ $::tomcat::jars :
            require => Package[$::tomcat::package],
        }
    }
}
