define tomcat::install_jars {

    file { "/opt/tomcat/lib/${name}":
        ensure => present,
        owner  => $::tomcat::jar_owner,
        group  => $::tomcat::jar_group,
        source => "puppet:///modules/tomcat/${name}",
        mode   => "0644",
    }
}
