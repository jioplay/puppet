class tomcat::config {

    if $::tomcat::override_catalina {
        file { "/opt/tomcat/bin/${::tomcat::catalina_config}":
            ensure  => present,
            content => template("tomcat/${::tomcat::catalina_config}.erb"),
            owner   => $::tomcat::config_owner,
            group   => $::tomcat::config_group,
            mode    => "0754",
            replace => true,
        }
    }
    if $::tomcat::override_server_config {
        file { $::tomcat::server_config_loc :
            ensure  => present,
            source  => "puppet://${::puppetserver}/modules/tomcat/${::tomcat::server_config_src}",
            owner   => $tomcat::config_owner,
            group   => $tomcat::config_group,
            mode    => "0644",
            replace => true,
        }
    }

    if 'mobi-tomcat-config-8080' in $::tomcat::package {
        $heapsize = $::tomcat::heapsize
        $initheapsize = $::tomcat::initheapsize
        file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/setenv.d/tomcat.override.050.heap.size.sh":
            replace => true,
            owner   => "rtv",
            group   => "rtv",
            mode    => "0444",
            content => template('tomcat/tomcat.override.050.heap.size.sh.erb'),
        }
    }
}
