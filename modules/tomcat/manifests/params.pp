class tomcat::params {
    #the "sane" defaults.
    $package = "mobi-tomcat-heapsize-increased"
    $version = "6.0.16.1-170413"
    $override_catalina = true
    $catalina_config = "catalina.sh"
    $heapsize = "1536"
    $initheapsize = "512"
    $permgensize= "1024"
    $override_server_config = true
    $server_config_loc = "/opt/tomcat/conf/server.xml"
    $server_config_src = "server.xml"
    $config_owner = "rtv"
    $config_group = "rtv"
    $initscript = "tomcat"
    $jrehome ="/usr/java/latest"
    $jars = undef
    $jar_owner = "rtv"
    $jar_group = "rtv"

  # these only apply to the new tomcat packaging structure
    $core_package = "mobi-tomcat-core"
    $core_version = "6.0.35"
    $jmx_auth_package = "mobi-tomcat-jmx-auth"
    $jmx_auth_version = "6.0.35"
    $service_ensure = running
}
