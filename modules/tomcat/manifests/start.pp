# = Class: tomcat::start
#
# starts tomcat. Typically you will configure this to trigger after you have deployed your app
#
# == Parameters:
#
#    $command:: the tomcat instance to start e.g. tomcat8008 , tomcat8081 etc
#
# == Sample Usage
#
#  class {
#    "tomcat::start" :
#      command => $command,
#      stage => "runtime",
#  }
#
# Remember: No empty lines between comments and class definition
#
class tomcat::start ($command) {
  notify {
    "Starting tomcat: ${command}" :
      before => Exec["tomcat::start::exec"],
  }
  notify {
    "Starting tomcat COMPLETE: ${command}" :
  }
  exec {
    'tomcat::start::exec' :
    # the echo on the end of this command is to mask the exit code from the tomcat script
    # puppet is sensitive to non zero exit codes, and the tomcat script appears not to
    # follow the puppet protocol. We should probably reconcole this gap. I'll file a bug
    # for this
      command => "/etc/init.d/${command} start ; echo ''",
      require => Notify["Starting tomcat: ${command}"],
      before => Notify["Starting tomcat COMPLETE: ${command}"],
  }
}
