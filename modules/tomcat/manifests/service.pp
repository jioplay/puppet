class tomcat::service {

    service { "$::tomcat::initscript" :
        ensure    => $::tomcat::service_ensure,
        start     => "JRE_HOME=${::tomcat::jrehome} /etc/init.d/${::tomcat::initscript} start || /bin/true ",
        stop      => "/etc/init.d/${::tomcat::initscript} stop || /bin/true ",
        restart   => "JRE_HOME=${::tomcat::jrehome} /etc/init.d/${::tomcat::initscript} restart || /bin/true ",
        enable    => true,
        hasstatus => false,
        pattern   => "org.apache.catalina.startup.Bootstrap",
        subscribe => [
            Class["tomcat::install"],
            Class["tomcat::config"],
            Package["java-install"],
            Class["java::config"]
        ],
    }

}
