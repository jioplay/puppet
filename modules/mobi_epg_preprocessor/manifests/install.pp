class mobi_epg_preprocessor::install {
    package { "${mobi_epg_preprocessor::package_name}":
        ensure => "${mobi_epg_preprocessor::package_ensure}",
        require => Class["tomcat::install"],
    }
}
