class mobi_epg_preprocessor::icinga {

  if $::mobi_epg_preprocessor::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::mobi_epg_preprocessor::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_epg_preprocessor module when icinga = true")
    }

    @@nagios_service { "check_http_mobi_epg_preprocessor_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_http! ${::mobi_epg_preprocessor::icinga_cmd_args}",
      service_description => "check_http_mobi_epg_preprocessor",
      notes => "PATH: ${::mobi_epg_preprocessor::icinga_cmd_args}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mobi_epg_preprocessor::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }

  }
}

