class mobi_epg_preprocessor::params {

$package_name = "mobi-epg-preprocessor"
$package_ensure = undef

###icinga.pp

$icinga = true
$icinga_instance = "icinga"
$icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-epg-preprocessor/monitoring/health -w 5 -c 10"



#### Config file defaults
$source_data_file = "/opt/mobi-epg-preprocessor/webapp/WEB-INF/classes/data/xmltv_terrestrial.xml"
$xsl_file = "/opt/mobi-epg-preprocessor/webapp/WEB-INF/classes/xslt/epgvalidator.xsl"
$temp_folder = "/opt/mobi-epg-preprocessor/temp/"
$filler_file = "/opt/mobi-epg-preprocessor/webapp/WEB-INF/classes/data/filler.xml"
$output_file = "/opt/mobi-epg-ingestor/webapp/WEB-INF/classes/data/xmltv_terrestrial.xml"
}
