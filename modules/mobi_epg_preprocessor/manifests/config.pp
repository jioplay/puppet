class mobi_epg_preprocessor::config {

    file {"/opt/${mobi_epg_preprocessor::package_name}/webapp/config/epg-preprocessor.properties":
         ensure => present,
         notify => Class["tomcat::service"],
         require => Class["mobi_epg_preprocessor::install"],
         content => template("mobi_epg_preprocessor/epg-preprocessor.properties.erb"),
    }
}
