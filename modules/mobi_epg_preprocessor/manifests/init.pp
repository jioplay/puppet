## == Class: mobi_epg_preprocessor
#
# Manage the mobi_epg_preprocessor component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_epg_preprocessor (
$package_name = $mobi_epg_preprocessor::params::package_name,
$package_ensure = $mobi_epg_preprocessor::params::package_ensure,
$icinga = $mobi_epg_preprocessor::params::icinga,
$icinga_instance = $mobi_epg_preprocessor::params::icinga_instance,
$icinga_cmd_args = $mobi_epg_preprocessor::params::icinga_cmd_args,
$source_data_file = $mobi_epg_preprocessor::params::source_data_file,
$xsl_file = $mobi_epg_preprocessor::params::xsl_file,
$temp_folder = $mobi_epg_preprocessor::params::temp_folder,
$filler_file =$mobi_epg_preprocessor::params::filler_file,
$output_file =$mobi_epg_preprocessor::params::output_file,

) inherits mobi_epg_preprocessor::params {

    anchor { "mobi_epg_preprocessor::begin": } ->
    class { "mobi_epg_preprocessor::install": } ->
    class { "mobi_epg_preprocessor::config": } ->
    class { "mobi_epg_preprocessor::icinga": } ->
    anchor { "mobi_epg_preprocessor::end": }

    os::motd::register{ "${name}":}
}
