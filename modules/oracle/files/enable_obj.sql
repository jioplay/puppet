set serveroutput on
begin
dbms_output.put_line('ENABLING PRIMARY KEYS');
for i in (select owner, constraint_name, table_name from dba_constraints where owner not in ('SYSTEM','SYS','DBSNMP') and CONSTRAINT_TYPE='P' and table_name not like '%PCC%') LOOP
execute immediate 'alter table '||i.owner||'.'||i.table_name||' enable constraint '||i.constraint_name||'';
end loop;
dbms_output.put_line('ENABLING OTHER CONSTRAINTS');
for j in (select owner, constraint_name, table_name from dba_constraints where owner not in ('SYSTEM','SYS','DBSNMP') and table_name not like '%PCC%') LOOP
execute immediate 'alter table '||j.owner||'.'||j.table_name||' enable constraint '||j.constraint_name||'';
end loop;
dbms_output.put_line('ENABLING TRIGGERS');
for k in (select owner, object_name from dba_objects where owner not in ('SYSTEM','SYS','DBSNMP') and object_type='TRIGGER') LOOP
execute immediate 'alter trigger '||k.owner||'.'||k.object_name||' enable';
end loop;
end;
/
