#!/bin/bash
# Post-install steps for oracle install. This script must be run as 
# root.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Dave Lowell, 5/25/2011

# Exit script if any command returns a fail result code
set -e

if [ `whoami` != 'root' ]; then
        echo You must run this script as user root
	exit 1
fi

echo "Running oracle post install root.sh script"
/var/app/oracle/product/10204/root.sh -silent

# Open up access to the client libraries
chmod a+rx /var/app/oracle/product/10204/lib/libclntsh*
chmod a+rx /var/app/oracle/product/10204/lib
chmod a+rx /var/app/oracle/product/10204
chmod a+rx /var/app/oracle/product

# Add oracle libs dir to ld.so path
echo "/var/app/oracle/product/10204/lib" > /etc/ld.so.conf.d/oracle_libs.conf
/sbin/ldconfig

echo "... done"