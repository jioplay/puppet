#!/bin/sh -x
#***********************************************************************************************
#*
#*  Name: create_db_10g.sh
#*
#*
#*  Description: Script to create a standard Oracle 10g database
#*
#*
#*
#*
#***********************************************************************************************

if [ $# -ne 1 ];then
   echo 'Usage:$0 SID'
   exit 1;
fi

. ~/.bash_profile

#SID=`echo $1 | tr A-Z a-z`
SID=$1
DB=$SID
export ORACLE_SID=$SID

# List of users who are to be notified
MAIL_USERS="dba@mobitv.com"

# Create directory structure, if it does not exist
export CF1="/data0/oradata/$ORACLE_SID"
export CF2="/data1/oradata/$ORACLE_SID"
export CF3="/data2/oradata/$ORACLE_SID"
export TEMPS="/data2/oradata/$ORACLE_SID"
export DAT0="/data0/oradata/$ORACLE_SID"
export DAT1="/data1/oradata/$ORACLE_SID"
export DAT2="/data2/oradata/$ORACLE_SID"
export DAT3="/data3/oradata/$ORACLE_SID"
export IDX="/data0/oradata/$ORACLE_SID"
export UNDO="/data0/oradata/$ORACLE_SID"
export ARC="/data2/oradata/$ORACLE_SID/arch"
export AUD="/data2/oradata/$ORACLE_SID/audit"
export XPO="/data2/oradata/$ORACLE_SID/export"
export RMN="/data2/oradata/$ORACLE_SID/rman"
export DGRD="/home/oracle/admin/$ORACLE_SID/dataguard"
export BDUMP="/home/oracle/admin/$ORACLE_SID/bdump"
export CDUMP="/home/oracle/admin/$ORACLE_SID/cdump"
export PFILE="/home/oracle/admin/$ORACLE_SID/pfile"
export UDUMP="/home/oracle/admin/$ORACLE_SID/udump"

if [ -s $TEMPS/system01.dbf ]; then
   set +x
   echo "$TEMPS/system01.dbf exists"
   echo "Cut/paste the following to remove the database files."
   echo "then re-run this script."
   echo "#################################################################"
   echo "rm -rf $CF1 > /dev/null 2>&1"
   echo "rm -rf $CF2 > /dev/null 2>&1"
   echo "rm -rf $CF3 > /dev/null 2>&1"
   echo "rm -rf $TEMPS > /dev/null 2>&1"
   echo "rm -rf $DAT0 > /dev/null 2>&1"
   echo "rm -rf $DAT1 > /dev/null 2>&1"
   echo "rm -rf $DAT2 > /dev/null 2>&1"
   echo "rm -rf $DAT3 > /dev/null 2>&1"
   echo "rm -rf $IDX > /dev/null 2>&1"
   echo "rm -rf $UNDO > /dev/null 2>&1"
   echo "rm -rf $ARC > /dev/null 2>&1"
   echo "rm -rf $AUD > /dev/null 2>&1"
   echo "rm -rf $XPO > /dev/null 2>&1"
   echo "rm -rf $RMN > /dev/null 2>&1"
   echo "rm -rf $DGRD > /dev/null 2>&1"
   echo "rm -rf $BDUMP > /dev/null 2>&1"
   echo "rm -rf $CDUMP > /dev/null 2>&1"
   echo "rm -rf $PFILE > /dev/null 2>&1"
   echo "rm -rf $UDUMP > /dev/null 2>&1"
   echo "#################################################################"
   exit
fi


mkdir -p $CF1 > /dev/null 2>&1
mkdir -p $CF2 > /dev/null 2>&1
mkdir -p $CF3 > /dev/null 2>&1
mkdir -p $TEMPS > /dev/null 2>&1
mkdir -p $DAT0 > /dev/null 2>&1
mkdir -p $DAT1 > /dev/null 2>&1
mkdir -p $DAT2 > /dev/null 2>&1
mkdir -p $DAT3 > /dev/null 2>&1
mkdir -p $IDX > /dev/null 2>&1
mkdir -p $UNDO > /dev/null 2>&1
mkdir -p $ARC > /dev/null 2>&1
mkdir -p $AUD > /dev/null 2>&1
mkdir -p $XPO > /dev/null 2>&1
mkdir -p $RMN > /dev/null 2>&1
mkdir -p $DGRD > /dev/null 2>&1
mkdir -p $BDUMP > /dev/null 2>&1
mkdir -p $CDUMP > /dev/null 2>&1
mkdir -p $PFILE > /dev/null 2>&1
mkdir -p $UDUMP > /dev/null 2>&1

# Create init.ora for the new instance
init_ora="*.audit_file_dest='$AUD'
*.background_dump_dest='$BDUMP'
*.compatible='10.2.0.4.0'
*.control_files='$CF1/control01.ctl','$CF2/control02.ctl','$CF3/control03.ctl'
*.core_dump_dest='$CDUMP'
# *.cursor_sharing='SIMILAR'
*.db_block_checking=true
*.db_block_size=8192
*.db_cache_size=80m
*.db_domain=''
*.db_name='$DB'
*.dml_locks=4864
*.instance_name='$SID'
*.java_pool_size=150m
*.log_archive_dest_1='location=$ARC'
*.log_archive_dest_state_1='enable'
*.log_archive_format='"$DB"_%t_%s_%r.arc'
*.log_archive_max_processes=2
*.open_cursors=300
*.pga_aggregate_target=64m
*.processes=200
*.recyclebin=off
*.remote_login_passwordfile='EXCLUSIVE'
*.sessions=200
*.sga_max_size=512m
*.shared_pool_size=200m
*.shared_pool_reserved_size=16m
*.streams_pool_size=200m
*.timed_statistics=TRUE
*.transactions=200
*.undo_management='AUTO'
*.undo_retention=10800
*.undo_tablespace='UNDOTBS1'
*.user_dump_dest='$UDUMP'"

# Create login.sql file
login_f="define _editor=vi
set lines 200
set termout off
col host_name new_value p_host
col username new_value p_user
col dbname new_value p_dbname
define p_user='SQL'
define p_dbname=''
define p_host='> '
select '[' || lower(host_name) || '] ' host_name
from v\$instance;
select lower(user) username
from dual;
select '_' || upper(name) dbname
from v\$database;
set sqlprompt \"&p_host&p_user&p_dbname> \"
set termout on
set time on
!stty erase ^?
"

IFS=''
echo $init_ora > $PFILE/init$SID.ora
ln -s $PFILE/init$SID.ora $ORACLE_HOME/dbs/init$SID.ora
echo $login_f > /home/oracle/login.sql
unset IFS

# Create password file
rm -f $ORACLE_HOME/dbs/orapw$SID > /dev/null 2>&1
orapwd file=$ORACLE_HOME/dbs/orapw$SID password=secret entries=100

# Create database and tablespaces
sqlplus "/ as sysdba" <<-EOF
shutdown immediate;
startup nomount;

CREATE DATABASE $DB CONTROLFILE REUSE
LOGFILE GROUP 1 ( '$DAT0/log11.log', '$DAT1/log12.log') SIZE 250M,
        GROUP 2 ( '$DAT1/log21.log', '$DAT2/log22.log') SIZE 250M,
        GROUP 3 ( '$DAT2/log31.log', '$DAT3/log32.log') SIZE 250M,
        GROUP 4 ( '$DAT3/log41.log', '$DAT0/log42.log') SIZE 250M
DATAFILE '$TEMPS/system01.dbf' SIZE 600M AUTOEXTEND ON NEXT 100M MAXSIZE 2000M
SYSAUX DATAFILE '$TEMPS/sysaux.dbf' SIZE 600M AUTOEXTEND ON NEXT 100M MAXSIZE 2000M
UNDO TABLESPACE undotbs1
   DATAFILE '$UNDO/undotbs01.dbf' SIZE 200M AUTOEXTEND ON NEXT 50M MAXSIZE 10G
EXTENT MANAGEMENT LOCAL
DEFAULT TEMPORARY TABLESPACE temp
   TEMPFILE '$TEMPS/temp01.dbf' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE 2000M
MAXLOGFILES 100
MAXLOGMEMBERS 4
MAXDATAFILES 100
MAXINSTANCES 1
MAXLOGHISTORY 226
CHARACTER SET AL32UTF8
NATIONAL CHARACTER SET AL16UTF16;

CREATE TABLESPACE TOOLS DATAFILE
  '$DAT1/tools01.dbf' SIZE 10M AUTOEXTEND ON NEXT 100M MAXSIZE 300M
LOGGING
ONLINE
PERMANENT
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
BLOCKSIZE 8K
SEGMENT SPACE MANAGEMENT AUTO;

CREATE TABLESPACE USERS DATAFILE
  '$DAT1/users01.dbf' SIZE 25M AUTOEXTEND ON NEXT 100M MAXSIZE 300M
LOGGING
ONLINE
PERMANENT
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
BLOCKSIZE 8K
SEGMENT SPACE MANAGEMENT AUTO;

-- Run Scripts to Build Data Dictionary Views
@$ORACLE_HOME/rdbms/admin/catalog.sql;
@$ORACLE_HOME/rdbms/admin/catproc.sql;
@$ORACLE_HOME/rdbms/admin/dbmspool.sql;
@$ORACLE_HOME/rdbms/admin/prvtpool.plb;
-- Install JVM
@$ORACLE_HOME/javavm/install/initjvm.sql
---change password for system
alter user system identified by mobitv account unlock;
---create data_pump_dir and grant proper privs on it
CREATE OR REPLACE directory data_pump_dir as '/home/oracle/datapump';
grant read,write on directory data_pump_dir to system;
exit
EOF

# Open db and prepare db for the import
sqlplus system/mobitv <<EOF
shutdown immediate
startup mount
alter database open;
@$ORACLE_HOME/sqlplus/admin/pupbld.sql;
exit
EOF
