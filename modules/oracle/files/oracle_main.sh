#!/bin/bash
# Main script for the oracle install, running sub-scripts as needed.
# This script must be run as root.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Dave Lowell, 5/25/2011

# Exit script if any command returns a fail result code
set -e

if [ `whoami` != 'root' ]; then
        echo You must run this script as user root
	exit 1
fi


#for puppet we do everything from this directory
export INSTALL_DIR="/home/oracle/install"

echo "Start ORACLE main $(date)"

su - oracle -c "cd ${INSTALL_DIR}; INSTALL_DIR=${INSTALL_DIR} ./oracle_install.sh"

./oracle_postinstall_root.sh

su - oracle -c "cd ${INSTALL_DIR}; INSTALL_DIR=${INSTALL_DIR} ./oracle_install_patch.sh"

./oracle_postinstall_root.sh

echo "Stop ORACLE main $(date)"

su - oracle -c "cd ${INSTALL_DIR}; touch oracle_main.done"
