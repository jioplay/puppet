#!/bin/bash
# chkconfig: 345 85 20
# description: Starts and stops the Oracle DBMS and listener

# Source function library.
. /etc/rc.d/init.d/functions

if [ `whoami` != 'root' ]; then
        echo You must run this script as root
        exit 1
fi

RETVAL=0
prog="oracle"

start() {
	# Start DBMS and listener
	cat > /tmp/dbstart <<EOF
startup
exit
EOF

	echo -n $"Starting $prog: "

	su - oracle -c "sqlplus -s / as sysdba < /tmp/dbstart > /dev/null 2>&1"
	rm /tmp/dbstart

	if ! su - oracle -c "lsnrctl status > /dev/null 2>&1"; then
		su - oracle -c "lsnrctl start > /dev/null 2>&1"
		RETVAL=$?
	fi

	return $RETVAL
}

stop() {
	# Stop DBMS and listener
	cat > /tmp/dbstop <<EOF
shutdown
exit
EOF

	echo -n $"Stopping $prog: "

	su - oracle -c "sqlplus -s / as sysdba < /tmp/dbstop > /dev/null 2>&1"
	rm /tmp/dbstop

	if su - oracle -c "lsnrctl status > /dev/null 2>&1"; then
		su - oracle -c "lsnrctl stop > /dev/null 2>&1"
		RETVAL=$?
	fi

	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

if [ $RETVAL == 0 ]; then
	success
else
	failure
fi
echo

exit $RETVAL
