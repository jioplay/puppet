set serveroutput on
set echo on
set feed on
set serverout on size 1000000

DECLARE
        l_sql varchar2(254);
        cursor_id integer;
        result integer;

cursor get_users is
        select username
        from dba_users
        where account_status='OPEN';

BEGIN

cursor_id:=dbms_sql.open_cursor;

dbms_output.put_line('Starting password change');

FOR obj_rec in get_users LOOP

        l_sql := 'alter user ' || obj_rec.username || ' identified by ' || obj_rec.username;
        dbms_output.put_line(l_sql);
        dbms_sql.parse(cursor_id,l_sql,dbms_sql.native);
        result := dbms_sql.execute(cursor_id);

END LOOP;

dbms_sql.close_cursor(cursor_id);

END;
/

alter user system identified by mobitv account unlock;

