#!/bin/bash
# Perform oracle setup and install. This script must be run as oracle user.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Dave Lowell, 5/25/2011

# Exit script if any command returns a fail result code
set -e

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Get oracle binaries from build.corp, if we don't have them already.
# NOTE: the 10203 patch is not needed for our MOBI2, prdspt, prdjb databases.

# I have stored these tarballs on build.corp. Unfortunately, the old apache
# on that server cannot serve files larger than 2 GB. I have temporarily
# copied these files to a dir on my linux workstation "zambezi.corp" which 
# can serve them. We should find a long term home for these tarballs that
# can be accessed in dmz, lab, and corp and update this script. -Dave

# base 10g installer
mkdir -p /var/app/oracle/stage/10201
cd /var/app/oracle/stage/10201
if [ ! -e oracle_10g_10201.tar ]; then 
    echo Linking base oracle installer binaries
    ln -s ${INSTALL_DIR}/oracle_10g_10201.tar
fi
if [ ! -e B24792-01_1of5.zip ]; then
    echo Extracting base oracle installer binaries
    tar xvf oracle_10g_10201.tar
fi
if [ ! -e database/runInstaller ]; then
    echo Unzipping first oracle install image 
    unzip B24792-01_1of5.zip
fi

cd /var/app/oracle/stage/10201
ln -fs ${INSTALL_DIR}/enterprise.rsp

##
## Kick off oracle install
##
echo "Start Installer $(date)"
cd /var/app/oracle/stage/10201/database
./runInstaller -noconsole -silent -noconfig -ignoreSysPrereqs -responseFile /var/app/oracle/stage/10201/enterprise.rsp
#sleep while the backgrounded installer is running
sleep 30s
while [ "`ps aux | grep -i ora | grep install | grep -c java`" -eq "1" ] ; do
sleep 20s
done
echo "Finish Installer $(date)"

