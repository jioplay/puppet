#!/bin/bash
# Perform oracle setup and install. This script must be run as oracle user.
#
# THE STUFF IN THIS SCRIPT SHOULD BE IDEMPOTENT.
# -Dave Lowell, 5/25/2011

# Exit script if any command returns a fail result code
set -e

if [ `whoami` != 'oracle' ]; then
	echo You must run this script as user oracle
	exit 1
fi

# Patch 10204 installer
mkdir -p /var/app/oracle/stage/10204
cd /var/app/oracle/stage/10204
if [ ! -e oracle_10g_10204.tar ]; then 
	echo Downloading oracle 10204 patch
	ln -s ${INSTALL_DIR}/oracle_10g_10204.tar
fi
if [ ! -e p6810189_10204_Linux-x86-64.zip ]; then
	echo Extracting oracle 10204 patch installer
	tar xvf oracle_10g_10204.tar
fi
if [ ! -e Disk1/runInstaller ]; then
	echo Unzipping 10204 patch image
	unzip p6810189_10204_Linux-x86-64.zip
fi
cd /var/app/oracle/stage/10204
ln -fs ${INSTALL_DIR}/10204_patchset.rsp

##
## Kick off oracle patch
##
echo "Start patch install $(date)"
cd /var/app/oracle/stage/10204/Disk1
./runInstaller -noconsole -silent -noconfig -ignoreSysPrereqs -responseFile /var/app/oracle/stage/10204/10204_patchset.rsp
#sleep while the backgrounded installer is running
sleep 30s
while [ "`ps aux | grep -i ora | grep install | grep -c java`" -eq "1" ] ; do
sleep 20s
done
echo "Stop patch install $(date)"

