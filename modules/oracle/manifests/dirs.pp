class oracle::dirs {

    $install_dirs = [
                     "/data2/backups",
                     "/home/oracle/.ssh",
                     "/home/oracle/awr",
                     "/home/oracle/datapump",
                     "/home/oracle/dba",
                     "/home/oracle/install",
                     "/var/app",
                     "/var/app/oracle",
                     "/var/app/oracle/product",
                     "/var/app/oracle/stage",
                     "/var/data",
                     "/var/opt",
                     "/var/opt/oracle",
                     "/var/opt/oracle/prod",
                     "/var/opt/oracle/prod/log",
                     ]

    oracle::install_dirs { $install_dirs: }


    file { "/data":
        ensure => link,
        target => "/var/data",
        require => File['/var/data']
    }

    $oracle_data = [
                    "data0",
                    "data1",
                    "data2",
                    "data3",
                    ]

    oracle::data_dirs{ $oracle_data:
        require => [ File["/data"], File["/var/data"] ],
    }

}
