class oracle::service
{
    #cannot use service because we have oracle processes already
    service { "oracle" :
        ensure  => running,
        enable  => true,
        pattern => "oracle",
        require => Class["oracle::install"],
    }
}
