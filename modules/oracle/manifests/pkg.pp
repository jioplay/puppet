class oracle::pkg {

    $oracle_tars = [
                    "oracle_10g_10201.tar",
                    "oracle_10g_10204.tar",
                    "home.tar.bz2",
                    ]

    file { "large.md5":
        ensure => file,
        owner => "oracle",
        group => "dba",
        source => "puppet:///modules/oracle/large.md5",
        path => "/home/oracle/install/large.md5",
        require => File['/home/oracle/install'],
    }

    oracle::fetch_tars { $oracle_tars :
        logoutput => $oracle::logoutput,
        require => [ Package["wget"],File["large.md5"] ],
    }

}

