class oracle::install_dbs
{
    $db_install_scripts = [
                           "ch_pswrds.sql",
                           "disable_obj.sql",
                           "enable_obj.sql",
                           ]

    oracle::get_scripts { $db_install_scripts :
        require => File["/home/oracle/install"],
    }

    File {
        ensure => file,
        owner => "oracle",
        group => "dba",
        mode => "0644",
    }

    file { "create_db_10g" :
        path => "/var/opt/oracle/prod/create_db_10g.sh",
        mode => "0755",
        source => "puppet:///modules/oracle/cr_db_10g.sh"
    }

    file { "listener":
        path => "/var/app/oracle/product/10204/network/admin/listener.ora",
        content => template("oracle/listener.ora.erb"),
    }

    file { "tnsnames":
        path => "/var/app/oracle/product/10204/network/admin/tnsnames.ora",
        content => template("oracle/tnsnames.ora.erb"),
    }


    $database_sids = hash_keys( $oracle::database_hash )

    oracle::db_install { $oracle::install_dbs::database_sids :
        hash => $oracle::database_hash,
        dbhost => $oracle::dbhost,
        webdir => $oracle::webdir,
        logoutput => $oracle::logoutput,
        require => [ File["create_db_10g"],
                     File["listener"],
                     File["tnsnames"],
                     Oracle::Get_scripts[$db_install_scripts] ],
    }

    oracle::start_listener { $oracle::install_dbs::database_sids :
        require => Oracle::Db_install[$oracle::install_dbs::database_sids],
    }


}
