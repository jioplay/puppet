define oracle::install_dirs {
    file { $name :
        ensure => directory,
        owner => "oracle",
        group => "dba",
        mode => "0750",
    }
}
