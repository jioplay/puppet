class oracle::config {

    exec { "oracle-home" :
        path      => "/bin:/usr/bin",
        cwd       => "/home/oracle",
        command   => "tar -jxvf install/home.tar.bz2",
        unless    => [ "test -f /home/oracle/listener.ora" ],
        logoutput => $oracle::logoutput,
        require   => Oracle::Fetch_Tars["home.tar.bz2"],
    }

    file { "/etc/security/limits.d/oracle" :
        ensure => file,
        source => "puppet:///modules/oracle/oracle.limits",
        owner  => "root",
        group  => "root",
        mode   => "0640",
    }

    file { "/etc/init.d/oracle" :
        ensure => file,
        source => "puppet:///modules/oracle/oracle",
        owner  => "root",
        group  => "root",
        mode   => "0755",
    }

    file { "/var/app/oracle/oratab" :
        ensure => file,
        source => "puppet:///modules/oracle/oracle.oratab",
        owner  => "oracle",
        group  => "dba",
        mode   => "0640",
    }

    file { "/etc/oraInst.loc" :
        ensure => file,
        source => "puppet:///modules/oracle/oracle.oraInst.loc",
        owner  => "root",
        group  => "root",
        mode   => "0644",
    }

}
