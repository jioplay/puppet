define oracle::get_scripts {
    file { $name:
        ensure => file,
        owner => oracle,
        group => dba,
        source => "puppet:///modules/oracle/${name}",
        path => "/home/oracle/install/${name}",
        mode => 755,
    }
}
