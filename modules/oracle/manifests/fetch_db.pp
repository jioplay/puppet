define oracle::fetch_db ( $sid, $dbhost, $webdir, $logoutput, $datafile ) {

    exec { "fetch-${name}-${sid}" :
        path => "/usr/bin:/usr/sbin:/bin:/sbin/",
        user => "oracle",
        cwd => "/var/tmp",
        command => "/usr/bin/wget --quiet -N http://${dbhost}${webdir}/${datafile}",
        #give up after 50 minutes
        timeout => 3000,
        logoutput => $logoutput,
        unless => ["test -f /var/tmp/${datafile}",],
    }

    file { "link-${name}-${sid}" :
        ensure => link,
        path => "/home/oracle/datapump/${sid}-${datafile}",
        target => "/var/tmp/${datafile}",
        require => Exec["fetch-${name}-${sid}"],
    }
}
