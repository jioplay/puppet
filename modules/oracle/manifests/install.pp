class oracle::install
{
    $install_scripts = [
                        "10204_patchset.rsp",
                        "dbca.rsp",
                        "enterprise.rsp",
                        "oracle_main.sh",
                        "oracle_install.sh",
                        "oracle_postinstall_root.sh",
                        "oracle_install_patch.sh",
                        ]

    oracle::get_scripts { $oracle::install::install_scripts :
        require => File["/home/oracle/install"],
        before => Exec["run-install"],
    }


    exec { "run-install":
        path => "/usr/bin:/usr/sbin:/bin:/sbin/",
        cwd => "/home/oracle/install/",
        command => "/home/oracle/install/oracle_main.sh 2>&1 > oracle_install.log",
        require => [
                    File["/home/oracle/install/oracle_main.sh"],
                    File["/home/oracle/install/oracle_install.sh"],
                    File["/home/oracle/install/oracle_install_patch.sh"],
                    File["/home/oracle/install/oracle_postinstall_root.sh"],
                    File["/home/oracle/install/enterprise.rsp"],
                    File["/home/oracle/install/10204_patchset.rsp"],
                    Exec["oracle_10g_10201.tar"],
                    Exec["oracle_10g_10204.tar"]
                    ],
        timeout => 6000,
        #don"t run this again if the binaries exist
        unless => ["test -f /var/app/oracle/product/10204/bin/impdp","test -f /var/app/oracle/product/10204/bin/sqlplus"],
        logoutput => $oracle::logoutput,
    }

}
