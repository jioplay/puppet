define oracle::fetch_tars ($logoutput) {

    exec { "${name}":
        #make sure the oracle user fetches them to the install directory
        path => "/usr/bin:/usr/sbin:/bin:/sbin/",
        user => "oracle",
        cwd => "/home/oracle/install",
        command => "/usr/bin/wget --quiet -N http://cobbler:80/pkg_files/new_oracle/${name}",
        #give up after 50 minutes
        timeout => 3000,
        #this means that all files have to match md5sum
        unless => ["test -f /home/oracle/install/oracle_install.log","md5sum --status -c /home/oracle/install/large.md5", ],
        logoutput => $logoutput,
    }
}
