define oracle::start_listener ($sid=$name) {

    #pick the correct oracle version
    $oracle_home =  $sid ? {
        "mobi2" => "/var/app/oracle/product/10204",
        default => "/var/app/oracle/product/10204",
    }

    $oracle_sid = $sid ? {
        "mobi2" => "MOBI2",
        default => $sid,
    }

    $db_listener = $sid ? {
        mobi2 => "LIS_MOBI2",
        prdspt => "LIS_PRDSPT",
        prdjb => "LIS_PRDJB",
        prdsub => "LIS_PRDSUB",
    }

    Exec {
        user => "oracle",
        path => "/bin:/usr/bin:/var/opt/oracle/prod/:${oracle_home}/bin",
        cwd => "/home/oracle/datapump",
        environment => [ "DB=${sid}",
                         "ORACLE_SID=${oracle_sid}",
                         "OHOME=${oracle_home}",
                         "ORACLE_HOME=${oracle_home}",
                         "OBASE=/home/oracle",
                         "ORACLE_BASE=/home/oracle",
                         "HOME=/home/oracle",
                         "TERM=dumb",
                         ],
        provider => shell,
        timeout => 6000,
    }

    exec { "${sid}-start-listener":
        command => "lsnrctl start ${db_listener}",
        unless => "[[ $(lsnrctl status | grep -c 'Instance \"${oracle_sid}') == 2 ]]"
    }
}
