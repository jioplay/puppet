#This manifest installs the dependencies for an oracle instal.
#This has been relocated from a cobbler snippet.
# alevy@mobitv
class oracle::deps {
    include accounts

    $oracle_deplist = [ "automake",
                        "binutils",
                        "compat-db.i386",
                        "compat-glibc.i386",
                        "compat-libgcc-296.i386",
                        "compat-libstdc++-296.i386",
                        "compat-libstdc++-33.i386",
                        "compat-libstdc++-33.x86_64",
                        "emacs",
                        "flex",
                        "gcc",
                        "gettext",
                        "glibc-devel.i386",
                        "glibc-devel.x86_64",
                        "libXtst.i386",
                        "libXtst.x86_64",
                        "libgcc.i386",
                        "libgcc.x86_64",
                        "libstdc++.i386",
                        "libstdc++.x86_64",
                        "libtool",
                        "libxml2-devel",
                        "make",
                        "openssl-devel",
                        "patch",
                        "pkgconfig",
                        "redhat-rpm-config",
                        "rpm-build",
                        "wget",
                        ]

    oracle::dep_helper { $oracle_deplist : }

    realize(Accounts::Group["dba"])
    realize(Accounts::User["oracle"])
}
