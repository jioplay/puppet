define oracle::data_dirs {
    File {
        owner => "oracle",
        group => "dba",
        mode => 750,
    }

    file { "/data/${name}":
        ensure => directory,
    }

    file { "/${name}":
        ensure => link,
        target => "/data/${name}",
    }
}
