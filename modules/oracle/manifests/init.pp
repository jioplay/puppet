# == Class: oracle
#
#    installs oracle and imports repos and images
#
# === Parameters:
#
#    $database_hash: { sid => dbname } hash to install on this machine
#    $dbhost:        the host containing the dumps
#    $webdir:        directory on host with dumps
#    $logoutput:     enable debugging mode (D:false)
#    $metadata_only: disable import of data only do metadata (D:false)
#
# === Requires
#
#   os-static-network
#   java
#
# === Sample Usage
#  class { "oracle" :
#       database_sids => {"jb_aaa" => "prdjb"},
#       dbhost => "db02.oak1.mobitv",
#       webdir => "/dpump",
#       require => Class["java"],
#      }
#
# Remember: No empty lines between comments and class definition
#
class oracle
(
    $database_hash = $oracle::params::database_hash,
    $dbhost        = $oracle::params::dbhost,
    $webdir        = $oracle::params::webdir,
    $logoutput     = $oracle::params::logoutput,
    $metadata_only = $oracle::params::metadata_only,
)
inherits oracle::params
{
    anchor { "begin" :} ->
    class  { oracle::deps : } ->
    class  { oracle::dirs : } ->
    class  { oracle::pkg : } ->
    class  { oracle::config : } ->
    class  { oracle::install : } ->
    class  { oracle::service : } ->
    class  { oracle::install_dbs : } ->
    anchor { "end":}

    os::motd::register { $name: }
}
