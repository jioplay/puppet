define oracle::db_install ( $hash, $dbhost, $webdir, $logoutput, $sid=$name) {

    $db_src = $hash[$name]

    #pick the correct oracle version
    $oracle_home =  $sid ? {
        "mobi2" => "/var/app/oracle/product/10204",
        default => "/var/app/oracle/product/10204",
    }

    $oracle_sid = $sid ? {
        "mobi2" => "MOBI2",
        default => $sid,
    }

    Exec {
        user => "oracle",
        path => "/bin:/usr/bin:/var/opt/oracle/prod/:${oracle_home}/bin",
        cwd => "/home/oracle/datapump",
        environment => [ "DB=${sid}",
                         "ORACLE_SID=${oracle_sid}",
                         "OHOME=${oracle_home}",
                         "ORACLE_HOME=${oracle_home}",
                         "OBASE=/home/oracle",
                         "ORACLE_BASE=/home/oracle",
                         "HOME=/home/oracle",
                         "TERM=dumb",
                         ],
        provider => shell,
        logoutput => $logoutput,
        timeout => 6000,
    }

    os::motd::register { "oracle::db_install::${sid}" : }

    exec { "create-${sid}" :
        command => "create_db_10g.sh ${oracle_sid}",
        #check for ora processes
        unless => "ps aux | grep -vq grep | grep -iEq 'ora_.*_${sid}'",
    }

    Oracle::Fetch_db {
        sid => $sid,
        dbhost => $dbhost,
        webdir => $webdir,
        logoutput => $logoutput,
    }

    oracle::fetch_db { "${sid}-${db_src}_METADATA" :
        datafile => "exp_${db_src}_METADATA_ONLY.dmp",
    }

    oracle::fetch_db { "${sid}-${db_src}_DATA" :
        datafile => "exp_${db_src}_DATA_ONLY.dmp",
    }

    exec { "imp-meta-${sid}" :
        command => "impdp system/mobitv DIRECTORY=DATA_PUMP_DIR LOGFILE=${sid}_METADATA_ONLY.log DUMPFILE=${sid}-exp_${db_src}_METADATA_ONLY.dmp",
        require => [ Oracle::Fetch_db["${sid}-${db_src}_METADATA"], Exec["create-${sid}"], ],
        unless => "test -f /home/oracle/datapump/${sid}_METADATA_ONLY.log",
    }

    exec { "chg-pass-${sid}" :
        command => "sqlplus \"/ as sysdba\" @/home/oracle/install/ch_pswrds.sql",
        require => Exec["imp-meta-${sid}"],
        unless => "test -f /home/oracle/datapump/${sid}_DATA_ONLY.log",
    }

    exec { "disable-obj-${sid}" :
        command => "sqlplus \"/ as sysdba\" @/home/oracle/install/disable_obj.sql",
        require => Exec["chg-pass-${sid}"],
        unless => "test -f /home/oracle/datapump/${sid}_DATA_ONLY.log",
    }

    if ( ! $oracle::metadata_only ) {
        exec { "imp-data-${name}" :
            command => "impdp system/mobitv DIRECTORY=DATA_PUMP_DIR LOGFILE=${sid}_DATA_ONLY.log DUMPFILE=${sid}-exp_${db_src}_DATA_ONLY.dmp",
            require => [ Oracle::Fetch_db["${sid}-${db_src}_DATA"], Exec["disable-obj-${sid}"], ],
            unless => "test -f /home/oracle/datapump/${sid}_DATA_ONLY.log",
        }
    }

    exec { "enable-obj-${sid}" :
        command => "sqlplus \"/ as sysdba\" @/home/oracle/install/enable_obj.sql",
        require => Exec["imp-data-${sid}"],
        unless => "[[ $(lsnrctl status | grep -c 'Instance \"${oracle_sid}') == 2 ]]"
    }
}
