## == Class: mobi_mediatools
#
# Manage the mobi_mediatools components.
#
# === Parameters:
#
# No configuration parameters for this module
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_mediatools (
    $package = $mobi_mediatools::params::package,
    $version = $mobi_mediatools::params::version,
    $auto_update = $mobi_mediatools::params::auto_update,
) inherits mobi_mediatools::params {

    anchor { "mobi_mediatools::begin": } ->
    class { "mobi_mediatools::install": } ->
    anchor { "mobi_mediatools::end": }

    os::motd::register{ "${name}":}
}
