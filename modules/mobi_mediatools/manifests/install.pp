class mobi_mediatools::install {

    $package_name = $mobi_mediatools::version ? {
        ""      => $mobi_mediatools::package,
        default => "$mobi_mediatools::package-$mobi_mediatools::version"
    }

    if $mobi_mediatools::auto_update == true {

        package { "$mobi_mediatools::install::package_name":
          provider => yum,
          ensure   => latest,
        }
      } else {
        package { "$mobi_mediatools::install::package_name":
          ensure => present,
        }
      }
}
