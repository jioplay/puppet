class mobi_notification_manager::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-notification-manager/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    $package = "mobi-notification-manager"
    $package_ensure = "present"

    $notificationmanager_properties_hash = {}
}
