class mobi_notification_manager::config {
    File {
        owner => rtv,
        group => rtv,
        mode => "0644",
    }
    file { "/opt/mobi-notification-manager/webapp/WEB-INF/classes/notificationmanager.properties":
        ensure => present,
        content => template("mobi_notification_manager/notificationmanager.properties.erb"),
        require => Class["mobi_notification_manager::install"],
        notify => Class["tomcat::service"],
    }
}
