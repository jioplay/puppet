class mobi_notification_manager::install {

    package { $::mobi_notification_manager::package:
      ensure => $::mobi_notification_manager::package_ensure,
      notify => Class["tomcat::service"],
    }
}
