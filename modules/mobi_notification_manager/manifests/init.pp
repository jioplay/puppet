class mobi_notification_manager(
    ###icinga.pp
    $icinga = $mobi_notification_manager::params::icinga,
    $icinga_instance = $mobi_notification_manager::params::icinga_instance,
    $icinga_cmd_args = $mobi_notification_manager::params::icinga_cmd_args,
    ###end icinga.pp
  $package = $::mobi_notification_manager::params::package,
  $package_ensure = $::mobi_notification_manager::params::package_ensure,
  $notificationmanager_properties_hash = $mobi_notification_manager::params::notificationmanager_properties_hash,
)
inherits mobi_notification_manager::params{
  anchor { "mobi_notification_manager::begin":} ->
  class{"mobi_notification_manager::install":} ->
  class{"mobi_notification_manager::config":} ->
    class { "mobi_notification_manager::icinga":} ->
  anchor { "mobi_notification_manager::end": }

  os::motd::register { $name : }
}
