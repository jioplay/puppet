class app_upgrade_html::install {
  package { "${app_upgrade_html::package}-${app_upgrade_html::version}":
    ensure => present,
  }
}
