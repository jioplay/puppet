class app_upgrade_html::params {
  $version = udef
  $package = "app-upgrade-html"
  $rewrite_rule = undef
  $proxy_pass_list = []
  $web_content_dir = "/var/www/html"
}
