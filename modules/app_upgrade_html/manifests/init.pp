## == Class: app_upgrade_html
#
# Manage the app_upgrade_html component.
#
# === Parameters:
# List parameters and descriptions here with (default value)
# version: no default value
# package: app-upgrade-html (default)
# rewrite_rule: no default value
# proxy_pass_list: no default value
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#class { "app_upgrade_html":
#    version => "0.0.0.1",
#    package => "app-upgrade-html",
#    rewrite_rule => "^/app-upgrade/(.*)$",
#    proxy_pass_list => [
#      "ProxyPassMatch /ril-api-upgrade-html/(.*)$ http://127.0.0.1/$1",
#    ]
#}
#
# [Remember: No empty lines between comments and class definition]
#
class app_upgrade_html (
  $version = $::app_upgrade_html::params::version,
  $package = $::app_upgrade_html::params::package,
  $rewrite_rule = $::app_upgrade_html::params::rewrite_rule,
  $proxy_pass_list = $::app_upgrade_html::params::proxy_pass_list,
) inherits app_upgrade_html::params {

    anchor { "app_upgrade_html::begin": } ->
    class { "app_upgrade_html::install": } ->
    class { "app_upgrade_html::config": } ->
    anchor { "app_upgrade_html::end": }

    os::motd::register{ "${name}":}
}
