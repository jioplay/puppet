class app_upgrade_html::config {
  File {
    owner => root,
    group => root,
    mode  => 0664,
  }

  file { "/etc/httpd/conf.d/${app_upgrade_html::package}.conf":
    ensure => file,
    content => template("app_upgrade_html/app-upgrade-html.conf.erb"),
    require => Class["app_upgrade_html::install"],
    notify => Class["apache::service"],
  }
}
