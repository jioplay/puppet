# = Class: timezone
#
# Sets the host time zone
#
# == Parameters:
#
# $zone::          the standard name for the requested timezone e.g. "Europe/Amsterdam"
#
#
# == Actions:
#     Sets the host time zone
#
# == Requires:
#
# == Sample Usage
#       class { "timezone":
#               $zone => "Europe/Amsterdam",
#             }
#
# Remember: No empty lines between comments and class definition
#
class timezone ($zone = "UTC") {
  file {
    '/etc/localtime' :
      ensure => 'link',
      target => "/usr/share/zoneinfo/$zone",
  }
}
