class memcached::install {
    package { "${memcached::package}-${memcached::version}":
        ensure => present,
        notify => Service["memcached"],
        }
}
