class memcached::config {
    file { "/etc/sysconfig/memcached":
        ensure => present,
        owner => "root",
        group => "root",
        source => "puppet:///modules/memcached/${memcached::config}",
        notify => Service["memcached"],
    }
}
