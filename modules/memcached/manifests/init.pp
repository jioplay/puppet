# == Class: memcached
#
# installs memcached and starts it
#
# === Parameters:
#
#    $package::      the memcached package name
#    $version::      the memcached package version
#    $srvc_ensure::   state of service (running, etc.)
#    $srvc_enable::   chkconfig on (tru|false)
#
# === Requires
#
# === Sample Usage
#
#  class { "memcached" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class memcached (
    $package = $memcached::params::package,
    $version = $memcached::params::version,
    $srvc_ensure = $memcached::params::srvc_ensure,
    $srvc_enable = $memcached::params::srvc_enable,
    $config = $memcached::params::config,
)
inherits memcached::params {
    include memcached::install, memcached::config, memcached::service
    anchor { "memcached::begin": } -> Class["memcached::install"]
    Class["memcached::config"] -> anchor { "memcached::end": }
    os::motd::register { $name : }
}
