class memcached::params {
    $package = "memcached"
    $version = "1.4.5-1"
    $srvc_ensure = running
    $srvc_enable = true
    $config = "memcached.mgo"
}
