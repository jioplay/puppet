class memcached::service {
    service { "memcached":
        ensure => $memcached::srvc_ensure,
        enable => $memcached::srvc_enable,
    }
}
