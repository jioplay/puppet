# == Class: mobi_jobdistributor
#
#  installs mobi_jobdistributor
#
# === Parameters:
#
#    $package::        The mobi_jobdistributor package name (D: mobi-recordingwriter)
#
#    $version::        The mobi_jobdistributor package version (D: N/A)
#
#    $jd_user_name::     The user account that the service should drop privileges to. (D: rtv)
#                        May be left empty in which case the service will keep running as root
#                        If assigned then that user account must exists and the group identifier
#                        cannot be empty
#
#    $jd_group_name::    The user group that the service should drop privileges to. (D: rtv)
#                        May be left empty in which case the service will keep running as root
#                        If assigned then that user group must exists and the user identifier
#                        cannot be empty
#
#    $jd_rest_nic::      The NIC to bind the REST api to. (D: N/A)
#                        May be left empty in which case REST calls will be accepted on any NIC.
#
#    $jd_rest_port::     The port that the REST api is listening to (D: 4884)
#
#    $shard_amq_broker_hosts::  host/cname/vip name that should be listed as broker. (D: N/A)
#
#    $shard_request_channel_name:: The name of the ActiveMQ queue that the job distributor will send
#                                  requests to the recording writer (D: SHARD.REQUESTS)
#
#    $shard_control_channel_name:: The name of the ActiveMQ topic that the job distributor will send
#                               control message to the recording writer (D: SHARD.CONTROL)
#
#    $netpvr_request_queue_name:: The name of the external queue that recording manager may post requests to
#                                 Optional: append a / and broker information when a another activemq broker
#                                 is used compared the the shard messages (D: NETPVR.REQUESTS)
#
#    $netpvr_amq_broker_hosts:: The host/cname/vip names of the active mq brokers that should be used for messages
#                               posted from outside of the netpvr (i.e recording manager) (D: ["amq01", "amq02"])
#
#    $jd_shard_identifier:: The test identifier for this shard. MUST be assigned. (D: N/A)
#
#    $segment_duration:: The length in seconds of one recording piece on the recording writer.
#                        This should correspond to the fragment file length (D: 180)
#
#    $jd_recording_start_delay:: The number of seconds to wait after the actual start time is reached
#                                before issuing a recording command to the recording writer (D: 10)
#
#    $jd_recording_hold_duration:: The number of seconds after a recording is completed that a record
#                                  of the recording is maintained in the netpvr. (D: 300)
#
#    $jd_recording_response_timeout:: The timeout value in seconds that a response for a recording must
#                                     arrive from the recording writer. This should not be lower than
#                                     the $segment_duration value. (D: 200)
#
#    $jd_recording_stop_timeout:: The timeout value in seconds that a response for a stop request must
#                                 arrive from the recording writer. (D: 15)
#
#    $jd_recording_delete_timeout:: The timeout value in seconds that a response for a delete request must
#                                 arrive from the recording writer. (D: 15)
#
#    $jd_db_engine:: The engine type for the database. Could be one of
#                    (oracle, postgre, sqlite)   (D: N/A)
#     $jd_db_connect:: Information needed to connect to the database. This can be information such as host
#                     port, user, password etc. The format of this value is dependent on the engine type
#                     (D: N/A)
#    $jd_shard_table_name:: The name of the database table to be used for this specific shard.
#                           Typically something like NETPVR_JOBS_{shard identifier}. This must be
#                           coordinated with the setup of the database as typically the user account for
#                           the job distributor do not have privileges to create the table (D: N/A)
#    $service_ensure:: controls the ensure parameter passed to the service. Defaults to running
#
#
# === Requires
#
#     -
#
# === Sample Usage
#
#  class { "mobi_jobdistributor" :
#        $shard_amq_broker_hosts => ["CHANGE_TO_SHARD_AMQ_CNAME_1","CHANGE_TO_SHARD_AMQ_CNAME_2",....],
#        $jd_shard_identifier => "CHANGE_TO_SHARD_IDENTIFIER",
#        $jd_db_engine => "oracle",
#        $jd_db_connect => "user password host:port:sid",
#        $jd_shard_table_name => "NETPVR_JOBS_CHANGE_TO_SHARD_IDENTIFIER"
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_jobdistributor (
    ###icinga.pp
    $icinga = hiera('mobi_jobdistributor::icinga',$mobi_jobdistributor::params::icinga),
    $icinga_instance = hiera('icinga_instance',$mobi_jobdistributor::params::icinga_instance),
    $icinga_cmd_args = hiera('mobi_jobdistributor::icinga_cmd_args',$mobi_jobdistributor::params::icinga_cmd_args),
    ###end icinga.pp
    $package = hiera('mobi_jobdistributor::package',$mobi_jobdistributor::params::package),
    $version = hiera('mobi_jobdistributor::version',$mobi_jobdistributor::params::version),

    $jd_user_name=hiera('mobi_jobdistributor::jd_user_name',$mobi_jobdistributor::params::jd_user_name),
    $jd_group_name=hiera('mobi_jobdistributor::jd_group_name',$mobi_jobdistributor::params::jd_group_name),
    $jd_rest_nic=hiera('mobi_jobdistributor::jd_rest_nic',$mobi_jobdistributor::params::jd_rest_nic),
    $jd_rest_port=hiera('mobi_jobdistributor::jd_rest_port',$mobi_jobdistributor::params::jd_rest_port),
    $shard_amq_broker_hosts=hiera('mobi_jobdistributor::shard_amq_broker_hosts',$mobi_jobdistributor::params::shard_amq_broker_hosts),
    $shard_request_channel_name=hiera('mobi_jobdistributor::shard_request_channel_name',$mobi_jobdistributor::params::shard_request_channel_name),
    $shard_control_channel_name=hiera('mobi_jobdistributor::shard_control_channel_name',$mobi_jobdistributor::params::shard_control_channel_name),
    $netpvr_request_queue_name=hiera('mobi_jobdistributor::netpvr_request_queue_name',$mobi_jobdistributor::params::netpvr_request_queue_name),
    $netpvr_amq_broker_hosts=hiera('mobi_jobdistributor::netpvr_amq_broker_hosts',$mobi_jobdistributor::params::netpvr_amq_broker_hosts),
    $jd_shard_identifier=hiera('mobi_jobdistributor::jd_shard_identifier',$mobi_jobdistributor::params::jd_shard_identifier),
    $segment_duration=hiera('mobi_jobdistributor::segment_duration',$mobi_jobdistributor::params::segment_duration),
    $jd_recording_start_delay=hiera('mobi_jobdistributor::jd_recording_start_delay',$mobi_jobdistributor::params::jd_recording_start_delay),
    $jd_recording_hold_duration=hiera('mobi_jobdistributor::jd_recording_hold_duration',$mobi_jobdistributor::params::jd_recording_hold_duration),
    $jd_recording_response_timeout=hiera('mobi_jobdistributor::jd_recording_response_timeout',$mobi_jobdistributor::params::jd_recording_response_timeout),
    $jd_recording_stop_timeout=hiera('mobi_jobdistributor::jd_recording_stop_timeout',$mobi_jobdistributor::params::jd_recording_stop_timeout),
    $jd_recording_delete_timeout=hiera('mobi_jobdistributor::jd_recording_delete_timeout',$mobi_jobdistributor::params::jd_recording_delete_timeout),
    $jd_db_engine=hiera('mobi_jobdistributor::jd_db_engine',$mobi_jobdistributor::params::jd_db_engine),
    $jd_db_connect=hiera('mobi_jobdistributor::jd_db_connect',$mobi_jobdistributor::params::jd_db_connect),
    $jd_shard_table_name=hiera('mobi_jobdistributor::jd_shard_table_name',$mobi_jobdistributor::params::jd_shard_table_name),
    $service_ensure=hiera('mobi_jobdistributor::service_ensure',"running"),
)
inherits mobi_jobdistributor::params {

    include mobi_jobdistributor::install, mobi_jobdistributor::config, mobi_jobdistributor::service

    anchor { "mobi_jobdistributor::begin": } -> Class["mobi_jobdistributor::install"]
    Class["mobi_jobdistributor::config"] ~> Service["job_distributor"] ->
    class { "mobi_jobdistributor::icinga":} ->
    anchor { "mobi_jobdistributor::end": }

    os::motd::register { $name : }
}
