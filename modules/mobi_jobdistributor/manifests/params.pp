class mobi_jobdistributor::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-H $::ipaddress -p 4884 -u /jobdistributor/monitoring/health -w 5 -c 10 -v"
    ###end icinga.pp

    # install.pp variables
    $package = "mobi-jobdistributor"
    $version = ""

    # config.pp variables
    $jd_user_name="rtv"
    $jd_group_name="rtv"
    $jd_rest_port="4884"
    $shard_amq_broker_hosts= ["nvamq01","nvamq02"]
    $shard_request_channel_name="SHARD.REQUESTS"
    $shard_control_channel_name="SHARD.CONTROL"
    $netpvr_request_queue_name="NETPVR.REQUESTS"
    $netpvr_amq_broker_hosts=["amq01", "amq02"]
    $jd_shard_identifier=false
    $segment_duration="180"
    $jd_recording_start_delay="10"
    $jd_recording_hold_duration="300"
    $jd_recording_response_timeout="200"
    $jd_recording_stop_timeout="15"
    $jd_recording_delete_timeout="15"
    $jd_db_engine=false
    $jd_db_connect=false
    $jd_shard_table_name=false
}
