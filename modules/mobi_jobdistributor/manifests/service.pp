class mobi_jobdistributor::service {
  service { "job_distributor":
    ensure     => "${mobi_jobdistributor::service_ensure}",
    provider   => "redhat",
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    require    => Class["mobi_jobdistributor::install"],
    subscribe  => Class["mobi_jobdistributor::config"],
  }
}
