class mobi_jobdistributor::config {

    File {
        owner => root,
        group => root,
        mode => "0644",
    }

    if ! $mobi_jobdistributor::jd_db_engine {
        fail ("You must configure the database engine")
      }
    $jd_db_engine = $mobi_jobdistributor::jd_db_engine
    if ! $mobi_jobdistributor::jd_db_connect {
        fail("You must configure the connect information for the database engine")
        }
    $jd_db_connect = $mobi_jobdistributor::jd_db_connect
    if ! $mobi_jobdistributor::jd_shard_table_name {
        fail("You must configure the database table name for this shard")
          }
    $jd_shard_table_name = $mobi_jobdistributor::jd_shard_table_name
    if ! $mobi_jobdistributor::jd_shard_identifier {
        fail("You must configure the shard identifier")
     }
    $jd_shard_identifier = $mobi_jobdistributor::jd_shard_identifier
    if ! $mobi_jobdistributor::shard_amq_broker_hosts {
        fail("You must configure the active mq broker cname(s) for this shard")
              }
    $shard_amq_broker_hosts = $mobi_jobdistributor::shard_amq_broker_hosts
    $jd_user_name=$mobi_jobdistributor::jd_user_name
    $jd_group_name=$mobi_jobdistributor::jd_group_name
    $jd_rest_nic=$mobi_jobdistributor::jd_rest_nic
    $jd_rest_port=$mobi_jobdistributor::jd_rest_port
    $shard_request_channel_name=$mobi_jobdistributor::shard_request_channel_name
    $shard_control_channel_name=$mobi_jobdistributor::shard_control_channel_name
    $netpvr_request_queue_name=$mobi_jobdistributor::netpvr_request_queue_name
    $netpvr_amq_broker_hosts=$mobi_jobdistributor::netpvr_amq_broker_hosts
    $segment_duration=$mobi_jobdistributor::segment_duration
    $jd_recording_start_delay=$mobi_jobdistributor::jd_recording_start_delay
    $jd_recording_hold_duration=$mobi_jobdistributor::jd_recording_hold_duration
    $jd_recording_response_timeout=$mobi_jobdistributor::jd_recording_response_timeout
    $jd_recording_stop_timeout=$mobi_jobdistributor::jd_recording_stop_timeout
    $jd_recording_delete_timeout=$mobi_jobdistributor::jd_recording_delete_timeout

    file { "/usr/local/ods/job-distributor/conf.d/job_distributor_config.xml":
        ensure  => present,
        content => template("mobi_jobdistributor/job_distributor_config.xml.erb"),
        require => Class["mobi_jobdistributor::install"],
    }

    file { "/etc/logrotate.d/jobdistributor":
              source => "puppet://$puppetserver/modules/mobi_jobdistributor/jobdistributor.logrotate",
    }

    # add rsyslog files if rsyslog being used
    if defined(Class["rsyslog"]) {
        file { "/etc/rsyslog.d/mobi_jobdistributor.conf":
            ensure => present,
            notify => Class["rsyslog::service"],
            source => "puppet:///modules/mobi_jobdistributor/rsyslog/mobi_jobdistributor.conf",
        }

        file { "/etc/rsyslog.d/mobi_jobdistributor_debug.conf":
            ensure => present,
            notify => Class["rsyslog::service"],
            source => "puppet:///modules/mobi_jobdistributor/rsyslog/mobi_jobdistributor_debug.conf",
        }
    }
}
