class mobi_jobdistributor::install {
  package { "$mobi_jobdistributor::package":
    provider => "yum",
    ensure   => "$mobi_jobdistributor::version",
  }
}