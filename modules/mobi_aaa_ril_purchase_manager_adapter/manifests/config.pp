class mobi_aaa_ril_purchase_manager_adapter::config {
 file { "/opt/mobi-aaa-ril-purchase-manager-adapter/webapp/WEB-INF/classes/application-config-default.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_aaa_ril_purchase_manager_adapter/application-config-default.properties.erb'),
        require => Class["mobi_aaa_ril_purchase_manager_adapter::install"],
        notify   => Class["tomcat::service"],
    }

}
