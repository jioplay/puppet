class mobi_aaa_ril_purchase_manager_adapter::install {
    package {"mobi-aaa-ril-purchase-manager-adapter-${mobi_aaa_ril_purchase_manager_adapter::mobi_aaa_ril_purchase_manager_adapter_version}":
      ensure => present,
      notify => [Class["tomcat::service"],],
    }
}
