## == Class: mobi_aaa_ril_purchase_manager_adapter
#
# Manage the mobi_aaa_ril_purchase_manager_adapter component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
# mobi_aaa_ril_purchase_manager_adapter_version -> The version
# of the purchase manager adapter rpm.
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_aaa_ril_purchase_manager_adapter (
  $mobi_aaa_ril_purchase_manager_adapter_version =
  $mobi_aaa_ril_purchase_manager_adapter::params::mobi_aaa_ril_purchase_manager_adapter_version,

 $properties_context = $mobi_aaa_ril_purchase_manager_adapter::params::properties_context,
   $application_name = $mobi_aaa_ril_purchase_manager_adapter::params::application_name,
$merchant_username = $mobi_aaa_ril_purchase_manager_adapter::params::merchant_username,
$merchant_password = $mobi_aaa_ril_purchase_manager_adapter::params::merchant_password,
$rpsl_server_url = $mobi_aaa_ril_purchase_manager_adapter::params::rpsl_server_url,
$check_balance_url = $mobi_aaa_ril_purchase_manager_adapter::params::check_balance_url,
$purchase_url = $mobi_aaa_ril_purchase_manager_adapter::params::purchase_url,
$idm_profile_dynamic_mobilenumber_attribute =  $mobi_aaa_ril_purchase_manager_adapter::params::idm_profile_dynamic_mobilenumber_attribute,
   $auth_identity_manager_url = $mobi_aaa_ril_purchase_manager_adapter::params::auth_identity_manager_url,
   $auth_rights_manager_url = $mobi_aaa_ril_purchase_manager_adapter::params::auth_rights_manager_url,
    $jerseyclient_connection_timeout_seconds = $mobi_aaa_ril_purchase_manager_adapter::params::jerseyclient_connection_timeout_seconds,
    $jersey_client_read_timeout_seconds = $mobi_aaa_ril_purchase_manager_adapter::params::jersey_client_read_timeout_seconds,
   $jerseyclient_httpconnection_poolsize = $mobi_aaa_ril_purchase_manager_adapter::params::jerseyclient_httpconnection_poolsize,
$http_proxy_enabled = $mobi_aaa_ril_purchase_manager_adapter::params::http_proxy_enabled,
$http_proxy_host_address = $mobi_aaa_ril_purchase_manager_adapter::params::http_proxy_host_address,
$http_proxy_port = $mobi_aaa_ril_purchase_manager_adapter::params::http_proxy_port,
$retry_rules_enabled = $mobi_aaa_ril_purchase_manager_adapter::params::retry_rules_enabled,
)
inherits mobi_aaa_ril_purchase_manager_adapter::params {
    anchor { "mobi_aaa_ril_purchase_manager_adapter::begin": } ->
    class { "mobi_aaa_ril_purchase_manager_adapter::install": } ->
    class { "mobi_aaa_ril_purchase_manager_adapter::config": } ->
    anchor { "mobi_aaa_ril_purchase_manager_adapter::end": }
os::motd::register{ "${name}":}
}
