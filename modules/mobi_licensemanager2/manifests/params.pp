class mobi_licensemanager2::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /licensemanager2/monitoring/health -w 5 -c 10"
    ###end icinga.pp

    # install.pp variables
    $package = "mobi-server-drm-licensemanager2"
    $version = ""

    # config.pp variables
    $db_type="POSTGRESQL"
    $authnz_enabled=true
    $authnz_identity_url="http://identitymanagervip:8080/mobi-aaa-dt-identity-manager-oauth2"
    $authnz_rights_url="http://localhost:8080/mobi-aaa-rights-manager"
    $keystore="/opt/drm/conf/keystore"
    $ri_url=undef
    $pek_duration="1h"
    $sek_duration="14d"
    $sek_salt="ye4kf8ngnhd9749BDSlld03fkkgfpqjh"
    $pek_salt="wol58i7rc04e4213jdnv845jdmsjfruv"
    $max_peks_per_ro="24"
    $max_seks_per_ro="4"
    $cclock_before_drmtime="15m"
    $cclock_after_drmtime="15m"
    $vodprog_rec_ccache_time="365d"
    $rs_rights_url = "http://localhost:8080/mobi-aaa-rights-manager/core/v5/rights"
    $min_drmagent="1.0"
    $rs_ocsp=true
    $rs_ocsp_url= "http://ocspvip/drm/v2/ocsp/request"
    $rs_ocsp_strict_blocking=true
    $rs_qa_proxy=false
    $rs_qa_proxy_sku_regexp='.\*'
    $rs_qa_proxy_ex_rs_url="http://127.0.0.1:8080/roapserver/services/roap"
    $lm_salt_rotation_interval="10m"
    $client_cert_min_vtime="3650"
    $keypair_generation_batch="20"
    $keypair_generation_min="1500"
    $rs_resourceservice_connection_timeout="10000"
    $rs_resourceservice_read_timeout="10000"
    $rs_resourceservice_follow_redirects="true"
    $rs_resourceservice_threadpool_size="200"
    $salt_rotation_interval="7d"
    $salt_gen_time_buffer="30d"
    $lm_user="lm_user"
    $lm_user_pass="lm_user"
    $jdbc_driver="org.postgresql.Driver"
    $validation_query="select 1"
    $db_url=undef
    $roapserver_rights_start_threshold=0
    $roapserver_rights_end_threshold=0
    $memcached_host_list="127.0.0.1:11211"
    $salt_rotation_enabled=false
    $allow=".*"
}
