
class mobi_licensemanager2::httpd_proxymodule ()
  inherits mobi_licensemanager2::httpd_proxymodule::params {
    include mobi_licensemanager2::httpd_proxymodule::install
    anchor { "mobi_licensemanager2::httpd_proxymodule::begin":} -> Class["mobi_licensemanager2::httpd_proxymodule::install"]
    Class["mobi_licensemanager2::httpd_proxymodule::install"] -> anchor { "mobi_licensemanager2::httpd_proxymodule::end": }
    os::motd::register { $name : }
}
