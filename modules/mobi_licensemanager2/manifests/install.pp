class mobi_licensemanager2::install {
  # dependency handler
  # shouldn't this have a version number associated with it? This is strange
  package { "mobi-server-drm-deps": ensure => present, }

  package { "${::mobi_licensemanager2::package}":
    provider => yum,
    ensure   => "${::mobi_licensemanager2::version}",
    notify   => Class["tomcat::service"],
  }
}
