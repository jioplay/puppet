class mobi_licensemanager2::config {

    File {
        owner => rtv,
        group => rtv,
        mode => "0644",
    }
    #Need this becasue /opt/drm/conf owner,groupby default is root,root...we want rtv,rtv
    file { "/opt/drm/conf" : ensure => directory }

    #set to local variables for the template and check for required
    $db_type = $mobi_licensemanager2::db_type
    $authnz_enabled = $mobi_licensemanager2::authnz_enabled
    if ($mobi_licensemanager2::authnz_enabled) and (! $mobi_licensemanager2::authnz_identity_url) {
        fail("You must configure the identity manager url")
    }
    $authnz_identity_url =  $mobi_licensemanager2::authnz_identity_url
    if ($mobi_licensemanager2::authnz_enabled) and (! $mobi_licensemanager2::authnz_rights_url) {
        fail("You must configure the rights manager url")
    }
    $authnz_rights_url = $mobi_licensemanager2::authnz_rights_url
    $keystore = $mobi_licensemanager2::keystore
    if ! $mobi_licensemanager2::ri_url {
        fail("You must configure the rights manager url")
    }
    $ri_url = $mobi_licensemanager2::ri_url
    $pek_duration = $mobi_licensemanager2::pek_duration
    $sek_duration = $mobi_licensemanager2::sek_duration
    $pek_salt = $mobi_licensemanager2::pek_salt
    $sek_salt = $mobi_licensemanager2::sek_salt
    $max_peks_per_ro = $mobi_licensemanager2::max_peks_per_ro
    $max_seks_per_ro = $mobi_licensemanager2::max_seks_per_ro
    $cclock_before_drmtime = $mobi_licensemanager2::cclock_before_drmtime
    $cclock_after_drmtime = $mobi_licensemanager2::cclock_after_drmtime
    $vodprog_rec_ccache_time = $mobi_licensemanager2::vodprog_rec_ccache_time
    if ! $mobi_licensemanager2::rs_rights_url {
        fail("You must configure the roap server rights manager url")
    }
    $rs_rights_url = $mobi_licensemanager2::rs_rights_url
    $min_drmagent = $mobi_licensemanager2::min_drmagent
    $rs_ocsp = $mobi_licensemanager2::rs_ocsp
    if ! $mobi_licensemanager2::rs_ocsp_url {
        fail("You must specify the roapserver ocsp url")
    }
    $rs_ocsp_url = $mobi_licensemanager2::rs_ocsp_url
    $rs_ocsp_strict_blocking = $mobi_licensemanager2::rs_ocsp_strict_blocking
    $rs_qa_proxy = $mobi_licensemanager2::rs_qa_proxy
    $rs_qa_proxy_sku_regexp = $mobi_licensemanager2::rs_qa_proxy_sku_regexp
    $rs_qa_proxy_ex_rs_url = $mobi_licensemanager2::rs_qa_proxy_ex_rs_url
    $salt_rotation_interval = $mobi_licensemanager2::salt_rotation_interval
    $client_cert_min_vtime = $mobi_licensemanager2::client_cert_min_vtime
    $keypair_generation_batch = $mobi_licensemanager2::keypair_generation_batch
    $keypair_generation_min = $mobi_licensemanager2::keypair_generation_min
    $rs_resourceservice_connection_timeout = $mobi_licensemanager2::rs_resourceservice_connection_timeout
    $rs_resourceservice_read_timeout = $mobi_licensemanager2::rs_resourceservice_read_timeout
    $rs_resourceservice_follow_redirects = $mobi_licensemanager2::rs_resourceservice_follow_redirects
    $rs_resourceservice_threadpool_size = $mobi_licensemanager2::rs_resourceservice_threadpool_size
    $salt_gen_time_buffer = $mobi_licensemanager2::salt_gen_time_buffer
    $lm_salt_rotation_interval = $mobi_licensemanager2::lm_salt_rotation_interval
    $roapserver_rights_start_threshold = $mobi_licensemanager2::roapserver_rights_start_threshold
    $roapserver_rights_end_threshold = $mobi_licensemanager2::roapserver_rights_end_threshold
    $memcached_host_list = $mobi_licensemanager2::memcached_host_list
    $salt_rotation_enabled = $mobi_licensemanager2::salt_rotation_enabled

    file { "/opt/drm/conf/licensemanager.properties":
        ensure => present,
        owner => "rtv",
        group => "rtv",
        content => template("mobi_licensemanager2/licensemanager2.properties.erb"),
        require => Class["mobi_licensemanager2::install"],
        notify => Class["tomcat::service"],
    }
    $allow = $mobi_licensemanager2::allow
    $lm_user = $mobi_licensemanager2::lm_user
    $lm_user_pass = $mobi_licensemanager2::lm_user_pass
    $jdbc_driver = $mobi_licensemanager2::jdbc_driver
    $validation_query = $mobi_licensemanager2::validation_query

    if ! $mobi_licensemanager2::db_url {
        fail("You must set the LM database url")
    }
    $db_url = $mobi_licensemanager2::db_url


    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/licensemanager2.xml" :
        ensure => present,
        content => template("mobi_licensemanager2/licensemanager2.xml.erb"),
        require => Class["mobi_licensemanager2::install"],
        notify => Class["tomcat::service"],
    }
}
