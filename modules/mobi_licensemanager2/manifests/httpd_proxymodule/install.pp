class mobi_licensemanager2::httpd_proxymodule::install {

    file { "/etc/httpd/conf.d/mobi_licensemanager2.conf":
        ensure => present,
        owner  => "root",
        group  => "root",
        mode   => "0644",
        content => template('mobi_licensemanager2/httpd_proxymodule/httpd_mobi_licensemanager2.conf.erb'),
        require => Package["httpd"],
        notify  => Class["apache::service"],
    }
}
