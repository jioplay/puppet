class mobi_aaa_dt_adapter_rights_manager::install {

  package {
    "mobi-aaa-adapter-rights-manager-dt-${mobi_aaa_dt_adapter_rights_manager::rights_manager_adapter_version}":
      ensure => present,
      notify =>[ Class["tomcat::service"], ]
  }

}
