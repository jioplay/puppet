class mobi_aaa_dt_adapter_rights_manager::config {

  file {
    "/opt/mobi-aaa/config/rights-manager-dt/applicationProperties.xml":
      ensure => file,
      content => template('mobi_aaa_dt_adapter_rights_manager/applicationProperties.xml.erb'),
      owner => "rtv",
      group => "rtv",
      mode => "755",
      notify => Class["tomcat::service"],
      require => Class["mobi_aaa_dt_adapter_rights_manager::install"],
  }

}
