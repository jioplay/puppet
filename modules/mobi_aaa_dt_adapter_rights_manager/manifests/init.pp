# == class: mobi_aaa_dt_adapter_rights_manager
#
#  install rights manager adapter for DT
#
# === Parameters:
#    $rights_manager_adapter_version::
#
# === Requires:
#
#    java
#    tomcat
#
# === Sample Usage
#
#
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_dt_adapter_rights_manager(
    $rights_manager_adapter_version = $mobi_aaa_dt_adapter_rights_manager::params::rights_manager_adapter_version,
)
inherits mobi_aaa_dt_adapter_rights_manager::params {
    include mobi_aaa_dt_adapter_rights_manager::install, mobi_aaa_dt_adapter_rights_manager::config
    anchor { "mobi_aaa_dt_adapter_rights_manager::begin": } -> Class["mobi_aaa_dt_adapter_rights_manager::install"]
    Class["mobi_aaa_dt_adapter_rights_manager::config"] -> anchor { "mobi_aaa_dt_adapter_rights_manager::end" :}
    os::motd::register { $name : }
}

