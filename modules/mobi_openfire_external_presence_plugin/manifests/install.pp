class mobi_openfire_external_presence_plugin::install {

    if $::mobi_openfire_external_presence_plugin::mobi_openfire_external_presence_plugin_package_ensure !~ /^(absent|present|latest|ensure)$/ {
        fail("$name expects \$mobi_openfire_external_presence_plugin_package_ensure to be one of: absent, present, latest, or ensure.\nPlease add version to version param instead.")
    }

    package { "${::mobi_openfire_external_presence_plugin::mobi_openfire_external_presence_plugin_package}":
        ensure => $::mobi_openfire_external_presence_plugin::mobi_openfire_external_presence_plugin_package_ensure,
    } ->
    Class ["mobi_openfire::service"]->
    exec {"start dispatcher":
        path => "/",
        command => "/bin/sleep 15",
    } ->
    Class ["mobi_push_dispatcher::service"]
}
