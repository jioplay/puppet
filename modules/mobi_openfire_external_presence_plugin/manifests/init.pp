## == Class: mobi_openfire_external_presence_plugin
#
# Manage the mobi_openfire_external_presence_plugin component.
#
# === Parameters:
#
#  mobi_openfire_external_presence_plugin_package:: package name for the component
#  mobi_openfire_external_presence_plugin_package_ensure:: present or latest
#
# === Actions:
#
#  This plugin writes the Connect and Disconnect messages to activemq when client connects to Openfire server
#
# === Requires:
#
#  openfire
#
# === Examples:
#
#   class { "mobi_openfire_external_presence_plugin":
#       mobi_openfire_external_presence_plugin_package => "mobi-openfire-external-presence-plugin-3.8.0-244595.SNAPSHOT",
#       mobi_openfire_external_presence_plugin_package_ensure => "present",
#       require => Class["mobi_openfire"],
#   }
#
# [Remember: No empty lines between comments and class definition]
#
class mobi_openfire_external_presence_plugin (
    $mobi_openfire_external_presence_plugin_package = $mobi_openfire_external_presence_plugin::params::mobi_openfire_external_presence_plugin_package,
    $mobi_openfire_external_presence_plugin_package_ensure = $mobi_openfire_external_presence_plugin::params::mobi_push_message_manager_package_ensure,
) inherits mobi_openfire_external_presence_plugin::params {

    anchor { "mobi_openfire_external_presence_plugin::begin": } ->
    class { "mobi_openfire_external_presence_plugin::install": } ->
    anchor { "mobi_openfire_external_presence_plugin::end": }

    os::motd::register{ "${name}":}
}
