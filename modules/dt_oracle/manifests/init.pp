class dt_oracle
{
  include config::motd
  include yum::repo::clean
  include yum::repo::goldmaster
  include config::static-network
  #note changed by alex due to break out of db installation for
  #possibility of clean oracle db install
  include oracle::install_dbs
  include config::disable-puppet
  include oracle::start
  include oracle::clean
  include dt::profiles::database-install
  Class['config::static-network'] -> Class['oracle::install_dbs'] -> Class['oracle::start'] -> Class['oracle::clean'] -> Class['dt::profiles::database-install'] -> Class['config::disable-puppet']

}
