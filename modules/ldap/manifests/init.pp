class ldap::client::config {
    file { "/etc/openldap/ldap.conf":
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        source  => "puppet://$puppetserver/modules/ldap/ldap.conf",
    }

    file { "/etc/ldap.conf":
        ensure  => "/etc/openldap/ldap.conf",
    }
}
