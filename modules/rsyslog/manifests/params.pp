# Class: rsyslog::params
#
# This module manages rsyslog paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class rsyslog::params {

    ### install.pp
    $package = "rsyslog"
    $manage_package = "present"

    ### service.pp
    $service = "rsyslog"
    $service_ensure = "running"
    $service_enable = true

    ### config.pp
    $sysconfig_file = "rsyslog.sysconfig"
    $config_file = "rsyslog.conf"
    $disable_rate_limit_interval = false

}
