class rsyslog::config {

    File {
        ensure  => file,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        require => Class["rsyslog::install"],
    }

    exec { "/bin/rmdir /etc/sysconfig/rsyslog /etc/rsyslog.conf":
        onlyif => "/usr/bin/test -d /etc/sysconfig/rsyslog || /usr/bin/test -d /etc/rsyslog.conf",
        before => [ File["/etc/sysconfig/rsyslog"], File["/etc/rsyslog.conf"] ],
    }

    file { "/etc/sysconfig/rsyslog":
        source  => "puppet://$puppetserver/modules/rsyslog/${::rsyslog::sysconfig_file}",
    }

    file { "/etc/rsyslog.conf":
        ensure => present,
        content => template("rsyslog/el6/rsyslog.conf.erb"),
    }

    # Added by Greg Rice - create a syslog conf directory
    file { "/etc/rsyslog.d":
        ensure     => "directory",
        recurse    => true,
    }

    # Added to remove /etc/logrotate.d/syslog_ng
    file { ["/etc/logrotate.d/syslog-ng", "/etc/logrotate.d/.syslog"] :
        ensure => absent,
    }

    if $::operatingsystemrelease >= 6 {
        $syslog_source = "puppet://$puppetserver/modules/rsyslog/el6/syslog.logrotate"
    }else{
        $syslog_source = "puppet://$puppetserver/modules/rsyslog/syslog.logrotate"
    }

    file { "/etc/logrotate.d/syslog" :
        ensure => present,
        source => $syslog_source,
        mode   => "0644",
    }
}
