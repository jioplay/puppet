# Class: rsyslog
#
# This module manages rsyslog
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class rsyslog (

    $package = $rsyslog::params::package,
    $manage_package = $rsyslog::params::manage_package,

    $service = $rsyslog::params::service,
    $service_ensure = $rsyslog::params::service_ensure,
    $service_enable = $rsyslog::params::service_enable,

    $sysconfig_file = $rsyslog::params::sysconfig_file,
    $config_file = $rsyslog::params::config_file,
    $config_products = [],
    $disable_rate_limit_interval = $rsyslog::params::disable_rate_limit_interval,


) inherits rsyslog::params {

    if defined(Class["syslog_ng"]) {
        fail("Cannot include rsyslog and syslog-ng; choose one or the other")
    } else {

        anchor {  "rsyslog::begin": } ->
        class { "rsyslog::install": } ->
        class { "rsyslog::config": } ->
        anchor { "rsyslog::end": }

        # keep sep so modules can notify it
        class { "rsyslog::service": }

        # include module name in motd
        os::motd::register { $name: }
    }

}
