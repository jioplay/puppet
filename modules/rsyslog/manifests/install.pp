class rsyslog::install {
  package { $::rsyslog::package:
    ensure => $::rsyslog::manage_package,
  }
}
