class rsyslog::service {

    # /sbin/service fails, changing from redhat provider to init
    service { "syslog":
        ensure    => stopped,
        enable    => false,
        hasstatus => true,
    }

    service { "syslog-ng":
        ensure    => stopped,
        enable    => false,
        hasstatus => true,
    }

    service { $::rsyslog::service:
        ensure      => $::rsyslog::service_ensure,
        enable      => $::rsyslog::service_enable,
        hasstatus   => true,
        hasrestart  => true,
        subscribe   => [ Service["syslog"],
                         Service["syslog-ng"],
                         Class["rsyslog::install"],
                         Class["rsyslog::config"],
                         File["/etc/rsyslog.conf"],
                         File["/etc/rsyslog.d"], ],
    }

}
