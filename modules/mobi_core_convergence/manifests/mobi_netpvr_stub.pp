class mobi_core_convergence::mobi_netpvr_stub(
   $mobi_netpvr_stub_version = $mobi_core_convergence::mobi_netpvr_stub::params::mobi_netpvr_stub_version,
)
inherits mobi_core_convergence::mobi_netpvr_stub::params{
  include mobi_core_convergence::mobi_netpvr_stub::install
  anchor { "mobi_core_convergence::mobi_netpvr_stub::begin":} -> Class["mobi_core_convergence::mobi_netpvr_stub::install"] ->
  anchor { "mobi_core_convergence::mobi_netpvr_stub::end": }
  os::motd::register { $name : }
}
