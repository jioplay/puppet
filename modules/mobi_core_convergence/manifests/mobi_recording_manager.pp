class mobi_core_convergence::mobi_recording_manager(
   $mobi_recording_manager_version = $mobi_core_convergence::mobi_recording_manager::params::mobi_recording_manager_version,
)
inherits mobi_core_convergence::mobi_recording_manager::params{
  include mobi_core_convergence::mobi_recording_manager::install
  anchor { "mobi_core_convergence::mobi_recording_manager::begin":} -> Class["mobi_core_convergence::mobi_recording_manager::install"] ->
  anchor { "mobi_core_convergence::mobi_recording_manager::end": }
  os::motd::register { $name : }
}
