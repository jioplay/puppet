class mobi_core_convergence::mobi_config_manager::install {
    package { "mobi-config-manager-${mobi_core_convergence::mobi_config_manager::mobi_config_manager_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
    package { "mobi-publisher-endpoint-${mobi_core_convergence::mobi_config_manager::mobi_publisher_endpoint_version}":
      ensure => present,
      notify => [ Class["apache::service"], ]
    }
}
