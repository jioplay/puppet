class mobi_core_convergence::mobi_program_scheduler_database::install {

    file { "/var/mobi-program-scheduler-database":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi-program-scheduler-database/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_core_convergence/mobi_program_scheduler_database",
        require => File["/var/mobi-program-scheduler-database"],
    }

    file { "/var/mobi-program-scheduler-database/database/install.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_core_convergence/mobi_program_scheduler_database/install.sh.erb'),
        require => File["/var/mobi-program-scheduler-database"],
    }

    file { "/var/mobi-program-scheduler-database/database/uninstall.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_core_convergence/mobi_program_scheduler_database/uninstall.sh.erb'),
        require => File["/var/mobi-program-scheduler-database"],
    }

    file { "/var/mobi-program-scheduler-database/database/create_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_core_convergence/mobi_program_scheduler_database/create_databases.sql.erb'),
        require => File["/var/mobi-program-scheduler-database"],
    }

    file { "/var/mobi-program-scheduler-database/database/delete_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_core_convergence/mobi_program_scheduler_database/delete_databases.sql.erb'),
        require => File["/var/mobi-program-scheduler-database"],
    }

    #test to see if we have been run
    exec { "Install DB":
        command => "/bin/sh /var/mobi-program-scheduler-database/database/install.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi-program-scheduler-database/database/installation.done",
        logoutput => "true",
        require => File["/var/mobi-program-scheduler-database/database"],
    }

}
