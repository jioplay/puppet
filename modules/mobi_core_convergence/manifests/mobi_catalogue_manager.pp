class mobi_core_convergence::mobi_catalogue_manager(
   $mobi_catalogue_manager_version = $mobi_core_convergence::mobi_catalogue_manager::params::mobi_catalogue_manager_version,
)
inherits mobi_core_convergence::mobi_catalogue_manager::params{
  include mobi_core_convergence::mobi_catalogue_manager::install
  anchor { "mobi_core_convergence::mobi_catalogue_manager::begin":} -> Class["mobi_core_convergence::mobi_catalogue_manager::install"] ->
  anchor { "mobi_core_convergence::mobi_catalogue_manager::end": }
  os::motd::register { $name : }
}
