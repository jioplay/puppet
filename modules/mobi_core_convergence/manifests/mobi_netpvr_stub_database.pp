# == Class: mobi_core_convergence::mobi_netpvr_stub_database
#
# === Parameters:
#
#   $database_root_password::
#   $netpvr_username::
#   $netpvr_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_core_convergence::mobi_netpvr_stub_database" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_core_convergence::mobi_netpvr_stub_database (
    $database_root_password = $mobi_core_convergence::mobi_netpvr_stub_database::params::database_root_password,
    $netpvr_username = $mobi_core_convergence::mobi_netpvr_stub_database::params::netpvr_username,
    $netpvr_password = $mobi_core_convergence::mobi_netpvr_stub_database::params::netpvr_password,
)
  inherits mobi_core_convergence::mobi_netpvr_stub_database::params {
    include mobi_core_convergence::mobi_netpvr_stub_database::install
    anchor { "mobi_core_convergence::mobi_netpvr_stub_database::begin":} -> Class["mobi_core_convergence::mobi_netpvr_stub_database::install"]
    Class["mobi_core_convergence::mobi_netpvr_stub_database::install"] -> anchor { "mobi_core_convergence::mobi_netpvr_stub_database::end": }
    os::motd::register { $name : }
}
