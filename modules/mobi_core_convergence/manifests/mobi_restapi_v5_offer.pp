class mobi_core_convergence::mobi_restapi_v5_offer(
   $mobi_restapi_v5_offer_version = $mobi_core_convergence::mobi_restapi_v5_offer::params::mobi_restapi_v5_offer_version,
)
inherits mobi_core_convergence::mobi_restapi_v5_offer::params{
  include mobi_core_convergence::mobi_restapi_v5_offer::install
  anchor { "mobi_core_convergence::mobi_restapi_v5_offer::begin":} -> Class["mobi_core_convergence::mobi_restapi_v5_offer::install"] ->
  anchor { "mobi_core_convergence::mobi_restapi_v5_offer::end": }
  os::motd::register { $name : }
}
