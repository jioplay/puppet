class mobi_core_convergence::mobi_program_scheduler(
   $mobi_program_scheduler_version = $mobi_core_convergence::mobi_program_scheduler::params::mobi_program_scheduler_version,
)
inherits mobi_core_convergence::mobi_program_scheduler::params{
  include mobi_core_convergence::mobi_program_scheduler::install
  anchor { "mobi_core_convergence::mobi_program_scheduler::begin":} -> Class["mobi_core_convergence::mobi_program_scheduler::install"] ->
  anchor { "mobi_core_convergence::mobi_program_scheduler::end": }
  os::motd::register { $name : }
}
