class  mobi_core_convergence::mobi_location_services::install {
    package { "${mobi_core_convergence::mobi_location_services::mobi_location_services_package}":
      ensure => "${mobi_core_convergence::mobi_location_services::mobi_location_services_version}",
      notify => [ Class["tomcat::service"], ]
    }
}
