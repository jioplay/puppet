class mobi_core_convergence::mobi_netpvr_stub::install {
    package { "mobi-netpvr-stub-${mobi_core_convergence::mobi_netpvr_stub::mobi_netpvr_stub_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
