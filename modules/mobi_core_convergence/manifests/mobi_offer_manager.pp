class mobi_core_convergence::mobi_offer_manager(
  $mobi_oms_web_version = $mobi_core_convergence::mobi_offer_manager::params::mobi_oms_web_version,
)
inherits mobi_core_convergence::mobi_offer_manager::params{
  include mobi_core_convergence::mobi_offer_manager::install
  anchor { "mobi_core_convergence::mobi_offer_manager::begin":} -> Class["mobi_core_convergence::mobi_offer_manager::install"] ->
  anchor { "mobi_core_convergence::mobi_offer_manager::end": }
  os::motd::register { $name : }
}