# == Class: mobi_core_convergence::msfrn
#
#  installs msfrn component
#
# === Parameters:
#
#   $mobi_restapi_v5_guide::
#
# === Requires:
#
#     java
#     tomcat
#     httpd
#     group::arlanda
#     user::apache
#     user::rtv
#     mobi_core_convergence::filesystem or nf mounts setup
#
# === Sample Usage
#
#  class { "mobi_core_convergence::msfrn" :
#           ingestor_version => "5.0.0-179808",
#           searchagent_version => "5.0.0-181163",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_core_convergence::msfrn (
   $mobi_restapi_v5_guide = $mobi_core_convergence::msfrn::params::mobi_restapi_v5_guide,
)
inherits mobi_core_convergence::msfrn::params {
    include mobi_core_convergence::msfrn::install
    anchor { "mobi_core_convergence::msfrn::begin":} -> Class["mobi_core_convergence::msfrn::install"] ->
    anchor { "mobi_core_convergence::msfrn::end": }
    os::motd::register { $name : }
}
