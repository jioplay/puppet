class mobi_core_convergence::mobi_blackout_services(
  $mobi_blackout_services_version = $mobi_core_convergence::mobi_blackout_services::params::mobi_blackout_services_version,
)
inherits mobi_core_convergence::mobi_blackout_services::params{
  include mobi_core_convergence::mobi_blackout_services::install
  anchor { "mobi_core_convergence::mobi_blackout_services::begin":} -> Class["mobi_core_convergence::mobi_blackout_services::install"] ->
  anchor { "mobi_core_convergence::mobi_blackout_services::end": }
  os::motd::register { $name : }
}