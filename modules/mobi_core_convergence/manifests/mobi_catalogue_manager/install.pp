class mobi_core_convergence::mobi_catalogue_manager::install {
    package { "mobi-catalogue-manager-${mobi_core_convergence::mobi_catalogue_manager::mobi_catalogue_manager_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
