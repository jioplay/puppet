# == Class: mobi_core_convergence::mobi_program_scheduler_database
#
#  installs mgo database
#
# === Parameters:
#
#   $database_root_password::
#   $program_username::
#   $program_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_core_convergence::mobi_program_scheduler_database" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_core_convergence::mobi_program_scheduler_database (
    $database_root_password = $mobi_core_convergence::mobi_program_scheduler_database::params::database_root_password,
    $program_username = $mobi_core_convergence::mobi_program_scheduler_database::params::program_username,
    $program_password = $mobi_core_convergence::mobi_program_scheduler_database::params::program_password,
)
  inherits mobi_core_convergence::mobi_program_scheduler_database::params {
    include mobi_core_convergence::mobi_program_scheduler_database::install
    anchor { "mobi_core_convergence::mobi_program_scheduler_database::begin":} -> Class["mobi_core_convergence::mobi_program_scheduler_database::install"]
    Class["mobi_core_convergence::mobi_program_scheduler_database::install"] -> anchor { "mobi_core_convergence::mobi_program_scheduler_database::end": }
    os::motd::register { $name : }
}
