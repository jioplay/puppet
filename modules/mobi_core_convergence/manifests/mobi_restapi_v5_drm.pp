class mobi_core_convergence::mobi_restapi_v5_drm(
   $version = $mobi_core_convergence::mobi_restapi_v5_drm::params::version,
   $drmvip_hostname =  $mobi_core_convergence::mobi_restapi_v5_drm::params::drmvip_hostname,

)
inherits mobi_core_convergence::mobi_restapi_v5_drm::params{
  #include mobi_core_convergence::mobi_restapi_v5_drm::install,mobi_core_convergence::mobi_restapi_v5_drm::config
  anchor { "mobi_core_convergence::mobi_restapi_v5_drm::begin":} ->
    Class["mobi_core_convergence::mobi_restapi_v5_drm::install"] ->
    Class["mobi_core_convergence::mobi_restapi_v5_drm::config"] ->
  anchor { "mobi_core_convergence::mobi_restapi_v5_drm::end": }
  os::motd::register { $name : }
}
