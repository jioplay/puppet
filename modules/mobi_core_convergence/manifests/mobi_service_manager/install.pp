class mobi_core_convergence::mobi_service_manager::install {
    package { "mobi-service-manager-${mobi_core_convergence::mobi_service_manager::mobi_service_manager_version}":
      ensure => present,
      notify => [ Class["jetspeed::service"], ]
    }
}
