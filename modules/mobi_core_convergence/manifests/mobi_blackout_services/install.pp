class  mobi_core_convergence::mobi_blackout_services::install {
    package { "mobi-blackout-services":
      ensure => "${mobi_core_convergence::mobi_blackout_services::mobi_blackout_services_version}",
      notify => [ Class["tomcat::service"], ]
    }
}
