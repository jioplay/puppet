class mobi_core_convergence::mobi_personalization_manager(
  $mobi_personalization_manager_package_and_version = $mobi_core_convergence::mobi_personalization_manager::params::mobi_personalization_manager_package_and_version,
  $mobi_personalization_manager_config_package_and_version = $mobi_core_convergence::mobi_personalization_manager::params::mobi_personalization_manager_config_package_and_version,
  $carrier,
)
inherits mobi_core_convergence::mobi_personalization_manager::params{
  include mobi_core_convergence::mobi_personalization_manager::install, mobi_core_convergence::mobi_personalization_manager::config
  if $carrier == undef { fail("Please specify carrier")}
  anchor { "mobi_core_convergence::mobi_personalization_manager::begin":} ->
  Class["mobi_core_convergence::mobi_personalization_manager::install"] ->
  Class["mobi_core_convergence::mobi_personalization_manager::config"] ->
  anchor { "mobi_core_convergence::mobi_personalization_manager::end": }
  os::motd::register { $name : }
}
