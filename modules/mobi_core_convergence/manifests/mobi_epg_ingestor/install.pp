class mobi_core_convergence::mobi_epg_ingestor::install {
  package { "mobi-epg-ingestor":
    ensure => "${mobi_core_convergence::mobi_epg_ingestor::mobi_epg_ingestor_version}",
    notify => [Class["tomcat::service"],]
  }
}
