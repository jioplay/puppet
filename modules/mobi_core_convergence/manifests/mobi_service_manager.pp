class mobi_core_convergence::mobi_service_manager(
   $mobi_service_manager_version = $mobi_core_convergence::mobi_service_manager::params::mobi_service_manager_version,
)
inherits mobi_core_convergence::mobi_service_manager::params{
  include mobi_core_convergence::mobi_service_manager::install
  anchor { "mobi_core_convergence::mobi_service_manager::begin":} -> Class["mobi_core_convergence::mobi_service_manager::install"] ->
  anchor { "mobi_core_convergence::mobi_service_manager::end": }
  os::motd::register { $name : }
}
