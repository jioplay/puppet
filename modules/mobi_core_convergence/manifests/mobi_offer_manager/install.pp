class  mobi_core_convergence::mobi_offer_manager::install {
    package { "mobi-offer-manager-${mobi_core_convergence::mobi_offer_manager::mobi_oms_web_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}



