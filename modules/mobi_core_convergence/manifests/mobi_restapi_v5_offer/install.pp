class mobi_core_convergence::mobi_restapi_v5_offer::install {
    package { "mobi_restapi_v5_offer-${mobi_core_convergence::mobi_restapi_v5_offer::mobi_restapi_v5_offer_version}":
      ensure => present,
      notify => [ Class["apache::service"], ]
    }
}
