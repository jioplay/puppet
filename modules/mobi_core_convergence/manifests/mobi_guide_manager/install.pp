class mobi_core_convergence::mobi_guide_manager::install {
    package { "mobi-guide-manager-${mobi_core_convergence::mobi_guide_manager::mobi_guide_manager_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
