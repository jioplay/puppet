class mobi_core_convergence::mobi_guide_manager::config {

  file { "/opt/mobi-guide-manager/override.d":
    ensure => "directory",
    owner  => "rtv",
    group  => "rtv",
    mode   => "0755",
  }

  file { "/opt/mobi-guide-manager/override.d/guidemanager.properties":
    mode => "0644",
    owner  => "rtv",
    group  => "rtv",
    source => "puppet:///modules/mobi_core_convergence/mobi_guide_manager/override.d/guidemanager.properties",
  }
}
