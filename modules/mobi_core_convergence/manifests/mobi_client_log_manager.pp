class mobi_core_convergence::mobi_client_log_manager(
   $mobi_client_log_manager_version = $mobi_core_convergence::mobi_config_manager::params::mobi_client_log_manager_version,
)
inherits mobi_core_convergence::mobi_client_log_manager::params{
  include mobi_core_convergence::mobi_client_log_manager::install
  anchor { "mobi_core_convergence::mobi_client_log_manager::begin":} -> Class["mobi_core_convergence::mobi_client_log_manager::install"] ->
  anchor { "mobi_core_convergence::mobi_client_log_manager::end": }
  os::motd::register { $name : }
}
