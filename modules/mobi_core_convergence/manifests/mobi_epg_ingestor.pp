class mobi_core_convergence::mobi_epg_ingestor(
$mobi_epg_ingestor_version = $mobi_core_convergence::mobi_epg_ingestor::params::mobi_epg_ingestor_version,
   $mobi_epg_ingestor_enabled=$mobi_core_convergence::mobi_epg_ingestor::params::mobi_epg_ingestor_enabled,
$mobi_channel_configuration=$mobi_core_convergence::mobi_epg_ingestor::params::mobi_channel_configuration,
)
inherits mobi_core_convergence::mobi_epg_ingestor::params{
  include mobi_core_convergence::mobi_epg_ingestor::install
  include mobi_core_convergence::mobi_epg_ingestor::config
  anchor { "mobi_core_convergence::mobi_epg_ingestor::begin":} -> Class["mobi_core_convergence::mobi_epg_ingestor::install"] ->
  Class["mobi_core_convergence::mobi_epg_ingestor::config"] ->
  anchor { "mobi_core_convergence::mobi_epg_ingestor::end": }
  os::motd::register { $name : }
}
