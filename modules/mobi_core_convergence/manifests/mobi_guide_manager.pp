class mobi_core_convergence::mobi_guide_manager(
   $mobi_guide_manager_version = $mobi_core_convergence::mobi_guide_manager::params::mobi_guide_manager_version,
)
inherits mobi_core_convergence::mobi_guide_manager::params{
  include mobi_core_convergence::mobi_guide_manager::install
  include  mobi_core_convergence::mobi_guide_manager::config
  anchor { "mobi_core_convergence::mobi_guide_manager::begin":} -> Class["mobi_core_convergence::mobi_guide_manager::install"] -> Class["mobi_core_convergence::mobi_guide_manager::config"] -> anchor { "mobi_core_convergence::mobi_guide_manager::end": }
  os::motd::register { $name : }

}
