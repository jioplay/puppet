class mobi_core_convergence::mobi_restapi_v5_guide(
   $mobi_restapi_v5_guide_version = $mobi_core_convergence::mobi_restapi_v5_guide::params::mobi_restapi_v5_guide_version,
)
inherits mobi_core_convergence::mobi_restapi_v5_guide::params{
  include mobi_core_convergence::mobi_restapi_v5_guide::install
  anchor { "mobi_core_convergence::mobi_restapi_v5_guide::begin":} -> Class["mobi_core_convergence::mobi_restapi_v5_guide::install"] ->
  anchor { "mobi_core_convergence::mobi_restapi_v5_guide::end": }
  os::motd::register { $name : }
}
