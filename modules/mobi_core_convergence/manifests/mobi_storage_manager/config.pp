class mobi_core_convergence::mobi_storage_manager::config {

  file { "/opt/mobi-storage-manager/spring.d":
    ensure => directory,
    source => "puppet:///modules/mobi_core_convergence/mobi_storage_manager/${carrier}/spring",
    recurse => true,
    owner  => "rtv",
    group  => "rtv",
  }

}
