class mobi_core_convergence::mobi_storage_manager::install {
    package { "mobi-storage-manager-${mobi_core_convergence::mobi_storage_manager::mobi_storage_manager_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
