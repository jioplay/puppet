class mobi_core_convergence::mobi_personalization_manager::config {

  file { "/var/log/mobi_personalization_manager":
    ensure => "directory",
    owner  => "rtv",
    group  => "rtv",
    mode   => "0755",
  }

  file { "/var/log/mobi-aaa/mobi_personalization_manager":
    ensure => "directory",
    owner  => "rtv",
    group  => "rtv",
    mode   => "0755",
  }

  file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes":
    source => "puppet:///modules/mobi_core_convergence/mobi_personalization_manager/${carrier}/config",
    recurse => true,
    owner  => "rtv",
    group  => "rtv",
  }

  file { "/opt/mobi-personalization-manager/webapp/WEB-INF/classes/spring/context":
    source => "puppet:///modules/mobi_core_convergence/mobi_personalization_manager/${carrier}/spring/context",
    recurse => true,
    owner  => "rtv",
    group  => "rtv",
  }
}
