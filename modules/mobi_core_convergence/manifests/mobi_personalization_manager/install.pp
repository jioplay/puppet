class mobi_core_convergence::mobi_personalization_manager::install {
  package { "${mobi_core_convergence::mobi_personalization_manager::mobi_personalization_manager_package_and_version}":
    ensure => present,
    notify => Class["tomcat::service"],
  }

  package { "${mobi_core_convergence::mobi_personalization_manager::mobi_personalization_manager_config_package_and_version}":
    ensure => present,
    notify => Class["tomcat::service"], 
  }
}
