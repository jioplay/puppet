class  mobi_core_convergence::mobi_oms_web::install {
    package { "mobi-oms-web-${mobi_core_convergence::mobi_oms_web::mobi_oms_web_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}