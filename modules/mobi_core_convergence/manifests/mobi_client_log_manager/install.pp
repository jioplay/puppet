class mobi_core_convergence::mobi_client_log_manager::install {
    package { "mobi-client-log-manager":
      ensure => "${mobi_core_convergence::mobi_client_log_manager::mobi_client_log_manager_version}",
      notify => [ Class["tomcat::service"], ]
    }
}
