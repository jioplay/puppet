class mobi_core_convergence::mobi_storage_manager(
   $mobi_storage_manager_version = $mobi_core_convergence::mobi_storage_manager::params::mobi_storage_manager_version,
   $carrier,
)
inherits mobi_core_convergence::mobi_storage_manager::params{
  include mobi_core_convergence::mobi_storage_manager::install, mobi_core_convergence::mobi_storage_manager::config
  if $carrier == undef { fail("Please specify carrier")}
  anchor { "mobi_core_convergence::mobi_storage_manager::begin":} -> Class["mobi_core_convergence::mobi_storage_manager::install"] ->
  Class["mobi_core_convergence::mobi_storage_manager::config"] ->
  anchor { "mobi_core_convergence::mobi_storage_manager::end": }
  os::motd::register { $name : }
}
