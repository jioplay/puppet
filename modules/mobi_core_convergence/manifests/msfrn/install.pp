class mobi_core_convergence::msfrn::install {
    package { "mobi-restapi-v5-guide-${mobi_core_convergence::msfrn::mobi_restapi_v5_guide}":
      ensure => present,
      notify => Class["apache::service"],
    }
}
