class mobi_core_convergence::mobi_recording_manager::install {
    package { "mobi-recording-manager-${mobi_core_convergence::mobi_recording_manager::mobi_recording_manager_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
