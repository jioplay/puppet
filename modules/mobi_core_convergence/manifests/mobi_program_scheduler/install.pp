class mobi_core_convergence::mobi_program_scheduler::install {
    package { "mobi-program-scheduler-${mobi_core_convergence::mobi_program_scheduler::mobi_program_scheduler_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
