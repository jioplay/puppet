class mobi_core_convergence::mobi_oms_web(
  $mobi_oms_web_version = $mobi_core_convergence::mobi_oms_web::params::mobi_oms_web_version,
)
inherits mobi_core_convergence::mobi_oms_web::params{
  include mobi_core_convergence::mobi_oms_web::install
  anchor { "mobi_core_convergence::mobi_oms_web::begin":} -> Class["mobi_core_convergence::mobi_oms_web::install"] ->
  anchor { "mobi_core_convergence::mobi_oms_web::end": }
  os::motd::register { $name : }
}