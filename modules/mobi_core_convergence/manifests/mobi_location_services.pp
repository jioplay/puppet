class mobi_core_convergence::mobi_location_services (
  $mobi_location_services_version = $mobi_core_convergence::mobi_location_services::params::mobi_location_services_version,
  $mobi_location_services_package = $mobi_core_convergence::mobi_location_services::params::mobi_location_services_package,
  )
inherits mobi_core_convergence::mobi_location_services::params {
  include mobi_core_convergence::mobi_location_services::install
  anchor { "mobi_core_convergence::mobi_location_services::begin": } -> Class["mobi_core_convergence::mobi_location_services::install"
    ] -> anchor { "mobi_core_convergence::mobi_location_services::end": }

  os::motd::register { $name: }
}
