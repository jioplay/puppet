class mobi_core_convergence::mobi_restapi_v5_guide::install {
    package { "mobi-restapi-v5-guide-${mobi_core_convergence::mobi_restapi_v5_guide::mobi_restapi_v5_guide_version}":
      ensure => present,
      notify => [ Class["tomcat::service"], ]
    }
}
