class mobi_core_convergence::mobi_config_manager(
   $mobi_config_manager_version = $mobi_core_convergence::mobi_config_manager::params::mobi_config_manager_version,
   $mobi_publisher_endpoint_version = $mobi_core_convergence::mobi_config_manager::params::mobi_publisher_endpoint_version,
)
inherits mobi_core_convergence::mobi_config_manager::params{
  include mobi_core_convergence::mobi_config_manager::install
  anchor { "mobi_core_convergence::mobi_config_manager::begin":} -> Class["mobi_core_convergence::mobi_config_manager::install"] ->
  anchor { "mobi_core_convergence::mobi_config_manager::end": }
  os::motd::register { $name : }
}
