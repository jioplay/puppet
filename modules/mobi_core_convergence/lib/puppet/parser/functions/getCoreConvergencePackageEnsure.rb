module Puppet::Parser::Functions
  newfunction(:getCoreConvergencePackageEnsure, :type => :rvalue) do |args|
  
    $certName = lookupvar('::clientcert')
    if $certName =~ /^.*\.ci\-cp\.dev\.oak1\.mobitv$/ then
      $result="latest"
    else
      $result="present"
    end
    debug("core convergence package ensure for host" + $certName + " is " + $result)
    return $result
  end
end