<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="/">
		
		<!-- TODO: Auto-generated template -->
		<VantagePlayList>
			  <xsl:if test="playlist/name" > 
				<xsl:element name="Name">
		          <xsl:value-of select="playlist/name" />
		        </xsl:element>
			  </xsl:if>
		
		<xsl:apply-templates select="playlist/clips/clip" mode="file"></xsl:apply-templates> 
		
		<EDL>
			  <Timecode> 
					<xsl:attribute name="name">Start Timecode Override</xsl:attribute>
			        <xsl:attribute name="type">time</xsl:attribute>00:00:00:00</Timecode>	
			 <!--  <xsl:apply-templates select="playlist/clips/clip" mode="edit"></xsl:apply-templates>   -->
			 		<xsl:for-each select="playlist/clips/clip">
			 		<!-- 	<Edit>
							  <xsl:attribute name="type">fill</xsl:attribute>
					          <xsl:attribute name="sequence"><xsl:value-of select="position()*2-2" /></xsl:attribute>
					          <xsl:attribute name="timecode_in"></xsl:attribute>
					          <xsl:attribute name="timecode_out"></xsl:attribute>
					          <xsl:attribute name="markin">0</xsl:attribute>
					          <xsl:attribute name="markout">-1</xsl:attribute>
					    </Edit>
			 		 -->
			 			<Edit>
							  <xsl:attribute name="type">file</xsl:attribute>
					          <xsl:attribute name="sequence"><xsl:value-of select="position()-1" /></xsl:attribute>
					          <xsl:attribute name="timecode_in"></xsl:attribute>
					          <xsl:attribute name="timecode_out"></xsl:attribute>
					          <xsl:attribute name="markin">0</xsl:attribute>
					          <xsl:attribute name="markout">-1</xsl:attribute>
					          <xsl:attribute name="file"><xsl:value-of select="@uuid" /></xsl:attribute>
          				</Edit>
			 		</xsl:for-each>
		</EDL>
		</VantagePlayList>
	</xsl:template>
	
	<xsl:template name="getFileName">
		<xsl:param name="filename"/>
		
		  <xsl:choose>
		    <xsl:when test="contains($filename, '\')">
		    <xsl:call-template name="getFileName">
		      <xsl:with-param name="filename" select="substring-after($filename, '\')"/>
		    </xsl:call-template>
		    </xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="$filename"/>
		    </xsl:otherwise>
		  </xsl:choose>
</xsl:template>

	
	
	
	<xsl:template match="playlist/clips/clip" mode="file">
		<xsl:variable name='clipPath' ><xsl:value-of select="." /></xsl:variable>
		<xsl:variable name='delim' >\</xsl:variable>
		<xsl:variable name='fileName' ><xsl:call-template name="getFileName">
                        <xsl:with-param name="filename" select="."/>
                        </xsl:call-template>
        </xsl:variable>
		<xsl:variable name='initialPath' >
			<xsl:value-of select="substring-before($clipPath, $fileName)" />
		</xsl:variable>
		
		  <File>
			  <xsl:attribute name="uuid"><xsl:value-of select="@uuid" /></xsl:attribute>
	          <xsl:attribute name="path"><xsl:value-of select="concat($initialPath,'colorBarRemoved\' ,$fileName)" /></xsl:attribute>
	      </File>
   </xsl:template>
	
<!-- 	<xsl:template match="playlist/clips/clip" mode="edit">
	  	<Edit>
			  <xsl:attribute name="type">file</xsl:attribute>
	          <xsl:attribute name="sequence"><xsl:value-of select="@sequence" /></xsl:attribute>
	          <xsl:attribute name="timecode_in"></xsl:attribute>
	          <xsl:attribute name="timecode_out"></xsl:attribute>
	          <xsl:attribute name="markin">0</xsl:attribute>
	          <xsl:attribute name="markout">-1</xsl:attribute>
	          <xsl:attribute name="file"><xsl:value-of select="@uuid" /></xsl:attribute>
          </Edit>
   </xsl:template> -->
          
	
	
</xsl:stylesheet>