<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">

	<xsl:output method="xml" indent="yes" />

	<xsl:template match="/">
		<normalizedProviderMetadata>

			<xsl:if test="/root/record/show">
				<xsl:element name="show_id">
					<xsl:value-of select="normalize-space(/root/record/show)" />
				</xsl:element>
			</xsl:if>

			<xsl:template name="AudioLanguage">
				<xsl:if test="string-length(.) &gt; 0">
					<xsl:call-template name="tokenize">
						<xsl:with-param name="string" select="normalize-space(/root/record/Language)" />
						<xsl:with-param name="delim" select="','" />
						<xsl:with-param name="listName" select="'audioLangList'" />
					</xsl:call-template>
				</xsl:if>
			</xsl:template>

			<xsl:template name="category">
				<xsl:if test="string-length(.) &gt; 0">
					<xsl:call-template name="tokenize">
						<xsl:with-param name="string" select="normalize-space(/root/record/Content_Type)" />
						<xsl:with-param name="delim" select="','" />
						<xsl:with-param name="listName" select="'category'" />
					</xsl:call-template>
				</xsl:if>
			</xsl:template>


			<xsl:template name="Genre">
				<xsl:if test="string-length(.) &gt; 0">
					<xsl:call-template name="tokenize">
						<xsl:with-param name="string" select="normalize-space(/root/record/Sub_Genre)" />
						<xsl:with-param name="delim" select="','" />
						<xsl:with-param name="listName" select="'genre'" />
					</xsl:call-template>
				</xsl:if>
			</xsl:template>


		<pair>

				<metadata>
					<xsl:attribute name="type">STRING</xsl:attribute>
					<xsl:attribute name="key">Content_Type</xsl:attribute>
					<xsl:attribute name="value">
    		        	<xsl:value-of select="normalize-space(/root/record/Content_Type)" />
          			</xsl:attribute>
				</metadata>

				<metadata>
					<xsl:variable name="u" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
					<xsl:variable name="l" select="'abcdefghijklmnopqrstuvwxyz'" />
					<xsl:attribute name="type">STRING</xsl:attribute>
					<xsl:attribute name="key">sub_type</xsl:attribute>
					<xsl:attribute name="value">
            			<xsl:value-of select="translate(normalize-space(/root/record/Content_Type),$u,$l)" />
          			</xsl:attribute>
				</metadata>

				<metadata>
					<xsl:attribute name="type">STRING</xsl:attribute>
					<xsl:attribute name="key">show</xsl:attribute>
					<xsl:attribute name="value">
            			<xsl:value-of select="normalize-space(/root/record/show)" />
          			</xsl:attribute>
				</metadata>

				<metadata>
					<xsl:attribute name="type">STRING</xsl:attribute>
					<xsl:attribute name="key">Vendor</xsl:attribute>
					<xsl:attribute name="value">
            			<xsl:value-of select="normalize-space(/root/record/Vendor)" />
          			</xsl:attribute>
				</metadata>

				 <xsl:template name="Actors">
                                        <xsl:if test="string-length(.) &gt; 0">
                                                <xsl:call-template name="tokenizeMetadata">
                                                        <xsl:with-param name="string" select="normalize-space(/root/record/Actors)" />
                                                        <xsl:with-param name="delim" select="','" />
                                                        <xsl:with-param name="listName" select="'actors_list'" />
                                                </xsl:call-template>
                                        </xsl:if>
                                </xsl:template> 
				 <xsl:template name="Director">
                                        <xsl:if test="string-length(.) &gt; 0">
                                                <xsl:call-template name="tokenizeMetadata">
                                                        <xsl:with-param name="string" select="normalize-space(/root/record/Director)" />
                                                        <xsl:with-param name="delim" select="','" />
                                                        <xsl:with-param name="listName" select="'directors_list'" />
                                                </xsl:call-template>
                                        </xsl:if>
                                </xsl:template>
				 <xsl:choose>
					<xsl:when test="normalize-space(/root/record/Ratings) &gt; 0"> 
						<xsl:choose>
							 <xsl:when test="normalize-space(/root/record/Ratings) &lt; 7">
								<metadata>
		                                        	<xsl:attribute name="type">STRING</xsl:attribute>
	        		                                <xsl:attribute name="key">em_rating</xsl:attribute>
        			        	                <xsl:attribute name="value">
                        			        	<xsl:value-of select="normalize-space(/root/record/Ratings)" />
	                        				</xsl:attribute>
	        	                        		</metadata>
							</xsl:when>
						</xsl:choose>
					</xsl:when>
				</xsl:choose>

				<xsl:template name="Years">
					<xsl:if test="string-length(.) &gt; 0">
						<xsl:call-template name="tokenizeMetadata">
							<xsl:with-param name="string" select="normalize-space(/root/record/Release_Date)" />
							<xsl:with-param name="delim" select="','" />
							<xsl:with-param name="listName" select="'years'" />
						</xsl:call-template>
					</xsl:if>
				</xsl:template>

			</pair>
		</normalizedProviderMetadata>
	</xsl:template>

	<xsl:template name="tokenizeMetadata">
		<xsl:param name="string" />
		<xsl:param name="delim" />
		<xsl:param name="listName" />
		<xsl:variable name="u" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
		<xsl:variable name="l" select="'abcdefghijklmnopqrstuvwxyz'" />

		<xsl:choose>
			<xsl:when test="contains($string, $delim)">
				<metadata>
					<xsl:attribute name="type">STRING_MULTI</xsl:attribute>
					<xsl:attribute name="key"> 
				              	<xsl:value-of select="$listName" />
				          </xsl:attribute>

					<xsl:if test="$listName='genre_list'">
						<xsl:attribute name="value">
				          				<xsl:value-of
							select="normalize-space(substring-before(translate(normalize-space($string),$u,$l), $delim))" />
	          			  		 </xsl:attribute>
					</xsl:if>
					<xsl:if test="$listName='actors_list'">
                                                <xsl:attribute name="value">
                                                                        <xsl:value-of
                                                        select="normalize-space(substring-before(translate(normalize-space($string),$u,$l), $delim))" />
                                                         </xsl:attribute>
                                        </xsl:if>

					 <xsl:if test="$listName='directors_list'">
                                                <xsl:attribute name="value">
                                                                        <xsl:value-of
                                                        select="normalize-space(substring-before(translate(normalize-space($string),$u,$l), $delim))" />
                                                         </xsl:attribute>
                                        </xsl:if>
					<xsl:if test="$listName='years'">
						<xsl:attribute name="value">
				          				<xsl:value-of
							select="normalize-space(substring-before(translate(normalize-space(substring($string,7,10)),$u,$l), $delim))" />
	          			  		 </xsl:attribute>
					</xsl:if>

				</metadata>
				<xsl:call-template name="tokenizeMetadata">
					<xsl:with-param name="string"
						select="normalize-space(substring-after(normalize-space($string), $delim))" />
					<xsl:with-param name="delim" select="$delim" />
					<xsl:with-param name="listName" select="$listName" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<metadata>
					<xsl:attribute name="type">STRING_MULTI</xsl:attribute>
					<xsl:attribute name="key">
				         	 <xsl:value-of select="$listName" />
				          </xsl:attribute>
					<xsl:if test="$listName='genre_list'">
						<xsl:attribute name="value">
				             <xsl:value-of
							select="translate(normalize-space($string),$u,$l)" />
				          </xsl:attribute>
					</xsl:if>
					<xsl:if test="$listName='actors_list'">
                                                <xsl:attribute name="value">
                                             <xsl:value-of
                                                        select="translate(normalize-space($string),$u,$l)" />
                                          </xsl:attribute>
                                        </xsl:if>
					 <xsl:if test="$listName='directors_list'">
                                                <xsl:attribute name="value">
                                             <xsl:value-of
                                                        select="translate(normalize-space($string),$u,$l)" />
                                          </xsl:attribute>
                                        </xsl:if>
					<xsl:if test="$listName='years'">
						<xsl:attribute name="value">
				          				<xsl:value-of
							select="translate(normalize-space(substring($string,7,10)),$u,$l)" />
	          			  		 </xsl:attribute>
					</xsl:if>
				</metadata>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="tokenize">
		<xsl:param name="string" />
		<xsl:param name="delim" />
		<xsl:param name="listName" />
		<xsl:variable name="u" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
		<xsl:variable name="l" select="'abcdefghijklmnopqrstuvwxyz'" />

		<xsl:choose>
			<xsl:when test="contains($string, $delim)">
				<xsl:element name="{$listName}">
					<xsl:value-of
						select="normalize-space(substring-before(translate(normalize-space($string),$u,$l), $delim))" />
				</xsl:element>
				<xsl:call-template name="tokenize">
					<xsl:with-param name="string"
						select="normalize-space(substring-after(normalize-space($string), $delim))" />
					<xsl:with-param name="delim" select="$delim" />
					<xsl:with-param name="listName" select="$listName" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$string !=''">
					<xsl:element name="{$listName}">
						<xsl:value-of select="translate(normalize-space($string),$u,$l)" />
					</xsl:element>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>


