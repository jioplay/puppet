
--
-- Table structure for table `TV_INVENTORY`
--
 
DROP TABLE IF EXISTS `TV_INVENTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TV_INVENTORY` (
  `INVENTORY_ID` bigint(20) NOT NULL auto_increment,
  `BACKEND_CHANNEL` bigint(20) NOT NULL default '0' ,
  `DESCRIPTION` varchar(2047) default NULL,
  `DISPLAY_NAME` varchar(255) NOT NULL,
  `ICON_NAME` varchar(255) default NULL,
  `JUKEBOX_CHANNEL_ID` bigint(20) default NULL,
  `LBS_DEFAULT_RULE` varchar(255) NOT NULL,
  `LONG_NAME` varchar(255) default NULL,
  `MEDIACLASS_OVERRIDE` varchar(40) default NULL,
  `MEDIA_RESTRICTIONS` varchar(255) default NULL,
  `MEDIATYPE` varchar(255) NOT NULL,
  `MEDIA_ASPECT_RATIO` varchar(255) default '4x3',
  `NAME` varchar(255) NOT NULL,
  `STATION_ID` varchar(255) default NULL,
  PRIMARY KEY  (`INVENTORY_ID`),
  UNIQUE KEY `INVENTORY_ID` (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `FOLDER_INVENTORY`
--
 
DROP TABLE IF EXISTS `FOLDER_INVENTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FOLDER_INVENTORY` (
  `CHAPTERIZED_CONTENT` varchar(255) default NULL,
  `EXPIRES` datetime default NULL,
  `GENRE` varchar(255) default NULL,
  `KEYWORD` varchar(255) default NULL,
  `MCDPATH` varchar(255) NOT NULL,
  `NETWORK` varchar(255) default NULL,
  `PLAYLIST_ENABLED` varchar(255) default NULL,
  `PROVIDER_ID` bigint(20) NOT NULL,
  `SHOW` varchar(255) default NULL,
  `START_OF_AVAILABILITY` datetime default NULL,
  `THUMBNAIL_LOCATION` varchar(255) default NULL,
  `TOTAL_NUMBER_OF_PARTS` int(11) default NULL,
  `UI_NAME` varchar(255) NOT NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`INVENTORY_ID`),
  KEY `FK6A4A02AB7F5F6C9A` (`INVENTORY_ID`),
  CONSTRAINT `FK6A4A02AB7F5F6C9A` FOREIGN KEY (`INVENTORY_ID`) REFERENCES `TV_INVENTORY` (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 
--
-- Table structure for table `PROVIDERS`
--
 
DROP TABLE IF EXISTS `PROVIDERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROVIDERS` (
  `PROVIDER_ID` bigint(20) NOT NULL auto_increment,
  `DFLT_CONTENT_SHELF_LIFE` float default NULL,
  `PROVIDER` varchar(255) default NULL,
  `SECURED_OVER_WIFI` varchar(255) default NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `TIME_ZONE` varchar(255) default NULL,
  `MANAGE_CHAPTERIZED_CONTENT` varchar(255) default NULL,
  PRIMARY KEY  (`PROVIDER_ID`),
  UNIQUE KEY `PROVIDER_ID` (`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 
--
-- Table structure for table `PROVIDER_METADATA_CONFIG`
--
 
DROP TABLE IF EXISTS `PROVIDER_METADATA_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROVIDER_METADATA_CONFIG` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `CHECKSUM` varchar(255) default NULL,
  `CRID` varchar(255) default NULL,
  `ENABLE_LOCAL_PLAYBACK` varchar(255) default NULL,
  `ENABLE_WIFI` varchar(255) default NULL,
  `EXPIRES` varchar(255) default NULL,
  `FOLDER` varchar(255) default NULL,
  `GENRE` varchar(255) default NULL,
  `INDIRECT_UPLOAD_URL` varchar(255) default NULL,
  `INDUSTRY` varchar(255) default NULL,
  `KEYWORD` varchar(255) default NULL,
  `SHORT_TITLE` varchar(255) default NULL,
  `SLOT` varchar(255) default NULL,
  `START_OF_AVAILABILITY` varchar(255) default NULL,
  `SYNOPSIS` varchar(255) default NULL,
  `THUMBNAIL_UPLOAD_URL` varchar(255) default NULL,
  `TICKER` varchar(255) default NULL,
  `TITLE` varchar(255) default NULL,
  PRIMARY KEY  (`PROVIDER_ID`),
  UNIQUE KEY `PROVIDER_ID` (`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 
--
-- Table structure for table `TV_FOLDER_ITEM`
--
 
DROP TABLE IF EXISTS `TV_FOLDER_ITEM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TV_FOLDER_ITEM` (
  `ID` bigint(20) NOT NULL auto_increment,
  `CREATED_TIME` datetime NOT NULL,
  `DISPLAY_CODE` varchar(255) NOT NULL,
  `DISPLAY_ORDER` bigint(20) NOT NULL,
  `ITEM_INVENTORY_ID` bigint(20) NOT NULL,
  `LAST_UPDATED_TIME` datetime NOT NULL,
  `LAST_UPDATED_BY` varchar(255) NOT NULL,
  `FOLDER_INVENTORY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `FOLDER_INVENTORY_ID` (`FOLDER_INVENTORY_ID`,`ITEM_INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 