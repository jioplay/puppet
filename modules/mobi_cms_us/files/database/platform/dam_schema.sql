-- MySQL dump 10.11
--
-- Host: localhost    Database: dam
-- ------------------------------------------------------
-- Server version	5.0.95

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DAM_ADDITIONAL_METADATA`
--

DROP TABLE IF EXISTS `DAM_ADDITIONAL_METADATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DAM_ADDITIONAL_METADATA` (
  `METADATA_ID` bigint(20) NOT NULL auto_increment,
  `METAKEY` varchar(255) NOT NULL,
  `METAVALUE` varchar(1024) default NULL,
  `ASSET_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`METADATA_ID`),
  KEY `FKECFF0F8D829B9EC` (`ASSET_ID`),
  CONSTRAINT `FKECFF0F8D829B9EC` FOREIGN KEY (`ASSET_ID`) REFERENCES `DAM_ASSET` (`ASSET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DAM_ASSET`
--

DROP TABLE IF EXISTS `DAM_ASSET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DAM_ASSET` (
  `ASSET_ID` bigint(20) NOT NULL auto_increment,
  `TYPE` varchar(255) NOT NULL,
  `AVAILABLE` bit(1) default NULL,
  `CORRELATION_ID` varchar(1024) default NULL,
  `CRID` varchar(1024) NOT NULL,
  `DOWNLOAD_ATTEMPTS` int(11) default NULL,
  `ENCRYPTION_REQUIRED` varchar(255) NOT NULL,
  `EXPIRED` bit(1) default NULL,
  `EXPIRES` datetime default NULL,
  `GENRE` varchar(1024) default NULL,
  `GROUP_ID` int(11) default NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  `KEYWORD` varchar(1024) default NULL,
  `LAST_MODIFIED` datetime default NULL,
  `MEDIA_RESTRICTIONS` varchar(40) default NULL,
  `POST_ROLL_INVENTORY_ID` bigint(20) default NULL,
  `PRE_ROLL_INVENTORY_ID` bigint(20) default NULL,
  `PROVIDER_ID` int(11) NOT NULL,
  `PUBLISHED` bit(1) default NULL,
  `SHORT_TITLE` varchar(512) default NULL,
  `START_OF_AVAILABILITY` datetime default NULL,
  `STATE` int(11) default NULL,
  `SYNOPSIS` varchar(1024) default NULL,
  `TITLE` varchar(1024) default NULL,
  `TRASHED` bit(1) default NULL,
  `UPLOAD_DATE` datetime default NULL,
  `VERSION` int(11) NOT NULL,
  `EPISODE_NUMBER` bigint(20) default NULL,
  `SERIES_NUMBER` bigint(20) default NULL,
  PRIMARY KEY  (`ASSET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DAM_ASSET_INSTANCE`
--

DROP TABLE IF EXISTS `DAM_ASSET_INSTANCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DAM_ASSET_INSTANCE` (
  `INSTANCE_ID` bigint(20) NOT NULL auto_increment,
  `BITRATE` double default NULL,
  `CHECKSUM` varchar(255) default NULL,
  `DURATION` double default NULL,
  `ENCODING_ID` int(11) NOT NULL,
  `FORMAT` varchar(24) default NULL,
  `INSTANCE_LINKTYPE` varchar(255) default NULL,
  `IS_MBR` bit(1) default NULL,
  `IS_ORIGINAL` bit(1) default NULL,
  `PATH` varchar(1024) default NULL,
  `SIZE_` double default NULL,
  `TYPE` varchar(255) default NULL,
  `ASSET_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`INSTANCE_ID`),
  KEY `FKAE7085D3D829B9EC` (`ASSET_ID`),
  CONSTRAINT `FKAE7085D3D829B9EC` FOREIGN KEY (`ASSET_ID`) REFERENCES `DAM_ASSET` (`ASSET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DAM_CONFIG`
--

DROP TABLE IF EXISTS `DAM_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DAM_CONFIG` (
  `CONFIG_KEY` varchar(255) NOT NULL,
  `CONFIG_VALUE` varchar(1024) default NULL,
  `DEFAULT_CONFIG_VALUE` varchar(1024) default NULL,
  `DESCRIPTION` varchar(2048) default NULL,
  `LAST_MODIFIED` datetime default NULL,
  PRIMARY KEY  (`CONFIG_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DAM_TRACK`
--

DROP TABLE IF EXISTS `DAM_TRACK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DAM_TRACK` (
  `TRACK_ID` bigint(20) NOT NULL auto_increment,
  `BITRATE` double default NULL,
  `BITSPERSAMPLE` int(11) default NULL,
  `CHANNELS` int(11) default NULL,
  `CODEC` varchar(255) default NULL,
  `DURATION` double default NULL,
  `FMTP` varchar(255) default NULL,
  `FRAMERATE` double default NULL,
  `HEIGHT` int(11) default NULL,
  `PAYLOAD` varchar(255) default NULL,
  `SAMPLERATE` double default NULL,
  `SIZE_` bigint(20) default NULL,
  `START_` double default NULL,
  `TYPE` varchar(255) default NULL,
  `WIDTH` int(11) default NULL,
  `INSTANCE_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`TRACK_ID`),
  KEY `FK527C0F9CDE92065C` (`INSTANCE_ID`),
  CONSTRAINT `FK527C0F9CDE92065C` FOREIGN KEY (`INSTANCE_ID`) REFERENCES `DAM_ASSET_INSTANCE` (`INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-05-15  0:23:42
