Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (81,'AppleSegmentedVod','Apple Segmented Vod encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (141,'FragmentedMp4','Fragmented MP4 encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (202,'Core 16x9','Core WQVGA encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (201,'Core','Core QVGA encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (221,'WVGA ProgressiveDownload','WVGA PGDL 4x3 encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (222,'WVGA ProgressiveDownload 16x9','WVGA PGDL 16x9 encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (121,'RTSP 16x9','RTSP 500 and 800 Kbps WQVGA',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (122,'ProgressiveDownload 16x9','Progressive Download WQVGA encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (181,'DRM 16x9','DRM Encoding for 16x9',1);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (26,'Audio','Audio-only encoding set.',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (41,'ProgressiveDownload','Progressive Download encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (61,'DRM','DRM Encoding',1);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (101,'RSTP High','RTSP 500 and 800 Kbps QVGA',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (102,'ProgressiveDownload VGA 800','ProgressiveDownload VGA 800',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (21,'Master','Superchunk master format.',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (22,'Chunk','Legacy chunk encoding set.',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (23,'RTSP','Standard RTSP encoding set.',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (24,'WinMo','Windows Media / Mobile encoding set.',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (25,'PCTV','Windows Media / PCTV encoding set.',0);


Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (61,11);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (401,1);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (402,2);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (403,3);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (404,4);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (405,5);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (406,6);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (626,65);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (521,94);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (407,7);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (408,8);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (522,91);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (627,66);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (74,100);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (628,75);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (409,12);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (410,13);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (411,14);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (412,15);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (413,18);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (569,70);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (570,71);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (181,102);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (182,101);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (571,77);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (572,78);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (573,79);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (574,72);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (575,73);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (576,74);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (462,21);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (463,23);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (464,22);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (465,25);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (466,24);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (467,27);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (468,26);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (361,73);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (523,93);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (524,95);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (601,103);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (121,41);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (122,40);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (577,68);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (578,69);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (579,64);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (580,67);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (581,62);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (582,61);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (583,60);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (602,104);


Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (21,61);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,401);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,402);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,403);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,404);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,405);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,406);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,407);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,408);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,409);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,410);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,411);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,412);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (23,413);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (24,181);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (24,182);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (25,74);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (26,121);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (26,122);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,569);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,570);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,571);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,572);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,573);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,574);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,575);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (41,576);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (61,521);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (61,522);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (102,361);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,462);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,463);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,464);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,465);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,466);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,467);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (121,468);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,577);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,578);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,579);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,580);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,581);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,582);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (122,583);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (181,523);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (181,524);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (201,601);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (202,602);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (221,626);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (221,627);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (221,628);



Insert into WFM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('workflowCleanupJob','DEFAULT',null,'com.mobitv.arlanda.wfm.tasks.CleanupTask','0','0','0','0');

Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (61,'TranscodeWinMo','Winmo transcode workflow','TranscodeWinMo','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>WinMo</value>
                </dataitem>
            </taskdata>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (2,'DefaultTranscode','Default content transcoding workflow','Transcode','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
     <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
        <dependency>GetAssetInfo</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>GetAssetInfo</id>
      <class>GetAssetInfoTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (81,'TranscodeRTSP','RTSP transcode workflow','TranscodeRTSP','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>RTSP</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (121,'DefaultAudioIngest','Default audio-only content ingestion workflow','IngestAudioContent','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>XCodeAudioSetup</id>
            <class>XCodeAudioSetup</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeAudioSetup</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (161,'TranscodeProgressiveDownload','Progressive Download transcode workflow','TranscodeProgressiveDownload','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>ProgressiveDownload</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (1,'DefaultIngest','Default content ingestion workflow','IngestContent','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
 <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
        <dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
        <dependency>XCodePreflight</dependency>
      </dependencies>
      <taskdata>
     <dataitem>
     <key>xcodeTaskName</key>
     <value>ElementalXCodeTask</value>
     </dataitem>
     <dataitem>
     <key>produceEncodingsForAllAspectRatios</key>
     <value>false</value>
     </dataitem>
     </taskdata>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (201,'TranscodePCTV','PCTV transcode workflow','TranscodePCTV','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>MakeAssetAvailable</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>GetAssetInfo</id>
            <class>GetAssetInfoTask</class>
            <dependencies/>
            <taskdata/>
        </task>
        <task>
            <id>XCodeTask</id>
            <class>XCodeTask</class>
            <dependencies>
                <dependency>GetAssetInfo</dependency>
            </dependencies>
            <taskdata>
                <dataitem>
                    <key>encodingName</key>
                    <value>PCTV</value>
                </dataitem>
            </taskdata>
        </task>
        <task>
            <id>MakeAssetAvailable</id>
            <class>MakeAssetAvailableTask</class>
            <dependencies>
                <dependency>XCodeTask</dependency>
            </dependencies>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (141,'RegenerateProviderSMILDocuments','Default SMIL document regeneration workflow','RegenerateProviderSMILDocuments','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
    <tasklist>
        <task>
            <id>Finished</id>
            <class>FinishedTask</class>
            <dependencies>
                <dependency>RegenerateProviderSMILDocuments</dependency>
            </dependencies>
            <taskdata/>
        </task>
        <task>
            <id>RegenerateProviderSMILDocuments</id>
            <class>RegenerateProviderSMILDocumentsTask</class>
            <dependencies/>
            <taskdata/>
        </task>
    </tasklist>
</workflowdescription>
');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (181,'IngestWithAds','Default ingestion workflow with ads','IngestContent','<?xml version="1.0"
  encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
<dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
<dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
<dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
<dependency>XCodePreflight</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');
Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (182,'TranscodeWithAds','Default transcode workflow with ads','Transcode','<?xml version="1.0"
  encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
<dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
<dependency>CreateSMILDocumentForAsset</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>CreateSMILDocumentForAsset</id>
      <class>CreateSMILDocumentForAssetTask</class>
      <dependencies>
<dependency>XCodeSetup</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodeSetup</id>
      <class>XCodeSetup</class>
      <dependencies>
<dependency>XCodePreflight</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>XCodePreflight</id>
      <class>XCodePreflight</class>
      <dependencies>
        <dependency>SelectAdvertisements</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>SelectAdvertisements</id>
      <class>SelectAdvertisementsTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');

Insert into WFM_PROVIDER_CONTEXT (ID,PRIORITY,CONTENT_AUTH_MODE,TRANSCODING_CLUSTER,ADVERTISING_SOURCE,ENCRYPTION_REQUIRED) values (2,500,'NotRequired','simulator',null,'FALSE');

Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,21);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,23);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,41);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,61);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,141);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,81);

Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (2,1);

commit;
