-- MySQL dump 10.11
--
-- Host: localhost    Database: aspera_stats_collector
-- ------------------------------------------------------
-- Server version	5.0.95

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fasp_files`
--

DROP TABLE IF EXISTS `fasp_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fasp_files` (
  `id` bigint(20) NOT NULL auto_increment,
  `logged_from` varchar(255) NOT NULL,
  `node_uuid` varchar(36) NOT NULL,
  `session_id` varchar(36) NOT NULL,
  `status` varchar(16) NOT NULL,
  `created_at` datetime NOT NULL,
  `started_at` datetime default NULL,
  `stopped_at` datetime default NULL,
  `file_fullpath` text NOT NULL,
  `file_index` varchar(32) NOT NULL,
  `file_basename` text NOT NULL,
  `source_item` text,
  `size` bigint(20) NOT NULL default '0',
  `start_byte` bigint(20) NOT NULL default '0',
  `bytes_written` bigint(20) NOT NULL default '0',
  `bytes_contig` bigint(20) NOT NULL default '0',
  `bytes_lost` bigint(20) NOT NULL default '0',
  `usecs` bigint(20) NOT NULL default '0',
  `checksum` varchar(255) default NULL,
  `checksum_type` varchar(16) default NULL,
  `err_code` int(11) NOT NULL,
  `err_desc` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `idx_unique` (`session_id`,`node_uuid`,`file_index`),
  KEY `idx_session_id` (`session_id`),
  KEY `idx_node_uuid` (`node_uuid`),
  KEY `idx_file_basename_16` (`file_basename`(16))
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fasp_nodes`
--

DROP TABLE IF EXISTS `fasp_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fasp_nodes` (
  `id` bigint(20) NOT NULL auto_increment,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `node_address` varchar(128) NOT NULL,
  `node_name` varchar(255) NOT NULL,
  `node_uuid` varchar(36) default NULL,
  `reported_hostname` varchar(255) default NULL,
  `reported_central_uri` varchar(255) default NULL,
  `reported_active_sessions` int(11) default NULL,
  `reported_logger_version` varchar(255) default NULL,
  `reported_os` varchar(16) default NULL,
  `reported_os_version` varchar(255) default NULL,
  `reported_from` varchar(255) default NULL,
  `reported_at` datetime default NULL,
  `port` int(11) default NULL,
  `node_status` varchar(255) default 'initial',
  `poll_status` varchar(255) default 'idle',
  `owner_uuid` varchar(255) default NULL,
  `sessions_iteration_token` varchar(255) default NULL,
  `files_iteration_token` varchar(255) default NULL,
  `last_poll_ts` bigint(20) default '0',
  `last_successful_poll_ts` bigint(20) default '0',
  `keep_alive_ts` bigint(20) default '0',
  `err_desc` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `idx_node_address` (`node_address`),
  KEY `idx_node_uuid` (`node_uuid`),
  KEY `idx_node_name` (`node_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fasp_sessions`
--

DROP TABLE IF EXISTS `fasp_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fasp_sessions` (
  `id` bigint(20) NOT NULL auto_increment,
  `logged_from` varchar(255) NOT NULL,
  `node_uuid` varchar(36) NOT NULL,
  `session_id` varchar(36) NOT NULL,
  `status` varchar(16) NOT NULL,
  `created_at` datetime NOT NULL,
  `started_at` datetime default NULL,
  `stopped_at` datetime default NULL,
  `last_queried_at` datetime default NULL,
  `last_requested_at` datetime default NULL,
  `user` varchar(128) default NULL,
  `cookie` text,
  `token` text,
  `initiator` varchar(16) NOT NULL,
  `operation` varchar(16) NOT NULL,
  `server_addr` varchar(255) default NULL,
  `server_sshport` int(11) NOT NULL,
  `server_faspport` int(11) NOT NULL,
  `client_addr` varchar(255) default NULL,
  `client_faspport` int(11) NOT NULL,
  `cipher` varchar(16) NOT NULL,
  `dest_path` text,
  `files_complete` int(11) NOT NULL,
  `files_failed` int(11) NOT NULL,
  `bytes_written` bigint(20) NOT NULL default '0',
  `bytes_transferred` bigint(20) NOT NULL default '0',
  `bytes_lost` bigint(20) NOT NULL default '0',
  `usecs` bigint(20) NOT NULL default '0',
  `temp_prev_usecs` bigint(20) default NULL,
  `temp_prev_bytes_transferred` bigint(20) default NULL,
  `temp_bitrate` bigint(20) default NULL,
  `temp_bitrate_calculated_at` datetime default NULL,
  `network_delay` int(11) default NULL,
  `err_code` int(11) NOT NULL,
  `err_desc` varchar(255) default NULL,
  `bytes_pretransfer` bigint(20) default '0',
  `files_pretransfer` int(11) default '0',
  `dirs_pretransfer` int(11) default '0',
  `priority` varchar(16) default 'normal',
  `transport` varchar(16) default NULL,
  `args_attempted` int(11) default NULL,
  `args_completed` int(11) default NULL,
  `paths_attempted` int(11) default NULL,
  `paths_failed` int(11) default NULL,
  `paths_irreg` int(11) default NULL,
  `paths_excluded` int(11) default NULL,
  `dirscans_completed` int(11) default NULL,
  `filescans_completed` int(11) default NULL,
  `mkdirs_attempted` int(11) default NULL,
  `mkdirs_failed` int(11) default NULL,
  `mkdirs_passed` int(11) default NULL,
  `files_attempted` int(11) default NULL,
  `files_skipped` int(11) default NULL,
  `retry_timeout` int(11) default NULL,
  `fallback_protocol` varchar(16) default NULL,
  `temp_smoothed_bitrate` bigint(20) default NULL,
  `temp_eta` datetime default NULL,
  `source_paths` text,
  `start_email_sent_at` datetime default NULL,
  `stop_email_sent_at` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `idx_node_uuid_session_id` (`node_uuid`,`session_id`),
  KEY `idx_node_uuid` (`node_uuid`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-09 22:15:04
