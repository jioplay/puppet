Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.instance.smil.naming','/var/Jukebox/vod/smil/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','/var/Jukebox/vod/smil/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','Path and naming convention to where SMIL asset instances should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${VERSION}      - The version
    \${SUFFIX}       - The suffix of the file',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('business.search.method','solr','solr','If ''solr'', then the SOLR search server will be used for business search queries.  If ''dam'', then the DAM
    will perform all business search queries agains the relational database.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('search.server.url','http://jusearch:8080/mobi-search/arlanda','http://jusearch:8080/mobi-search/arlanda','The URL to the SOLR search server.  Must be specified if using the ''solr'' business.search.method.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('apply.winmo.pctv.namingpatch','true','true','If true DAM creates symlink named inventoryId.\${extension} to the latest version of an instance having
    encodingId 100, 101 and 102. true or false.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('service.stalled.ftp','1','1','This value desides when an ftp transport is concidered to be stalled. I.e the state is transporting and
	lastModified > this value. The number is in hours.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('remove.deadassets.job','0 0 8 * * ?','0 0 8 * * ?','This job permenantly removes any Assets that are concidered dead.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('monitor.event.directory','/opt/ReachTV/Farm/monitor','/opt/ReachTV/Farm/monitor','The directory to where the DAM should write its monitor event files. Since only one node writes the event files
    this directory should be a shared directory (SAN).',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('assetexpired.job','0 0/10 * * * ?','0 * * * * ?','When executed, the DAM will find any Assets that has expired since last time this job ran,
    concecvently the more infrekvent this job runs the more inexact the extarnal systems will
    be informed of the expiration of the Asset.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('service.job','0 0/5 * * * ?','0 0/5 * * * ?','Interval when the DAM should run service jobs.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.instance.link.type','move','move','How DAM is going to move instance files into the asset repository, this can be supplied as a parameter
    when adding an Asset, if not DAM fallbacks to this configured value. Valid values are: copy, move, hardlink
    or softlink. This also applies to original instances.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('max.download.attempts','3','3','This value desides how many times DAM tries to download an Asset before it is concidered as an error.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('fetchassets.job','0 * * * * ?','0 * * * * ?','Upon indirect upload, the DAM tries to download the Asset from a remote location.
    If for some reason the remote server is unavailable. The Asset is put in a waiting
    state. When this jub runs the DAM tries to any Assets that not has been downloaded.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('max.concurrent.downloads','100','200','This value desides how many concurrent downloads from remote site on DAM node can have.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.topic.ttl','43200000','43200000','The time in milliseconds on how long a message sent to ASSET.TOPIC should be stored
    until it expires by the jmsbroker.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.original.naming','/var/Jukebox/uploads/originals/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','/var/Jukebox/uploads/originals/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','Path and naming convention to where asset original files should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${GROUP_ID}     - The group id
    \${VERSION}      - The version
    \${SUFFIX}       - The suffix of the file',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('original.metadata.naming','/var/Jukebox/uploads/metadata/\${PROVIDER_ID}/\${INVENTORY_ID}-\${VERSION}.xml','/var/Jukebox/uploads/metadata/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.xml','Path and naming convention to where original meta data files (if provided) should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${GROUP_ID}     - The group id
    \${VERSION}      - The version',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('dead.asset','10','7','This value desides when an Asset is concidered as dead. I.e. it should be expired, 
    and lastModified > this value. The number is in days.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('publishassets.job','0 * * * * ?','0 * * * * ?','When this job is triggered the DAM finds all Assets that are ready to be published
    (since last time this job was triggered) an publish them.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.instance.naming','/var/Jukebox/vod/encoding-\${ENCODING_ID}/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','/var/Jukebox/vod/encoding-\${ENCODING_ID}/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','Path and naming convention to where asset instances should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${GROUP_ID}     - The group id
    \${VERSION}      - The version
    \${ENCODING_ID}  - The encoding id
    \${SUFFIX}       - The suffix of the file',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('version.removal','1','1','When an Asset is replaced, the DAM waits until the duration of the Asset + this value (in minutes) has
    gone before removing the old version permanently from disk.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('monitor.event.job','0 0/30 * * * ?','0 0/5 * * * ?','Interval in minutes between when the DAM should write its monitor event files.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('jmx.hibernate.stats','true','false','Boolean true / false if Hibernate JMX stat bean should be used, only applicable if Hibernate is used as JPA provider.
    Changing this conf parameter will need a restart before changes take effect.',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('max.request.threads','10','10','The maximum simultainusly threads DAM can use for handling JMS messages',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.original.thumbnail.naming','/var/Jukebox/uploads/thumbnails/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','/var/Jukebox/uploads/thumbnails/\${PROVIDER_ID}/\${INVENTORY_ID}\${VERSION}.\${SUFFIX}','Path and naming convention to where asset thumbnail original files should be stored. Valid expressions are:
    \${INVENTORY_ID} - The inventory id
    \${PROVIDER_ID}  - The provider id
    \${GROUP_ID}     - The group id
    \${VERSION}      - The version
    \${SUFFIX}       - The suffix of the file',CURRENT_TIMESTAMP);
Insert into DAM_CONFIG (CONFIG_KEY,CONFIG_VALUE,DEFAULT_CONFIG_VALUE,DESCRIPTION,LAST_MODIFIED) values ('asset.instance.encodings.list.namings','/var/Jukebox/vod/encodings-list/${PROVIDER_ID}/${INVENTORY_ID}${VERSION}.ini','/var/Jukebox/vod/encodings-list/${PROVIDER_ID}/${INVENTORY_ID}${VERSION}.ini','Location to store encodings-list files created by FFP',CURRENT_TIMESTAMP);









Insert into DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('AssetExpiredJob','dam',null,'com.mobitv.arlanda.dam.scheduler.AssetExpiredJob','0','0','0','1');
Insert into DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('FetchAssetsJob','dam',null,'com.mobitv.arlanda.dam.scheduler.FetchAssetsJob','0','0','0','1');
Insert into DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('PublishAssetsJob','dam',null,'com.mobitv.arlanda.dam.scheduler.PublishAssetJob','0','0','0','1');
Insert into DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('RemoveDeadAssetsJob','dam',null,'com.mobitv.arlanda.dam.scheduler.RemoveDeadAssetsJob','0','0','0','1');
Insert into DAM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('ServiceJob','dam',null,'com.mobitv.arlanda.dam.scheduler.ASMServiceJob','0','0','0','1');




Insert into DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('RemoveDeadAssetsJobTrigger','dam ','RemoveDeadAssetsJob','dam','0',null,1303542000000,1303455600000,5,'WAITING','CRON',1290151231000,0,null,0);
Insert into DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('AssetExpiredJobTrigger','dam','AssetExpiredJob','dam','0',null,1303495740000,1303495680000,5,'WAITING','CRON',1218236517000,0,null,0);
Insert into DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('FetchAssetsJobTrigger','dam','FetchAssetsJob','dam','0',null,1303495740000,1303495680000,5,'WAITING','CRON',1218236517000,0,null,0);
Insert into DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('PublishAssetsJobTrigger','dam','PublishAssetsJob','dam','0',null,1303495740000,1303495680000,5,'WAITING','CRON',1218236517000,0,null,0);
Insert into DAM_QRTZ_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,JOB_NAME,JOB_GROUP,IS_VOLATILE,DESCRIPTION,NEXT_FIRE_TIME,PREV_FIRE_TIME,PRIORITY,TRIGGER_STATE,TRIGGER_TYPE,START_TIME,END_TIME,CALENDAR_NAME,MISFIRE_INSTR) values ('ServiceJobTrigger','dam','ServiceJob','dam','0',null,1303495800000,1303495500000,5,'WAITING','CRON',1218236517000,0,null,0);


Insert into DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('RemoveDeadAssetsJobTrigger','dam ','0 0 7 * * ?','UTC');
Insert into DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('AssetExpiredJobTrigger','dam','0 * * * * ?','Europe/London');
Insert into DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('FetchAssetsJobTrigger','dam','0 * * * * ?','Europe/London');
Insert into DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('PublishAssetsJobTrigger','dam','0 * * * * ?','Europe/London');
Insert into DAM_QRTZ_CRON_TRIGGERS (TRIGGER_NAME,TRIGGER_GROUP,CRON_EXPRESSION,TIME_ZONE_ID) values ('ServiceJobTrigger','dam','0 0/5 * * * ?','Europe/London');
