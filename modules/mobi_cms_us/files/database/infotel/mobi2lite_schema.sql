-- MySQL dump 10.11
--
-- Host: localhost    Database: mobi2lite
-- ------------------------------------------------------
-- Server version	5.0.95

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CONTENT`
--

DROP TABLE IF EXISTS `CONTENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTENT` (
  `ACTUAL_LOCATION` varchar(255) NOT NULL,
  `IS_AD` bit(1) default NULL,
  `CHECKSUM` varchar(255) default NULL,
  `CONSUMED` varchar(255) NOT NULL,
  `CRID` varchar(255) NOT NULL,
  `DURATION` float NOT NULL,
  `EXPIRES` datetime default NULL,
  `GENRE` varchar(255) default NULL,
  `IS_INTERSTITIAL` varchar(255) default NULL,
  `KEYWORD` varchar(255) default NULL,
  `LASTMOD` datetime default NULL,
  `NUM_FRAMES` bigint(20) default NULL,
  `ORIGINAL_LOCATION` varchar(255) default NULL,
  `PROVIDER_ID` bigint(20) NOT NULL,
  `SLOT` bigint(20) NOT NULL,
  `START_OF_AVAILABILITY` datetime default NULL,
  `STATUS` bigint(20) NOT NULL,
  `UPLOAD_DATE` datetime NOT NULL,
  `VERSION` int(11) default NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`INVENTORY_ID`),
  KEY `FK6382C05951E0F5B5` (`INVENTORY_ID`),
  CONSTRAINT `FK6382C05951E0F5B5` FOREIGN KEY (`INVENTORY_ID`) REFERENCES `TV_INVENTORY` (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Tables structure for table `FOLDER_INVENTORY`
--

DROP TABLE IF EXISTS `FOLDER_INVENTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FOLDER_INVENTORY` (
  `CHAPTERIZED_CONTENT` varchar(255) default NULL,
  `EXPIRES` datetime default NULL,
  `GENRE` varchar(255) default NULL,
  `KEYWORD` varchar(255) default NULL,
  `MCDPATH` varchar(255) NOT NULL,
  `NETWORK` varchar(255) default NULL,
  `PLAYLIST_ENABLED` varchar(255) default NULL,
  `PROVIDER_ID` bigint(20) NOT NULL,
  `SHOW` varchar(255) default NULL,
  `START_OF_AVAILABILITY` datetime default NULL,
  `THUMBNAIL_LOCATION` varchar(255) default NULL,
  `TOTAL_NUMBER_OF_PARTS` int(11) default NULL,
  `UI_NAME` varchar(255) NOT NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  `EPISODE_NUMBER` bigint(20) default NULL,
  `SERIES_NUMBER` bigint(20) default NULL,
  PRIMARY KEY  (`INVENTORY_ID`),
  KEY `FK6A4A02AB7F5F6C9A` (`INVENTORY_ID`),
  CONSTRAINT `FK6A4A02AB7F5F6C9A` FOREIGN KEY (`INVENTORY_ID`) REFERENCES `TV_INVENTORY` (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `JB_ADVERTISING_FOLDERS`
--

DROP TABLE IF EXISTS `JB_ADVERTISING_FOLDERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JB_ADVERTISING_FOLDERS` (
  `CONTENT_FOLDER_ID` bigint(20) NOT NULL,
  `PROVIDER_ID` bigint(20) NOT NULL,
  `POST_ROLL_FOLDER_ID` bigint(20) default NULL,
  `PRE_ROLL_FOLDER_ID` bigint(20) default NULL,
  PRIMARY KEY  (`CONTENT_FOLDER_ID`,`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `JB_FOLDER_CLIP_CONFIG`
--

DROP TABLE IF EXISTS `JB_FOLDER_CLIP_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JB_FOLDER_CLIP_CONFIG` (
  `clip_assign_type` varchar(255) NOT NULL,
  `clip_assign_to_id` bigint(20) NOT NULL,
  `created_by_app` varchar(255) default NULL,
  `folder_inventory_id` bigint(20) NOT NULL,
  `subscription_mode` bigint(20) NOT NULL,
  PRIMARY KEY  (`clip_assign_to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROVIDERS`
--

DROP TABLE IF EXISTS `PROVIDERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROVIDERS` (
  `PROVIDER_ID` bigint(20) NOT NULL auto_increment,
  `DFLT_CONTENT_SHELF_LIFE` float default NULL,
  `PROVIDER` varchar(255) default NULL,
  `SECURED_OVER_WIFI` varchar(255) default NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `REQUIRES_XML_METADATA` varchar(10) default 'FALSE',
  `TIME_ZONE` varchar(255) default NULL,
  `MANAGE_CHAPTERIZED_CONTENT` varchar(255) default NULL,
  PRIMARY KEY  (`PROVIDER_ID`),
  UNIQUE KEY `PROVIDER` (`PROVIDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `PROVIDER_METADATA_CONFIG`
--

DROP TABLE IF EXISTS `PROVIDER_METADATA_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROVIDER_METADATA_CONFIG` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `CHECKSUM` varchar(255) default NULL,
  `CRID` varchar(255) default NULL,
  `ENABLE_LOCAL_PLAYBACK` varchar(255) default NULL,
  `ENABLE_WIFI` varchar(255) default NULL,
  `EXPIRES` varchar(255) default NULL,
  `FOLDER` varchar(255) default NULL,
  `GENRE` varchar(255) default NULL,
  `INDIRECT_UPLOAD_URL` varchar(255) default NULL,
  `INDUSTRY` varchar(255) default NULL,
  `KEYWORD` varchar(255) default NULL,
  `SHORT_TITLE` varchar(255) default NULL,
  `SLOT` varchar(255) default NULL,
  `START_OF_AVAILABILITY` varchar(255) default NULL,
  `SYNOPSIS` varchar(255) default NULL,
  `THUMBNAIL_UPLOAD_URL` varchar(255) default NULL,
  `TICKER` varchar(255) default NULL,
  `TITLE` varchar(255) default NULL,
  `CLOSED_CAPTION_URL` varchar(255) default NULL,
  `ENCRYPTION_REQUIRED` varchar(255) default NULL,
  `ASPECT_RATIO` varchar(255) default 'ASPECT_RATIO',
  `EPISODE_NUMBER` VARCHAR(255) default 'EPISODE_NUMBER',
  `SERIES_NUMBER` VARCHAR(255) default 'SERIES_NUMBER',
  PRIMARY KEY  (`PROVIDER_ID`),
  UNIQUE KEY `PROVIDER_ID` (`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TV_FOLDER_ITEM`
--

DROP TABLE IF EXISTS `TV_FOLDER_ITEM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TV_FOLDER_ITEM` (
  `ID` bigint(20) NOT NULL auto_increment,
  `CREATED_TIME` datetime NOT NULL,
  `DISPLAY_CODE` varchar(255) NOT NULL,
  `DISPLAY_ORDER` bigint(20) NOT NULL,
  `ITEM_INVENTORY_ID` bigint(20) NOT NULL,
  `LAST_UPDATED_TIME` datetime NOT NULL,
  `LAST_UPDATED_BY` varchar(255) NOT NULL,
  `FOLDER_INVENTORY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `FOLDER_INVENTORY_ID` (`FOLDER_INVENTORY_ID`,`ITEM_INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TV_INVENTORY`
--

DROP TABLE IF EXISTS `TV_INVENTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TV_INVENTORY` (
  `INVENTORY_ID` bigint(20) NOT NULL auto_increment,
  `BACKEND_CHANNEL` bigint(20) NOT NULL default '0' ,
  `DESCRIPTION` varchar(2047) default NULL,
  `DISPLAY_NAME` varchar(255) NOT NULL,
  `ICON_NAME` varchar(255) default NULL,
  `JUKEBOX_CHANNEL_ID` bigint(20) default NULL,
  `LBS_DEFAULT_RULE` varchar(255) NOT NULL,
  `LONG_NAME` varchar(255) default NULL,
  `MEDIA_RESTRICTIONS` varchar(255) default NULL,
  `MEDIATYPE` varchar(255) NOT NULL,
  `MEDIA_ASPECT_RATIO` varchar(255) default '4x3',
  `NAME` varchar(255) NOT NULL,
  `STATION_ID` varchar(255) default NULL,
  PRIMARY KEY  (`INVENTORY_ID`),
  UNIQUE KEY `INVENTORY_ID` (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TV_PACKAGE_INVENTORY`
--

DROP TABLE IF EXISTS `TV_PACKAGE_INVENTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TV_PACKAGE_INVENTORY` (
  `PACKAGE_ID` bigint(20) NOT NULL,
  `INVENTORY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`INVENTORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-30  5:50:37


