<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <normalizedProviderMetadata>
      
      <xsl:if test="/root/record/show" >
		<xsl:element name="show_id">
          <xsl:value-of select="/root/record/show" />
        </xsl:element>
	  </xsl:if>
      
      <pair>
		
		<metadata>
          <xsl:attribute name="type">STRING</xsl:attribute>
          <xsl:attribute name="key">Content_Type</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="/root/record/Content_Type" />
          </xsl:attribute>
        </metadata>

        <metadata>
          <xsl:variable name="u" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
          <xsl:variable name="l" select="'abcdefghijklmnopqrstuvwxyz'"/>	
          <xsl:attribute name="type">STRING</xsl:attribute>
          <xsl:attribute name="key">sub_type</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="translate(/root/record/Content_Type,$u,$l)" />
          </xsl:attribute>
        </metadata>

        <metadata>
          <xsl:attribute name="type">STRING</xsl:attribute>
          <xsl:attribute name="key">show</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="/root/record/show" />
          </xsl:attribute>
        </metadata>


        <metadata>
          <xsl:attribute name="type">STRING</xsl:attribute>
          <xsl:attribute name="key">Language</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="/root/record/Language" />
          </xsl:attribute>
        </metadata>

        <metadata>
          <xsl:attribute name="type">STRING</xsl:attribute>
          <xsl:attribute name="key">rating</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="/root/record/Ratings" />
          </xsl:attribute>
        </metadata>


        <metadata>
          <xsl:attribute name="type">STRING</xsl:attribute>
          <xsl:attribute name="key">Vendor</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="/root/record/Vendor" />
          </xsl:attribute>
        </metadata>

        <xsl:template name="Genre">
                <xsl:if test="string-length(.) &gt; 0">
                        <xsl:call-template name="tokenize">
                        <xsl:with-param name="string" select="/root/record/Sub_Genre"/>
                        <xsl:with-param name="delim" select="','"/>
                        </xsl:call-template>
                </xsl:if>
                </xsl:template>

        <metadata>
          <xsl:attribute name="type">INTEGER</xsl:attribute>
          <xsl:attribute name="key">years</xsl:attribute>
          <xsl:variable name='datetime' select="/root/record/Release_Date" />
          <xsl:attribute name="value">
                 <xsl:value-of select='substring( $datetime,7,10)' />
          </xsl:attribute>
        </metadata>

        <metadata>
          <xsl:attribute name="type">STRING</xsl:attribute>
          <xsl:attribute name="key">media_aspect_ratio</xsl:attribute>
          <xsl:variable name='aspectRatio' select="/root/record/Version_Availability" />
          <xsl:attribute name="value">
                        <xsl:if test="$aspectRatio='SD'">
                                <xsl:value-of select="'4x3'" />
                        </xsl:if>
                        <xsl:if test="$aspectRatio='HD'">
                                <xsl:value-of select="'16x9'" />
                        </xsl:if>
          </xsl:attribute>
        </metadata>
	<metadata>
	  <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
	  <xsl:attribute name="key">sku_ids</xsl:attribute>
	  <xsl:attribute name="value">VODSERVICE_123456</xsl:attribute>
	</metadata>
	<metadata>
	  <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
	  <xsl:attribute name="key">vid</xsl:attribute>
	  <xsl:attribute name="value">infotel-r4g-5.0-rest</xsl:attribute>
	</metadata>

<!--
        <metadata>
          <xsl:attribute name="type">STRING</xsl:attribute>
          <xsl:attribute name="key">Content_Type</xsl:attribute>
          <xsl:variable name='contentType' select="/root/record/Content_Type" />
          <xsl:attribute name="value">
                        <xsl:if test="$contentType='SD'">
                                <xsl:value-of select="'4x3'" />
                        </xsl:if>
                        <xsl:if test="$aspectRatio='HD'">
                                <xsl:value-of select="'16x9'" />
                        </xsl:if>
          </xsl:attribute>
        </metadata>
-->

      </pair>
    </normalizedProviderMetadata>
  </xsl:template>


  <xsl:template name="tokenize">
        <xsl:param name="string" />
        <xsl:param name="delim" />
        <xsl:variable name="u" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
        <xsl:variable name="l" select="'abcdefghijklmnopqrstuvwxyz'"/>
		
        <xsl:choose>
                <xsl:when test="contains($string, $delim)">
                        <metadata>
          <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
          <xsl:attribute name="key">genre_list</xsl:attribute>
          <xsl:attribute name="value">
          <xsl:value-of select="substring-before(translate(normalize-space($string),$u,$l), $delim)" />
          </xsl:attribute>
          </metadata>
                        <xsl:call-template name="tokenize">
                        
                                <xsl:with-param name="string" select="substring-after($string, $delim)" />
                                <xsl:with-param name="delim" select="$delim" />
                        </xsl:call-template>
                </xsl:when>

                <xsl:otherwise>
                        <metadata>
          <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
          <xsl:attribute name="key">genre_list</xsl:attribute>
          <xsl:attribute name="value">
           <xsl:value-of select="translate(normalize-space($string),$u,$l)" />
           </xsl:attribute>
          </metadata>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>

</xsl:stylesheet>


