drop database activemq;
create database activemq;
create user activemq;
ALTER USER activemq with password 'activemq';
GRANT ALL PRIVILEGES ON database activemq to activemq;