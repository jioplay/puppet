class mobi_cms_us::eesimulator::config {

    file { "/opt/arlanda/conf/tools/eesimulator/eesimulator.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_cms_us/eesimulator_files/eesimulator.properties",
        require => Class["eesimulator::install"],
        notify   => Service["eesimulator"],
    }

}
