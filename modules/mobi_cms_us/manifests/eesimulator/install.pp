class mobi_cms_us::eesimulator::install {

  package { "mobi-arlanda-tools-eesimulator-${mobi_cms_us::eesimulator::eesimulator_version}" :
    ensure => present,
    notify => Service["eesimulator"],
  }

}
