class mobi_cms_us::eesimulator::service {
    service { "eesimulator":
        ensure => $mobi_cms_us::eesimulator::ensure,
        enable => $mobi_cms_us::eesimulator::enable,
        require => [ Class["mobi_cms_us::eesimulator::install"] ],
    }
}
