# == Class: mobi_cms_us::filesystem
#
#  creates directories needed for configuration when nfs mounts not present
#
# === Parameters:
#
# === Requires:
#
# === Sample Usage
#
#  class { "mobi_cms_us::filesystem": }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::filesystem (
  $external_filesystem = false,
)
{


    file { "/var/www":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/www/html":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    if (
        (! $external_filesystem) and
        (
          ($domain != "qa.dmz") and
          ($hostname != "ts-dev-01") and
          ($hostname != "vmqamgocms") and
          ($hostname != "vmqamgomn") and
          ($hostname != "vmqamgowvss") and
          ($hostname != "vmqamgolucene") and
          ($hostname != "lab126"))
        )
      {

#        file { "/home/apache":
#          ensure => "directory",
#          owner  => "apache",
#          group  => "apache",
#          mode   => "775",
#        }

        file { "/var/Jukebox/vod":
          ensure => "directory",
          owner  => "root",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/vantage":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0755",
        }

        file { "/var/Jukebox/local":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/logs":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/monitor":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ftpupload":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0755",
        }

        file { "/var/Jukebox/extfeeds":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0755",
        }

        file { "/var/Jukebox/tmp":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/cache":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/socks":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/SCL":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/aspera":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/elemental":
          ensure => "directory",
          owner  => "root",
          group  => "root",
          mode   => "0755",
        }

        file { "/var/Jukebox/widevine":
          ensure => "directory",
          owner  => "root",
          group  => "root",
          mode   => "0755",
        }

        file { "/var/Jukebox/ee":
          ensure => "directory",
          owner  => "root",
          group  => "root",
          mode   => "0755",
        }

        file { "/var/Jukebox/mp":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

      }

        file { "/var/Jukebox/logs/contentd":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }


        file { "/var/Jukebox/ftpupload/mobi_test":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ftpupload/infotel":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ftpupload/yume":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/elemental/Input":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => 777,
        }


        file { "/var/Jukebox/elemental/Output":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => 777,
        }


        file { "/var/Jukebox/widevine/Input":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => 777,
        }


        file { "/var/Jukebox/widevine/Output":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => 777,
        }

        file { "/var/Jukebox/vod/mgo":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/LOW":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/PD":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/SD":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/HD":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/HD+":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/vod/widevine":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/vod/encodings-list":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/uploads/scc/":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/vod/widevine/LOW":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1101"
        }


        file { "/var/Jukebox/vod/widevine/PD":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1102"
        }


        file { "/var/Jukebox/vod/widevine/SD":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1103"
        }


        file { "/var/Jukebox/vod/widevine/HD":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1104"
        }


        file { "/var/Jukebox/vod/widevine/HD+":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1105"
        }


        file { "/var/Jukebox/ee/simulator":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/simulator/Input":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/simulator/Output":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/media":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/media/Input":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/media/Output":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/media/Output/mp":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/mp/metadata-repository":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/mp/setting-repository":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
          recurse => true,
          source => "puppet:///modules/mobi_cms_us/filesystem/settings",
        }
        file { "/var/Jukebox/uploads/metadata":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/originals":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/search_transform":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/smil":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/smil/staging":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/smil/templates":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/smil/templates/smil_template_advertising_enabled.xml":
          ensure => "present",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0664",
          source => "puppet:///modules/mobi_cms_us/filesystem/smil_template_advertising_enabled.xml",
        }

        file { "/var/Jukebox/uploads/smil/templates/smil_template_advertising_disabled.xml":
          ensure => "present",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0664",
          source => "puppet:///modules/mobi_cms_us/filesystem/smil_template_advertising_disabled.xml",
        }

        file { "/var/Jukebox/uploads/thumbnails":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/xslt":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }



      file { "/var/Jukebox/ftpupload/mobi_test/Device_pc-other":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Device_phone-android":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Device_phone-iOS":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Device_stb-other":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Device_tablet-android":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Device_tablet-iOS":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Location_India":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Location_United States":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Location_United States Minor Outlying Islands":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Max_H3":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Max_H9":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Network_GSM-CDMA":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Network_LTE":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Network_WiFi-ETH":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Screen_ExternalDisplay":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Screen_HDCP":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Baseline Content Policy":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }

      file { "/var/Jukebox/ftpupload/mobi_test/Screen_Phone":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Screen_STB":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }
      file { "/var/Jukebox/ftpupload/mobi_test/Screen_Tablet":
        ensure => "directory",
        owner  => "apache",
        group  => "arlanda",
        mode   => "0775",
      }

      file { "/var/Jukebox/ftpupload/mobi_test/Quality-Max_Bitrate_300":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0775",
        }

       file { "/var/Jukebox/ftpupload/mobi_test/Quality-Max_Height_400":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0775",
        }
        file { "/var/Jukebox/ftpupload/mobi_test/Quality-Max_Width_300":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0775",
        }


    file { "/opt/ReachTV":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/opt/ReachTV/Farm":
      ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }

    file { "/opt/ReachTV/Farm/monitor":
      ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "0775",
    }

    file { "/var/log/vsftpd.log":
      ensure => "present",
      owner  => "root",
      group  => "root",
      mode   => "0666",
    }

    file { "/var/log/ReachTV":
      ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }


    file { "/var/log/ReachTV/Farm":
      ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }

    file { "/opt/arlanda/":
        ensure => directory,
    }

    file { "/opt/arlanda/dam/":
        ensure => directory,
        require => File["/opt/arlanda/"],
    }

    file { "/opt/arlanda/dam/monitor":
        ensure => directory,
        require => File["/opt/arlanda/dam/"],
    }


}
