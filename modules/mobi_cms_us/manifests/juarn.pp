# == Class: mobi_cms_us::juarn
#
#  installs juarn component
#
# === Parameters:
#
#    $wfm_version::
#    $mp_version::
#    $fm_version::
#    $dam_version::
#    $dam_username::
#    $dam_password::
#    $wfm_username::
#    $wfm_password::
#    $mp_username::
#    $mp_password::
#    $fm_username::
#    $fm_password::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_cms_us::juarn" :
#           dam_version => "5.0.0-178058",
#           wfm_version => "5.0.0-180924",
#           mp_version => "5.0.0-180894",
#           fm_version => "5.0.0-179993",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::juarn (
    $dam_version = $mobi_cms_us::juarn::params::dam_version,
    $wfm_version = $mobi_cms_us::juarn::params::wfm_version,
    $mp_version = $mobi_cms_us::juarn::params::mp_version,
    $fm_version = $mobi_cms_us::juarn::params::fm_version,
    $dam_username = $mobi_cms_us::juarn::params::dam_username,
    $dam_password = $mobi_cms_us::juarn::params::dam_password,
    $wfm_username = $mobi_cms_us::juarn::params::wfm_username,
    $wfm_password = $mobi_cms_us::juarn::params::wfm_password,
    $mp_username = $mobi_cms_us::juarn::params::mp_username,
    $mp_password = $mobi_cms_us::juarn::params::mp_password,
    $fm_username = $mobi_cms_us::juarn::params::fm_username,
    $fm_password = $mobi_cms_us::juarn::params::fm_password,
    $hibernate_database_dialect = $mobi_cms_us::juarn::params::hibernate_database_dialect,
    $hibernate_database_vendor = $mobi_cms_us::juarn::params::hibernate_database_vendor,
    $jdbc_driver = $mobi_cms_us::juarn::params::jdbc_driver,
    $fm_oracle_native_queries_enabled = $mobi_cms_us::juarn::params::fm_oracle_native_queries_enabled,
    $dam_db_jdbc_url = $mobi_cms_us::juarn::params::dam_db_jdbc_url,
    $wfm_db_jdbc_url = $mobi_cms_us::juarn::params::wfm_db_jdbc_url,
    $mp_db_jdbc_url = $mobi_cms_us::juarn::params::mp_db_jdbc_url,
    $fm_db_jdbc_url = $mobi_cms_us::juarn::params::fm_db_jdbc_url,
)

inherits mobi_cms_us::juarn::params {
    include mobi_cms_us::juarn::install, mobi_cms_us::juarn::config, mobi_cms_us::tomcat_config
    anchor { "mobi_cms_us::juarn::begin": } -> Class["mobi_cms_us::juarn::install"] ->
    Class["mobi_cms_us::juarn::config"] -> Class["mobi_cms_us::tomcat_config"] ->
    anchor { "mobi_cms_us::juarn::end" :} -> os::motd::register { $name : }
}
