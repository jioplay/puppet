# == Class: mobi_cms_us::filesystem
#
#  creates directories needed for configuration when nfs mounts not present
#
# === Parameters:
#
# === Requires:
#
# === Sample Usage
#
#  class { "mobi_cms_us::filesystem": }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::filesystem2 (
  $external_filesystem = false,
)
{


    file { "/var/www":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }
    ->
    file { "/var/www/html":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    if ($external_filesystem){
      file { "/var/Jukebox":
        ensure       => "directory",
        mode         => "0775",
      }
    }else{
      #Need to add recurselimit cos file strutcture is huge which causes puppet run to hang
      file { "/var/Jukebox":
        ensure       => "directory",
        recurse      => true,
        recurselimit => 1,
        ignore       => ".snapshot",
        mode         => "0775",
        source       => "puppet:///modules/mobi_cms_us/Jukebox",
      }
    }
    file{
        ['/var/Jukebox/aspera','/var/Jukebox/SCL']:
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => '10001',
           group        => '888',
           require      => File['/var/Jukebox'],
        ;
        ['/var/Jukebox/cache','/var/Jukebox/local','/var/Jukebox/logs','/var/Jukebox/socks','/var/Jukebox/tmp']:
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => '48',
           group        => '48',
           require      => File['/var/Jukebox'],
        ;
        ['/var/Jukebox/extfeeds','/var/Jukebox/ftpupload','/var/Jukebox/monitor','/var/Jukebox/mp','/var/Jukebox/vantage']:
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => '48',
           group        => '888',
           require      => File['/var/Jukebox'],
        ;
        '/var/Jukebox/vod':
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => 'root',
           group        => '888',
           require      => File['/var/Jukebox'],
        ;
        '/var/Jukebox/mediasrc/ftpupload':
           ensure       => directory,
           recurse      => true,
           recurselimit => 2,
           ignore       => ".snapshot",
           owner        => '48',
           group        => '888',
           require      => File['/var/Jukebox'],
        ;
        '/var/Jukebox/mediasrc/uploads':
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => '10001',
           group        => '888',
           require      => File['/var/Jukebox'],
        ;
        ['/var/Jukebox/mediasrc/uploads/metadata','/var/Jukebox/mediasrc/uploads/originals','/var/Jukebox/mediasrc/uploads/smil','/var/Jukebox/mediasrc/uploads/thumbnails','/var/Jukebox/mediasrc/uploads/xslt']:
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => '48',
           group        => '888',
           require      => File['/var/Jukebox/mediasrc/uploads'],
        ;
        ['/var/Jukebox/mediasrc/uploads/scc','/var/Jukebox/mediasrc/uploads/search_transform']:
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => '10001',
           group        => '888',
           require      => File['/var/Jukebox/mediasrc/uploads'],
        ;
        ['/var/Jukebox/transcode/ee/simulator','/var/Jukebox/transcode/ee/media']:
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => '10001',
           group        => '888',
           require      => File['/var/Jukebox'],
        ;
        ['/var/Jukebox/vod/encodings-list','/var/Jukebox/vod/FMP4','/var/Jukebox/vod/mgo','/var/Jukebox/vod/widevine']:
           ensure       => directory,
           recurse      => true,
           recurselimit => 1,
           ignore       => ".snapshot",
           owner        => '10001',
           group        => '888',
           require      => File['/var/Jukebox/vod'],
        ;
    }

    file { ['/opt/ReachTV',"/opt/ReachTV/Farm",'/opt/ReachTV/Farm/monitor']:
      ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }
    file { "/var/log/vsftpd.log":
      ensure => "present",
      owner  => "root",
      group  => "root",
      mode   => "0666",
    }

    file { ["/var/log/ReachTV",'/var/log/ReachTV/Farm']:
      ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }
    file { "/opt/arlanda/":
        ensure => directory,
    }

    file { "/opt/arlanda/dam/":
        ensure  => directory,
        require => File["/opt/arlanda/"],
    }
    file { "/opt/arlanda/dam/monitor":
        ensure  => directory,
        require => File["/opt/arlanda/dam/"],
    }
}
