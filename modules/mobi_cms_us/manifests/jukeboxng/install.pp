class mobi_cms_us::jukeboxng::install {

    include apache

    # RPMs for the Jukebox NG program are based on what's specified in
    # http://jira.mobitv.corp/browse/PCC-6704

    package {"libxslt-${mobi_cms_us::jukeboxng::libxslt_version}":
      ensure => present,
      provider => yum,
    }

    package {"apr-${mobi_cms_us::jukeboxng::apr_version}":
      ensure => present,
      provider => yum,
    }

    package {"postgresql-libs-${mobi_cms_us::jukeboxng::postgresql_libs_version}":
      ensure => present,
      provider => yum,
    }

    package {"apr-util-${mobi_cms_us::jukeboxng::apr_version}":
      ensure => present,
      provider => yum,
    }

    package {"php53-common-${mobi_cms_us::jukeboxng::php_version}":
      ensure => present,
      provider => yum,
    }

    package {"gmp-${mobi_cms_us::jukeboxng::gmp_version}":
      ensure => present,
      provider => yum,
    }

    package {"php53-cli-${mobi_cms_us::jukeboxng::php_version}":
      ensure => present,
      provider => yum,
    }

    package {"php53-gd-${mobi_cms_us::jukeboxng::php_version}":
      ensure => present,
      provider => yum,
    }

    package {"php53-${mobi_cms_us::jukeboxng::php_version}":
      ensure => present,
      provider => yum,
    }

    exec { "Ruby":
      command => "/bin/rpm -i ${mobi_cms_us::jukeboxng::ruby_rpm_url}",
      onlyif => "/usr/bin/test `/bin/rpm -qa ruby | /bin/grep ruby-${mobi_cms_us::jukeboxng::ruby_version} | wc -l` -eq 0",
      notify => Class["apache::service"],
    }

    exec { "Ruby Gems":
      command => "/bin/rpm -i ${mobi_cms_us::jukeboxng::rubygems_rpm_url}",
      onlyif => "/usr/bin/test `/bin/rpm -qa rubygems | /bin/grep rubygems-${mobi_cms_us::jukeboxng::rubygems_version} | wc -l` -eq 0",
      notify => Class["apache::service"],
    }

    package {"mobi-user-rtv-${mobi_cms_us::jukeboxng::rtv_version}":
      ensure => present,
      provider => yum,
    }

    package {"cms_ui-${mobi_cms_us::jukeboxng::cms_ui_version}":
      ensure => present,
      provider => yum,
      notify => Class["apache::service"],
    }

    package {"ext-${mobi_cms_us::jukeboxng::ext_version}":
      ensure => present,
      provider => yum,
      notify => Class["apache::service"],
    }

    exec { "CMS UI QA Config":
      command => "/bin/rpm -i --force ${mobi_cms_us::jukeboxng::cms_ui_qaconf_rpm_url}",
      onlyif => "/usr/bin/test `/bin/rpm -qa cms_ui_qaconf | /bin/grep cms_ui_qaconf-${mobi_cms_us::jukeboxng::cms_ui_qaconf_version} | wc -l` -eq 0",
      notify => Class["apache::service"],
    }

    Package["mobi-user-rtv-${mobi_cms_us::jukeboxng::rtv_version}"] ->
    Package["libxslt-${mobi_cms_us::jukeboxng::libxslt_version}"] ->
    Package["apr-${mobi_cms_us::jukeboxng::apr_version}"] ->
    Package["postgresql-libs-${mobi_cms_us::jukeboxng::postgresql_libs_version}"] ->
    Package["apr-util-${mobi_cms_us::jukeboxng::apr_version}"] ->
    Package["php53-common-${mobi_cms_us::jukeboxng::php_version}"] ->
    Package["gmp-${mobi_cms_us::jukeboxng::gmp_version}"] ->
    Package["php53-cli-${mobi_cms_us::jukeboxng::php_version}"] ->
    Package["php53-gd-${mobi_cms_us::jukeboxng::php_version}"] ->
    Package["php53-${mobi_cms_us::jukeboxng::php_version}"] ->
    Exec["Ruby"] ->
    Exec["Ruby Gems"] ->
    Package["cms_ui-${mobi_cms_us::jukeboxng::cms_ui_version}"] ->
    Package["ext-${mobi_cms_us::jukeboxng::ext_version}"] ->
    Exec["CMS UI QA Config"]
}
