class mobi_cms_us::juamq::service {
    service { "activemq":
        ensure => $mobi_cms_us::juamq::ensure,
        enable => $mobi_cms_us::juamq::enable,
        require => [ Class["mobi_cms_us::juamq::install"], Class["mobi_cms_us::juamq::config"] ],
    }
}
