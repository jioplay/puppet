class mobi_cms_us::juamq::install {
    package { $::mobi_cms_us::juamq::package_name:
        ensure => $mobi_cms_us::juamq::version,
        notify   => Service["activemq"],
    }

    file { ["/opt/activemq" , "/opt/activemq/database"]:
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
    }

    file { "/opt/activemq/database/activemq_schema.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_cms_us/juamq/activemq_schema.sql.erb'),
        require => File["/opt/activemq/database"],
    }
}
