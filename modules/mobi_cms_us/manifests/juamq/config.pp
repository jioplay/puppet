class mobi_cms_us::juamq::config {
  $activemq_username = $mobi_cms_us::juamq::activemq_username
  $activemq_password = $mobi_cms_us::juamq::activemq_password
  $jdbc_driver = $mobi_cms_us::juamq::jdbc_driver
  $activemq_db_jdbc_url = $mobi_cms_us::juamq::activemq_db_jdbc_url
  $is_59 = $mobi_cms_us::juamq::is_59

  if ($is_59) {
    file { "/opt/activemq/conf/activemq.xml":
      replace => true,
      owner  => "rtv",
      group  => "rtv",
      mode => "0444",
      content => template("mobi_cms_us/juamq/activemq_59.xml.erb"),
      require => Class["mobi_cms_us::juamq::install"],
      notify   => Service["activemq"],
    }
  } elsif ($mobi_cms_us::juamq::ril) {
    file { "/opt/activemq/conf/activemq.xml":
      replace => true,
      owner   => "rtv",
      group   => "rtv",
      mode    => "0444",
      content => template('mobi_cms_us/juamq/activemq.ril.xml.erb'),
      require => Class["mobi_cms_us::juamq::install"],
      notify  => Service["activemq"],
    }
  } else {
    file { "/opt/activemq/conf/activemq.xml":
      replace => true,
      owner   => "rtv",
      group   => "rtv",
      mode    => "0444",
      content => template('mobi_cms_us/juamq/activemq.xml.erb'),
      require => Class["mobi_cms_us::juamq::install"],
      notify  => Service["activemq"],
    }
  }

  file { "/opt/activemq/lib/postgresql-9.1-901.jdbc4.jar":
    owner   => "rtv",
    group   => "rtv",
    mode    => "0644",
    source  => "puppet:///modules/mobi_cms_us/juamq_files/postgresql-9.1-901.jdbc4.jar",
    require => Class["mobi_cms_us::juamq::install"],
    notify  => Service["activemq"],
  }

  file { "/opt/activemq/lib/optional/mysql-connector-java-5.1.19-bin.jar":
    owner   => "rtv",
    group   => "rtv",
    mode    => "0644",
    source  => "puppet:///modules/mobi_cms_us/juamq_files/mysql-connector-java-5.1.19-bin.jar",
    require => Class["mobi_cms_us::juamq::install"],
    notify  => Service["activemq"],
  }

  file { "/opt/activemq/lib/optional/ojdbc-14_g.jar":
    ensure  => "present",
    require => Class["mobi_cms_us::juamq::install"],
    notify  => Service["activemq"],
  }
}
