# == Class: mobi_cms_us::juamq
#
#  installs juamq component
#
# === Parameters:
#
#     $activemq_ver = "5.5.0.0-170413"
#    $activemq_username::
#    $activemq_password::
#
# === Requires:
#
#     java
#     mobi_cms_us::database
#
# === Sample Usage
#
#  class { "mobi_cms_us::juamq" :
#          version => "5.5.0.0-170413",
#          ensure => "running",
#          enable => true,
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::juamq (
    $version = $mobi_cms_us::juamq::params::version,
    $package_name = $mobi_cms_us::juamq::params::package_name,
    $ensure = $mobi_cms_us::juamq::params::ensure,
    $enable = $mobi_cms_us::juamq::params::enable,
    $activemq_username = $mobi_cms_us::juamq::params::activemq_username,
    $activemq_password = $mobi_cms_us::juamq::params::activemq_password,
    $jdbc_driver = $mobi_cms_us::juamq::params::jdbc_driver,
    $activemq_db_jdbc_url = $mobi_cms_us::juamq::params::activemq_db_jdbc_url,
    $is_59 = $mobi_cms_us::juamq::params::is_59,
    $ril = $mobi_cms_us::juamq::params::ril,
    $config_package = undef, # required
    $config_package_ensure = latest,
)
inherits mobi_cms_us::juamq::params {
    include mobi_cms_us::juamq::install, mobi_cms_us::juamq::config, mobi_cms_us::juamq::service

    anchor { "mobi_cms_us::juamq::begin": } -> Class["mobi_cms_us::juamq::install"]
    Class["mobi_cms_us::juamq::config"] -> anchor { "mobi_cms_us::juamq::end":}
    os::motd::register { $name : }
}
