# == Class: mobi_cms_us::eesimulator
#
#  installs eesimulator component
#
# === Parameters:
#
#    $eesimulator_version::
#    $eesimulator_ensure::
#    $eesimulator_enable::
#
# === Requires:
#
#     java
#     group::arlanda
#     user::apache
#     user::rtv
#     mobi_cms_us::filesystem or nf mounts setup
#
# === Sample Usage
#
#  class { "mobi_cms_us::eesimulator" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::eesimulator (
    $eesimulator_version = $mobi_cms_us::eesimulator::params::eesimulator_version,
    $eesimulator_ensure = $mobi_cms_us::eesimulator::params::eesimulator_ensure,
    $eesimulator_enable = $mobi_cms_us::eesimulator::params::eesimulator_enable,
)
inherits mobi_cms_us::eesimulator::params {
    include mobi_cms_us::eesimulator::install, mobi_cms_us::eesimulator::config, mobi_cms_us::eesimulator::service
    anchor { "mobi_cms_us::eesimulator::begin":} -> Class["mobi_cms_us::eesimulator::install"] ->
    Class["mobi_cms_us::eesimulator::config"] ->
    Class["mobi_cms_us::eesimulator::service"] -> anchor { "mobi_cms_us::eesimulator::end": } ->
    os::motd::register { $name : }
}
