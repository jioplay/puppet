class mobi_cms_us::jufmp::install {

    package { "mobi-arlanda-fragfileprocessor-${mobi_cms_us::jufmp::fragfileprocessor_version}" :
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    package { "mobi-fragwriter-${mobi_cms_us::jufmp::fragwriter_version}" :
        ensure => present,
    }

    package { "mobi-mfe-${mobi_cms_us::jufmp::mfe_version}" :
        ensure => present,
    }

}
