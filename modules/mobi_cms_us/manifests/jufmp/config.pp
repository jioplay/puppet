class mobi_cms_us::jufmp::config {

    file { "/opt/arlanda/conf/fragfileprocessor/fragfileprocessor.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_cms_us/jufmp_files/fragfileprocessor.properties",
        notify   => Class["tomcat::service"],
    }

}
