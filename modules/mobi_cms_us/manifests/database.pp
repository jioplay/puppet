# == Class: mobi_cms_us::database
#
#  installs mgo database
#
# === Parameters:
#
#   $database_root_password::
#   $dam_username::
#   $dam_password::
#   $wfm_username::
#   $wfm_password::
#   $mp_username::
#   $mp_password::
#   $ing_username::
#   $ing_password::
#   $fm_username::
#   $fm_password::
#   $activemq_username::
#   $activemq_password::
#   $carrier::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_cms_us::database" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::database (
    $database_root_password = $mobi_cms_us::database::params::database_root_password,
    $dam_username = $mobi_cms_us::database::params::dam_username,
    $dam_password = $mobi_cms_us::database::params::dam_password,
    $wfm_username = $mobi_cms_us::database::params::wfm_username,
    $wfm_password = $mobi_cms_us::database::params::wfm_password,
    $mp_username = $mobi_cms_us::database::params::mp_username,
    $mp_password = $mobi_cms_us::database::params::mp_password,
    $ing_username = $mobi_cms_us::database::params::ing_username,
    $ing_password = $mobi_cms_us::database::params::ing_password,
    $fm_username = $mobi_cms_us::database::params::fm_username,
    $fm_password = $mobi_cms_us::database::params::fm_password,
    $program_scheduler_username = $mobi_cms_us::database::params::program_scheduler_username,
    $program_scheduler_password = $mobi_cms_us::database::params::program_scheduler_password,
    $activemq_username = $mobi_cms_us::database::params::activemq_username,
    $activemq_password = $mobi_cms_us::database::params::activemq_password,
    $carrier = $mobi_cms_us::database::params::carrier,
)
  inherits mobi_cms_us::database::params {
    include mobi_cms_us::database::install
    anchor { "mobi_cms_us::database::begin":} -> Class["mobi_cms_us::database::install"]
    Class["mobi_cms_us::database::install"] -> anchor { "mobi_cms_us::database::end": }
    os::motd::register { $name : }
}
