# == Class: mobi_cms_us::juing
#
#  installs juing component
#
# === Parameters:
#
#    $ingestor_version::
#    $searchagent_version::
#    $ftplogparser_version::
#    $vsftpd_version::
#    $arlandasearch_version::
#    $pam_version::
#    $ing_username::
#    $ing_password::
#    $wfm_username::
#    $wfm_password::
#    $ftplogparser_mobi2_username::
#    $ftplogparser_mobi2_password::
#    $searchagent_update_legacy_solr::
#
# === Requires:
#
#     java
#     tomcat
#     group::arlanda
#     user::apache
#     user::rtv
#     mobi_cms_us::filesystem or nf mounts setup
#
# === Sample Usage
#
#  class { "mobi_cms_us::juing" :
#           ingestor_version => "5.0.0-179808",
#           searchagent_version => "5.0.0-181163",
#           mgo_cms_postprocessor_version => "5.0.0-183268",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::juing (
    $ingestor_version = $mobi_cms_us::juing::params::ingestor_version,
    $searchagent_version = $mobi_cms_us::juing::params::searchagent_version,
    $ftplogparser_version = $mobi_cms_us::juing::params::ftplogparser_version,
    $vsftpd_version = $mobi_cms_us::juing::params::vsftpd_version,
    $pam_version = $mobi_cms_us::juing::params::pam_version,
    $arlandasearch_version = $mobi_cms_us::juing::params::arlandasearch_version,
    $ftplogparser_ensure = $mobi_cms_us::juing::params::ftplogparser_ensure,
    $ftplogparser_enable = $mobi_cms_us::juing::params::ftplogparser_enable,
    $vsftpd_ensure = $mobi_cms_us::juing::params::vsftpd_ensure,
    $vsftpd_enable = $mobi_cms_us::juing::params::vsftpd_enable,
    $ftplogparser_mobi2_username = $mobi_cms_us::juing::params::ftplogparser_mobi2_username,
    $ftplogparser_mobi2_password = $mobi_cms_us::juing::params::ftplogparser_mobi2_password,
    $ing_username = $mobi_cms_us::juing::params::ing_username,
    $ing_password = $mobi_cms_us::juing::params::ing_password,
    $wfm_username = $mobi_cms_us::juing::params::wfm_username,
    $wfm_password = $mobi_cms_us::juing::params::wfm_password,
    $widevine_url_base = $mobi_cms_us::juing::params::widevine_url_base,
    $mgo_ftp_hostname = $mobi_cms_us::juing::params::mgo_ftp_hostname,
    $mgo_ftp_port = $mobi_cms_us::juing::params::mgo_ftp_port,
    $mgo_ftp_username = $mobi_cms_us::juing::params::mgo_ftp_username,
    $mgo_ftp_password = $mobi_cms_us::juing::params::mgo_ftp_password,
    $mgo_ftp_pathname = $mobi_cms_us::juing::params::mgo_ftp_pathname,
    $sftp_host = $mobi_cms_us::juing::params::sftp_host,
    $sftp_port = $mobi_cms_us::juing::params::sftp_port,
    $sftp_user = $mobi_cms_us::juing::params::sftp_user,
    $sftp_password = $mobi_cms_us::juing::params::sftp_password,
    $searchagent_update_legacy_solr = $mobi_cms_us::juing::params::searchagent_update_legacy_solr,
    $hibernate_database_dialect = $mobi_cms_us::juing::params::hibernate_database_dialect,
    $hibernate_database_vendor = $mobi_cms_us::juing::params::hibernate_database_vendor,
    $jdbc_driver = $mobi_cms_us::juing::params::jdbc_driver,
    $ingestor_ing_db_jdbc_url = $mobi_cms_us::juing::params::ingestor_ing_db_jdbc_url,
    $ingestor_mobi2_db_jdbc_url = $mobi_cms_us::juing::params::ingestor_mobi2_db_jdbc_url,
    $ingestor_aspera_db_jdbc_url = $mobi_cms_us::juing::params::ingestor_aspera_db_jdbc_url,
)
inherits mobi_cms_us::juing::params {
    include mobi_cms_us::juing::install, mobi_cms_us::juing::config, mobi_cms_us::tomcat_config, mobi_cms_us::juing::service
    anchor { "mobi_cms_us::juing::begin":} -> Class["mobi_cms_us::juing::install"]
    Class["mobi_cms_us::juing::config"] -> Class["mobi_cms_us::tomcat_config"] ->
    anchor { "mobi_cms_us::juing::end": } -> os::motd::register { $name : }
}
