# == Class: mobi_cms_us::jufmp
#
#  installs jufmp component
#
# === Parameters:
#
#    $fragfileprocessor_version::
#    $fragwriter_version::
#    $mfe_version::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_cms_us::jufmp" :
#           fragfileprocessor_version => "5.0.0-174523",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::jufmp (
    $dam_version = $mobi_cms_us::jufmp::params::dam_version,
    $wfm_version = $mobi_cms_us::jufmp::params::wfm_version,
    $mp_version = $mobi_cms_us::jufmp::params::mp_version,
    $fragfileprocessor_version = $mobi_cms_us::jufmp::params::fragfileprocessor_version,
    $fragwriter_version = $mobi_cms_us::jufmp::params::fragwriter_version,
    $mfe_version = $mobi_cms_us::jufmp::params::mfe_version,
)
inherits mobi_cms_us::jufmp::params {
    include mobi_cms_us::jufmp::install
    include mobi_cms_us::jufmp::config
    anchor { "mobi_cms_us::jufmp::begin": } -> Class["mobi_cms_us::jufmp::install"] ->
    Class["mobi_cms_us::jufmp::config"] -> anchor { "mobi_cms_us::jufmp::end" :}
    os::motd::register { $name : }
}
