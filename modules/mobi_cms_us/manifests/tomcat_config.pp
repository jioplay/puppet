# == Class: mobi_cms_us::tomcat_config
#
#  installs tomcat override configuration files
#
# === Parameters:
#
# === Requires:
#
# === Sample Usage
#
#  class { "mobi_cms_us::tomcat_config": }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::tomcat_config (
    $tomcat_umask = "002",
) {
    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/setenv.d/tomcat.override.090.umask.sh":
      owner  => "rtv",
      group  => "rtv",
      mode   => "0775",
      content =>template('mobi_cms_us/tomcat_config/tomcat.override.090.umask.sh.erb'),
      notify   => Class["tomcat::service"],
    }

}
