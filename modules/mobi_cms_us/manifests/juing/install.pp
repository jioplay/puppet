class mobi_cms_us::juing::install {

  class {"apache":
    package => "httpd.x86_64",
  }

  class {"php": }

  package {"php-soap-5.1.6":
    ensure => present,
    notify => Class["apache::service"],
  }

  package { "mobi-arlanda-searchagent-${mobi_cms_us::juing::searchagent_version}" :
    ensure => present,
    notify => Class["tomcat::service"],
  }

  package { "mobi-arlanda-search-${mobi_cms_us::juing::arlandasearch_version}" :
    ensure => present,
    notify => Class["tomcat::service"],
  }

  package { "mobi-arlanda-ftplogparser-${mobi_cms_us::juing::ftplogparser_version}":
    ensure => present,
    notify   => Service["ftplogparser"],
  }

  package { "mobi-arlanda-ingestor-${mobi_cms_us::juing::ingestor_version}":
    ensure => present,
    notify => Class["tomcat::service"],
  }

  package {"mobi-vsftpd-${mobi_cms_us::juing::vsftpd_version}":
    ensure => present,
    notify => Service["vsftpd"],
  }

  package {"mobi-arlanda-pam-${mobi_cms_us::juing::pam_version}":
    ensure => present,
  }
}
