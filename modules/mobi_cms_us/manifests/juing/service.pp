class mobi_cms_us::juing::service {
    service { "ftplogparser":
        ensure => $mobi_cms_us::juing::ftplogparser_ensure,
        enable => $mobi_cms_us::juing::ftplogparser_enable,
        require => [ Class["mobi_cms_us::juing::install"], Class["mobi_cms_us::juing::config"], Service["vsftpd"], ],
    }

    service { "vsftpd":
        ensure => $mobi_cms_us::juing::vsftpd_ensure,
        enable => $mobi_cms_us::juing::vsftpd_enable,
        require => [ Class["mobi_cms_us::juing::install"], Class["mobi_cms_us::juing::config"] ],
    }
}
