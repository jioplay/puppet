class mobi_cms_us::juing::params {
    $ingestor_version = "5.0.0-192632"
    $searchagent_version = "5.0.0-211674"
    $ftplogparser_version = "5.0.0-210408"
    $vsftpd_version = "2.0.5"
    $arlandasearch_version = "5.0.0-208190"
    $pam_version = "1.0.1-213697"

    $ing_username = "ing_user" #
    $ing_password = "ing_user" #
    $wfm_username = "wfm_user" #
    $wfm_password = "wfm_user" #

    $ftplogparser_ensure = "running"
    $ftplogparser_enable = true

    $vsftpd_ensure = "running"
    $vsftpd_enable = true

    $ftplogparser_mobi2_username = "ing_user"
    $ftplogparser_mobi2_password = "ing_user"

    $widevine_url_base = "http://media.v1.mgo.com/widevine"
    $mgo_ftp_hostname = "replace_me"
    $mgo_ftp_port = "21"
    $mgo_ftp_username = "replace_me"
    $mgo_ftp_password = "replace_me"
    $mgo_ftp_pathname = "."

    $sftp_host = "inftp01p4.smf2.mobitv"
    $sftp_port = "20022"
    $sftp_user = "mobi_cms_us"
    $sftp_password = "e0rtv12"

    $searchagent_update_legacy_solr = "false"

    # Global database params
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $hibernate_database_vendor = "MYSQL"
    $jdbc_driver = "com.mysql.jdbc.Driver"

    $ingestor_ing_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/ing?autoReconnect=true"
    $ingestor_mobi2_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/mobi2lite?autoReconnect=true"
    $ingestor_aspera_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/aspera_stats_collector?autoReconnect=true"
}

