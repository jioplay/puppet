class mobi_cms_us::juing::config {

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/ingestor.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juing/ingestor-tomcat-context.xml.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    # Database persistence files
    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/legacy-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juing/ingestor-legacy-persistence.xml.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/ingestor-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juing/ingestor-ing-persistence.xml.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/aspera-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juing/ingestor-aspera-persistence.xml.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    # Customized Quartz Spring configs
    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/spring/quartzContext.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_cms_us/juing_files/ingestor-quartzContext.xml",
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/arlanda/conf/ingestor/ingestor.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juing/ingestor.properties.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/arlanda/conf/ftplogparser/ftplogparser.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juing/ftplogparser.properties.erb'),
        require => Class["juing::install"],
        notify   => Service["ftplogparser"],
    }

    file { "/opt/arlanda/conf/searchagent/searchagent.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juing/searchagent.properties.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/var/Jukebox/uploads/search_transform/search_default_transformation.xslt":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        require => Class["juing::install"],
        source => "puppet:///modules/mobi_cms_us/juing_files/search_default_transformation.xslt",
    }

    #Create symbolic link only if the file is currently absent
    exec { "Create mobi_test search transform XSLT link":
        command => "/bin/ln -s /var/Jukebox/uploads/search_transform/search_default_transformation.xslt /var/Jukebox/uploads/search_transform/2.xslt",
        onlyif => "/usr/bin/test ! -e /var/Jukebox/uploads/search_transform/2.xslt",
        logoutput => "true",
    }

     exec { "Create infotel search transform XSLT link":
        command => "/bin/ln -s /var/Jukebox/uploads/search_transform/search_default_transformation.xslt /var/Jukebox/uploads/search_transform/3.xslt",
        onlyif => "/usr/bin/test ! -e /var/Jukebox/uploads/search_transform/3.xslt",
        logoutput => "true",
    }

    exec { "Create yume search transform XSLT link":
        command => "/bin/ln -s /var/Jukebox/uploads/search_transform/search_default_transformation.xslt /var/Jukebox/uploads/search_transform/4.xslt",
        onlyif => "/usr/bin/test ! -e /var/Jukebox/uploads/search_transform/4.xslt",
        logoutput => "true",
    }

    file { "/usr/local/bin/create_provider_account.php":
        replace => true,
        owner  => "rtv",
        group  => "arlanda",
        mode => "0440",
        content => template('mobi_cms_us/juing/create_provider_account.php.erb'),
    }

    file { "/usr/local/bin/retranscode.php":
        replace => true,
        owner  => "rtv",
        group  => "arlanda",
        mode => "0440",
        content => template('mobi_cms_us/juing/retranscode.php.erb'),
    }
}
