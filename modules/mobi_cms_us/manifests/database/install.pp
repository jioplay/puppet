class mobi_cms_us::database::install {
    $carrier = $mobi_cms_us::database::carrier

    file { "/var/mobi-cms":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi-cms/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        require => File["/var/mobi-cms"],
    }

    file { "/var/mobi-cms/database/${carrier}":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_cms_us/database/${carrier}",
        require => File["/var/mobi-cms/database"],
    }

    file { "/var/mobi-cms/database/install.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_cms_us/database/install.sh.erb'),
        require => File["/var/mobi-cms/database/${carrier}"],
    }

    file { "/var/mobi-cms/database/uninstall.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_cms_us/database/uninstall.sh.erb'),
        require => File["/var/mobi-cms/database/install.sh"],
    }

    file { "/var/mobi-cms/database/create_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_cms_us/database/create_databases.sql.erb'),
        require => File["/var/mobi-cms/database/uninstall.sh"],
    }

    file { "/var/mobi-cms/database/delete_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_cms_us/database/delete_databases.sql.erb'),
        require => File["/var/mobi-cms/database/create_databases.sql"],
    }

    #test to see if we have been run
    exec { "Install DB":
        command => "/bin/sh /var/mobi-cms/database/install.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi-cms/database/installation.done",
        logoutput => "true",
        require => File["/var/mobi-cms/database/delete_databases.sql"],
    }

}
