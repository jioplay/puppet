# == Class: mobi_cms_us::nfs::nfs_stub
#
#  exports /opt/nfs as an nfs share. This is for use in test environments only, where we do not have access to the SAN
#
# === Parameters:
#
# === Requires:
#
# === Sample Usage
#
#  class { "mobi_cms_us::nfs::nfs_stub" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_cms_us::nfs::nfs_stub () {

# I am not sure what the appropriate permissions are here, so I'm making them very open for now. This
# is intended for dev environments only.
  file {
    "/opt/nfs" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/uploads" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/uploads/search_transform" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/vantage" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/logs" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/transcode" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/transcode/ee" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/transcode/ee/simulator" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/transcode/ee/simulator/Input" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/transcode/ee/simulator/Output" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/vod" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/vod/encodings-list" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mediasrc" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mediasrc/uploads" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mediasrc/uploads/originals" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mediasrc/uploads/search_transform" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mediasrc/uploads/metadata" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mediasrc/tmp" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mediasrc/extfeeds" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mediasrc/ftpupload" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/ftpupload" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/recordings" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  file {
    "/opt/nfs/mp" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
      require => File["/opt/nfs"],
  }
  file {
    "/opt/nfs/mp/metadata-repository" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
      require => File["/opt/nfs/mp"],
  }
  file {
    "/opt/nfs/mp/setting-repository" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
      require => File["/opt/nfs/mp"],
  }
  file {
    "/opt/nfs/appdata" :
      ensure => directory,
      owner => "rtv",
      group => "rtv",
      mode => "0777",
  }
  class {
    "nfs" :
      client_ensure => false,
      client_enable => false,
      server_ensure => true,
      server_enable => true,
      server_exports => ["/opt/nfs",],
      nfsutil_ensure => latest,
      autofs_ensure => latest,
      portmap_ensure => latest,
      require => File["/opt/nfs"],
  }
}
