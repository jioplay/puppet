class mobi_cms_us::juarn::config {

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/dam.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content =>template('mobi_cms_us/juarn/dam-tomcat-context.xml.erb'),
        require => Class["mobi_cms_us::juarn::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/wfm.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/wfm-tomcat-context.xml.erb'),
        require => Class["mobi_cms_us::juarn::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/mp.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/mp-tomcat-context.xml.erb'),
        require => Class["mobi_cms_us::juarn::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/foldermanager.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/foldermanager-tomcat-context.xml.erb'),
        require => Class["mobi_cms_us::juarn::install"],
        notify   => Class["tomcat::service"],
    }

    # Database persistence files
    file { "/opt/mobi-arlanda-mp/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/mp-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_cms_us::juarn::install"],
    }


    file { "/opt/mobi-arlanda-dam/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/dam-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_cms_us::juarn::install"],
    }

    file { "/opt/mobi-arlanda-wfm/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/wfm-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_cms_us::juarn::install"],
    }

    file { "/opt/mobi-arlanda-fm/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/folder-manager-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_cms_us::juarn::install"],
    }


    # Customized Quartz Spring configs
    file { "/opt/mobi-arlanda-wfm/webapp/WEB-INF/classes/spring/quartzContext.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_cms_us/juarn_files/wfm-quartzContext.xml",
        notify   => Class["tomcat::service"],
        require => Class["mobi_cms_us::juarn::install"],
    }

    file { "/opt/arlanda/conf/foldermanager/foldermanager.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/foldermanager.properties.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_cms_us::juarn::install"],
    }

    file { "/opt/arlanda/conf/wfm/wfm.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_cms_us/juarn/wfm.properties.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_cms_us::juarn::install"],
    }

}
