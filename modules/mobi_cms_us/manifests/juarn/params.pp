class mobi_cms_us::juarn::params {
    $dam_version = "5.0.0-183115"
    $wfm_version = "5.0.0-189083"
    $mp_version = "5.0.0-187724"
    $fm_version = "5.0.0-210960"

    $mediainfo_version = "0.7.58" #
    $libmediainfo_version = "0.7.58" #
    $libzen_version = "0.4.26" #

    $dam_username = "dam_user"
    $dam_password = "dam_user"

    $wfm_username = "wfm_user"
    $wfm_password = "wfm_user"

    $mp_username = "mp_user"
    $mp_password = "mp_user"

    $fm_username = "fm_user"
    $fm_password = "fm_user"

    # Global database params
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $hibernate_database_vendor = "MYSQL"
    $jdbc_driver = "com.mysql.jdbc.Driver"

    $fm_oracle_native_queries_enabled = "false"
    $dam_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/dam?autoReconnect=true"
    $wfm_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/wfm?autoReconnect=true"
    $mp_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/mp?autoReconnect=true"
    $fm_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/mobi2lite?autoReconnect=true"
}
