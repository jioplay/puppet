
class mobi_cms_us::juarn::install {

    package { "mobi-arlanda-dam-${mobi_cms_us::juarn::dam_version}":
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    package { "mobi-arlanda-wfm-${mobi_cms_us::juarn::wfm_version}" :
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    package { "mobi-arlanda-mp-${mobi_cms_us::juarn::mp_version}" :
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    package { "mobi-arlanda-fm-${mobi_cms_us::juarn::fm_version}" :
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    # mediainfo and its dependencies required by WFM for
    # M_Go source_content_driven transcoding profiles
    package { "mediainfo-${mobi_cms_us::juarn::mediainfo_version}":
      ensure => present,
      provider => yum,
    }

    package { "libmediainfo0-${mobi_cms_us::juarn::libmediainfo_version}":
      ensure => present,
      provider => yum,
    }

    package { "libzen0-${mobi_cms_us::juarn::libzen_version}":
      ensure => present,
      provider => yum,
    }
}
