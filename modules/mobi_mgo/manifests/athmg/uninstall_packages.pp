define mobi_mgo::athmg::uninstall_packages () {
    package { "mobi-aaa-rights-manager":
        ensure => absent,
    }
    package { "mobi-account-management" :
        ensure => absent,
    }
    package { "mobi-aaa-stream-manager" :
        ensure => absent,
    }
}
