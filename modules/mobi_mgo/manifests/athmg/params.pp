class mobi_mgo::athmg::params {
    $rights_manager_ver = undef
    $stream_manager_ver = undef
    $account_management_ver = undef
    $database_user = undef
    $database_url = undef
    $database_passwd = undef
    $upgrade = true
    $memcached_servers = undef
    $auth_connections_per_host = undef
    $auth_connections_max = undef
}
