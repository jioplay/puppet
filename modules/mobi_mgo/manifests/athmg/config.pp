class mobi_mgo::athmg::config
{

    #$database_host = $mobi_mgo::athmg::database_host
    #$database_port = $mobi_mgo::athmg::database_port
    $database_user = $mobi_mgo::athmg::database_user
    $database_passwd = $mobi_mgo::athmg::database_passwd
    #$database_sid = $mobi_mgo::athmg::database_sid
    $upgrade = $mobi_mgo::athmg::upgrade
    $memcached_servers = $mobi_mgo::athmg::memcached_servers
    $auth_connections_per_host = $mobi_mgo::athmg::auth_connections_per_host
    $auth_connections_max = $mobi_mgo::athmg::auth_connections_max

    if($upgrade){
      file { "/opt/mobi-account-management/config/mobi-account-management.properties":
        ensure => file,
        content => template("mobi_mgo/athmg_templates/acctmgmt/mobi-account-management.properties.erb"),
        notify => Class["tomcat::service"],
        require => Class["mobi_mgo::athmg::install"],
      }

      file { "/opt/mobi-aaa/config/stream-manager/applicationProperties.xml":
        ensure => file,
        content => template("mobi_mgo/athmg_templates/stream/applicationProperties.xml.erb"),
        notify => Class["tomcat::service"],
        require => Class["mobi_mgo::athmg::install"],
      }
    }
    #rollback
    else{
      file { "/opt/mobi-account-management/config/mobi-account-management.properties":
        ensure => file,
        source => "puppet:///modules/mobi_mgo/athmg_files/acctmgmt/mobi-account-management.properties.bak",
        notify => Class["tomcat::service"],
        require => Class["mobi_mgo::athmg::install"],
      }

      file { "/opt/mobi-aaa/config/stream-manager/applicationProperties.xml":
        ensure => file,
        source => "puppet:///modules/mobi_mgo/athmg_files/stream/applicationProperties.xml.bak",
        notify => Class["tomcat::service"],
        require => Class["mobi_mgo::athmg::install"],
      }
    }
}
