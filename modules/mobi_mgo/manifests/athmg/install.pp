class mobi_mgo::athmg::install {
  if ($mobi_mgo::athmg::upgrade){
    mobi_mgo::athmg::install_packages { "mobi_mgo::athmg::install_upgrade":}
  }
  else{
    mobi_mgo::athmg::uninstall_packages { "mobi_mgo::athmg::uninstall":}
    ->
    mobi_mgo::athmg::install_packages { "mobi_mgo::athmg::install":}
  }

}
