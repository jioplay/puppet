define mobi_mgo::athmg::install_packages () {
  if ($mobi_mgo::athmg::account_management_ver != undef){
      package { "mobi-account-management-${mobi_mgo::athmg::account_management_ver}" :
      ensure => present,
      notify => Class["tomcat::service"]
    }
  }
  else{
    package { "mobi-account-management" :
      ensure => present,
      notify => Class["tomcat::service"]
    }
  }
  if ($mobi_mgo::athmg::rights_manager_ver != undef){
    package { "mobi-aaa-rights-manager-${mobi_mgo::athmg::rights_manager_ver}" :
      ensure => present,
      notify => Class["tomcat::service"]
    }
  }
  else{
    package { "mobi-aaa-rights-manager" :
      ensure => present,
      notify => Class["tomcat::service"]
    }
  }
  if ($mobi_mgo::athmg::stream_manager_ver != undef){
    package { "mobi-aaa-stream-manager-${mobi_mgo::athmg::stream_manager_ver}":
      ensure => present,
      notify => Class["tomcat::service"]
    }
  }
  else{
    package { "mobi-aaa-stream-manager":
      ensure => present,
      notify => Class["tomcat::service"]
    }
  }
}
