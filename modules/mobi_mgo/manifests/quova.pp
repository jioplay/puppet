# == Class: mobi_mgo::quova
#
#  installs quova component
#
# === Parameters:
#
#    $quova_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#  class { "mobi_mgo::quova" :
#           version => "5.0.0-179808",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::quova (
    $version = $mobi_mgo::quova::params::version,
    $quova_licence_key = $mobi_mgo::quova::params::quova_licence_key,
)
inherits mobi_mgo::quova::params {
    include mobi_mgo::quova::install, mobi_mgo::quova::config
    anchor { "mobi_mgo::quova::begin":} ->
    Class["mobi_mgo::quova::install"]->
    Class["mobi_mgo::quova::config"] ->
    anchor { "mobi_mgo::quova::end": }
    os::motd::register { "mobi-mgo::quova": }
}
