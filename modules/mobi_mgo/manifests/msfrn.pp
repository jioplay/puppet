# == Class: mobi_mgo::msfrn
#
#  installs msfrn component
#
# === Parameters:
#
#    $account_manager_mgo_ver
#    $identity_manager_oauth2_mgo_ver
#    $account_manager_ng_ver
#    $stream_manager_ng_ver
#    $location_ver
#    $frontend_ng_ver
#    $foursports_frontend_ver
#    $frontend_mgo_ver
#    $vwm_rules
#    $url_template_files_list
#
# === Requires:
#
#
# === Sample Usage
#
#   class { "mobi_mgo::msfrn" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::msfrn (
    $account_manager_mgo_ver = $mobi_mgo::msfrn::params::account_manager_mgo_ver,
    $identity_manager_oauth2_mgo_ver = $mobi_mgo::msfrn::params::identity_manager_oauth2_mgo_ver,
    $account_management_ver = $mobi_mgo::msfrn::params::account_manager_ng_ver,
    $stream_manager_ng_ver = $mobi_mgo::msfrn::params::stream_manager_ng_ver,
    $location_ver = $mobi_mgo::msfrn::params::location_ver,
    $frontend_ng_ver =  $mobi_mgo::msfrn::params::frontend_ng_ver,
    $foursports_frontend_ver = $mobi_mgo::msfrn::params::foursports_frontend_ver,
    $frontend_mgo_ver = $mobi_mgo::msfrn::params::frontend_mgo_ver,
    $vwm_rules = $mobi_mgo::msfrn::params::vwm_rules,
    $url_template_files_list = $mobi_mgo::msfrn::params::url_template_files_list,
)
inherits mobi_mgo::msfrn::params {
    include mobi_mgo::msfrn::install, mobi_mgo::msfrn::config
    anchor { "mobi_mgo::msfrn::begin": } ->
    Class["mobi_mgo::msfrn::install"] ->
    Class["mobi_mgo::msfrn::config"] ->
    anchor { "mobi_mgo::msfrn::end" :}
    os::motd::register { $name : }
}
