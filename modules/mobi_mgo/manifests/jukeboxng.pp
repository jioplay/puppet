# == Class: mobi_mgo::jukeboxng
#
#  installs jukeboxng component
#
# === Parameters:
#
#    version::   version of the package
#    propfile::  properties file to use
#
# === Requires:
#
#     httpd
#
# === Sample Usage
#
#  class { "mobi_mgo::jukeboxng" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::jukeboxng (
    $php_version = $mobi_mgo::jukeboxng::params::php_version,
    $cms_ui_version = $mobi_mgo::jukeboxng::params::cms_ui_version,
    $ext_version = $mobi_mgo::jukeboxng::params::ext_version,
    $apr_version = $mobi_mgo::jukeboxng::params::apr_version,
    $httpd_version = $mobi_mgo::jukeboxng::params::httpd_version,
    $postgresql_libs_version = $mobi_mgo::jukeboxng::params::postgresql_libs_version,
    $gmp_version = $mobi_mgo::jukeboxng::params::gmp_version,
    $libxslt_version = $mobi_mgo::jukeboxng::params::libxslt_version,
    $ruby_rpm_url = $mobi_mgo::jukeboxng::params::ruby_rpm_url,
    $ruby_version = $mobi_mgo::jukeboxng::params::ruby_version,
    $rubygems_rpm_url = $mobi_mgo::jukeboxng::params::rubygems_rpm_url,
    $rubygems_version = $mobi_mgo::jukeboxng::params::rubygems_version,
    $cms_ui_qaconf_rpm_url = $mobi_mgo::jukeboxng::params::cms_ui_qaconf_rpm_url,
    $cms_ui_qaconf_version = $mobi_mgo::jukeboxng::params::cms_ui_qaconf_version,
    $rtv_version = $mobi_mgo::jukeboxng::params::rtv_version,
)
inherits mobi_mgo::jukeboxng::params {

    include mobi_mgo::jukeboxng::install, mobi_mgo::jukeboxng::config

    anchor { "mobi_mgo::jukeboxng::begin": } ->
    Class["mobi_mgo::jukeboxng::install"] ->
    Class["mobi_mgo::jukeboxng::config"] ->
    anchor { "mobi_mgo::jukeboxng::end": }
    os::motd::register { $name : }
}

