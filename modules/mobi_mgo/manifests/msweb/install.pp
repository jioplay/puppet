class mobi_mgo::msweb::install {
 if ($mobi_mgo::msweb::upgrade){
    mobi_mgo::msweb::install_packages { "mobi_mgo::msweb::install_upgrade":}
 }
 else{
   mobi_mgo::msweb::uninstall_packages { "mobi_mgo::msweb::uninstall":}
   ->
   mobi_mgo::msweb::install_packages { "mobi_mgo::msweb::install":}
 }
}
