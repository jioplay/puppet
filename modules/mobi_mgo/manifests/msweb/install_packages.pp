define mobi_mgo::msweb::install_packages () {
 if ($mobi_mgo::msweb::version == undef){
        package { "mgo-html-webkit":
          ensure => present,
          notify   => Class["apache::service"],
        }
 }
 else{
        package { "mgo-html-webkit-${::mobi_mgo::msweb::version}":
          ensure => present,
          notify   => Class["apache::service"],
        }
 }
}
