class mobi_mgo::msweb::config {
  $version = $::mobi_mgo::msweb::version
  $proxy_pass_list = $::mobi_mgo::msweb::proxy_pass_list
  file{ '/etc/httpd/conf.d/mgo-ios.conf':
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    content => template("mobi_mgo/msweb/mgo-ios.conf.erb"),
    require => Class["msweb::install"],
    notify  => Class["apache::service"],
  }


  #if you don't specify host entry in node manifest, default is undef, it shouldn't change the entry
  if ($mobi_mgo::msweb::app_host_and_protocol != undef) {
    file{'/tmp/app_host_config.sh':
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      require => Class["msweb::install"],
      content => template("mobi_mgo/msweb/app_host_config.erb"),
    }
    ->
    exec{"/tmp/app_host_config.sh":
      path      => ['usr/local/bin','opt/local/bin','/usr/bin','/usr/sbin','/bin','/sbin'],
      unless    => "/bin/bash -c \" find /var/www  -name 'data.js' | xargs -I {} fgrep -i app_host {} | grep -o ${mobi_mgo::msweb::app_host_and_protocol}\"",
    }
  }

  #if you don't specify host entry in node manifest, default is undef, it shouldn't change the entry
  if ($mobi_mgo::msweb::cdn_thumbnail_endpoint != undef) {
    file{'/tmp/cdn_thumbnail_endpoint.sh':
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      require => Class["msweb::install"],
      content => template("mobi_mgo/msweb/cdn_thumbnail_endpoint.erb"),
    }
    ->
    exec{"/tmp/cdn_thumbnail_endpoint.sh":
      path      => ['usr/local/bin','opt/local/bin','/usr/bin','/usr/sbin','/bin','/sbin'],
      unless    => "/bin/bash -c \" find /var/www  -name 'data.js' | xargs -I {} fgrep -i cdn_thumbnail_endpoint {} | grep -o ${mobi_mgo::msweb::cdn_thumbnail_endpoint}\"",
    }
  }


  if ($mobi_mgo::msweb::version != undef) {
      $tmparr = split($::mobi_mgo::msweb::version,'-')
      $split_filename = split($tmparr[0],'[.]')
      $file_version = inline_template("<%= @split_filename.join('') %>")
      $file = "/var/www/html/mgo/${file_version}/index.html"
      if ( $::mobi_mgo::msweb::web_id != undef   )
      {
           file_line{ "search_replace":
             path  => $file,
             line  => " var web_id = '${::mobi_mgo::msweb::web_id}';",
             match => "^\s?var\s+web_id\s?=\s?'.*';\s?$",
           }
      }
  }
  file { "/var/www/html/UVConnect.html":
    ensure   => present,
    source   => "puppet:///modules/mobi_mgo/msweb_files/UVConnect.html",
    notify   => Class["apache::service"],
  }
  #TO DO, HOW DO I GET THE VERSION NUMBER IF ITS NOT SPECIFIED? FORCE USER TO SPECIFY VERSION NUMBER
  if ($mobi_mgo::msweb::xap_file != undef) {
    file{'/tmp/xap_file_line_add.sh':
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      require => Class["msweb::install"],
      content => template("mobi_mgo/msweb/xap_file_line_add.erb"),
    }
    ->
    exec{"/tmp/xap_file_line_add.sh":
      path      => ['usr/local/bin','opt/local/bin','/usr/bin','/usr/sbin','/bin','/sbin'],
      unless    => "/bin/bash -c \" find /var/www  -name 'data.js' | xargs -I {} fgrep -i xap_file {} | grep -o ${mobi_mgo::msweb::xap_file}\"",
    }
  }



}
