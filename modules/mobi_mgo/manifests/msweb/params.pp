class mobi_mgo::msweb::params {
    $version = undef
    $proxy_pass_list = []
    $app_host_and_protocol = undef
    $filename = undef
    $web_id = undef
    $upgrade = true
    $cdn_thumbnail_endpoint = undef
    $xap_file = undef
}
