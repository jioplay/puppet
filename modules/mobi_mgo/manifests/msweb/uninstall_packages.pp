define mobi_mgo::msweb::uninstall_packages () {
    package { "mgo-html-webkit":
      ensure => purged,
    }
}
