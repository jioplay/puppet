class mobi_mgo::jukeboxng::config {

    include apache

    file { "/opt/mobi-cms-provider-ui/config/initializers/jukebox_config.rb":
      replace => true,
      owner  => "rtv",
      group  => "rtv",
      mode => "0444",
      source => "puppet:///modules/mobi_mgo/jukeboxng_files/jukebox_config.rb",
      notify   => Class["apache::service"],
    }

}
