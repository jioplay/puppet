class mobi_mgo::jukeboxng::install {

    include apache

    # RPMs for the Jukebox NG program are based on what's specified in
    # http://jira.mobitv.corp/browse/PCC-6704

    package {"libxslt-${mobi_mgo::jukeboxng::libxslt_version}":
      ensure => present,
      provider => yum,
    }

    package {"apr-${mobi_mgo::jukeboxng::apr_version}":
      ensure => present,
      provider => yum,
    }

    package {"postgresql-libs-${mobi_mgo::jukeboxng::postgresql_libs_version}":
      ensure => present,
      provider => yum,
    }

    package {"apr-util-${mobi_mgo::jukeboxng::apr_version}":
      ensure => present,
      provider => yum,
    }

    package {"php53-common-${mobi_mgo::jukeboxng::php_version}":
      ensure => present,
      provider => yum,
    }

    package {"gmp-${mobi_mgo::jukeboxng::gmp_version}":
      ensure => present,
      provider => yum,
    }

    package {"php53-cli-${mobi_mgo::jukeboxng::php_version}":
      ensure => present,
      provider => yum,
    }

    package {"php53-gd-${mobi_mgo::jukeboxng::php_version}":
      ensure => present,
      provider => yum,
    }

    package {"php53-${mobi_mgo::jukeboxng::php_version}":
      ensure => present,
      provider => yum,
    }

    exec { "Ruby":
      command => "/bin/rpm -i ${mobi_mgo::jukeboxng::ruby_rpm_url}",
      onlyif => "/usr/bin/test `/bin/rpm -qa ruby | /bin/grep ruby-${mobi_mgo::jukeboxng::ruby_version} | wc -l` -eq 0",
      notify => Class["apache::service"],
    }

    exec { "Ruby Gems":
      command => "/bin/rpm -i ${mobi_mgo::jukeboxng::rubygems_rpm_url}",
      onlyif => "/usr/bin/test `/bin/rpm -qa rubygems | /bin/grep rubygems-${mobi_mgo::jukeboxng::rubygems_version} | wc -l` -eq 0",
      notify => Class["apache::service"],
    }

    package {"mobi-user-rtv-${mobi_mgo::jukeboxng::rtv_version}":
      ensure => present,
      provider => yum,
    }

    package {"cms_ui-${mobi_mgo::jukeboxng::cms_ui_version}":
      ensure => present,
      provider => yum,
      notify => Class["apache::service"],
    }

    package {"ext-${mobi_mgo::jukeboxng::ext_version}":
      ensure => present,
      provider => yum,
      notify => Class["apache::service"],
    }

    exec { "CMS UI QA Config":
      command => "/bin/rpm -i --force ${mobi_mgo::jukeboxng::cms_ui_qaconf_rpm_url}",
      onlyif => "/usr/bin/test `/bin/rpm -qa cms_ui_qaconf | /bin/grep cms_ui_qaconf-${mobi_mgo::jukeboxng::cms_ui_qaconf_version} | wc -l` -eq 0",
      notify => Class["apache::service"],
    }

    Package["mobi-user-rtv-${mobi_mgo::jukeboxng::rtv_version}"] ->
    Package["libxslt-${mobi_mgo::jukeboxng::libxslt_version}"] ->
    Package["apr-${mobi_mgo::jukeboxng::apr_version}"] ->
    Package["postgresql-libs-${mobi_mgo::jukeboxng::postgresql_libs_version}"] ->
    Package["apr-util-${mobi_mgo::jukeboxng::apr_version}"] ->
    Package["php53-common-${mobi_mgo::jukeboxng::php_version}"] ->
    Package["gmp-${mobi_mgo::jukeboxng::gmp_version}"] ->
    Package["php53-cli-${mobi_mgo::jukeboxng::php_version}"] ->
    Package["php53-gd-${mobi_mgo::jukeboxng::php_version}"] ->
    Package["php53-${mobi_mgo::jukeboxng::php_version}"] ->
    Exec["Ruby"] ->
    Exec["Ruby Gems"] ->
    Package["cms_ui-${mobi_mgo::jukeboxng::cms_ui_version}"] ->
    Package["ext-${mobi_mgo::jukeboxng::ext_version}"] ->
    Exec["CMS UI QA Config"]
}
