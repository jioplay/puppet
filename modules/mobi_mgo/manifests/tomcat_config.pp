# == Class: mobi_mgo::tomcat_config
#
#  installs tomcat override configuration files
#
# === Parameters:
#
# === Requires:
#
# === Sample Usage
#
#  class { "mobi_mgo::tomcat_config": }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::tomcat_config {

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/setenv.d/tomcat.override.090.umask.sh":
      owner  => "rtv",
      group  => "rtv",
      mode   => "0775",
      source => "puppet:///modules/mobi_mgo/tomcat_config/tomcat.override.090.umask.sh",
      notify   => Class["tomcat::service"],
    }

}
