class mobi_mgo::database::install {

    file { "/var/mobi-mgo":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi-mgo/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_mgo/database",
        require => File["/var/mobi-mgo"],
    }

    file { "/var/mobi-mgo/database/install.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_mgo/database/install.sh.erb'),
        require => File["/var/mobi-mgo"],
    }

    file { "/var/mobi-mgo/database/uninstall.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_mgo/database/uninstall.sh.erb'),
        require => File["/var/mobi-mgo"],
    }

    file { "/var/mobi-mgo/database/mgo_create_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_mgo/database/mgo_create_databases.sql.erb'),
        require => File["/var/mobi-mgo"],
    }

    file { "/var/mobi-mgo/database/mgo_delete_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_mgo/database/mgo_delete_databases.sql.erb'),
        require => File["/var/mobi-mgo"],
    }

    #test to see if we have been run
    exec { "Install DB":
        command => "/bin/sh /var/mobi-mgo/database/install.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi-mgo/database/installation.done",
        logoutput => "true",
        require => File["/var/mobi-mgo/database"],
    }

}
