class mobi_mgo::quova::install {
    package { "quova-geo-directory-server-${mobi_mgo::quova::version}":
        ensure => present,
        notify => Class["tomcat::service"],
    }
}
