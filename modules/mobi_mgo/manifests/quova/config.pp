class mobi_mgo::quova::config {
    $quova_licence_key = $mobi_mgo::quova::quova_licence_key
    if ($version){
      $version = $mobi_mgo::quova::version
      $tmparr = split($version,'-')
      $folder_version = $tmparr[0]
      if ($quova_licence_key){
          $filename = "${quova_licence_key}_${::hostname}.txt"
          file_line{ "search_replace":
            path  => "/opt/mgo/GeoDirectoryServer-${folder_version}/quova/conf/GeoDirectoryServer.properties",
            line  => "com.quova.gds.core.licenseFile=\${com.quova.gds.Home}/quova/conf/${filename}",
            match => "^\s?com\.quova\.gds\.core\.licenseFile.*\s?$",
         }
         ->
         file { "/opt/mgo/GeoDirectoryServer-${folder_version}/quova/conf/${filename}":
             ensure => present,
             owner  => "rtv",
             group  => "rtv",
             mode   => "0444",
             notify => Class["tomcat::service"],
             source => "puppet:///modules/mobi_mgo/quova_files/${filename}",
         }

      }
    }
}
