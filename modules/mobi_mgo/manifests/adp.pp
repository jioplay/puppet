# == Class: mobi_mgo::adp
#
#  installs adp component
#
# === Parameters:
#
#    $account_manager_ver::
#    $identity_manager_oauth2_ver::
#    $uvconnect_auth,
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_mgo::adp" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::adp (
    $rights_manager_ver = $mobi_mgo::adp::params::rights_manager_ver,
    $account_manager_ver = $mobi_mgo::adp::params::account_manager_ver,
    $identity_manager_oauth2_ver = $mobi_mgo::adp::params::identity_manager_oauth2_ver ,
    $content_device_manager_ver = $mobi_mgo::adp::params::content_device_manager_ver,
    $stream_manager_ver = $mobi_mgo::adp::params::stream_manager_ver,
    $database_url = $mobi_mgo::adp::params::database_url,
    $database_username = $mobi_mgo::adp::params::database_username,
    $database_password = $mobi_mgo::adp::params::database_password,
    $memcached_servers = $mobi_mgo::adp::params::memcached_servers,
    $policy_server = $mobi_mgo::adp::params::policy_server,
    $policy_port = $mobi_mgo::adp::params::policy_port,
    $account_url = $mobi_mgo::adp::params::account_url,
    $oauth_url = $mobi_mgo::adp::params::oauth_url,
    $technicolor_url = $mobi_mgo::adp::params::technicolor_url,
    $household_url = $mobi_mgo::adp::params::household_url,
    $jersey_client_logging = $mobi_mgo::adp::params::jersey_client_logging,
    $uvconnect_auth = $mobi_mgo::adp::params::uvconnect_auth,
    $uvconnect_create_url = $mobi_mgo::adp::params::uvconnect_create_url,
    $uvconnect_delete_url = $mobi_mgo::adp::params::uvconnect_delete_url,
    $account_manager_host = $mobi_mgo::adp::params::account_manager_host,
    $stream_manager_host = $mobi_mgo::adp::params::stream_manager_host,
    $upgrade = $mobi_mgo::adp::params::upgrade,
    $access_token_secs = $mobi_mgo::adp::params::access_token_secs,
    $refresh_token_secs = $mobi_mgo::adp::params::refresh_token_secs,
    $token_verified_secs = $mobi_mgo::adp::params::token_verified_secs,
    $auth_connections_per_host = $mobi_mgo::adp::params::auth_connections_per_host,
    $auth_connections_max = $mobi_mgo::adp::params::auth_connections_max,
    $uv_connections_per_host = $mobi_mgo::adp::params::uv_connections_per_host,
    $uv_connections_max = $mobi_mgo::adp::params::uv_connections_max,
)
inherits mobi_mgo::adp::params {
    include mobi_mgo::adp::install, mobi_mgo::adp::config
    anchor { "mobi_mgo::adp::begin": } ->
    Class["mobi_mgo::adp::install"] ->
    Class["mobi_mgo::adp::config"] ->
    anchor { "mobi_mgo::adp::end" :}
    os::motd::register { $name : }
}
