class mobi_mgo::maxmind::install {
    package { "mobi-location-maxmind-server-${mobi_mgo::maxmind::version}":
        ensure => present,
        notify => Class["tomcat::service"],
    }
}
