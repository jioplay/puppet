# == Class: mobi_mgo::asperaingestor
#
#  installs asperaingestor component
#
# === Parameters:
#
#    $enterprise_version::
#    $asperaingestor_version::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_mgo::asperaingestor" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::asperaingestor (
    $enterprise_version = $mobi_mgo::asperaingestor::params::enterprise_version,
    $asperaingestor_version = $mobi_mgo::asperaingestor::params::asperaingestor_version,
)
inherits mobi_mgo::asperaingestor::params {
    include mobi_mgo::asperaingestor::install, mobi_mgo::asperaingestor::config, mobi_mgo::asperaingestor::service
    anchor { "mobi_mgo::asperaingestor::begin": } -> Class["mobi_mgo::asperaingestor::install"] -> Class["mobi_mgo::asperaingestor::service"]
    Class["mobi_mgo::asperaingestor::config"] -> anchor { "mobi_mgo::asperaingestor::end" :}
    os::motd::register { $name : }
}
