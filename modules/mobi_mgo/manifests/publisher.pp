# == Class: mobi_mgo::publisher
#
# installs publisher framework and selected transactions
# for each transaction, the caller must provide the RPM version and host enpoint list.
#
# === Parameters:
#
#
# === Requires
#
#    java
#    python
#
# === Sample Usage
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::publisher (
    $pub_txn_configmgr_ng_endpointlist = $mobi_mgo::publisher::params::pub_txn_configmgr_ng_endpointlist,
    $pub_txn_configrepo_endpointlist = $mobi_mgo::publisher::params::pub_txn_configrepo_endpointlist,
    $pub_txn_devdetect_ng_endpointlist = $mobi_mgo::publisher::params::pub_txn_devdetect_ng_endpointlist,
    $pub_txn_freq_ng_endpointlist = $mobi_mgo::publisher::params::pub_txn_freq_ng_endpointlist,
    $pub_txn_icon_ng_endpointlist = $mobi_mgo::publisher::params::pub_txn_icon_ng_endpointlist,
    $pub_txn_nightly_ng_endpointlist = $mobi_mgo::publisher::params::pub_txn_nightly_ng_endpointlist,
    $pub_txn_staticres_ng_endpointlist = $mobi_mgo::publisher::params::pub_txn_staticres_ng_endpointlist,
    $pub_txn_tiles_ng_endpointlist = $mobi_mgo::publisher::params::pub_txn_tiles_ng_endpointlist,
    $pub_txn_widgets_ng_endpointlist = $mobi_mgo::publisher::params::pub_txn_widgets_ng_endpointlist,
    $db_host = $mobi_mgo::publisher::params::db_host,
    $db_port = $mobi_mgo::publisher::params::db_port,
    $db_user = $mobi_mgo::publisher::params::db_user,
    $db_passwd = $mobi_mgo::publisher::params::db_passwd,
    $db_sid = $mobi_mgo::publisher::params::db_sid,
    $node_guide_publisher_ver = $mobi_mgo::publisher::params::node_guide_publisher_ver,
    $neon_ver = $mobi_mgo::publisher::params::neon_ver,
)
inherits mobi_mgo::publisher::params {
    include mobi_mgo::publisher::install, mobi_mgo::publisher::config
    anchor { "mobi_mgo::publisher::begin": } ->
    Class["mobi_mgo::publisher::install"] ->
    Class["mobi_mgo::publisher::config"] -> anchor { "mobi_mgo::publisher::end": }
    os::motd::register { $name : }
}
