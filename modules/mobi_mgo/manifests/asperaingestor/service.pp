class mobi_mgo::asperaingestor::service {
    service { "asperacentral":
        ensure => stopped,
        enable => false,
    }

    service { "asperahttpd":
        ensure => stopped,
        enable => false,
    }
}
