class mobi_mgo::asperaingestor::config {
    file { "/opt/aspera/var/run/aspera":
      owner => rtv,
      group => arlanda,
      mode => "0775",
      require => Class["asperaingestor::install"],
    }

    file { "/opt/aspera/etc/aspera-license":
      replace => true,
      source => ["puppet:///modules/mobi_mgo/aspera_files/aspera-license.${hostname}","puppet:///modules/mobi_mgo/aspera_files/aspera-license",],
      require => Class["asperaingestor::install"],
    }

    file { "/opt/aspera/etc/aspera.conf":
      replace => true,
      source => "puppet:///modules/mobi_mgo/aspera_files/aspera.conf",
      require => Class["asperaingestor::install"],
    }
}
