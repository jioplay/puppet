class mobi_mgo::asperaingestor::install {

    package { "aspera-entsrv-$mobi_mgo::asperaingestor::enterprise_version":
        ensure => present,
    }

    package { "mobi-arlanda-asperaingestor-$mobi_mgo::asperaingestor::asperaingestor_version":
      ensure => present,
      notify   => Class["tomcat::service"],
    }

    package { "mediainfo-${mobi_mgo::asperaingestor::mediainfo_version}":
      ensure => present,
      provider => yum,
    }

    package { "libmediainfo0-${mobi_mgo::asperaingestor::libmediainfo_version}":
      ensure => present,
      provider => yum,
    }

    package { "libzen0-${mobi_mgo::asperaingestor::libzen_version}":
      ensure => present,
      provider => yum,
    }
}
