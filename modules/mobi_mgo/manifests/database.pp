# == Class: mobi_mgo::database
#
#  installs mgo database
#
# === Parameters:
#
#   $database_root_password::
#   $dam_username::
#   $dam_password::
#   $wfm_username::
#   $wfm_password::
#   $mp_username::
#   $mp_password::
#   $ing_username::
#   $ing_password::
#   $fm_username::
#   $fm_password::
#   $activemq_username::
#   $activemq_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_mgo::database" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::database (
    $database_root_password = $mobi_mgo::database::params::database_root_password,
    $dam_username = $mobi_mgo::database::params::dam_username,
    $dam_password = $mobi_mgo::database::params::dam_password,
    $wfm_username = $mobi_mgo::database::params::wfm_username,
    $wfm_password = $mobi_mgo::database::params::wfm_password,
    $mp_username = $mobi_mgo::database::params::mp_username,
    $mp_password = $mobi_mgo::database::params::mp_password,
    $ing_username = $mobi_mgo::database::params::ing_username,
    $ing_password = $mobi_mgo::database::params::ing_password,
    $fm_username = $mobi_mgo::database::params::fm_username,
    $fm_password = $mobi_mgo::database::params::fm_password,
    $activemq_username = $mobi_mgo::database::params::activemq_username,
    $activemq_password = $mobi_mgo::database::params::activemq_password,
)
  inherits mobi_mgo::database::params {
    include mobi_mgo::database::install
    anchor { "mobi_mgo::database::begin":} -> Class["mobi_mgo::database::install"]
    Class["mobi_mgo::database::install"] -> anchor { "mobi_mgo::database::end": }
    os::motd::register { $name : }
}
