# == Class: mobi_mgo::msweb
#
#  installs msweb component
#
# === Parameters:
#
#    $version
#    $proxy_pass_list
#    $app_host_and_protocol
#    $filename
#    $web_id
#    $upgrade
#    $xap_file
#
# === Requires:
#
#     apache
#
#
# === Sample Usage
#
#   class { "mobi_mgo::msweb" :
#           version         => "1.5.2-218594",
#           proxy_pass_list => [
#             "ProxyPass /catalog    http://core.mgoint-navimnt.smf2.mobitv:8080",
#             "ProxyPass /uv         http://core.mgoint-navimnt.smf2.mobitv:8080/uv",
#           ],
#           upgrade => true,
#           web_id  => "UA-36041408-1",
#           require => Class["apache"],
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::msweb (
    $version = $mobi_mgo::msweb::params::version,
    $proxy_pass_list = $mobi_mgo::msweb::params::proxy_pass_list,
    $app_host_and_protocol = $mobi_mgo::msweb::params::app_host_and_protocol,
    $cdn_thumbnail_endpoint = $mobi_mgo::msweb::params::cdn_thumbnail_endpoint,
    $filename = $mobi_mgo::msweb::params::filename,
    $web_id = $mobi_mgo::msweb::params::web_id,
    $upgrade = $mobi_mgo::msweb::params::upgrade,
    $xap_file = $mobi_mgo::msweb::params::xap_file,
)
inherits mobi_mgo::msweb::params {
    include mobi_mgo::msweb::install,mobi_mgo::msweb::config
    anchor { "mobi_mgo::msweb::begin": } ->
    Class["mobi_mgo::msweb::install"]->
    Class["mobi_mgo::msweb::config"] ->
    anchor { "mobi_mgo::msweb::end" :}
    os::motd::register { $name :}
}
