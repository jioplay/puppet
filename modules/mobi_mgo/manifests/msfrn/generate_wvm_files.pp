define mobi_mgo::msfrn::generate_wvm_files() {
  $base_path = "/var/www/publisher/configrepo/current/domain/urltemplate/template/core/v4/media/technicolor/mgo/5.0"

  $kv = split($name, "->")
  $kv[0] = regsubst($kv[0], '([\s]+)', '', 'G')
  $kv[1] = regsubst($kv[1], '([\s]+)', '', 'G')
  $sub_file_path = split($kv[0],'urltemplate.txt')
  notify{"${base_path}/${sub_file_path}":}

  notify{"key-${kv[0]} and val-${kv[1]}":}

  exec{"makedirectory_p ${title}":
    #create onlf if dir does not exist
    creates => "${base_path}/${sub_file_path}",
    command => "mkdir -p ${base_path}/${sub_file_path}",
    path    => ['/bin', '/sbin','/usr/local/bin', '/usr/sbin','/usr/bin'],
    require => Class["msfrn::install"],
  }
  ->
  file {"${base_path}/${kv[0]}":
      ensure   => present,
      owner    => "root",
      group    => "root",
      mode     => "0644",
      content  => "http://${kv[1]}/widevine/{RESOLUTION}/{MID}.wvm",
      require  => [Class["msfrn::install"]],
      notify   => Class["apache::service"],
      #content => template("mobi_mgo/msfrn/mgo-ios.conf.erb"),
  }
}
#foo { $test_arr: }
#$test_arr = ["key1 -> val1", "key2 -> val2"]
