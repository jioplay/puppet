class mobi_mgo::msfrn::install {
      package { "mobi-node-frontend-ng-${mobi_mgo::msfrn::frontend_ng_ver}":
        ensure  => present,
        notify  => Class["apache::service"],
      }
      ->
      package { "mobi-node-4sports-frontend-${mobi_mgo::msfrn::foursports_frontend_ver}":
        ensure => present,
        notify => Class["apache::service"],
      }
      ->
      package { "mobi-node-frontend-mgo-${mobi_mgo::msfrn::frontend_mgo_ver}":
        ensure => present,
        notify => Class["apache::service"],
      }

      if ($mobi_mgo::msfrn::account_manager_mgo_ver == undef){
        package { "mobi-restapi-core-v5-account-manager-mgo":
          ensure => present,
          notify => Class["apache::service"],
        }
      }
      else{
        package { "mobi-restapi-core-v5-account-manager-mgo-${mobi_mgo::msfrn::account_manager_mgo_ver}":
          ensure => present,
          notify => Class["apache::service"],
        }
      }

      if ($mobi_mgo::msfrn::account_management_ver == undef){
        package { "mobi-restapi-core-v5-account-management":
          ensure => present,
          notify => Class["apache::service"],
        }
      }
      else{
        package { "mobi-restapi-core-v5-account-management-${mobi_mgo::msfrn::account_management_ver}":
          ensure => present,
          notify => Class["apache::service"],
        }
      }
}
