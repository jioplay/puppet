class mobi_mgo::msfrn::config {
  file{['/var/www/publisher/configrepo',
        '/var/www/publisher/configrepo/current',
        '/var/www/publisher/configrepo/current/domain',
        '/var/www/publisher/configrepo/current/domain/urltemplate',
        '/var/www/publisher/configrepo/current/domain/urltemplate/rule',
        '/var/www/publisher/configrepo/current/domain/urltemplate/rule/vod']:
    ensure  => directory,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => Class["mobi_mgo::msfrn::install"],
  }

  file{['/var/www/publisher/configrepo/current/domain/urltemplate/template',
  '/var/www/publisher/configrepo/current/domain/urltemplate/template/core',
  '/var/www/publisher/configrepo/current/domain/urltemplate/template/core/v4',
  '/var/www/publisher/configrepo/current/domain/urltemplate/template/core/v4/media',
  '/var/www/publisher/configrepo/current/domain/urltemplate/template/core/v4/media/technicolor',
  '/var/www/publisher/configrepo/current/domain/urltemplate/template/core/v4/media/technicolor/mgo',  '/var/www/publisher/configrepo/current/domain/urltemplate/template/core/v4/media/technicolor/mgo/5.0']:
    ensure  => directory,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => [Class["mobi_mgo::msfrn::install"], File["/var/www/publisher/configrepo/current/domain/urltemplate"]],
  }
  ->
  mobi_mgo::msfrn::generate_wvm_files {$::mobi_mgo::msfrn::url_template_files_list:}

  file{ '/var/www/publisher/configrepo/current/domain/urltemplate/rule/vod/WVM.rules':
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    content => template("mobi_mgo/msfrn/rules.erb"),
    require => [Class["mobi_mgo::msfrn::install"],File["/var/www/publisher/configrepo/current/domain/urltemplate/rule/vod"]],
    notify => Class["apache::service"],
  }
}
