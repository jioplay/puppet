class mobi_mgo::msfrn::params {
    $account_manager_mgo_ver = undef
    $identity_manager_oauth2_mgo_ver = "5.0.0-187474"
    $account_management_ver = undef
    $stream_manager_ng_ver = "5.0.0-187474"
    $location_ver = "4.6.60-138473"
    $frontend_ng_ver = "5.0.0-187528"
    $foursports_frontend_ver = "4.6.60-138938"
    $frontend_mgo_ver = "5.0.0-187528"
    $url_template_files_list = []
    $vwm_rules = []
}
