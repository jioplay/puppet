# == Class: mobi_mgo::juarn
#
#  installs juarn component
#
# === Parameters:
#
#    $wfm_version::
#    $mp_version::
#    $fm_version::
#    $dam_version::
#    $dam_username::
#    $dam_password::
#    $wfm_username::
#    $wfm_password::
#    $mp_username::
#    $mp_password::
#    $fm_username::
#    $fm_password::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_mgo::juarn" :
#           dam_version => "5.0.0-178058",
#           wfm_version => "5.0.0-180924",
#           mp_version => "5.0.0-180894",
#           fm_version => "5.0.0-179993",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::juarn (
    $dam_version = $mobi_mgo::juarn::params::dam_version,
    $wfm_version = $mobi_mgo::juarn::params::wfm_version,
    $mp_version = $mobi_mgo::juarn::params::mp_version,
    $fm_version = $mobi_mgo::juarn::params::fm_version,
    $dam_username = $mobi_mgo::juarn::params::dam_username,
    $dam_password = $mobi_mgo::juarn::params::dam_password,
    $wfm_username = $mobi_mgo::juarn::params::wfm_username,
    $wfm_password = $mobi_mgo::juarn::params::wfm_password,
    $mp_username = $mobi_mgo::juarn::params::mp_username,
    $mp_password = $mobi_mgo::juarn::params::mp_password,
    $fm_username = $mobi_mgo::juarn::params::fm_username,
    $fm_password = $mobi_mgo::juarn::params::fm_password,)

inherits mobi_mgo::juarn::params {
    include mobi_mgo::juarn::install, mobi_mgo::juarn::config, mobi_mgo::tomcat_config
    anchor { "mobi_mgo::juarn::begin": } -> Class["mobi_mgo::juarn::install"] ->
    Class["mobi_mgo::juarn::config"] -> Class["mobi_mgo::tomcat_config"] ->
    anchor { "mobi_mgo::juarn::end" :} -> os::motd::register { $name : }
}
