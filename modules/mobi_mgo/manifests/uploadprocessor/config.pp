class mobi_mgo::uploadprocessor::config {

    file { "/opt/arlanda/conf/mgo-upload-processor/mgo-upload-processor.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_mgo/uploadprocessor/mgo-upload-processor.properties.erb'),
        require => Class["uploadprocessor::install"],
        notify   => Class["tomcat::service"],
    }
}
