class mobi_mgo::uploadprocessor::install {
    package { "mobi-mgo-upload-processor-${mobi_mgo::uploadprocessor::version}":
        ensure => present,
        notify   => Class["tomcat::service"],
    }
}
