class mobi_mgo::wvss::config {
    file { "/etc/httpd/conf.d/wvss.conf":
      replace => true,
      source  => "puppet:///modules/mobi_mgo/wvss_files/wvss.conf",
      notify  => Class["apache::service"],
      require => Class["apache::install"],
    }
}
