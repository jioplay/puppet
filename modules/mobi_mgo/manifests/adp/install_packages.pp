define mobi_mgo::adp::install_packages () {
 package { "mobi-aaa-adapter-account-manager-mgo-${mobi_mgo::adp::account_manager_ver}" :
       ensure => present,
       notify => Class["tomcat::service"]
 }
 package { "mobi-aaa-adapter-rights-manager-mgo-${mobi_mgo::adp::rights_manager_ver}" :
       ensure => present,
       notify => Class["tomcat::service"]
 }
 package { "mobi-aaa-identity-manager-oauth2-mgo-${mobi_mgo::adp::identity_manager_oauth2_ver}":
       ensure => present,
       notify => Class["tomcat::service"]
 }
 package { "mobi-aaa-mgo-content-device-manager-${mobi_mgo::adp::content_device_manager_ver}":
       ensure => present,
       notify => Class["tomcat::service"]
 }
 package { "mobi-aaa-adapter-mgo-stream-manager-${mobi_mgo::adp::stream_manager_ver}":
       ensure => present,
       notify => Class["tomcat::service"]
 }
}
