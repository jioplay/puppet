class mobi_mgo::adp::install {
 if ($mobi_mgo::adp::upgrade){
    mobi_mgo::adp::install_packages { "mobi_mgo::adp::install_upgrade":}
 }
 else{
    mobi_mgo::adp::uninstall_packages { "mobi_mgo::adp::uninstall":}
    ->
    mobi_mgo::adp::install_packages { "mobi_mgo::adp::install":}
 }
}
