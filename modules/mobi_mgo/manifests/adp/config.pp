class mobi_mgo::adp::config {

    $database_url = $mobi_mgo::adp::database_url
    $database_username = $mobi_mgo::adp::database_username
    $database_password = $mobi_mgo::adp::database_password
    $memcached_servers = $mobi_mgo::adp::memcached_servers
    $policy_server = $mobi_mgo::adp::policy_server
    $policy_port = $mobi_mgo::adp::policy_port
    $account_url = $mobi_mgo::adp::account_url
    $oauth_url = $mobi_mgo::adp::oauth_url
    $technicolor_url = $mobi_mgo::adp::technicolor_url
    $household_url = $mobi_mgo::adp::household_url
    $jersey_client_logging = $mobi_mgo::adp::jersey_client_logging
    $uvconnect_auth = $mobi_mgo::adp::uvconnect_auth
    $uvconnect_create_url = $mobi_mgo::adp::uvconnect_create_url
    $uvconnect_delete_url = $mobi_mgo::adp::uvconnect_delete_url
    $account_manager_host = $mobi_mgo::adp::account_manager_host
    $stream_manager_host = $mobi_mgo::adp::stream_manager_host
    $upgrade = $mobi_mgo::adp::upgrade
    $access_token_secs = $mobi_mgo::adp::access_token_secs
    $refresh_token_secs = $mobi_mgo::adp::refresh_token_secs
    $token_verified_secs = $mobi_mgo::adp::token_verified_secs
    $auth_connections_per_host = $mobi_mgo::adp::auth_connections_per_host
    $auth_connections_max = $mobi_mgo::adp::auth_connections_max
    $uv_connections_per_host = $mobi_mgo::adp::uv_connections_per_host
    $uv_connections_max = $mobi_mgo::adp::uv_connections_max
    #notify{$access_token_secs:}
    #notify{$refresh_token_secs:}
    #notify{$token_verified_secs:}
    if ($upgrade){
      file { "/opt/mobi-aaa/config/content-device-manager/applicationProperties.xml":
           ensure  => file,
           content => template("mobi_mgo/adp_templates/cdc/applicationProperties.xml.erb"),
           notify  => Class["tomcat::service"],
           require => Class["mobi_mgo::adp::install"],
        }

        file { "/opt/mobi-aaa/config/adapter-mgo-stream-manager/applicationProperties.xml":
           ensure => file,
           content => template("mobi_mgo/adp_templates/stream/applicationProperties.xml.erb"),
           notify => Class["tomcat::service"],
           require => Class["mobi_mgo::adp::install"],
        }

        file { "/opt/mobi-aaa/config/mgo-account-manager/applicationProperties.xml":
           ensure => file,
           content => template("mobi_mgo/adp_templates/acctmgmt/applicationProperties.xml.erb"),
           notify => Class["tomcat::service"],
           require => Class["mobi_mgo::adp::install"],
        }

        file { "/opt/mobi-aaa/config/mgo-identity-manager-oauth2/application-config-override.properties":
           ensure => file,
           content => template("mobi_mgo/adp_templates/idmgr/applicationProperties.xml.erb"),
           notify => Class["tomcat::service"],
           require => Class["mobi_mgo::adp::install"],
        }
    }
    #rollback
    else{
      file { "/opt/mobi-aaa/config/content-device-manager/applicationProperties.xml":
           ensure  => file,
           source  => "puppet:///modules/mobi_mgo/adp_files/cdc/applicationProperties.xml.bak",
           notify  => Class["tomcat::service"],
           require => Class["mobi_mgo::adp::install"],
        }

        file { "/opt/mobi-aaa/config/adapter-mgo-stream-manager/applicationProperties.xml":
           ensure => file,
           source  => "puppet:///modules/mobi_mgo/adp_files/stream/applicationProperties.xml.bak",
           notify => Class["tomcat::service"],
           require => Class["mobi_mgo::adp::install"],
        }

        file { "/opt/mobi-aaa/config/mgo-account-manager/applicationProperties.xml":
           ensure => file,
           source  => "puppet:///modules/mobi_mgo/adp_files/acctmgmt/applicationProperties.xml.bak",
           notify => Class["tomcat::service"],
           require => Class["mobi_mgo::adp::install"],
        }

        file { "/opt/mobi-aaa/config/mgo-identity-manager-oauth2/application-config-override.properties":
           ensure => file,
           source  => "puppet:///modules/mobi_mgo/adp_files/idmgr/applicationProperties.xml.bak",
           notify => Class["tomcat::service"],
           require => Class["mobi_mgo::adp::install"],
        }
   }
}
