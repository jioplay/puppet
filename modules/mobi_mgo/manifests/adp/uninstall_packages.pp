define mobi_mgo::adp::uninstall_packages () {
  package { "mobi-aaa-adapter-account-manager-mgo" :
    ensure  => absent,
  }
  package { "mobi-aaa-adapter-rights-manager-mgo" :
    ensure  => absent,
 }

 package { "mobi-aaa-identity-manager-oauth2-mgo":
   ensure  => absent,
 }

 package { "mobi-aaa-mgo-content-device-manager":
   ensure  => absent,
 }
 package { "mobi-aaa-adapter-mgo-stream-manager":
   ensure => absent,
 }
}
