class mobi_mgo::juarn::config {

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/dam.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content =>template('mobi_mgo/juarn/dam-tomcat-context.xml.erb'),
        require => Class["mobi_mgo::juarn::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/wfm.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_mgo/juarn/wfm-tomcat-context.xml.erb'),
        require => Class["mobi_mgo::juarn::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/mp.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_mgo/juarn/mp-tomcat-context.xml.erb'),
        require => Class["mobi_mgo::juarn::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/foldermanager.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_mgo/juarn/foldermanager-tomcat-context.xml.erb'),
        require => Class["mobi_mgo::juarn::install"],
        notify   => Class["tomcat::service"],
    }

    # Database persistence files
    file { "/opt/mobi-arlanda-mp/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juarn_files/mp-persistence.xml",
        notify   => Class["tomcat::service"],
        require => Class["mobi_mgo::juarn::install"],
    }


    file { "/opt/mobi-arlanda-dam/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juarn_files/dam-persistence.xml",
        notify   => Class["tomcat::service"],
        require => Class["mobi_mgo::juarn::install"],
    }

    file { "/opt/mobi-arlanda-wfm/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juarn_files/wfm-persistence.xml",
        notify   => Class["tomcat::service"],
        require => Class["mobi_mgo::juarn::install"],
    }

    file { "/opt/mobi-arlanda-fm/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juarn_files/folder-manager-persistence.xml",
        notify   => Class["tomcat::service"],
        require => Class["mobi_mgo::juarn::install"],
    }


    # Customized Quartz Spring configs
    file { "/opt/mobi-arlanda-wfm/webapp/WEB-INF/classes/spring/quartzContext.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juarn_files/wfm-quartzContext.xml",
        notify   => Class["tomcat::service"],
        require => Class["mobi_mgo::juarn::install"],
    }

    file { "/opt/arlanda/conf/foldermanager/foldermanager.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juarn_files/foldermanager.properties",
        notify   => Class["tomcat::service"],
        require => Class["mobi_mgo::juarn::install"],
    }

    file { "/opt/arlanda/conf/wfm/wfm.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juarn_files/wfm.properties",
        notify   => Class["tomcat::service"],
        require => Class["mobi_mgo::juarn::install"],
    }

}
