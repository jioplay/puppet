class mobi_mgo::juarn::params {
    $dam_version = "5.0.0-183115"
    $wfm_version = "5.0.0-189083"
    $mp_version = "5.0.0-187724"
    $fm_version = "*"

    $mediainfo_version = "0.7.58" #
    $libmediainfo_version = "0.7.58" #
    $libzen_version = "0.4.26" #

    $dam_username = "dam_user"
    $dam_password = "dam_user"

    $wfm_username = "wfm_user"
    $wfm_password = "wfm_user"

    $mp_username = "mp_user"
    $mp_password = "mp_user"

    $fm_username = "fm_user"
    $fm_password = "fm_user"
}
