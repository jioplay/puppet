
class mobi_mgo::juarn::install {

    package { "mobi-arlanda-dam-${mobi_mgo::juarn::dam_version}":
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    package { "mobi-arlanda-wfm-${mobi_mgo::juarn::wfm_version}" :
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    package { "mobi-arlanda-mp-${mobi_mgo::juarn::mp_version}" :
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    package { "mobi-arlanda-fm-${mobi_mgo::juarn::fm_version}" :
        ensure => present,
        notify   => Class["tomcat::service"],
    }

    # mediainfo and its dependencies required by WFM for
    # M_Go source_content_driven transcoding profiles
    package { "mediainfo-${mobi_mgo::juarn::mediainfo_version}":
      ensure => present,
      provider => yum,
    }

    package { "libmediainfo0-${mobi_mgo::juarn::libmediainfo_version}":
      ensure => present,
      provider => yum,
    }

    package { "libzen0-${mobi_mgo::juarn::libzen_version}":
      ensure => present,
      provider => yum,
    }
}
