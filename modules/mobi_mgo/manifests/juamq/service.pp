class mobi_mgo::juamq::service {
    service { "activemq":
        ensure => $mobi_mgo::juamq::ensure,
        enable => $mobi_mgo::juamq::enable,
        require => [ Class["mobi_mgo::juamq::install"], Class["mobi_mgo::juamq::config"] ],
    }
}
