class mobi_mgo::juamq::config {

    file { "/opt/activemq/conf/activemq.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_mgo/juamq/activemq.xml.erb'),
        require => Class["mobi_mgo::juamq::install"],
        notify   => Service["activemq"],
    }

    file { "/opt/activemq/lib/optional/mysql-connector-java-5.1.19-bin.jar":
        owner  => "rtv",
        group  => "rtv",
        mode => "0644",
        source => "puppet:///modules/mobi_mgo/juamq_files/mysql-connector-java-5.1.19-bin.jar",
        require => Class["mobi_mgo::juamq::install"],
        notify   => Service["activemq"],
    }

    file { "/opt/activemq/lib/optional/ojdbc-14_g.jar":
        ensure => "absent",
        require => Class["mobi_mgo::juamq::install"],
        notify   => Service["activemq"],
    }
}
