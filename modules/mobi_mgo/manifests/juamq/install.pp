class mobi_mgo::juamq::install {
    package { "mobi-activemq-${version}":
        ensure => present,
        notify   => Service["activemq"],
    }
  }
