# == Class: mobi_mgo::licenseproxy
#
#  installs licenseproxy component
#
# === Parameters:
#
#    $version::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#  class { "mobi_mgo::licenseproxy" :
#           version => "5.0.0-178058",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::licenseproxy (
    $version = $mobi_mgo::licenseproxy::params::version,
    $cdc_url = $mobi_mgo::licenseproxy::params::cdc_url,
    $profile_url = $mobi_mgo::licenseproxy::params::profile_url,
    $auth_oauth2_server_url = $mobi_mgo::licenseproxy::params::auth_oauth2_server_url,
    $auth_rights_server_url = $mobi_mgo::licenseproxy::params::auth_rights_server_url,
    $failopen = $mobi_mgo::licenseproxy::params::failopen,
    $rights_verify = $mobi_mgo::licenseproxy::params::rights_verify,
    $digital_locker_url = $mobi_mgo::licenseproxy::params::digital_locker_url,
    $geo_location_url = $mobi_mgo::licenseproxy::params::geo_location_url,
)
inherits mobi_mgo::licenseproxy::params {
    include mobi_mgo::licenseproxy::install, mobi_mgo::licenseproxy::config
    anchor { "mobi_mgo::licenseproxy::begin": } ->
    Class["mobi_mgo::licenseproxy::install"]->
    Class["mobi_mgo::licenseproxy::config"] ->
    anchor { "mobi_mgo::licenseproxy::end" :}
    os::motd::register { $name : }
}
