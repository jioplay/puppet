# == Class: mobi_mgo::filesystem
#
#  creates directories needed for configuration when nfs mounts not present
#
# === Parameters:
#
# === Requires:
#
# === Sample Usage
#
#  class { "mobi_mgo::filesystem": }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::filesystem {

    file { "/var/www":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/www/html":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/var/Jukebox":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    if (($domain != "qa.dmz") and
        ($hostname != "vmqamgocms") and
        ($hostname != "vmqamgomn") and
        ($hostname != "vmqamgowvss") and
        ($hostname != "vmqamgolucene") and
        ($hostname != "lab126"))
      {
        file { "/var/Jukebox/vod":
          ensure => "directory",
          owner  => "root",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads":
          ensure => "directory",
          owner  => "root",
          group  => "root",
          mode   => "0755",
        }

        file { "/var/Jukebox/uploads/metadata":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/originals":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/search_transform":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/smil":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/thumbnails":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/uploads/xslt":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/local":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/logs":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/logs/contentd":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/monitor":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ftpupload":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0755",
        }

        file { "/var/Jukebox/extfeeds":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "0755",
        }

        file { "/var/Jukebox/tmp":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/cache":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/socks":
          ensure => "directory",
          owner  => "apache",
          group  => "apache",
          mode   => "0755",
        }

        file { "/var/Jukebox/SCL":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/aspera":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/elemental":
          ensure => "directory",
          owner  => "root",
          group  => "root",
          mode   => "0755",
        }


        file { "/var/Jukebox/elemental/Input":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => 777,
        }


        file { "/var/Jukebox/elemental/Output":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => 777,
        }

        file { "/var/Jukebox/widevine":
          ensure => "directory",
          owner  => "root",
          group  => "root",
          mode   => "0755",
        }


        file { "/var/Jukebox/widevine/Input":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => 777,
        }


        file { "/var/Jukebox/widevine/Output":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => 777,
        }

        file { "/var/Jukebox/vod/mgo":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/LOW":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/PD":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/SD":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/HD":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }


        file { "/var/Jukebox/vod/mgo/HD+":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/vod/widevine":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/vod/widevine/LOW":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1101"
        }


        file { "/var/Jukebox/vod/widevine/PD":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1102"
        }


        file { "/var/Jukebox/vod/widevine/SD":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1103"
        }


        file { "/var/Jukebox/vod/widevine/HD":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1104"
        }


        file { "/var/Jukebox/vod/widevine/HD+":
          ensure => link,
          target => "/var/Jukebox/vod/encoding-1105"
        }

        file { "/var/Jukebox/ee":
          ensure => "directory",
          owner  => "root",
          group  => "root",
          mode   => "0755",
        }

        file { "/var/Jukebox/ee/simulator":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/simulator/Input":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/simulator/Output":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/media":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/media/Input":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/media/Output":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/ee/media/Output/mp":
          ensure => "directory",
          owner  => "rtv",
          group  => "arlanda",
          mode   => "0775",
        }

        file { "/var/Jukebox/mp":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/mp/metadata-repository":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
        }

        file { "/var/Jukebox/mp/setting-repository":
          ensure => "directory",
          owner  => "apache",
          group  => "arlanda",
          mode   => "4775",
          recurse => true,
          source => "puppet:///modules/mobi_mgo/settings",
        }
      }

    file { "/opt/ReachTV":
      ensure => "directory",
      owner  => "apache",
      group  => "apache",
      mode   => "0755",
    }

    file { "/opt/ReachTV/Farm":
      ensure => "directory",
      owner  => "rtv",
      group  => "arlanda",
      mode   => "0775",
    }

    file { "/opt/ReachTV/Farm/monitor":
      ensure => "directory",
      owner  => "apache",
      group  => "arlanda",
      mode   => "0775",
    }

    file { "/var/log/vsftpd.log":
      ensure => "present",
      owner  => "root",
      group  => "root",
      mode   => "0666",
    }

    file { "/var/log/ReachTV":
      ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }


    file { "/var/log/ReachTV/Farm":
      ensure => "directory",
      owner  => "root",
      group  => "root",
      mode   => "0755",
    }

    file { "/opt/arlanda/":
        ensure => directory,
    }

    file { "/opt/arlanda/dam/":
        ensure => directory,
        require => File["/opt/arlanda/"],
    }

    file { "/opt/arlanda/dam/monitor":
        ensure => directory,
        require => File["/opt/arlanda/dam/"],
    }


}
