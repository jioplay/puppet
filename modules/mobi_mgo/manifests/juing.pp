# == Class: mobi_mgo::juing
#
#  installs juing component
#
# === Parameters:
#
#    $ingestor_version::
#    $searchagent_version::
#    $mgo_cms_postprocessor_version::
#    $ing_username::
#    $ing_password::
#    $wfm_username::
#    $wfm_password::
#    widevine_url_base::
#    $mgo_ftp_hostname::
#    $mgo_ftp_port::
#    $mgo_ftp_username::
#    $mgo_ftp_password::
#    $mgo_ftp_pathname::
#    $sftp_host::
#    $sftp_port::
#    $sftp_user::
#    $sftp_password::
#
# === Requires:
#
#     java
#     tomcat
#     group::arlanda
#     user::apache
#     user::rtv
#     mobi_mgo::filesystem or nf mounts setup
#
# === Sample Usage
#
#  class { "mobi_mgo::juing" :
#           ingestor_version => "5.0.0-179808",
#           searchagent_version => "5.0.0-181163",
#           mgo_cms_postprocessor_version => "5.0.0-183268",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::juing (
    $ingestor_version = $mobi_mgo::juing::params::ingestor_version,
    $searchagent_version = $mobi_mgo::juing::params::searchagent_version,
    $mgo_cms_postprocessor_version = $mobi_mgo::juing::params::mgo_cms_postprocessor_version,
    $ing_username = $mobi_mgo::juing::params::ing_username,
    $ing_password = $mobi_mgo::juing::params::ing_password,
    $wfm_username = $mobi_mgo::juing::params::wfm_username,
    $wfm_password = $mobi_mgo::juing::params::wfm_password,
    $widevine_url_base = $mobi_mgo::juing::params::widevine_url_base,
    $mgo_ftp_hostname = $mobi_mgo::juing::params::mgo_ftp_hostname,
    $mgo_ftp_port = $mobi_mgo::juing::params::mgo_ftp_port,
    $mgo_ftp_username = $mobi_mgo::juing::params::mgo_ftp_username,
    $mgo_ftp_password = $mobi_mgo::juing::params::mgo_ftp_password,
    $mgo_ftp_pathname = $mobi_mgo::juing::params::mgo_ftp_pathname,
    $sftp_host = $mobi_mgo::juing::params::sftp_host,
    $sftp_port = $mobi_mgo::juing::params::sftp_port,
    $sftp_user = $mobi_mgo::juing::params::sftp_user,
    $sftp_password = $mobi_mgo::juing::params::sftp_password,
)
inherits mobi_mgo::juing::params {
    include mobi_mgo::juing::install, mobi_mgo::juing::config, mobi_mgo::tomcat_config
    anchor { "mobi_mgo::juing::begin":} -> Class["mobi_mgo::juing::install"]
    Class["mobi_mgo::juing::config"] -> Class["mobi_mgo::tomcat_config"] ->
    anchor { "mobi_mgo::juing::end": } -> os::motd::register { $name : }
}
