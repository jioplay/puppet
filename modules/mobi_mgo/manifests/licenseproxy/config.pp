class mobi_mgo::licenseproxy::config {

    file { "/opt/arlanda/conf/licenseproxy/licenseproxy.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_mgo/licenseproxy/licenseproxy.properties.erb'),
        notify   => Class["tomcat::service"],
    }
}
