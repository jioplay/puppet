class mobi_mgo::licenseproxy::params {
    $version = "*"
    $cdc_url = "http://aaa-ng-mgo-cdc-vip:8080/mobi-aaa-mgo-content-device-manager/core/v5/device/technicolor/mgo/5.0/%s/content/stream.json"
    $profile_url = "http://aaa-ng-mgo-idmgr-vip:8080/mobi-aaa-identity-manager-oauth2-mgo/core/v5/session/technicolor/mgo/5.0/current/profile"
    $auth_oauth2_server_url = "http://aaa-ng-mgo-idmgr-vip:8080/mobi-aaa-identity-manager-oauth2-mgo"
    $auth_rights_server_url = "http://aaa-ng-core-rightsmgr-vip:8080/mobi-aaa-rights-manager"
    $failopen = "false"
    $rights_verify = "false"
    $digital_locker_url = "http://cor-ns-mgo-vip.smf2.mobitv:8080/locker/right"
    $geo_location_url = "http://ms4lbsvip.smf1.mobitv:8080/mobi-location/ip_check/technicolor/mgo/5.0"
}
