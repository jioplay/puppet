class mobi_mgo::licenseproxy::install {
    package { "mobi-arlanda-licenseproxy-${mobi_mgo::licenseproxy::version}":
        ensure => present,
        notify   => Class["tomcat::service"],
    }
}
