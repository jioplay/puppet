class mobi_mgo::publisher::params {
    $pub_txn_configmgr_ng_endpointlist=undef
    $pub_txn_configrepo_endpointlist=undef
    $pub_txn_devdetect_ng_endpointlist=undef
    $pub_txn_freq_ng_endpointlist=undef
    $pub_txn_icon_ng_endpointlist=undef
    $pub_txn_nightly_ng_endpointlist=undef
    $pub_txn_staticres_ng_endpointlist=undef
    $pub_txn_tiles_ng_endpointlist=undef
    $pub_txn_widgets_ng_endpointlist=undef
    $db_host = undef
    $db_port = undef
    $db_sid = undef
    $db_user = undef
    $db_passwd = undef
    $node_guide_publisher_ver = "5.0.0-180378"
    $neon_ver = "0.25.5-10.el5_4.1"
}
