define mobi_mgo::publisher::config_helper ( $dirname , $endpoint_list ) {
    $ng_pub_endpoint_list = $endpoint_list

    exec{"makedirectory_p ${dirname}":
      #create onlf if dir does not exist
      creates  => $dirname,
      command  => "mkdir -p ${dirname}",
      path     => ['/bin', '/sbin','/usr/local/bin', '/usr/sbin','/usr/bin'],
      require  => Class["mobi_mgo::publisher::install"],
    }
    ->
    file { "${dirname}/endpoint.host.json":
        ensure  => present,
        owner   => "rtv",
        group   => "rtv",
        mode    => "0644",
        content => template("mobi_mgo/publisher/list.json.erb"),
        require => Class["mobi_mgo::publisher::install"],
    }
}
