class mobi_mgo::publisher::install {
    #This neon package is a dependency for subversion package which is a dependency for one of the publisher packages
    package{"neon-${mobi_mgo::publisher::neon_ver}":
      ensure => present,

    }
    ->
    package { "mobi-node-guide-publisher-ng-${mobi_mgo::publisher::node_guide_publisher_ver}":
        ensure => present,
    }
}
