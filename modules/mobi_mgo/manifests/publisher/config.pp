class mobi_mgo::publisher::config {
    if $mobi_mgo::publisher::pub_txn_configrepo_endpointlist {
        mobi_mgo::publisher::config_helper { "mobi_mgo::publisher::pub_txn_configrepo_endpointlist":
            dirname => "/opt/mobi-publisher/mobi-publisher-transaction-configrepo/config",
            endpoint_list => $mobi_mgo::publisher::pub_txn_configrepo_endpointlist
        }
    }

    if $mobi_mgo::publisher::pub_txn_devdetect_ng_endpointlist {
        mobi_mgo::publisher::config_helper { "mobi_mgo::publisher::pub_txn_devdetect_ng_endpointlist":
            dirname => "/opt/mobi-publisher/mobi-publisher-transaction-devdetect/config",
            endpoint_list => $mobi_mgo::publisher::pub_txn_devdetect_ng_endpointlist
        }
    }

    if $mobi_mgo::publisher::pub_txn_freq_ng_endpointlist {
        mobi_mgo::publisher::config_helper { "mobi_mgo::publisher::pub_txn_freq_ng_endpointlist":
            dirname => "/opt/mobi-publisher/mobi-publisher-transaction-frequent/config",
            endpoint_list => $mobi_mgo::publisher::pub_txn_freq_ng_endpointlist
        }
    }

    if $mobi_mgo::publisher::pub_txn_nightly_ng_endpointlist {
        mobi_mgo::publisher::config_helper { "mobi_mgo::publisher::pub_txn_nightly_ng_endpointlist":
            dirname => "/opt/mobi-publisher/mobi-publisher-transaction-nightly/config",
            endpoint_list => $mobi_mgo::publisher::pub_txn_nightly_ng_endpointlist
        }
    }

    if $mobi_mgo::publisher::pub_txn_staticres_ng_endpointlist {
        mobi_mgo::publisher::config_helper { "mobi_mgo::publisher::pub_txn_staticres_ng_endpointlist":
            dirname => "/opt/mobi-publisher/mobi-publisher-transaction-staticres/config",
            endpoint_list => $mobi_mgo::publisher::pub_txn_staticres_ng_endpointlist
        }
    }

    if $mobi_mgo::publisher::pub_txn_tiles_ng_endpointlist {
        mobi_mgo::publisher::config_helper { "mobi_mgo::publisher::pub_txn_tiles_ng_endpointlist":
            dirname => "/opt/mobi-publisher/mobi-publisher-transaction-tiles/config",
            endpoint_list => $mobi_mgo::publisher::pub_txn_tiles_ng_endpointlist
        }
    }

    if $mobi_mgo::publisher::pub_txn_widgets_ng_endpointlist {
        mobi_mgo::publisher::config_helper { "mobi_mgo::publisher::pub_txn_widgets_ng_endpointlist":
            dirname => "/opt/mobi-publisher/mobi-publisher-transaction-widgets/config",
            endpoint_list => $mobi_mgo::publisher::pub_txn_widgets_ng_endpointlist
        }
    }
    exec{"makedirectory_p":
      #create onlf if dir does not exist
      creates => "/opt/mobi-publisher/mobi-publisher-framework/bin/",
      command => "mkdir -p /opt/mobi-publisher/mobi-publisher-framework/bin/",
      path    => ['/bin', '/sbin','/usr/local/bin', '/usr/sbin','/usr/bin'],
      require => Class["mobi_mgo::publisher::install"],
    }
    ->
    file { "/opt/mobi-publisher/mobi-publisher-framework/bin/application.properties.override":
      ensure  => file,
      content => template("mobi_mgo/publisher/mobi-db-properties.erb"),
      notify  => Class["tomcat::service"],
      require => Class["mobi_mgo::publisher::install"],
    }



}

