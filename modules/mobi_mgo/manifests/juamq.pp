# == Class: mobi_mgo::juamq
#
#  installs juamq component
#
# === Parameters:
#
#     $activemq_ver = "5.5.0.0-170413"
#    $activemq_username::
#    $activemq_password::
#
# === Requires:
#
#     java
#     mobi_mgo::database
#
# === Sample Usage
#
#  class { "mobi_mgo::juamq" :
#          version => "5.5.0.0-170413",
#          ensure => "running",
#          enable => true,
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::juamq (
    $version = $mobi_mgo::juamq::params::version,
    $ensure = $mobi_mgo::juamq::params::ensure,
    $enable = $mobi_mgo::juamq::params::enable,
    $activemq_username = $mobi_mgo::juamq::params::activemq_username,
    $activemq_password = $mobi_mgo::juamq::params::activemq_password,
)
inherits mobi_mgo::juamq::params {
    include mobi_mgo::juamq::install, mobi_mgo::juamq::config, mobi_mgo::juamq::service

    anchor { "mobi_mgo::juamq::begin": } -> Class["mobi_mgo::juamq::install"]
    Class["mobi_mgo::juamq::config"] -> anchor { "mobi_mgo::juamq::end":}
    os::motd::register { $name : }
}
