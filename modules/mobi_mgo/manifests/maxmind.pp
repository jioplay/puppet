# == Class: mobi_mgo::maxmind
#
#  installs maxmind component
#
# === Parameters:
#
#    $maxmind_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#  class { "mobi_mgo::maxmind" :
#           version => "5.0.0-179808",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::maxmind (
    $version = $mobi_mgo::maxmind::params::version,
)
inherits mobi_mgo::maxmind::params {
    include mobi_mgo::maxmind::install, mobi_mgo::maxmind::config
    anchor { "mobi_mgo::maxmind::begin":} -> Class["mobi_mgo::maxmind::install"]
    Class["mobi_mgo::maxmind::config"] -> anchor { "mobi_mgo::juing::end": }
    os::motd::register { "mobi-mgo::maxmind": }
}
