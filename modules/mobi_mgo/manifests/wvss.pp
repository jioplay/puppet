# == Class: mobi_mgo::wvss
#
#  installs wvss component
#
# === Parameters:
#
#
# === Requires:
#
#    httpd
#
# === Sample Usage
#
#   class { "mobi_mgo::wvss" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::wvss (
)
inherits mobi_mgo::wvss::params {
    include mobi_mgo::wvss::install, mobi_mgo::wvss::config
    anchor { "mobi_mgo::wvss::begin": } -> Class["mobi_mgo::wvss::install"]
    Class["mobi_mgo::wvss::config"] -> anchor { "mobi_mgo::wvss::end" :}
}
