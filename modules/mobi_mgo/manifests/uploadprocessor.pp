# == Class: mobi_mgo::uploadprocessor
#
#  installs uploadprocessor component
#
# === Parameters:
#
#    $version::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#  class { "mobi_mgo::uploadprocessor" :
#           version => "5.0.0-205720",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::uploadprocessor (
    $version = $mobi_mgo::uploadprocessor::params::version,
    $widevine_url_base = $mobi_mgo::uploadprocessor::params::widevine_url_base,
    $mgo_ftp_hostname = $mobi_mgo::uploadprocessor::params::mgo_ftp_hostname,
    $mgo_ftp_port = $mobi_mgo::uploadprocessor::params::mgo_ftp_port,
    $mgo_ftp_username = $mobi_mgo::uploadprocessor::params::mgo_ftp_username,
    $mgo_ftp_password = $mobi_mgo::uploadprocessor::params::mgo_ftp_password,
    $mgo_ftp_pathname = $mobi_mgo::uploadprocessor::params::mgo_ftp_pathname,
    $sftp_host = $mobi_mgo::uploadprocessor::params::sftp_host,
    $sftp_port = $mobi_mgo::uploadprocessor::params::sftp_port,
    $sftp_user = $mobi_mgo::uploadprocessor::params::sftp_user,
    $sftp_password = $mobi_mgo::uploadprocessor::params::sftp_password,
    $upload_base_path = $mobi_mgo::uploadprocessor::params::upload_base_path,
)
inherits mobi_mgo::uploadprocessor::params {
    include mobi_mgo::uploadprocessor::install, mobi_mgo::uploadprocessor::config
    anchor { "mobi_mgo::uploadprocessor::begin": } -> Class["mobi_mgo::uploadprocessor::install"]
    Class["mobi_mgo::uploadprocessor::config"] -> anchor { "mobi_mgo::uploadprocessor::end" :}
    os::motd::register { $name : }
}
