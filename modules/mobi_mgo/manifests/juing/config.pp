class mobi_mgo::juing::config {

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/ingestor.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_mgo/juing/ingestor-tomcat-context.xml.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    # Database persistence files
    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/legacy-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juing_files/ingestor-legacy-persistence.xml",
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/ingestor-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juing_files/ingestor-ing-persistence.xml",
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/META-INF/aspera-persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juing_files/ingestor-aspera-persistence.xml",
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    # Customized Quartz Spring configs
    file { "/opt/mobi-arlanda-ingestor/webapp/WEB-INF/classes/spring/quartzContext.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juing_files/ingestor-quartzContext.xml",
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/arlanda/conf/ingestor/ingestor.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
  content => template('mobi_mgo/juing/ingestor.properties.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/arlanda/conf/mgo-cms-postprocessor/mgo-cms-postprocessor.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_mgo/juing/mgo-cms-postprocessor.properties.erb'),
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/arlanda/conf/searchagent/searchagent.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        source => "puppet:///modules/mobi_mgo/juing_files/searchagent.properties",
        require => Class["juing::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/var/Jukebox/uploads/search_transform/search_default_transformation.xslt":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        require => Class["juing::install"],
        source => "puppet:///modules/mobi_mgo/juing_files/search_default_transformation.xslt",
    }

    file { "/var/Jukebox/uploads/search_transform/2.xslt":
        ensure => link,
        target => "/var/Jukebox/uploads/search_transform/search_default_transformation.xslt",
    }

    file { "/var/www/html/strtotime.php":
        replace => true,
        owner  => "apache",
        group  => "arlanda",
        mode => "0444",
        require => Class["juing::install"],
        source => "puppet:///modules/mobi_mgo/juing_files/strtotime.php",
    }

    file { "/usr/local/bin/create_provider_account.php":
        replace => true,
        owner  => "rtv",
        group  => "arlanda",
        mode => "0440",
        content => template('mobi_mgo/juing/create_provider_account.php.erb'),
    }

    file { "/usr/local/bin/retranscode.php":
        replace => true,
        owner  => "rtv",
        group  => "arlanda",
        mode => "0440",
        content => template('mobi_mgo/juing/retranscode.php.erb'),
    }
}
