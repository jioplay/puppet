class mobi_mgo::juing::install {

  class {"apache":
    package => "httpd.x86_64",
  }

  class {"php": }

  package {"php-soap-5.1.6":
    ensure => present,
    notify => Class["apache::service"],
  }

  package { "mobi-arlanda-searchagent-${mobi_mgo::juing::searchagent_version}" :
    ensure => present,
    notify => Class["tomcat::service"],
  }

  package { "mobi-mgo-cms-postprocessor-${mobi_mgo::juing::mgo_cms_postprocessor_version}":
    ensure => present,
    notify   => Class["tomcat::service"],
  }

  package { "mobi-arlanda-ingestor-${mobi_mgo::juing::ingestor_version}":
    ensure => present,
    require => Package["mobi-arlanda-searchagent-${mobi_mgo::juing::searchagent_version}"],
    notify => Class["tomcat::service"],
  }

}
