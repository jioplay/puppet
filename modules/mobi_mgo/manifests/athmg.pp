# == Class: mobi_mgo::athmg
#
#  installs athmg component
#
# === Parameters:
#
#    $rights_manager_ver::
#    $stream_manager_ver::
#    $account_management_ver::
#    $database_host::
#    $database_port::
#    $database_user::
#    $database_passwd::
#    $database_sid::
#    $uprgade::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_mgo::athmg" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mgo::athmg (
    $rights_manager_ver =     $mobi_mgo::athmg::params::rights_manager_ver ,
    $stream_manager_ver =     $mobi_mgo::athmg::params::stream_manager_ver ,
    $account_management_ver = $mobi_mgo::athmg::params::account_management_ver ,
    $database_url = $mobi_mgo::athmg::params::database_url ,
    $database_user = $mobi_mgo::athmg::params::database_user,
    $database_passwd = $mobi_mgo::athmg::params::database_passwd,
    $upgrade = $mobi_mgo::athmg::params::upgrade,
    $memcached_servers = $mobi_mgo::athmg::params::memcached_servers,
    $auth_connections_per_host = $mobi_mgo::athmg::params::auth_connections_per_host,
    $auth_connections_max = $mobi_mgo::athmg::params::auth_connections_max,
)
inherits mobi_mgo::athmg::params {
    include mobi_mgo::athmg::install, mobi_mgo::athmg::config
    anchor { "mobi_mgo::athmg::begin": } ->
    Class["mobi_mgo::athmg::install"] ->
    Class["mobi_mgo::athmg::config"] -> anchor { "mobi_mgo::athmg::end" :}
    os::motd::register { $name : }
}
