-- Define Packages
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (1,'LOW','Low bit-rate encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (2,'PD','Progressive-Download encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (3,'SD','Standard Definition encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (4,'HD','High Definition encoding set',0);
Insert into WFM_ENCODING (ID,NAME,DESCRIPTION,IS_ENCRYPTED) values (5,'HD+','Super High Definition encoding set',0);

-- Define encodings
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (1,1001);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (2,1002);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (3,1003);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (4,1004);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (5,1005);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (6,1006);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (7,1007);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (8,1008);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (9,1009);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (10,1010);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (11,1011);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (12,1012);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (13,1013);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (14,1014);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (15,1015);
Insert into WFM_ENCODING_JOB (ID,ENCODING_JOB_ID) values (16,1016);

-- Pair the encodings with packages
-- LOW Package
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (1,1);

-- PD Package
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,2);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,3);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,4);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (2,5);

-- SD
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (3,6);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (3,7);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (3,8);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (3,9);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (3,10);

-- HD
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (4,11);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (4,12);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (4,13);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (4,14);
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (4,15);

-- HD+
Insert into WFM_ENCODING_MAP (WFM_ENCODING_ID,MP_ENCODING_ID) values (5,16);

Insert into WFM_QRTZ_JOB_DETAILS (JOB_NAME,JOB_GROUP,DESCRIPTION,JOB_CLASS_NAME,IS_DURABLE,IS_VOLATILE,IS_STATEFUL,REQUESTS_RECOVERY) values ('workflowCleanupJob','DEFAULT',null,'com.mobitv.arlanda.wfm.tasks.CleanupTask','0','0','0','0');


Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (1,'DefaultIngestContent','Default content transcoding workflow','IngestContent','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>DeleteEncoding LOW</dependency>
        <dependency>DeleteEncoding PD</dependency>
        <dependency>DeleteEncoding SD</dependency>
        <dependency>DeleteEncoding HD</dependency>
        <dependency>DeleteEncoding HD+</dependency>
      </dependencies>
      <taskdata/>
    </task>

    <!-- LOW ENCODING -->
    <task>
      <id>WidevineEncryption LOW</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>LOW</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1101</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding LOW</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption LOW</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>LOW</value>
        </dataitem>
      </taskdata>
    </task>

    <!-- PD ENCODING -->
    <task>
      <id>WidevineEncryption PD</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>PD</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1102</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>trickplayEncodingId</key>
          <value>1005</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding PD</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption PD</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>PD</value>
        </dataitem>
      </taskdata>
    </task>

    <!-- SD ENCODING -->
    <task>
      <id>WidevineEncryption SD</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>SD</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1103</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>trickplayEncodingId</key>
          <value>1010</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding SD</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption SD</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>SD</value>
        </dataitem>
      </taskdata>
    </task>

    <!-- HD ENCODING -->
    <task>
      <id>WidevineEncryption HD</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>HD</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1104</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>trickplayEncodingId</key>
          <value>1015</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding HD</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption HD</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>HD</value>
        </dataitem>
      </taskdata>
    </task>


    <!-- HD+ ENCODING -->
    <task>
      <id>WidevineEncryption HD+</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>HD+</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1105</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding HD+</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption HD+</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>HD+</value>
        </dataitem>
      </taskdata>
    </task>

    <task>
      <id>ContentBasedXCodeSetup</id>
      <class>ContentBasedXCodeSetupTask</class>
      <dependencies>
        <dependency>GetAssetInfo</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingTaskType</key>
          <value>ElementalXCodeTask</value>
        </dataitem>
      </taskdata>
    </task>

    <task>
      <id>GetAssetInfo</id>
      <class>GetAssetInfoTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');




Insert into WFM_WORKFLOW (ID,NAME,DESCRIPTION,WORKFLOW_TYPE,XML_TEMPLATE) values (2,'DefaultTranscode','Default content transcoding workflow','Transcode','<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<workflowdescription>
  <tasklist>
    <task>
      <id>Finished</id>
      <class>FinishedTask</class>
      <dependencies>
        <dependency>MakeAssetAvailable</dependency>
      </dependencies>
      <taskdata/>
    </task>
    <task>
      <id>MakeAssetAvailable</id>
      <class>MakeAssetAvailableTask</class>
      <dependencies>
        <dependency>DeleteEncoding LOW</dependency>
        <dependency>DeleteEncoding PD</dependency>
        <dependency>DeleteEncoding SD</dependency>
        <dependency>DeleteEncoding HD</dependency>
        <dependency>DeleteEncoding HD+</dependency>
      </dependencies>
      <taskdata/>
    </task>

    <!-- LOW ENCODING -->
    <task>
      <id>WidevineEncryption LOW</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>LOW</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1101</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding LOW</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption LOW</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>LOW</value>
        </dataitem>
      </taskdata>
    </task>

    <!-- PD ENCODING -->
    <task>
      <id>WidevineEncryption PD</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>PD</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1102</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>trickplayEncodingId</key>
          <value>1005</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding PD</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption PD</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>PD</value>
        </dataitem>
      </taskdata>
    </task>

    <!-- SD ENCODING -->
    <task>
      <id>WidevineEncryption SD</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>SD</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1103</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>trickplayEncodingId</key>
          <value>1010</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding SD</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption SD</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>SD</value>
        </dataitem>
      </taskdata>
    </task>

    <!-- HD ENCODING -->
    <task>
      <id>WidevineEncryption HD</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>HD</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1104</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>trickplayEncodingId</key>
          <value>1015</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding HD</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption HD</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>HD</value>
        </dataitem>
      </taskdata>
    </task>


    <!-- HD+ ENCODING -->
    <task>
      <id>WidevineEncryption HD+</id>
      <class>WidevineEncodingTask</class>
      <dependencies>
        <dependency>ContentBasedXCodeSetup</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>HD+</value>
        </dataitem>
        <dataitem>
          <key>encodingId</key>
          <value>1105</value>
        </dataitem>
        <dataitem>
          <key>providerName</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>contentOwner</key>
          <value>medianavi</value>
        </dataitem>
        <dataitem>
          <key>drmPolicy</key>
          <value>default</value>
        </dataitem>
      </taskdata>
    </task>
    <task>
      <id>DeleteEncoding HD+</id>
      <class>DeleteEncodingTask</class>
      <dependencies>
        <dependency>WidevineEncryption HD+</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingName</key>
          <value>HD+</value>
        </dataitem>
      </taskdata>
    </task>

    <task>
      <id>ContentBasedXCodeSetup</id>
      <class>ContentBasedXCodeSetupTask</class>
      <dependencies>
        <dependency>GetAssetInfo</dependency>
      </dependencies>
      <taskdata>
        <dataitem>
          <key>encodingTaskType</key>
          <value>ElementalXCodeTask</value>
        </dataitem>
      </taskdata>
    </task>

    <task>
      <id>GetAssetInfo</id>
      <class>GetAssetInfoTask</class>
      <dependencies/>
      <taskdata/>
    </task>
  </tasklist>
</workflowdescription>');


-- Priority should range from 1 - 100, with a default of 50
Insert into WFM_PROVIDER_CONTEXT (ID,PRIORITY,CONTENT_AUTH_MODE,ADVERTISING_SOURCE,TRANSCODING_CLUSTER, ENCRYPTION_REQUIRED) values (2,50,'NotRequired',null,'simulator', 'TRUE');


Insert into WFM_PROVIDER_WORKFLOW (PROVIDER_ID,WORKFLOW_ID) values (2,1);

Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,1);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,2);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,3);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,4);
Insert into WFM_PROVIDER_ENCODING (PROVIDER_ID,ENCODING_ID) values (2,5);
