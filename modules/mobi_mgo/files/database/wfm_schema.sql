-- MySQL dump 10.11
--
-- Host: localhost    Database: wfm
-- ------------------------------------------------------
-- Server version	5.0.95

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `WFM_ASSET_CONTEXT`
--

DROP TABLE IF EXISTS `WFM_ASSET_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_ASSET_CONTEXT` (
  `id` bigint(20) NOT NULL auto_increment,
  `ASSET_ID` bigint(20) default NULL,
  `CARRIER_APPROVED` bit(1) default NULL,
  `EXPIRES` varchar(255) default NULL,
  `INVENTORY_ID` bigint(20) default NULL,
  `MEDIA_ASPECT_RATIO` varchar(255) default NULL,
  `MEDIA_URL` text,
  `METADATA_URL` text,
  `POST_ROLL_URL` text,
  `PRE_ROLL_URL` text,
  `PROVIDER_ID` bigint(20) default NULL,
  `VERSION` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_EMAIL_NOTIFICATION`
--

DROP TABLE IF EXISTS `WFM_EMAIL_NOTIFICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_EMAIL_NOTIFICATION` (
  `id` bigint(20) NOT NULL auto_increment,
  `EMAIL_ADDRESS` text NOT NULL,
  `NOTIFICATION_TYPE` varchar(64) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_ENCODING`
--

DROP TABLE IF EXISTS `WFM_ENCODING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_ENCODING` (
  `id` bigint(20) NOT NULL auto_increment,
  `DESCRIPTION` text,
  `IS_ENCRYPTED` bit(1) default NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_ENCODING_JOB`
--

DROP TABLE IF EXISTS `WFM_ENCODING_JOB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_ENCODING_JOB` (
  `id` bigint(20) NOT NULL auto_increment,
  `ENCODING_JOB_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_ENCODING_MAP`
--

DROP TABLE IF EXISTS `WFM_ENCODING_MAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_ENCODING_MAP` (
  `WFM_ENCODING_ID` bigint(20) NOT NULL,
  `MP_ENCODING_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`WFM_ENCODING_ID`,`MP_ENCODING_ID`),
  KEY `FK_ENCODING_MAP_02` (`MP_ENCODING_ID`),
  KEY `FK_ENCODING_MAP_01` (`WFM_ENCODING_ID`),
  CONSTRAINT `FK_ENCODING_MAP_01` FOREIGN KEY (`WFM_ENCODING_ID`) REFERENCES `WFM_ENCODING` (`id`),
  CONSTRAINT `FK_ENCODING_MAP_02` FOREIGN KEY (`MP_ENCODING_ID`) REFERENCES `WFM_ENCODING_JOB` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_MESSAGE_MAP`
--

DROP TABLE IF EXISTS `WFM_MESSAGE_MAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_MESSAGE_MAP` (
  `id` bigint(20) NOT NULL auto_increment,
  `CORRELATION_ID` varchar(255) NOT NULL,
  `MESSAGE_ID` varchar(255) NOT NULL,
  `TASK_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_TASK_MESSAGE_MAP_01` (`TASK_ID`),
  CONSTRAINT `FK_TASK_MESSAGE_MAP_01` FOREIGN KEY (`TASK_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_PROVIDER_CONTEXT`
--

DROP TABLE IF EXISTS `WFM_PROVIDER_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_PROVIDER_CONTEXT` (
  `id` bigint(20) NOT NULL,
  `ADVERTISING_SOURCE` varchar(255) default NULL,
  `CONTENT_AUTH_MODE` varchar(255) NOT NULL,
  `ENCRYPTION_REQUIRED` varchar(255) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `TRANSCODING_CLUSTER` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_PROVIDER_ENCODING`
--

DROP TABLE IF EXISTS `WFM_PROVIDER_ENCODING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_PROVIDER_ENCODING` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `ENCODING_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`PROVIDER_ID`,`ENCODING_ID`),
  KEY `FK_PROVIDER_ENCODING_02` (`ENCODING_ID`),
  KEY `FK_PROVIDER_ENCODING_01` (`PROVIDER_ID`),
  CONSTRAINT `FK_PROVIDER_ENCODING_01` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `WFM_PROVIDER_CONTEXT` (`id`),
  CONSTRAINT `FK_PROVIDER_ENCODING_02` FOREIGN KEY (`ENCODING_ID`) REFERENCES `WFM_ENCODING` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_PROVIDER_NOTIFICATION`
--

DROP TABLE IF EXISTS `WFM_PROVIDER_NOTIFICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_PROVIDER_NOTIFICATION` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `NOTIFICATION_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`PROVIDER_ID`,`NOTIFICATION_ID`),
  UNIQUE KEY `NOTIFICATION_ID` (`NOTIFICATION_ID`),
  KEY `FK_PROVIDER_NOTIFICATION_01` (`PROVIDER_ID`),
  KEY `FK_PROVIDER_NOTIFICATION_02` (`NOTIFICATION_ID`),
  CONSTRAINT `FK_PROVIDER_NOTIFICATION_02` FOREIGN KEY (`NOTIFICATION_ID`) REFERENCES `WFM_EMAIL_NOTIFICATION` (`id`),
  CONSTRAINT `FK_PROVIDER_NOTIFICATION_01` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `WFM_PROVIDER_CONTEXT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_PROVIDER_WORKFLOW`
--

DROP TABLE IF EXISTS `WFM_PROVIDER_WORKFLOW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_PROVIDER_WORKFLOW` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `WORKFLOW_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`PROVIDER_ID`,`WORKFLOW_ID`),
  KEY `FK_PROVIDER_WORKFLOW_01` (`PROVIDER_ID`),
  KEY `FK_PROVIDER_WORKFLOW_02` (`WORKFLOW_ID`),
  CONSTRAINT `FK_PROVIDER_WORKFLOW_02` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `WFM_WORKFLOW` (`id`),
  CONSTRAINT `FK_PROVIDER_WORKFLOW_01` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `WFM_PROVIDER_CONTEXT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_TASK_DATA`
--

DROP TABLE IF EXISTS `WFM_TASK_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_TASK_DATA` (
  `id` bigint(20) NOT NULL auto_increment,
  `KEY` varchar(255) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_TASK_INSTANCE`
--

DROP TABLE IF EXISTS `WFM_TASK_INSTANCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_TASK_INSTANCE` (
  `id` bigint(20) NOT NULL auto_increment,
  `FINISHED_AT` datetime default NULL,
  `PROVIDER_ID` bigint(20) default NULL,
  `QUEUED_AT` datetime default NULL,
  `STARTED_AT` datetime default NULL,
  `TASK_STATUS` varchar(64) NOT NULL,
  `TASK_TYPE` varchar(64) NOT NULL,
  `OPTLOCK` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_TASK_INSTANCE_DATA`
--

DROP TABLE IF EXISTS `WFM_TASK_INSTANCE_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_TASK_INSTANCE_DATA` (
  `TASK_INSTANCE_ID` bigint(20) NOT NULL,
  `TASK_DATA_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`TASK_INSTANCE_ID`,`TASK_DATA_ID`),
  UNIQUE KEY `TASK_DATA_ID` (`TASK_DATA_ID`),
  KEY `FK_TASK_INSTANCE_DATA_02` (`TASK_DATA_ID`),
  KEY `FK_TASK_INSTANCE_DATA_01` (`TASK_INSTANCE_ID`),
  CONSTRAINT `FK_TASK_INSTANCE_DATA_01` FOREIGN KEY (`TASK_INSTANCE_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`),
  CONSTRAINT `FK_TASK_INSTANCE_DATA_02` FOREIGN KEY (`TASK_DATA_ID`) REFERENCES `WFM_TASK_DATA` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_TASK_INSTANCE_DEPENDENCY`
--

DROP TABLE IF EXISTS `WFM_TASK_INSTANCE_DEPENDENCY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_TASK_INSTANCE_DEPENDENCY` (
  `TASK_INSTANCE_ID` bigint(20) NOT NULL,
  `TASK_DEPENDS_ON_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`TASK_INSTANCE_ID`,`TASK_DEPENDS_ON_ID`),
  KEY `FK_TASK_INSTANCE_DEPENDENCY_01` (`TASK_DEPENDS_ON_ID`),
  KEY `FK_TASK_INSTANCE_DEPENDENCY_02` (`TASK_INSTANCE_ID`),
  CONSTRAINT `FK_TASK_INSTANCE_DEPENDENCY_02` FOREIGN KEY (`TASK_INSTANCE_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`),
  CONSTRAINT `FK_TASK_INSTANCE_DEPENDENCY_01` FOREIGN KEY (`TASK_DEPENDS_ON_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_WORKFLOW`
--

DROP TABLE IF EXISTS `WFM_WORKFLOW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_WORKFLOW` (
  `id` bigint(20) NOT NULL auto_increment,
  `DESCRIPTION` text,
  `NAME` varchar(255) NOT NULL,
  `WORKFLOW_TYPE` varchar(255) NOT NULL,
  `XML_TEMPLATE` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_WORKFLOW_INSTANCE`
--

DROP TABLE IF EXISTS `WFM_WORKFLOW_INSTANCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_WORKFLOW_INSTANCE` (
  `id` bigint(20) NOT NULL auto_increment,
  `FINISHED_AT` datetime default NULL,
  `STARTED_AT` datetime default NULL,
  `OPTLOCK` int(11) default NULL,
  `WORKFLOW_STATUS` varchar(64) NOT NULL,
  `ASSET_CONTEXT_ID` bigint(20) NOT NULL,
  `PROVIDER_CONTEXT_ID` bigint(20) NOT NULL,
  `WORKFLOW_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_WORKFLOW_INSTANCE_03` (`PROVIDER_CONTEXT_ID`),
  KEY `FK_WORKFLOW_INSTANCE_02` (`ASSET_CONTEXT_ID`),
  KEY `FK_WORKFLOW_INSTANCE_01` (`WORKFLOW_ID`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_01` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `WFM_WORKFLOW` (`id`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_02` FOREIGN KEY (`ASSET_CONTEXT_ID`) REFERENCES `WFM_ASSET_CONTEXT` (`id`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_03` FOREIGN KEY (`PROVIDER_CONTEXT_ID`) REFERENCES `WFM_PROVIDER_CONTEXT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WFM_WORKFLOW_INSTANCE_TASK`
--

DROP TABLE IF EXISTS `WFM_WORKFLOW_INSTANCE_TASK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_WORKFLOW_INSTANCE_TASK` (
  `WORKFLOW_INSTANCE_ID` bigint(20) NOT NULL,
  `TASK_INSTANCE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`WORKFLOW_INSTANCE_ID`),
  KEY `FK_WORKFLOW_INSTANCE_TASK_02` (`WORKFLOW_INSTANCE_ID`),
  KEY `FK_WORKFLOW_INSTANCE_TASK_01` (`TASK_INSTANCE_ID`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_TASK_01` FOREIGN KEY (`TASK_INSTANCE_ID`) REFERENCES `WFM_WORKFLOW_INSTANCE` (`id`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_TASK_02` FOREIGN KEY (`WORKFLOW_INSTANCE_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-05-15  0:12:15
