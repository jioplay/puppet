<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/">
    <normalizedProviderMetadata>
      <pair>
	<metadata>
	  <xsl:attribute name="type">STRING</xsl:attribute>
	  <xsl:attribute name="key">technicolor_id</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/technicolor_id" />
	  </xsl:attribute>
	</metadata>

	<metadata>
	  <xsl:attribute name="type">STRING</xsl:attribute>
	  <xsl:attribute name="key">technicolor_external_studio</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/provider_display" />
	  </xsl:attribute>
	</metadata>

	<metadata>
	  <xsl:attribute name="type">STRING</xsl:attribute>
	  <xsl:attribute name="key">type</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/media_type" />
	  </xsl:attribute>
	</metadata>

	<metadata>  
	  <xsl:attribute name="type">STRING</xsl:attribute>
	  <xsl:attribute name="key">runtime</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/runtime" />
	  </xsl:attribute>
	</metadata>

	<metadata>
	  <xsl:attribute name="type">INTEGER</xsl:attribute>
	  <xsl:attribute name="key">original_release_year</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/original_release_year" />
	  </xsl:attribute>
	</metadata>

	<xsl:for-each select="/content/ratings/rating">
	<metadata>
	  <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
	  <xsl:attribute name="key">mpaa_rating</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="." />
	  </xsl:attribute>
	</metadata>
	</xsl:for-each>

	<xsl:for-each select="/content/genres/genre">
	<metadata>
	  <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
	  <xsl:attribute name="key">genre</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="." />
	  </xsl:attribute>
	</metadata>
	</xsl:for-each>

	<xsl:for-each select="/content/cast/actor">
	<metadata>
	  <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
	  <xsl:attribute name="key">cast</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="@display" />
	  </xsl:attribute>
	</metadata>
	</xsl:for-each>

	<xsl:for-each select="/content/crew/member">
	<metadata>
	  <xsl:attribute name="type">STRING_MULTI</xsl:attribute>
	  <xsl:attribute name="key">crew</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="@display" />
	  </xsl:attribute>
	</metadata>
	</xsl:for-each>

	<!-- TV BEGIN -->
	<metadata>  
	  <xsl:attribute name="type">STRING</xsl:attribute>
	  <xsl:attribute name="key">tv_season</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/tvMetadata/season" />
	  </xsl:attribute>
	</metadata>

	<metadata>  
	  <xsl:attribute name="type">STRING</xsl:attribute>
	  <xsl:attribute name="key">tv_season_year</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/tvMetadata/seasonYear" />
	  </xsl:attribute>
	</metadata>

	<metadata>  
	  <xsl:attribute name="type">STRING</xsl:attribute>
	  <xsl:attribute name="key">tv_production_episode_number</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/tvMetadata/productionEpisodeNumber" />
	  </xsl:attribute>
	</metadata>

	<metadata>  
	  <xsl:attribute name="type">STRING</xsl:attribute>
	  <xsl:attribute name="key">tv_episode_title</xsl:attribute>
	  <xsl:attribute name="value">
	    <xsl:value-of select="/content/tvMetadata/episodeTitle" />
	  </xsl:attribute>
	</metadata>
	<!-- TV END -->

      </pair>
    </normalizedProviderMetadata>
  </xsl:template>
</xsl:stylesheet>
