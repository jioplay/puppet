# Class: ntpd::params
#
# This module manages ntpd paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class ntpd::params {

  ### install.pp
  $package = "ntp"
  $package_ensure = present

  ### service.pp
  $service = "ntpd"
  $service_ensure = running
  $service_enable = true

  ### config.pp
  $config = "ntp.conf"

  ### icinga.pp
  $icinga = true
  $icinga_instance = "icinga"
}
