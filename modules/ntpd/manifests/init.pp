# Class: ntpd
#
# This module manages ntpd
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class ntpd (

  $package = $ntpd::params::package,
  $package_ensure = $ntpd::params::package_ensure,

  $service = $ntpd::params::service,
  $service_ensure = $ntpd::params::service_ensure,
  $service_enable = $ntpd::params::service_enable,

  $config = $ntpd::params::config,

  $icinga = $ntpd::params::icinga,
  $icinga_instance = $ntpd::params::icinga_instance,

) inherits ntpd::params {

  anchor {  "ntpd::begin": } ->
  class { "ntpd::install": } ->
  class { "ntpd::config": } ->
  class { "ntpd::service": } ->
  class { "ntpd::icinga": } ->
  anchor { "ntpd::end": }

  # include module name in motd
  os::motd::register { "ntpd": }
}
