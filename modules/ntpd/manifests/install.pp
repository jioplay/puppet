class ntpd::install {
  package { $::ntpd::package:
    ensure => $::ntpd::package_ensure,
  }
}
