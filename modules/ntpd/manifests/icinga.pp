class ntpd::icinga {

  if $::ntpd::icinga {

    if ! $::ntpd::icinga_instance {
      fail("Must provide icinga_instance parameter to ntpd module when icinga = true")
    }

    @@nagios_service { "check_ntp_drift_${::fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_ntp_drift!",
      service_description   => "check_ntp_drift",
      normal_check_interval => "40", # less aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::ntpd::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }

    @@nagios_service { "check_proc_ntpd_${::fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_proc_ntpd!",
      service_description   => "check_proc_ntpd",
      normal_check_interval => "30", # less aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::ntpd::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
            require               => Class["icinga::register"],
    }
  }
}
