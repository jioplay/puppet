class ntpd::config {
  $config_file = $::ntpd::config ? {
    /^.+dmz$/        =>  'ntp.conf.dmz',
    'mum1.r4g.com'   => 'ntp.conf.mum1',
    default          => 'ntp.conf',
  }
  file { "/etc/ntp.conf":
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0644",
    source  => "puppet://$puppetserver/modules/ntpd/${config_file}",
    require => Class["ntpd::install"],
  }
}
