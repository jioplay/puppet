class ntpd::service {
  service { $::ntpd::service:
    ensure      => $::ntpd::service_ensure,
    enable      => $::ntpd::service_enable,
    hasstatus   => true,
    hasrestart  => true,
    subscribe   => [ Class["ntpd::install"], Class["ntpd::config"] ]
  }
}
