# == Class: mobi_arlanda_fm
#
#  installs juarn component
#
# === Parameters:
#
#    $wfm_version::
#    $mp_version::
#    $fm_version::
#    $dam_version::
#    $dam_username::
#    $dam_password::
#    $wfm_username::
#    $wfm_password::
#    $mp_username::
#    $mp_password::
#    $fm_username::
#    $fm_password::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_arlanda_fm" :
#           version => "5.0.0-179993",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_fm (
    ###icinga.pp
    $icinga = $mobi_arlanda_fm::params::icinga,
    $icinga_instance = $mobi_arlanda_fm::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_fm::params::icinga_cmd_args,
    ###end icinga.pp
    $version = $mobi_arlanda_fm::params::version,
    $amq_broker_url = $mobi_arlanda_fm::params::amq_broker_url,
    $dam_url = $mobi_arlanda_fm::params::dam_url,
    $ingestor_url = $mobi_arlanda_fm::params::ingestor_url,
    $db_username = $mobi_arlanda_fm::params::db_username,
    $db_password = $mobi_arlanda_fm::params::db_password,
    $hibernate_database_dialect = $mobi_arlanda_fm::params::hibernate_database_dialect,
    $hibernate_database_vendor = $mobi_arlanda_fm::params::hibernate_database_vendor,
    $jdbc_driver = $mobi_arlanda_fm::params::jdbc_driver,
    $connection_test_query = $mobi_arlanda_fm::params::connection_test_query,
    $oracle_native_queries_enabled = $mobi_arlanda_fm::params::oracle_native_queries_enabled,
    $fm_db_jdbc_url = $mobi_arlanda_fm::params::fm_db_jdbc_url,
)

inherits mobi_arlanda_fm::params {
    include mobi_arlanda_fm::install, mobi_arlanda_fm::config, mobi_cms_us::tomcat_config
    anchor { "mobi_arlanda_fm::begin": } -> Class["mobi_arlanda_fm::install"] ->
    Class["mobi_arlanda_fm::config"] -> Class["mobi_cms_us::tomcat_config"] ->
    class { "mobi_arlanda_fm::icinga":} ->
    anchor { "mobi_arlanda_fm::end" :} -> os::motd::register { $name : }
}

