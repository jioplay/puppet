class mobi_arlanda_fm::config {

    file { "/opt/mobi-tomcat-config/mobi-tomcat-config-8080/conf/Catalina/localhost/foldermanager.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_fm/foldermanager-tomcat-context.xml.erb'),
        require => Class["mobi_arlanda_fm::install"],
        notify   => Class["tomcat::service"],
    }

    file { "/opt/mobi-arlanda-fm/webapp/WEB-INF/classes/META-INF/persistence.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_fm/folder-manager-persistence.xml.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_arlanda_fm::install"],
    }

    file { "/opt/arlanda/conf/foldermanager/foldermanager.properties":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_arlanda_fm/foldermanager.properties.erb'),
        notify   => Class["tomcat::service"],
        require => Class["mobi_arlanda_fm::install"],
    }

}
