class mobi_arlanda_fm::params {
    ###icinga.pp
    ### Monitoring details
    ## From Linda Wei: health check no longer valid in 5.4: it’s folded into dam.
    $icinga = false
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-arlanda-fm/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $version = "5.0.0-210960"

    $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100"

    $dam_url = "http://juarnvip:8080/dam/ws?wsdl"
    $ingestor_url = "http://juingvip:8080/ingestor/"

    $db_username = "fm_user"
    $db_password = "fm_user"

    # Global database params
    $hibernate_database_dialect = "org.hibernate.dialect.MySQL5Dialect"
    $hibernate_database_vendor = "MYSQL"
    $jdbc_driver = "com.mysql.jdbc.Driver"
    $connection_test_query = "select 1 from dual"

    $oracle_native_queries_enabled = "false"
    $fm_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/mobi2lite?autoReconnect=true"
}
