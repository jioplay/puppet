class mobi_arlanda_fm::install {

    package { "mobi-arlanda-fm-${mobi_arlanda_fm::version}" :
        ensure => present,
        notify   => Class["tomcat::service"],
    }
}
