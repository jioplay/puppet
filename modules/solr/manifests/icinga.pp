class solr::icinga {

  if $::solr::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::solr::icinga_instance {
      fail("Must provide icinga_instance parameter to solr module when icinga = true")
    }

    @@nagios_service { "check_solr_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_solr! ${::solr::icinga_cmd_args}",
      service_description => "check_solr",
      notes => "PATH: ${::solr::icinga_cmd_args}",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::solr::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

  }
}
