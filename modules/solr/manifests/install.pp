class solr::install {

    package {"mobi-solr-default":
        ensure => $solr::common_ver
    }

    # Can configure machine to have Master, Slave or Repeater
    case $solr::mode {
        master:   { $pkg = "mobi-solr-master" }
        slave:    { $pkg = "mobi-solr-slave" }
        repeater: { $pkg = "mobi-solr-repeater" }
        default:  { fail("Unknown solr mode ($solr::mode)") }
    }

    package { "$solr::install::pkg":
        ensure  => $solr::version,
        require => Package["mobi-solr-default"],
    }

    exec {"Create Sym link":
        path      => "/usr/bin:/usr/local/bin:/bin",
        command   => "ln -s /var/data/solr/data /opt/mobitv/solr/",
        tries     => 3,
        try_sleep => 5,
        onlyif    => "test ! -L '/opt/mobitv/solr/data'",
        logoutput => true,
        require   => Package["mobi-solr-default"],
    }
}
