class solr::params {
    ###icinga.pp
    $icinga = true
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-H $::ipaddress -p 8080 -W mobi-solr -P"
    ###end icinga.pp

    $mode = "master"
    $common_ver = "5.0.0-171007"
    $version = "5.0.0-169919"
    #SOLR
    #$mobi_solr_default_version = "5.0.0_171007"
    #$mobi_solr_master_version = "5.0.0_169919"
    #$mobi_solr_slave_version = "5.0.0_169919"
    #$mobi_solr_repeater_version = "5.0.0_169919"
}
