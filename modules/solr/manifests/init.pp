# == Class: solr
#
# installs and configure solr
#
# === Parameters:
#
#    mode::        master,slave or repeater solr installation mode
#    version::     the version of the package for this mode
#    common_ver::  solr default or core version
#
# === Requires
#
#    java
#    tomcat
#
# === Sample Usage
#
#  class { "solr" :
#
#  }
#
# Remember: No empty lines between comments and class definition
#
class solr (
    ###icinga.pp
    $icinga = $solr::params::icinga,
    $icinga_instance = $solr::params::icinga_instance,
    $icinga_cmd_args = $solr::params::icinga_cmd_args,
    ###end icinga.pp
    $mode = $solr::params::mode,
    $common_ver = $solr::params::common_ver,
    $version = $solr::params::version,
)
inherits solr::params {
    include solr::install
    anchor { "solr::begin": } -> Class["solr::install"] -> class {"solr::icinga":} -> anchor { "solr::end": }
    os::motd::register { "solr": }
}
