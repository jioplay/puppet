# == Class: mobi_arlanda_ftplogparser
#
#  installs juarn component
#
# === Parameters:
#
#    ftplogparser_version::
#    ftplogparser_username::
#    ftplogparser_password::
#    mediainfo_timeout::  The timeout for mediainfo (D: 5000) in ms.
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_cms_us::juarn" :
#           ftplogparser_version => "5.0.0-178058",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_arlanda_ftplogparser (
    ###icinga.pp
    $icinga = $mobi_arlanda_ftplogparser::params::icinga,
    $icinga_instance = $mobi_arlanda_ftplogparser::params::icinga_instance,
    $icinga_cmd_args = $mobi_arlanda_ftplogparser::params::icinga_cmd_args,
    ###end icinga.pp
    $version = $mobi_arlanda_ftplogparser::params::version,
    $package_name = $mobi_arlanda_ftplogparser::params::package_name,
    $amq_broker_url = $mobi_arlanda_ftplogparser::params::amq_broker_url,
    $dam_url = $mobi_arlanda_ftplogparser::params::dam_url,
    $folder_manager_rest_url = $mobi_arlanda_ftplogparser::params::folder_manager_rest_url,
    $vsftpd_version = $mobi_arlanda_ftplogparser::params::vsftpd_version,
    $vsftpd_package_name = $mobi_arlanda_ftplogparser::params::vsftpd_package_name,
    $pam_version = $mobi_arlanda_ftplogparser::params::pam_version,
    $pam_package_name = $mobi_arlanda_ftplogparser::params::pam_package_name,
    $db_username = $mobi_arlanda_ftplogparser::params::db_username,
    $db_password = $mobi_arlanda_ftplogparser::params::db_password,
    $jdbc_driver = $mobi_arlanda_ftplogparser::params::jdbc_driver,
    $ftplogparser_db_jdbc_url = $mobi_arlanda_ftplogparser::params::ftplogparser_db_jdbc_url,
    $ftplogparser_ensure = $mobi_arlanda_ftplogparser::params::ftplogparser_ensure,
    $ftplogparser_enable = $mobi_arlanda_ftplogparser::params::ftplogparser_enable,
    $vsftpd_ensure = $mobi_arlanda_ftplogparser::params::vsftpd_ensure,
    $vsftpd_enable = $mobi_arlanda_ftplogparser::params::vsftpd_enable,
    $vsftpd_local_auth = $mobi_arlanda_ftplogparser::params::vsftpd_local_auth,
    $ingestor_url = $mobi_arlanda_ftplogparser::params::ingestor_url,
    $mediainfo_timeout = $mobi_arlanda_ftplogparser::params::mediainfo_timeout,
    $local_root = $mobi_arlanda_ftplogparser::params::local_root,
)

inherits mobi_arlanda_ftplogparser::params {
    include mobi_arlanda_ftplogparser::install, mobi_arlanda_ftplogparser::config, mobi_arlanda_ftplogparser::service
    anchor { "mobi_arlanda_ftplogparser::begin": } ->
    Class["mobi_arlanda_ftplogparser::install"] ->
    Class["mobi_arlanda_ftplogparser::config"] ->
    Class["mobi_arlanda_ftplogparser::service"] ->
    class { "mobi_arlanda_ftplogparser::icinga":} ->
    anchor { "mobi_arlanda_ftplogparser::end" :} -> os::motd::register { $name : }
}

