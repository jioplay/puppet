class mobi_arlanda_ftplogparser::params {
    ###icinga.pp
    $icinga = false
    $icinga_instance = "icinga"
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-arlanda-ftplogparser/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $version = "5.0.0-210408"
    $package_name = "mobi-arlanda-ftplogparser"
    $vsftpd_package_name = "mobi-vsftpd"
    $vsftpd_version = "2.0.5-89067"
    $pam_package_name = "mobi-arlanda-pam"
    $pam_version = "1.0.1-213697"

    $dam_url = "http://damvip:8080/dam/ws?wsdl"
    $folder_manager_rest_url = "http://damvip:8080/foldermanager/"
    $amq_broker_url = "failover://(tcp://amq01:61616,tcp://amq02:61616)?initialReconnectDelay=100"

    $ftplogparser_ensure = "running"
    $ftplogparser_enable = true

    $vsftpd_ensure = "running"
    $vsftpd_enable = true
    $vsftpd_local_auth = false

    $db_username = "ing_user"
    $db_password = "ing_user"

    $jdbc_driver = "com.mysql.jdbc.Driver"

    $ftplogparser_db_jdbc_url = "jdbc:mysql://mydtbvip:3306/mobi2lite?autoReconnect=true"

    $ingestor_url = "http://ingestorvip:8080/ingestor/"
    $mediainfo_timeout = "5000"
    $local_root = "/var/Jukebox/ftpupload/\$USER"
}
