class mobi_arlanda_ftplogparser::config {
    if($mobi_arlanda_ftplogparser::version != "absent"){
      file { "/opt/arlanda/conf/ftplogparser/ftplogparser.properties":
          replace => true,
          owner  => "rtv",
          group  => "rtv",
          mode => "0444",
          content => template('mobi_arlanda_ftplogparser/ftplogparser.properties.erb'),
          require => Class["mobi_arlanda_ftplogparser::install"],
          notify   => Service["ftplogparser"],
      }
    }

    if ($::lsbmajdistrelease == 6){
      file{"/etc/vsftpd/vsftpd.conf":
        ensure => "absent",
      }

      file{"/etc/vsftpd/mobi_vsftpd.conf":
        owner => "root",
        group => "root",
        mode  => "0644",
        content => template('mobi_arlanda_ftplogparser/mobi_vsftpd.conf.erb'),
        notify => Service["vsftpd"],
      }

      file{"/etc/logrotate.d/vsftpd.logrotate":
        owner => "root",
        group => "root",
        mode  => "0644",
        source => "puppet:///modules/mobi_arlanda_ftplogparser/vsftpd.logrotate",
        notify => Service["rsyslog"],
      }

      file{"/etc/vsftpd.mobi_user_list":
        owner => "root",
        group => "root",
        mode  => "0644",
        source => "puppet:///modules/mobi_arlanda_ftplogparser/vsftpd.mobi_user_list",
        notify => Service["vsftpd"],
      }
    }
    else{ #Centos 5.xx
      if($mobi_arlanda_ftplogparser::vsftpd_version != "absent"){
        file {"/etc/pam.d/vsftpd.local_auth":
          replace => true,
          owner => "root",
          group => "root",
          mode => "0555",
          source => "puppet:///modules/mobi_arlanda_ftplogparser/vsftpd.local_auth",
          require => Class["mobi_arlanda_ftplogparser::install"],
          notify => Service["vsftpd"],
        }
      }

    if $mobi_arlanda_ftplogparser::vsftpd_local_auth {
      file {"/etc/pam.d/vsftpd":
        ensure => present,
        owner => "root",
        group => "root",
        mode => "0555",
        source =>"/etc/pam.d/vsftpd.local_auth",
        require => File["/etc/pam.d/vsftpd.local_auth"],
        notify => Service["vsftpd"],
      }
    }
    else{
      file {"/etc/pam.d/vsftpd":
        ensure => present,
        owner => "root",
        group => "root",
        mode => "0555",
        source =>"/etc/pam.d/vsftpd.jukebox",
        require => Class["mobi_arlanda_ftplogparser::install"],
        notify => Service["vsftpd"],
      }
    }
  }
}
