class mobi_arlanda_ftplogparser::service {
    $ftplogparser_ensure = $mobi_arlanda_ftplogparser::version ? {
      'absent' => 'stopped',
      default => 'running',
    }

    $ftplogparser_enable = $mobi_arlanda_ftplogparser::version ? {
      'absent' => false,
      default => true,
    }

    $vsftpd_ensure = $mobi_arlanda_ftplogparser::vsftpd_version ? {
      'absent' => 'stopped',
      default => 'running',
    }

    $vsftpd_enable = $mobi_arlanda_ftplogparser::vsftpd_version ? {
      'absent' => false,
      default => true,
    }

    service { "ftplogparser":
        ensure => $ftplogparser_ensure,
        enable => $ftplogparser_enable,
        require => [ Class["mobi_arlanda_ftplogparser::install"], Class["mobi_arlanda_ftplogparser::config"], Service["vsftpd"], ],
    }

    service { "vsftpd":
        ensure => $vsftpd_ensure,
        enable => $vsftpd_enable,
        require => [ Class["mobi_arlanda_ftplogparser::install"], Class["mobi_arlanda_ftplogparser::config"] ],
    }
}
