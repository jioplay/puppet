class mobi_arlanda_ftplogparser::install {
  package { "${mobi_arlanda_ftplogparser::package_name}":
    ensure => "${mobi_arlanda_ftplogparser::version}",
    notify   => Service["ftplogparser"],
  }

  package {"${mobi_arlanda_ftplogparser::vsftpd_package_name}":
    ensure => "${mobi_arlanda_ftplogparser::vsftpd_version}",
    notify => Service["vsftpd"],
  }

  package {"${mobi_arlanda_ftplogparser::pam_package_name}":
    ensure => "${mobi_arlanda_ftplogparser::pam_version}",
    notify => Service["vsftpd"],
  }
}
