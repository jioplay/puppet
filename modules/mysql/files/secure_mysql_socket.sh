#!/bin/bash

[ "x${1}" == "x" ] && password="" || password="${1}"
cur_password=""
socket=$2

# set root password
mysql --socket=$2 -uroot --password=${cur_password} -e "UPDATE mysql.user SET Password=PASSWORD(\"${password}\") WHERE User='root'"

# remove anonymous user
mysql --socket=$2 -uroot --password=${cur_password} -e "DELETE FROM mysql.user WHERE User=''"

# remove remote root
mysql --socket=$2 -uroot --password=${cur_password} -e "DELETE FROM mysql.user WHERE User='root' AND Host!='localhost'"

# remove test dbase and associated privileges
mysql --socket=$2 -uroot --password=${cur_password} -e "DROP DATABASE test"
mysql --socket=$2 -uroot --password=${cur_password} -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"

# reload privileges table
mysql --socket=$2 -uroot --password=${cur_password} -e "FLUSH PRIVILEGES"
