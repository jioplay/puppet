#!/bin/sh
#set -x
#######################
#Requirenment
# 1. GRANT SHOW DATABASES, SELECT, LOCK TABLES, RELOAD, EVENT ON *.* to backup@localhost IDENTIFIED BY 'unbreaKab1e'; FLUSH PRIVILEGES;
# http://dev.mysql.com/doc/mysql-enterprise-backup/3.6/en/mysqlbackup.privileges.html
#######################
if [ $# -lt 1 ]; then
        echo 1>&2 "$0: Not enough arguments! Please specify the number of days to keep backups (7)"
        exit 2
fi

DIR=/mysql_arch/backup
DATESTAMP=$(date +%Y%m%d-%H%M)
BKPDIR=$DIR/"$DATESTAMP"
DB_USER=backup
DB_PASS='unbreaKab1e'
DAYS_KEEP=$1 #backup retention
NOTIFY=script_alert
EXCL_DB='information_schema|performance_schema'
status=0

# create backups securely
umask 006

# get list of active instances
for DB_ENTRY in `cat $HOME/.mytab | grep -v \# | grep -v \* | grep -v ^$ | grep -v 'N$'`
do
        COMPONENT=`echo $DB_ENTRY | awk -F: {' print $1 '}`
        VERSION=`echo $DB_ENTRY | awk -F: {' print $2 '} | tr '.' '_'`
        DB_INS=`echo "$COMPONENT"_${VERSION%_*}`
        source $HOME/.bash_profile_mysql $DB_INS
        mkdir -p $BKPDIR/$DB_INS
# list MySQL databases and dump each
        DB_LIST=`/var/app/mysql-"$VERSION"/"$COMPONENT"/bin/mysql -u $DB_USER -p"$DB_PASS" --socket=$MYSQL_DATA/mysql.sock --port=$MYSQL_TCP_PORT -e'show databases;' | egrep -v $EXCL_DB`
        if [ $? -eq 1 ]; then
                status=1
        fi
        DB_LIST=${DB_LIST##Database}
        for DB in $DB_LIST;
        do
           FILENAME=${BKPDIR}/${DB_INS}/${DB}-${DATESTAMP}.sql.gz
           options='--opt --skip-lock-tables --single-transaction --skip-events'
             if [ "$DB" = "mysql" ]; then
                /var/app/mysql-"$VERSION"/"$COMPONENT"/bin/mysqldump -u $DB_USER -p"$DB_PASS" --socket=$MYSQL_DATA/mysql.sock --port=$MYSQL_TCP_PORT $options --events $DB > ${FILENAME%.*}
                 echo "FLUSH PRIVILEGES;" >> ${FILENAME%.*}
                 gzip ${FILENAME%.*}
                 if [ $? -eq 1 ]; then
                     status=1         
                 fi
              else
                  /var/app/mysql-"$VERSION"/"$COMPONENT"/bin/mysqldump -u $DB_USER -p"$DB_PASS" --socket=$MYSQL_DATA/mysql.sock --port=$MYSQL_TCP_PORT $options $DB | gzip > $FILENAME
                 if [ $? -eq 1 ]; then
                     status=1
                 fi
              fi
        done
done

if [ $status -eq 1 ]; then
        mail -s "MySQL backup failed on `hostname`. Contact DBA immediately." $NOTIFY < /dev/null > /dev/null 2>&1
        exit 2
else
# remove backups older than $DAYS_KEEP
        find ${DIR}* -type d -mtime +$DAYS_KEEP -exec rm -fr {} \; 2> /dev/null
#find ${DIR}* -type d -mmin +$DAYS_KEEP -exec rm -rf {} \; 2> /dev/null
mail -s "MySQL backup succeeded on `hostname`" dba_msg  < /dev/null > /dev/null 2>&1
        exit 0
fi

