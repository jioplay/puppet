#!/bin/sh
set -x
### DESC
# This job will check for entries in error logs located under /var/log/mysql/*/* for the past 10 minus. Schedule via crontab to run every 10 minutes
######################

source param_file.cfg
tmp_mon=/tmp/mysql_mon_tmp
mon=/tmp/mysql_mon

##prep
if [ -a "$mon" ]; then
        rm $mon
fi

##main
for dir in $base_log/*/*
do
        if [ -s "$dir/$err_log" ]; then
                tline=`date --date="10 minutes ago" +\%g\%m\%d\ \%H\:\%M`
                grep "${tline%?}" "$dir/$err_log" > $tmp_mon
                if [ -s "$tmp_mon" ]; then
                        print_header "$dir/$err_log" >> $mon
                        cat $tmp_mon >> $mon
                fi
        fi
done
if [ -s $mon ]; then
        mail -s "MySQL messages from `hostname`." $notify < $mon > /dev/null 2>&1
fi

##cleanup

exit 0
