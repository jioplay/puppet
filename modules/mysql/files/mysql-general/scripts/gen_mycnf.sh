#!/bin/sh
#set -x

gen_myscnf=generic_mycnf
tempfile=/tmp/$gen_myscnf
cnf=my.cnf

# innodb_buffer_pool_size is set to 80% of RAM
ram=`grep MemTotal /proc/meminfo`

if [ -e $MYSQL_HOME/$cnf ]; then
    mv $MYSQL_HOME/$cnf $MYSQL_HOME/$cnf."$(date +%Y%m%d-%H%M)"
fi

# prepare files

if [ -e $gen_myscnf ]; then
        cp $gen_myscnf $tempfile
        sed -i "s#\$MYSQL_DATA#$MYSQL_DATA#g" $tempfile
        sed -i "s#\$MYSQL_HOME#$MYSQL_HOME#g" $tempfile
        sed -i "s#\$MYSQL_LOG#$MYSQL_LOG#g" $tempfile
        sed -i "s#\$MYSQL_TCP_PORT#$MYSQL_TCP_PORT#g" $tempfile
else
        echo 'Generic cnf file does not exist. Place proper file into local folder.'
        exit 2
fi

while read val
do
SKIPROW=0
if [ ${val:0:1} = "@" ]; then
#echo "1: $val"
VERTAG=`echo ${val%@*} | tr -d '@'`
#adding parameter
        if [ ${VERTAG:0:1} = "+" ]; then
                CHVER=${VERTAG:1:2}
                CURVER=`echo ${DB#*_*}|tr -d '_'`
                if [ $CURVER -lt $CHVER ]; then
                       SKIPROW=1
                fi
#echo "2: $SKIPROW"
#removing parameter if it's depreceted
                if [ ${#VERTAG} -eq 6 ]; then
                        CHVER=${VERTAG:4:2}
                        if [ ${VERTAG:3:1} = "-" ] && [ $CURVER -ge $CHVER ]; then
#echo ${#VERTAG}
#echo ${VERTAG:3:1}
#echo $CURVER
#echo $CHVER
                        SKIPROW=1
                        fi
                fi
        else
               echo 'Error is generic cnf file. Tags should be set at the beginning of a line and have format @+<version in which parameter is introduced-<version in which parameter is deprecated>@>'
               exit 2
       fi
#       val="${val#@*@}"
fi
#echo "3: $SKIPROW"
        if [ $SKIPROW -ne 1 ]; then
            if [[ $val =~ ^### ]] || [[ $val =~ \] ]]; then
                      echo >> $MYSQL_HOME/$cnf
                      elif [[ "$val" = "innodb_buffer_pool_size" ]]; then
			                              val="innodb_buffer_pool_size="`printf "%.0f" $(echo "${ram//[!0-9]/} * 0.8 / 1024" | bc -l)`"M"
	      fi
            val="${val#@*@}"
#echo 4: $val
            echo "$val" >> $MYSQL_HOME/$cnf
        fi
done < $tempfile

# cleanup
rm $tempfile

exit 0


