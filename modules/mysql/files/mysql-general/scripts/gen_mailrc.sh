#!/bin/sh

var=`hostname`

if [[ $var == *prod* ]]
then
	 alert_alias='dba@mobitv.com,noc-830@mobitv.com'
else
         alert_alias='dba@mobitv.com'
fi

  echo "alias dba_alert $alert_alias
alias dba_msg dba@mobitv.com
alias script_alert dba@mobitv.com" > $HOME/.mailrc

 if [ $? -eq 0 ]; then
      echo "mailrc generation failed" > /tmp/gen_mailrc
      exit 2
 else
      exit 0
 fi

