#!/bin/sh
set -x

if [ $# -lt 1 ]; then
        echo 1>&2 "$0: Not enough arguments! Please specify the number of days to keep logs (365)"
        exit 2
fi

SUBJECT="Log rotation on `hostname`"
MAIL=/bin/mail;
EMAIL=script_alert
DATE=$(date +%Y%m%d)
DAYS_KEEP=$1
export base_dir=/var/log/mysql
export archive=/mysql_arch/log_arch

mkdir -p $archive/"$DATE"

for dir in $base_dir/*/*
do
#copy logs to archive destination
        cd $dir
        inst_arch=$archive/"$DATE"/$(basename $(dirname $dir))/$(basename $dir)
        mkdir -p $inst_arch
        for f in *.log; do
        cp $f $inst_arch/$f
        > $f
        done
#archive per instance
        cd $inst_arch
        for f in *.log; do
        fn="${f%.*}
        zip $fn."$DATE".zip $f
        done
        rm $inst_arch/*log
done

find ${archive}* -type d -mtime +$DAYS_KEEP -exec rm -fr {} \; 2> /dev/null

rcode=$?

   if [ $rcode -ne 0 ]; then
       (echo "rotate_logs.sh script failed on `hostname`") | $MAIL -s "$SUBJECT" $EMAIL
       exit 2
   else
       exit 0
   fi
