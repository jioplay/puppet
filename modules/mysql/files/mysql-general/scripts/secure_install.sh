#!/bin/sh
#set -x

root_pass=$(cat $1)

run_statement() {
    "$MYSQL_HOME"/bin/mysql -uroot --password="" --socket="$MYSQL_DATA"/mysql.sock --port="$MYSQL_TCP_PORT" -e "$1"
    if [ $? -eq 0 ]; then
        echo " ... Success!...Statement: $1"
    else
        echo " ... Failed!...Statement: $1"
    fi
    return 0
}

run_statement "DROP DATABASE IF EXISTS test"
run_statement "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
run_statement "DELETE FROM mysql.user WHERE User=''"
run_statement "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost')"
run_statement "GRANT ALL PRIVILEGES ON *.* TO 'zyphir'@'%.mobitv.corp' IDENTIFIED BY PASSWORD '*DED656A8B30E4500B05FED872D5488A200B0299D'"
run_statement "GRANT ALL PRIVILEGES ON *.* TO 'zyphir'@'172.16.%' IDENTIFIED BY PASSWORD '*DED656A8B30E4500B05FED872D5488A200B0299D'"
run_statement "GRANT ALL PRIVILEGES ON *.* TO 'zyphir'@'localhost' IDENTIFIED BY PASSWORD '*DED656A8B30E4500B05FED872D5488A200B0299D' WITH GRANT OPTION;"
run_statement "GRANT SELECT ON *.* TO 'reader'@'172.16.%' IDENTIFIED BY PASSWORD '*5B00940B180474EC2B21F3EC2630750A44AB99B7'"
run_statement "GRANT SELECT ON *.* TO 'reader'@'%.mobitv.corp' IDENTIFIED BY PASSWORD  '*5B00940B180474EC2B21F3EC2630750A44AB99B7';"
run_statement "GRANT SHOW DATABASES, SELECT, LOCK TABLES, RELOAD,EVENT ON *.* to backup@localhost IDENTIFIED BY PASSWORD '*62D525773990DF2C800759B436648EB02AA62682';"
run_statement "UPDATE mysql.user SET Password=PASSWORD('$root_pass') WHERE User='root'"
run_statement "FLUSH PRIVILEGES"

exit 0
