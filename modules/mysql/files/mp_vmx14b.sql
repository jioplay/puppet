-- MySQL dump 10.11
--
-- Host: localhost    Database: mp
-- ------------------------------------------------------
-- Server version	5.0.95

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MP_CONFIG`
--

DROP TABLE IF EXISTS `MP_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MP_CONFIG` (
  `CONFIG_KEY` varchar(255) NOT NULL,
  `CONFIG_VALUE` varchar(4096) default NULL,
  `DEFAULT_CONFIG_VALUE` varchar(1024) default NULL,
  `DESCRIPTION` varchar(2048) default NULL,
  `LAST_MODIFIED` datetime default NULL,
  PRIMARY KEY  (`CONFIG_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MP_CONFIG`
--

LOCK TABLES `MP_CONFIG` WRITE;
/*!40000 ALTER TABLE `MP_CONFIG` DISABLE KEYS */;
INSERT INTO `MP_CONFIG` (`CONFIG_KEY`, `CONFIG_VALUE`, `DEFAULT_CONFIG_VALUE`, `DESCRIPTION`, `LAST_MODIFIED`) VALUES ('clip.combination.method','ee','mp','The method of applying pre-roll/post-roll clips (default is EE, anything else uses ','2012-10-12 23:21:03'),('encoder.elemental.encoder.url','http://elementalvip/jobs','http://elementalvip/jobs','The URL of the Elemental VOD encoder to submit encoding requests','2012-10-12 23:21:03'),('encoder.elemental.input.root','/var/Jukebox/elemental/Input','/var/Jukebox/elemental/Input','The input base directory for input to the Elemental VOD encoder.','2012-10-12 23:21:03'),('encoder.elemental.key','yffLm83ZuWouJ6LqMI','yffLm83ZuWouJ6LqMI','The passkey that the Media Processor uses to access the Elemental VOD encoder REST APIs','2012-10-12 23:21:03'),('encoder.elemental.originals.mount','/data/mnt/originals','/data/mnt/originals','The base directory for original clip files used on the Elemental encoder.','2012-10-12 23:21:03'),('encoder.elemental.output.root','/var/Jukebox/elemental/Output','/var/Jukebox/elemental/Output','The output base directory for output to the Elemental VOD encoder.','2012-10-12 23:21:03'),('encoder.elemental.status.to.url','http://juarnvip:8080/mp/elemental_response','http://juarnvip:8080/mp/elemental_response','The URL that the Elemental VOD encoder should use to send notifications about encoding operations','2012-10-12 23:21:03'),('encoder.elemental.url.signing.window','30','30','The number of seconds that signed Elemental encoder requests are valid for (default 30 seconds).','2012-10-12 23:21:03'),('encoder.elemental.user','cms','cms','The user account that the Media Processor uses to access the Elemental VOD encoder REST APIs','2012-10-12 23:21:03'),('encoder.elemental.workdir.mount','/data/mnt/workdir','/data/mnt/workdir','The base directory for Elemental encoder Input and Output directories.','2012-10-12 23:21:03'),('encoder.episode.cleanup','true','true','If the input and outpufolder shold be deleted after encoding batch been completed\n    should always be true, but for debug it may sometimes be usefull','2012-10-12 23:21:03'),('encoder.episode.clusters','media,simulator','media','A comma-separated list of the names of EE clusters.','2012-10-12 23:21:03'),('encoder.episode.default.outpath','mp','/var/Jukebox/ee/media/Output/mp/','If no outpath is provided in EncodingRequest, EpisodeEngineEncoder encodes files into this directory.\n    must end with a / character. IRTANT, this dir needs to be accessably by Episode Engine.','2012-10-12 23:21:03'),('encoder.episode.link.mode','symlink','symlink','This specifies how files should be copied for encoding into the Episode encoding folder.\n     Two values are supported \"copy\" or \"symlink\". \"copy\" is slower, but usefull when lack of\n     shared disk.','2012-10-12 23:21:03'),('encoder.episode.outfolder.root','/var/Jukebox/ee/','/var/Jukebox/ee/media/Output/','The root outfolder of Episode Engine. Make sure has read / write privs in this dir.','2012-10-12 23:21:03'),('encoder.episode.save.meta.data','false','false','If the meta data about every encoding files from Episode should be stored.','2012-10-12 23:21:03'),('encoder.episode.watchfolder.input.foldername','Input','Input','The name of the input directory relative to each cluster.','2012-10-12 23:21:03'),('encoder.episode.watchfolder.output.foldername','Output','Output','The name of the output directory relative to each cluster.','2012-10-12 23:21:03'),('encoder.episode.watchfolder.root','/var/Jukebox/ee/','/var/Jukebox/ee/media/Input/','The root watchfolder of Episode Engine. Make sure has read / write privs in this dir.','2012-10-12 23:21:03'),('encoder.widevine.input.root','/var/Jukebox/widevine/Input','/var/Jukebox/widevine/Input','The input base directory for input to the Widevine Packager ','2012-10-12 23:21:03'),('encoder.widevine.output.root','/var/Jukebox/widevine/Output','/var/Jukebox/widevine/Output','The output base directory for output from the Widevine Packager ','2012-10-12 23:21:03'),('encoder.widevine.packager.url','http://wvpkgrvip/widevine/voddealer/cgi-bin/packagenotify.cgi','http://wvpkgrvip/widevine/voddealer/cgi-bin/packagenotify.cgi','The URL of the Widevine Packager to submit packaging requests','2012-10-12 23:21:03'),('encoder.widevine.status.to.url','http://juarnvip:8080/mp/widevine_response','http://juarnvip:8080/mp/widevine_response','The URL that the Widevine Packager should use to send notifications about packaging operations','2012-10-12 23:21:03'),('ffmpeg.location','/usr/local/bin/ffmpeg','/usr/local/bin/ffmpeg','The location of where metadata files should store encoding.','2012-10-12 23:21:03'),('max.request.threads','10','1000','The maximum simultainusly threads can use for encoding requests / responses (JMS and WS).','2012-10-12 23:21:03'),('metadata.repository','/var/Jukebox/mp/metadata-repository','/var/Jukebox/mp/metadata-repository','The location of where metadata files should store encoding.','2012-10-12 23:21:03'),('setting.repository','/var/Jukebox/mp/setting-repository','/var/Jukebox/mp/setting-repository','The location where stores encoding setting files. Make sure has read/write\n    privs in this dir.','2012-10-12 23:21:03');
/*!40000 ALTER TABLE `MP_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MP_ENCODER_MAPPING`
--

DROP TABLE IF EXISTS `MP_ENCODER_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MP_ENCODER_MAPPING` (
  `ENCODING_ID` int(11) NOT NULL,
  `CLASS_NAME` varchar(1024) default NULL,
  `DESCRIPTION` varchar(1024) default NULL,
  `FILE_FORMAT` varchar(24) default NULL,
  `FILE_NAME` varchar(512) default NULL,
  PRIMARY KEY  (`ENCODING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MP_ENCODER_MAPPING`
--

LOCK TABLES `MP_ENCODER_MAPPING` WRITE;
/*!40000 ALTER TABLE `MP_ENCODER_MAPPING` DISABLE KEYS */;
INSERT INTO `MP_ENCODER_MAPPING` (`ENCODING_ID`, `CLASS_NAME`, `DESCRIPTION`, `FILE_FORMAT`, `FILE_NAME`) VALUES (1,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-1.setting'),(2,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-2.setting'),(3,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-3.setting'),(4,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-4.setting'),(5,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-5.setting'),(6,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-6.setting'),(7,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-7.setting'),(8,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-8.setting'),(11,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-11.setting'),(12,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-12.setting'),(13,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-13.setting'),(14,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-14.setting'),(15,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-15.setting'),(18,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-18.setting'),(21,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-21.setting'),(22,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-22.setting'),(23,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-23.setting'),(24,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-24.setting'),(25,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-25.setting'),(26,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-26.setting'),(27,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-27.setting'),(28,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-28.setting'),(40,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-40.setting'),(41,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-41.setting'),(60,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-60.setting'),(61,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-61.setting'),(62,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-62.setting'),(64,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-64.setting'),(65,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-65.setting'),(66,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-66.setting'),(67,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-67.setting'),(68,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-68.setting'),(69,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-69.setting'),(70,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-70-2.setting'),(71,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-71.setting'),(72,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-72.setting'),(73,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-73.setting'),(74,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-74.setting'),(75,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-75.setting'),(77,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-77.setting'),(78,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-78.setting'),(79,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-79.setting'),(91,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-91.setting'),(93,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-93.setting'),(94,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-94.setting'),(95,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','encoding-95.setting'),(100,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'wmv','encoding-100.mbrsetting'),(101,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'wmv','encoding-101.mbrsetting'),(102,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'wmv','encoding-102.mbrsetting'),(103,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','fourthreemaster.setting'),(104,'com.mobitv.arlanda.mediaprocessor.encoder.EpisodeEngineEncoder',NULL,'3gp','sixteenninemaster.setting');
/*!40000 ALTER TABLE `MP_ENCODER_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-05-21 23:35:08
