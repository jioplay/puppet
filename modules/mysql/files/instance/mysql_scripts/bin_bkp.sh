#!/bin/sh
set -x

if [ $# -lt 1 ]; then
        echo 1>&2 "$0: Not enough arguments! Please specify the number of days to keep backups (3)"
        exit 2
fi

DIR=/mysql_arch/backup_bin
DATESTAMP=$(date +%Y%m%d-%H%M)
#BKPDIR=$DIR/"$DATESTAMP"
#DB_USER=backup
#DB_PASS='unbreaKab1e'
DAYS_KEEP=$1 #backup retention
#NOTIFY=dba@mobitv.com
NOTIFY=aalexeeva@mobitv.com
myport=port3306

# file of the day
curr_bkp_file=`echo $DIR/$myport/bin_"$(date +%m%d-2014)".sql`

# file of the day
MYSQL_DATA=/var/lib/mysql

# make sure that initial directory/files exist/created
if [ ! -d "$DIR/$myport" ]; then
	mkdir -p $DIR/$myport
        touch $curr_bkp_file
fi
# every day create a new file
	switch=`find $DIR/$myport -mtime +1 | wc -l`
        if [ $switch -gt 0 ]; then
        	touch $curr_bkp_file
        fi
# copy last 15 minutes od transactions
	export curr_bin_file=`tail -1 $MYSQL_DATA/mysql-bin.index | awk '{print substr($0,3)}'`
        mysqlbinlog --start-datetime="`date --date="15 minutes ago" +\%Y-\%m-\%d\ \%H:\%M:\%S`" --stop-datetime="`date +\%Y-\%m-\%d\ \%H:\%M:\%S`" $MYSQL_DATA/"$curr_bin_file" >> $curr_bkp_file



exit 0
