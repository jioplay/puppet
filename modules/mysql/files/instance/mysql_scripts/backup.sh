#!/bin/sh
set -x
#######################
#Requirenment
# 1. GRANT SHOW DATABASES, SELECT, LOCK TABLES, RELOAD ON *.* to backup@localhost IDENTIFIED BY 'unbreaKab1e'; FLUSH PRIVILEGES;
# http://dev.mysql.com/doc/mysql-enterprise-backup/3.6/en/mysqlbackup.privileges.html
#######################
if [ $# -lt 1 ]; then
        echo 1>&2 "$0: Not enough arguments! Please specify the number of days to keep backups (7)"
        exit 2
fi

DIR=/mysql_arch/backup
DATESTAMP=$(date +%Y%m%d-%H%M)
BKPDIR=$DIR/"$DATESTAMP"
DB_USER=backup
DB_PASS='unbreaKab1e'
DAYS_KEEP=$1 #backup retention
EXCL_DB='information_schema|performance_schema'
#NOTIFY=`logname`
#if [ "$NOTIFY" == "mysql" ]; then
#        NOTIFY=dba
#fi
NOTIFY=script_alert
chk=0

# create backups securely
umask 006

# list MySQL databases (port 3306) and dump each
mkdir -p $BKPDIR/port3306
        DB_LIST=`mysql -u $DB_USER -p"$DB_PASS" -e'show databases;' | egrep -v $EXCL_DB`
if [ $? != 0 ]; then
        chk=1
fi
        DB_LIST=${DB_LIST##Database}
        for DB in $DB_LIST;
        do
                FILENAME=${BKPDIR}/port3306/${DB}-${DATESTAMP}.sql.gz
                options='--opt --skip-lock-tables --single-transaction'
                if [ "$DB" = "mysql" ]; then
                        mysqldump -u $DB_USER -p"$DB_PASS" $options $DB > ${FILENAME%.*}
                        echo "FLUSH PRIVILEGES;" >> ${FILENAME%.*}
                        gzip ${FILENAME%.*}
                else
                        mysqldump -u $DB_USER -p"$DB_PASS" $options $DB | gzip > $FILENAME
                fi
        done
if [ $? != 0 ]; then
        chk=1
fi
# list MySQL databases (port 3307) and dump each
mkdir -p $BKPDIR/port3307
        DB_LIST=`/usr/bin/mysql_third_party/bin/mysql -u $DB_USER -p"$DB_PASS" --socket=/var/lib/mysql_third_party/mysql.sock --port=3307 -e'show databases;' | egrep -v $EXCL_DB`
if [ $? != 0 ]; then
        chk=1
fi
        DB_LIST=${DB_LIST##Database}
        for DB in $DB_LIST;
        do
                FILENAME=${BKPDIR}/port3307/${DB}-${DATESTAMP}.sql.gz
                options='--opt --skip-lock-tables --single-transaction'
                if [ "$DB" = "mysql" ]; then
                        /usr/bin/mysql_third_party/bin/mysqldump -u $DB_USER -p"$DB_PASS" --socket=/var/lib/mysql_third_party/mysql.sock --port=3307 $options $DB > ${FILENAME%.*}
                        echo "FLUSH PRIVILEGES;" >> ${FILENAME%.*}
                        gzip ${FILENAME%.*}
                else
                        /usr/bin/mysql_third_party/bin/mysqldump -u $DB_USER -p"$DB_PASS" --socket=/var/lib/mysql_third_party/mysql.sock --port=3307 $options $DB | gzip > $FILENAME
                fi
        done
if [ $? != 0 ]; then
        chk=1
fi
if [ $chk -eq 0 ]; then
# remove backups older than $DAYS_KEEP
#        find ${DIR}* -name "*.sql*" -mtime +$DAYS_KEEP -exec rm -fr {} \; 2> /dev/null
		find ${DIR}* -type d -mtime +$DAYS_KEEP -exec rm -rf {} \; 2> /dev/null
        exit 0
else
#       mail -s "MySQL backup failed on `hostname`. Contact DBA immediately." "$NOTIFY"@mobitv.com < /dev/null > /dev/null 2>&1
       mail -s "MySQL backup failed on `hostname`. Contact DBA immediately." script_alert < /dev/null > /dev/null 2>&1
        exit 2
fi
