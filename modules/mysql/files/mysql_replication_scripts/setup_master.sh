#!/bin/sh
#set -x
if [ $# -ne 3 ];then
   echo 'Usage:$0 <SLAVE_FQDN> $1 <MSOCKET> $2 <MPORT>'
   exit 1;
fi

SLAVE_FQDN=$1
MSOCKET=$2
MPORT=$3
MUSR=root
MPSWD=TE1WeuHiflzmLMxi
bkp_file=/var/tmp/rep_dump_"$MPORT".sql
cur_dir=`pwd`
master_f="${cur_dir}/setup_master.sql"
slave_f="${cur_dir}/setup_slave.sql"
mm_slave_f="${cur_dir}/setup_mm_slave.sql"
SLAVE_IP=$(echo `nslookup $SLAVE_FQDN | cut -f 4 | grep Address | awk {'print $2'}`)
MASTER_IP=`hostname -i`
MASTER_FQDN=`hostname`


rm -f /tmp/err_set_seq

##################################################################################### set starting sequence for auto_increment

proc_exists=`mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -e "SHOW PROCEDURE STATUS like 'set_seq' \G" | grep set_seq | awk {'print $2'}`
if [ "$proc_exists" = 'set_seq' ];then  
	mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -e "drop procedure dba.set_seq;"
fi

echo "CREATE DATABASE IF NOT EXISTS dba DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;

use dba;
 
drop procedure if exists dba.set_seq;

DELIMITER //
CREATE PROCEDURE dba.set_seq(id_server TINYINT(1))
safePoint:BEGIN
DECLARE no_more_tables INT DEFAULT FALSE;
DECLARE cursor_SCH,cursor_TBL  VARCHAR(50);
DECLARE cursor_i CURSOR FOR select TABLE_SCHEMA,TABLE_NAME from information_schema.TABLES where TABLE_SCHEMA not in ('mysql','information_schema','performance_schema') and AUTO_INCREMENT mod 2 = id_server-1;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_tables = TRUE;

SET session sql_log_bin = 0;
OPEN cursor_i;
read_loop: LOOP
FETCH cursor_i INTO cursor_SCH, cursor_TBL;
IF no_more_tables THEN
LEAVE read_loop;
END IF;
select max(AUTO_INCREMENT),max(AUTO_INCREMENT) mod 2 into @new_start, @id_seq from information_schema.TABLES where TABLE_SCHEMA=cursor_SCH and TABLE_NAME=cursor_TBL;
IF id_server = 1 THEN
        IF @id_seq = 0 THEN
        SET @new_start=@new_start+1;
        END IF;
ELSEIF id_server = 2 THEN
        IF @id_seq = 1 THEN
        SET @new_start=@new_start+1;
        END IF;
ELSE
        select 'Wrong server id. Acceptable values are 1, 2';
        LEAVE safePoint;
END IF;
SET @query = CONCAT('ALTER TABLE ', cursor_SCH, '.\`',cursor_TBL, '\` auto_increment=', @new_start);
PREPARE stmt FROM @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END LOOP;
CLOSE cursor_i;
END;//

DELIMITER ;
" > proc_set_seq.sql

mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -e"source proc_set_seq.sql; select MID(@@hostname,7,1) into @id_server; call dba.set_seq(@id_server);"

if [ -e /tmp/err_set_seq ]; then 
	echo "Procedure set_seq failed. Check /tmp/err_set_seq"
	exit 2
fi	

##################################################################################### setup master
echo "
GRANT REPLICATION SLAVE ON *.* TO 'rep_user'@'$SLAVE_IP' IDENTIFIED BY 'sFHto8jno99jf_9';
GRANT REPLICATION SLAVE ON *.* TO 'rep_user'@'$SLAVE_FQDN' IDENTIFIED BY 'sFHto8jno99jf_9';
FLUSH PRIVILEGES;
FLUSH TABLES WITH READ LOCK;
" > $master_f

mysql --socket=$MSOCKET -u$MUSR -p$MPSWD < $master_f

##################################################################################### create slave file
echo "
GRANT USAGE on *.* to 'rep_user'@'$MASTER_IP' IDENTIFIED BY 'sFHto8jno99jf_9';
GRANT REPLICATION SLAVE ON *.* TO 'rep_user'@'$MASTER_IP';
GRANT USAGE on *.* to 'rep_user'@'$MASTER_FQDN' IDENTIFIED BY 'sFHto8jno99jf_9';
GRANT REPLICATION SLAVE ON *.* TO 'rep_user'@'$MASTER_FQDN';
FLUSH PRIVILEGES;

STOP SLAVE;
RESET SLAVE;

CHANGE MASTER TO
MASTER_HOST='$MASTER_IP',
MASTER_PORT = $MPORT,
MASTER_USER='rep_user',
MASTER_PASSWORD='sFHto8jno99jf_9',
MASTER_LOG_FILE='" > $slave_f

master_log=`mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -A -e "SHOW MASTER STATUS\G" | grep 'File' |awk '{print $2}'`"',"
sed -i '${s/$/'"$master_log/}" $slave_f
echo "MASTER_LOG_POS=" >> $slave_f
master_pos=`mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -A -e "SHOW MASTER STATUS\G" | grep 'Position' |awk '{print $2}'`
sed -i '${s/$/'"$master_pos/}" $slave_f
echo ";
#START SLAVE;" >> $slave_f

echo "SET session sql_log_bin = 0;
SET global general_log=OFF;" > $bkp_file
mysqldump --socket=$MSOCKET -u$MUSR -p$MPSWD --all-databases >> $bkp_file
echo "SET session sql_log_bin = 1;
SET global general_log=ON;" >> $bkp_file

mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -e "UNLOCK TABLES"
##################################################################################### copy files to slave
scp -p -C $bkp_file $SLAVE_IP:/var/tmp/.
scp -p -C $slave_f $SLAVE_IP:"${cur_dir}/."

exit 0
