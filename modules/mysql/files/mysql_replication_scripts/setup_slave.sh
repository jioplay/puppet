#!/bin/sh
#set -x
if [ $# -lt 2 ];then
   echo 'Usage:$0 <SLAVE_FQDN>(Optional) $1 <MSOCKET> $2 <MPORT>'
   exit 1;
fi

MSOCKET=$2
MPORT=$3
MUSR=root
MPSWD=TE1WeuHiflzmLMxi
bkp_file=/var/tmp/rep_dump_"$MPORT".sql
cur_dir=`pwd`
master_f="${cur_dir}/setup_master.sql"
slave_f="${cur_dir}/setup_slave.sql"
mm_slave_f="${cur_dir}/setup_mm_slave.sql"

rm -f /tmp/err_set_seq

mysql --socket=$MSOCKET -u$MUSR -p$MPSWD < $bkp_file

proc_exists=`mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -e "SHOW PROCEDURE STATUS like 'set_seq' \G" | grep set_seq | awk {'print $2'}`
if [ "$proc_exists" != 'set_seq' ];then  
	echo "Procedure dba.set_seq does not exist. Check mysql dump /var/tmp/rep_dump_"$MPORT".sql. It should not be the case"
fi

mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -e"select MID(@@hostname,7,1) into @id_server;call dba.set_seq(@id_server);"
if [ -e /tmp/err_set_seq ]; then 
	echo "Procedure set_seq failed. Check /tmp/err_set_seq"
	exit 2
fi	

mysql --socket=$MSOCKET -u$MUSR -p$MPSWD < $slave_f
mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -e "START SLAVE"

if [ $# -eq 3 ]; then
   echo 'configuration for MASTER-MASTER replication'
   SLAVE_FQDN=$1
   MASTER_IP=`hostname -i`
   echo "
   STOP SLAVE;
   RESET SLAVE;
   
   CHANGE MASTER TO
   MASTER_HOST='$MASTER_IP',
   MASTER_PORT = $MPORT,
   MASTER_USER='rep_user',
   MASTER_PASSWORD='sFHto8jno99jf_9',
   MASTER_LOG_FILE='" > $mm_slave_f

   master_log=`mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -A -e "SHOW MASTER STATUS\G" | grep 'File' |awk '{print $2}'`"',"
   sed -i '${s/$/'"$master_log/}" $mm_slave_f
   echo "MASTER_LOG_POS=" >> $mm_slave_f
   master_pos=`mysql --socket=$MSOCKET -u$MUSR -p$MPSWD -A -e "SHOW MASTER STATUS\G" | grep 'Position' |awk '{print $2}'`
   sed -i '${s/$/'"$master_pos/}" $mm_slave_f
   echo ";
   START SLAVE;" >> $mm_slave_f

scp -p -C $mm_slave_f $SLAVE_FQDN:$mm_slave_f

fi

exit 0
