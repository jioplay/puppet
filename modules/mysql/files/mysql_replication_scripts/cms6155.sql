CREATE DATABASE IF NOT EXISTS `dba` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;

use dba;
 
drop procedure if exists dba.set_seq;

DELIMITER //
CREATE PROCEDURE dba.set_seq(id_server TINYINT(1))
safePoint:BEGIN
DECLARE no_more_tables INT DEFAULT FALSE;
DECLARE cursor_SCH,cursor_TBL  VARCHAR(50);
DECLARE cursor_i CURSOR FOR select TABLE_SCHEMA,TABLE_NAME from information_schema.TABLES where TABLE_SCHEMA not in ('mysql','information_schema','performance_schema') and AUTO_INCREMENT mod 2 = id_server-1;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_tables = TRUE;

SET session sql_log_bin = 0;
OPEN cursor_i;
read_loop: LOOP
FETCH cursor_i INTO cursor_SCH, cursor_TBL;
IF no_more_tables THEN
LEAVE read_loop;
END IF;
select max(AUTO_INCREMENT),max(AUTO_INCREMENT) mod 2 into @new_start, @id_seq from information_schema.TABLES where TABLE_SCHEMA=cursor_SCH and TABLE_NAME=cursor_TBL;
IF id_server = 1 THEN
        IF @id_seq = 0 THEN
        SET @new_start=@new_start+1;
        END IF;
ELSEIF id_server = 2 THEN
        IF @id_seq = 1 THEN
        SET @new_start=@new_start+1;
        END IF;
ELSE
        select "Wrong server id. Acceptable values are 1, 2";
        LEAVE safePoint;
END IF;
select id_server, @new_start, @id_seq;
SET @query = CONCAT('ALTER TABLE ', cursor_SCH, '.`',cursor_TBL, '` auto_increment=', @new_start);
PREPARE stmt FROM @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END LOOP;
CLOSE cursor_i;
END;//

DELIMITER ;

