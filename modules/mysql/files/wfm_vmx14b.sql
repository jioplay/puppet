-- MySQL dump 10.11
--
-- Host: localhost    Database: wfm
-- ------------------------------------------------------
-- Server version	5.0.95

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `WFM_ASSET_CONTEXT`
--

DROP TABLE IF EXISTS `WFM_ASSET_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_ASSET_CONTEXT` (
  `id` bigint(20) NOT NULL auto_increment,
  `ASSET_ID` bigint(20) default NULL,
  `CARRIER_APPROVED` bit(1) default NULL,
  `EXPIRES` varchar(255) default NULL,
  `INVENTORY_ID` bigint(20) default NULL,
  `MEDIA_ASPECT_RATIO` varchar(255) default NULL,
  `MEDIA_URL` text,
  `METADATA_URL` text,
  `POST_ROLL_URL` text,
  `PRE_ROLL_URL` text,
  `PROVIDER_ID` bigint(20) default NULL,
  `VERSION` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_ASSET_CONTEXT`
--

LOCK TABLES `WFM_ASSET_CONTEXT` WRITE;
/*!40000 ALTER TABLE `WFM_ASSET_CONTEXT` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_ASSET_CONTEXT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_EMAIL_NOTIFICATION`
--

DROP TABLE IF EXISTS `WFM_EMAIL_NOTIFICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_EMAIL_NOTIFICATION` (
  `id` bigint(20) NOT NULL auto_increment,
  `EMAIL_ADDRESS` text NOT NULL,
  `NOTIFICATION_TYPE` varchar(64) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_EMAIL_NOTIFICATION`
--

LOCK TABLES `WFM_EMAIL_NOTIFICATION` WRITE;
/*!40000 ALTER TABLE `WFM_EMAIL_NOTIFICATION` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_EMAIL_NOTIFICATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_ENCODING`
--

DROP TABLE IF EXISTS `WFM_ENCODING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_ENCODING` (
  `id` bigint(20) NOT NULL auto_increment,
  `DESCRIPTION` text,
  `IS_ENCRYPTED` bit(1) default NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_ENCODING`
--

LOCK TABLES `WFM_ENCODING` WRITE;
/*!40000 ALTER TABLE `WFM_ENCODING` DISABLE KEYS */;
INSERT INTO `WFM_ENCODING` (`id`, `DESCRIPTION`, `IS_ENCRYPTED`, `NAME`) VALUES (21,'Superchunk master format.','\0','Master'),(22,'Legacy chunk encoding set.','\0','Chunk'),(23,'Standard RTSP encoding set.','\0','RTSP'),(24,'Windows Media / Mobile encoding set.','\0','WinMo'),(25,'Windows Media / PCTV encoding set.','\0','PCTV'),(26,'Audio-only encoding set.','\0','Audio'),(41,'Progressive Download encoding set','\0','ProgressiveDownload'),(61,'DRM Encoding','','DRM'),(81,'Apple Segmented Vod encoding set','\0','AppleSegmentedVod'),(101,'RTSP 500 and 800 Kbps QVGA','\0','RSTP High'),(102,'ProgressiveDownload VGA 800','\0','ProgressiveDownload VGA 800'),(121,'RTSP 500 and 800 Kbps WQVGA','\0','RTSP 16x9'),(122,'Progressive Download WQVGA encoding set','\0','ProgressiveDownload 16x9'),(141,'Fragmented MP4 encoding set','\0','FragmentedMp4'),(181,'DRM Encoding for 16x9','','DRM 16x9'),(191,'Mobile HD ProgressiveDownload','\0','Mobile HD ProgressiveDownload'),(192,'Mobile HD RTSP','\0','Mobile HD RTSP'),(201,'Core QVGA encoding set','\0','Core'),(202,'Core WQVGA encoding set','\0','Core 16x9'),(221,'WVGA PGDL 4x3 encoding set','\0','WVGA ProgressiveDownload'),(222,'WVGA PGDL 16x9 encoding set','\0','WVGA ProgressiveDownload 16x9');
/*!40000 ALTER TABLE `WFM_ENCODING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_ENCODING_JOB`
--

DROP TABLE IF EXISTS `WFM_ENCODING_JOB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_ENCODING_JOB` (
  `id` bigint(20) NOT NULL auto_increment,
  `ENCODING_JOB_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=629 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_ENCODING_JOB`
--

LOCK TABLES `WFM_ENCODING_JOB` WRITE;
/*!40000 ALTER TABLE `WFM_ENCODING_JOB` DISABLE KEYS */;
INSERT INTO `WFM_ENCODING_JOB` (`id`, `ENCODING_JOB_ID`) VALUES (61,11),(74,100),(121,41),(122,40),(181,102),(182,101),(361,73),(401,1),(402,2),(403,3),(404,4),(405,5),(406,6),(407,7),(408,8),(409,12),(410,13),(411,14),(412,15),(413,18),(462,21),(463,23),(464,22),(465,25),(466,24),(467,27),(468,26),(521,94),(522,91),(523,93),(524,95),(569,70),(570,71),(571,77),(572,78),(573,79),(574,72),(575,73),(576,74),(577,68),(578,69),(579,64),(580,67),(581,62),(582,61),(583,60),(601,103),(602,104),(626,65),(627,66),(628,75);
/*!40000 ALTER TABLE `WFM_ENCODING_JOB` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_ENCODING_MAP`
--

DROP TABLE IF EXISTS `WFM_ENCODING_MAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_ENCODING_MAP` (
  `WFM_ENCODING_ID` bigint(20) NOT NULL,
  `MP_ENCODING_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`WFM_ENCODING_ID`,`MP_ENCODING_ID`),
  KEY `FK_ENCODING_MAP_02` (`MP_ENCODING_ID`),
  KEY `FK_ENCODING_MAP_01` (`WFM_ENCODING_ID`),
  CONSTRAINT `FK_ENCODING_MAP_01` FOREIGN KEY (`WFM_ENCODING_ID`) REFERENCES `WFM_ENCODING` (`id`),
  CONSTRAINT `FK_ENCODING_MAP_02` FOREIGN KEY (`MP_ENCODING_ID`) REFERENCES `WFM_ENCODING_JOB` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_ENCODING_MAP`
--

LOCK TABLES `WFM_ENCODING_MAP` WRITE;
/*!40000 ALTER TABLE `WFM_ENCODING_MAP` DISABLE KEYS */;
INSERT INTO `WFM_ENCODING_MAP` (`WFM_ENCODING_ID`, `MP_ENCODING_ID`) VALUES (21,61),(25,74),(26,121),(26,122),(24,181),(24,182),(102,361),(23,401),(23,402),(23,403),(23,404),(23,405),(23,406),(23,407),(23,408),(23,409),(23,410),(23,411),(23,412),(23,413),(121,462),(121,463),(121,464),(121,465),(121,466),(121,467),(121,468),(61,521),(61,522),(181,523),(181,524),(41,569),(41,570),(41,571),(41,572),(41,573),(41,574),(41,575),(41,576),(122,577),(122,578),(122,579),(122,580),(122,581),(122,582),(122,583),(201,601),(202,602),(221,626),(221,627),(221,628);
/*!40000 ALTER TABLE `WFM_ENCODING_MAP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_MESSAGE_MAP`
--

DROP TABLE IF EXISTS `WFM_MESSAGE_MAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_MESSAGE_MAP` (
  `id` bigint(20) NOT NULL auto_increment,
  `CORRELATION_ID` varchar(255) NOT NULL,
  `MESSAGE_ID` varchar(255) NOT NULL,
  `TASK_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_TASK_MESSAGE_MAP_01` (`TASK_ID`),
  CONSTRAINT `FK_TASK_MESSAGE_MAP_01` FOREIGN KEY (`TASK_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1655 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_MESSAGE_MAP`
--

LOCK TABLES `WFM_MESSAGE_MAP` WRITE;
/*!40000 ALTER TABLE `WFM_MESSAGE_MAP` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_MESSAGE_MAP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_PROVIDER_CONTEXT`
--

DROP TABLE IF EXISTS `WFM_PROVIDER_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_PROVIDER_CONTEXT` (
  `id` bigint(20) NOT NULL,
  `ADVERTISING_SOURCE` varchar(255) default NULL,
  `CONTENT_AUTH_MODE` varchar(255) NOT NULL,
  `ENCRYPTION_REQUIRED` varchar(255) NOT NULL default 'TRUE',
  `PRIORITY` int(11) NOT NULL,
  `TRANSCODING_CLUSTER` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_PROVIDER_CONTEXT`
--

LOCK TABLES `WFM_PROVIDER_CONTEXT` WRITE;
/*!40000 ALTER TABLE `WFM_PROVIDER_CONTEXT` DISABLE KEYS */;
INSERT INTO `WFM_PROVIDER_CONTEXT` (`id`, `ADVERTISING_SOURCE`, `CONTENT_AUTH_MODE`, `ENCRYPTION_REQUIRED`, `PRIORITY`, `TRANSCODING_CLUSTER`) VALUES (2,NULL,'NotRequired','TRUE',500,'simulator');
/*!40000 ALTER TABLE `WFM_PROVIDER_CONTEXT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_PROVIDER_ENCODING`
--

DROP TABLE IF EXISTS `WFM_PROVIDER_ENCODING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_PROVIDER_ENCODING` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `ENCODING_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`PROVIDER_ID`,`ENCODING_ID`),
  KEY `FK_PROVIDER_ENCODING_02` (`ENCODING_ID`),
  KEY `FK_PROVIDER_ENCODING_01` (`PROVIDER_ID`),
  CONSTRAINT `FK_PROVIDER_ENCODING_01` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `WFM_PROVIDER_CONTEXT` (`id`),
  CONSTRAINT `FK_PROVIDER_ENCODING_02` FOREIGN KEY (`ENCODING_ID`) REFERENCES `WFM_ENCODING` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_PROVIDER_ENCODING`
--

LOCK TABLES `WFM_PROVIDER_ENCODING` WRITE;
/*!40000 ALTER TABLE `WFM_PROVIDER_ENCODING` DISABLE KEYS */;
INSERT INTO `WFM_PROVIDER_ENCODING` (`PROVIDER_ID`, `ENCODING_ID`) VALUES (2,21),(2,23),(2,41),(2,81),(2,141);
/*!40000 ALTER TABLE `WFM_PROVIDER_ENCODING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_PROVIDER_NOTIFICATION`
--

DROP TABLE IF EXISTS `WFM_PROVIDER_NOTIFICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_PROVIDER_NOTIFICATION` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `NOTIFICATION_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`PROVIDER_ID`,`NOTIFICATION_ID`),
  UNIQUE KEY `NOTIFICATION_ID` (`NOTIFICATION_ID`),
  KEY `FK_PROVIDER_NOTIFICATION_01` (`PROVIDER_ID`),
  KEY `FK_PROVIDER_NOTIFICATION_02` (`NOTIFICATION_ID`),
  CONSTRAINT `FK_PROVIDER_NOTIFICATION_01` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `WFM_PROVIDER_CONTEXT` (`id`),
  CONSTRAINT `FK_PROVIDER_NOTIFICATION_02` FOREIGN KEY (`NOTIFICATION_ID`) REFERENCES `WFM_EMAIL_NOTIFICATION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_PROVIDER_NOTIFICATION`
--

LOCK TABLES `WFM_PROVIDER_NOTIFICATION` WRITE;
/*!40000 ALTER TABLE `WFM_PROVIDER_NOTIFICATION` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_PROVIDER_NOTIFICATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_PROVIDER_WORKFLOW`
--

DROP TABLE IF EXISTS `WFM_PROVIDER_WORKFLOW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_PROVIDER_WORKFLOW` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `WORKFLOW_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`PROVIDER_ID`,`WORKFLOW_ID`),
  KEY `FK_PROVIDER_WORKFLOW_01` (`PROVIDER_ID`),
  KEY `FK_PROVIDER_WORKFLOW_02` (`WORKFLOW_ID`),
  CONSTRAINT `FK_PROVIDER_WORKFLOW_01` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `WFM_PROVIDER_CONTEXT` (`id`),
  CONSTRAINT `FK_PROVIDER_WORKFLOW_02` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `WFM_WORKFLOW` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_PROVIDER_WORKFLOW`
--

LOCK TABLES `WFM_PROVIDER_WORKFLOW` WRITE;
/*!40000 ALTER TABLE `WFM_PROVIDER_WORKFLOW` DISABLE KEYS */;
INSERT INTO `WFM_PROVIDER_WORKFLOW` (`PROVIDER_ID`, `WORKFLOW_ID`) VALUES (2,1);
/*!40000 ALTER TABLE `WFM_PROVIDER_WORKFLOW` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_BLOB_TRIGGERS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_BLOB_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_BLOB_TRIGGERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `TRIGGER_NAME` (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `WFM_QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `WFM_QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_BLOB_TRIGGERS`
--

LOCK TABLES `WFM_QRTZ_BLOB_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_BLOB_TRIGGERS` DISABLE KEYS */;
INSERT INTO `WFM_QRTZ_BLOB_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`, `BLOB_DATA`) VALUES ('workflowCleanupTrigger','DEFAULT','��\0sr\05org.springframework.scheduling.quartz.CronTriggerBean��mD��~.\0L\0beanNamet\0Ljava/lang/String;L\0	jobDetailt\0Lorg/quartz/JobDetail;xr\0org.quartz.CronTrigger����y�\0L\0cronExt\0Lorg/quartz/CronExpression;L\0endTimet\0Ljava/util/Date;L\0nextFireTimeq\0~\0L\0previousFireTimeq\0~\0L\0	startTimeq\0~\0xr\0org.quartz.Trigger��W;\r���\0I\0misfireInstructionI\0priorityZ\0\nvolatilityL\0calendarNameq\0~\0L\0descriptionq\0~\0L\0fireInstanceIdq\0~\0L\0groupq\0~\0L\0\njobDataMapt\0Lorg/quartz/JobDataMap;L\0jobGroupq\0~\0L\0jobNameq\0~\0L\0nameq\0~\0L\0triggerListenerst\0Ljava/util/LinkedList;xp\0\0\0\0\0\0\0\0pppt\0DEFAULTsr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xp\0sr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0\0x\0q\0~\0\nt\0workflowCleanupJobt\0workflowCleanupTriggersr\0java.util.LinkedList)S]J`�\"\0\0xpw\0\0\0\0xsr\0org.quartz.CronExpression\0\0\0�~/\0L\0cronExpressionq\0~\0L\0timeZonet\0Ljava/util/TimeZone;xpt\00 0 0 * * ?sr\0\Zsun.util.calendar.ZoneInfo$���\0q�\0I\0checksumI\0\ndstSavingsI\0	rawOffsetI\0\rrawOffsetDiffZ\0willGMTOffsetChange[\0offsetst\0[I[\0simpleTimeZoneParamsq\0~\0[\0transitionst\0[Jxr\0java.util.TimeZone1���wD��\0L\0IDq\0~\0xpt\0Etc/UTC\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ppppsr\0java.util.Datehj�KYt\0\0xpw\0\0>ɉ\0xpsq\0~\0 w\0\0>��g xq\0~\0sr\0org.quartz.JobDetail��H�d��\0	Z\0\ndurabilityZ\0\rshouldRecoverZ\0\nvolatilityL\0descriptionq\0~\0L\0groupq\0~\0L\0jobClasst\0Ljava/lang/Class;L\0\njobDataMapq\0~\0L\0jobListenerst\0Ljava/util/Set;L\0nameq\0~\0xp\0\0\0pq\0~\0\nvr\0(com.mobitv.arlanda.wfm.tasks.CleanupTask\0\0\0\0\0\0\0\0\0\0\0xpsq\0~\0\0sq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0\0x\0sr\01org.apache.commons.collections.set.ListOrderedSet�Ӟ���S\0L\0setOrdert\0Ljava/util/List;xr\0Corg.apache.commons.collections.set.AbstractSerializableSetDecorator�k�\0\0xpsr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0\0xxsr\0java.util.ArrayListx����a�\0I\0sizexp\0\0\0\0w\0\0\0\0xq\0~\0');
/*!40000 ALTER TABLE `WFM_QRTZ_BLOB_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_CALENDARS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_CALENDARS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_CALENDARS` (
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY  (`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_CALENDARS`
--

LOCK TABLES `WFM_QRTZ_CALENDARS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_CALENDARS` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_QRTZ_CALENDARS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_CRON_TRIGGERS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_CRON_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_CRON_TRIGGERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) default NULL,
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `TRIGGER_NAME` (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `WFM_QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `WFM_QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_CRON_TRIGGERS`
--

LOCK TABLES `WFM_QRTZ_CRON_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_CRON_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_QRTZ_CRON_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_FIRED_TRIGGERS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_FIRED_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_FIRED_TRIGGERS` (
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `IS_VOLATILE` varchar(1) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) default NULL,
  `JOB_GROUP` varchar(200) default NULL,
  `IS_STATEFUL` varchar(1) default NULL,
  `REQUESTS_RECOVERY` varchar(1) default NULL,
  PRIMARY KEY  (`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_FIRED_TRIGGERS`
--

LOCK TABLES `WFM_QRTZ_FIRED_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_FIRED_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_QRTZ_FIRED_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_JOB_DETAILS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_JOB_DETAILS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_JOB_DETAILS` (
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) default NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_VOLATILE` varchar(1) NOT NULL,
  `IS_STATEFUL` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY  (`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_JOB_DETAILS`
--

LOCK TABLES `WFM_QRTZ_JOB_DETAILS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_JOB_DETAILS` DISABLE KEYS */;
INSERT INTO `WFM_QRTZ_JOB_DETAILS` (`JOB_NAME`, `JOB_GROUP`, `DESCRIPTION`, `JOB_CLASS_NAME`, `IS_DURABLE`, `IS_VOLATILE`, `IS_STATEFUL`, `REQUESTS_RECOVERY`, `JOB_DATA`) VALUES ('workflowCleanupJob','DEFAULT',NULL,'com.mobitv.arlanda.wfm.tasks.CleanupTask','0','0','0','0','��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xp\0sr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0\0x\0');
/*!40000 ALTER TABLE `WFM_QRTZ_JOB_DETAILS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_JOB_LISTENERS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_JOB_LISTENERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_JOB_LISTENERS` (
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `JOB_LISTENER` varchar(200) NOT NULL,
  PRIMARY KEY  (`JOB_NAME`,`JOB_GROUP`,`JOB_LISTENER`),
  KEY `JOB_NAME` (`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `WFM_QRTZ_JOB_LISTENERS_ibfk_1` FOREIGN KEY (`JOB_NAME`, `JOB_GROUP`) REFERENCES `WFM_QRTZ_JOB_DETAILS` (`JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_JOB_LISTENERS`
--

LOCK TABLES `WFM_QRTZ_JOB_LISTENERS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_JOB_LISTENERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_QRTZ_JOB_LISTENERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_LOCKS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_LOCKS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_LOCKS` (
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_LOCKS`
--

LOCK TABLES `WFM_QRTZ_LOCKS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_LOCKS` DISABLE KEYS */;
INSERT INTO `WFM_QRTZ_LOCKS` (`LOCK_NAME`) VALUES ('CALENDAR_ACCESS'),('JOB_ACCESS'),('MISFIRE_ACCESS'),('STATE_ACCESS'),('TRIGGER_ACCESS');
/*!40000 ALTER TABLE `WFM_QRTZ_LOCKS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_PAUSED_TRIGGER_GRPS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_PAUSED_TRIGGER_GRPS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_PAUSED_TRIGGER_GRPS` (
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY  (`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_PAUSED_TRIGGER_GRPS`
--

LOCK TABLES `WFM_QRTZ_PAUSED_TRIGGER_GRPS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_PAUSED_TRIGGER_GRPS` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_QRTZ_PAUSED_TRIGGER_GRPS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_SCHEDULER_STATE`
--

DROP TABLE IF EXISTS `WFM_QRTZ_SCHEDULER_STATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_SCHEDULER_STATE` (
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY  (`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_SCHEDULER_STATE`
--

LOCK TABLES `WFM_QRTZ_SCHEDULER_STATE` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_SCHEDULER_STATE` DISABLE KEYS */;
INSERT INTO `WFM_QRTZ_SCHEDULER_STATE` (`INSTANCE_NAME`, `LAST_CHECKIN_TIME`, `CHECKIN_INTERVAL`) VALUES ('vmx14b.dev.dmz1369102772765',1369179324677,20000);
/*!40000 ALTER TABLE `WFM_QRTZ_SCHEDULER_STATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_SIMPLE_TRIGGERS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_SIMPLE_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_SIMPLE_TRIGGERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `TRIGGER_NAME` (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `WFM_QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `WFM_QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_SIMPLE_TRIGGERS`
--

LOCK TABLES `WFM_QRTZ_SIMPLE_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_SIMPLE_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_QRTZ_SIMPLE_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_TRIGGERS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_TRIGGERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `IS_VOLATILE` varchar(1) NOT NULL,
  `DESCRIPTION` varchar(250) default NULL,
  `NEXT_FIRE_TIME` bigint(13) default NULL,
  `PREV_FIRE_TIME` bigint(13) default NULL,
  `PRIORITY` int(11) default NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) default NULL,
  `CALENDAR_NAME` varchar(200) default NULL,
  `MISFIRE_INSTR` smallint(2) default NULL,
  `JOB_DATA` blob,
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `JOB_NAME` (`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `WFM_QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`JOB_NAME`, `JOB_GROUP`) REFERENCES `WFM_QRTZ_JOB_DETAILS` (`JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_TRIGGERS`
--

LOCK TABLES `WFM_QRTZ_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_TRIGGERS` DISABLE KEYS */;
INSERT INTO `WFM_QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`, `JOB_NAME`, `JOB_GROUP`, `IS_VOLATILE`, `DESCRIPTION`, `NEXT_FIRE_TIME`, `PREV_FIRE_TIME`, `PRIORITY`, `TRIGGER_STATE`, `TRIGGER_TYPE`, `START_TIME`, `END_TIME`, `CALENDAR_NAME`, `MISFIRE_INSTR`, `JOB_DATA`) VALUES ('workflowCleanupTrigger','DEFAULT','workflowCleanupJob','DEFAULT','0',NULL,1369180800000,-1,5,'WAITING','BLOB',1369102772000,0,NULL,0,'');
/*!40000 ALTER TABLE `WFM_QRTZ_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_QRTZ_TRIGGER_LISTENERS`
--

DROP TABLE IF EXISTS `WFM_QRTZ_TRIGGER_LISTENERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_QRTZ_TRIGGER_LISTENERS` (
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `TRIGGER_LISTENER` varchar(200) NOT NULL,
  PRIMARY KEY  (`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_LISTENER`),
  KEY `TRIGGER_NAME` (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `WFM_QRTZ_TRIGGER_LISTENERS_ibfk_1` FOREIGN KEY (`TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `WFM_QRTZ_TRIGGERS` (`TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_QRTZ_TRIGGER_LISTENERS`
--

LOCK TABLES `WFM_QRTZ_TRIGGER_LISTENERS` WRITE;
/*!40000 ALTER TABLE `WFM_QRTZ_TRIGGER_LISTENERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_QRTZ_TRIGGER_LISTENERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_TASK_DATA`
--

DROP TABLE IF EXISTS `WFM_TASK_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_TASK_DATA` (
  `id` bigint(20) NOT NULL auto_increment,
  `KEY` varchar(255) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1077 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_TASK_DATA`
--

LOCK TABLES `WFM_TASK_DATA` WRITE;
/*!40000 ALTER TABLE `WFM_TASK_DATA` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_TASK_DATA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_TASK_INSTANCE`
--

DROP TABLE IF EXISTS `WFM_TASK_INSTANCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_TASK_INSTANCE` (
  `id` bigint(20) NOT NULL auto_increment,
  `FINISHED_AT` datetime default NULL,
  `PROVIDER_ID` bigint(20) default NULL,
  `QUEUED_AT` datetime default NULL,
  `STARTED_AT` datetime default NULL,
  `TASK_STATUS` varchar(64) NOT NULL,
  `TASK_TYPE` varchar(64) NOT NULL,
  `OPTLOCK` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1547 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_TASK_INSTANCE`
--

LOCK TABLES `WFM_TASK_INSTANCE` WRITE;
/*!40000 ALTER TABLE `WFM_TASK_INSTANCE` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_TASK_INSTANCE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_TASK_INSTANCE_DATA`
--

DROP TABLE IF EXISTS `WFM_TASK_INSTANCE_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_TASK_INSTANCE_DATA` (
  `TASK_INSTANCE_ID` bigint(20) NOT NULL,
  `TASK_DATA_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`TASK_INSTANCE_ID`,`TASK_DATA_ID`),
  UNIQUE KEY `TASK_DATA_ID` (`TASK_DATA_ID`),
  KEY `FK_TASK_INSTANCE_DATA_02` (`TASK_DATA_ID`),
  KEY `FK_TASK_INSTANCE_DATA_01` (`TASK_INSTANCE_ID`),
  CONSTRAINT `FK_TASK_INSTANCE_DATA_01` FOREIGN KEY (`TASK_INSTANCE_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`),
  CONSTRAINT `FK_TASK_INSTANCE_DATA_02` FOREIGN KEY (`TASK_DATA_ID`) REFERENCES `WFM_TASK_DATA` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_TASK_INSTANCE_DATA`
--

LOCK TABLES `WFM_TASK_INSTANCE_DATA` WRITE;
/*!40000 ALTER TABLE `WFM_TASK_INSTANCE_DATA` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_TASK_INSTANCE_DATA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_TASK_INSTANCE_DEPENDENCY`
--

DROP TABLE IF EXISTS `WFM_TASK_INSTANCE_DEPENDENCY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_TASK_INSTANCE_DEPENDENCY` (
  `TASK_INSTANCE_ID` bigint(20) NOT NULL,
  `TASK_DEPENDS_ON_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`TASK_INSTANCE_ID`,`TASK_DEPENDS_ON_ID`),
  KEY `FK_TASK_INSTANCE_DEPENDENCY_01` (`TASK_DEPENDS_ON_ID`),
  KEY `FK_TASK_INSTANCE_DEPENDENCY_02` (`TASK_INSTANCE_ID`),
  CONSTRAINT `FK_TASK_INSTANCE_DEPENDENCY_01` FOREIGN KEY (`TASK_DEPENDS_ON_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`),
  CONSTRAINT `FK_TASK_INSTANCE_DEPENDENCY_02` FOREIGN KEY (`TASK_INSTANCE_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_TASK_INSTANCE_DEPENDENCY`
--

LOCK TABLES `WFM_TASK_INSTANCE_DEPENDENCY` WRITE;
/*!40000 ALTER TABLE `WFM_TASK_INSTANCE_DEPENDENCY` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_TASK_INSTANCE_DEPENDENCY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_WORKFLOW`
--

DROP TABLE IF EXISTS `WFM_WORKFLOW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_WORKFLOW` (
  `id` bigint(20) NOT NULL auto_increment,
  `DESCRIPTION` text,
  `NAME` varchar(255) NOT NULL,
  `WORKFLOW_TYPE` varchar(255) NOT NULL,
  `XML_TEMPLATE` text,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_WORKFLOW`
--

LOCK TABLES `WFM_WORKFLOW` WRITE;
/*!40000 ALTER TABLE `WFM_WORKFLOW` DISABLE KEYS */;
INSERT INTO `WFM_WORKFLOW` (`id`, `DESCRIPTION`, `NAME`, `WORKFLOW_TYPE`, `XML_TEMPLATE`) VALUES (1,'Default content ingestion workflow','DefaultIngest','IngestContent','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n  <tasklist>\n    <task>\n      <id>Finished</id>\n      <class>FinishedTask</class>\n      <dependencies>\n        <dependency>MakeAssetAvailable</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>MakeAssetAvailable</id>\n      <class>MakeAssetAvailableTask</class>\n      <dependencies>\n        <dependency>CreateSMILDocumentForAsset</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>CreateSMILDocumentForAsset</id>\n      <class>CreateSMILDocumentForAssetTask</class>\n      <dependencies>\n        <dependency>XCodeSetup</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>XCodeSetup</id>\n      <class>XCodeSetup</class>\n      <dependencies>\n        <dependency>XCodePreflight</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>XCodePreflight</id>\n      <class>XCodePreflight</class>\n      <dependencies>\n        <dependency>SelectAdvertisements</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>SelectAdvertisements</id>\n      <class>SelectAdvertisementsTask</class>\n      <dependencies/>\n      <taskdata/>\n    </task>\n  </tasklist>\n</workflowdescription>'),(2,'Default content transcoding workflow','DefaultTranscode','Transcode','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n  <tasklist>\n    <task>\n      <id>Finished</id>\n      <class>FinishedTask</class>\n      <dependencies>\n        <dependency>MakeAssetAvailable</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>MakeAssetAvailable</id>\n      <class>MakeAssetAvailableTask</class>\n      <dependencies>\n        <dependency>XCodeSetup</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>XCodeSetup</id>\n      <class>XCodeSetup</class>\n      <dependencies>\n        <dependency>GetAssetInfo</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>GetAssetInfo</id>\n      <class>GetAssetInfoTask</class>\n      <dependencies/>\n      <taskdata/>\n    </task>\n  </tasklist>\n</workflowdescription>'),(61,'Winmo transcode workflow','TranscodeWinMo','TranscodeWinMo','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n    <tasklist>\n        <task>\n            <id>MakeAssetAvailable</id>\n            <class>MakeAssetAvailableTask</class>\n            <dependencies>\n                <dependency>XCodeTask</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>GetAssetInfo</id>\n            <class>GetAssetInfoTask</class>\n            <dependencies/>\n            <taskdata/>\n        </task>\n        <task>\n            <id>Finished</id>\n            <class>FinishedTask</class>\n            <dependencies>\n                <dependency>MakeAssetAvailable</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>XCodeTask</id>\n            <class>XCodeTask</class>\n            <dependencies>\n                <dependency>GetAssetInfo</dependency>\n            </dependencies>\n            <taskdata>\n                <dataitem>\n                    <key>encodingName</key>\n                    <value>WinMo</value>\n                </dataitem>\n            </taskdata>\n        </task>\n    </tasklist>\n</workflowdescription>\n'),(81,'RTSP transcode workflow','TranscodeRTSP','TranscodeRTSP','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n    <tasklist>\n        <task>\n            <id>XCodeTask</id>\n            <class>XCodeTask</class>\n            <dependencies>\n                <dependency>GetAssetInfo</dependency>\n            </dependencies>\n            <taskdata>\n                <dataitem>\n                    <key>encodingName</key>\n                    <value>RTSP</value>\n                </dataitem>\n            </taskdata>\n        </task>\n        <task>\n            <id>MakeAssetAvailable</id>\n            <class>MakeAssetAvailableTask</class>\n            <dependencies>\n                <dependency>XCodeTask</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>Finished</id>\n            <class>FinishedTask</class>\n            <dependencies>\n                <dependency>MakeAssetAvailable</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>GetAssetInfo</id>\n            <class>GetAssetInfoTask</class>\n            <dependencies/>\n            <taskdata/>\n        </task>\n    </tasklist>\n</workflowdescription>\n'),(121,'Default audio-only content ingestion workflow','DefaultAudioIngest','IngestAudioContent','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n    <tasklist>\n        <task>\n            <id>XCodeAudioSetup</id>\n            <class>XCodeAudioSetup</class>\n            <dependencies/>\n            <taskdata/>\n        </task>\n        <task>\n            <id>MakeAssetAvailable</id>\n            <class>MakeAssetAvailableTask</class>\n            <dependencies>\n                <dependency>XCodeAudioSetup</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>Finished</id>\n            <class>FinishedTask</class>\n            <dependencies>\n                <dependency>MakeAssetAvailable</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n    </tasklist>\n</workflowdescription>\n'),(141,'Default SMIL document regeneration workflow','RegenerateProviderSMILDocuments','RegenerateProviderSMILDocuments','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n    <tasklist>\n        <task>\n            <id>Finished</id>\n            <class>FinishedTask</class>\n            <dependencies>\n                <dependency>RegenerateProviderSMILDocuments</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>RegenerateProviderSMILDocuments</id>\n            <class>RegenerateProviderSMILDocumentsTask</class>\n            <dependencies/>\n            <taskdata/>\n        </task>\n    </tasklist>\n</workflowdescription>\n'),(161,'Progressive Download transcode workflow','TranscodeProgressiveDownload','TranscodeProgressiveDownload','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n    <tasklist>\n        <task>\n            <id>Finished</id>\n            <class>FinishedTask</class>\n            <dependencies>\n                <dependency>MakeAssetAvailable</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>XCodeTask</id>\n            <class>XCodeTask</class>\n            <dependencies>\n                <dependency>GetAssetInfo</dependency>\n            </dependencies>\n            <taskdata>\n                <dataitem>\n                    <key>encodingName</key>\n                    <value>ProgressiveDownload</value>\n                </dataitem>\n            </taskdata>\n        </task>\n        <task>\n            <id>MakeAssetAvailable</id>\n            <class>MakeAssetAvailableTask</class>\n            <dependencies>\n                <dependency>XCodeTask</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>GetAssetInfo</id>\n            <class>GetAssetInfoTask</class>\n            <dependencies/>\n            <taskdata/>\n        </task>\n    </tasklist>\n</workflowdescription>\n'),(181,'Default ingestion workflow with ads','IngestWithAds','IngestContent','<?xml version=\"1.0\"\n  encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n  <tasklist>\n    <task>\n      <id>Finished</id>\n      <class>FinishedTask</class>\n      <dependencies>\n<dependency>MakeAssetAvailable</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>MakeAssetAvailable</id>\n      <class>MakeAssetAvailableTask</class>\n      <dependencies>\n<dependency>CreateSMILDocumentForAsset</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>CreateSMILDocumentForAsset</id>\n      <class>CreateSMILDocumentForAssetTask</class>\n      <dependencies>\n<dependency>XCodeSetup</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>XCodeSetup</id>\n      <class>XCodeSetup</class>\n      <dependencies>\n<dependency>XCodePreflight</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>XCodePreflight</id>\n      <class>XCodePreflight</class>\n      <dependencies>\n        <dependency>SelectAdvertisements</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>SelectAdvertisements</id>\n      <class>SelectAdvertisementsTask</class>\n      <dependencies/>\n      <taskdata/>\n    </task>\n  </tasklist>\n</workflowdescription>'),(182,'Default transcode workflow with ads','TranscodeWithAds','Transcode','<?xml version=\"1.0\"\n  encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n  <tasklist>\n    <task>\n      <id>Finished</id>\n      <class>FinishedTask</class>\n      <dependencies>\n<dependency>MakeAssetAvailable</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>MakeAssetAvailable</id>\n      <class>MakeAssetAvailableTask</class>\n      <dependencies>\n<dependency>CreateSMILDocumentForAsset</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>CreateSMILDocumentForAsset</id>\n      <class>CreateSMILDocumentForAssetTask</class>\n      <dependencies>\n<dependency>XCodeSetup</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>XCodeSetup</id>\n      <class>XCodeSetup</class>\n      <dependencies>\n<dependency>XCodePreflight</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>XCodePreflight</id>\n      <class>XCodePreflight</class>\n      <dependencies>\n        <dependency>SelectAdvertisements</dependency>\n      </dependencies>\n      <taskdata/>\n    </task>\n    <task>\n      <id>SelectAdvertisements</id>\n      <class>SelectAdvertisementsTask</class>\n      <dependencies/>\n      <taskdata/>\n    </task>\n  </tasklist>\n</workflowdescription>'),(201,'PCTV transcode workflow','TranscodePCTV','TranscodePCTV','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<workflowdescription>\n    <tasklist>\n        <task>\n            <id>Finished</id>\n            <class>FinishedTask</class>\n            <dependencies>\n                <dependency>MakeAssetAvailable</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n        <task>\n            <id>GetAssetInfo</id>\n            <class>GetAssetInfoTask</class>\n            <dependencies/>\n            <taskdata/>\n        </task>\n        <task>\n            <id>XCodeTask</id>\n            <class>XCodeTask</class>\n            <dependencies>\n                <dependency>GetAssetInfo</dependency>\n            </dependencies>\n            <taskdata>\n                <dataitem>\n                    <key>encodingName</key>\n                    <value>PCTV</value>\n                </dataitem>\n            </taskdata>\n        </task>\n        <task>\n            <id>MakeAssetAvailable</id>\n            <class>MakeAssetAvailableTask</class>\n            <dependencies>\n                <dependency>XCodeTask</dependency>\n            </dependencies>\n            <taskdata/>\n        </task>\n    </tasklist>\n</workflowdescription>\n');
/*!40000 ALTER TABLE `WFM_WORKFLOW` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_WORKFLOW_INSTANCE`
--

DROP TABLE IF EXISTS `WFM_WORKFLOW_INSTANCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_WORKFLOW_INSTANCE` (
  `id` bigint(20) NOT NULL auto_increment,
  `FINISHED_AT` datetime default NULL,
  `STARTED_AT` datetime default NULL,
  `OPTLOCK` int(11) default NULL,
  `WORKFLOW_STATUS` varchar(64) NOT NULL,
  `ASSET_CONTEXT_ID` bigint(20) NOT NULL,
  `PROVIDER_CONTEXT_ID` bigint(20) NOT NULL,
  `WORKFLOW_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_WORKFLOW_INSTANCE_03` (`PROVIDER_CONTEXT_ID`),
  KEY `FK_WORKFLOW_INSTANCE_02` (`ASSET_CONTEXT_ID`),
  KEY `FK_WORKFLOW_INSTANCE_01` (`WORKFLOW_ID`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_01` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `WFM_WORKFLOW` (`id`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_02` FOREIGN KEY (`ASSET_CONTEXT_ID`) REFERENCES `WFM_ASSET_CONTEXT` (`id`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_03` FOREIGN KEY (`PROVIDER_CONTEXT_ID`) REFERENCES `WFM_PROVIDER_CONTEXT` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_WORKFLOW_INSTANCE`
--

LOCK TABLES `WFM_WORKFLOW_INSTANCE` WRITE;
/*!40000 ALTER TABLE `WFM_WORKFLOW_INSTANCE` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_WORKFLOW_INSTANCE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WFM_WORKFLOW_INSTANCE_TASK`
--

DROP TABLE IF EXISTS `WFM_WORKFLOW_INSTANCE_TASK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WFM_WORKFLOW_INSTANCE_TASK` (
  `WORKFLOW_INSTANCE_ID` bigint(20) NOT NULL,
  `TASK_INSTANCE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`WORKFLOW_INSTANCE_ID`),
  KEY `FK_WORKFLOW_INSTANCE_TASK_02` (`WORKFLOW_INSTANCE_ID`),
  KEY `FK_WORKFLOW_INSTANCE_TASK_01` (`TASK_INSTANCE_ID`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_TASK_01` FOREIGN KEY (`TASK_INSTANCE_ID`) REFERENCES `WFM_WORKFLOW_INSTANCE` (`id`),
  CONSTRAINT `FK_WORKFLOW_INSTANCE_TASK_02` FOREIGN KEY (`WORKFLOW_INSTANCE_ID`) REFERENCES `WFM_TASK_INSTANCE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WFM_WORKFLOW_INSTANCE_TASK`
--

LOCK TABLES `WFM_WORKFLOW_INSTANCE_TASK` WRITE;
/*!40000 ALTER TABLE `WFM_WORKFLOW_INSTANCE_TASK` DISABLE KEYS */;
/*!40000 ALTER TABLE `WFM_WORKFLOW_INSTANCE_TASK` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-05-21 23:35:28
