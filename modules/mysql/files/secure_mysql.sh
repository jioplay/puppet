#!/bin/bash

[ "x${1}" == "x" ] && password="" || password="${1}"
cur_password=""

# set root password
mysql -uroot --password=${cur_password} -e "UPDATE mysql.user SET Password=PASSWORD(\"${password}\") WHERE User='root'"

# remove anonymous user
mysql -uroot --password=${cur_password} -e "DELETE FROM mysql.user WHERE User=''"

# remove remote root
mysql -uroot --password=${cur_password} -e "DELETE FROM mysql.user WHERE User='root' AND Host!='localhost'"

# remove test dbase and associated privileges
mysql -uroot --password=${cur_password} -e "DROP DATABASE test"
mysql -uroot --password=${cur_password} -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"

# reload privileges table
mysql -uroot --password=${cur_password} -e "FLUSH PRIVILEGES"
