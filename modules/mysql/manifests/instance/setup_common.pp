define mysql::instance::setup_common(
    $mysql_backup,
) {
    $mysql_app_directories = [
        '/var/app',
        '/var/app/mysql_tools',
        '/var/app/mysql_tools/scripts/logs',
        '/var/app/mysql_arch',
    ]
    file { $mysql_app_directories:
        owner => 'mysql',
        group => 'mysql',
        ensure => directory,
    }

    file { '/var/app/mysql_tools/scripts/':
        owner   => 'mysql',
        group   => 'mysql',
        mode    => '0755',
        recurse => true,
        source  => "puppet://$puppetserver/modules/mysql/instance/mysql_scripts/",
    }

    file { "/mysql_arch":
        ensure => link,
        target => "/var/app/mysql_arch",
    }

    cron { "mysql_logrotate":
        command => '/var/app/mysql_tools/scripts/log_rotate.sh 30 /mysql_arch /var/app/mysql_tools/scripts/logs/log_rotate_`date +\%d\%m\%y_\%H\%M`',
        user    => mysql,
        weekday => '*/3',
        hour    => '17',
        minute  => '00'
    }
    if ($mysql_backup){
        cron { "mysql_backup":
            command => '/var/app/mysql_tools/scripts/backup.sh 3 > /tmp/mysql_bkp.log 2>&1',
            user    => mysql,
            hour    => '03',
            minute  => '00'
        }
    }

}
