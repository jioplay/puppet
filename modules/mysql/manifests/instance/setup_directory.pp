define mysql::instance::setup_directory(
    $mysql_var_subdirectory = undef,
    $mysql_usr_subdirectory = undef,
    $mysql_bin_subdirectory = undef,
) {
    File {
        owner   => 'root',
        group   => 'root',
    }

    file { "/usr/sbin/${mysql_usr_subdirectory}/":
        recurse => true,
        recurselimit => 1,
        source => "file:///usr/sbin/",
    }

    file { "/usr/bin/${mysql_usr_subdirectory}/":
        ensure => directory,
    }
    ->
    file { "/usr/bin/${mysql_bin_subdirectory}/":
        # recurse => true,
        # recurselimit => 1,
        # source => "file:///usr/bin/mysql*",
        ensure => directory,
    }
    ->
    exec { 'copy_mysql_bin':
        path => "/bin",
        command => "cp /usr/bin/mysql* /usr/bin/${mysql_bin_subdirectory}/ || true",
        unless => "ls /usr/bin/${mysql_bin_subdirectory}/mysql* 2>/dev/null",
    }
    file { "/usr/share/${mysql_usr_subdirectory}/":
        recurse => true,
        source => "file:///usr/share/mysql/",
    }

    file { "/var/lib/${mysql_var_subdirectory}":
        owner => 'mysql',
        group => 'mysql',
        ensure => directory,
    }
}
