#This allows us to run any sql on any db after install i.e. update

define mysql::sql (
    $sqlfile ,
    $password     = $::mysql::params::mysql_password,
    $subscribe_to = undef,
    $logoutput    = false,
    $user         = "root",
    $db           = undef,
    $onlyif       = undef,
    $unless       = undef,
    $socket       = undef,
)
{
    $socket_arg = $socket ? {
      undef => "",
      default => "--socket=${socket}",
    }
    $cmd = "mysql ${socket_arg} -u${user} -p${password} ${db} < ${sqlfile}"
    if ($subscribe_to){
      exec { "${name}_${sqlfile}":
        path    => "/usr/bin:/usr/sbin:/bin",
        command => "$cmd",
        logoutput => $logoutput,
        refreshonly => true,
        onlyif => $onlyif,
        unless => $unless,
        timeout => 1800,
        subscribe => File[$subscribe_to],
        # require => Class['mysql::secure'],
        require => Class['mysql'],
      }
    }
    else{
      exec { "${name}_${sqlfile}":
        path    => "/usr/bin:/usr/sbin:/bin",
        command => "$cmd",
        logoutput => $logoutput,
        onlyif => $onlyif,
        unless => $unless,
        # require => Class['mysql::secure'],
        require => Class['mysql'],
      }
   }
}

