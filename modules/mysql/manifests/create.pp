define mysql::create ( $password , $logoutput, $db, $user="root", $socket=undef){

    $socket_arg = $socket ? {
        undef => "",
        default => "--socket=${socket}",
    }
    $cmd = "mysqladmin ${socket_arg} -u${user} -p${password} create ${db}"

    exec { "$name":
        path    => "/usr/bin:/usr/sbin:/bin",
        command => "$cmd",
        unless  => "mysql ${socket_arg} -u${user} -p${password} -e \"use ${db}\"",
        logoutput => $logoutput,
    }

}

