class mysql::config {

    if ! $mysql::use_default_config{
        if $mysql::replica_master_server_id {
          $template_file = "my.cnf.replication.erb"
        } else {
          $template_file = "my.cnf.default.erb"
        }
        $mysqld_options_mapping_hash = $mysql::mysqld_options_mapping_hash
        file { "/etc/my.cnf":
          ensure  => present,
          owner   => "root",
          group   => "root",
          mode    => "0644",
          content => template("mysql/$template_file"),
          require => Class["mysql::install"],
          notify  => Class["mysql::service"],
        }
    }
}
