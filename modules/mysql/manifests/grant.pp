# Creates a new user with access to a certain database, or gives permission
# to an existing user to access a certain database.
#
# == Parameters
#
# [*user*]
#   The name of the user to give access to (or the new user to be created)
#
# [*password*]
#   The password for the new user
#
# [*db*]
#   The name of the database to give access to
#
# [*host*]
#   The host from where the user is allowed to connect
#
# == Examples
#
# Invoke with the name and source for the new db, as in:
#   mysql::grant { "MyService_MyDb_MyUser": user => "MyUser", password => "MyPass, db => "MyDb" }
#
# == Authors
#
# CERN IT/GT/DMS <it_dep_gt_dms@cern.ch>
#
define mysql::grant(
  $user,
  $password,
  $db,
  $host="localhost",
  $mysql_password = $::mysql::params::mysql_password,
  $privileges = "all privileges",
  $socket = undef,
) {

  include mysql::params
  $socket_arg = $socket ? {
      undef => "",
      default => "--socket=${socket}",
  }
  exec { "mysql_user_grant_${user}_${db}_${host}_${socket}":
    path    => "/usr/bin:/usr/sbin:/bin",
    command => "mysql ${socket_arg} -uroot -p${mysql_password} -e \"grant ${privileges} on ${db}.* to '${user}'@'${host}' identified by '${password}'\"",
    #unless  => "mysql -u${user} -p${password} -D${db} -h${host}",
    unless  => "/usr/bin/test $(/bin/echo \"select * from db where Host =\\\"${host}\\\" and Db = \\\"${db}\\\" and User = \\\"${user}\\\"\" | /usr/bin/mysql ${socket_arg} -hlocalhost -uroot -p${mysql_password} -Dmysql | /bin/egrep -v ^Host | /usr/bin/wc -l) -eq 1",
    #require => Service['mysqld'],
    # require => Class['mysql::secure'],
    require => Class['mysql'],
  }
}

define mysql::grant_replica(
  $user,
  $password,
  $host,
  $mysql_password = $::mysql::params::mysql_password,

) {

  include mysql::params

  exec { "mysql_user_grant_replica_${user}_${host}":
    path    => "/usr/bin:/usr/sbin:/bin",
    command => "mysql -uroot -p${mysql_password} -e \"grant replication slave on *.* to '${user}'@'${host}' identified by '${password}'\"",
    unless  => "/usr/bin/test $(/bin/echo \"select * from user where Host =\\\"${host}\\\" and User = \\\"${user}\\\"\" | /usr/bin/mysql -hlocalhost -uroot -p${mysql_password} -Dmysql | /bin/egrep -v ^Host | /usr/bin/wc -l) -eq 1",
    require => Class['mysql::secure'],
  }
}
