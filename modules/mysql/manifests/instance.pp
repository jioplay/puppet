define mysql::instance(
    $default_instance = false,
    $mysql_identifier = false,
    $mysql_port = false,
    $mysql_binlog_format = false,
    $mysql_password = $mysql::params::mysql_password,
    $mysql_backup = false,
    $server_id = false
) {
    if (!$default_instance) and (!$mysql_identifier) {
        fail("mysql identifier must be provided for non default instances")
    }
    if (!$mysql_port) {
        fail("mysql port must be provided")
    }
    if (!$mysql_binlog_format) {
        fail("mysql binlog format must be provided")
    }
    if (!defined(Class['mysql'])) or (!$mysql::install_package_only) {
        fail("mysql class must be instantiated with install_package_only parameter set to true")
    }

    $mysql_var_subdirectory = $default_instance ? {
        true => 'mysql',
        false => "mysql_${mysql_identifier}",
    }
    $mysql_usr_subdirectory = $default_instance ? {
        true => '',
        false => "mysql_${mysql_identifier}",
    }
    $mysql_bin_subdirectory = $default_instance ? {
        true => '',
        false => "${mysql_usr_subdirectory}/bin",
    }

    # bin directory
    if(!$default_instance) {
        mysql::instance::setup_directory{ "mysql_${mysql_port}":
            mysql_var_subdirectory => $mysql_var_subdirectory,
            mysql_usr_subdirectory => $mysql_usr_subdirectory,
            mysql_bin_subdirectory => $mysql_bin_subdirectory,
            before => File["/usr/bin/${mysql_bin_subdirectory}/defaults_file"],
        }
    }

    if (!defined(Mysql::Instance::Setup_common['mysql_common_setup'])) {
        mysql::instance::setup_common{ 'mysql_common_setup':
            mysql_backup => $mysql_backup,
        }
    }

    # log file
    $mysql_log_subdirectory = $default_instance ? {
        true => 'mysql/mobitv',
        false => "mysql/${mysql_identifier}",
    }

    $instance_log_directory = "/var/log/${mysql_log_subdirectory}"
    $instance_log = [
        "${instance_log_directory}/mysqld.log",
        "${instance_log_directory}/error.log",
        "${instance_log_directory}/general.log",
        "${instance_log_directory}/slow.log",
    ]
    file { $instance_log_directory:
        owner => 'mysql',
        group => 'mysql',
        ensure => directory,
    }
    ->
    file { $instance_log:
        owner => 'mysql',
        group => 'mysql',
        ensure => present,
        before => Service["mysqld_${mysql_port}"],
    }

    # remove default my.cnf
    if (!defined(File['/etc/my.cnf'])) {
        file { '/etc/my.cnf':
            ensure => 'absent',
            before => Service["mysqld_${mysql_port}"],
        }
    }

    # default file
    file { "/usr/bin/${mysql_bin_subdirectory}/defaults_file":
        owner   => 'root',
        group   => 'root',
        content => template('mysql/instance/defaults_file.erb'),
        notify  => Service["mysqld_${mysql_port}"],
    }

    # my.cnf file
    $my_cnf = "/var/lib/${mysql_var_subdirectory}/my.cnf"
    concat { $my_cnf:
        owner => 'mysql',
        group => 'mysql',
        mode  => '0644',
        notify  => Service["mysqld_${mysql_port}"],
    }
    if (!defined(Class['concat::setup'])) {
        class { 'concat::setup': }
    }
    concat::fragment{ "my_cnf_header_${mysql_port}":
        target  => $my_cnf,
        content => template('mysql/instance/my.cnf.erb'),
        order   => '01',
    }
    concat::fragment{ "my_cnf_tail_${mysql_port}":
        target  => $my_cnf,
        content => template('mysql/instance/defaults_file.erb'),
        order   => '02',
    }

    file { "/etc/init.d/mysqld_${mysql_port}":
        owner => 'root',
        group => 'root',
        mode  => '0755',
        content => template('mysql/instance/mysqld.erb'),
        notify  => Service["mysqld_${mysql_port}"],
    }

    service { "mysqld_${mysql_port}":
        enable          => true,
        ensure          => running,
        hasstatus       => true,
        hasrestart      => true,
        require         => [Concat[$my_cnf]],
    }

    if (!defined(File['/usr/local/sbin/secure_mysql_socket.sh'])) {
        file { "/usr/local/sbin/secure_mysql_socket.sh":
            ensure  => present,
            owner   => "root",
            group   => "root",
            mode    => "0700",
            source  => "puppet://$puppetserver/modules/mysql/secure_mysql_socket.sh",
            require => Service["mysqld_${mysql_port}"],
        }
    }

    exec { "mysql_secure_${mysql_port}":
        unless  => "/usr/bin/mysqladmin --socket=/var/lib/${mysql_var_subdirectory}/mysql.sock -uroot -p${mysql_password} status",
        command => "/usr/local/sbin/secure_mysql_socket.sh ${mysql_password} '/var/lib/${mysql_var_subdirectory}/mysql.sock'",
        require => [File["/usr/local/sbin/secure_mysql_socket.sh"],
                    Service["mysqld_${mysql_port}"]],
    }

    mysql::grant { "icinga_user_all_${mysql_port}":
        user        => "${::mysql::icinga_mysql_user}",
        password    => "${::mysql::icinga_mysql_password}",
        socket      => "/var/lib/${mysql_var_subdirectory}/mysql.sock",
        db          => "*",
        host        => "%",
        privileges  => "SELECT, PROCESS, SUPER, REPLICATION CLIENT",
        require => [Exec["mysql_secure_${mysql_port}"],
                    Service["mysqld_${mysql_port}"]],
    }
    if $mysql_backup {
        mysql::grant { "back_user_${mysql_port}":
            user        => "${::mysql::backup_mysql_user}",
            password    => "${::mysql::backup_mysql_password}",
            socket      => "/var/lib/${mysql_var_subdirectory}/mysql.sock",
            db          => "*",
            host        => "localhost",
            privileges  => "SHOW DATABASES, SELECT, LOCK TABLES, RELOAD",
            require => [Exec["mysql_secure_${mysql_port}"],
                        Service["mysqld_${mysql_port}"]],
        }
    }

    if $::mysql::icinga {
        if ! defined("icinga::register") {
          fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
        }

        if ! $::mysql::icinga_instance {
          fail("Must provide icinga_instance parameter to mysql module when icinga = true")
        }

        @@nagios_service { "check_mysql_master_${fqdn}_${mysql_port}":
          host_name             => $::fqdn,
          check_command         => "check_mysql_master!${::mysql::icinga_mysql_user}!${::mysql::icinga_mysql_password}!${mysql_port}",
          service_description => "check_mysql_master_${mysql_port}",
          notes => "monitor master connectivity",
          normal_check_interval => "15", # mildly aggressive
          use                   => "generic-service",
          tag                   => "icinga_instance:${::mysql::icinga_instance}",
          target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
          notify                => Exec["icinga_reload"],
          require               => Class["icinga::register"],
        }

        @@nagios_service { "check_mysql_slave_${fqdn}_${mysql_port}":
          host_name             => $::fqdn,
          check_command         => "check_mysql_slave!${::mysql::icinga_mysql_user}!${::mysql::icinga_mysql_password}!${mysql_port}",
          service_description => "check_mysql_slave_${mysql_port}",
          notes => "monitor slave syncronization",
          normal_check_interval => "15", # mildly aggressive
          use                   => "generic-service",
          tag                   => "icinga_instance:${::mysql::icinga_instance}",
          target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
          notify                => Exec["icinga_reload"],
          require               => Class["icinga::register"],
        }
    }

}
