define mysql::create_version($version) {

  file { ["/var/mysql_data/$version",]:
          ensure  => directory,
          owner   => mysql,
          group   => mysql,
          mode    => "0755",
        }

   file { ["/var/log/mysql/$version"]:
          ensure  => directory,
          owner   => mysql,
          group   => mysql,
          mode    => "0755",
         }

    file { "/var/app/install_mysql/$version":
          ensure  => directory,
          owner   => mysql,
          group   => mysql,
          mode    => "0755",
          }

    file { ["/var/app/mysql-$version"]:
          ensure  => directory,
          owner   => root,
          group   => root,
          mode    => "0755",
         }
    }
