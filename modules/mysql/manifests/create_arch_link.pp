define mysql::create_arch_link ($arch_link)
{
  file { "${arch_link}":
           ensure => link,
           target => "/var/opt/mysql_arch",
        }
 }
