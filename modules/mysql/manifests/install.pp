class mysql::install {
    package { "mysql":
        ensure => $mysql::mysql_package_ensure,
    }
    package { "mysql-server":
        ensure => $mysql::mysql_server_package_ensure,
    }
}
