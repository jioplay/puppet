define mysql::install_mysql($mytab_config,$tarfile)
{
   realize(Accounts::Group["mysql"])
   realize(Accounts::User["mysql"])
   $mytab_value = $mytab_config
   $items = split($mytab_value,':')
   $component = $items[0]
   $VERSION = $items[1]
   $item2 = split($VERSION,'_')
   $halfname = "mysql_${item2[0]}_${item2[1]}_${component}"
   $alia ="${component}_${item2[0]}_${item2[1]}"
   $port = $items[2]
   $MYSQL_HOME = "/var/app/mysql-$VERSION/$component"
   $MYSQL_DATA = "/var/mysql_data/$VERSION/$component"

  if (!defined(Mysql::Create_Files["create_file"])) {
     mysql::create_files{"create_file":
     }
    }
  if (!defined(Mysql::Create_Version["create_${VERSION}"])) {
     mysql::create_version{"create_${VERSION}":
         version   => $VERSION,
      }
    }
     mysql::create_instance{"create_${VERSION}_$component":
       version   => $VERSION,
       instance  => $component,
       tarfile   => $tarfile,
       initname  => $halfname,
       alia      => $alia,
       port      => $port,
     }


   if (!defined(Mysql::Create_Cron["create_cron"])) {
      mysql::create_cron{"create_cron":}
      }
}
