class mysql::params {

    # For replication
    $replica_master_server_id           = "${::replica_master_server_id}"
    $replica_db                         = "${::replica_db}"
    $replica_auto_increment_increment   = "${::replica_auto_increment_increment}"
    $replica_auto_increment_offset      = "${::replica_auto_increment_offset}"
    $replica_master_host                = "${::replica_auto_master_host}"
    $replica_master_user                = "${::replica_auto_master_user}"
    $replica_master_passwd              = "${::replica_auto_master_passwd}"
    $lower_case_table_names             = false
    $use_default_config                 = false
    $server_id                          = false
    $mysql_password                     = "TE1WeuHiflzmLMxi"
    $mysql_package_ensure               = present
    $mysql_server_package_ensure        = present
    $mysqld_options_mapping_hash        = {}
    $mytab_config                       = false
    ##icinga.pp
    $icinga = true
    $icinga_instance = "icinga.qa.smf1.mobitv"
    $icinga_mysql_user = "icinga"
    $icinga_mysql_password = "zQwacy8WoRfPE5lG3gD"
    ##icinga.pp

    $install_package_only               = false
    $backup_mysql_user                  = "backup"
    $backup_mysql_password              = "unbreaKab1e"
}

