class mysql (
    $replica_master_server_id           = hiera('mysql::replica_master_server_id',$mysql::params::replica_master_server_id),
    $replica_db                         = hiera('mysql::replica_db',$mysql::params::replica_db),
    $replica_auto_increment_increment   = hiera('mysql::replica_auto_increment_increment',$mysql::params::replica_auto_increment_increment),
    $replica_auto_increment_offset      = hiera('mysql::replica_auto_increment_offset',$mysql::params::replica_auto_increment_offset),
    $replica_master_host                = hiera('mysql::replica_master_host',$mysql::params::replica_master_host),
    $replica_master_user                = hiera('mysql::replica_master_user',$mysql::params::replica_master_user),
    $replica_master_passwd              = hiera('mysql::replica_master_passwd',$mysql::params::replica_master_passwd),
    $mysql_password                     = hiera('mysql::mysql_password',$mysql::params::mysql_password),
    $lower_case_table_names             = hiera('mysql::lower_case_table_names',$mysql::params::lower_case_table_names),
    $use_default_config                 = hiera('mysql::use_default_config',$mysql::params::use_default_config),
    $mysql_package_ensure               = hiera('mysql::mysql_package_ensure',$mysql::params::mysql_package_ensure),
    $mysql_server_package_ensure        = hiera('mysql::mysql_server_package_ensure',$mysql::params::mysql_server_package_ensure),
    $mysqld_options_mapping_hash        = hiera('mysql::mysqld_options_mapping_hash',$mysql::params::mysqld_options_mapping_hash),
    ##icinga.pp
    $icinga = hiera('mysql::icinga',$mysql::params::icinga),
    $icinga_instance = hiera('icinga_instance',$mysql::params::icinga_instance),
    $icinga_mysql_user = hiera('mysql::icinga_mysql_user',$mysql::params::icinga_mysql_user),
    $icinga_mysql_password = hiera('mysql::icinga_mysql_password',$mysql::params::icinga_mysql_password),
    ##icinga.pp

    $server_id                          = hiera('mysql::server_id',$mysql::params::server_id),
    $install_package_only               = hiera('mysql::install_package_only',$mysql::params::install_package_only),
    $backup_mysql_user                  = hiera('mysql::backup_mysql_user',$mysql::params::backup_mysql_user),
    $backup_mysql_password              = hiera('mysql::backup_mysql_password',$mysql::params::backup_mysql_password),
)   inherits mysql::params {

    if ($install_package_only){
        anchor{ "mysql::begin": } ->
        class { "mysql::install": }  ->
        anchor{ "mysql::end": }
    }else{
        anchor{ "mysql::begin": } ->
        class { "mysql::install": }  ->
        class { "mysql::config": }   ->
        class { "mysql::secure": }   ->
        class { "mysql::icinga": } ->
        anchor{ "mysql::end": }
        class {"mysql::service": } ->
        class { "mysql::icinga_grant": }

    }

      # include module name in motd
    os::motd::register { "mysql": }
}

