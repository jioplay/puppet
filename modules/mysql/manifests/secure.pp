# Perform hardening equivalent to mysql_secure_installation command.
class mysql::secure {
    file { "/usr/local/sbin/secure_mysql.sh":
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => "0700",
        source  => "puppet://$puppetserver/modules/mysql/secure_mysql.sh",
        require => Class["mysql::service"],
    }

    exec { "mysql_secure":
        unless  => "/usr/bin/mysqladmin -uroot -p${mysql::mysql_password} status",
        command => "/usr/local/sbin/secure_mysql.sh ${mysql::mysql_password}",
        require => [File["/usr/local/sbin/secure_mysql.sh"],
                    Service["mysqld"]],
    }
}
