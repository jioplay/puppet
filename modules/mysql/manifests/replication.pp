class mysql::replication(
  $mysql_user = 'mysql_replicator',
) {
     realize(Accounts::User[$mysql_user])
     realize(Accounts::Group[$mysql_user])
     file { "/home/${mysql_user}/.ssh/id_rsa":
        owner   => $mysql_user,
        group   => $mysql_user,
        mode   => '0400',
        ensure  => present,
        source  => "puppet:///modules/mysql/mysql_replication/id_rsa",
        require => [Accounts::User[$mysql_user],Accounts::Group[$mysql_user]]
     }

     file { "/home/${mysql_user}/.ssh/id_rsa.pub":
        owner   => $mysql_user,
        group   => $mysql_user,
        mode   => '0400',
        ensure  => present,
        source  => "puppet:///modules/${module_name}/mysql_replication/id_rsa.pub",
        require => [Accounts::User[$mysql_user],Accounts::Group[$mysql_user]]
     }

     file { "/home/${mysql_user}/.ssh/config":
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        ensure => present,
        source => "puppet:///modules/${module_name}/mysql_replication/ssh_config",
        require  => [Accounts::User[$mysql_user],Accounts::Group[$mysql_user]]
     }

    file{"/home/${mysql_user}/mysql_replication_scripts":
      owner   => $mysql_user,
      group   => $mysql_user,
      ensure  => directory,
      source  => "puppet:///modules/${module_name}/mysql_replication_scripts",
      recurse => true,
    }

   exec { "stop_service":
      path    => ["/bin","/usr/bin","/sbin"],
      command => "/etc/init.d/mysqld stop",
      onlyif  => "bash -c '/etc/init.d/mysqld status | grep running'",
   }

   exec { "chkconfig_off_mysqld":
      path    => ["/bin","/usr/bin","/sbin"],
      command => "/sbin/chkconfig mysqld off",
      onlyif  => "bash -c '/sbin/chkconfig --list mysqld | grep on'",
   }

}
