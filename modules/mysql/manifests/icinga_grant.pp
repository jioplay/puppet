class mysql::icinga_grant {
  if $::mysql::icinga {
    mysql::grant { "icinga_user_all":
      user        => "${::mysql::icinga_mysql_user}",
      password    => "${::mysql::icinga_mysql_password}",
      db          => "*",
      host        => "%",
      privileges  => "SELECT, PROCESS",
    }
  }
}
