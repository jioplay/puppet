# Defines a new MySQL database on the local server.
#
# == Parameters
#
# [*name*]
#   The name of the new database
#
# [*source*]
#   The location of the file containing the SQL source to create the db entities
#
# == Examples
#
# Invoke with the name and source for the new db, as in:
#   mysql::db { "NewDbName": source => "/path/to/the/source.sql" }
#
# == Authors
#
# CERN IT/GT/DMS <it_dep_gt_dms@cern.ch>
#
define mysql::db(
    $source = undef,
    $mysql_password = $::mysql::params::mysql_password,
    $logoutput = false,
    $socket = undef,
) {

    if $source {
        mysql::sql { "sql-${name}" :
            password => $mysql_password,
            sqlfile => $source,
            logoutput => $logoutput,
            socket => $socket,
            require => Class['mysql::secure'],
        }
    } else {
        mysql::create { "create-${name}":
            password => $mysql_password,
            db => $name,
            logoutput => $logoutput,
            socket => $socket,
            require => Class['mysql::secure'],
        }
    }

  #need create and import
  #refactor the import to its own define.

}
