class mysql::service {
    service { "mysqld":
        enable          => true,
        ensure          => running,
        hasstatus       => true,
        hasrestart      => true,
        require         => Class["mysql::config"],
        #subscribe       => Class["mysql::config"],
        notify          => Class["mysql::secure"],
    }
}
