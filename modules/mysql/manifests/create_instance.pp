define mysql::create_instance($version,$instance,$tarfile,$initname,$alia,$port)
{
      file { ["/var/log/mysql/$version/$instance","/var/mysql_data/$version/$instance","/var/app/install_mysql/$version/$instance"]:
            ensure  => directory,
            owner   => mysql,
            group   => mysql,
            mode    => "0755",
            }

      file { ["/var/log/mysql/$version/$instance/error.log","/var/log/mysql/$version/$instance/general.log","/var/log/mysql/$version/$instance/slow.log"]:
           replace => "no",
           ensure  => "present",
           content => "From Puppet\n",
           owner   => "mysql",
           group   => "mysql",
           mode    => "0755",
            }
        ->
       file { "/var/app/install_mysql/$version/$instance/$tarfile.tar.gz":
            ensure  => present,
            mode    => "0755",
            source  => "puppet:///modules/mysql/mysql-general/$tarfile.tar.gz",
            }
        ->
        exec { "install_mysql_$version_$instance":
                 cwd     => "/var/app/install_mysql/$version/$instance",
                 path    => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
                 command => "tar -xzvf /var/app/install_mysql/$version/$instance/$tarfile.tar.gz ",
                 unless  => "test -d /var/app/install_mysql/$version/$instance/$tarfile/bin",
              }
        ->
        file { "/var/app/mysql-$version/$instance":
               ensure  => link,
               target  => "/var/app/install_mysql/$version/$instance/$tarfile",
               require => File["/var/app/install_mysql/$version/$instance"],
             }
        ->
        file { "/var/app/mysql-$version/$instance/scripts/gen_mycnf.sh":
                  ensure  => file,
                  owner   => root,
                  group   => root,
                  mode    => "0755",
                  source  => "puppet:///modules/mysql/mysql-general/scripts/gen_mycnf.sh",
              }
         ->
         file { "/var/app/mysql-$version/$instance/scripts/generic_mycnf":
              ensure  => file,
              owner   => root,
              group   => root,
              mode    => "0755",
              source  => "puppet:///modules/mysql/mysql-general/scripts/generic_mycnf",
              }
         ->
         file { "/var/app/mysql-$version/$instance/scripts/secure_install.sh":
                  ensure  => file,
                  owner   => root,
                  group   => root,
                  mode    => "0755",
                  source  => "puppet:///modules/mysql/mysql-general/scripts/secure_install.sh",
               }
         ->
         exec { "test_$version_$instance":
                  cwd      => "/root",
                  provider => 'shell',
                  path     => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
                  command  => "export DB=${alia};export MYSQL_HOME=/var/app/mysql-${version}/${instance};export MYSQL_DATA=/var/mysql_data/${version}/${instance};export LD_RUN_PATH=/var/app/mysql-${version}/${instance}/lib;export MYSQL_TCP_PORT=$port;export MYSQL_LOG=/var/log/mysql/$version/$instance;",
               }
         ->
         exec { "gen_mycnf_$version_$instance":
                  cwd      => "/var/app/mysql-$version/$instance/scripts",
                  provider => 'shell',
                  path     => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
                  command  => "export DB=${alia};export MYSQL_HOME=/var/app/mysql-${version}/${instance};export MYSQL_DATA=/var/mysql_data/${version}/${instance};export LD_RUN_PATH=/var/app/mysql-${version}/${instance}/lib;export MYSQL_TCP_PORT=$port;export MYSQL_LOG=/var/log/mysql/$version/$instance;./gen_mycnf.sh",
                  unless   => "test -f /var/app/mysql-$version/$instance/my.cnf"
              }
         ->
         exec { "remove /etc/my.cnf_$version_$instance":
                  cwd      => "/root",
                  provider => 'shell',
                  path     => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
                  command  => "rm /etc/my.cnf",
                  unless   => "test ! -f /etc/my.cnf",
                }
          ->
          exec { "mysql_install_db_$version_$instance":
                  cwd      => "/root",
                  provider => 'shell',
                  path     => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
                  command  => "export DB=${alia};export MYSQL_HOME=/var/app/mysql-${version}/${instance};export MYSQL_DATA=/var/mysql_data/${version}/${instance};export LD_RUN_PATH=/var/app/mysql-${version}/${instance}/lib;export MYSQL_TCP_PORT=${port};export MYSQL_LOG=/var/log/mysql/${version}/${instance};/var/app/mysql-${version}/${instance}/scripts/mysql_install_db --basedir=/var/app/mysql-$version/$instance --datadir=/var/mysql_data/$version/$instance --user=mysql",
                   unless => "test -s /var/mysql_data/$version/$instance/ibdata1"
               }
           ->
           exec { "prepare /etc/init.d/mysql_init_$version_$instance":
                  cwd      => "/root",
                  provider => 'shell',
                  path     => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
                  command  => "source /root/.bash_profile_mysql  && export DB=${alia} && cp /var/app/mysql-$version/$instance/support-files/mysql.server /etc/init.d/${initname} && sed -i \'s|^basedir=|basedir=/var/app/mysql-$version/$instance|g\' /etc/init.d/${initname} && sed -i \'s|^datadir=|datadir=/var/mysql_data/$version/$instance|g\' /etc/init.d/${initname} && sed -i \'s|^pid_file=|pid_file=/var/mysql_data/$version/$instance/mysqld.pid|g\' /etc/init.d/${initname} && sed -i \'s|^mysqld_pid_file_path=|mysqld_pid_file_path=/var/mysql_data/$version/$instance/mysqld.pid|g\' /etc/init.d/${initname}",
                  unless  => "test -f /etc/init.d/${initname}",
                }
            ->
            exec { "start_mysql_$version_$instance":
                  cwd      => "/root",
                  provider => 'shell',
                  path     => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
                  command  => "export DB=${alia};export MYSQL_HOME=/var/app/mysql-${version}/${instance};export MYSQL_DATA=/var/mysql_data/${version}/${instance};export LD_RUN_PATH=/var/app/mysql-${version}/${instance}/lib;export MYSQL_TCP_PORT=${port};export MYSQL_LOG=/var/log/mysql/${version}/${instance};/etc/init.d/${initname} start",
                  unless  => "test -S $/var/mysql_data/$version/$instance/mysql.sock"
               }
             ->
             exec { "secure install_$version_$instance":
                  cwd  => "/root",
                  provider => 'shell',
                  path     => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
                  command  => "source /root/.bash_profile_mysql;export DB=${alia};export MYSQL_HOME=/var/app/mysql-${version}/${instance};export MYSQL_DATA=/var/mysql_data/${version}/${instance};export LD_RUN_PATH=/var/app/mysql-${version}/${instance}/lib;export MYSQL_TCP_PORT=${port};export MYSQL_LOG=/var/log/mysql/${version}/${instance};/var/app/mysql-${version}/${instance}/scripts/secure_install.sh /root/.mypwd",
             }

}
