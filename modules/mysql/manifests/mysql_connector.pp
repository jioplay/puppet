define mysql::mysql_connector (
    $package_ensure = undef,
) {
    if (!$package_ensure) { fail("mysql connector ver is needed") }

    $connector_path = "/usr/share/java/"
    $connector_name = "mysql-connector-java"
    $connector_jar = "${connector_name}-${mysql_connector_java_version}.jar"
    $connector_link = "${connector_name}.jar"

    File {
        owner   => "root",
        group   => "root",
        mode    => "0644",
    }
    file { "${connector_path}":
        ensure => directory,
        mode   => "755",
    }
    ->
    file { "${connector_path}${connector_jar}":
        ensure => present,
        source => "puppet:///modules/mysql/connector/${connector_jar}"
    }
    ->
    file { "${connector_path}${connector_link}":
        ensure => link,
        target => "${connector_path}${connector_jar}",
    }
}