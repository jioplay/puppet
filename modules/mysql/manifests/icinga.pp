class mysql::icinga {

  if $::mysql::icinga {
    if ! defined("icinga::register") {
      fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    if ! $::mysql::icinga_instance {
      fail("Must provide icinga_instance parameter to mysql module when icinga = true")
    }


    @@nagios_service { "check_mysql_${fqdn}":
      host_name             => $::fqdn,
      check_command         => "check_mysql!${::mysql::icinga_mysql_user}!${::mysql::icinga_mysql_password}",
      service_description => "check_mysql",
      notes => "monitor INFORMATION_SCHEMA table statistics",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "icinga_instance:${::mysql::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
      notify                => Exec["icinga_reload"],
      require               => Class["icinga::register"],
    }

  }
}
