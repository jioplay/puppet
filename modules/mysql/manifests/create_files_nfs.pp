define mysql::create_files_nfs ()
 {

    file { ["/root/.mytab","/etc/mytab"]:
       ensure  => file,
       mode    => "0644",
       owner   => root,
       group   => root,
       content => template("mysql/mytab_multi.erb"),
    }

    file { "/root/.bash_profile_mysql":
       ensure  => file,
       mode    => "0755",
       owner   => root,
       group   => root,
       source  => "/var/Jukebox/mysql/install/bash_profile_mysql",
     }

     file { "/home/mysql/.bash_profile":
       ensure  => file,
       mode    => "0644",
       owner   => mysql,
       group   => mysql,
       source  => "/var/Jukebox/mysql/install/bash_profile_mysql",
     }

     file { ["/root/.mypwd","/home/mysql/.mypwd"]:
       ensure  => file,
       mode    => "0644",
       owner   => root,
       group   => root,
       source  => "/var/Jukebox/mysql/install/mypwd",
     }

      file { ["/var/app"]:
        ensure  => directory,
        group   => root,
        owner   => root,
        mode    => "0755",
      }

      file { ["/var/app/install_mysql"]:
        ensure  => directory,
        owner   => root,
        group   => root,
        mode    => "0755",
        require => File["/var/app"]
      }

      file { ["/var/app/mysql_tools","/var/opt/mysql_arch","/var/opt/mysql","/var/mysql_data","/var/log/mysql",]:
         ensure  => directory,
         owner   => mysql,
         group   => mysql,
         mode    => "0755",
         require => File["/var/app"],
      }
     ->
      file { ["/var/app/mysql_tools/logs"]:
         ensure  => directory,
         owner   => mysql,
         group   => mysql,
         mode    => "0644",
      }
     ->
       file { "/var/app/mysql_tools/scripts":
         ensure  => directory,
         recurse => true,
         owner   => mysql,
         group   => mysql,
         mode    => "0755",
         source  => "/var/Jukebox/mysql/install/scripts",
       }
      ->
      file { "/var/app/mysql_tools/tools":
         ensure  => directory,
         recurse => true,
         owner   => mysql,
         group   => mysql,
         mode    => "0755",
         source  => "/var/Jukebox/mysql/install/tools",
         require => File["/var/app/mysql_tools"]
      }
      ->
      file { "/var/opt/mysql/stage":
         ensure  => directory,
         recurse => true,
         owner   => mysql,
         group   => mysql,
         mode    => "0755",
         source  => "/var/Jukebox/mysql/install/stage",
      }
      ->
      exec { "Add additional rpms":
          cwd      => "/var/opt/mysql/stage/tools",
          provider => 'shell',
          path     => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
          command  => 'yum -y install perl-DBD-MySQL.x86_64 perl-TermReadKey.x86_64 && source /root/.bash_profile_mysql && cp $MYSQL_HOME/lib/libmysqlclient.so.18 /usr/lib64/',
          unless   => "rpm -qa | grep perl-DBD",
      }
      ->
      exec { "Do innotop-1.9.1 make":
          cwd     => "/var/app/mysql_tools/tools",
          provider=> 'shell',
          path    => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
          command => 'tar -zxvf /var/opt/mysql/stage/tools/innotop-1.9.1.tar.gz && chown mysql:mysql -R innotop-1.9.1 && cd innotop-1.9.1/ && perl Makefile.PL && make install',
          unless  => "test -d /var/app/mysql_tools/tools/innotop-1.9.1",
      }
      ->
      exec { "Do mytop-1.6 make":
          cwd     => "/var/app/mysql_tools/tools",
          provider=> 'shell',
          path    => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin',
          command => "tar -zxvf /var/opt/mysql/stage/tools/mysqlreport-3.5.tgz && tar -zxvf /var/opt/mysql/stage/tools/mytop-1.6.tar.gz && chown -R mysql:mysql mytop-1.6 && chown -R mysql:mysql mysqlreport-3.5 && cd mytop-1.6 / && perl Makefile.PL && make install ",
          unless  => "test -d /var/app/mysql_tools/tools/mytop-1.6",
      }

 }
