define mysql::create_cron ()
{
   cron { "mysql_logrotate":
           command => '/var/app/mysql_tools/scripts/log_rotate.sh 30 /mysql_arch /var/app/mysql_tools/scripts/logs/log_rotate_`date +\%d\%m\%y_\%H\%M`',
           user    => mysql,
           weekday => '*/3',
           hour    => '23',
           minute  => '00'
         }

    cron { "mysql_backup":
            command => '/var/app/mysql_tools/scripts/backup.sh 3 > /tmp/mysql_bkp.log 2>&1',
            user    => mysql,
            hour    => '03',
            minute  => '00'
          }
 }
