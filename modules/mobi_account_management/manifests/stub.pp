# == Class: mobi_account_management::stub
#
#  installs platform account management stub
#
# === Parameters:
#
#   $stub_version::
#
# === Requires:
#
#     tomcat8080
#
# === Sample Usage
#
#  class { "mobi_account_management::stub" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_account_management::stub (
$stub_version = $mobi_account_management::stub::params::stub_version,
) inherits mobi_account_management::stub::params {
    include mobi_account_management::stub::install
    anchor { "mobi_account_management::stub::begin":} -> Class["mobi_account_management::stub::install"]
    Class["mobi_account_management::stub::install"] -> anchor { "mobi_account_management::stub::end": }
    os::motd::register { $name : }
}

