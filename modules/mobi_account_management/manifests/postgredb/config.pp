class mobi_account_management::postgredb::config
{

    $user = $mobi_account_management::postgredb::user
    $pswd = $mobi_account_management::postgredb::pswd
    $data_folder = $mobi_account_management::postgredb::data_folder

    file { "/var/mobi_account_management/postgredb/acctmgmt_schema.sql":
        ensure => file,
        content => template("mobi_account_management/postgredb/acctmgmt_schema.sql.erb"),
        require => Class["mobi_account_management::postgredb::install"],
    }
}
