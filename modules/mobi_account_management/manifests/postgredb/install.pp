class mobi_account_management::postgredb::install {

    exec { "mkdir_mobi_account_management":
      path => ['/bin','/usr/bin'],
      command => "mkdir -p /var/mobi_account_management",
      unless => "test -d /var/mobi_account_management"
    }

    exec { "mkdir_acctmgmt_data":
      path => ['/bin','/usr/bin'],
      command => "mkdir -p $data_folder",
      unless => "test -d $data_folder"
    }

    exec { "mkdir_acctmgmt_data_tblspc":
      path => ['/bin','/usr/bin'],
      command => "mkdir -p $data_folder/pg_tblspc/acctmgmt_data",
      unless => "test -d $data_folder/pg_tblspc/acctmgmt_data"
    }

    exec { "mkdir_acctmgmt_index_tblspc":
      path => ['/bin','/usr/bin'],
      command => "mkdir -p $data_folder/pg_tblspc/acctmgmt_index",
      unless => "test -d $data_folder/pg_tblspc/acctmgmt_index"
    }

    file { "/var/mobi_account_management/postgredb":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_account_management/postgredb",
        require => Exec['mkdir_mobi_account_management'],
    }

}
