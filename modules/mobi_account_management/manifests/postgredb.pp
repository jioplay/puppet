# == Class: mobi_account_management::postgredb
#
#  installs platform account management database
#
# === Parameters:
#
#   $database_root_password::
#   $acctmgmt_username::
#   $acctmgmt_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_account_management::postgredb" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_account_management::postgredb (
    $user = $mobi_account_management::postgredb::params::user,
    $pswd = $mobi_account_management::postgredb::params::pswd,
    $data_folder = $mobi_account_management::postgredb::params::data_folder,
) inherits mobi_account_management::postgredb::params {
    include mobi_account_management::postgredb::install, mobi_account_management::postgredb::config
    anchor { "mobi_account_management::postgredb::begin":} -> Class["mobi_account_management::postgredb::install"]
    Class["mobi_account_management::postgredb::config"] -> anchor { "mobi_account_management::postgredb::end": }
    os::motd::register { $name : }
}

