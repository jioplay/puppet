class mobi_account_management::icinga {

  if $::mobi_account_management::icinga {

    if ! $::mobi_account_management::icinga_instance {
      fail("Must provide icinga_instance parameter to mobi_account_management module when icinga = true")
    }

    @@nagios_service { "check_http_${fqdn}":
      host_name             => "$::fqdn",
      check_command         => "check_http! ${::mobi_account_management::icinga_cmd_args}",
      service_description   => "check_http",
      normal_check_interval => "15", # mildly aggressive
      use                   => "generic-service",
      tag                   => "$::mobi_account_management::icinga_instance",
    }

  }
}

