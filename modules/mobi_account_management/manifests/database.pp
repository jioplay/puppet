# == Class: mobi_account_management::database
#
#  installs platform account management database
#
# === Parameters:
#
#   $database_root_password::
#   $acctmgmt_username::
#   $acctmgmt_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_account_management::database" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_account_management::database (
    $database_root_password = $mobi_account_management::database::params::database_root_password,
    $acctmgmt_username = $mobi_account_management::database::params::acctmgmt_username,
    $acctmgmt_password = $mobi_account_management::database::params::acctmgmt_password,
)
  inherits mobi_account_management::database::params {
    include mobi_account_management::database::install
    anchor { "mobi_account_management::database::begin":} -> Class["mobi_account_management::database::install"]
    Class["mobi_account_management::database::install"] -> anchor { "mobi_account_management::database::end": }
    os::motd::register { $name : }
}
