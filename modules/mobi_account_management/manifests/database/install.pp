class mobi_account_management::database::install {

    file { "/var/mobi_account_management":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi_account_management/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_account_management/database",
        require => File["/var/mobi_account_management"],
    }

    file { "/var/mobi_account_management/database/install.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_account_management/database/install.sh.erb'),
        require => File["/var/mobi_account_management"],
    }

    file { "/var/mobi_account_management/database/uninstall.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_account_management/database/uninstall.sh.erb'),
        require => File["/var/mobi_account_management"],
    }

    file { "/var/mobi_account_management/database/con_am_create_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_account_management/database/con_am_create_databases.sql.erb'),
        require => File["/var/mobi_account_management"],
    }

    file { "/var/mobi_account_management/database/con_am_delete_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_account_management/database/con_am_delete_databases.sql.erb'),
        require => File["/var/mobi_account_management"],
    }

    #test to see if we have been run
    exec { "exec /var/mobi_account_management/database/install.sh":
        command => "/bin/sh /var/mobi_account_management/database/install.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi_account_management/database/installation.done",
        logoutput => "true",
        require => [File["/var/mobi_account_management/database/install.sh"] , File["/var/mobi_account_management/database/con_am_create_databases.sql"],],
    }

    file { "/var/mobi_account_management/database/install_seed.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_account_management/database/install_seed.sh.erb'),
        require => File["/var/mobi_account_management"],
    }

    #test to see if we have been run
    exec { "exec /var/mobi_account_management/database/install_seed.sh":
        command => "/bin/sh /var/mobi_account_management/database/install_seed.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi_account_management/database/installation_seed.done",
        logoutput => "true",
        require => [File["/var/mobi_account_management/database/install_seed.sh"], Exec["exec /var/mobi_account_management/database/install.sh"],],
    }

    file { "/var/mobi_account_management/database/install_alter_member_add_salt.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_account_management/database/install_alter_member_add_salt.sh.erb'),
        require => File["/var/mobi_account_management"],
    }

    #test to see if we have been run
    exec { "exec /var/mobi_account_management/database/install_alter_member_add_salt.sh":
        command => "/bin/sh /var/mobi_account_management/database/install_alter_member_add_salt.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi_account_management/database/installation_alter_member_add_salt.done",
        logoutput => "true",
        require => [File["/var/mobi_account_management/database/install_alter_member_add_salt.sh"], Exec["exec /var/mobi_account_management/database/install.sh"],],
    }

    file { "/var/mobi_account_management/database/install_index.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_account_management/database/install_index.sh.erb'),
        require => File["/var/mobi_account_management"],
    }

    #test to see if we have been run
    exec { "exec /var/mobi_account_management/database/install_index.sh":
        command => "/bin/sh /var/mobi_account_management/database/install_index.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi_account_management/database/installation_index.done",
        logoutput => "true",
        require => [File["/var/mobi_account_management/database/install_index.sh"], Exec["exec /var/mobi_account_management/database/install.sh"],],
    }
}