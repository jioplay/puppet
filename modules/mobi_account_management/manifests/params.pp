class mobi_account_management::params {
    ###icinga.pp
    $icinga = false
    $icinga_instance = undef
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-account-management/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $account_management_ver = "5.3.0-229906"
    $auth_identity_url = undef
    $auth_rights_url = undef
    $auth_session_url = undef
    $auth_enable_session_mgr = undef
    $auth_type = undef
    $account_adapter_url = undef
    $memcached_servers = undef
    $memcached_health_enable = undef
    $account_host = undef
    $account_port = undef
    $db_url = undef
    $db_user = undef
    $db_passwd = undef
    $db_driver = undef
    $db_validation_query = undef
    $hbm_dialect = undef
    $hbm_show_sql = undef
    $hbm_format_sql = undef
    $hbm2ddl_auto = undef
    $hbm_generate_ddl = undef
    $device_blacklist = undef
    $support_shared_profile = true
    $shared_profile_deletable = false
    $support_partner_profile = false
    $shared_profile_can_purchase = false
    $switch_profile_auth_type_default = "PIN"
    $purchase_auth_type_default = "PIN"
    $max_users = 3
    $max_devices = 5
    $partner_device_policy = false
    $partner_device_register = false
    $auto_device_register = false
    $adp_carrier = "technicolor"
    $adp_product = "mgo"
    $adp_version = "5.0"
    $pin_failed_attempts = 10
    $lock_timeout_mins = 30
    $email_validation_regex = undef
}
