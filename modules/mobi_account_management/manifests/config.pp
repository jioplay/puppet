class mobi_account_management::config
{

    $db_url = $mobi_account_management::db_url
    $db_user = $mobi_account_management::db_user
    $db_passwd = $mobi_account_management::db_passwd
    $db_driver = $mobi_account_management::db_driver
    $device_blacklist = $mobi_account_management::device_blacklist
    $auth_identity_url = $mobi_account_management::auth_identity_url
    $auth_rights_url = $mobi_account_management::auth_rights_url
    $auth_session_url = $mobi_account_management::auth_session_url
    $auth_enable_session_mgr = $mobi_account_management::auth_enable_session_mgr
    $auth_type = $mobi_account_management::auth_type
    $account_adapter_url = $mobi_account_management::account_adapter_url
    $memcached_servers = $mobi_account_management::memcached_servers
    $memcached_health_enable = $mobi_account_management::memcached_health_enable
    $account_host = $mobi_account_management::account_host
    $account_port = $mobi_account_management::account_port
    $hbm_dialect = $mobi_account_management::hbm_dialect
    $hbm_show_sql = $mobi_account_management::hbm_show_sql
    $hbm_format_sql = $mobi_account_management::hbm_format_sql
    $hbm2ddl_auto = $mobi_account_management::hbm2ddl_auto
    $hbm_generate_ddl = $mobi_account_management::hbm_generate_ddl
    $support_shared_profile = $mobi_account_management::support_shared_profile
    $shared_profile_deletable = $mobi_account_management::shared_profile_deletable
    $support_partner_profile = $mobi_account_management::support_partner_profile
    $shared_profile_can_purchase = $mobi_account_management::shared_profile_can_purchase
    $max_users = $mobi_account_management::max_users
    $max_devices = $mobi_account_management::max_devices
    $partner_device_policy = $mobi_account_management::partner_device_policy
    $partner_device_register = $mobi_account_management::partner_device_register
    $auto_device_register = $mobi_account_management::auto_device_register
    $adp_carrier = $mobi_account_management::adp_carrier
    $adp_product = $mobi_account_management::adp_product
    $adp_version = $mobi_account_management::adp_version
    $db_validation_query = $mobi_account_management::db_validation_query
    $pin_failed_attempts = $mobi_account_management::pin_failed_attempts
    $lock_timeout_mins = $mobi_account_management::lock_timeout_mins
    $email_validation_regex = $mobi_account_management::email_validation_regex

    file { "/opt/mobi-account-management/config/mobi-account-management.properties":
        ensure => file,
        content => template("mobi_account_management/mobi-account-management.properties.erb"),
        notify => Class["tomcat::service"],
        require => Class["mobi_account_management::install"],
    }

    file { "/opt/mobi-account-management/webapp/WEB-INF/classes/accountManagementConfig.xml":
        ensure => file,
        content => template("mobi_account_management/accountManagementConfig.xml.erb"),
        notify => Class["tomcat::service"],
        require => Class["mobi_account_management::install"],
    }

    file { "/opt/mobi-account-management/webapp/WEB-INF/classes/partnerOverrides.xml":
        ensure => file,
        content => template("mobi_account_management/partnerOverrides.xml.erb"),
        notify => Class["tomcat::service"],
        require => Class["mobi_account_management::install"],
    }
}
