class mobi_account_management::stub::install {

    package { "mobi-account-management-partner-adapter-stub-${mobi_account_management::stub::stub_version}" :
        ensure => present,
        notify => Class["tomcat::service"],
    }

}
