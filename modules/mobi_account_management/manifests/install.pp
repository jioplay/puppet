class mobi_account_management::install {

    package { "mobi-account-management-${mobi_account_management::account_management_ver}" :
        ensure => present,
        notify => Class["tomcat::service"],
    }

}
