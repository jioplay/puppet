# == Class: mobi_account_management
#
#  installs athmg component
#
# === Parameters:
#
#    $account_management_ver::
#    $db_host::
#    $db_port::
#    $db_user::
#    $db_passwd::
#    $db_sid::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "mobi_account_management" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_account_management (
    ###icinga.pp
    $icinga = $mobi_account_management::params::icinga,
    $icinga_instance = $mobi_account_management::params::icinga_instance,
    $icinga_cmd_args = $mobi_account_management::params::icinga_cmd_args,
    ###end icinga.pp
    $account_management_ver = $mobi_account_management::params::account_management_ver ,
    $auth_identity_url = $mobi_account_management::params::auth_identity_url,
    $auth_rights_url = $mobi_account_management::params::auth_rights_url,
    $auth_session_url = $mobi_account_management::params::auth_session_url,
    $auth_enable_session_mgr = $mobi_account_management::params::auth_enable_session_mgr,
    $auth_type = $mobi_account_management::params::auth_type,
    $account_adapter_url = $mobi_account_management::params::account_adapter_url,
    $memcached_servers = $mobi_account_management::params::memcached_servers,
    $db_url = $mobi_account_management::params::db_url ,
    $db_user = $mobi_account_management::params::db_user,
    $db_passwd = $mobi_account_management::params::db_passwd,
    $db_driver = $mobi_account_management::params::db_driver,
    $db_validation_query = $mobi_account_management::params::db_validation_query,
    $account_host = $mobi_account_management::params::account_host,
    $account_port = $mobi_account_management::params::account_port,
    $hbm_dialect = $mobi_account_management::params::hbm_dialect,
    $hbm_show_sql = $mobi_account_management::params::hbm_show_sql,
    $hbm_format_sql = $mobi_account_management::params::hbm_format_sql,
    $hbm2ddl_auto = $mobi_account_management::params::hbm2ddl_auto,
    $hbm_generate_ddl = $mobi_account_management::params::hbm_generate_ddl,
    $memcached_health_enable = $mobi_account_management::params::memcached_health_enable,
    $device_blacklist = $mobi_account_management::params::device_blacklist,
    $support_shared_profile = $mobi_account_management::params::support_shared_profile,
    $shared_profile_deletable = $mobi_account_management::params::shared_profile_deletable,
    $support_partner_profile = $mobi_account_management::params::support_partner_profile,
    $shared_profile_can_purchase = $mobi_account_management::params::shared_profile_can_purchase,
    $max_users = $mobi_account_management::params::max_users,
    $max_devices = $mobi_account_management::params::max_devices,
    $partner_device_policy = $mobi_account_management::params::partner_device_policy,
    $partner_device_register = $mobi_account_management::params::partner_device_register,
    $auto_device_register = $mobi_account_management::params::auto_device_register,
    $adp_carrier = $mobi_account_management::params::adp_carrier,
    $adp_product = $mobi_account_management::params::adp_product,
    $adp_version = $mobi_account_management::params::adp_version,
    $pin_failed_attempts = $mobi_account_management::params::pin_failed_attempts,
    $lock_timeout_mins = $mobi_account_management::params::lock_timeout_mins,
    $email_validation_regex = $mobi_account_management::params::email_validation_regex,
)
inherits mobi_account_management::params {
    include mobi_account_management::install, mobi_account_management::config
    anchor { "mobi_account_management::begin": } -> Class["mobi_account_management::install"]
    Class["mobi_account_management::config"] -> anchor { "mobi_account_management::end" :}
    os::motd::register { $name : }
}
