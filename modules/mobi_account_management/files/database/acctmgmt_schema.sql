DROP DATABASE `ACCTMGMT`;
CREATE DATABASE IF NOT EXISTS `ACCTMGMT`;

USE `ACCTMGMT`;
 --
 -- Table structure for table `ACCOUNT`
 --

 CREATE TABLE `ACCTMGMT`.`ACCOUNT` (
   `ACCOUNT_ID` bigint(20) NOT NULL auto_increment,
   `CREATED_TIME` datetime default NULL,
   `LAST_UPDATED_TIME` datetime default NULL,
    `ACCOUNT_GUID` varchar(255) default NULL,
   `ACCOUNT_STATUS` varchar(255) default NULL,
   `CARRIER` varchar(255) default NULL,
   `CREATED_BY` varchar(255) default NULL,
   `HAS_UV_ACCESS` bit(1) default NULL,
   `LAST_UPDATED_BY` varchar(255) default NULL,
   PRIMARY KEY `pk_account` (`ACCOUNT_ID`),
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
  
 --
 -- Table structure for table `ACCOUNT_BILLING`
 --

 CREATE TABLE `ACCTMGMT`.`ACCOUNT_BILLING` (
   `ACCOUNT_BILLING_ID` bigint(20) NOT NULL auto_increment,
   `CREATED_TIME` datetime default NULL,
   `LAST_UPDATED_TIME` datetime default NULL,
   `BILLING_STATUS` varchar(255) default NULL,
   `VENDOR` varchar(255) default NULL,
   `VUID` varchar(255) default NULL,
   `ACCOUNT_ID` bigint(20) default NULL,
   PRIMARY KEY `pk_account_billing` (`ACCOUNT_BILLING_ID`),
   UNIQUE KEY `uk_account_billing_vuve` (`VUID`, `VENDOR`),
   FOREIGN KEY `fk_account_billing_aid` (`ACCOUNT_ID`) REFERENCES `ACCOUNT`(`ACCOUNT_ID`),
   KEY `idx_account_billing_aid` (`ACCOUNT_ID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
 
 --
 -- Table structure for table `DEVICE`
 --

 CREATE TABLE `ACCTMGMT`.`DEVICE` (
   `DEVICE_ID` bigint(20) NOT NULL auto_increment,
   `CREATED_TIME` datetime default NULL,
   `LAST_UPDATED_TIME` datetime default NULL,
   `CREATED_BY` varchar(255) default NULL,
   `DATE_REGISTERED` datetime default NULL,
   `DEVICE_NAME` varchar(255) default NULL,
   `DEVICE_STATUS` varchar(255) default NULL,
   `DEVICE_TYPE` varchar(255) default NULL,
   `LAST_UPDATED_BY` varchar(255) default NULL,
   `NATIVE_DEVICE_ID` varchar(255) default NULL,
   `PKI_HASH` varchar(255) default NULL,
   `ACCOUNT_ID` bigint(20) default NULL,
   PRIMARY KEY `pk_device` (`DEVICE_ID`),
   UNIQUE KEY `uk_device_pkih` (`PKI_HASH`),
   FOREIGN KEY `fk_device_aid` (`ACCOUNT_ID`) REFERENCES `ACCOUNT`(`ACCOUNT_ID`),
   KEY `idx_device_aid` (`ACCOUNT_ID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
 
 --
 -- Table structure for table `DEVICE_HISTORY`
 --

 CREATE TABLE `ACCTMGMT`.`DEVICE_HISTORY` (
   `DEVICE_HISTORY_ID` bigint(20) NOT NULL auto_increment,
   `CREATED_TIME` datetime default NULL,
   `LAST_UPDATED_TIME` datetime default NULL,
   `ACCOUNT_ID` bigint(20) default NULL,
   `CREATED_BY` varchar(255) default NULL,
   `DATE_DEREGISTERED` datetime default NULL,
   `DATE_REGISTERED` datetime default NULL,
   `DEVICE_NAME` varchar(255) default NULL,
   `DEVICE_TYPE` varchar(255) default NULL,
   `LAST_UPDATED_BY` varchar(255) default NULL,
   `NATIVE_DEVICE_ID` varchar(255) default NULL,
   `PKI_HASH` varchar(255) default NULL,
   PRIMARY KEY `pk_device_history` (`DEVICE_HISTORY_ID`),
   FOREIGN KEY `fk_device_history_aid` (`ACCOUNT_ID`) REFERENCES `ACCOUNT`(`ACCOUNT_ID`),
   KEY `idx_device_history_aid` (`ACCOUNT_ID`),
   KEY `idx_device_history_ndid` (`NATIVE_DEVICE_ID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
 
 --
 -- Table structure for table `MEMBER`
 --

 CREATE TABLE `ACCTMGMT`.`MEMBER` (
   `MEMBER_ID` bigint(20) NOT NULL auto_increment,
   `CREATED_TIME` datetime default NULL,
   `LAST_UPDATED_TIME` datetime default NULL,
   `ACCOUNT_PIN` varchar(255) default NULL,
   `ADDRESS_CITY` varchar(255) default NULL,
   `ADDRESS_COUNTRY` varchar(255) default NULL,
   `ADDRESS_POSTAL_CODE` varchar(255) default NULL,
   `ADDRESS_State` varchar(255) default NULL,
   `ADDRESS_STREET` varchar(255) default NULL,
   `ADDRESS_STREET2` varchar(255) default NULL,
   `ALTERNATE_EMAIL_ADDRESS` varchar(255) default NULL,
   `AVATAR_URL` varchar(255) default NULL,
   `CAN_PURCHASE` varchar(255) default NULL,
   `CARRIER` varchar(255) default NULL,
   `CREATED_BY` varchar(255) default NULL,
   `DELETABLE` varchar(255) default NULL,
   `EMAIL` varchar(255) default NULL,
   `EPG_HEADEND` varchar(255) default NULL,
   `EULA_ACCEPTED` bit(1) default NULL,
   `EULA_VERSION` varchar(255) default NULL,
   `FAILED_LOGIN_ATTEMPTS` bigint(20) default NULL,
   `FIRST_NAME` varchar(255) default NULL,
   `INCLUDE_IN_SHARED_MODE` varchar(255) default NULL,
   `LAST_NAME` varchar(255) default NULL,
   `LAST_UPDATED_BY` varchar(255) default NULL,
   `LOGIN_LOCK_EXPIRATION` datetime default NULL,
   `MEMBER_GUID` varchar(255) default NULL,
   `MEMBER_STATUS` varchar(255) default NULL,
   `PARENTAL_CONTROL_MOVIE` varchar(255) default NULL,
   `PARENTAL_CONTROL_TV` varchar(255) default NULL,
   `PARTNER_MEMBER_ID` varchar(255) default NULL,
   `PASSWORD` varchar(255) default NULL,
   `PHONE` varchar(255) default NULL,
   `PRODUCT` varchar(255) default NULL,
   `PURCHASE_AUTH_TYPE` varchar(255) default NULL,
   `ROLE` varchar(255) default NULL,
   `STATUS_UPDATE_DATE` datetime default NULL,
   `SWITCH_PROFILE_AUTH_TYPE` varchar(255) default NULL,
   `PARTNER_USER_NAME` varchar(255) default NULL,
   `USER_NICK_NAME` varchar(255) default NULL,
   `ACCOUNT_ID` bigint(20) default NULL,
   `SALT` VARCHAR(255) default NULL,
   PRIMARY KEY `pk_member` (`MEMBER_ID`),
   FOREIGN KEY `fk_member_aid` (`ACCOUNT_ID`) REFERENCES `ACCOUNT`(`ACCOUNT_ID`),
   KEY `idx_member_aid` (`ACCOUNT_ID`),
   KEY `idx_member_addr` (`ALTERNATE_EMAIL_ADDRESS`),
   KEY `idx_member_email` (`EMAIL`)  
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

 --
 -- Table structure for table `ROLE`
 --

 CREATE TABLE `ACCTMGMT`.`ROLE` (
   `ROLE_ID` bigint(20) NOT NULL auto_increment,
   `CARRIER` varchar(255) default NULL,
   `CREATED_BY` varchar(255) default NULL,
   `CREATED_TIME` datetime default NULL,
   `DISPLAY_NAME` varchar(255) default NULL,
   `LAST_UPDATED_BY` varchar(255) default NULL,
   `LAST_UPDATED_TIME` datetime default NULL,
   `ROLE_NAME` varchar(255) default NULL,
   PRIMARY KEY `pk_role` (`ROLE_ID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
 
 --
 -- Table structure for table `MEMBER_ROLE`
 --

 CREATE TABLE `ACCTMGMT`.`MEMBER_ROLE` (
   `MEMBER_ROLE_ID` bigint(20) NOT NULL auto_increment,
   `CREATED_BY` varchar(255) default NULL,
   `CREATED_TIME` datetime default NULL,
   `IS_ADMIN` bit(1) default NULL,
   `LAST_UPDATED_BY` varchar(255) default NULL,
   `LAST_UPDATED_TIME` datetime default NULL,
   `ROLE_ID` bigint(20) NOT NULL,
   `MEMBER_ID` bigint(20) NOT NULL,
   PRIMARY KEY `pk_member_role` (`MEMBER_ROLE_ID`),
   FOREIGN KEY `fk_member_role_mid` (`MEMBER_ID`) REFERENCES `MEMBER`(`MEMBER_ID`),
   FOREIGN KEY `fk_member_role_rid` (`ROLE_ID`) REFERENCES `ROLE`(`ROLE_ID`),
   UNIQUE KEY `uk_member_role` (`MEMBER_ID`,`ROLE_ID`),
   KEY `idx_member_role_mid` (`MEMBER_ID`),
   KEY `idx_member_role_rid` (`ROLE_ID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


