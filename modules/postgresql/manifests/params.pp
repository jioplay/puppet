# Class: postgresql::params
#
#   The postgresql configuration settings.
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class postgresql::params {
    $user                         = 'postgres'
    $group                        = 'postgres'
    $ip_mask_deny_postgres_user   = '0.0.0.0/0'
    $ip_mask_allow_all_users      = '127.0.0.1/32'
    $checkpoint_segments          = 32
    $checkpoint_timeout           = "20min"
    $effective_cache_size         = undef
    $fsync                        = "off"
    $listen_addresses             = 'localhost'
    $maintenance_work_mem         = "512MB"
    $max_connections              = 100
    $max_files_per_process        = 100
    $shared_buffers               = undef
    $temp_buffers                 = "32MB"
    $wal_buffers                  = "8MB"
    $wal_sync_method              = "fdatasync"
    $work_mem                     = "128MB"

    # postgresql logs, remove older than N days
    $compress_older_than          = "2"
    $remove_older_than            = "10"

    # TODO: figure out a way to make this not platform-specific
    $manage_redhat_firewall       = false
    $icinga = true
    $icinga_instance = "icinga"
    $benchmark_tools = false
    $benchmark_package_ensure = latest
    # This is a bit hacky, but if the puppet nodes don't have pluginsync enabled,
    # they will fail with a not-so-helpful error message.  Here we are explicitly
    # verifying that the custom fact exists (which implies that pluginsync is
    # enabled and succeeded).  If not, we fail with a hint that tells the user
    # that pluginsync might not be enabled.  Ideally this would be handled directly
    # in puppet.
    if ($::postgres_default_version == undef) {
        fail "No value for postgres_default_version facter fact; it's possible that you don't have pluginsync enabled."
    }

    case $::operatingsystem {
        default: {
            $service_provider = undef
        }
    }

    case $::osfamily {
        'RedHat': {
            $service_name             = 'postgresql'
            $client_package_name      = 'postgresql'
            $server_package_name      = 'postgresql-server'
            $benchmark_package_name   = 'postgresql91-contrib'
            $needs_initdb             = true
            $encoding                 = 'UTF8'
            $createdb_path            = '/usr/bin/createdb'
            $psql_path                = '/usr/bin/psql'
            $firewall_supported       = false
            $persist_firewall_command = '/sbin/iptables-save > /etc/sysconfig/iptables'
        } default: {
            fail("Unsupported osfamily: ${::osfamily} operatingsystem: ${::operatingsystem}, module ${module_name} currently only supports osfamily RedHat and Debian")
        }
    }

}
