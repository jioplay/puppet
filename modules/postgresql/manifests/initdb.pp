# puppet-postgresql
# For all details and documentation:
# http://github.com/inkling/puppet-postgresql
#
# Copyright 2012- Inkling Systems, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class postgresql::initdb(
    $encoding = $postgresql::params::encoding,
    $options='',
    $package_name = $postgresql::params::initdb_user,
) inherits postgresql::params {


    # figure out the version here
    $package_vmaj=inline_template("<% a=/postgresql(9[0-9])-server.*/.match(\"$package_name\") -%><% if a -%><%= a[1].split(\"\")[0] -%><% end -%>")
    $package_vmin=inline_template("<% a=/postgresql(9[0-9])-server.*/.match(\"$package_name\") -%><% if a -%><%= a[1].split(\"\")[1] -%><% end -%>")

    if ( $::osfamily == "RedHat" ) {
        if ( $package_vmaj == "9" ) {
            $initdb_path          = "/etc/init.d/postgresql-$package_vmaj.$package_vmin initdb"
            $initdb_user          = "root"
        } else {
            $initdb_path          = '/usr/bin/initdb'
            $initdb_user          = "postgres"
        }
        $datadir              = "/var/lib/pgsql/$package_vmaj.$package_vmin/data"
    } else {
        fail("Only RedHat supported")
    }

    exec {"${initdb_path} --encoding '$encoding' --pgdata '$datadir'":
        creates => "${datadir}/PG_VERSION",
        user    => "$initdb_user",
        before => Class["postgresql::config"],
    }
}
