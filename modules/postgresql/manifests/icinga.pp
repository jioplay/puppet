class postgresql::icinga {

    if ! defined("icinga::register") {
        fail("icinga set to true in $name but icinga::register not declared (base class manage_icinga).")
    }

    @@nagios_service { "check_proc_postgres_${::fqdn}":
        host_name             => $::fqdn,
        check_command         => "check_proc_postgres!",
        service_description   => "check_proc_postgres",
        normal_check_interval => "20", # less aggressive
        use                   => "generic-service",
        tag                   => "icinga_instance:${::postgesql::server::icinga_instance}",
      target                => "/etc/icinga/conf.d/${::fqdn}.cfg",
            require               => Class["icinga::register"],
    }
}
