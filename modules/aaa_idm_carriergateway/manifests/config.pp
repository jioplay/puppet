class aaa_idm_carriergateway::config {

    $access_token_lifetime = $aaa_idm_carriergateway::access_token_lifetime
    $refresh_token_lifetime = $aaa_idm_carriergateway::refresh_token_lifetime

    file { "/opt/mobi-aaa/config/carrier-gateway-identity-manager-oauth2/application-config-override.properties":
    ensure => file,
    content => template("aaa_idm_carriergateway/application-config-override.properties.erb"),
    notify => Class["tomcat::service"],
    require => Class["aaa_idm_carriergateway::install"],
    }
}
