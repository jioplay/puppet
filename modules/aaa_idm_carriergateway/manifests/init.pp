# == Class: aaa_idm_carriergateway
#
#  installs idm adapter ng component
#
# === Parameters:
#
#    $idm_ver::5.2.0
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "aaa_idm_carriergateway" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class aaa_idm_carriergateway (
    $idm_ver = $aaa_idm_carriergateway::params::idm_ver,
    $access_token_lifetime = $aaa_idm_carriergateway::params::access_token_lifetime,
    $refresh_token_lifetime = $aaa_idm_carriergateway::params::refresh_token_lifetime,
)
inherits aaa_idm_carriergateway::params {
    include aaa_idm_carriergateway::install, aaa_idm_carriergateway::config
    anchor { "aaa_idm_carriergateway::begin": } -> Class["aaa_idm_carriergateway::install"]
    Class["aaa_idm_carriergateway::config"] -> anchor { "aaa_idm_carriergateway::end" :}
    os::motd::register { $name : }
}
