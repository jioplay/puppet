class aaa_idm_carriergateway::install {
    package { "mobi-aaa-carrier-gateway-identity-manager-oauth2-${aaa_idm_carriergateway::idm_ver}" :
        ensure => present,
  notify => Class["tomcat::service"]
    }
}
