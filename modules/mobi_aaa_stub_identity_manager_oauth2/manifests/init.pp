# == class: mobi_aaa_stub_identity_manager_oauth2
#
#  install identity adapter (STS)
#
# === Parameters:
#    $identity_ver::
#
# === Requires:
#
#    java
#    tomcat
#
# === Sample Usage
#
#
#
#
# Remember: No empty lines between comments and class definition
#
class mobi_aaa_stub_identity_manager_oauth2(
    $identity_ver = $mobi_aaa_stub_identity_manager_oauth2::params::identity_ver,
    $access_token_lifetime_secs = $mobi_aaa_stub_identity_manager_oauth2::params::access_token_lifetime_secs,
    $refresh_token_lifetime_secs = $mobi_aaa_stub_identity_manager_oauth2::params::refresh_token_lifetime_secs,
    $anonymous_scope = $mobi_aaa_stub_identity_manager_oauth2::params::anonymous_scope,
    $service_asset = $mobi_aaa_stub_identity_manager_oauth2::params::service_asset,
)
inherits mobi_aaa_stub_identity_manager_oauth2::params {
    include mobi_aaa_stub_identity_manager_oauth2::install, mobi_aaa_stub_identity_manager_oauth2::config
    anchor {"mobi_aaa_stub_identity_manager_oauth2::begin": } -> Class["mobi_aaa_stub_identity_manager_oauth2::install"]
    Class["mobi_aaa_stub_identity_manager_oauth2::config"] -> anchor { "mobi_aaa_stub_identity_manager_oauth2::end" :}
    os::motd::register { $name : }
}
