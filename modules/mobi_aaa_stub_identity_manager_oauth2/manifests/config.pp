class mobi_aaa_stub_identity_manager_oauth2::config{
    $access_token_lifetime_secs = $mobi_aaa_stub_identity_manager_oauth2::access_token_lifetime_secs
    $refresh_token_lifetime_secs = $mobi_aaa_stub_identity_manager_oauth2::refresh_token_lifetime_secs
    $anonymous_scope = $mobi_aaa_stub_identity_manager_oauth2::anonymous_scope
    $service_asset = $mobi_aaa_stub_identity_manager_oauth2::service_asset

    file { "/opt/mobi-aaa/config/mobi-aaa-stub-identity-manager-oauth2/application-config-override.properties":
        ensure => file,
        content => template("mobi_aaa_stub_identity_manager_oauth2/application-config-override.properties.erb"),
        notify => Class["tomcat::service"],
        require => Class["mobi_aaa_stub_identity_manager_oauth2::install"],
    }
}
