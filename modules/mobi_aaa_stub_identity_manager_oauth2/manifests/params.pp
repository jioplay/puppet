class mobi_aaa_stub_identity_manager_oauth2::params {
    $identity_ver = "5.3.0-233877"
    $access_token_lifetime_secs = undef
    $refresh_token_lifetime_secs = undef
    $anonymous_scope = undef
    $service_asset = undef

}
