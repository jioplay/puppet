class mobi_aaa_stub_identity_manager_oauth2::install {
    package { "mobi-aaa-stub-identity-manager-oauth2-${mobi_aaa_stub_identity_manager_oauth2::identity_ver}" :
        ensure => present,
  notify => Class["tomcat::service"]
    }
}
