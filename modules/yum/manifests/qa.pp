# Helper class for qa repos, which includes repos from staging, and prod.

class yum::qa (
    # branch is one of daily, or stable
    $branch = "stable",
    $server = "pulp",
) {

    # qa hosts/environments include all staging repos
    class { "yum::staging":
        branch => $branch,
        server => $server,
    }

    class { "yum::repos::qa":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    class { "yum::repos::qa_thirdparty":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    class { "yum::repos::mobi_untested_thirdparty":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    class { "yum::repos::mobi_untested":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    # include module name in motd
    os::motd::register { $name : }
}
