class yum::cleanup {
    # clean the yum.repos.d directory of any non_managed files
    file { "/etc/yum.repos.d":
        ensure  => directory,
        purge   => true,
        recurse => true,
    }
}
