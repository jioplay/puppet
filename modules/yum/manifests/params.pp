# Class: yum::params
#
# This module manages yum paramaters
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
#
class yum::params {

  # lsbmajdistrelease fact not guaranteed (lsb_release required)
  $majordistrelease = inline_template("<%= @operatingsystemrelease.split('.')[0] -%>")

  # Determine whether major.minor or just major should be used in repo baseurl.
  #
  # We want 5.6 or later to use repo with latest packages, e.g.
  # baseurl: http://server/pulp/repos/prod/centos/6/os/i386/
  # Alternatively, we want 5.5 or lower to use archived vault repo major.minor

  if $::operatingsystemrelease > 5.6 {
    $repohost = "centos"
    $release = inline_template("<%= @operatingsystemrelease.split('.')[0] -%>")
  } else {
    $repohost = "vault"
    $release = $::operatingsystemrelease
  }

  # centos
  $priority_centos_os = "5"
  $priority_centos_updates = "5"
  $priority_centos_extras = "20"
  $priority_centos_centosplus = "30"

  # legacy RH/Centos use the vault as origin
  $priority_vault_os = "5"
  $priority_vault_updates = "5"
  $priority_vault_extras = "20"
  $priority_vault_centosplus = "30"

  # epel
  $priority_epel = "5"

  # puppetlabs
  $priority_puppetlabs_products = "5"
  $priority_puppetlabs_dependencies = "5"

  # foreman
  $priority_puppetlabs_stable = "5"
  $priority_puppetlabs_test = "5"

  # phusion (passenger)
  $priority_phusion = "10"

  # cloudera
  $priority_cloudera = "5"

  # postgresql
  $priority_postgresql = "5"

  # nginx
  $priority_nginx = "5"

  # varnish
  $priority_varnish = "5"

  # Reliance production repo
  $priority_ril_prod = "45"
  $priority_ril_prod_qa = "45"
  # pulp
  $priority_pulp = "5"

  # new mobi replacement repos
  $priority_dev = "45"
  $priority_qa = "45"
  $priority_staging = "45"
  $priority_prod = "45"
  $priority_qa_thirdparty = "45"
  $priority_staging_thirdparty = "45"
  $priority_prod_thirdparty = "45"

  # mobi
  $priority_mobi_untested = "45"
  $priority_mobi_untested_thirdparty = "45"
  $priority_mobi_staging = "45"
  $priority_mobi_staging_thirdparty = "45"
  $priority_mobi_ops = "5"
  $priority_mobi_thirdparty = "45"
  $priority_mobi_gold_mastered = "45"
  $priority_mobi_reliancepoc = "45"
  $priority_mobi_cms_unstable = "45"
  $priority_mobi_snapshot = "45"

  # rpmforge
  $priority_rpmforge = "10"
}
