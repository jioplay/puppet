# Helper class for staging repos, which includes repos from prod.

class yum::staging (
    # branch is one of daily, untested, or stable
        $branch = "stable",
        $server = "pulp",
) {

    # staging hosts/environments include all prod repos
    class { "yum::prod":
        branch => $branch,
        server => $server,
    }

    class { "yum::repos::staging":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    class { "yum::repos::staging_thirdparty":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    # Mobi repos
    class { "yum::repos::mobi_staging_thirdparty":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    class { "yum::repos::mobi_staging":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    # include module name in motd
    os::motd::register { $name : }
}
