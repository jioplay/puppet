class yum {

  case $::operatingsystemrelease {
    /^[4,6].*/: { $priorities_package = "yum-plugin-priorities" }
    /^5.*/: { $priorities_package = "yum-priorities" }
    default: { fail("Unrecognized operating system release: $::operatingsystemrelease") }
  }

  package { $priorities_package:
    ensure   => present,
    provider => yum,
  }

  # required package to obtain lsbmajordistrelease fact, used here.
  package { "redhat-lsb": ensure => present }

  # rc script gives incorrect exit status of 0 when process not running
  # and status arg used. Switching to process table lookup using pattern.
  service { "yum-updatesd":
    ensure     => stopped,
    enable     => false,
    hasstatus  => false,
    hasrestart => true,
    pattern    => "yum-updatesd",
  }
}
