# Helper class for qa repos, which includes repos from staging, and prod.
class yum::ril_qa (
    # branch is one of daily, or stable
    $branch = "stable",
    $server = "pulp",
    $reponame = "centos",
) {

    # qa hosts/environments include all staging repos
  class { "yum::cleanup": }

  class { "yum::repos::ril_prod_qa":
    server => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::${reponame}_os":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::${reponame}_updates":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::${reponame}_extras":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::epel":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::rpmforge":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::puppetlabs_products":
      branch  => $branch,
      server  => $server,
      require => Class["yum::cleanup"],
   }

  class { "yum::repos::puppetlabs_dependencies":
      branch  => $branch,
      server  => $server,
      require => Class["yum::cleanup"],
  }

  class { "yum::repos::varnish":
          branch  => $branch,
          server  => $server,
          require => Class["yum::cleanup"],
        }

    # include module name in motd
    os::motd::register { $name : }
}
