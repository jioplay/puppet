define yum::protect ( $repo = $title ) {
    file { "/etc/yum.repos.d/${repo}.repo":
        ensure => present,
        owner => "root",
        group => "root",
    }
}
