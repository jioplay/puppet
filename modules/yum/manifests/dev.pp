# Helper class for default dev repos, which includes all repos from qa, staging, prod.
class yum::dev (
    # branch is one of daily, untested, or stable
    $branch = "stable",
    $server = "pulp",
) {

    # dev hosts/environments include all qa, staging, prod repos
    class { "yum::qa":
        branch => $branch,
        server => $server,
    }

    class { "yum::repos::dev":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    class { "yum::repos::mobi_snapshot":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    class { "yum::repos::mobi_cms_unstable":
        server  => $server,
        require => Class["yum::cleanup"],
    }

    # include module name in motd
    os::motd::register { $name : }
}
