# Helper class for prod repos, these repos are common to all systems environment.

class yum::prod (
  # branch is one of daily, or stable
  $branch = "stable",
  $server = "pulp",
) {

  class { "yum": }
  class { "yum::cleanup": }

  if ($::operatingsystemrelease < 5.2) {
    $reponame = "vault"
  } else {
    $reponame = "centos"
  }

  class { "yum::repos::${reponame}_os":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::${reponame}_updates":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::${reponame}_extras":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::epel":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::rpmforge":
    branch  => $branch,
    server  => $server,
    require => Class["yum::cleanup"],
  }

  if ( $::operatingsystemrelease >= 5 ) {
    class { "yum::repos::postgresql":
      branch  => $branch,
      server  => $server,
      require => Class["yum::cleanup"],
    }

    class { "yum::repos::puppetlabs_products":
      branch  => $branch,
      server  => $server,
      require => Class["yum::cleanup"],
    }

    class { "yum::repos::puppetlabs_dependencies":
      branch  => $branch,
      server  => $server,
      require => Class["yum::cleanup"],
    }

    if $::architecture == "x86_64" {

        class { "yum::repos::phusion":
          branch  => $branch,
          server  => $server,
          require => Class["yum::cleanup"],
        }

        class { "yum::repos::nginx":
          branch  => $branch,
          server  => $server,
          require => Class["yum::cleanup"],
        }

        class { "yum::repos::varnish":
          branch  => $branch,
          server  => $server,
          require => Class["yum::cleanup"],
        }
    }
  }
  # new replacement mobi repos
  class { "yum::repos::prod":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::prod_thirdparty":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  # Mobi repos
  class { "yum::repos::mobi_goldmasters_thirdparty":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::mobi_goldmasters":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  # include module name in motd
  os::motd::register { $name : }
}
