class yum::repos::qa (
  $server = undef,
  $priority = $::yum::params::priority_qa,

) inherits yum::params {

  $reponame = "qa"

  # fail hard if server undef
  if $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $baseurl = "http://${server}/pulp/repos/${reponame}"

  yumrepo { $reponame:
    descr           => $reponame,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
  }

  yum::protect { $reponame: }
}

