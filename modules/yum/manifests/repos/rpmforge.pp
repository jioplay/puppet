class yum::repos::rpmforge (

  $branch = undef,
  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_rpmforge,

) inherits yum::params {

  $repohost = "rpmforge"
  $reponame = "redhat"
  $release = inline_template("<%= release_full.split('.')[0] -%>")

  # fail hard if branch or server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-${repohost}-${reponame}-${release}-${::architecture}"
  $baseurl = "http://${server}/pulp/repos/${branch}/${repohost}/${reponame}/el${release}/en/${::architecture}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
    exclude         => "nagios* tmux*",
  }


  yum::protect { $repo: }
}

