class yum::repos::cloudera3 (

  $branch = undef,
  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_cloudera3,

) inherits yum::params {

  $repohost = "cloudera"
  $reponame = "redhat/cdh/3"

  # fail hard if branch and server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-cloudera3"
  $baseurl = "http://${server}/pulp/repos/${branch}/${repohost}/${reponame}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}

