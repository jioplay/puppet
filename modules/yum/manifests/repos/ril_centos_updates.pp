class yum::repos::ril_centos_updates (

  $server = undef,
  #$release = $::yum::params::release,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_centos_os,

) inherits yum::params {

  $repohost = "centos"
  $reponame = "updates"
  $release = inline_template("<%= release_full.split('.')[0] -%>")

  # fail hard if server undef
  if  $server == undef {
    fail("\$server param is required!")
  }

  $repo = "${repohost}-${release}-${reponame}-${architecture}"
  $baseurl = "http://${server}/${repohost}/${release}/updates/${architecture}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
    exclude         => "python-imaging*",
  }

  yum::protect { $repo: }
}
