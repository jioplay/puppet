class yum::repos::ril_prod (

  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_ril_prod,

) inherits yum::params {

  $repohost = "ril-prod"


  # fail hard if server undef
  if $server == undef {
    fail("\$server param is required!")
  }

  $repo = "${repohost}"
  $baseurl = "http://${server}/${repohost}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}
