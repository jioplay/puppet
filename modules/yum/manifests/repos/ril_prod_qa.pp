class yum::repos::ril_prod_qa (

  $server = undef,
  $release = $::yum::params::release,
  $priority = $::yum::params::priority_ril_prod_qa,

) inherits yum::params {

  $repohost = "ril"
  $reponame = "prod"

  # fail hard if branch and server undef
  if $server == undef {
    fail("\$server param is required!")
  }

  $repo = "${repohost}-${reponame}"
  $baseurl = "http://${server}/pulp/repos/${repohost}/${reponame}"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}

