class yum::repos::prod_thirdparty (
  $server = undef,
  $priority = $::yum::params::priority_prod_thirdparty,

) inherits yum::params {

  $reponame = "prod-thirdparty"

  # fail hard if server undef
  if $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $baseurl = "http://${server}/pulp/repos/${reponame}"

  yumrepo { $reponame:
    descr           => $reponame,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
  }

  yum::protect { $reponame: }
}

