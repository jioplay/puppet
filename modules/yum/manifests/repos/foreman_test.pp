class yum::repos::foreman_test (

  $branch = undef,
  $server = undef,
  $priority = $::yum::params::priority_foreman_stable,

) inherits yum::params {

  $repohost = "foreman"
  $reponame = "test"

  # fail hard if branch and server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-${repohost}-${reponame}"
  $baseurl = "http://${server}/pulp/repos/${branch}/${repohost}/${reponame}"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}

