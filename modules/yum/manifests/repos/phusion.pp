class yum::repos::phusion (

  $branch = undef,
  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_phusion,

) inherits yum::params {

  $repohost = "passenger"
  $reponame = "rhel"
  $release = inline_template("<%= release_full.split('.')[0] -%>")

  # fail hard if branch and server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-${repohost}-${release}-${architecture}"
  $baseurl = "http://${server}/pulp/repos/${branch}/${repohost}/${reponame}/${release}/${architecture}"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}
