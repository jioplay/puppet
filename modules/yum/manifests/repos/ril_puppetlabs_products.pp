class yum::repos::ril_puppetlabs_products (

  $server       = undef,
  $release_full = $::yum::params::release,
  $priority     = $::yum::params::priority_epel,

) inherits yum::params {

  $repohost = "puppetlabs"
  $reponame = "products"

  case $release_full {
    /^5.*$/: { $release = "5" }
    /^6.*$/: { $release = "6" }
    default: { fail("Unrecognized operating system release: $release_full") }
  }

  # fail hard if server undef
  if $server == undef {
    fail("\$server param is required!")
  }

  $repo = "${repohost}-${reponame}-${release}-${::architecture}"
  $baseurl = "http://${server}/${repohost}/${reponame}/${release}/${::architecture}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
    exclude         => "nagios* tmux*",
  }

  yum::protect { $repo: }
}

