class yum::repos::cloudera4 (

  $branch = undef,
  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_cloudera4,

) inherits yum::params {

  $repohost = "cloudera"
  $reponame = "cdh4/redhat"

  if ( $release_full < 6 ) {
    $release = "5"
  } else {
    $release = "6"
  }

  # fail hard if branch and server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-cloudera4"
  $baseurl = "http://${server}/pulp/repos/${branch}/${repohost}/${reponame}/${release}/${::architecture}/cdh/4"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}

