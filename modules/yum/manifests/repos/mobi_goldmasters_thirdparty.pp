class yum::repos::mobi_goldmasters_thirdparty (
  $server = undef,
  $priority = $::yum::params::priority_mobi_thirdparty,
) inherits yum::params {

  $repohost = "mobi"
  $reponame = "goldmasters-thirdparty"

  # fail hard if server undef
  if $server == undef {
    fail("Error: $name requires \$server param.")
  }

  $repo = "${repohost}-${reponame}"
  $baseurl = "http://${server}/pulp/repos/${repohost}/${reponame}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
    exclude         => "ruby* php*",
  }

  yum::protect { $repo: }
}
