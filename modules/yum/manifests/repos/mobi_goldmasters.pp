class yum::repos::mobi_goldmasters (
  $server = undef,
  $priority = $::yum::params::priority_mobi_gold_mastered,

) inherits yum::params {

  $repohost = "mobi"
  $reponame = "goldmasters"

  # fail hard if server undef
  if $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${repohost}-${reponame}"
  $baseurl = "http://${server}/pulp/repos/${repohost}/${reponame}"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}

