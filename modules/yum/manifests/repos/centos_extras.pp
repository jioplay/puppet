class yum::repos::centos_extras (

  $branch = undef,
  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_centos_extras,

) inherits yum::params {

  $repohost = "centos"
  $reponame = "extras"
  $release = inline_template("<%= release_full.split('.')[0] -%>")

  # fail hard if branch and server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-${repohost}-${release}-${reponame}-${architecture}"
  $baseurl = "http://${server}/pulp/repos/${branch}/${repohost}/${release}/${reponame}/${architecture}"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }

  # Add a corresponding file resource
  yum::protect { $repo: }
}

