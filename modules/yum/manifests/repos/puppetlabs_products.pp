class yum::repos::puppetlabs_products (

  $branch = undef,
  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_puppetlabs_products,

) inherits yum::params {

  $repohost = "puppetlabs"
  $reponame = "products"

  if ( $release_full < 6 ) {
    $release = "5"
  } else {
    $release = "6"
  }

  # fail hard if branch and server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-${repohost}-${release}-${reponame}-${architecture}"
  $baseurl = "http://${server}/pulp/repos/${branch}/${repohost}/el/${release}/${reponame}/${architecture}"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }


  yum::protect { $repo: }
}

