class yum::repos::pulp (

  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_pulp,

) inherits yum::params {

  if ( $release_full < 6 ) {
      fail("${name} requires CentOS 6.2 or later.")
  }

  $repo = 'pulp-v2-production-release'
  $baseurl = 'http://repos.fedorapeople.org/repos/pulp/pulp/v2/stable/$releasever/$basearch/'

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}
