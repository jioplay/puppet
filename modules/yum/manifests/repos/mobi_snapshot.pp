class yum::repos::mobi_snapshot (

  $server = undef,
  $release = $::yum::params::release,
  $priority = $::yum::params::priority_mobi_snapshot,

) inherits yum::params {

  $repohost = "mobi"
  $reponame = "snapshot"

  # fail hard if branch and server undef
  if $server == undef {
    fail("\$server param is required!")
  }

  $repo = "${repohost}-${reponame}"
  # not using pulp since snapshot in filename and not metadata..not okay with pulp
  #$baseurl = "http://${server}/pulp/repos/${repohost}/${reponame}"
  $baseurl = "http://172.16.2.136/yum/snapshot/mobi"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}

