class yum::repos::mobi_ops (

  $branch = undef,
  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_mobi_ops,

) inherits yum::params {

  $repohost = "mobi"
  $reponame = "ops"

  $release = inline_template("<%= @operatingsystemrelease.split('.')[0] -%>")
  #if ( $release_full < 6 ) {
  #  $release = "5"
  #} else {
  #  $release = "6"
  #}

  # fail hard if branch and server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-${repohost}-${release}-${reponame}-${architecture}"
  $baseurl = "http://${server}/pulp/repos/${branch}/${reponame}/${release}/${architecture}"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }


  yum::protect { $repo: }
}

