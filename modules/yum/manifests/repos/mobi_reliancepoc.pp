class yum::repos::mobi_reliancepoc (

  $server = undef,
  $release = $::yum::params::release,
  $priority = $::yum::params::priority_mobi_reliancepoc,

) inherits yum::params {

  $repohost = "mobi"
  $reponame = "mobi_reliancepoc"

  # fail hard if branch and server undef
  if $server == undef {
    fail("\$server param is required!")
  }

  $repo = "${repohost}-${reponame}"
  $baseurl = "http://${server}/pulp/repos/${repohost}/${reponame}"

  yumrepo { $repo:
    descr     => $repo,
    baseurl   => $baseurl,
    priority  => $priority,
    enabled   => "1",
    gpgcheck  => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}

