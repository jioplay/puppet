class yum::repos::vault_centosplus (

  $branch = undef,
  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_vault_centosplus,

) inherits yum::params {

  $repohost = "vault"
  $reponame = "centosplus"

  case $release_full {
    /^4.*$/: { $release = "4.5" }
    /^5.*[0-1]$/: { $release = "5.3" }
    /^5.*[2-6]$/: { $release = "5.6" }
    default: { fail("Unrecognized operating system release: $release_full") }
  }

  # fail hard if branch and server undef
  if $branch == undef or $server == undef {
    fail("\$branch and \$server params are required!")
  }

  $repo = "${branch}-${repohost}-${release}-${reponame}-${::architecture}"
  $baseurl = "http://${server}/pulp/repos/${branch}/${repohost}/${release}/${reponame}/${::architecture}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}

