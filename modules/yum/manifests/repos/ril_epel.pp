class yum::repos::ril_epel (

  $server       = undef,
  $release_full = $::yum::params::release,
  $priority     = $::yum::params::priority_epel,

) inherits yum::params {

  $repohost = "epel"
  $reponame = ""

  case $release_full {
    /^5.*$/: { $release = "5" }
    /^6.*$/: { $release = "6" }
    default: { fail("Unrecognized operating system release: $release_full") }
  }

  # fail hard if server undef
  if $server == undef {
    fail("\$server param is required!")
  }

  $repo = "${repohost}-${release}-${::architecture}"
  $baseurl = "http://${server}/${repohost}/${release}/${::architecture}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
    exclude         => "ganglia* varnish* haproxy* tmux*",
  }

  yum::protect { $repo: }
}

