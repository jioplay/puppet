class yum::repos::ril_varnish (

  $server = undef,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_varnish,

) inherits yum::params {

  $repohost = "varnish"

  if ( $release_full < 6 ) {
    $release = "5"
  } else {
    $release = "6"
  }

  # fail hard if server undef
  if $server == undef {
    fail("\$server param is required!")
  }

  $repo = "${repohost}-${release}-${::architecture}"
  $baseurl = "http://${server}/${repohost}/${release}/${::architecture}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
  }

  yum::protect { $repo: }
}
