class yum::repos::ril_centos_os (

  $server = undef,
  #$release = $::yum::params::release,
  $release_full = $::yum::params::release,
  $priority = $::yum::params::priority_centos_os,

) inherits yum::params {

  $repohost = "centos"
  $reponame = "os"
  $release = inline_template("<%= release_full.split('.')[0] -%>")

  # fail hard if branch and server undef
  if $server == undef {
    fail("\$server params are required!")
  }

  $repo = "${repohost}-${release}-${reponame}-${architecture}"
  $baseurl = "http://${server}/${repohost}/${release}/os/${architecture}"

  yumrepo { $repo:
    descr           => $repo,
    baseurl         => $baseurl,
    priority        => $priority,
    enabled         => "1",
    gpgcheck        => "0",
    metadata_expire => "600",
    exclude         => "python-imaging*",
  }

  yum::protect { $repo: }
}
