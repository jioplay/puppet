# Helper class for Ril prod repos. All reliance servers will share them

class yum::ril (
  $server = "yum.mum1.r4g.com",
  $reponame = "centos",
) {

  class { "yum": }
  class { "yum::cleanup": }

  class { "yum::repos::ril_${reponame}_os":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::ril_${reponame}_updates":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::ril_${reponame}_extras":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::ril_epel":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  class { "yum::repos::ril_rpmforge":
    server  => $server,
    require => Class["yum::cleanup"],
  }

  if ( $::operatingsystemrelease >= 5 ) {

    class { "yum::repos::ril_puppetlabs_products":
      server  => $server,
      require => Class["yum::cleanup"],
    }

    class { "yum::repos::ril_puppetlabs_dependencies":
      server  => $server,
      require => Class["yum::cleanup"],
    }
    class { "yum::repos::ril_prod":
      server => $server,
      require => Class["yum::cleanup"],
    }

    if $::architecture == "x86_64" {

        class { "yum::repos::ril_varnish":
          server  => $server,
          require => Class["yum::cleanup"],
        }
    }
  }

  # include module name in motd
  os::motd::register { $name : }
}
