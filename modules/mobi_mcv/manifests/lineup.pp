# == Class: mobi_mcv::lineup
#
#  installs lineup component
#
# === Parameters:
#
#    $mobi_mcv_lineup_tool_version::
#
# === Requires:
#
#     java
#     tomcat
#     user::rtv
#
# === Sample Usage
#
#  class { "mobi_mcv::lineup" :
#           mobi_mcv_lineup_tool_version => "5.0.0-205386",
#            mobi_mcv_loader_app_version => "5.0.0-205326",
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mcv::lineup (
    $mobi_mcv_lineup_tool_version = $mobi_mcv::lineup::params::mobi_mcv_lineup_tool_version,
    $mobi_mcv_loader_app_version = $mobi_mcv::lineup::params::mobi_mcv_loader_app_version,
)
inherits mobi_mcv::lineup::params {
    include mobi_mcv::lineup::install
    anchor { "mobi_mcv::lineup::begin":} -> Class["mobi_mcv::lineup::install"]
    anchor { "mobi_mcv::lineup::end": }
    os::motd::register { $name : }
}
