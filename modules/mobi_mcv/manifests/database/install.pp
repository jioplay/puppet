class mobi_mcv::database::install {

    file { "/var/mobi-mcv":
      ensure => "directory",
      owner  => "rtv",
      group  => "rtv",
      mode   => "0755",
    }

    file { "/var/mobi-mcv/database":
        ensure => "directory",
        owner  => "rtv",
        group  => "rtv",
        mode   => "0755",
        recurse => true,
        source => "puppet:///modules/mobi_mcv/database",
        require => File["/var/mobi-mcv"],
    }

    file { "/var/mobi-mcv/database/install.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_mcv/database/install.sh.erb'),
        require => File["/var/mobi-mcv"],
    }

    file { "/var/mobi-mcv/database/uninstall.sh":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0775",
        content => template('mobi_mcv/database/uninstall.sh.erb'),
        require => File["/var/mobi-mcv"],
    }

    file { "/var/mobi-mcv/database/mcv_create_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_mcv/database/mcv_create_databases.sql.erb'),
        require => File["/var/mobi-mcv"],
    }

    file { "/var/mobi-mcv/database/mcv_delete_databases.sql":
        ensure => present,
        owner  => "rtv",
        group  => "rtv",
        mode   => "0664",
        content => template('mobi_mcv/database/mcv_delete_databases.sql.erb'),
        require => File["/var/mobi-mcv"],
    }

    #test to see if we have been run
    exec { "Install DB":
        command => "/bin/bash /var/mobi-mcv/database/install.sh",
        onlyif => "/usr/bin/test ! -e /var/mobi-mcv/database/installation.done",
        logoutput => "true",
        require => File["/var/mobi-mcv/database"],
    }

}
