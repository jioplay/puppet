# == Class: mobi_mcv::database
#
#  installs mcv database
#
# === Parameters:
#
#   $database_root_password::
#   $dam_username::
#   $dam_password::
#
# === Requires:
#
#     mysql
#
# === Sample Usage
#
#  class { "mobi_mcv::database" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mcv::database (
    $database_root_password = $mobi_mcv::database::params::database_root_password,
    $mcv_username = $mobi_mcv::database::params::mcv_username,
    $mcv_password = $mobi_mcv::database::params::mcv_password,
)
  inherits mobi_mcv::database::params {
    include mobi_mcv::database::install
    anchor { "mobi_mcv::database::begin":} -> Class["mobi_mcv::database::install"]
    Class["mobi_mcv::database::install"] -> anchor { "mobi_mcv::database::end": }
    os::motd::register { $name : }
}
