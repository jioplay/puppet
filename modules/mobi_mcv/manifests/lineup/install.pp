class mobi_mcv::lineup::install {

    package {"mobi-mcv-lineup-tool-${mobi_mcv::lineup::mobi_mcv_lineup_tool_version}":
      ensure => present,
      notify => Class["tomcat::service"],
    }
    package {"mobi-mcv-loader-app-${mobi_mcv::lineup::mobi_mcv_loader_app_version}":
      ensure => present,
      notify => Class["tomcat::service"],
    }
}
