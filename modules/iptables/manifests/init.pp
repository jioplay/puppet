# == Class: iptables
#
# installs iptables configures firewall rules and starts
#
# === Parameters:
#
#    $configfile::      iptables config file
#    $srvc_ensure::  state of service
#    $srvc_enable::  state of service at boot
#    $cnfg_own::     config file owner
#    $cnfg_grp::     config file group
#    $cnfg_mode::    config file mode
#
# === Requires
#
#
# === Sample Usage
#
#  class { "iptables" :
#    configfile => "iptables.mgo",
#  }
#
# Remember: No empty lines between comments and class definition
#
class iptables (
    $configfile = $iptables::params::configfile,
    $srvc_ensure = $iptables::params::srvc_ensure,
    $srvc_enable = $iptables::params::srvc_enable,
    $cnfg_own = $iptables::params::cnfg_own,
    $cnfg_grp = $iptables::params::cnfg_grp,
    $cnfg_mode = $iptables::params::cnfg_mode,
)
inherits iptables::params {

    include iptables::install, iptables::config, iptables::service
    anchor { "iptables::begin": } -> Class["iptables::install"]
    Class["iptables::service"] -> anchor { "iptables::end":}
    os::motd::register { "iptables": }
}
