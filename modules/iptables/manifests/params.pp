class iptables::params {
    $configfile="iptables.mgo"
    $srvc_ensure = stopped
    $srvc_enabled = false
    $cnfg_own = "rtv"
    $cnfg_grp = "rtv"
    $cnfg_mode = "444"
}
