class iptables::config {

    file { "/etc/sysconfig/iptables":
        replace => true,
        owner  => $iptables::cnfg_own,
        group  => $iptables::cnfg_grp,
        mode => $iptables::cnfg_mode,
        source => "puppet:///modules/iptables/${iptables::configfile}",
        notify   => Service["iptables"],
        require => Class["iptables::install"],
    }

}
