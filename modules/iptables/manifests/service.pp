class iptables::service {

    service { "iptables":
      ensure => $iptables::srvc_ensure,
      enable => $iptables::srvc_enable,
      require => Class["iptables::install"],
    }

}
