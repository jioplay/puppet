# == Class: aaa_adapter_sprintws
#
#  installs adapter_sprint_ws ng component
#
# === Parameters:
#
#    $adapter_sprint_ws_ver::
#
# === Requires:
#
#     java
#     tomcat
#
# === Sample Usage
#
#   class { "aaa_adapter_sprintws" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class aaa_adapter_sprintws (
    ###icinga.pp
    $icinga = $aaa_adapter_sprintws::params::icinga,
    $icinga_instance = $aaa_adapter_sprintws::params::icinga_instance,
    $icinga_cmd_args = $aaa_adapter_sprintws::params::icinga_cmd_args,
    ###end icinga.pp
    $adapter_sprint_ws_ver = $aaa_adapter_sprintws::params::adapter_sprint_ws_ver,
    $sprint_wsip_soap_url = $aaa_adapter_sprintws::params::sprint_wsip_soap_url,
    $sprint_psi_soap_url = $aaa_adapter_sprintws::params::sprint_psi_soap_url,
    $sprint_health_check_url = $aaa_adapter_sprintws::params::sprint_health_check_url,
    $delay_notification_by = $aaa_adapter_sprintws::params::delay_notification_by,
    $sprint_encryption_jar_path = $aaa_adapter_sprintws::params::sprint_encryption_jar_path,
    $healthcheck_enabled = $aaa_adapter_sprintws::params::healthcheck_enabled,
    $package_ensure = "present",

)
inherits aaa_adapter_sprintws::params {
    include aaa_adapter_sprintws::install, aaa_adapter_sprintws::config
    anchor { "aaa_adapter_sprintws::begin": } -> Class["aaa_adapter_sprintws::install"]
    Class["aaa_adapter_sprintws::config"] -> anchor { "aaa_adapter_sprintws::end" :}
    os::motd::register { $name : }
}
