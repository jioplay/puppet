class aaa_adapter_sprintws::install {
  package { "mobi-aaa-adapter-sprint-ws":
    ensure => "${aaa_adapter_sprintws::adapter_sprint_ws_ver}",
    notify => Class["tomcat::service"],
  }
}