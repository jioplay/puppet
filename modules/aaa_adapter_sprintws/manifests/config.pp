class aaa_adapter_sprintws::config {

    $sprint_wsip_soap_url = $aaa_adapter_sprintws::sprint_wsip_soap_url
    $sprint_health_check_url = $aaa_adapter_sprintws::sprint_health_check_url
    $delay_notification_by = $aaa_adapter_sprintws::delay_notification_by
    $sprint_encryption_jar_path = $aaa_adapter_sprintws::sprint_encryption_jar_path
    $healthcheck_enabled = $aaa_adapter_sprintws::healthcheck_enabled

  file { "/opt/mobi-aaa-adapter-sprint-ws/conf/application-config-override.xml":
    ensure => file,
    content => template("aaa_adapter_sprintws/applicationProperties.xml.erb"),
    notify => Class["tomcat::service"],
    require => Class["aaa_adapter_sprintws::install"],
    }
}
