class aaa_adapter_sprintws::params {
    ###icinga.pp
    $icinga = false
    $icinga_instance = undef
    $icinga_cmd_args = "-I $::ipaddress -p 8080 -u /mobi-aaa-adapter-sprint-ws/monitoring/health -w 5 -c 10"
    ###end icinga.pp
    $adapter_sprint_ws_ver = "5.2.0-215699"
    $sprint_wsip_soap_url = undef
    $sprint_psi_soap_url = undef
    $sprint_health_check_url = undef
    $delay_notification_by = undef
    $sprint_encryption_jar_path = undef
    $healthcheck_enabled = undef
}
