class ganglia::gmond (

  ### gmond.pp
  $gmond_package = $ganglia::params::gmond_package,
  $gmond_package_ensure = $ganglia::params::gmond_package_ensure,

  $gmond_service = $ganglia::params::gmond_service,
  $gmond_service_ensure = $ganglia::params::gmond_service_ensure,
  $gmond_service_enable = $ganglia::params::gmond_service_enable,

  $gmond_ismaster = true,

  $cluster_name = $ganglia::params::cluster_name,
  $cluster_owner = $ganglia::params::cluster_owner,
  $cluster_url = $ganglia::params::cluster_url,
  $host_location = $ganglia::params::host_location,

) inherits ganglia::params {

  if ! $cluster_name {
    fail("\$cluster_name is a required parameter to ${name}.")
  }

  package { $gmond_package:
    ensure  => $gmond_package_ensure,
    notify  => Service[$gmond_service],
  }

  File {
    owner   => "root",
    group   => "root",
    mode    => "0644",
    notify  => Service[$gmond_service],
    require => Package[$gmond_package],
  }

  if ($::operatingsystemrelease < 6) and ($::architecture == "i386") {
      file { "/etc/gmond.conf":
          ensure  => file,
          content => template("ganglia/gmond/gmond.conf.3.0.erb"),
      }
  } else {
      file { "/etc/ganglia/gmond.conf":
        ensure  => file,
        content => template("ganglia/gmond/gmond.conf.erb"),
      }
  }

  file { "/etc/ganglia":
    ensure  => directory,
  }

  file { "/etc/ganglia/conf.d":
    ensure  => directory,
    recurse => true,
    purge   => false,
    source  => "puppet:///modules/ganglia/conf.d",
  }

  file { "/usr/lib64/ganglia/python_modules":
    ensure  => directory,
    recurse => true,
    purge   => false,
    source  => "puppet:///modules/ganglia/python_modules",
  }

  file { "/etc/ganglia/gmond.d":
    ensure  => directory,
    recurse => true,
    purge   => true,
  }

  # collect exported udp_send_channel stanzas/files from cluster masters
  File <<| tag == "gmond_cluster_${cluster_name}" |>> {
    require => File["/etc/ganglia/gmond.d"],
  }

  if $gmond_ismaster {
    # export udp_send_channel stanza/file
    @@file { "/etc/ganglia/gmond.d/${::fqdn}.conf":
      ensure  => file,
      content => template("ganglia/gmond/udp_send_channel.erb"),
      tag     => "gmond_cluster_${cluster_name}",
    }

    # export cluster_name dir
    @@exec { "create_cluster_dir_${::fqdn}":
      command => "/bin/mkdir -p /tmp/gmetad/${cluster_name}",
      unless  => "/usr/bin/test -d /tmp/gmetad/${cluster_name}",
      tag     => "gmetad-directory",
      before  => File["/tmp/gmetad/${cluster_name}/${::fqdn}"],
    }

    # export cluster_name and fqdn for gmetad data_source line.
    @@file { "/tmp/gmetad/${cluster_name}/${::fqdn}":
      ensure  => file,
      tag     => "gmetad-file",
    }
  }

  service { $gmond_service:
    ensure  => $gmond_service_ensure,
    enable  => $gmond_service_enable,
    require => Package[$gmond_package],
  }
}
