class ganglia::config (
){

    if ! $::ganglia::gridname {
        fail("\$gridname is a required parameter of the ganglia class.")
    } else {
        # in gmetad.conf.erb
        $gridname = $::ganglia::gridname
    }

  File {
    ensure  => present,
    owner   => "ganglia",
    group   => "ganglia",
    mode    => "0644",
    require => Class["ganglia::install"],
  }

  file { "/etc/ganglia/conf.php":
    source => "puppet://$puppetserver/modules/ganglia/${::ganglia::ganglia_php_conf}",
  }

  file { "/etc/httpd/conf.d/ganglia.conf":
    owner   => "root",
    group   => "root",
    mode    => "0644",
    source  => "puppet://$puppetserver/modules/ganglia/${::ganglia::apache_conf}",
    notify  => Service["httpd"],
  }

  file { "/etc/ganglia/htpasswd.users":
    source  => "puppet://$puppetserver/modules/ganglia/${::ganglia::htpasswd_conf}",
  }

  $gmetad_header = "/etc/ganglia/gmetad.header"
  file { $gmetad_header:
    ensure  => file,
    content => template("ganglia/gmetad.conf.erb"),
    notify  => Exec["assemble_gmetad"],
  }

  file { "/tmp/gmetad":
    ensure => directory,
  }

  # collect exported gmond cluster master files
  File <<| tag == "gmetad-file" |>> {
    require => File["/tmp/gmetad"],
    notify  => Exec["assemble_gmetad"],
    before  => Exec["delete_empty_dirs"],
  }

  Exec <<| tag == "gmetad-directory" |>> {
    require => File["/tmp/gmetad"],
  }

  # remove empty /tmp/gmetad/ dirs, happens after cluster name change leaves cluster with zero members
  # left there would result in bad gmetad.conf
  exec { "delete_empty_dirs":
      cwd     => "/tmp/gmetad",
      path    => ["/bin","/usr/bin"],
      command => "bash -c 'find . -type d -empty -exec rm -rf {} +'",
      onlyif  => "bash -c 'test -n `find . -type d -empty`'",
      notify  => Exec["assemble_gmetad"],
  }

  $gmetad_conf = "/etc/ganglia/gmetad.conf"
  # assemble data_source lines with first five masters for each cluster, concatenated with gmetad_header
  $cmd = "bash -c '(cat ${gmetad_header}; for f in `ls -1`; do echo \"data_source \${f} `ls -1 \${f}/ | head -5 | tr \"\\n\" \" \" `\"; done ) > ${gmetad_conf}' "
  # notify {"\$cmd is: $cmd": }
  exec { "assemble_gmetad":
    refreshonly => true,
    path        => ["/bin", "/usr/bin"],
    cwd         => "/tmp/gmetad",
    command     => $cmd,
  }
}
