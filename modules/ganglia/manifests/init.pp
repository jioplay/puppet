# Class: ganglia
#
# This module manages ganglia
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class ganglia (

  ### init.pp
  $ganglia_package = $ganglia::params::ganglia_package,
  $ganglia_package_ensure = $ganglia::params::ganglia_package_ensure,

  ### service.pp
  $gmetad_service = $ganglia::params::gmetad_service,
  $gmetad_service_ensure = $ganglia::params::gmetad_service_ensure,
  $gmetad_service_enable = $ganglia::params::gmetad_service_enable,

  ### config.pp
  $ganglia_php_conf = $ganglia::params::ganglia_php_conf,
  $apache_conf = $ganglia::params::apache_conf,
  $htpasswd_conf = $ganglia::params::htpasswd_conf,
  $gridname = $ganglia::params::gridname,

) inherits ganglia::params {

  anchor { "ganglia::begin": } ->
  class { "ganglia::install": } ->
  class { "ganglia::config": } ->
  anchor { "ganglia::end": }

  # outside anchor so modules can notify service
  class { "ganglia::service": }

  # include module name in motd
  os::motd::register { "${name}": }
}
