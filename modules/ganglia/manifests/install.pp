class ganglia::install {
     # ganglia packages renamed for 3.4, a ganglia rpm no longer exists and
     # must be removed as it conflicts.

     package { $::ganglia::ganglia_package:
          ensure  => $::ganglia::ganglia_package_ensure,
     }
}
