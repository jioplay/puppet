#
# Class: ganglia::params
#
# This module manages ganglia paramaters
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
class ganglia::params {


  ### install.pp
  # package nomenclature changed from 3.1 to 3.4, this is 3.4 rpmforge compatible
  $ganglia_package = [ "ganglia-web", "ganglia-gmetad", "ganglia-debuginfo" ]
  $ganglia_package_ensure = "present"

  ### service.pp
  $gmetad_service = "gmetad"
  $gmetad_service_ensure = "running"
  $gmetad_service_enable = "true"

  ### config.pp
  $ganglia_php_conf = "conf.php"
  $apache_conf = "ganglia.conf"
  $htpasswd_conf = "htpasswd.users"
  $gridname = undef

  ### optional subclasses

  ### gmond.pp
  #if else doing same thing...yes i know, just testing, would clean up
  #changed else statement cos we now have ganglia-gmond-python in the el6 repo
  if ($::operatingsystemrelease < 6) {
      $gmond_package = [ "ganglia-gmond", "ganglia-gmond-python" ]
  } else {
      $gmond_package = [ "ganglia-gmond", "ganglia-gmond-python" ]
  }

  $gmond_package_ensure = "present"

  $gmond_service = "gmond"
  $gmond_service_ensure = "running"
  $gmond_service_enable = "true"

  $gmond_ismaster = true

  $cluster_name = undef
  $cluster_owner = "unspecified"
  $cluster_url = "unspecified"
  $host_location = "unspecified"

}
