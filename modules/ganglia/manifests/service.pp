class ganglia::service {

  service { "ganglia":
    name       => $::ganglia::gmetad_service,
    ensure     => $::ganglia::gmetad_service_ensure,
    enable     => $::ganglia::gmetad_service_enable,
    hasstatus  => true,
    hasrestart => true,
    subscribe  => [ Class["ganglia::install"], Class["ganglia::config"] ],
  }

}
