class mobi_rest_api_stub::config {
    file { "/opt/mobi-rest-api-stub/webapp/WEB-INF/classes/default.xml":
        replace => true,
        owner  => "rtv",
        group  => "rtv",
        mode => "0444",
        content => template('mobi_rest_api_stub/default.xml.erb'),
        require => Class["mobi_rest_api_stub::install"],
        notify   => Class["tomcat::service"],
    }
}
