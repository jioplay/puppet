 ## == Class: mobi_rest_api_stub
#
# Manage the mobi_rest_api_stub component.
#
# === Parameters:
#
# List parameters and descriptions here with (default value)
#
# === Actions:
#
# Describe the actions of this class here
#
# === Examples:
#
# Put example of class usage in node manifest here
#
# [Remember: No empty lines between comments and class definition]
 #
class mobi_rest_api_stub (
  $version = $mobi_rest_api_stub::params::mobi_rest_api_stub_version,
  $suppress_missing_policyfile_error = $mobi_rest_api_stub::params::suppress_missing_policyfile_error,
) inherits mobi_rest_api_stub::params {

    anchor { "mobi_rest_api_stub::begin": } ->
    class { "mobi_rest_api_stub::install": } ->
    class { "mobi_rest_api_stub::config": } ->
    anchor { "mobi_rest_api_stub::end": }

    os::motd::register{ "${name}":}
}
