class mobi_rest_api_stub::install {
  include(tomcat::install)
  package { "mobi-rest-api-stub":
    ensure => "${mobi_rest_api_stub::version}",
    notify => [ Class["tomcat::service"], ]
  }
}
